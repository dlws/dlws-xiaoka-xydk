var price = "";
var money = parseFloat($(".money_deposit").html())
$(".deposit_money input").on("input", function() {
	price = $(this).val();

	var length = $(this).val().length
	if(length != 0) {
		$(this).css({
			"fontSize": "9.6vw",
			"margin": "2px 0 8px"
		})
		$(".deposit_money span").css("margin", "18px 0 0")
	} else {
		$(this).css({
			"fontSize": "3.733vw",
			"marginTop": "6px"
		})
		$(".deposit_money span").css("margin", "0 0 11px")
	}

	if(parseFloat(price) > parseFloat(money)) {
		$(".warn_money").show();
		$(".money_yu").hide();
	} else {
		$(".warn_money").hide();
		$(".money_yu").show();
	}
	if(parseFloat(price) <= parseFloat(money) && parseFloat(price) != 0) {
		$(".deposit_foot").addClass("deposit_green");
	}else{
		$(".deposit_foot").removeClass("deposit_green");
	}
	
});
var h=$(document).height();

//提现按钮
$(".deposit_foot").click(function() {

	if(parseFloat(price) <= parseFloat(money) && parseFloat(price) != 0) {
		$(".deposit_foot").addClass("deposit_green");
		$(".mask_common").css("height",h).show();
		$(".deposit_popup").show();
		putMoney();

	} else {
		$(".deposit_foot").removeClass("deposit_green");

	}
})
$(".ensure").click(function(){
	$(".deposit_popup").hide();
	$(".mask_common").hide();
	$("#formId").submit();
})
$(".close").click(function(){
	$(".deposit_popup").hide();
	$(".mask_common").hide();
})
//点击全部提现
$(".total_deposit").click(function(){
	
	$(".deposit_money input").val(money);
	if(money == 0){
		alert("当前可提现余额不足");
		return
	}
	$(".deposit_foot").addClass("deposit_green");
		$(".mask_common").css("height",h).show()
		$(".deposit_popup").show();
	$(".deposit_money input").css({
			"fontSize": "9.6vw",
			"margin": "2px 0 8px"
		})
	$(".deposit_money span").css("margin", "18px 0 0")
	
	putMoney();
})

//将当前文本框中的内容放入到span标签中
function putMoney(){
	
	//获取当前文本框中的文字
	var money = $("#withdrawMoney").val();
	//拼接字符串
	money = "￥"+money+"元";
	//放入span标签中
	$("#getMoney").html(money);
}



