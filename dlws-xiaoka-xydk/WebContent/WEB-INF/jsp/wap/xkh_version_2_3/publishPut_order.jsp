<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta
	content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1"
	id="viewport" name="viewport" />
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link rel="stylesheet"
	href="${path}/xkh_version_2.3/iconfont/iconfont.css" />
<link rel="stylesheet" href="${path}/xkh_version_2.3/css/mui.min.css" />
<link rel="stylesheet"
	href="${path}/xkh_version_2.3/css/mui.picker.min.css" />
<link rel="stylesheet" href="${path}/xkh_version_2.3/css/common.css" />
<link rel="stylesheet" href="${path}/xkh_version_2.3/css/publish.css" />
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<title>商家发布-精准投放</title>
</head>
<body>
	<div class="detail">
		<h3>批量投放</h3>
	</div>
	<form action="<%=basePath%>publish/addPutOrder.html" id="orderForm"
		method="post">
		<input type="hidden" name="imgUrl" id="imgUrl"
			value="${serverMap.imgUrl }" /> <input type="hidden"
			name="serviceType" id="serviceType" value="${serverMap.introduce }" />
		<input type="hidden" name="introduce" id="introduce"
			value="${serverMap.introduce }" /> <input type="hidden" name="price"
			id="price" value="${serverMap.price }" /> <input type="hidden"
			name="salesVolume" id="salesVolume" value="${serverMap.salesVolume }" />
		<input type="hidden" name="obj" id="obj" value="${obj }" /> <input
			type="hidden" name="totalPrice" id="totalPrice" value="${price }" />
		<input type="hidden" name="serviceName" id="serviceName"
			value="${serverMap.serviceName }" /> <input type="hidden"
			name="picUrl" id="picUrl"> <input type="hidden" name="time"
			id="formTime" /> <input type="hidden" name="activtyUrl"
			id="activtyUrl" value="${objMap.imgString}" />
		<div class="substance_content seller_content">
			<div class="substance_left">
				<img src="${serverMap.imgUrl }" class="search_pic">
			</div>
			<div class="substance_Right">
				<span class="substance_Music">${serverMap.serviceName }</span> <span
					class="substance_Line"> <c:choose>
						<c:when test="${serverMap.serviceType == 1 }">
							<span>线上</span>
						</c:when>
						<c:otherwise>
							<span>线下</span>
						</c:otherwise>
					</c:choose>
				</span> <span class="orderDetail_info">${serverMap.introduce }</span> <span
					class="substance_price">${serverMap.price }</span><span
					class="substance_price">元/次</span> <span class="sell_Num">[已售${serverMap.salesVolume }]</span>
			</div>
		</div>
		<ul class="information_content publishPut_content">
			<li><span class="publishPut_title">投放内容</span> 
			<span class="accurate-title publishPut_biaoti">标&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;题</span>
				<input type="text" placeholder="请输入投放标题" name="title" id="title"
				class="accurate_inp" style="border: 0;width:49vw !important;" maxlength="10" /></li>
			<li>
			     <span class="accurate-title publishPut_biaoti">文&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本</span>
				<!--  <div class="textarea">
					<textarea class="Introduce placeOrder_mes" name="aboutMe"
						id="aboutMe" onkeyup="load()" onkeydown="load()"
						placeholder="请输入文本内容"></textarea>
					<div id="span" class="Span">
						<span>0</span>/500
					</div>
				</div>  -->
				<div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce placeOrder_mes" name="aboutMe" id="aboutMe" onkeyup="load()" onkeydown="load()" placeholder="请输入文本内容"></textarea>
					</div>
					<div id="span" class="Span"><span>0</span>/200</div>
				</div>
				<span class="accurate-title publishPut_pic">图片<span
					class="publish_choose">(选填)</span></span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="picture_box_activity">
						<c:if test="${not empty imgArr}">
							<c:forEach items="${imgArr }" var="img">
								<div class="add_picture">
									<img src="${img}" class="search_pic" /> <span
										onclick="delImgActivtyUrl('${basicInfo.xkh_imgId }','${img}',this)"
										class="iconfont icon-shanchu"></span>
								</div>
							</c:forEach>
						</c:if>
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage();">
						<span class="iconfont icon-pic"></span> <span class="size">+添加图片</span>
					</div>
				</div> <span class="accurate-title publishPut_pic lianjie">链接<span
					class="publish_choose">(选填)</span></span> <input type="text" placeholder="请输入链接地址"
				name="link" id="link"
				class="accurate_inp publishPut_inp lianjie_address" /></li>
			<li><span
				class="accurate-title accurate_title2 publishPut_margin">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
				<input type="text" placeholder="请填写您的姓名" name="name" id="name"
				class="accurate_inp"></li>
			<li><span
				class="accurate-title accurate_title2 publishPut_margin">联系电话</span>
				<input type="number" placeholder="请填写您的联系电话" name="phone" id="phone"
				class="accurate_inp phone">
				<p class="warn_tel"></p></li>
			<li><span
				class="accurate-title accurate_title3 publishPut_margin">买家留言<span
					class="choose_title">(&nbsp;选填&nbsp;)</span></span>
				<!-- <div class="textarea">
					<textarea class="Introduce2" name="note" id="note"
						onkeyup="load2()" onkeydown="load2()" placeholder="请输入文本内容"></textarea>
					<div id="span2" class="Span">
						<span>0</span>/500
					</div>
				</div> -->
				<div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce2" name="note" id="note" onkeyup="load2()" onkeydown="load2()" placeholder="请输入文本内容"></textarea>
					</div>
					<div id="span2" class="Span"><span>0</span>/200</div>
				</div>
				</li>
		</ul>
		<div class="selected_bg"></div>
		<div class="accurate_time">
			<span>投放时间&nbsp;:&nbsp;</span> <span name="time" id="time"
				class="Btn mui-btn mui-btn-block" data-options='{}'>2016-02-10
				10:00</span> <span>投放内容需经平台审核，投放时间必须在48小时以后</span>
		</div>
		<div class="accurate_total">
			<span>合计&nbsp;:&nbsp;</span> <span>${price }元</span>
		</div>
		<div class="substance_Bg"></div>
		<div class="footer">
			<span>提交订单</span>
		</div>
		<!--点击提交订单出来的-->
		<div class="popup_mask"></div>
		<div class="consult_popup">
			<div class="close">
				<img src="${path}/xkh_version_2.3/img/close.png" class="search_pic" />
			</div>
			<div class="popup_content">
				<h3>温馨提示</h3>
				<span class="popup_pic"><img
					src="${path}/xkh_version_2.3/img/style.png" class="search_pic" /></span>
				<span class="told">为避免打扰用户，如需在线咨询，请先下单支付咨询费用。</span> <span
					class="sold">￥&nbsp;${price }元</span> <span class="sold1">(支付后即可与卖家直接沟通哦~)</span>
				<span class="go"><span class="immediately">立即<br />支付
				</span></span>
			</div>
		</div>
	</form>
</body>
<script type="text/javascript"
	src="${path}/xkh_version_2.3/js/jquery-2.1.0.js"></script>
<script type="text/javascript"
	src="${path}/xkh_version_2.3/js/mui.min.js"></script>
<script type="text/javascript"
	src="${path}/xkh_version_2.3/js/mui.picker.min.js"></script>
<script type="text/javascript"
	src="${path}/xkh_version_2.3/js/publish/publishPut_order.js"></script>
<script type="text/javascript">
		var pid = "";
		$(".footer").click(function() {
			//var imgUrl = $("#imgUrl").val();//隐藏数据 投放图片url
			//var serviceType = $("#serviceType").val();//隐藏数据 服务类型线上&线下
			//var serviceName = $("#serviceName").val();//隐藏数据 服务名字
			//var introduce = $("#introduce").val();//隐藏数据 简介
			//var price = $("#price").val();//隐藏数据 单价
			//var salesVolume = $("#salesVolume").val();//销量
			//var obj = $("#obj").val();//隐藏数据
			//var totalPrice = $("#totalPrice").val();//隐藏数据  总价
			var title = $("#title").val().trim();//标题
			var aboutMe = $("#aboutMe").val().trim();//文本
			//var link = $("#link").val().trim();//链接选填
			var name = $("#name").val().trim();//联系人
			var phone = $("#phone").val();//联系电话
			//var note = $("#note").val().trim();//买家留言 选填
			var time = $("#time").text();//投放时间
			$("#formTime").val(time);
			if(title == null || title == ""){
				alert("请输入标题!");
				return;
			}
			if(aboutMe == null || aboutMe == ""){
				alert("请输入文本!");
				return ;
			}
			if(name == null || name == ""){
				alert("请输入联系人!");
				return;
			}
			if(phone == null || phone == ""){
				alert("请输入电话!");
				return ;
			}
// 			$("#orderForm").submit();
			uploadActivityImageSelf();
			/* $.ajax({
				url : 'addPutOrder.html',
				data : {
					"imgUrl" : imgUrl,
					"serviceType" : serviceType,
					"serviceName" : serviceName,
					"introduce" : introduce,
					"price" : price,
					"salesVolume" : salesVolume,
					"obj" : obj,
					"totalPrice" : totalPrice,
					"title" : title,
					"aboutMe" : aboutMe,
					"link" : link,
					"name" : name,
					"phone" : phone,
					"note" : note,
					"time" : time
				},
				type : 'post',
				cache : false,
				dataType : 'json',
				async:false, 
				success : function(data) {
					pid = data.pid;
					if(pid != null && pid != ""){
						$(".popup_mask").css({
							"height": $("body").height(),
							"display": "block"
						});
						$(".consult_popup").show();
					}
				},
				error : function() {
					alert("异常！");
				}
			}); */
		});
		
		//支付
		$(".immediately").click(function(){
			//var price = $("#price").val();
			window.location.href="${path}/publish/seachPubDetail.html?pid="+pid;
		});
		
		
		
		//微信上传 全局变量
		var wxSelf;
		var localArr = new Array();
		var localHeadArr = new Array();//头像
		var localDailyArr = new Array();//日常推文的组合  
		var localActivityArr = new Array();//存放活动的图片
		var serverArr = new Array();

		var serverHeadArr = new Array();//头像
		var serverDailyArr = new Array();//日常推文的组合  
		var serverActivityArr = new Array();//存放活动的图片

		/*001 选择图片   日常推文选择图片*/
		function uploadImage() {
			var clientUrl = window.location.href;
			var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : reqPath,
				dataType : 'json',
				data : {
					"clientUrl" : clientUrl
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					/* alert(return_date ); */
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : false, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
					});
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						wxSelf=wx;
						wx.chooseImage({
							count: 9, // 默认9
							sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
							sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
							success: function (res) {
									localActivityArr=appandArray(localActivityArr,res.localIds);
									if(localActivityArr.length > 0){
										var result = "";
								        for(var j = 0; j < res.localIds.length; j++){
								        	result += "<div id='xkh_activity_"+j+"' class='add_picture'>"+
													"<img src='"+res.localIds[j]+"' class='search_pic' />"+
														"<span onclick='removeImgActiv("+j+","+res.localIds[j]+")' class='iconfont icon-shanchu' ></span>"+
														"<span class='cover'></span>"+
													"</div>";
								        }
								        var imgNum = $("#picture_box_activity img").length;
								        var picNum =  parseInt(imgNum) + parseInt(localDailyArr.length);
								        if(picNum>9){
								        	alert("请上传1-9张图片！");
								        }else{
								        	$("#picture_box_activity").append(result);
								        } 
									}
							}
						 });

						});
						 wx.error(function(res) {
							alert(res.errMsg);
						});
					}
			});
		}		


		/*活动图片 点击提交后上传到微信*/
		/*使用递归的方式进行上传图片*/
		function uploadActivityImageSelf() {
			
			//alert("----------localActivityArr.length：----------"+localActivityArr.length)
		    if (localActivityArr.length == 0) {
		    	var serverStr="";
		    	if(0 == serverActivityArr.length ){
		    		//alert("---------------调用提交方法一---------------------");
		    		$("#orderForm").submit();
		    	}else{
		            for(var j=0;j<serverActivityArr.length;j++){
		            	serverStr+=serverActivityArr[j]+";" ;
		            }
		            $.ajax({
		        		type : "post",
		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
		        		dataType : "json",
		        		data : {serverId:serverStr},
		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
		        			
		        			//alert("+++++++++data++++++:"+data);	
		        			var imageList = data;
		        			//var pathList = data.
		        			
		        			var imgPath="";
		        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
							for(var f=0;f<imageList.length;f++){
						//		alert("+++++++++++++++:"+imageList[f]);
								var imgPathTemp = imageList[f];
								imgPath += imgPathTemp+",";
							}
							
							var u = $("#activtyUrl").val()+imgPath;
							$("#activtyUrl").val(u)
							
							var imgNum = $("#picture_box_activity img").length;
							//alert("imgNum:"+imgNum);
							if(imgNum<1||imgNum>9){
								alert("图片个数为1-9");
								$(".webservice_mask").addClass("mask_none");
								return;
							}
							
							//alert("---------------调用提交方法二---------------------");
		        			$("#orderForm").submit();
							$(".webservice_mask").removeClass("mask_none");
		        		}
		        	});	
		    	}
		    }
		    var localId = localActivityArr[0];
		    //tmd 一定要加     解决IOS无法上传的坑 
		    if (localId.indexOf("wxlocalresource") != -1) {
		        localId = localId.replace("wxlocalresource", "wxLocalResource");
		    }
		    wxSelf.uploadImage({
		        localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
		        isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
		        success: function (res) {
		           // serverIds.push(res.serverId); // 返回图片的服务器端ID
		           var serverId = res.serverId; // 返回图片的服务器端ID
		           serverActivityArr.push(serverId);
		            localActivityArr.shift();
		            uploadActivityImageSelf();
		            serverStr+=serverId+";" ;
		        },
		        fail: function (res) {
		            alert("上传失败，请重新上传！");
		        }
		    });
		}

		function appandArray(a,b){

			for(var i=0;i<b.length;i++){
				 a.push(b[i]);
			}
			return a;
		}



		function removeImgActiv(imageId,val){
			$("#xkh_activity_"+imageId).remove();
			localActivityArr.remove(val);
		}

		function removeImgDairy(imageId,val){
			$("#xkh_daily_"+imageId).remove();
			localDailyArr.remove(val);
		}

		//点击删除推文图片
		function delImgDairyTUrl(imgId,val,obj){
			if(confirm("确定删除?")){
					//表示修改需要数据库删除
					$.ajax({
						type : "post",
						url : "<%=basePath%>highSchoolSkill/delImage.html",
						dataType : "json",
						data : {imgeId:imgId,imageUrl:val},
						success : function(data) {
							if(data.flag){
								$("#dairyTweetUrl").val(data.typeValue);
								obj.parentElement.remove();
							}
						}
					});
		  }
		}

		//点击删除活动图片
		function delImgActivtyUrl(imgId,val,obj){
			if(confirm("确定删除?")){
					//表示修改需要数据库删除
					$.ajax({
						type : "post",
						url : "<%=basePath%>highSchoolSkill/delImage.html",
				dataType : "json",
				data : {
					imgeId : imgId,
					imageUrl : val
				},
				success : function(data) {
					if (data.flag) {
						$("#activtyUrl").val(data.typeValue);
						obj.parentElement.remove();
					}
				}
			});
		}
	}
</script>
</html>
