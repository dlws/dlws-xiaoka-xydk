<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>我的发票</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/invoice.css" />
		<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	</head>
	<%@ include file="/WEB-INF/jsp/wap/xkh_version_2/allshare.jsp"%>

	<body>
		<header class="openInvoice">
			<span class="open open_border"><span class="open_green">未开发票</span></span>
			<span class="open"><span>已开发票</span></span>
		</header>
		<section>
			<!--未开发票-->
			<ul class="openInvoice_box" >
				<c:forEach items="${unInvoicedList }" var="invoice">
					<li>
						<div class="openInvoice_left openInvoice_border">
							<input type="hidden" name="orderId" value="${invoice.orderId }" />
						</div>
						<div class="openInvoice_right">
							<div class="publishConsult_title">
								<span class="title_picture"><img src="${invoice.sellerHeadPortrait }" class="search_pic"></span>
								<h3>${invoice.name }</h3>
							</div>
							<div class="openInvoice_photo"><img src="${invoice.imgUrl }" class="search_pic"></div>
							<div class="openInvoice_Right">
								<div class="music_content">
									<span class="openInvoice_music">${invoice.skillName }</span>
									<span class="substance_Line publishConsult_line">
										<c:choose>
											<c:when test="${invoice.serviceType == 1 }">
												<span>线上</span>
											</c:when>
											<c:otherwise>
												<span>线下</span>
											</c:otherwise>
										</c:choose>
									</span>
								</div>
								<p class="invoice_message">${invoice.skillDepict }</p>
								<span class="substance_price publishConsult_price">
									<span class="publishconsult_price">${invoice.skillPrice }</span>/${invoice.dic_name }</span>
							</div>
							<div class="openInvoice_time">
								<c:choose>
									<c:when test="${invoice.enterType == 'jzzx' }">
										
									</c:when>
									<c:when test="${invoice.enterType == 'jztf' }">
										<span>投放时间:</span>
										<span class="time"><fmt:formatDate value="${invoice.endDate }"
								type="both" pattern="yyyy-MM-dd HH:mm" /></span>
									</c:when>
									<c:otherwise>
										<span>服务时间:</span>
										<span class="time"><fmt:formatDate value="${invoice.startDate }"
								type="both" pattern="yyyy-MM-dd HH:mm" />&nbsp;至&nbsp;<fmt:formatDate value="${invoice.endDate }"
								type="both" pattern="yyyy-MM-dd HH:mm" /></span>
									</c:otherwise>
								</c:choose>
							</div>
							<div class="openInvoice_time">
								<span>购买数量:</span>
								<span>${invoice.tranNumber }</span>
								<div class="invoice_total">
									<div class="total_pic"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/diamond.png" class="search_pic"></div>
									<span>合计&nbsp;:&nbsp;<strong class="total_yuan">${invoice.payMoney }</strong>元</span>
								</div>
							</div>
						</div>
					</li>
				</c:forEach>
			</ul>
		    <!--已开发票-->
		    <ul class="alredyInvoice_box mask_none">
		    	<c:forEach items="${invoicedList }" var="invoice">
		    		<li>
			    		<span class="showTime">${invoice.key }</span>
			    		<c:forEach items="${invoice.value }" var="inv">
			    				<div class="openInvoice_right alredyInvoice_right">
									<div class="publishConsult_title">
										<span class="title_picture"><img src="${inv.sellerHeadPortrait }" class="search_pic"></span>
										<h3>${inv.name }</h3>
									</div>
									<div class="openInvoice_photo"><img src="${inv.imgUrl }" class="search_pic"></div>
									<div class="openInvoice_Right alredyInvoice_Right">
										<div class="music_content">
											<span class="openInvoice_music">${inv.skillName }</span>
											<span class="substance_Line publishConsult_line">
												<c:choose>
													<c:when test="${inv.serviceType == 1 }">
														<span>线上</span>
													</c:when>
													<c:otherwise>
														<span>线下</span>
													</c:otherwise>
												</c:choose>
											</span>
										</div>
										<p class="invoice_message invoice_meg">${inv.skillDepict }</p>
										<span class="substance_price publishConsult_price">
											<span class="publishconsult_price">${inv.skillPrice }</span>元/${inv.dic_name }
										</span>
			
									</div>
									
										<c:choose>
											<c:when test="${inv.enterType == 'jzzx' }">
												
											</c:when>
											<c:when test="${inv.enterType == 'jztf' }">
											<div class="openInvoice_time">
												<span>投放时间:</span>
												<span class="time"><fmt:formatDate value="${inv.endDate }"
										type="both" pattern="yyyy-MM-dd HH:mm:ss" /></span>
										</div>
											</c:when>
											<c:otherwise>
											<div class="openInvoice_time">
												<span>服务时间:</span>
												<span class="time"><fmt:formatDate value="${inv.startDate }"
										type="both" pattern="yyyy-MM-dd HH:mm:ss" />&nbsp;至&nbsp;<fmt:formatDate value="${invoice.endDate }"
										type="both" pattern="yyyy-MM-dd HH:mm:ss" /></span>
										</div>
											</c:otherwise>
										</c:choose>
									
									<div class="openInvoice_time">
										<span>购买数量:</span>
										<span>${inv.tranNumber }</span>
										<div class="invoice_total Invoice_total">
											<div class="total_pic"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/diamond.png" class="search_pic"></div>
											<span>合计&nbsp;:&nbsp;<strong class="total_yuan">${inv.payMoney }</strong>元</span>
										</div>
									</div>
								</div>
			    		</c:forEach>
			    	</li>
		    	</c:forEach>
		    </ul>
		</section>
		<!-- 當前沒有發票時候出現 -->
			<div class="bank_content" style="display: none">
			     <img src="${path}/xkh_version_2.2/img/bank_invoice.png" class="bank_photo" />
			   <span class="bank_message">暂无相关发票</span>
			</div>
		<footer class="openInvoice_foot">
			<div class="foot_left">
				<span class="foot_border openInvoice_border"></span>
			    <span>全选</span>
			</div>
			<div class="foot_right">
				<div class="price_total">
					<span>总计&nbsp;:&nbsp;</span>
				    <span class="summation">0元</span>
				</div>
				<div class="foot_openInvoice" id="doinvoice">
					<span>开具发票</span>
					<span>已选择<strong class="number">0</strong>个订单</span>
				</div>
			</div>
		</footer>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/invoice/openInvoice.js" ></script>
    <script type="text/javascript">
    	$("#doinvoice").click(function(){
    		var data = $('.openInvoice_check');
			var obj = [];
			if(data.length > 0){
				for(var i =0;i<data.length;i++){
				    var child = data[i].children;
				    var json = {};
				    for(var j = 0;j<child.length;j++){
					   var name = child[j].name;
					   var val = child[j].value;
					   json[name] = val;
				    }
				    obj.push(json);
			    }
			    obj = JSON.stringify(obj);
			    var price = $('.summation').text();
				price = price.substring(1,price.length-1);
				var f = document.createElement("form");
				document.body.appendChild(f);
				var i = document.createElement("input");
				i.type = "hidden";
				f.appendChild(i);
				i.value = price;
				i.name = "price";
				var j = document.createElement("input");
				j.type = "hidden";
				f.appendChild(j);
				j.value = obj;
				j.name = "obj";
				f.method="post";
				f.action = "${path}/invoice/openInvoice.html";
				f.submit();
			}else{
				alert("请选择订单!");
			}
    	});
    </script>
</html>