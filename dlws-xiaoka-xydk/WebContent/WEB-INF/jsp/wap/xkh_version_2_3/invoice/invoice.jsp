<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>开具发票</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/invoice.css" />
		<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	</head>
	<%@ include file="/WEB-INF/jsp/wap/xkh_version_2/allshare.jsp"%>
	<body>
		<ul class="invoiceStyle_box">
			<input name="totalPrice" id="totalPrice" type="hidden" value="${totalPrice }" />
			<input name="obj" id="obj" type="hidden" value="${obj }" />
			<li>
				<span class="invoice">发票类型</span>
				<div class="invoicePic_box">
					<c:forEach items="${typeList }" var="type" varStatus="i">
						<c:choose>
							<c:when test="${i.index == 0 }">
								<div class="invoice_pic">
									<span class="normal_green"></span>
									<span class="groom_Green">
										${type.dic_name }
										<input type="hidden" name="id" value="${type.id }">
										<input type="hidden" name="value" value="${type.dic_value }">
									</span>
								</div>
							</c:when>
							<c:otherwise>
								<div class="invoice_pic invoice_photo">
									<span class="special"></span>
									<span>
										${type.dic_name }
										<input type="hidden" name="id" value="${type.id }">
										<input type="hidden" name="value" value="${type.dic_value }">
									</span>
								</div>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</div>
			</li>
		</ul>
		<div class="enterInfo_bg"></div>
		<!--增值税普通发票-->
		<section class="normal_content">
			<ul class="personal_content publishPut_content">
				<li>
					<span class="invoice_name">开票主体</span>
					<div class="nature_box nature_Box">
						<div class="nature">
							<span class="nature_choose"></span>
							<span class="groom_Green">个人</span>
						</div>
						<div class="nature">
							<span></span>
							<span>单位</span>
						</div>
					</div>
					<div class="invoice_rise">
						<span class="invoice_name">发票抬头</span>
						<input class="invoice_inp" type="text" id="ptfpSign" 
							onkeyup="this.value=this.value.replace(/\s+/g,'')" placeholder="请填写发票抬头" />
					</div>
				</li>
				<li>
					<span class="invoice_name">发票内容</span>
					<span class="point">服务费(税点&nbsp;:&nbsp;${point })</span>
					<input type="hidden" id="ptfpPoint" value="${pointMap.id }" />
					<input type="hidden" id="ptfpMoney" value="${ptfpMoney }" />
					<div class="invoice_rise">
						<span class="invoice_name">快递费用</span>
						<div class="nature_box express_box express_Box">
							<c:forEach items="${logisticsList }" var="logistics" varStatus="i">
								<c:choose>
									<c:when test="${i.index == 0 }">
										<div class="nature">
											<span class="nature_choose"></span>
											<span class="groom_Green">
												${logistics.dic_name }${logistics.dic_value }元
												<input name="dic_name" type="hidden" value="${logistics.dic_name }" />
												<input name="dic_value" type="hidden" value="${logistics.dic_value }" />
											</span>
										</div>
									</c:when>
									<c:otherwise>
										<div class="nature">
											<span></span>
											<span>
												${logistics.dic_name }${logistics.dic_value }元
												<input name="dic_name" type="hidden" value="${logistics.dic_name }" />
												<input name="dic_value" type="hidden" value="${logistics.dic_value }" />
											</span>
										</div>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
					</div>
				</li>
			</ul>
			<div class="enterInfo_bg"></div>
			<ul class="personal_content publishPut_content">
				<li>
					<span class="invoice_name invoice_person">收件人信息</span>
					<div class="invoice_rise">
						<span class="invoice_name">姓名</span>
						<input class="invoice_inp" type="text" id="pufpName" onkeyup="this.value=this.value.replace(/\s+/g,'')" placeholder="请填写收件人姓名" />
					</div>
				</li>
				<li>
					<span class="invoice_name tell_card">手机号码</span>
					<input class="invoice_inp tell_inp phone" id="ptfpPhone" type="text" placeholder="请填写收件人手机号码" />
					<p class="warn_tel"></p>
				</li>
				<li>
					<span class="invoice_name invoice_person invoice_address">收货地址</span>
					<div class="textarea">
						<div class="textarea_info">
							<textarea class="introduce" id="ptfpAddress" onkeyup="load()" placeholder="请填写详细的收货地址"></textarea>
						</div>
						<div id="span" class="span"><span>0</span>/200</div>
					</div>
				</li>
			</ul>
			<div class="enterInfo_bg"></div>
			<div class="Invoice_total">
				<span>合计&nbsp;:&nbsp;</span>
				<span><strong id="ordinary">${allMoney }</strong>元</span>
				<input type="hidden" id="allMoney" value="${allMoney }" />
				<p>(包含发票税点费用和快递费用)</p>
			</div>
		</section>
        <section class="special_content mask_none">
        	<ul class="personal_content publishPut_content">
        		<li>
					<span class="invoice_name tell_card">公司名称</span>
					<input class="invoice_inp tell_inp" name="dataName" id="dataName" 
						onkeyup="this.value=this.value.replace(/\s+/g,'')" type="text" placeholder="请填写公司全称" />
				</li>
				<li>
					<span class="invoice_name tell_card taxpayer">纳税人识别号</span>
					<input class="invoice_inp tell_inp" id="nsrsbh" onkeyup="this.value=this.value.replace(/\s+/g,'')"
						type="text" placeholder="请填写纳税人识别号" />
				</li>
				<li>
					<span class="invoice_name invoice_person invoice_address">收货地址</span>
					<div class="textarea">
						<div class="textarea_info invoice_text">
							<textarea class="introduce2" name="firmAdress" id="firmAdress" onkeyup="load2()" 
								placeholder="请填写详细的收货地址"></textarea>
						</div>
						<div id="span2" class="span"><span>0</span>/200</div>
					</div>
					<span class="invoice_name tell_card">公司电话</span>
					<input class="invoice_inp tell_inp telphone" name="firmPhone" id="firmPhone" type="text" 
						onkeyup="this.value=this.value.replace(/\s+/g,'')" placeholder="请填写公司电话号码" />
					<p class="warn_phone"></p>
				</li>
				<li>
					<span class="invoice_name tell_card">公司账号</span>
					<input class="invoice_inp tell_inp" name="firmNumber" id="firmNumber" type="text" 
						onkeyup="this.value=this.value.replace(/\s+/g,'')" placeholder="请填写公司账号" />
				</li>
				<li>
					<span class="invoice_name tell_card bank_card">开户行全称</span>
					<input class="invoice_inp tell_inp" name="bankName" id="bankName" type="text" 
						onkeyup="this.value=this.value.replace(/\s+/g,'')" placeholder="请填写开户行全称" />
				</li>
				<li>
					<span class="invoice_name">发票内容</span>
					<span class="point">服务费(税点&nbsp;:&nbsp;${specd })</span>
					<input type="hidden" id="zyfpPoint" value="${specPointMap.id }" />
					<input type="hidden" id="zyfpMoney" value="${zyfpMoney }" />
					<div class="invoice_rise">
						<span class="invoice_name">快递费用</span>
						<div class="nature_box express_box express_Box2">
							<c:forEach items="${logisticsList }" var="logistics" varStatus="i">
								<c:choose>
									<c:when test="${i.index == 0 }">
										<div class="nature">
											<span class="nature_choose"></span>
											<span class="groom_Green">${logistics.dic_name }${logistics.dic_value }元
												<input name="dic_name" type="hidden" value="${logistics.dic_name }" />
												<input name="dic_value" type="hidden" value="${logistics.dic_value }" />
											</span>
										</div>
									</c:when>
									<c:otherwise>
										<div class="nature">
											<span></span>
											<span>${logistics.dic_name }${logistics.dic_value }元
												<input name="dic_name" type="hidden" value="${logistics.dic_name }" />
												<input name="dic_value" type="hidden" value="${logistics.dic_value }" />
											</span>
										</div>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
					</div>
				</li>
        	</ul>
        	<div class="enterInfo_bg"></div>
        	<ul class="personal_content publishPut_content">
				<li>
					<span class="invoice_name invoice_person">收件人信息</span>
					<div class="invoice_rise">
						<span class="invoice_name">姓名</span>
						<input class="invoice_inp" name="recipientName" id="zyfpName" type="text" 
							onkeyup="this.value=this.value.replace(/\s+/g,'')" placeholder="请填写收件人姓名" />
					</div>
				</li>
				<li>
					<span class="invoice_name tell_card">手机号码</span>
					<input class="invoice_inp tell_inp special_phone" id="zyfpPhone" type="text" 
						onkeyup="this.value=this.value.replace(/\s+/g,'')" placeholder="请填写收件人手机号码" />
					<p class="warn_special"></p>
				</li>
				<li>
					<span class="invoice_name invoice_person invoice_address">收货地址</span>
					<div class="textarea">
						<div class="textarea_info">
							<textarea class="introduce3" id="zyfpAdress" onkeyup="load3()" placeholder="请填写详细的收货地址"></textarea>
						</div>
						<div id="span3" class="span"><span>0</span>/200</div>
					</div>
				</li>
			</ul>
			<div class="enterInfo_bg"></div>
			<div class="Invoice_total">
				<span>合计&nbsp;:&nbsp;</span>
				<span><strong id="specal">${specMoney }</strong>元</span>
				<input type="hidden" id="specMoney" value="${specMoney }" />
				<p>(包含发票税点费用和快递费用)</p>
			</div>
        </section>
		<footer class="write_foot"><span>提交</span></footer>
		<!--点击提交进入支付页面-->
		<div class="popup_mask"></div>
		<div class="consult_popup">
			<div class="close"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/close.png" class="search_pic" /></div>
			<div class="popup_content">
				<h3>支付提示</h3>
				<span class="popup_pic"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/invoice_bar.png" class="search_pic" /></span>
				<span class="told">支付开具发票所需费用</span>
				<span class="sold">合计:<span id="pMoney"></span>元</span>
				<input type="hidden" id="invoId" />
				<a href="#" class="go">确定</a>
			</div>
		</div>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/invoice/invoice.js" ></script>
    <script type="text/javascript">
    	$(".go").click(function(){
    		var price = $("#pMoney").text();
    		//var price = 0.02;
    		var id = $("#invoId").val();
			//window.location.href="${path}/invoice/updateOrderSelltoPay.html?id="+id+"&payMoney="+price;
    		window.location.href="../invoice/toPay.html?invoiceId="+id+"&&price="+price;
    	});
    	//普通发票
    	$(".express_Box").click(function(){
    		//快递运费
    		var logValue = $(".express_Box").find(".groom_Green").find("input[name='dic_value']").val();
    		var logName = $(".express_Box").find(".groom_Green").find("input[name='dic_name']").val();
    		//合计金额
    		var allMoney = $("#allMoney").val();
    		var countMoney = Number(logValue)+Number(allMoney)-10;
    		if(logName == '普通快递'){
    			$("#ordinary").text(allMoney);
    		}else{
    			$("#ordinary").text(countMoney);
    		}
    	});
    	//专用发票
    	$(".express_Box2").click(function(){
    		//快递运费
    		var logValue = $(".express_Box2").find(".groom_Green").find("input[name='dic_value']").val();
    		var logName = $(".express_Box2").find(".groom_Green").find("input[name='dic_name']").val();
    		//合计金额
    		var specMoney = $("#specMoney").val();
    		var countMoney = Number(logValue)+Number(specMoney)-10;
    		if(logName == "普通快递"){
    			$("#specal").text(specMoney);
    		}else{
    			$("#specal").text(countMoney);
    		}
    	});
	    $(".write_foot").click(function(){
	    	//发票大类型
	    	var invoiceTopType = $(".invoicePic_box").find(".groom_Green").find("input[name='value']").val();//增值税普通发票  增值税专用发票
	    	//发票类型id
	    	var id = $(".invoicePic_box").find(".groom_Green").find("input[name=id]").val();
	    	//单位or个人
	    	var signTypeId = "";
	    	//发票抬头
	    	var sign = "";
	    	//收件人姓名
	    	var recipientName = "";
	    	//收件人手机号
	    	var recipientPhone = "";
	    	//收货地址
	    	var recipientAdress = "";
	    	//发票类型id
	    	var typeId = "";
	    	//税点id
	    	var taxPointId = "";
	    	//税点后价格
	    	var taxPointValue = "";
	    	//快递名称
	    	var expressName = "";
	    	//快递价格
	    	var expressPrice = "";
	    	//公司地址(专用发票用到)
	    	var firmAdress = "";
	    	//公司电话(专用发票用到)
	    	var firmPhone = "";
	    	//公司账号(专用发票用到)
	    	var firmNumber = "";
	    	//开户行全称
	    	var bankName = "";
	    	//支付价格
	    	var payMoney = "";
	    	//发票类型
	    	var invoiceType = "";
	    	//支付状态
	    	var payStatus = "";
	    	var data = {};
	    	var obj = $("#obj").val();
	    	var totalPrice = $("#totalPrice").val();
	    	if(invoiceTopType == "0"){
	    		typeId = id;
	    		sign = $("#ptfpSign").val();
	    		signTypeId = $(".nature_Box").find(".groom_Green").text();
	    		if(signTypeId == "个人"){
	    			signTypeId = "0";
	    		}else{
	    			signTypeId = "1";
	    		}
	    		taxPointId = $("#ptfpPoint").val();
	    		taxPointValue = $("#ptfpMoney").val();
	    		expressName = $(".express_Box").find(".groom_Green").find("input[name='dic_name']").val();
	    		expressPrice = $(".express_Box").find(".groom_Green").find("input[name='dic_value']").val();
	    		payMoney = $("#allMoney").val();
	    		recipientName = $("#pufpName").val();
	    		recipientPhone = $("#ptfpPhone").val();
	    		recipientAdress = $("#ptfpAddress").val();
	    		invoiceType = 0;
	    		payStatus = 0;
	    		if(sign == ""){
	    			alert("请输入发票抬头!");
	    			return
	    		}
	    		if(recipientName == ""){
	    			alert("请输入收件人姓名!");
	    			return
	    		}
	    		if(recipientPhone == ""){
	    			alert("请输入收件人电话!");
	    			return
	    		}
	    		var warn_tel = $(".warn_tel").text();
	    		if(warn_tel != ""){
	    			alert("请输入正确格式的电话!");
	    			return
	    		}
	    		if(recipientAdress == ""){
	    			alert("请输入收件人地址!");
	    			return
	    		}
	    		data = {
	    			"typeId" : typeId,
	    			"sign" : sign,
	    			"signTypeId" : signTypeId,
	    			"taxPointId" : taxPointId,
	    			"taxPointValue" : taxPointValue,
	    			"expressName" : expressName,
	    			"expressPrice" : expressPrice,
	    			"payMoney" : payMoney,
	    			"recipientName" : recipientName,
	    			"recipientPhone" : recipientPhone,
	    			"recipientAdress" : recipientAdress,
	    			"obj" : obj,
	    			"totalPrice" : totalPrice,
	    			"invoiceType" : invoiceType,
	    			"payStatus" : payStatus,
	    			"invoiceTopType" : invoiceTopType
	    		}
	    	}else{
	    		typeId = id;
    			var dataName = $("#dataName").val();//公司名称
    			sign = $("#nsrsbh").val();//纳税人识别号
    			expressName = $(".express_Box2").find(".groom_Green").find("input[name='dic_name']").val();
	    		expressPrice = $(".express_Box2").find(".groom_Green").find("input[name='dic_value']").val();
    			firmAdress = $("#firmAdress").val();//公司地址
    			firmPhone = $("#firmPhone").val();//公司电话
    			firmNumber = $("#firmNumber").val();//公司账号
    			bankName = $("#bankName").val();//开户行名称
    			taxPointId = $("#zyfpPoint").val();//税点id
    			taxPointValue = $("#zyfpMoney").val();//税后值
    			recipientName = $("#zyfpName").val();
    			recipientPhone = $("#zyfpPhone").val();
    			recipientAdress = $("#zyfpAdress").val();
    			invoiceType = 1;
	    		payStatus = 0;
	    		payMoney = $("#specMoney").val();
	    		if(dataName == ""){
	    			alert("请输入公司名称!");
	    			return
	    		}
	    		if(sign == ""){
	    			alert("请输入纳税人识别号!");
	    			return
	    		}
	    		if(firmAdress.trim() == ""){
	    			alert("请输入公司地址!");
	    			return
	    		}
	    		if(firmPhone == ""){
	    			alert("请输入公司电话!");
	    			return
	    		}
	    		var warn_phone = $(".warn_phone").text();
	    		if(warn_phone != ""){
	    			alert("请输入正确格式的电话!");
	    			return
	    		}
	    		if(firmNumber == ""){
	    			alert("请输入公司账号!");
	    			return
	    		}
	    		if(bankName == ""){
	    			alert("请输入银行全称!");
	    			return
	    		}
	    		if(recipientName == ""){
	    			alert("请输入收件人姓名!");
	    			return
	    		}
	    		if(recipientPhone == ""){
	    			alert("请输入收件人电话!");
	    			return
	    		}
	    		var warn_special = $(".warn_special").text();
	    		if(warn_special != ""){
	    			alert("请输入正确格式的电话!");
	    			return
	    		}
	    		if(recipientAdress.trim() == ""){
	    			alert("请输入收货地址!");
	    			return
	    		}
	    		data = {
	    			"typeId" : typeId,
	    			"dataName" : dataName,
	    			"sign" : sign,
	    			"expressName" : expressName,
	    			"expressPrice" : expressPrice,
	    			"firmAdress" : firmAdress,
	    			"firmPhone" : firmPhone,
	    			"firmNumber" : firmNumber,
	    			"bankName" : bankName,
	    			"obj" : obj,
	    			"totalPrice" : totalPrice,
	    			"taxPointId" : taxPointId,
	    			"taxPointValue" : taxPointValue,
	    			"recipientName" : recipientName,
	    			"recipientPhone" : recipientPhone,
	    			"recipientAdress" : recipientAdress,
	    			"invoiceType" : invoiceType,
	    			"payStatus" : payStatus,
	    			"payMoney" : payMoney,
	    			"invoiceTopType" : 1
	    		}
    		}
	    	$.ajax({
	    	    type: 'GET',
				url : 'addInvoice.html',
				data : data,
				type : 'post',
				cache : false,
				dataType : 'json',
				success : function(data) {
					var invoId = data.id;
					var price = data.price;
					if(invoId != ""){
						$(".popup_mask").show();
				    	$(".consult_popup").show();
				    	$("body,html").css({"overflow":"hidden"});
				    	$("#pMoney").text(payMoney);
				    	$("#invoId").val(invoId);
				    	//alert("记录的id:"+invoiceId+"********************price:"+price);
				    	//window.location.href="../invoice/toPay.html?invoiceId="+invoId+"&&price="+price;
					}
				},
				error : function() {
					alert("异常！");
				}
			});
	    });
    </script>
</html>