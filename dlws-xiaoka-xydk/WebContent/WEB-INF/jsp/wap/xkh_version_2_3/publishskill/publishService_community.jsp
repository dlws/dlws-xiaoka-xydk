<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>发布服务</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.picker.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/enterInfo.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publishService.css" />
	</head>

	<body>
		<!--头部-->
		<div class="enterInfo_top">
			<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/enterInfo_top.png" class="search_pic" />
		</div>
		
		<form action="<%=basePath%>pubSkill/add.html" id="skillPub" method="post">
		<div class="person_style">
			<span>发布类型</span>
		</div>
		<div class="publishService_pic">
		<c:forEach items="${classList}" var="classInfo" varStatus="status">
			<c:choose>
				<c:when test="${classInfo.classValue=='gxst'}">
					<a href="<%=basePath%>pubSkill/chosePubSkill.html?classValue=${classInfo.classValue}&sid=${classInfo.id}&skillId=${paramsMap.skillId}&basicId=${paramsMap.basicId}" class="choose choose_style">
						<span class="picture_green"><img src="${classInfo.selectimage}" class="search_pic typePic" /></span>
						<span class="color_green">${classInfo.className }</span>
					</a>
					<input type="hidden" name="skillFatId" id="skillFatId" value="${classInfo.id}">
				</c:when>
				<c:otherwise>
					<a href="<%=basePath%>pubSkill/chosePubSkill.html?classValue=${classInfo.classValue}&sid=${classInfo.id}&skillId=${paramsMap.skillId}&basicId=${paramsMap.basicId}" class="choose choose_style">
						<span class="picture_green"><img src="${classInfo.noSelectimage}" class="search_pic typePic" /></span>
						<span>${classInfo.className }</span>
					</a>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		</div>
		<div class="enterInfo_bg"></div>
		<ul class="message_content">
			<li>
				<span class="enter_name">服务名称</span>
				<input class="inp" type="text" name="skillName" id="skillName" class="skillPrice" value="${objMap.skillName }" placeholder="请填写服务名称" />
			</li>
			<li>
				<span class="enter_name">服务类型</span>
				
				<div id="serviceStyle" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">请选择服务类型</span>
					<span class="myInfo_name enter_school show-result servType">
						<c:forEach items="${sonEditList}" var="one">
							<c:if test="${one.VALUE==objMap.skillSonId}">${one.text}</c:if>
						</c:forEach>
					</span>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
					<input type="hidden" name="skillSonId" id="skillSonId" value="${objMap.skillSonId}" />
				</div>

			</li>
			<li>
				<span class="enter_name">服务性质</span>
				<div class="nature_box">
					<div class="nature">
						<span class="nature_choose"></span>
						<span class="groom_Green">线上</span>
						<input type="hidden" name="serviceType" id="serviceType" value="1" class="serviceType">
					</div>
					<div class="nature">
						<span></span>
						<span>线下</span>
						 <input type="hidden" name="serviceType" id="serviceType" value="2" class="serviceType" disabled="disabled">
					</div>
				</div>
					 <input type="hidden"  id="serviceTypeEdit" value="${objMap.serviceType}">
			</li>
			<li>
				<span class="enter_name">服务价格</span>
				<div class="service_price unit_content">
					<input type="number" name="skillPrice" class="skillPrice" value="${objMap.skillPrice}" placeholder="请填写报价" onkeypress="this.value=(this.value.match(/\d+(\.\d{0,2})?/)||[''])[0]" />
					<span>元/</span>
					<div id="unitStyle" class="mui-input-row">
						<span class="choose_unit result-tips">请选择单位</span>
						<span class="choose_unit show-result choseUnit">
							<c:forEach items="${canpanyEdit}" var="one">
									<c:if test="${one.VALUE==skillInfo.companyId}">${one.text}</c:if>
							</c:forEach>
						</span>
					</div>
					<span style="margin-right:0 ;" class="result-tips">单位</span>
				</div>
			</li>
			<li>
				<span class="enter_name publishService_name">图&nbsp;&nbsp;&nbsp;&nbsp;片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="picture_box_activity">
						<c:if test="${not empty imgArr}">
							<c:forEach items="${imgArr }" var="img" >
								 <div class="add_picture">
									<img src="${img}" class="search_pic" />
									<span onclick="delImgActivtyUrl('${objMap.xkh_imgId }','${img}',this)" class="iconfont icon-shanchu"></span>
								 </div>
							</c:forEach>
						</c:if>
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage()">
						<span class="iconfont icon-pic"></span>
						<span class="size">+添加图片</span>
					</div>
				</div>
				<p class="limit">*请上传1-9张图片</p>
			</li>
			<li>
				<span class="enter_name publishService_name">服务介绍</span>
				<div class="textarea">
					<div class="textarea_info">
						<textarea class="introduce" id="skillDepict" name="skillDepict"  onkeyup="load()" placeholder="请简要描述服务优势，过往案例等">${objMap.skillDepict}</textarea>
					</div>
					<div id="span" class="span"><span>0</span>/200</div>
				</div>
			</li>
		</ul>
		<input type="hidden" name="basicId" id="basicId" value="${paramsMap.basicId}"><%-- ${paramsMap.basicId} --%>
		<input type="hidden" value="${paramsMap.skillId}" name="skillId" id="skillId"><!-- 技能Id -->
		<input type="hidden" name="activtyUrl" id="activtyUrl" value="${objMap.imgString}"/>
		<input type="hidden" name="company" id="company" value="${skillInfo.companyId}">
		</form>
		<footer class="write_foot" onclick="sub();"><span>提交</span></footer>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.picker.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.poppicker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/publishService/community.js"></script>
	<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
    <script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
    
	<script type="text/javascript">
	function sub(){
		var skillName = $("#skillName").val();
		if(skillName==""){
			alert("请输入服务名称")
			return;
		}
		var skillSonId = $("#skillSonId").val();
		if(skillSonId==""){
			alert("请选择服务类型")
			return;
		}
		var skillPrice = $(".skillPrice").val();
		if(skillPrice==""||!skillPrice){
			alert("请输入价格");
			return
		}
		var unitText = $.trim($(".choseUnit").html()) 
		if(unitText==""||!unitText){
			alert("请选择单位");
			return
		}
		uploadActivityImageSelf();
// 		$("#skillPub").submit();
	}
	var servType =  $.trim($(".servType").html());
	if(servType!=""&&servType!=null){
		document.querySelector('#serviceStyle .result-tips').style.display = "none";
		document.querySelector('#serviceStyle .show-result').style.display = "block";
	}
	var sonlist = ${sonList};
	//选择服务类型
	var servicestyle = new mui.PopPicker({
		layer: 1
	});
	servicestyle.setData(sonlist);
	var showservicestyleButton = document.getElementById('serviceStyle');
	showservicestyleButton.addEventListener('tap', function(event) {
		servicestyle.show(function(items) {
			document.querySelector('#serviceStyle .result-tips').style.display = "none";
			document.querySelector('#serviceStyle .show-result').style.display = "block";
			document.querySelector('#serviceStyle .show-result').innerText = items[0].text;
			$("#skillSonId").val(items[0].VALUE);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});

	}, false);
	
	
	var company = ${canpanyList};
	//选择单位
	var unitstyle = new mui.PopPicker({
		layer: 1
	});
	
	var companyId = "${skillInfo.companyId}";
	if(companyId!=""&&companyId!=null){
		document.querySelector('#unitStyle .result-tips').style.display = "none";
		document.querySelector('#unitStyle .show-result').style.display = "block";
	}
	unitstyle.setData(company);
	var showunitstyleButton = document.getElementById('unitStyle');
	showunitstyleButton.addEventListener('tap', function(event) {
		unitstyle.show(function(items) {
			document.querySelector('#unitStyle .result-tips').style.display = "none";
			document.querySelector('#unitStyle .show-result').style.display = "block";
			document.querySelector('#unitStyle .show-result').innerText = items[0].text;
			$("#company").val(items[0].VALUE);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});
	}, false);
	
	
	//选择服务性质
	$(".nature_box .nature").click(function(){
		$(".nature").eq($(this).index()).find("span").eq(0).addClass("nature_choose");
		$(".nature").eq($(this).index()).find("span").eq(1).addClass("groom_Green");
		
		$(this).siblings().find("span").eq(0).removeClass("nature_choose");
		$(this).siblings().find("span").eq(1).removeClass("groom_Green");
		
		$(this).siblings().find("input").attr("disabled",true); 
		$(this).find("input").removeAttr("disabled"); 
	})
	
	var serviceTypeEdit = $("#serviceTypeEdit").val();
	  if(serviceTypeEdit==2){
		$(".nature").eq(1).find("span").eq(0).addClass("nature_choose");
		$(".nature").eq(1).find("span").eq(1).addClass("groom_Green");
		
		$(".nature").eq(0).find("span").eq(0).removeClass("nature_choose");
		$(".nature").eq(0).find("span").eq(1).removeClass("groom_Green");
	} 
	  
	  
	  
	//微信上传 全局变量
	  var wxSelf;
	  var localArr = new Array();
	  var localHeadArr = new Array();//头像
	  var localDailyArr = new Array();//日常推文的组合  
	  var localActivityArr = new Array();//存放活动的图片
	  var serverArr = new Array();

	  var serverHeadArr = new Array();//头像
	  var serverDailyArr = new Array();//日常推文的组合  
	  var serverActivityArr = new Array();//存放活动的图片

	  /*001 选择图片   日常推文选择图片*/
	  function uploadImage() {
	  	var clientUrl = window.location.href;
	  	var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
	  	//请求后台，获取jssdk支付所需的参数
	  	$.ajax({
	  		type : 'post',
	  		url : reqPath,
	  		dataType : 'json',
	  		data : {
	  			"clientUrl" : clientUrl
	  			//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
	  		},
	  		cache : false,
	  		error : function() {
	  			alert("系统错误，请稍后重试");
	  			return false;
	  		},
	  		success : function(data) {
	  			//微信支付功能只有微信客户端版本大于等于5.0的才能调用
	  			var return_date = eval(data);
	  			/* alert(return_date ); */
	  			if (parseInt(data[0].agent) < 5) {
	  				alert("您的微信版本低于5.0无法使用微信支付");
	  				return;
	  			}
	  			//JSSDK支付所需的配置参数，首先会检查signature是否合法。
	  			wx.config({
	  				debug : false, //开启debug模式，测试的时候会有alert提示
	  				appId : return_date[0].appId, //公众平台中-开发者中心-appid
	  				timestamp : return_date[0].config_timestamp, //时间戳
	  				nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
	  				signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
	  				jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
	  			});
	  			//上方的config检测通过后，会执行ready方法
	  			wx.ready(function() {
	  				wxSelf=wx;
	  				wx.chooseImage({
	  					count: 9, // 默认9
	  					sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
	  					sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
	  					success: function (res) {
	  							localActivityArr=appandArray(localActivityArr,res.localIds);
	  							if(localActivityArr.length > 0){
	  								var result = "";
	  						        for(var j = 0; j < res.localIds.length; j++){
	  						        	result += "<div id='xkh_activity_"+j+"' class='add_picture'>"+
	  											"<img src='"+res.localIds[j]+"' class='search_pic' />"+
	  												"<span onclick='removeImg("+j+",this)' class='iconfont icon-shanchu' ></span>"+
	  												"<span class='cover'></span>"+
	  											"</div>";
	  						        }
	  						        var imgNum = $("#picture_box_activity img").length;
	  						        var picNum =  parseInt(imgNum) + parseInt(localDailyArr.length);
	  						        if(picNum>9){
	  						        	alert("请上传1-9张图片！");
	  						        }else{
	  						        	$("#picture_box_activity").append(result);
	  						        } 
	  							}
	  					}
	  				 });

	  				});
	  				 wx.error(function(res) {
	  					alert(res.errMsg);
	  				});
	  			}
	  	});
	  }		


	  /*活动图片 点击提交后上传到微信*/
	  /*使用递归的方式进行上传图片*/
	  function uploadActivityImageSelf() {
	      if (localActivityArr.length == 0) {
	      	var serverStr="";
	      	if(0 == serverActivityArr.length ){
	      		$("#skillPub").submit();
	      	}else{
	              for(var j=0;j<serverActivityArr.length;j++){
	              	serverStr+=serverActivityArr[j]+";" ;
	              }
	              $.ajax({
	          		type : "post",
	          		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
	          		dataType : "json",
	          		data : {serverId:serverStr},
	          		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
	          			
	          			//alert("+++++++++data++++++:"+data);	
	          			var imageList = data;
	          			//var pathList = data.
	          			 
	          			var imgPath="";
	          		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
	  					for(var f=0;f<imageList.length;f++){
	  				//		alert("+++++++++++++++:"+imageList[f]);
	  						var imgPathTemp = imageList[f];
	  						imgPath += imgPathTemp+",";
	  					}
	  					
	  					var u = $("#activtyUrl").val()+imgPath;
	  					$("#activtyUrl").val(u)
	  					
	  					var imgNum = $("#picture_box_activity img").length;
	  					//alert("imgNum:"+imgNum);
	  					if(imgNum<1||imgNum>9){
	  						alert("图片个数为1-9");
	  						$(".webservice_mask").addClass("mask_none");
	  						return;
	  					}
	  					
	          			$("#skillPub").submit();
	  					$(".webservice_mask").removeClass("mask_none");
	          		}
	          	});	
	      	}
	      }
	      var localId = localActivityArr[0];
	      //tmd 一定要加     解决IOS无法上传的坑 
	      if (localId.indexOf("wxlocalresource") != -1) {
	          localId = localId.replace("wxlocalresource", "wxLocalResource");
	      }
	      wxSelf.uploadImage({
	          localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
	          isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
	          success: function (res) {
	             // serverIds.push(res.serverId); // 返回图片的服务器端ID
	             var serverId = res.serverId; // 返回图片的服务器端ID
	             serverActivityArr.push(serverId);
	              localActivityArr.shift();
	              uploadActivityImageSelf();
	              serverStr+=serverId+";" ;
	          },
	          fail: function (res) {
	              alert("上传失败，请重新上传！");
	          }
	      });
	  }

	  function appandArray(a,b){

	  	for(var i=0;i<b.length;i++){
	  		 a.push(b[i]);
	  	}
	  	return a;
	  }


	  function removeImg(imageId,obj){
			obj.parentElement.remove();
			localActivityArr.splice(imageId,1)
	 }

	  //点击删除活动图片
	  function delImgActivtyUrl(imgId,val,obj){
	  	if(confirm("确定删除?")){
	  			//表示修改需要数据库删除
	  			$.ajax({
	  				type : "post",
	  				url : "<%=basePath%>pubSkill/delImage.html",
	  				dataType : "json",
	  				data : {imgeId:imgId,imageUrl:val},
	  				success : function(data) {
	  					if(data.flag){
	  						$("#activtyUrl").val(data.typeValue);
	  						obj.parentElement.remove();
	  					}
	  				}
	  			});
	    }
	  }
	  
	</script>
</html>
