<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>发布服务</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.picker.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/enterInfo.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publishService.css" />
	</head>

	<body>
		<!--头部-->
		<div class="enterInfo_top">
			<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/enterInfo_top.png" class="search_pic" />
		</div>
		<div class="person_style">
			<span>发布类型</span>
		</div>
		<form action="<%=basePath%>pubSkill/add.html" id="skillPub" method="post">
		<div class="publishService_pic">
			<c:forEach items="${classList}" var="classInfo" varStatus="status">
			<c:choose>
				<c:when test="${classInfo.classValue=='sqdv'}">
					<a href="<%=basePath%>pubSkill/chosePubSkill.html?classValue=${classInfo.classValue}&sid=${classInfo.id}&skillId=${paramsMap.skillId}&basicId=${paramsMap.basicId}" class="choose choose_style">
						<span class="picture_green"><img src="${classInfo.selectimage}" class="search_pic typePic" /></span>
						<span class="color_green">${classInfo.className }</span>
					</a>
					<input type="hidden" name="skillFatId" id="skillFatId" value="${classInfo.id}">
				</c:when>
				<c:otherwise>
					<a href="<%=basePath%>pubSkill/chosePubSkill.html?classValue=${classInfo.classValue}&sid=${classInfo.id}&skillId=${paramsMap.skillId}&basicId=${paramsMap.basicId}" class="choose choose_style">
						<span class="picture_green"><img src="${classInfo.noSelectimage}" class="search_pic typePic" /></span>
						<span >${classInfo.className}</span>
					</a>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		</div>
		<div class="enterInfo_bg"></div>
		<div class="li">
			<span class="enter_name">社群类型</span>
			<div id="associationStyle" class="mui-input-row">
				<span class="myInfo_name enter_school result-tips">请选择社群类型</span>
				<span class="myInfo_name enter_school show-result socialType">
					<c:forEach items="${sonEditList}" var="one">
						<c:if test="${one.VALUE==objMap.skillSonId}">${one.text}</c:if>
						<%-- <input type="text" id="socialType" value="${one.text}"> --%>
					</c:forEach>
				</span>
			    <span class="iconfont icon-youjiantou enter_youjiantou"></span>
			    <input type="hidden" name="skillSonId" id="skillSonId" value="${objMap.skillSonId}" />
			</div>
		</div>
		<section class="association_content">
			<!--社区大v除自媒体以外的部分-->
			<ul class="message_content noZmt">
				<li>
					<span class="enter_name">社群名称</span>
					<input class="inp" type="text" name="skillName" id="sqName" value="${objMap.skillName}" placeholder="请填写社群名称" />
				</li>
				<li>
					<span class="enter_name">社群人数</span>
					<input class="inp" type="text" name="groupNum" value="${skillInfo.groupNum}" id="groupNum" placeholder="请填写社群人数" />
					<p class="warn_groupNum"></p>
				</li>
				<li>
					<span class="enter_name">投放价格</span>
					<input class="inp" type="text" name="skillPrice" value="${objMap.skillPrice}" id="skillPrice" placeholder="请填写投放价格" />
					<p class="warn_touPrice"></p>
				</li>
				<li></li>
			</ul>
			<!--社群大V自媒体部分-->
			<ul class="message_content association_none zmt">
				<!--<li>
					<span class="enter_name community_enterName spacing">社群类型</span>
					<span class="myInfo_name enter_school">请选择社群类型</span>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</li>-->
				<li>
					<span class="community_enterName enter_name">公众号名称</span>
					<input class="inp" type="text" name="skillName" id="zmtName" value="${objMap.skillName}" placeholder="请填写公众号名称">
				</li>
				<li>
					<span class="community_enterName enter_name" style="letter-spacing: 0.853vw;">公众号ID</span>
					<input class="inp wechat" type="text" name="pubNumId"  value="${skillInfo.pubNumId}" placeholder="请填写公众号微信ID">
					<p class="warn_pubNum"></p>
				</li>
				<li>
					<span class="community_enterName enter_name spacing">粉丝人数</span>
					<input class="inp" type="number" name="fansNum" id="fansNum" value="${skillInfo.fansNum}" onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" placeholder="请填写公众号粉丝数">
					<p class="warn_funNum"></p>
				</li>
				<li>
					<span class="community_enterName enter_name">平均阅读量</span>
					<input class="inp" type="number" name="readNum" id="readNum"  value="${skillInfo.readNum}"  onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" placeholder="请填写单篇平均阅读量">
					<p class="warn_readNum"></p>
				</li>
				
				<li>
					<span class="community_enterName enter_name spacing">头条报价</span>
					<input type="number" class="community_inp" name="topLinePrice" id="topLinePrice" value="${skillInfo.topLinePrice}" onkeypress="this.value=(this.value.match(/\d+(\.\d{0,2})?/)||[''])[0]" placeholder="请填写头条软文报价">
					<span class="community_span">元/篇</span>
					<p class="warn_topPrice"></p>
				</li>
				<li>
					<span class="community_enterName enter_name spacing">次条报价</span>
					<input type="number" class="community_inp" name="lessLinePrice" id="lessLinePrice" class="skillPrice" value="${skillInfo.lessLinePrice}" onkeypress="this.value=(this.value.match(/\d+(\.\d{0,2})?/)||[''])[0]" placeholder="请填写次条软文报价">
					<span class="community_span">元/篇</span>
					<p class="warn_lesPrice"></p>
				</li>
				<li></li>
			</ul>
				
				
			<ul class="message_content">
				<li>
					<span class="enter_name publishService_name community_enterName" id="head">社群头像</span>
					<div class="webservice_content">
						<!--添加的图片-->
						<div class="picture_box">
						<c:choose>
							<c:when test="${skillInfo.myAvatarVal!=''&&skillInfo.myAvatarVal!=null&&skillInfo.myAvatarVal!='null'}">
								<div class="add_picture" onclick="uploadImage(1,1);">
									<img id="myAvatar" src="${skillInfo.myAvatarVal}" class="search_pic"/>
									<input type="text" id="myAvatarVal" name="myAvatarVal" value="${skillInfo.myAvatarVal}">
								</div>
							</c:when>
							<c:otherwise>
								<div class="add_picture"  style="display: none" onclick="uploadImage(1,1);">
									<img id="myAvatar" src=" " class="search_pic"/>
									<input type="hidden" id="myAvatarVal" name="myAvatarVal" value="">
								</div>
							</c:otherwise>
						</c:choose>
								
						</div>
						<!--<添加的图片完-->
						<c:if test="${empty skillInfo.myAvatarVal}">
							<div class="add_pic photoHead" onclick="uploadImage(1,1);">
								<span class="iconfont icon-pic"></span>
								<span class="size">+添加图片</span>
							</div>
						</c:if>
					</div>
					<p class="limit association_limit" id="title">*请上传社群头像</p>
					
					<span class="enter_name publishService_name community_pic" id="head_1">群成员图</span>
					<span class="introduct" id="titleSon">( 照片需体现群人数哦 )</span>
					<div class="webservice_content">
						<!--添加的图片-->
						<div class="picture_box" id="picture_box_daily">
							<c:if test="${not empty dairyTweetImg}">
									<c:forEach items="${dairyTweetImg}" var="img" >
									 <div class="add_picture">
										<img src="${img}" class="search_pic" />
										<span onclick="delImg('${objMap.xkh_dairyImgId}','${img}',this,'dairyTweetUrl')" class="iconfont icon-shanchu"></span>
										<span class="cover"></span>
									</div>
									</c:forEach>
							</c:if>
						</div>
						<!--<添加的图片完-->
						<div class="add_pic" onclick="uploadImage(2,9);">
							<span class="iconfont icon-pic"></span>
							<span class="size">+添加图片</span>
						</div>
					</div>
					<p class="limit association_limit" id="title_1">*请上传1-2张群成员图片</p>
					
					
					<span class="enter_name publishService_name community_pic spacing" id="head_2">活动图片</span>
					<span class="introduct">(&nbsp;选填&nbsp;)</span>
					<div class="webservice_content">
						<!--添加的图片-->
						<div class="picture_box" id="picture_box_activity">
						<c:if test="${not empty imgArr}">
								<c:forEach items="${imgArr }" var="img" >
								 <div class="add_picture">
									<img src="${img}" class="search_pic" />
									<span onclick="delImg('${objMap.xkh_imgId }','${img}',this,'activtyUrl')" class="iconfont icon-shanchu"></span>
									<span class="cover"></span>
								</div>
								</c:forEach>
							</c:if>
						</div>
						<!--<添加的图片完-->
						<div class="add_pic"  onclick="uploadImage(3,9);">
							<span class="iconfont icon-pic"></span>
							<span class="size">+添加图片</span>
						</div>
					</div>
					<p class="limit" id="title_2">*最多可上传9张活动照片</p>
				</li>
				
				<li>
					<span class="enter_name publishService_name community_pic spacing">活动案例</span>
					<span class="introduct">(&nbsp;选填&nbsp;)</span>
					<div class="textarea">
						<div class="textarea_info">
							<textarea class="Introduce" id="skillDepict" name="skillDepict" onkeyup="load2()" placeholder="请填写社群组织开展过的活动，群成员参与情况等">${objMap.skillDepict}</textarea>
						</div>
						<div id="span2" class="span"><span>0</span>/200</div>
					</div>
				</li>
			</ul>
			<input type="hidden" name="serviceType" id="serviceType" value="1" class="serviceType">
			<input type="hidden" name="basicId" id="basicId" value="${paramsMap.basicId}"><%-- ${paramsMap.basicId} --%>
			<input type="hidden" value="${paramsMap.skillId}" name="skillId" id="skillId"><!-- 技能Id -->
			<input type="hidden" name="myAvatarVal" id="myAvatarVal" value="${skillInfo.myAvatarVal}" required="required"/>
			<input type="hidden" name="dairyTweetUrl" id="dairyTweetUrl" value="${objMap.dairyTweetImg}" required="required"/>
			<input type="hidden" name="activtyUrl" id="activtyUrl" value="${objMap.imgString}" required="required"/>
		</section>
		</form>
		<footer class="write_foot" onclick="sub();"><span>提交</span></footer>
		</ul>
	</body>
   <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js" ></script>
   <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.min.js" ></script>
   <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.picker.min.js" ></script>
   <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.poppicker.js" ></script>
   <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/publishService/association.js" ></script>
   <script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
   <script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
   
   <script type="text/javascript">
   var socialType =  $.trim($(".socialType").html());
 
   if(socialType=="自媒体"){
	   
	   $("#head").text("公众号头像");
		$("#title").text("*请上传公众号头像");
		$("#head_1").text("日常推文图");
		$("#title_1").text("*请上传公众号头像");
		$("#titleSon").text("( 照片需体现阅读量哦 )");
		
	   $(".association_content ul:eq(1)").removeClass("association_none");
		$(".association_content ul:eq(0)").addClass("association_none");
		$(".li span:first-child").addClass("community_enterName spacing");
		
		$(".noZmt input").attr("disabled","disabled");
		$(".zmt input").removeAttr("disabled");
   }else{
	   $(".association_content ul:eq(0)").removeClass("association_none");
		$(".association_content ul:eq(1)").addClass("association_none");
		$(".li span:first-child").removeClass("community_enterName spacing");
		
		$(".noZmt input").removeAttr("disabled");
		$(".zmt input").attr("disabled","disabled");
   }
   
	function sub(){
		
		var picture_box_daily = $("#picture_box_daily").children("div").length;
		var skillSonId = $("#skillSonId").val();
		if(skillSonId==""){
			alert("请选择社群类型")
			return;
		}
		if(socialType=="自媒体"){
			var skillName = $("#zmtName").val();
			if(skillName==""||!skillName){
				alert("请输入公众号名称");
				return
			}
			var topVal = $("#topLinePrice").val();
			var lessVal = $("#lessLinePrice").val();
			if($.trim(topVal)==""){
				alert("请输入头条报价")
				return
			}
			if($.trim(lessVal)==""){
				alert("请输入次条报价");
				return
			}
			if(2>picture_box_daily||picture_box_daily>9){
				
				alert("日常推文图上传为2~9张");
				return 
			}
			
		}else{
			var skillName = $("#sqName").val();
			if(skillName==""||!skillName){
				alert("请输入社群名称");
				return
			}
			var groupNum = $("#groupNum").val();
			if($.trim(groupNum)==""){
				
				alert("请输入社群人数");
				return
			}
			
			var skillPrice = $("#skillPrice").val();
			if(skillPrice==""||skillPrice==0.0){
				alert("请输入价格");
				return
			}
			if(2>picture_box_daily||picture_box_daily>9){
				
				alert("群成员图片上传为2~9张");
				return 
			}
		}
		//判断当前用户是否上传图片
		var myAvatarVal = $("#myAvatar").attr('src'); 

		var picture_box_activity = $("#picture_box_activity").children("div").length;
		
		if($.trim(myAvatarVal)==""||$.trim(myAvatarVal)==0){
			alert("请上传头像");
			return 
		}
		
		if(picture_box_activity!=0){
			if(2>picture_box_activity||picture_box_activity>9){
				
				alert("活动图片上传为2~9张");
				return 
			}
		}
		uploadHeadImageSelf();
	}
	//选择社群类型
	var associationstyle = new mui.PopPicker({
		layer: 1
	});
	
	if(socialType!=""&&socialType!=null){
		document.querySelector('#associationStyle .result-tips').style.display = "none";
		document.querySelector('#associationStyle .show-result').style.display = "block";
	}
	var sonlist = ${sonList};
	associationstyle.setData(sonlist);
	var showassociationButton = document.getElementById('associationStyle');
	showassociationButton.addEventListener('tap', function(event) {
		associationstyle.show(function(items) {
			document.querySelector('#associationStyle .result-tips').style.display = "none";
			document.querySelector('#associationStyle .show-result').style.display = "block";
			document.querySelector('#associationStyle .show-result').innerText = items[0].text;
			socialType = items[0].text;
			$("#skillSonId").val(items[0].VALUE);
			//返回 false 可以阻止选择框的关闭
			//return false;
			if(items[0].text=="自媒体"){
				
				$("#head").text("公众号头像");
				$("#title").text("*请上传公众号头像");
				$("#head_1").text("日常推文图");
				$("#title_1").text("*请上传公众号头像");
				$("#titleSon").text("( 照片需体现阅读量哦 )");
				
				
				$(".association_content ul:eq(1)").removeClass("association_none");
				$(".association_content ul:eq(0)").addClass("association_none");
				$(".li span:first-child").addClass("community_enterName spacing");
				
				$(".noZmt input").attr("disabled","disabled");
				$(".zmt input").removeAttr("disabled");
			}else{
				$("#head").text("社群头像");
				$("#title").text("*请上传社群头像");
				$("#titleSon").text("( 照片需体现群人数哦 )");
				$("#head_1").text("群成员图");
				$("#title_1").text("*请上传1-2张群成员图片");
				
				
				
				$(".association_content ul:eq(0)").removeClass("association_none");
				$(".association_content ul:eq(1)").addClass("association_none");
				$(".li span:first-child").removeClass("community_enterName spacing");
				
				$(".noZmt input").removeAttr("disabled");
				$(".zmt input").attr("disabled","disabled");
			}
		});
	}, false);
	
	
	
	
	
	//微信上传 全局变量
	var wxSelf;
	var localArr = new Array();
	var localHeadArr = new Array();//头像
	var localDailyArr = new Array();//日常推文的组合  
	var localActivityArr = new Array();//存放活动的图片
	var serverArr = new Array();
	
	var serverHeadArr = new Array();//头像
	var serverDailyArr = new Array();//日常推文的组合  
	var serverActivityArr = new Array();//存放活动的图片

	/*001 选择图片   日常推文选择图片*/
	function uploadImage(flag,num) {
		
		var clientUrl = window.location.href;
		var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
		//请求后台，获取jssdk支付所需的参数
		$.ajax({
			type : 'post',
			url : reqPath,
			dataType : 'json',
			data : {
				"clientUrl" : clientUrl
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
			},
			cache : false,
			error : function() {
				alert("系统错误，请稍后重试");
				return false;
			},
			success : function(data) {
				//微信支付功能只有微信客户端版本大于等于5.0的才能调用
				var return_date = eval(data);
				/* alert(return_date ); */
				if (parseInt(data[0].agent) < 5) {
					alert("您的微信版本低于5.0无法使用微信支付");
					return;
				}
				//JSSDK支付所需的配置参数，首先会检查signature是否合法。
				wx.config({
					debug : false, //开启debug模式，测试的时候会有alert提示
					appId : return_date[0].appId, //公众平台中-开发者中心-appid
					timestamp : return_date[0].config_timestamp, //时间戳
					nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
					signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
					jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
				});
				//上方的config检测通过后，会执行ready方法
				wx.ready(function() {
					wxSelf=wx;
					wx.chooseImage({
						count: num, // 默认9
						sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						success: function (res) {
							
							if(flag =="1"){//头像
								localHeadArr = res.localIds;
								if(localHeadArr.length > 0){
							        for(var j = 0; j < localHeadArr.length; j++){
							        	 var myAvatar=localHeadArr[0];
									        $("#myAvatar").attr("src",myAvatar);
									        $("#myAvatarVal").val(myAvatar);
									        $("#myAvatar").parent(".add_picture").css('display','block');
									        headFlag=false; 
							        }
							        $(".photoHead").hide(); 
								}
							}
							if(flag =="2"){//推文图片
								localDailyArr=appandArray(localDailyArr,res.localIds);
								if(localDailyArr.length > 0){
									var result = "";
							        for(var j = 0; j < res.localIds.length; j++){
							        	result += "<div id='xkh_daily_"+j+"' class='add_picture'>"+
												"<img src='"+res.localIds[j]+"' class='search_pic' />"+
													"<span onclick='removeImgDairy("+j+",this)' class='iconfont icon-shanchu' ></span>"+
													"<span class='cover'></span>"+
												"</div>"; 
												
							        }
							        
							        var imgNum = $("#picture_box_daily img").length;
							        var picNum =  parseInt(imgNum) + parseInt(localDailyArr.length);
							        if(picNum>9){
							        	alert("请上传1-9张图片！");
							        }else{
							        	$("#picture_box_daily").append(result);
							        } 
							        
								}
							}
							if(flag =="3"){//活动图片
								//localActivityArr.push(res.localIds)
								localActivityArr=appandArray(localActivityArr,res.localIds);
								if(localActivityArr.length > 0){
									var result = "";
							        for(var j = 0; j < res.localIds.length; j++){
							        	result += "<div id='xkh_activity_"+j+"' class='add_picture'>"+
												"<img src='"+res.localIds[j]+"' class='search_pic' />"+
													"<span onclick='removeImgActiv("+j+",this)' class='iconfont icon-shanchu' ></span>"+
													"<span class='cover'></span>"+
												"</div>";
							        }
							        var imgNum = $("#picture_box_activity img").length;
							        var picNum =  parseInt(imgNum) + parseInt(localDailyArr.length);
							        if(picNum>9){
							        	alert("请上传1-9张图片！");
							        }else{
							        	$("#picture_box_activity").append(result);
							        } 
							        
								}
							}
							//localArr= res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
						}
					 });

					});
					 wx.error(function(res) {
						alert(res.errMsg);
					});
				}
		});
	}		
	
	
	
	
	/*002 头像 点击提交后上传到微信*/
	function uploadHeadImageSelf(){
		 if(0==localHeadArr.length){
			 uploadlocalDailyImageSelf();
		 }else{
			 for(var i=0;i<localHeadArr.length;i++){
				 wxSelf.uploadImage({
				        localId: localHeadArr[i], // 需要上传的图片的本地ID，由chooseImage接口获得
				        isShowProgressTips: 1, // 默认为1，显示进度提示
				        success: function (res) {
				            var serverId = res.serverId; // 返回图片的服务器端ID
				            serverHeadArr.push(serverId);
				            
				            if(i == localHeadArr.length){
				            	 //alert("执行上传 count"+serverArr);  
				                var serverStr="";
				                if(serverHeadArr[0].length >0 ){
				    	            serverStr=serverHeadArr[0] ;
				    	           // alert(serverStr);
				    	            $.ajax({
				    		        		type : "post",
				    		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
				    		        		dataType : "json",
				    		        		data : {serverId:serverStr},
				    		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
				    		        			//alert("data="+data.path);
				    		        			$("#myAvatarVal").val(data.path);
				    		        			uploadlocalDailyImageSelf();
				    		        		}
				    		        	});	
				                	
				                }else{
				                	alert("上传头像失败");
				                }
				                            	
				            }
				         }
				    }); 
			 	}
		 }
	}
	
	
	
	/*群成员图片或者日常推文图片 点击提交后上传到微信*/
	/*使用递归的方式进行上传群成员图片*/
	function uploadlocalDailyImageSelf() {
        if (localDailyArr.length == 0) {
        	var serverStr="";
        	if(0 == serverDailyArr.length ){
        		uploadActivityImageSelf();
        	}else{
	            for(var j=0;j<serverDailyArr.length;j++){
	            	serverStr+=serverDailyArr[j]+";" ;
	            }
            	
                $.ajax({
	        		type : "post",
	        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
	        		dataType : "json",
	        		data : {serverId:serverStr},
	        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
	        			var imageList = data;
	        			var imgPath="";
						for(var f=0;f<imageList.length;f++){
							var imgPathTemp = imageList[f];
							imgPath += imgPathTemp+",";
						}
						var u = $("#dairyTweetUrl").val()+imgPath;
						$("#dairyTweetUrl").val(u);
						
						var imgNum = $("#picture_box_daily img").length;
						if(imgNum<1||imgNum>9){
							alert("请上传1-9张图片");
							$(".webservice_mask").addClass("mask_none");
							return;
						}
						uploadActivityImageSelf();
	        		}
	        	});	
        	}
        }
        var localId = localDailyArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
               serverDailyArr.push(serverId);
               localDailyArr.shift();
               uploadlocalDailyImageSelf();
                serverStr+=serverId+";" ;
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
            }
        });
     
    }
	
	
	
	/*活动图片 点击提交后上传到微信*/
	/*使用递归的方式进行上传图片*/
	function uploadActivityImageSelf() {
        if (localActivityArr.length == 0) {
        	var serverStr="";
        	if(0 == serverActivityArr.length ){
        		$("#skillPub").submit();
        	}else{
	            for(var j=0;j<serverActivityArr.length;j++){
	            	serverStr+=serverActivityArr[j]+";" ;
	            }
            	
                $.ajax({
	        		type : "post",
	        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
	        		dataType : "json",
	        		data : {serverId:serverStr},
	        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
	        			
	        			//alert("+++++++++data++++++:"+data);	
	        			var imageList = data;
	        			//var pathList = data.
	        			
	        			var imgPath="";
	        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
						for(var f=0;f<imageList.length;f++){
					//		alert("+++++++++++++++:"+imageList[f]);
							var imgPathTemp = imageList[f];
							imgPath += imgPathTemp+",";
						}
						
						var u = $("#activtyUrl").val()+imgPath;
						$("#activtyUrl").val(u)
						
						var imgNum = $("#picture_box_activity img").length;
						//alert("imgNum:"+imgNum);
						if(imgNum<1||imgNum>9){
							alert("活动图片个数为1-9");
							$(".webservice_mask").addClass("mask_none");
							return;
						}
						
	        			$("#skillPub").submit();
						$(".webservice_mask").removeClass("mask_none");
	        		}
	        	});	
        	}
        }
        var localId = localActivityArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
               serverActivityArr.push(serverId);
                localActivityArr.shift();
                uploadActivityImageSelf();
                serverStr+=serverId+";" ;
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
            }
        });
    }
	
	function appandArray(a,b){

		for(var i=0;i<b.length;i++){
			 a.push(b[i]);
		}
		return a;
	}
	
	
	
	function removeImgActiv(imageId,obj){
		obj.parentElement.remove();
		localActivityArr.splice(imageId,1);
	}
	
	function removeImgDairy(imageId,obj){
		obj.parentElement.remove();
		localDailyArr.splice(imageId,1)
	}
	
	//点击删除图片                                
	function delImg(imgId,val,obj,str){
	  	if(confirm("确定删除?")){
	  			//表示修改需要数据库删除
	  			$.ajax({
	  				type : "post",
	  				url : "<%=basePath%>pubSkill/delImage.html",
	  				dataType : "json",
	  				data : {imgeId:imgId,imageUrl:val},
	  				success : function(data) {
	  					if(data.flag){
	  						$("#"+str).val(data.typeValue);
	  						obj.parentElement.remove();
	  					}
	  				}
	  			});
	    }
	  }
	
	
	
   </script>
</html>