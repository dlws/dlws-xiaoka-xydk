<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>发布服务</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.picker.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/enterInfo.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publishService.css" />
	</head>

	<body>
		<!--头部-->
		<div class="enterInfo_top">
			<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/enterInfo_top.png" class="search_pic" />
		</div>
		<div class="person_style">
			<span>发布类型</span>
		</div>
		<form action="<%=basePath%>pubSkill/add.html" id="skillPub" method="post">
		<div class="publishService_pic">
			<c:forEach items="${classList}" var="classInfo" varStatus="status">
			<c:choose>
				<c:when test="${classInfo.classValue=='xnsj'}">
					<a href="<%=basePath%>pubSkill/chosePubSkill.html?classValue=${classInfo.classValue}&sid=${classInfo.id}&skillId=${paramsMap.skillId}&basicId=${paramsMap.basicId}" class="choose choose_style">
						<span class="picture_green"><img src="${classInfo.selectimage}" class="search_pic typePic" /></span>
						<span class="color_green">${classInfo.className }</span>
					</a>
					<input type="hidden" name="skillFatId" id="skillFatId" value="${classInfo.id}">
				</c:when>
				<c:otherwise>
					<a href="<%=basePath%>pubSkill/chosePubSkill.html?classValue=${classInfo.classValue}&sid=${classInfo.id}&skillId=${paramsMap.skillId}&basicId=${paramsMap.basicId}" class="choose choose_style">
						<span class="picture_green"><img src="${classInfo.noSelectimage}" class="search_pic typePic" /></span>
						<span>${classInfo.className }</span>
					</a>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		</div>
		<div class="enterInfo_bg"></div>
		<ul class="message_content">
			<li>
				<span class="enter_name">商家名称</span>
				<input class="inp" type="text" name="skillName" id="skillName" value="${objMap.skillName }" placeholder="请填写商家名称" />
			</li>
			<li>
				<span class="enter_name">商家类型</span>
				<div id="placeStyle" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">请选择商家类型</span>
					<span class="myInfo_name enter_school show-result sellType">
						<c:forEach items="${sonEditList}" var="one">
							<c:if test="${one.VALUE==objMap.skillSonId }">${one.text}</c:if>
						</c:forEach>
					</span>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
					<input type="hidden" name="skillSonId" id="skillSonId" value="${objMap.skillSonId}" />
				</div>
			</li>
			<li>
				<span class="enter_name publish_schoolName">合作形式</span>
				<div class="resource_content">
					<c:forEach items="${cooperaTypeList}" var="cooperaType" varStatus="statu">
						<c:forEach items="${skillInfo.cooperaType}" var="selectCoopera">
							<c:if test="${cooperaType.id==selectCoopera.key}">
								<c:set var="flage" value="trues" scope="request"></c:set>
							</c:if>
						</c:forEach>
						
						<c:forEach items="${skillInfo.companyValue}" var="selectUnit">
							<c:if test="${cooperaType.id==selectUnit.key}">
								<c:set var="unit" value="${selectUnit.value}" scope="request"></c:set>
							</c:if>
						</c:forEach>
						
						<c:choose>
							<c:when test="${flage=='trues'}">
								<div class="resource place_resource resouce_green gou_${statu.index }">
									${cooperaType.dic_name}
									<input type="hidden" value="${cooperaType.id}" name="cooperaType">
									<input type="hidden" value="${unit}" name="cooperaCompany" id="cooperaCompany" class="cooperaCompany">
									<span style="display:block;"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/gou.png" class="search_pic"></span>
								</div>
							</c:when>
							
							<c:otherwise>
								<div class="resource place_resource">
									${cooperaType.dic_name}
									<input type="hidden" value="${cooperaType.id}" name="cooperaType" disabled="disabled">
									<input type="hidden" name="cooperaCompany" id="cooperaCompany" class="cooperaCompany" disabled="disabled">
									<span><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/gou.png" class="search_pic"></span>
								</div>
							</c:otherwise>
						</c:choose>
							<c:set var="flage" value="false" scope="request"></c:set> 
					</c:forEach>
				</div>
				
				<div class="shoppe_content">
					<c:forEach items="${cooperaTypeList}" var="cooperaType" varStatus="status">
						<c:forEach items="${skillInfo.cooperaType}" var="selectCoopera">
							<c:if test="${cooperaType.id==selectCoopera.key}">
								<c:set var="flage" value="trues" scope="request"></c:set>
								<c:set var="price" value="${selectCoopera.value}" scope="request"></c:set>
								<c:set var="unit" value="${selectCoopera.value}" scope="request"></c:set>
							</c:if>
						</c:forEach>
						
						<c:forEach items="${skillInfo.companyValue}" var="selectUnit">
							<c:if test="${cooperaType.id==selectUnit.key}">
								<c:set var="unit" value="${selectUnit.value}" scope="request"></c:set>
							</c:if>
						</c:forEach>
						
						<c:choose>
							<c:when test="${flage=='trues'}">
								<div class="shoppe_box">
								<span class="enter_name shoppe">${cooperaType.dic_name}</span>
								<div class="service_price serviceSchool_price">
									<input type="number" value="${price}" name="cooperaPrice" class="cooperaPrice" 2="this.value=(this.value.match(/\d+(\.\d{0,2})?/)||[''])[0]" this.style.imeMode='disabled' placeholder="请填写报价">
									<span>元/</span>
									<div id="unitStyle${status.index+1}" class="mui-input-row" style="float: left;">
										<span class="choose_unit result-tips school_unit">请选择单位</span>
										<span class="choose_unit school_unit show-result choseUnit">${unit}</span>
									</div>
									<span style="margin-right:0 ;">单位</span>
								</div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="shoppe_box mask_none">
								<span class="enter_name shoppe">${cooperaType.dic_name}</span>
								<div class="service_price serviceSchool_price">
									<input type="number"  name="cooperaPrice" class="cooperaPrice" onkeypress="this.value=(this.value.match(/\d+(\.\d{0,2})?/)||[''])[0]" placeholder="请填写报价" this.style.imeMode='disabled' disabled="disabled">
									<span>元/</span>
									<div id="unitStyle${status.index+1}" class="mui-input-row" style="float: left;">
										<span class="choose_unit result-tips school_unit">请选择单位</span>
										<span class="choose_unit school_unit show-result choseUnit"></span>
									</div>
									<span style="margin-right:0 ;">单位</span>
								</div>
								</div>
							</c:otherwise>
						</c:choose>
								<c:set var="flage" value="false" scope="request"></c:set> 
					</c:forEach>
				</div>
			</li>
			<li>
				<span class="enter_name publishService_name">门店照片</span>
				<span class="enter_name enterName2">全景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="allPhoto">
						<c:forEach items="${listAll}" var="all" varStatus="bs">
							<div class="add_picture">
							<img src="${all}" class="search_pic" />
							<span onclick="delImg('${objMap.allId}','${all}',this,'allPhoto_img')" class="iconfont icon-shanchu"></span>
							<span class="cover"></span>
							</div>
						</c:forEach>
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage(4,9);">
						<span class="iconfont icon-pic"></span>
						<input type="hidden" value="${AllStr}" name="allPhoto" id="allPhoto_img">
						<span class="size">+添加图片</span>
					</div>
				</div>
				<p class="limit">*请上传1-9张全景照片</p>
				<span class="enter_name add_photo enterName2">中景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="middlePho">
						<c:forEach items="${listMiddle}" var="middle" varStatus="bs">
						 <div class="add_picture">
							<img src="${middle}" class="search_pic" />
							<span onclick="delImg('${objMap.middleId}','${middle}',this,'middlePho_img')" class="iconfont icon-shanchu"></span>
							<span class="cover"></span>
						 </div>
						</c:forEach>
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage(3,9)">
						<span class="iconfont icon-pic"></span>
						<input type="hidden" value="${middleStr}" name="middlePho" id="middlePho_img">
						<span class="size">+添加图片</span>
					</div>
				</div>
				<p class="limit">*请上传1-9张中景照片</p>
				<span class="enter_name add_photo enterName2">近景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="closePhoto">
						<c:forEach items="${listClose}" var="close" varStatus="bs">
							<div class="add_picture">
								<img src="${close}" class="search_pic" />
								<span onclick="delImg('${objMap.closeId}','${close}',this,'closePhoto_img')" class="iconfont icon-shanchu"></span>
								<span class="cover"></span>
							</div>
						</c:forEach>
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage(2,9)">
						<span class="iconfont icon-pic"></span>
						<input type="hidden" value="${closeStr}" name="closePhoto" id="closePhoto_img">
						<span class="size">+添加图片</span>
					</div>
				</div>
				<p class="limit">*请上传1-9张近景照片</p>
			</li>
			<li>
				<span class="enter_name publishService_name">门店描述</span>
				<div class="textarea">
					<div class="textarea_info">
						<textarea class="introduce" id="skillDepict" name="skillDepict" onkeyup="load()" placeholder="请填写和门店有关的信息，譬如门店销售品类，人流量优势等等">${objMap.skillDepict}</textarea>
					</div>
					<div id="span" class="span"><span>0</span>/200</div>
				</div>
			</li>
		</ul>
		<input type="hidden" name="basicId" id="basicId" value="${paramsMap.basicId}"><%-- ${paramsMap.basicId} --%>
		<input type="hidden" name="serviceType" id="serviceType" value="2" class="serviceType">
		<input type="hidden" name="skillPrice" id="skillPrice" value="">
		<input type="hidden" value="${paramsMap.skillId}" name="skillId" id="skillId"><!-- 技能Id -->
		</form>
		<footer class="write_foot" onclick="sub();"><span>提交</span></footer>
	    </ul>		
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.picker.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.poppicker.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/publishService/school.js" ></script>
	<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
    <script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
	<script type="text/javascript" >
	
	 String.prototype.endWith=function(s){
	  if(s==null||s==""||this.length==0||s.length>this.length)
	     return false;
	  if(this.substring(this.length-s.length)==s)
	     return true;
	  else
	     return false;
	  return true;
	 }
	 
	//获取近景的照片
	function sub(){
		var skillName = $("#skillName").val();
		if(skillName==""||!skillName){
			alert("请输入商家名称");
			return
		}
		var skillSonId = $("#skillSonId").val();
		if(skillSonId==""){
			alert("请选择场地类型")
			return;
		}
		var skillPrice = "";
		$(".cooperaPrice").each(function(){
			if($(this).attr("disabled")!='disabled'&&$(this).val()!=""){
				skillPrice=$(this).val();
				return false;
			}
		});
		if(skillPrice==""){
			skillPrice=0;
			alert("请输入价格");
			return
		}
		$("#skillPrice").val(skillPrice);
		var unitText="";
		$(".choseUnit").each(function(){
			if($(this).css("display")=='block'){
				 unitText = $(this).html();
			}
		})
		if(unitText==""||!unitText){
			alert("请选择单位")			
			return
		}
		var greenLh = $(".resouce_green").length;
		if(greenLh<1){
			alert("请选择合作形式");
			return
		}
		uploadCloseImages();//近景图上传
	}
	//商家类型
	/* 修改回显******start**** */
	var sellType =  $.trim($(".sellType").html());
	if(sellType!=''&&sellType!=null){
		document.querySelector('#placeStyle .result-tips').style.display = "none";
		document.querySelector('#placeStyle .show-result').style.display = "block";
	}
	
	/* 修改回显******end**** */
	var sonlist = ${sonList};
	var placestyle = new mui.PopPicker({
		layer: 1
	});
	placestyle.setData(sonlist);
	var showplacestyleButton = document.getElementById('placeStyle');
	showplacestyleButton.addEventListener('tap', function(event) {
		placestyle.show(function(items) {
			document.querySelector('#placeStyle .result-tips').style.display = "none";
			document.querySelector('#placeStyle .show-result').style.display = "block";
			document.querySelector('#placeStyle .show-result').innerText = items[0].text;
			$("#skillSonId").val(items[0].VALUE);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});
	}, false);

	//合作形式
	$(".resource_content .resource").click(function(){
		var index=$(this).index();
		$(".shoppe_content .shoppe_box").eq(index).toggleClass("mask_none");
		
		$(".shoppe_content .shoppe_box").not(".shoppe_content .shoppe_box:first-child").children().addClass("store");
		if($(this).hasClass("resouce_green")){
			$(this).removeClass("resouce_green");
		    $(this).find("span").hide();
		    $(this).find("input").attr("disabled","disabled");
		    $(".shoppe_content .shoppe_box").find("input[type='number']").eq(index).attr("disabled","disabled");
		}else{
			$(this).addClass("resouce_green");
		    $(this).find("span").show();
		    $(this).find("input").removeAttr("disabled");
		    $(".shoppe_content .shoppe_box").find("input[type='number']").eq(index).removeAttr("disabled");
		}
	})
	
	
	var company = ${canpanyList};
	
 	/* var cooperaTypeList = $(".resource_content div").length;
	 for(var i=0;i<cooperaTypeList;i++){
		var unitstyle = "unitstyle"+(i+1);
		var unitstyle = new mui.PopPicker({
		layer: 1
		});
		
		unitstyle.setData(company);
		var showunitstyleButton = document.getElementById('unitStyle'+(i+1));
		showunitstyleButton.addEventListener('tap', function(event) {
			unitstyle.show(function(items) {
				var uStyle = "#unitStyle"+(i+1);
				document.querySelector(''+uStyle+' .result-tips').style.display = "none";  
				document.querySelector('#unitStyle1 .show-result').style.display = "block";
				document.querySelector('#unitStyle1 .show-result').innerText = items[0].text;
				$(".cooperaCompany").eq(0).val(items[0].VALUE);
			});
		}, false);
	 }  */
	 var unitLh = $(".unit").length;
	//选择单位1
	 var unitstyle1 = new mui.PopPicker({
		layer: 1
	});
	 if($(".gou_0").length>0){
		 document.querySelector('#unitStyle1 .result-tips').style.display = "none";  
		 document.querySelector('#unitStyle1 .show-result').style.display = "block";
	 }
		
	unitstyle1.setData(company);
	var showunitstyleButton = document.getElementById('unitStyle1');
	showunitstyleButton.addEventListener('tap', function(event) {
		unitstyle1.show(function(items) {
			document.querySelector('#unitStyle1 .result-tips').style.display = "none";  
			document.querySelector('#unitStyle1 .show-result').style.display = "block";
			document.querySelector('#unitStyle1 .show-result').innerText = items[0].text;
			$(".cooperaCompany").eq(0).val(items[0].VALUE);
		});

	}, false);
	
	//选择单位2
	var unitstyle2 = new mui.PopPicker({
		layer: 1
	});
	
	if($(".gou_1").length>0){
		document.querySelector('#unitStyle2 .result-tips').style.display = "none";  
		document.querySelector('#unitStyle2 .show-result').style.display = "block";
	}
	
	unitstyle2.setData(company);
	var showunitstyleButton = document.getElementById('unitStyle2');
	showunitstyleButton.addEventListener('tap', function(event) {
		unitstyle2.show(function(items) {
			document.querySelector('#unitStyle2 .result-tips').style.display = "none";  
			document.querySelector('#unitStyle2 .show-result').style.display = "block";
			document.querySelector('#unitStyle2 .show-result').innerText = items[0].text;
			$(".cooperaCompany").eq(1).val(items[0].VALUE);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});
	}, false);
	

	//选择单位3
	var unitstyle3 = new mui.PopPicker({
		layer: 1
	});
	if($(".gou_2").length>0){
		document.querySelector('#unitStyle3 .result-tips').style.display = "none";  
		document.querySelector('#unitStyle3 .show-result').style.display = "block";
	}
	unitstyle3.setData(company);
	var showunitstyleButton = document.getElementById('unitStyle3');
	showunitstyleButton.addEventListener('tap', function(event) {
		unitstyle3.show(function(items) {
			document.querySelector('#unitStyle3 .result-tips').style.display = "none";  
			document.querySelector('#unitStyle3 .show-result').style.display = "block";
			document.querySelector('#unitStyle3 .show-result').innerText = items[0].text;
			$(".cooperaCompany").eq(2).val(items[0].VALUE);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});
	}, false);
	
	//选择单位4
	var unitstyle4 = new mui.PopPicker({
		layer: 1
	});
	
	if($(".gou_3").length>0){
		document.querySelector('#unitStyle4 .result-tips').style.display = "none";  
		document.querySelector('#unitStyle4 .show-result').style.display = "block";
	}
	unitstyle4.setData(company);
	var showunitstyleButton = document.getElementById('unitStyle4');
	showunitstyleButton.addEventListener('tap', function(event) {
		unitstyle4.show(function(items) {
			document.querySelector('#unitStyle4 .result-tips').style.display = "none";  
			document.querySelector('#unitStyle4 .show-result').style.display = "block";
			document.querySelector('#unitStyle4 .show-result').innerText = items[0].text;
			$(".cooperaCompany").eq(3).val(items[0].VALUE);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});
	}, false);
	

	//选择单位5
	var unitstyle5 = new mui.PopPicker({
		layer: 1
	});
	if($(".gou_4").length>0){
		document.querySelector('#unitStyle5 .result-tips').style.display = "none";
		document.querySelector('#unitStyle5 .show-result').style.display = "block";
	}
	unitstyle5.setData(company);
	var showunitstyleButton = document.getElementById('unitStyle5');
	showunitstyleButton.addEventListener('tap', function(event) {
		unitstyle2.show(function(items) {
			document.querySelector('#unitStyle5 .result-tips').style.display = "none";  
			document.querySelector('#unitStyle5 .show-result').style.display = "block";
			document.querySelector('#unitStyle5 .show-result').innerText = items[0].text;
			$(".cooperaCompany").eq(4).val(items[0].VALUE);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});
	}, false);
	

	/** 微信图片上传* */
	var wxSelf;
	var localArr = new Array();
	var localHeadArr = new Array();//头像
	var localCloseArr = new Array();//日常推文的组合  
	var localMiddelArr = new Array();//存放活动的图片
	var localAlllArr = new Array();//全景图
	
	var serverHeadArr = new Array();//头像
	var serverCloseArr = new Array();//日常推文的组合  
	var serverMiddelArr = new Array();//存放活动的图片
	var serverAlllArr = new Array();//存放全景照片

	
	
	
	/*001 选择图片*/
	function uploadImage(flag,num) {
		var clientUrl = window.location.href;
	  	var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
		//请求后台，获取jssdk支付所需的参数
		$.ajax({
			type : 'post',
			url : reqPath,
			dataType : 'json',
			data : {
				"clientUrl" : clientUrl
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
			},
			cache : false,
			error : function() {
				alert("系统错误，请稍后重试");
				return false;
			},
			success : function(data) {
				//微信支付功能只有微信客户端版本大于等于5.0的才能调用
				var return_date = eval(data);
				/* alert(return_date ); */
				if (parseInt(data[0].agent) < 5) {
					alert("您的微信版本低于5.0无法使用微信支付");
					return;
				}
				//JSSDK支付所需的配置参数，首先会检查signature是否合法。
				wx.config({
					debug : false, //开启debug模式，测试的时候会有alert提示
					appId : return_date[0].appId, //公众平台中-开发者中心-appid
					timestamp : return_date[0].config_timestamp, //时间戳
					nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
					signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
					jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
				});
				//上方的config检测通过后，会执行ready方法
				wx.ready(function() {
					wxSelf=wx;
					wx.chooseImage({
						count: num, // 默认9
						sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						success: function (res) {
							if(flag =="2"){//近景
								localCloseArr= appandArray(localCloseArr,res.localIds);
								if(localCloseArr.length > 0){
									var result = "";
							        for(var j = 0; j < res.localIds.length; j++){
							        	result += "<div id='close_"+j+"' class='add_picture'>"+
												"<img src='"+res.localIds[j]+"' class='search_pic' />"+
													"<span onclick='removeImg("+j+",this,localCloseArr)' class='iconfont icon-shanchu' ></span>"+
													"<span class='cover'></span>"+
												"</div>";
												
							        }
							        var imgNum = $("#closePhoto img").length;
								    var picNum =  parseInt(imgNum) + parseInt(localCloseArr.length);
								    if(picNum>9){
								    	alert("请上传1-9张图片！");
								    }else{
								    	$("#closePhoto").append(result);
								    } 
								}
							}
							if(flag =="3"){//中景
								localMiddelArr= appandArray(localMiddelArr,res.localIds);
						
								if(localMiddelArr.length > 0){
									var result = "";
							        for(var j = 0; j < res.localIds.length; j++){
							        	result += "<div id='middle_"+j+"' class='add_picture'>"+
												"<img src='"+res.localIds[j]+"' class='search_pic' />"+
													"<span onclick='removeImg("+j+",this,localMiddelArr)' class='iconfont icon-shanchu' ></span>"+
													"<span class='cover'></span>"+
												"</div>";
							        }
							        var imgNum = $("#middlePho img").length;
								    var picNum =  parseInt(imgNum) + parseInt(localMiddelArr.length);
								    if(picNum>9){
								    	alert("请上传1-9张图片！");
								    }else{
								    	$("#middlePho").append(result);
								    }
								}
							}
							if(flag =="4"){//全景
								localAlllArr=appandArray(localAlllArr,res.localIds);
								if(localAlllArr.length > 0){
									var result = "";
							        for(var j = 0; j < res.localIds.length; j++){
							        	result += "<div id='all_"+j+"' class='add_picture'>"+
												"<img src='"+res.localIds[j]+"' class='search_pic' />"+
													"<span onclick='removeImg("+j+",this,localAlllArr)' class='iconfont icon-shanchu' ></span>"+
													"<span class='cover'></span>"+
												"</div>";
							        }
							        var imgNum = $("#allPhoto img").length;
								    var picNum =  parseInt(imgNum) + parseInt(localAlllArr.length);
								    if(picNum>9){
								    	alert("请上传1-9张图片！");
								    }else{
								    	$("#allPhoto").append(result);
								    }
							        
								}
							}
						}
					 });
					});
					 wx.error(function(res) {
						alert(res.errMsg);
					});
				}
		});
	}
	
	/*近景图片上传*/
	function uploadCloseImages() {
		//alert("Close*****function****localCloseArr.length："+localCloseArr.length);
        if (localCloseArr.length == 0) {
        	var serverStr="";//头像
        	if(0 == serverCloseArr.length ){
        		uploadMiddelImages();
        	}else{
        		for(var j=0;j<serverCloseArr.length;j++){
                	serverStr+=serverCloseArr[j]+";" ;
                }
        		//alert(serverStr);
                $.ajax({
            		type : "post",
            		url :  "<%=contextPath%>/skillUser/downloadImageSelf.html",
            		dataType : "json",
            		data : {serverId:serverStr},
            		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
            			
            			var imageList = data;
            			//alert(imageList);
            			var imgPath=$("#closePhoto_img").val();
    					for(var f=0;f<imageList.length;f++){
    						var imgPathTemp = imageList[f];
    						imgPath += imgPathTemp+",";
    					}
    					//alert(imgPath);
    					var closePhoto_img = $("#closePhoto_img").val();
    					if(closePhoto_img!=""&&closePhoto_img!=null){
    						
    						if(closePhoto_img.endWith(",")){
    							$("#closePhoto_img").val(closePhoto_img+imgPath);
    						}else{
    							$("#closePhoto_img").val(closePhoto_img+","+imgPath);
    						}
    						
    					}else{
    						$("#closePhoto_img").val(imgPath);
    					}
    					
    					//alert("Close*****function****data"+imgPath);
    					uploadMiddelImages();
            		}
            	});	
        	}
        	
        }
        var localId = localCloseArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 1, // 默认为1，显示进度提示
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
                serverCloseArr.push(serverId);
                localCloseArr.shift();
                uploadCloseImages(localCloseArr);
                serverStr+=serverId+";" ;
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
            }
        });
    }
	
	/* 中景图片上传*/
	function uploadMiddelImages() {
		//alert("Middel*****function****localMiddelArr.length："+localMiddelArr.length);
        if (localMiddelArr.length == 0) {
        	var serverStr="";
        	if(0 == serverMiddelArr.length ){
        		uploadAllImages();
        	}else{
        		for(var j=0;j<serverMiddelArr.length;j++){
                	serverStr+=serverMiddelArr[j]+";" ;
                }
    	        $.ajax({
    	    		type : "post",
    	    		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
    	    		dataType : "json",
    	    		data : {serverId:serverStr},
    	    		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
    	    			
    	    			//alert("+++++++++data++++++:"+data);	
    	    			var imageList = data;
    	    			//var pathList = data.
    	    			
    	    			var imgPath=$("#middlePho_img").val();
    	    		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
    					for(var f=0;f<imageList.length;f++){
    				//		alert("+++++++++++++++:"+imageList[f]);
    						var imgPathTemp = imageList[f];
    						imgPath += imgPathTemp+",";
    					}
    					//获取中景的照片
    					var middlePho_img = $("#middlePho_img").val();
    					if(middlePho_img!=""&&middlePho_img!=null){
    						if(middlePho_img.endWith(",")){
    							$("#middlePho_img").val(middlePho_img+imgPath);
    						}else{
    							$("#middlePho_img").val(middlePho_img+","+imgPath);
    						}
    						
    					}else{
    						$("#middlePho_img").val(imgPath);
    					}
    					//alert("Middel*****function****data"+imgPath);
    					uploadAllImages();
    	    		}
    	    	});	
        	}
        	
        }
        var localId = localMiddelArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 0, // 默认为1，显示进度提示
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
               serverMiddelArr.push(serverId);
               localMiddelArr.shift();
               uploadMiddelImages(localMiddelArr);
               serverStr+=serverId+";" ;
               // alert("localArr的长度："+localArr.length);
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
            }
        });
  }
	
	function removeCloseImg(imageId){
		$("#close_"+imageId).remove();
	}
	function removeMiddelImg(imageId){
		$("#middle_"+imageId).remove();
	}
	function removeImg(imageId,obj,Arr){
		obj.parentElement.remove();
		Arr.splice(imageId,1);
	}
	
	//点击删除图片
	function delImg(imgId,val,obj,str){
	  	if(confirm("确定删除?")){
	  			//表示修改需要数据库删除
	  			$.ajax({
	  				type : "post",
	  				url : "<%=basePath%>pubSkill/delImage.html",
	  				dataType : "json",
	  				data : {imgeId:imgId,imageUrl:val},
	  				success : function(data) {
	  					if(data.flag){
	  						$("#"+str).val(data.typeValue);
	  						obj.parentElement.remove();
	  					}
	  				}
	  			});
	    }
	  }
	// 追加数组的方法 a 被追加的数组 ， b 要追加的数组
	function appandArray(a,b){

		for(var i=0;i<b.length;i++){
			 a.push(b[i]);
		}
		return a;
	}
	
	
	
	
	
	/* 远景图片上传*/
	function uploadAllImages() {
		//alert("All*****function****localAlllArr.length："+localAlllArr.length);
        if (localAlllArr.length == 0) {
        	
        	var serverStr="";
        	if(0 == serverAlllArr.length ){
        		$("#skillPub").submit();
        	}else{
            for(var j=0;j<serverAlllArr.length;j++){
            	serverStr+=serverAlllArr[j]+";" ;
            }
            $.ajax({
        		type : "post",
        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
        		dataType : "json",
        		data : {serverId:serverStr},
        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
        			
        			//alert("+++++++++data++++++:"+data);	
        			var imageList = data;
        			//var pathList = data.
        			
        			var imgPath="";
        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
					for(var f=0;f<imageList.length;f++){
				//		alert("+++++++++++++++:"+imageList[f]);
						var imgPathTemp = imageList[f];
						imgPath += imgPathTemp+",";
					}
					//获取远景的照片
					var allPhoto_img = $("#allPhoto_img").val();
					if(allPhoto_img!=""&&allPhoto_img!=null){
						if(allPhoto_img.endWith(",")){
							$("#allPhoto_img").val(allPhoto_img+imgPath);
						}else{
							$("#allPhoto_img").val(allPhoto_img+","+imgPath);
						}
						
					}else{
						$("#allPhoto_img").val(imgPath);
					}
				//    alert("===========activity的imgUrl============"+$("#activityImage").val());
				//	alert("All*****function****data"+imgPath);
				alert(5)
        			$("#skillPub").submit();
        		}
        	});	
        }
        }
        var localId = localAlllArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 0, // 默认为1，显示进度提示
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
                serverAlllArr.push(serverId);
                localAlllArr.shift();
                uploadAllImages(localAlllArr);
                serverStr+=serverId+";";
               // alert("localArr的长度："+localArr.length);
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
                $(".webservice_mask").addClass("mask_none");
            }
        });
    }
	
	
	
	
	</script>
</html>