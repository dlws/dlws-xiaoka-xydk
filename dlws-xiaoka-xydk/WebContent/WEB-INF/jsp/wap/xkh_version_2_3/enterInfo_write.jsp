<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.picker.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/enterInfo.css" />
		<%-- <link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publish.css" /> --%>
		<title>校咖汇</title>
	</head>

	<body>
		<!--头部-->
		<div class="enterInfo_top">
			<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/enterInfo_top.png" class="search_pic" />
		</div>
		<!--内容区域-->
		<form action="<%=basePath%>userSettled/addEnterUser.html" id="entyUser" method="post">
		<section>
			<div class="person_style">
				<span>用户类型</span>
			</div>
			<div class="enterInfo_choose">
				<div class="choose">
					<span class="team"></span>
					<span class="color_green">团队</span>
					<input type="hidden" name="checkType" id="checkType" value="0">
				</div>
				<div class="choose">
					<span class="person"></span>
					<span>个人</span>
					<input type="hidden" name="checkType" id="checkType" value="1">
				</div>
			</div>
			<div class="enterInfo_bg"></div>
			<ul class="message_content">
			<li>
				<span class="enter_name">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span>
				<div id="activeCity" class="mui-input-row">
					<span class="myInfo_name enter_city result-tips">选择您所在的城市</span>
					<span class="myInfo_name show-result">${userInfoMap.cityName}</span>
					<input type="hidden" name="cityId" id="cityId" value="${userInfoMap.cityId}"/>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;校</span>
				<div id="activeShcool" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">选择您所在的学校</span>
					<span class="myInfo_name show-result">${userInfoMap.schoolName}</span>
					<input type="hidden" name="schoolId" id="schooId" value="${userInfoMap.schoolId}"/>
				   <span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
				
				<li>
					<span class="enter_name">入驻名称</span>
					<input class="inp" type="text" id="userName" name="userName" value="${userInfoMap.userName}" placeholder="请填写入驻名称" />
				    <p class="warn_ruzhu">入驻名称最多只可以输入12个字!</p>
				</li>
			
			
				<li>
					<span class="enter_name">入驻头像</span>
					<div class="webservice_content">
						<!--添加的图片-->
						<c:choose>
							<c:when test="${userInfoMap.headPortrait!=''&&userInfoMap.headPortrait!=null}">
								<div class="picture_box" onclick="uploadImage();">
									<div class="add_picture">
										<img id="myAvatar" name="myAvatar" src="${userInfoMap.headPortrait }" class="search_pic"/>
									</div>
								</div>
							 </c:when>
							 <c:otherwise>
								<div class="picture_box" style="display: none;" onclick="uploadImage();">
									<div class="add_picture">
										<img id="myAvatar" name="myAvatar" src="" class="search_pic"/>
									</div>
								</div>
								
								<div class="add_pic" onclick="uploadImage();">
									<span class="iconfont icon-pic"></span>
									<span class="size" >+添加图片</span>
								</div>
							 </c:otherwise>
						</c:choose>
						<!--<添加的图片完-->
						
					</div>
					
					<c:choose>
						<c:when test="${userInfoMap.headPortrait!=''&&userInfoMap.headPortrait!=null}">
							<p class="limit">*点击图片可重新上传</p>
						</c:when>
						<c:otherwise>
							<p class="limit">*请上传头像</p>
						</c:otherwise>
					</c:choose>
					<span class="enter_nameT">主页介绍</span>
					<div class="textarea">
						<div class="textarea_info">
							<textarea class="introduce" id="aboutMeT" name="aboutMe" onkeyup="load()" placeholder="请简述您的团队/个人优势，过往案例等，将展示在您的个人主页哦~">${userInfoMap.aboutMe }</textarea>
						</div>
						<div id="span" class="span"><span>0</span>/200</div>
					</div>
					<span class="enter_nameT">主页标签</span>
					<div class="groom_content">
					<c:forEach items="${Labe}" var="one" varStatus="index">
						<c:forEach items="${labelArr}" var="sel" varStatus="status">
							<c:if test="${one.value==sel}">
								<c:set var="flage" value="trues" scope="request"></c:set>
								<div class="groom groom_green">
									<span class="label04 groom_Green">${one.text}</span>
									<input type="hidden" name="lable" value="${one.value}">
								</div>
							</c:if>
						</c:forEach>
					</c:forEach>
					</div>
						 <span class="add_biaoqian" id="biaoStyle">+</span> 
					<div style="width: 100%;overflow: hidden;">
						<span class="enter_name">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
					<input class="inp" type="text" name="contactUser" id="contactUser" value="${userInfoMap.contactUser }" placeholder="请填写您的姓名" />
					</div>
					
				</li>
				<li>
					<span class="enter_name">联系电话</span>
					<input class="phone inp" type="number"  name="phoneNumber" id="phoneNumber" value="${userInfoMap.phoneNumber }" placeholder="请填写您的手机号码" />
					<p class="warn_tel"></p>
				</li>
				<li>
					<span class="enter_name">联系邮箱</span>
					<input class="letter inp" type="text" name="email" id="email" value="${userInfoMap.email }" placeholder="请填写您的联系邮箱" />
					<p class="warn_letter"></p>
				</li>
				<li>
					<span class="enter_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号</span>
					<input class="wechat inp" type="text" name="wxNumber" id="wxNumber" value="${userInfoMap.wxNumber }" placeholder="请填写您的微信账号" />
					<p class="warn_wechat"></p>
				</li>
				
				<input type="hidden" name="labelId" id="labelId" value="${userInfoMap.labelId }">
				<input type="hidden" name="picUrl" id="picUrl" value="${userInfoMap.headPortrait }">
				<input type="hidden" name="baseId" id="baseId" value="${userInfoMap.id }">
			</ul>
		</section>
		</form>
		<footer class="write_foot" onclick="sub();"><span>提交</span></footer>
		<!--添加主页标签的部分-->
		<div class="school_content school_Content">
			<div class="school_header">
				<span>取消</span>
				<span class="ensure_school">确定</span>
			</div>
			 <div class="school_Box">
					<div class="box_left">
						<div class="left_head border">
							<span class="publishConsult_border left_choose"></span>
							<span>全选</span>
						</div>
						<ul class="left_head" id='city'>
							<c:forEach items="${listLabel}" var="skill">
								<li>
									<span class="publishConsult_Left publishConsult_border"></span>
									<span class="color school_name">${skill.skill.className}</span>
								</li>
							</c:forEach>
						</ul>
					</div>
					<div class="box_right">
						<div class="left_head border">
							<span class="publishConsult_border right_choose"></span>
							<span>全选</span>
						</div>
						<div class="school_box">
							<c:forEach items="${listLabel}" var="Label" varStatus="bs">
								<c:choose>
									<c:when test="${bs.index eq 0}"><ul class="left_head none"></c:when>
									<c:otherwise><ul class="left_head none"></c:otherwise>
								</c:choose>
									<c:forEach items="${Label.label}" var="label">
										<c:set var="mark" value="false"></c:set>
										<c:forTokens delims="," items="${userInfoMap.labelId}" var="userLable">
											<c:if test="${userLable eq label.id}"><c:set var="mark" value="true_1"></c:set></c:if>
										</c:forTokens>

										<c:choose>
											<c:when test="${mark eq 'true_1'}">
												<li>
													<span class="publishConsult_left publishConsult_check"></span>
													<span class="school_name choose_color">${label.labelName}</span>
													<input type="hidden" name="labels" value="${label.id}">
												</li>
											</c:when>
											<c:otherwise>
												<li>
													<span class="publishConsult_left publishConsult_border"></span>
													<span class="color school_name">${label.labelName}</span>
													<input type="hidden" name="labels" value="${label.id}">
												</li>
											</c:otherwise>
										</c:choose>
										
									</c:forEach>
								</ul>
							</c:forEach>
						</div>
					</div>
				</div>
		</div>
		<div class="popup_mask"></div>
		<!--点击是否删除-->
		<div class="remove_box">
			<span class="remove">是否删除?</span>
			<div class="yes_content">
				<span>否</span>
				<span>是</span>
			</div>
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.picker.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.poppicker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/enterInfo.js"></script>
	<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
    <script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script type="text/javascript">

var arr=[];

//默认值
$(document).ready(function(){
	
	var array = $(".school_box .choose_color");
	
	for(var i=0;i<array.length;i++){
		arr.push($(array[i]).html());
	}
	console.log(arr);	
});

//获取当前选中的lable Id
function getLableIdStr(){
	var str = "";
	var array = $(".school_box .publishConsult_check");
	
	for(var i=0;i<array.length;i++){
		if(i==array.length-1){
			str += $(array[i]).siblings("input[name=labels]").val();
			return str;
		}
		str += $(array[i]).siblings("input[name=labels]").val();
		str +=",";
	}
	return str;
}


$(function(){
	var checkType = "${userInfoMap.checkType}";
	if(checkType==1){
		$(".checkType").eq(0).attr("disabled","disabled");
		$(".checkType").eq(1).removeAttr("disabled");
		$(".choose").eq(0).find("span").removeClass("color_green");
		$(".choose").eq(1).find("span").eq(1).addClass("color_green");
		$(".team").css({ "background": "url(${path}/xkh_version_2.3/xkh_version_2.3_1/img/team.png) no-repeat", "background-size": "100% 100%;" });
		$(".person").css({ "background": "url(${path}/xkh_version_2.3/xkh_version_2.3_1/img/person_green.png) no-repeat", "background-size": "100% 100%;" });
	}else{
		$(".checkType").eq(1).attr("disabled","disabled");
		$(".checkType").eq(0).removeAttr("disabled");
	}
	
	var cityId="${userInfoMap.cityId}";
	if(cityId!=""&&cityId){
	   	document.querySelector('#activeShcool .result-tips').style.display = "none";
		document.querySelector('#activeShcool .show-result').style.display = "block"; 
		
	 	document.querySelector('#activeCity .result-tips').style.display = "none";
		document.querySelector('#activeCity .show-result').style.display = "block";  
	}
});
function sub(){
	 //活动图片
	 var label = document.getElementsByName('lable');
	 var s='';
	   for(var i=0; i<label.length; i++){
		   if(label[i].disabled==false){
		   s+=label[i].value+',';  //如果选中，将value添加到变量s中
		   }
	   }
	   //那么现在来检测s的值就知道选中的复选框的值了
	   if(label==''||label.length<1){
		   s="";
	   }else{
		   s=s.substring(0, s.length-1);   
	   }
	   
	   $("#labelId").val(getLableIdStr()); 
	 
	   var schooId = $("#schooId").val();
		if(schooId==""||schooId==null){
			alert("请选择学校");
			return;
		} 
		var userName = $("#userName").val();
		if(userName==""||!userName){
			alert("请输入入驻名称")
			return
		}
		if(userName.length>12){
			alert("入驻名称不得超过12个字")
			return
		}
		var headPortrait = $("#myAvatar")[0].src;
		if(headPortrait==""||headPortrait==null){
			alert("请选择头像");
			return;
		} 

		var aboutMe = $("#aboutMeT").val();
		if(aboutMe==""||aboutMe==null){
			alert("请填写介绍");
			return;
		} 
		
		
	   var labelId = $("#labelId").val();
	   if(labelId==""||!labelId){
		   alert("请选择主页标签");
		   return
	   }
	   
	   var contactUser = $("#contactUser").val(); //联系人
	   if(contactUser==""||!contactUser){
	    	alert("请输入联系人");
	    	return
	   }
	   
	   var phone=$(".phone").val();//电话号码
		//电话验证
  		var re=/^1[3|4|7|5|8]\d{9}$/;
  		if(phone==""||!phone){
  			$(".warn_tel").html("请输入手机号").css("display","block");
  			return;
  		}else if(phone!=""){
   		if(!re.test(phone)){
   			$(".warn_tel").html("输入手机格式有误，请重新输入").css("display","block")
   			return;
   		}else{
   			$(".warn_tel").html("").css("display","none")
   		}
  		}
  		
  		var letter=$(".letter").val();//邮箱
  		//邮箱验证
		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
		if(letter==""||!letter){
			$(".warn_letter").html("请输入邮箱号").css("display","block");
			return;
		}else if(letter!=""&&letter){
			if(!reg.test(letter)){
				$(".warn_letter").html("输入邮箱格式有误，请重新输入").css("display","block");	
				return;
			}else{
				$(".warn_letter").html("").css("display","none");
			}
		} 
		 
	   uploadImageSelf();
}
	//选择所在地
	mui.init();
	var schoolData="";
	var cityPicker = new mui.PopPicker({
				layer: 2
	});
	var shcoolPicker = new mui.PopPicker({
		layer:2
	});
	var city = ${proList};
	cityPicker.setData(city);
	var showCityPickerButton = document.getElementById('activeCity');
	
	showCityPickerButton.addEventListener('tap', function(event) {
		$("#schooId").val("");
		
		cityPicker.show(function(items) {
			
			document.querySelector('#activeCity .result-tips').style.display = "none";
			document.querySelector('#activeCity .show-result').style.display = "block";
			document.querySelector('#activeCity .show-result').innerText = items[1].text;
			$("#cityId").val(items[1].value);
			
			document.querySelector('#activeShcool .result-tips').style.display = "block";
			document.querySelector('#activeShcool .show-result').style.display = "none";  
			//返回 false 可以阻止选择框的关闭
			//return false;
			$.ajax({
     		type : "post",
     		url : "<%=contextPath%>/skillUser/chooseSchool.html",
     		dataType : "json",
     		data : {cityId:items[1].value},
     		success : function(data) {
     			
     			shcoolPicker.setData(data);
     			var showShcoolPickerButton = document.getElementById('activeShcool');
     			showShcoolPickerButton.addEventListener('tap', function(event){
     				$(".mui-backdrop").remove();
 				
     			shcoolPicker.show(function(items) {
     				document.querySelector('#activeShcool .result-tips').style.display = "none";
     				document.querySelector('#activeShcool .show-result').style.display = "block";
     				document.querySelector('#activeShcool .show-result').innerText = items[1].text;
     				$("#schooId").val(items[1].value);
     				//返回 false 可以阻止选择框的关闭
     				//return false;
     			});
     		}, false);
     		}
     	});
		});
	}, false);
	
	
	//选择主页标签
	/* var biaoStyle = new mui.PopPicker({
		layer: 1
	});
	var count = 0;
	var dataList = ${Labelist};
	biaoStyle.setData(dataList);
	var showbiaoStyleButton = document.getElementById('biaoStyle');
	showbiaoStyleButton.addEventListener('tap', function(event) {
		count++;
		biaoStyle.show(function(items) {
			var content = items[0].text;
			var text = "<input type='hidden' id='lable' name='lable' value='"+items[0].value+"'>";
			$("#lablePa").append(text);
			console.log(content)
			var div=document.createElement("div");
			div.setAttribute("class", "groom groom_green");
			var span = document.createElement("span");
			span.setAttribute("class", "label" + count);
	        $(div).append(span)
			$(".groom_content").append(div)
			$(".label" + count).html(content);
			$(".label" + count).addClass("groom_Green");
			
		});
	}, false); */
	
	
	//用户类型选择变化
	$(".enterInfo_choose div").click(function() {
		if($(this).find("span").eq(0).hasClass("person")) {
			$(".team").css({ "background": "url(${path}/xkh_version_2.3/xkh_version_2.3_1/img/team.png) no-repeat", "background-size": "100% 100%;" });
			$(this).siblings().find("span").eq(1).removeClass("color_green");
			$(this).find("span").eq(1).addClass("color_green");
			$(".person").css({ "background": "url(${path}/xkh_version_2.3/xkh_version_2.3_1/img/person_green.png) no-repeat", "background-size": "100% 100%;" });
			$(".checkType").eq(0).attr("disabled","disabled");
			$(".checkType").eq(1).removeAttr("disabled");
		} else {
			$(".team").css({ "background": "url(${path}/xkh_version_2.3/xkh_version_2.3_1/img/team_green.png) no-repeat", "background-size": "100% 100%;" });
			$(this).siblings().find("span").eq(1).removeClass("color_green");
			$(this).find("span").eq(1).addClass("color_green");
			$(".person").css({ "background": "url(${path}/xkh_version_2.3/xkh_version_2.3_1/img/person.png) no-repeat", "background-size": "100% 100%;" });
			$(".checkType").eq(1).removeAttr("disabled");
			$(".checkType").eq(0).attr("disabled","disabled");
		}
	})

	
	
	
	
	//微信上传 全局变量
		var wxSelf;
		var localArr=new Array();
		var serverArr=new Array();
		
		var headFlag = true;
		
		/*001 选择图片*/
		function uploadImage() {
		    var clientUrl = window.location.href;
		    var reqPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : reqPath,
				dataType : 'json',
				data : {
					"clientUrl" : clientUrl
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					/* alert(return_date ); */
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : false, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'chooseImage','uploadImage','downloadImage']
					});
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						wxSelf=wx;
						wx.chooseImage({
						    count: 1, // 默认9
						    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						    success: function (res) {
						    	localArr= res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
						        if(localArr.length >0){
							        var myAvatar=localArr[0];
							        $("#myAvatar").attr("src",myAvatar);
							        $(".picture_box").css('display','block');
							        headFlag=false;
							        $("#addText").html("");
							        $(".add_pic").css('display','none');
							        $(".limit").html("点击图片可重新上传");
							        
						        }
						    }
						});

					});
					wx.error(function(res) {
						alert(res.errMsg);
					});
				}
			});
		}
		
		
		/*002 点击提交后上传到微信*/
		function uploadImageSelf(){
			 //alert("pic count="+localArr.length); 
			 for(var i=0;i<localArr.length;i++){
				 wxSelf.uploadImage({
				        localId: localArr[i], // 需要上传的图片的本地ID，由chooseImage接口获得
				        isShowProgressTips: 1, // 默认为1，显示进度提示
				        success: function (res) {
				            var serverId = res.serverId; // 返回图片的服务器端ID
				            serverArr.push(serverId);
				            
				            if(i == localArr.length){
				            	 //alert("执行上传 count"+serverArr);  
				                var serverStr="";
				                if(serverArr[0].length >0 ){
				    	            serverStr=serverArr[0] ;
				    	           // alert(serverStr);
				    	            $.ajax({
				    		        		type : "post",
				    		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
				    		        		dataType : "json",
				    		        		data : {serverId:serverStr},
				    		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
				    		        			$("#picUrl").val(data.path);
				    		        			$("#entyUser").submit();
				    		        		}
				    		        	});	
				                	
				                }else{
				                	alert("上传头像失败");
				                }
				                            	
				            }
				         }
				    }); 
				 
			 }
			 
			 if(localArr.length==0){
				 $("#entyUser").submit();
			 }
		}
</script>
</html>