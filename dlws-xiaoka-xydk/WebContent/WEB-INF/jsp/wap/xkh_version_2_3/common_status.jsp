<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>


	<!-- 订单状态需要引入的Jsp页面   -->
	<div class="consult_head">
			<c:choose>
				<c:when test="${param.status eq 1}">
					<div class="head_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic"></div>
						<span class="head_right">订单已生成，<span class="timmer">23小时59分59秒</span>后将自动取消</span>
						<input type="hidden" value="${param.time}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
					</div>
					<c:set var="statuStr" value="待付款" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 2}">
					<div class="auditing_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
						<span class="head_right">正在审核内容，通过后将自动投放</span>
					</div>
					<c:set var="statuStr" value="待服务" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 3}">
					<div class="auditing_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
						<span class="head_right">内容已审核通过，届时将自动投放</span>
					</div>
					<c:set var="statuStr" value="待确认" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 4}">
					<div class="already_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
						<span class="head_right">内容已投放</span>
					</div>
					<c:set var="statuStr" value="已完成" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 7}">
					<div class="close_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
						<span class="head_right">您已取消订单，服务已关闭</span>
					</div>
					<c:set var="statuStr" value="已关闭" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 11 || param.status eq 7 || param.status eq 5}">
					<div class="fail_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
						<span class="head_right">内容审核不通过，退款将在一个工作日内退回到您的账户</span></span>
					</div>
					<c:set var="statuStr" value="待退款" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 8}">
					<div class="success_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
						<span class="head_right">退款成功，请留意查收</span></span>
					</div>
					<c:set var="statuStr" value="已退款" scope="session"></c:set>
				</c:when>
			</c:choose>
	</div>
