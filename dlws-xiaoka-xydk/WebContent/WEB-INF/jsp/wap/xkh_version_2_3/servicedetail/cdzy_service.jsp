<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>校咖汇-技能详情-场地资源</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/swiper-3.3.1.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publishService.css" />
		<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	</head>
	<%@ include file="/WEB-INF/jsp/wap/xkh_version_2/allshare.jsp"%>
	<body>
	
		<div class="person_top">
			<img src="${listClose[0]}" class="search_pic">
			<div class="eye_symbol">
			<span><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/eye.png" class="search_pic"></span>
			<span>${userBasic.viewNum}</span>
			</div>
		</div>
		<!--跳转回个人详情页-->
		<a href="${path}/personInfoDetail/detail.html?id=${userBasic.id}" class="personal_content">
			<div class="personal_left">
				<img src="${userBasic.headPortrait}" class="search_pic" />
			</div>
			<div class="personal_right" style="float:right;">
				<p>${userBasic.nickname}</p>
				<div class="ctt_addresContent">
				<span class="personal_college">${userBasic.schoolName}</span>
				<div class="ctt_addres">
				<span class="personal_zuobiao"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/place.png" class="search_pic" /></span>
				<span class="personal_city">${userBasic.cityName}</span>
				</div>
				</div>
			</div>
		</a>
		<div class="enterInfo_bg"></div>
		<!--精准咨询，精准投放出来的部分-->
		<div class="ablity">
			<h3>${skillInfo.fathName}</h3>
		</div>
		<!--内容区域-->
		<!--精准咨询部分-->
		<div class="detail_content">
			<p>${skillInfo.sonName}</p>
			<p>服务介绍:${skillInfo.skillDepict}</p>
		</div>
		<!--这一部分为精准咨询，精准投放，社团服务，技能类型所特有-->
		<div class="place_detail">
			<div class="detail_service">
				<div class="detail_picture">
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/service_orange.png" class="search_pic">
				</div>
				<span class="detail_style place_style">服务性质&nbsp;:</span>
				<span class="detail_line">
				<c:choose>
					<c:when test="${skillInfo.serviceType eq 1}">线上</c:when>
					<c:otherwise>线下</c:otherwise>
				</c:choose>
				</span>
			</div>
			<div class="detail_service">
				<div class="detail_picture">
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/place_orange.png" class="search_pic">
				</div>
				<span class="detail_style place_style">场地规模&nbsp;:</span>
				<span class="detail_line">
					<c:forEach items="${filedSizeList}" var="list">
						<c:if test="${list.id eq detailMap.filedSize}">
							${list.dic_name}
						</c:if>
					</c:forEach>
				</span>
				
			</div>
			<div class="detail_service">
				<div class="detail_picture">
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/resource_orange.png" class="search_pic">
				</div>
				<span class="detail_style">基本物资配置&nbsp;:</span>
				<span class="detail_line">
					<c:forTokens delims="," items="${detailMap.material}" var="primer" varStatus="bs">
						<c:forEach items="${materialList}" var="material">
							<c:if test="${primer eq material.id}">
								<c:choose>
									<%-- 当当前循环不为最后一位的时候 --%>
									<c:when test="${bs.count != materTime}">${material.dic_name},</c:when>
									<c:otherwise>${material.dic_name}</c:otherwise>
								</c:choose>
							</c:if>
						</c:forEach>
					</c:forTokens>
				</span>
			</div>
			<div class="detail_service">
				<div class="detail_picture">
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/time_orange.png" class="search_pic">
				</div>
				<span class="detail_style">场地使用时间&nbsp;:</span>
				<span class="detail_line">${detailMap.beginDate}-${detailMap.endDate}</span>
			</div>
			<div class="detail_service">
				<div class="detail_picture">
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/person_orange.png" class="search_pic">
				</div>
				<span class="detail_style">是否提供兼职&nbsp;:</span>
				<span class="detail_line">
					<c:forEach items="${partimejobList}" var="partime">
						<c:if test="${partime.dic_value eq detailMap.partimejob}">
							${partime.dic_name}
						</c:if>
					</c:forEach> 
				</span>
			</div>
			<div class="detail_service">
				<div class="detail_picture">
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/service_price.png" class="search_pic">
				</div>
				<span class="detail_style place_style">服务价格&nbsp;:</span>
				<span class="detail_line">${detailMap.skillPrice}/${unit}</span>
				<span class="detail_number">已售[${skillInfo.sellNo}]</span>
			</div>
		</div>

		<!--为整个详情页所共有-->
		<c:if test="${isMySelf eq false}">
			<div class="about_box detail_box">
				<a href="javascript:void(0)" onclick="joinse(this)">加入已选</a>
				<a href="<%=contextPath%>/skillOrder/toBuySkill.html?skillId=${skillInfo.id}">立即下单</a>
				<input type="hidden" value="${skillInfo.id}" name="skillId">
				<input type="hidden" value="${userBasic.openId}" name="openId_sel">
				<input type="hidden" value="0" name="isPreferred">
				<input type="hidden" value="${skillInfo.fathValue}" name="enterType">
			</div>
		</c:if>
		<!-- 06-19加入已选弹窗 -->
	<div class="popupWindow_content">
       	<div class="popupWindow_title">
       		<span class="popupWindow_close"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/cha.png" alt="" class="search_pic" /></span>
       		<span class="poopupWindow_choose">已选</span>
       		<a class="popupWindow_car" href="${path}/skillOrder/SelectedList.html">
       		   <img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/buyCar.png" class="search_pic" />
       		   <img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/redCar.png" class="redCar_pic" />
       		</a>
       	</div>
       	<ul class="poopupWindow_box">
       		<c:forEach items="${skill_list }" var="skill">
	       		<li>
	       			<span class="queen"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/queen.png" class="search_pic" /></span>
	       			<div class="popupWindow_message">
	       				<span>${skill.skillName }</span>
	       				<span>${skill.skillDepict }</span>
	       			</div>
	       			<div class="popupWindow_number">
	       				<span class="reduce">-</span>
	       				<span class="value">${skill.buyNum }</span>
	       				<span class="Add">+</span>
	       			</div>
	       			<input type="hidden" value="${skill.seledId}" name="seledId">
	       		</li>
       		</c:forEach>
       	</ul>
       	<span class="popupWindow_more popupWindow_up"></span>
       </div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/swiper-3.3.1.jquery.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/publishService/detail.js"></script>
	<script type="text/javascript">
	 var baseP = "<%=basePath%>";
	 var path = "<%=basePath%>";
	</script>
	<script src="${path}/xkh_version_2.2/js/commen.js"></script>  
</html>
