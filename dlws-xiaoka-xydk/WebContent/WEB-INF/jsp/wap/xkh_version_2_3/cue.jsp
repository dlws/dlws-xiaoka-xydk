<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.3/css/common.css" />
		<title>提示</title>
	</head>
	<body>
		<div class="cue_content">
			<img src="${path}/xkh_version_2.3/img/cue.png" class="search_pic" />
			<!--发布提交提示页面-->
			<p>已提交成功</p>
			<a href="javascript:void(0);">正在为您跳转回首页...</a>
		</div>
	</body>
	<script type="text/javascript">
		onload=function(){ 
			setTimeout(go, 500); 
		}; 
		function go(){
			location.href="${path}/home/index.html"; 
		}
	</script>
</html>

