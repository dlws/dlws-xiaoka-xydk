<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.min.css" />
	
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.picker.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publish.css" />
		<title>发布-精准投放</title>
	</head>
	<body>
		<div class="publishConsult_Box">
			<header class="publish_demandHead none_border">
				<img src="${banner.picUrl }" class="search_pic" />
			</header>
			<section>
			<form id="formConsult" action="<%=basePath%>publish/publishPut.html" method="post">
				<div class="publish_bg"></div>
				<input type="hidden" name="resources" id="resources" /><!-- 筛选---选择资源 -->
				<input type="hidden" name="city" id="city"/><!-- 筛选---选择城市 -->
				<input type="hidden" name="label" id="label"/><!-- 筛选---选择院校级别 -->
				<input type="hidden"  value="" id="resultResources">
				<ul class="publishConsult_content">
					<li>
						<span class="demand_title">发布类型</span>
					</li>
					</ul>
					<div class="cttBox_content">
						<c:forEach items="${listType }" var="publish" varStatus="i">
							<a href="javascript:void(0);" onclick="changePage('${publish.classValue }')" class="demand_content">
								<c:choose>
									<c:when test="${i.index == 2 }">
										<span><img src="${publish.selectimage }" class="search_pic" /></span>
										<span class="green">${publish.className }</span>
									</c:when>
									<c:otherwise>
										<span><img src="${publish.noSelectimage }" class="search_pic" /></span>
										<span>${publish.className }</span>
									</c:otherwise>
								</c:choose>
							</a>
						</c:forEach>
					</div>
				
				<div class="filter_box">
					<div class="filter">
						<span>筛选</span>
						<span class="filter_pic"></span>
					</div>
					<div class="Filter">
						<span>筛选</span>
						<span class="filter_pic"></span>
					</div>
					<div class="pulishConsult_choose">
						<input placeholder="" name="userName" value="${userName }" id="searchInfo" type="text" />
					</div>
					<span class="publishConsult_search" id="search" >搜索</span>
				</div>
				<div class="selected_bg"></div>
				<ul class="publishConsult_box">
					<c:forEach items="${infoList }" var="info">
						<li>
							<div class="publishConsult_left publishConsult_border publishConsult_Left">
								<input type="hidden" name="id" value="${info.id }"/>
								<input type="hidden" name="headPortrait" value="${info.headPortrait }"/>
								<input type="hidden" name="openId" value="${info.openId }"/>
								<input type="hidden" name="skillId" value="${serverMap.id }"/>
								<input type="hidden" name="enterType" value="jztf"/>
								<input type="hidden" name="schoolId" value="${info.schoolId }"/>
								<input type="hidden" name="cityId" value="${info.cityId }"/>
								<input type="hidden" name="imgUrl" value="${serverMap.imgUrl }"/>
								<input type="hidden" name="serviceName" value="${serverMap.serviceName }"/>
								<input type="hidden" name="serviceType" value="${serverMap.serviceType }"/>
								<input type="hidden" name="price" value="${serverMap.price }"/>
								<input type="hidden" name="sellerOpenId" value="${info.openId }"/>
								<input type="hidden" name="nickname" value="${info.nickname }"/>
								<input type="hidden" name="phoneNumber" value="${info.phoneNumber }"/>
								<input type="hidden" name="salesVolume" value="${serverMap.salesVolume }"/>
							</div>
							<div class="publishConsult_right">
								<a href="${path}/skill_detail/getskill_detail.html?skillId=${serverMap.id }&classValue=jztf&personId=${info.id }">
								<div class="publishConsult_title">
									<span class="title_picture"><img src="${info.headPortrait }" class="search_pic" /></span>
									<h3>${info.userName }</h3>
								</div>
								<div class="publishConsult_photo"><img src="${serverMap.imgUrl }" class="search_pic" /></div>
								<div class="substance_Right publishConsult_Right">
									<div class="music_content">
										<span class="substance_Music publishConsult_music">${serverMap.serviceName }</span>
										<span class="substance_Line publishConsult_line">
											<c:choose>
												<c:when test="${serverMap.serviceType == 1 }">
													<span>线上</span>
												</c:when>
												<c:otherwise>
													<span>线下</span>
												</c:otherwise>
											</c:choose>
										</span>
									</div>
									<div class="price_Box">
										<span class="substance_price publishConsult_price"><span class="publishconsult_price">${serverMap.price }</span>元/次</span>
										<span class="sell_Num publishConsult_Num">[已售${serverMap.salesVolume }]</span>
									</div>
									<span class="publishConsult_school">${info.schoolName }</span>
									<div class="publishConsult_address">
										<img class="address_photo" src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/place.png" />
										<span>${info.cityName }</span>
									</div>
								</div>
								</a>
							</div>
						</li>
					</c:forEach>
				</ul>
			</form>
			</section>
			<footer class="publishConsult_foot">
				<div class="publishConsult_left publishConsult_border foot_border"></div>
				<div class="publishConsult_total">
					<span class="total_choose">全选<span class="chooseNum">已选<span class="choose_num">0</span>个资源</span></span>
					<span class="summation">合计&nbsp;:&nbsp;<span class="total_price">￥0元</span></span>
				</div>
				<div class="publishConsult_add"><span id="addSelected">加入已选</span></div>
				<div class="publishConsult_add add_bgColor"><span id="putNow">立即投放</span></div>
			</footer>
		</div>
		<!--点击筛选出来的-->
		<div class="popup_mask"></div>
		<div class="filter_content">
			<ul class="information_content publish_Demand publishConsult_Border">
				<li>
					<span class="publish_name">咨询类型</span>
					<div id="resourceStyle" class="mui-input-row">
					<span class="publish_jia publish_style publishConsult_style result-tips">选择资源类型</span>
					<span class="publish_jia publish_style publishConsult_style show-result"></span>
					<span class="publish_right"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right.png" class="search_pic" /></span>
					</div>
				</li>
				<li>
					<span class="publish_name">所&nbsp;在&nbsp;地</span>
					<div id="chooseCity" class="mui-input-row">
						<span class="publish_jia result-tips">请选择资源所在地</span>
						<span class="publish_jia show-result" id="resultCity"></span>
						<span class="publish_right"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right.png" class="search_pic" /></span>

					</div>
				</li>
				<li>
					<span class="publish_name">院校等级</span>
					<div id="schoolStyle" class="mui-input-row">
						<span class="publish_jia result-tips">请选择资源院校等级</span>
						<span class="publish_jia show-result" id="resultLabel"></span>
						<span class="publish_right"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right.png" class="search_pic" /></span>
				   </div>
				</li>
			</ul>
			<div class="filter_foot">
				<span class="add_bgcolor">重置</span>
				<span class="add_bgColor" id="makeSure">确定</span>
			</div>
		</div>
		<!--资源类型一级-->
       <%--  <div class="webservice_box2">
			
			<ul class="webservice_styleContent">
				<p>一级服务</p>
				<c:forEach items="${cateList }" var="category">
					<li>
						<span>${category.className }</span>
						<input type="hidden" value="${category.id }"/>
					</li>
				</c:forEach>
			</ul>
		
			<ul class="webservice_styleContent1" id="secondCategory">
				
			</ul>
			<form class="enter_form webservice_form" onsubmit="return false">
				<button>确认</button>
			</form>
		</div> --%>
	</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.picker.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/city.data.js" ></script>
	
	<script type="text/javascript">
		var dateSource = ${cateList};
		function changePage(val){
			if(val == 'jzxq'){
				window.location.href = "${path}/publish/publishDemand.html";
			}else if(val == 'jzzx'){
				window.location.href = "${path}/publish/publishConsult.html"
			}else if(val == 'jztf'){
				
			}
		}
		//搜索框
		$("#search").click(function(){
			$("#formConsult").submit();
		});
		//点击二级分类
		$(document).on("click",".webservice_styleContent1 li",function(){
			var index = $(this).index();
			$("span", this).addClass("webservice_color");
			$(this).siblings().find("span").removeClass("webservice_color");
			style = $(this).text();
			$(".webservice_form").click(function() {
				$(this).css("background", "#1D9243");
				$("button", this).css("background", "#1D9243");
				setTimeout(function() {
					$(".webservice_box2").hide();
					$(".webservice_box1").show();
					$("section").show();
					$("footer").show();
					$(".popup_mask").show();
					$(".filter_content").show()
					$(".publish_style").html(style);

				}, 1000)
			})
		});
		//条件查询
		$("#makeSure").click(function(){
			var resultResources = $("#resultResources").val();
			var resultCity = $("#resultCity").text();
			var resultLabel = $("#resultLabel").text();
			var resources = "";
			var city = "";
			var label = "";
			if(resultResources != "选择资源类型"){
				resources = resultResources;
			}
			if(resultCity != "请选择资源所在地"){
				city = resultCity;
			}
			if(resultLabel != "请选择资源院校等级"){
				label = resultLabel;
			}
			$("#resources").val(resources);
			$("#city").val(city);
			$("#label").val(label);
			$("#formConsult").submit();
		});
		//加入到已选
		$("#addSelected").click(function(){
			var data = $('.publishConsult_check');
			var obj = [];
			if(data.length > 0){
			    for(var i =0;i<data.length;i++){
				    var child = data[i].children;
				    var json = {};
				    for(var j = 0;j<child.length;j++){
					   var name = child[j].name;
					   var val = child[j].value;
					   json[name] = val;
				    }
				    obj.push(json);
			    }
			    obj = JSON.stringify(obj);
			    $.ajax({
					url :'${path}/publish/addPreferr.html',
					data :{
						"obj":obj
					},
					type :'post',
					cache :false,
					dataType :'json',
					success : function(data) {
						var flag = data.flag;
						if(flag){
							alert("加入已选成功");
						}else{
							alert("加入已选失败");
						}
					},
					error : function() {
						alert("异常！");
					}
				});
			}else{
				alert("请选择服务!");
			}
		});
		//立即投放
		$("#putNow").click(function(){
			var data = $('.publishConsult_check');
			var obj = [];
			if(data.length > 0){
				for(var i =0;i<data.length;i++){
				    var child = data[i].children;
				    var json = {};
				    for(var j = 0;j<child.length;j++){
					   var name = child[j].name;
					   var val = child[j].value;
					   if(name == "sellerOpenId" || name == "cityId" || name == "id" ||
							   name == "schoolId" || name == "imgUrl" || name == "headPortrait" ||
							   name == "serviceName" || name =="serviceType" || name == "salesVolume" ||
							   name == "price" || name == "nickname" || name == "phoneNumber"){
						   json[name] = val;
					   }
				    }
				    obj.push(json);
			    }
				obj = JSON.stringify(obj)
				var price = $('.total_price').text();
				price = price.substring(1,price.length-1);
				var f = document.createElement("form");
				document.body.appendChild(f);
				var i = document.createElement("input");
				i.type = "hidden";
				f.appendChild(i);
				i.value = price;
				i.name = "price";
				var j = document.createElement("input");
				j.type = "hidden";
				f.appendChild(j);
				j.value = obj;
				j.name = "obj";
				f.method="post";
				f.action = "${path}/publish/toPutOrder.html";
				f.submit();
			}else{
				alert("请选择服务!");
			}
		});
	</script>
<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/publish/publish_consult.js" ></script>
</html>

