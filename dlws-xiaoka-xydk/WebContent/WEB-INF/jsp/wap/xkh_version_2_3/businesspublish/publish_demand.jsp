<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.picker.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.dtpicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publish.css" />
		
		<title>发布-精准需求</title>
	</head>

	<body>
		<header class="publish_demandHead">
			<img src="${banner.picUrl }" class="search_pic" />
		</header>
		<div class="webservice_box1">
			<input type="hidden" value="" id="resourceType">
			<section>
				<ul class="information_content publish_Demand">
					<li>
						<span class="demand_title">发布类型</span>
					</li>
				</ul>	
					<div class="cttBox_content">
						<c:forEach items="${listType }" var="publish" varStatus="i">
							<a href="javascript:void(0);" onclick="changePage('${publish.classValue }')" class="demand_content">
								<c:choose>
									<c:when test="${i.index == 0 }">
										<span><img src="${publish.selectimage }" class="search_pic" /></span>
										<span class="green">${publish.className }</span>
									</c:when>
									<c:otherwise>
										<span><img src="${publish.noSelectimage }" class="search_pic" /></span>
										<span>${publish.className }</span>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${publish.classValue == 'jzxq'}">
										<input type="hidden" id="jzxq" value="${publish.id }" />
										<input type="hidden" id="classValue" value="${publish.classValue }" />
									</c:when>
								</c:choose>
							</a>
						</c:forEach>
					</div>
					<div class="selected_bg"></div>
					<ul class="information_content publishDemand">
					<li>
						<span class="publish_name">咨询类型</span>
						<div class="choose_webservice mui-input-row" id="resourceStyle">
							
					<span class="publish_jia publish_style publishConsult_style result-tips">选择资源类型</span>
					<span class="publish_jia publish_style publishConsult_style show-result"></span>
					<span class="publish_right"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right.png" class="search_pic" /></span>
					
						</div>
					</li>
					<li>
						<span class="publish_name">活动时间</span>
						<span id="time" class="publish_time btn mui-btn mui-btn-block" data-options='{"type":"date","beginYear":2017,"endYear":2022}'>2017-04-12</span>
						<span class="publish_line">至</span>
						<span id="time2" class="publish_time btn mui-btn mui-btn-block" data-options='{"type":"date","beginYear":2017,"endYear":2022}'>2017-04-18</span>
					</li>
					<li>
						<span class="publish_name">活动城市</span>
						<span class="publish_jia school_city" id="city">选择活动城市</span>
						<span class="publish_right"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right.png" class="search_pic" /></span>
					</li>
					<li>
						<span class="publish_name publish_school">活动院校<span class="publish_choose">(选填)</span></span>
						<span class="publish_jia choose_school" id="school">请选择活动落地院校</span>
						<span class="publish_right"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right.png" class="search_pic" /></span>
					</li>
					<li>
						<span class="publish_name">投入量级</span>
						<%-- <div id="publishDemand_price" class="mui-input-row">
							<span class="publish_jia result-tips">选择活动投入的资金</span>
							<span class="publish_jia show-result" id="money"></span>
							<span class="publish_right"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right.png" class="search_pic" /></span>
						</div> --%>
						<div class="nature_box">
					        <div class="nature">
					         	<span class="nature_choose"></span>
						        <span class="groom_Green">1000</span>
					       </div>
					       <div class="nature">
						       <span></span>
						       <span>2000</span>
					       </div>
					       <div class="nature">
						       <span></span>
						       <span>3000</span>
					       </div>
				       </div>
					</li>
					<li>
						<span class="publish_name">预期产出</span>
						<div class="textarea">
					       <div class="textarea_info" style="margin-bottom:20px;">
						     <textarea class="Introduce publish_introduce" name="aboutMe" id="output" onkeyup="load()" onkeydown="load()" placeholder="请填写活动预期效果"></textarea>
					       </div>
					       <div id="span" class="Span publish_span"><span>0</span>/200</div>
				        </div>
					</li>
					</ul>
					
					<div class="selected_bg"></div>
					<ul class="information_content publishDemand">
					<li>
						<span class="publish_name">您的姓名</span>
						<input type="text" id="name" placeholder="填写您的姓名" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" class="publish_jia" />
					</li>
					<li>
						<span class="publish_name">您的电话</span>
						<input type="number" id="phone" placeholder="填写您的联系方式" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" class="publish_jia phone" />
						<p class="warn_tel"></p>
					</li>
					<li>
						<span class="publish_name">您的公司</span>
						<input type="text" id="company" placeholder="填写您的公司全称" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" class="publish_jia" />
					</li>
					<li>
						<span class="publish_name publish_school">备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注&nbsp;<span class="publish_choose">(&nbsp;选填&nbsp;)</span></span>
						
						<div class="textarea publish_textarea">
					       <div class="textarea_info">
						     <textarea class="Introduce2 publish_introduce" name="aboutMe" id="note" onkeyup="load2()" onkeydown="load2()" placeholder=""></textarea>
					       </div>
					        <div id="span2" class="Span publish_span"><span>0</span>/200</div>
				        </div>
					</li>
				</ul>
			</section>
			<a href="javascript:void(0);" onclick="submitDem();" class="footer"><span>提交</span></a>
		</div>
		<!--遮罩层-->
		<div class="webservice_mask mask_none"></div>
         
		<!--弹窗选择城市-->
		<div class="popup_mask"></div>		
		<div class="school_content city_content">
			<div class="school_header">
				<span>取消</span>
				<span class="ensure_city">确定</span>
			</div>
			<div class="school_Box">
				<div class="box_left">
					<div class="left_head border">
						<span class="publishConsult_border left_choose"></span>
						<span>全选</span>
					</div>
					<ul class="left_head">
						<c:forEach items="${cityList }" var="city">
							<li>
								<span class="publishConsult_Left publishConsult_border"></span>
								
								<span class="color school_name">${city.province }</span>
								<input type="hidden" value="${city.id }">
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="box_right">
					<div class="left_head border">
						<span class="publishConsult_border right_choose"></span>
						<span>全选</span>
					</div>
					<div class="school_box">
						<c:forEach items="${cityList }" var="city">
							<ul class="left_head none">
								<li>
									<span class="publishConsult_left publishConsult_border"></span>
									<span class="color school_name">${city.cityName }</span>
									<input type="hidden" value="${city.id }" name="city_id">
								</li>
							</ul>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
		<!--选择学校-->
         <div class="school_content school_Content">
			<div class="school_header">
				<span>取消</span>
				<span class="ensure_school">确定</span>
			</div>
			<div class="school_Box">
				<div class="box_left">
					<div class="left_head border">
						<span class="publishConsult_border left_choose"></span>
						<span>全选</span>
					</div>
					<ul class="left_head">
						<c:forEach items="${labelList }" var="label">
							<li>
								<span class="publishConsult_Left publishConsult_border"></span>
								<span class="color school_name">${label.dic_name }</span>
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="box_right">
					<div class="left_head border">
						<span class="publishConsult_border right_choose"></span>
						<span>全选</span>
					</div>
					<div class="school_box">
						<!--这个985/211-->
						<ul class="left_head none">
							<c:forEach items="${nineList }" var="school">
								<li>
									<span class="publishConsult_left publishConsult_border"></span>
									<span class="color school_name">${school.schoolName }</span>
								</li>
							</c:forEach>
						</ul>
						<!--本科-->
						<ul class="left_head none">
							<c:forEach items="${gradList }" var="school">
								<li>
									<span class="publishConsult_left publishConsult_border"></span>
									<span class="color school_name">${school.schoolName }</span>
								</li>
							</c:forEach>
						</ul>
						<!--专科-->
						<ul class="left_head none">
							<c:forEach items="${specList }" var="school">
								<li>
									<span class="publishConsult_left publishConsult_border"></span>
									<span class="color school_name">${school.schoolName }</span>
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.picker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.dtpicker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.poppicker.js"></script>
	
<script type="text/javascript">

	var path = "${path}";
	var dateSource = ${cateList};
	$(document).on("click",".webservice_styleContent1 li",function(){
		var index = $(this).index();
		$("span", this).addClass("webservice_color");
		$(this).siblings().find("span").removeClass("webservice_color");
		style = $(this).text();
		$(".webservice_form").click(function() {
			$(this).css("background", "#1D9243");
			$("button", this).css("background", "#1D9243");
			setTimeout(function() {
				$(".webservice_box2").hide();
				$(".webservice_box1").show();
				$(".publish_reverse").html(style);
				//					$(".popup_mask").hide()
			}, 1000)
		})
	});
	function changePage(val){
		if(val == 'jzxq'){
			
		}else if(val == 'jzzx'){
			window.location.href = "${path}/publish/publishConsult.html";
		}else if(val == 'jztf'){
			window.location.href = "${path}/publish/publishPut.html"
		}
	}
	//获取当前页面所有选中的学校
	function getSelectSchool(){
		
		var schoolContent = "";
		var schoolArray = $(".school_Content .school_box .choose_color");
		for(var i = 0;i<schoolArray.length;i++){
			
			if(i==schoolArray.length-1){
				schoolContent += $(schoolArray[i]).html();
				return schoolContent;
			}
			schoolContent +=$(schoolArray[i]).html()+",";
		}
	}
	//获取当期页面所有选中的城市
	function getSelectCity(){
		
		var schoolContent = "";
		var schoolArray = $(".city_content .school_box .choose_color");
		for(var i = 0;i<schoolArray.length;i++){
			
			if(i==schoolArray.length-1){
				schoolContent += $(schoolArray[i]).html();
				return schoolContent;
			}
			schoolContent +=$(schoolArray[i]).html()+",";
		}
	}
	
	
	//点击提交
	function submitDem(){
		
		var typeId = $("#jzxq").val();
		var typeValue = $("#classValue").val();
		var resourceType = $("#resourceType").val();
		var time = $("#time").text();
		var time2 = $("#time2").text();
		var city = getSelectCity();
		var school = getSelectSchool();
		var money = $(".groom_Green").text();
		var output = $("#output").val().trim();
		var name = $("#name").val();
		var phone = $("#phone").val();
		var company = $("#company").val();
		var note = $("#note").val();
		if(resourceType == "选择资源类型"){
			alert("选择资源类型!");
			return ;
		}
		var d1 = new Date(time.replace(/\-/g, "\/"));  
		var d2 = new Date(time2.replace(/\-/g, "\/")); 
		if(d1 > d2){
			alert("开始时间不能大于结束时间!");
			return ;
		}
		if(city == "选择活动城市"){
			alert("请选择活动城市!");
			return;
		}
		if(school == "请选择活动落地院校"){
			alert("请选择活动落地院校!");
			return;
		}
		if(money == ""){
			alert("选择活动投入的资金!");
			return ;
		}
		if(output == ""){
			alert("请填写预期产出!");
			return;
		}
		if(name == ""){
			alert("请填写名字!");
			return ;
		}
		if(phone == ""){
			alert("请填写电话号!");
			return ;
		}
		if(company == ""){
			alert("请填写公司名!");
			return ;
		}
		$.ajax({
			url : 'addJzxq.html',
			data : {
				"typeId" : typeId ,
				"typeValue" : typeValue ,
				"resourceType" : resourceType ,
				"time" : time ,
				"time2" : time2 ,
				"city" : city ,
				"school" : school ,
				"money" : money ,
				"output" : output ,
				"name" : name ,
				"phone" : phone ,
				"company" : company ,
				"note" : note
			},
			type : 'post',
			cache : false,
			dataType : 'json',
			success : function(data) {
				if(data){
					//如果成功跳转到成功页面
					window.location.href = "cue.html";
				}
			},
			error : function() {
				alert("异常！");
			}
		});
	}
</script>
<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/publish/publish_demand.js"></script>
</html>
