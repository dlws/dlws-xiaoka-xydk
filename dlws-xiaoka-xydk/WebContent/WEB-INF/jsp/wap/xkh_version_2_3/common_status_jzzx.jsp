<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>


	<!-- 订单状态需要引入的Jsp页面   -->
	<div class="consult_head">
			<c:choose>
				<c:when test="${param.status eq 1}">
					<div class="consult_head">
						<div class="head_Content">
							<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
							<span class="head_right">等待卖家接单，<span class="timmer">23小时59分59秒</span>后将自动关闭</span>
							<input type="hidden" value="${param.time}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
						</div>
					</div>
					<c:set var="statuStr" value="待确认" scope="session"></c:set>
					<c:set var="hint" value="已购买咨询权限，卖家接单后可直接咨询" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 3|| param.status eq 0}">
					<div class="consult_head">
						<div class="buyer_Content3">
							<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic"></div>
							<span class="head_right">已有卖家接单</span>
						</div>
					</div>
					<c:set var="statuStr" value="待服务" scope="session"></c:set>
					<c:set var="hint" value="已接单的卖家可直接沟通，咨询权限保留24小时" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 4}">
					<div class="already_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic"></div>
						<span class="head_right">服务已完成</span>
					</div>
					<c:set var="statuStr" value="已完成" scope="session"></c:set>
					<c:set var="hint" value="咨询权限已到期，如需咨询请重新下订单" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 7}">
					<div class="headerBuy_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/plaint.png" class="search_pic"></div>
						<span class="head_right">您已取消订单，退款将在一个工作日内退回您的账户</span>
					</div>
					<c:set var="statuStr" value="待退款" scope="session"></c:set>
					<c:set var="hint" value="正在为您处理退款，如需咨询请重新下单" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 5}">
					<div class="headerSell_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
						<span class="head_right">部分卖家拒绝接单，退款将在一个工作日内退回您的账户</span></span>
					</div>
					<c:set var="statuStr" value="待退款" scope="session"></c:set>
					<c:set var="hint" value="正在为您处理退款，如需咨询请重新下单" scope="session"></c:set>
				</c:when>
				<c:when test="${param.status eq 8}">
					<div class="success_content">
						<div class="head_left"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png" class="search_pic" /></div>
						<span class="head_right">退款成功，请留意查收</span></span>
					</div>
					<c:set var="statuStr" value="已退款" scope="session"></c:set>
					<c:set var="hint" value="如需咨询请重新下单" scope="session"></c:set>
				</c:when>
			</c:choose>
	</div>
