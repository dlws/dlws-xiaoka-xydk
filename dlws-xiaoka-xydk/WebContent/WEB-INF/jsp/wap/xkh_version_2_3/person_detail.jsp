<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fs"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/swiper-3.3.1.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/person_detail.css" />
		<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<title>校咖汇</title>
	</head>

	<body>
		<div class="person_top">
			<c:choose>
				<c:when test="${userInfo.backPicture!=''&&userInfo.backPicture!=null}">
						<c:if test="${isMyself==true}">
							<img id="backPicUrl" src="${userInfo.backPicture}${person_top}"
								class="search_pic" onclick="changeBack();"/>
						</c:if>
						<c:if test="${isMyself==false}">
							<img id="backPicUrl" src="${userInfo.backPicture}${person_top}"
								class="search_pic"/>
						</c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${isMyself==true}">
						<img id="backPicUrl" src="${path}/xkh_version_2.1/img/per_banner01.jpg"
							class="search_pic" onclick="changeBack();" />
					</c:if>
					<c:if test="${isMyself==false}">
						<img id="backPicUrl" src="${path}/xkh_version_2.1/img/per_banner01.jpg"
							class="search_pic"/>
					</c:if>
				</c:otherwise>
			</c:choose>
			<div class="personal_pic">
				<img src="${userInfo.headPortrait}" class="search_pic" style="border-radius:100%;" />
			</div>
			<p class="personal_name">${userInfo.nickname}</p>
			<p class="personal_school">${userInfo.schoolName}</p>
			<span class="person_zuobiao"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/place.png" class="search_pic"></span>
			<span>${userInfo.cityName}</span>
			<div class="eye_symbol">
			<span><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/eye.png" class="search_pic" /></span>
			<span>${userInfo.viewNum}</span>
			</div>
		</div>
		<div class="label_style">
			<c:forEach items="${labelList}" var="label">
				<div class="tag"><span>${label}</span></div>
			</c:forEach>
		</div>
		<div class="enterInfo_bg"></div>
		<!--关于他-->
		<div class="person_about">
			<div class="about_line"></div>
			<span class="about_he"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/person_about.png" class="search_pic" /></span>
			<span class="about_him">关于TA</span>
			<div class="about_lineR"></div>
		</div>
		<div class="introduce_new">
			<div class="about_content p" id="box">${userInfo.aboutMe}</div>
			<span class="watch"></span>
		</div>
		<div class="enterInfo_bg"></div>
		<!--关于服务-->
		<div class="person_about">
			<div class="about_line"></div>
			<span class="about_he"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/person_service.png" class="search_pic" /></span>
			<span class="about_him">优选服务</span>
			<div class="about_lineR"></div>
		</div>
		<%-- 判定当前增加下划线的类名 --%>
		<c:choose>
			<c:when test="${fn:length(preferList) eq 1}">
				<c:set var="about_box" value="about_box none_border"/>
			</c:when>
			<c:otherwise>
				<c:set var="about_box" value="about_box"/>
			</c:otherwise>
		</c:choose>
		<c:forEach var="prefer" items="${preferList}">
		<div class="person_box">
			<a href="${path}/skill_detail/getskill_detail.html?skillId=${prefer.id}&classValue=${prefer.enterTypeValue}&personId=${userInfo.id}" class="skip">
					<c:choose>
						<c:when test="${prefer.enterTypeValue =='jzzx'}">
							<div class="pic_content">
						</c:when>
						<c:otherwise>
							<div class="pic_content" style="margin-top:20px;">
						</c:otherwise>
					</c:choose>
					<div class="about_pic">
						<img src="${prefer.imgUrl}" class="search_pic" />
						<span class="consult"><span>${prefer.serviceName}</span></span>
					</div>
				</div>
				<div class="about_sell">
					<div class="about_diamond">
						<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/icon_diamond.png" class="search_pic" />
					</div>
					<div class="num1"><span>${prefer.price}</span>元/次</div>
					<span class="about_dance"><span>
						<c:choose>
							<c:when test="${prefer.serviceType eq 1}">线上</c:when>
							<c:otherwise>线下</c:otherwise>
						</c:choose>
					</span></span>
					<div class="about_number">
						<span>[已售${prefer.salesVolume}]</span>
					</div>
					<p class="message">服务介绍:${prefer.introduce}</p>
				</div>
			</a>
			<c:if test="${isMyself eq false}">
				<c:choose>
					<c:when test="${prefer.enterTypeValue =='jzzx'}">
					<c:set var="jzzxPrice" value="${prefer.price}"></c:set>
						<div class="${about_box}">
							<a href="javascript:void(0);" onclick="joinse(this)">加入已选</a>
							<a href="javascript:void(0)" class="immediate">立即咨询</a>
							<input type="hidden" value="${prefer.id}" name="skillId">
							<input type="hidden" value="${userInfo.openId}" name="openId_sel">
							<input type="hidden" value="1" name="isPreferred">
							<input type="hidden" value="${prefer.enterTypeValue}" name="enterType">
						</div>
					</c:when>
					<c:otherwise>
						<div class="about_box none_border">
						<a href="javascript:void(0);" onclick="joinse(this)">加入已选</a>
						<a href="<%=contextPath%>/skillOrder/toBuySkill.html?skillId=${prefer.id}&id=${userInfo.id}">立即投放</a>
						<input type="hidden" value="${prefer.id}" name="skillId">
						<input type="hidden" value="${userInfo.openId}" name="openId_sel">
						<input type="hidden" value="1" name="isPreferred">
						<input type="hidden" value="${prefer.enterTypeValue}" name="enterType">
					</div>
					</c:otherwise>
				</c:choose>
			</c:if>
		</div>
		</c:forEach>
		
		<%--更多服务--%>
		<c:choose>
		<c:when test="${fn:length(gxstList) > 0||fn:length(cdzyList) > 0||fn:length(xnsjList) > 0||fn:length(jndrList) > 0||fn:length(sqdvList) > 0}">
		<div class="service_More">
		<div class="enterInfo_bg"></div>
			<div class="person_about">
				<div class="about_line"></div>
				<span class="about_he"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/person_more.png" class="search_pic" /></span>
				<span class="about_him">更多服务</span>
				<div class="about_lineR"></div>
			</div>
			<c:if test="${fn:length(gxstList) > 0}">
			<div class="service_content">
				<div class="ablity">
					<h3>${gxstList[0].fathName}</h3>
				</div>
				<c:forEach var="gxst" items="${gxstList}">
				<div class="person_box">
					<a href="${path}/skill_detail/getskill_detail.html?skillId=${gxst.id}&classValue=${gxst.fathValue}" class="skip">
						<div class="pic_content">
							<div class="about_pic">
								<img src="${gxst.activtyList[0]}" class="search_pic" />
								<span class="consult"><span>${gxst.sonName}</span></span>
							</div>
						</div>
						<div class="about_sell">
							<span class="person_service">${gxst.skillName}</span>
							<span class="about_dance"><span>
							<c:choose>
								<c:when test="${gxst.serviceType eq 1}">线上</c:when>
								<c:otherwise>线下</c:otherwise>
							</c:choose>
							</span></span>
							<div class="service_right">
								<div class="about_diamond">
									<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/icon_diamond.png" class="search_pic" />
								</div>
								<div class="num1"><span>${gxst.mapDetail.skillPrice}</span>元/${gxst.unit}</div>
								<div class="about_number">
									<span>[已售${gxst.sellNo}]</span>
								</div>
							</div>
							<p class="message">服务介绍:${gxst.skillDepict}</p>
						</div>
					</a>
					<c:if test="${isMyself eq false}">
						<div class="about_box">
							<a href="javascript:void(0);" onclick="joinse(this)">加入已选</a>
							<a href="<%=contextPath%>/skillOrder/toBuySkill.html?skillId=${gxst.id}">立即下单</a>
							<input type="hidden" value="${gxst.id}" name="skillId">
							<input type="hidden" value="${userInfo.openId}" name="openId_sel">
							<input type="hidden" value="0" name="isPreferred">
							<input type="hidden" value="${gxst.fathValue}" name="enterType">
						</div>
					</c:if>
				</div>
				</c:forEach>
			</div>
			</c:if>

			<c:if test="${fn:length(cdzyList) > 0}">
			<div class="service_content">
				<div class="ablity">
					<h3 class="address_service">${cdzyList[0].fathName}</h3>
				</div>
				<c:forEach var="cdzy" items="${cdzyList}">
				<div class="person_box">
					
						<!--选项卡切换部分-->
						<div class="person_title">
							<div class="title_head title_green"><span class="title_active">全景</span></div>
							<div class="title_head"><span>中景</span></div>
							<div class="title_head"><span>近景</span></div>
						</div>
						<a href="${path}/skill_detail/getskill_detail.html?skillId=${cdzy.id}&classValue=${cdzy.fathValue}" class="skip">
						<div class="pic_content">
							<!--远景图-->
							<div class="about_Pic about_pic">
								<div class="swiper-container swiper_box0">
									<div class="swiper-wrapper">
										<c:forEach var="all" items="${cdzy.allList}">
											<div class="swiper-slide"><img class="search_pic" src="${all}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${cdzy.sonName}</span></span>
							</div>
							<!--中景图-->
							<div class="about_Pic about_pic about_pic2">
								<div class="swiper-container swiper_box1">
									<div class="swiper-wrapper">
										<c:forEach var="middel" items="${cdzy.middelList}">
											<div class="swiper-slide"><img class="search_pic" src="${middel}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${cdzy.sonName}</span></span>
							</div>
							<!--近景图-->
							<div class="about_Pic about_pic about_pic2">
								<div class="swiper-container swiper_box2">
									<div class="swiper-wrapper">
										<c:forEach var="close" items="${cdzy.closeList}">
											<div class="swiper-slide"><img class="search_pic" src="${close}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${cdzy.sonName}</span></span>
							</div>
						</div>
						<div class="about_sell">
							<span class="person_service">${cdzy.skillName}</span>
							<span class="about_dance"><span>
							<c:choose>
								<c:when test="${cdzy.serviceType  eq 1}">线上</c:when>
								<c:otherwise>线下</c:otherwise>
							</c:choose>
							</span></span>

							<div class="service_right">
								<div class="about_diamond">
									<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/icon_diamond.png" class="search_pic" />
								</div>
								<div class="num1"><span>${cdzy.mapDetail.skillPrice}</span>元/${cdzy.unit}</div>
								<div class="about_number">
									<span>[已售${cdzy.sellNo}]</span>
								</div>
							</div>

							<p class="message">服务介绍:${cdzy.skillDepict}</p>
						</div>
					</a>
					<c:if test="${isMyself eq false}">
						<div class="about_box">
							<a href="javascript:void(0);" onclick="joinse(this)">加入已选</a>
							<a href="<%=contextPath%>/skillOrder/toBuySkill.html?skillId=${cdzy.id}">立即下单</a>
							<input type="hidden" value="${cdzy.id}" name="skillId">
							<input type="hidden" value="${userInfo.openId}" name="openId_sel">
							<input type="hidden" value="0" name="isPreferred">
							<input type="hidden" value="${cdzy.fathValue}" name="enterType">
						</div>
					</c:if>
				</div>
				</c:forEach>
			</div>
			</c:if>
			<!--商家服务-->
			<c:if test="${fn:length(xnsjList) > 0}">
			<div class="service_content">
				<div class="ablity">
					<h3 class="address_service">${xnsjList[0].fathName}</h3>
				</div>
				<c:forEach var="xnsj" items="${xnsjList}">
				<div class="person_box">
					
						<!--选项卡切换部分-->
						<div class="person_title">
							<div class="title_head title_green"><span class="title_active">全景</span></div>
							<div class="title_head"><span>中景</span></div>
							<div class="title_head"><span>近景</span></div>
						</div>
						<a href="${path}/skill_detail/getskill_detail.html?skillId=${xnsj.id}&classValue=${xnsj.fathValue}" class="skip">
						<div class="pic_content">
							<!--远景图-->
							<div class="about_Pic about_pic">
								<div class="swiper-container swiper_box0">
									<div class="swiper-wrapper">
										<c:forEach var="all" items="${xnsj.allList}">
											<div class="swiper-slide"><img class="search_pic" src="${all}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${xnsj.sonName}</span></span>
							</div>
							<!--中景图-->
							<div class="about_Pic about_pic about_pic2">
								<div class="swiper-container swiper_box1">
									<div class="swiper-wrapper">
										<c:forEach var="middel" items="${xnsj.middelList}">
											<div class="swiper-slide"><img class="search_pic" src="${middel}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${xnsj.sonName}</span></span>
							</div>
							<!--近景图-->
							<div class="about_Pic about_pic about_pic2">
								<div class="swiper-container swiper_box2">
									<div class="swiper-wrapper">
										<c:forEach var="all" items="${xnsj.allList}">
											<div class="swiper-slide"><img class="search_pic" src="${all}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${xnsj.sonName}</span></span>
							</div>
						</div>
						<div class="about_sell">
							<span class="person_service">${xnsj.skillName}</span>
							<span class="about_dance"><span>
							<c:choose>
								<c:when test="${xnsj.serviceType eq 1}">线上服务</c:when>
								<c:otherwise>线下服务</c:otherwise>
							</c:choose>
							</span></span>

							<div class="service_right">
								<div class="about_diamond">
									<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/icon_diamond.png" class="search_pic" />
								</div>
								<div class="num1"><span>${xnsj.mapDetail.skillPrice}</span>元/${xnsj.unit}</div>
								<div class="about_number">
									<span>[已售${cdzy.sellNo}]</span>
								</div>
							</div>

							<p class="message">服务介绍:${xnsj.skillDepict}</p>
						</div>
					</a>
					<c:if test="${isMyself eq false}">
						<div class="about_box">
							<a href="javascript:void(0);" onclick="joinse(this)">加入已选</a>
							<a href="<%=contextPath%>/skillOrder/toBuySkill.html?skillId=${xnsj.id}">立即下单</a>
							<input type="hidden" value="${xnsj.id}" name="skillId">
							<input type="hidden" value="${userInfo.openId}" name="openId_sel">
							<input type="hidden" value="0" name="isPreferred">
							<input type="hidden" value="${xnsj.fathValue}" name="enterType">
						</div>
					</c:if>
				</div>
				</c:forEach>
			</div>
			</c:if>
			<!--技能服务-->
			
			
			<c:if test="${fn:length(jndrList) > 0}"> 
			<div class="service_content">
				<div class="ablity">
					<h3 class="address_service">${jndrList[0].fathName}</h3>
				</div>
				<c:forEach var="jndr" items="${jndrList}">
				<div class="person_box">
					
						<!--选项卡切换部分-->
						<div class="person_title">
							<div class="title_head title_green"><span class="title_active">全景</span></div>
							<div class="title_head"><span>中景</span></div>
							<div class="title_head"><span>近景</span></div>
						</div>
						<a href="${path}/skill_detail/getskill_detail.html?skillId=${jndr.id}&classValue=${jndr.fathValue}" class="skip">
						<div class="pic_content">
							<!--远景图-->
							<div class="about_Pic about_pic">
								<div class="swiper-container swiper_box0">
									<div class="swiper-wrapper">
										<c:forEach var="active" items="${jndr.activtyList}">
											<div class="swiper-slide"><img class="search_pic" src="${active}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${jndr.sonName}</span></span>
							</div>
							<!--中景图-->
							<div class="about_Pic about_pic about_pic2">
								<div class="swiper-container swiper_box1">
									<div class="swiper-wrapper">
										<c:forEach var="active" items="${jndr.activtyList}">
											<div class="swiper-slide"><img class="search_pic" src="${active}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${jndr.sonName}</span></span>
							</div>
							<!--近景图-->
							<div class="about_Pic about_pic about_pic2">
								<div class="swiper-container swiper_box2">
									<div class="swiper-wrapper">
										<c:forEach var="active" items="${jndr.activtyList}">
											<div class="swiper-slide"><img class="search_pic" src="${active}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${jndr.sonName}</span></span>
							</div>
						</div>
						<div class="about_sell">
							<span class="person_service">${jndr.skillName}</span>
							<span class="about_dance"><span>
							<c:choose>
								<c:when test="${jndr.serviceType eq 1 }">线上</c:when>
								<c:otherwise>线下</c:otherwise>
							</c:choose>
							</span></span>

							<div class="service_right">
								<div class="about_diamond">
									<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/icon_diamond.png" class="search_pic" />
								</div>
								<div class="num1"><span>${jndr.mapDetail.skillPrice}</span>元/${jndr.unit}</div>
								<div class="about_number">
									<span>[已售${jndr.sellNo}]</span>
								</div>
							</div>

							<p class="message">服务介绍:${jndr.skillDepict}</p>
						</div>
					</a>
					<c:if test="${isMyself eq false}">
						<div class="about_box">
							<a href="javascript:void(0);" onclick="joinse(this)">加入已选</a>
							<a href="<%=contextPath%>/skillOrder/toBuySkill.html?skillId=${jndr.id}">立即下单</a>
							<input type="hidden" value="${jndr.id}" name="skillId">
							<input type="hidden" value="${userInfo.openId}" name="openId_sel">
							<input type="hidden" value="0" name="isPreferred">
							<input type="hidden" value="${jndr.fathValue}" name="enterType">
						</div>
					</c:if>
				</div>
				</c:forEach>
			</div>
			</c:if>
			<!--社群服务-->
			<c:if test="${fn:length(sqdvList) > 0}">
			<div class="service_content">
				<div class="ablity">
					<h3 class="address_service">${sqdvList[0].fathName}</h3>
				</div>
				<c:forEach var="sqdv" items="${sqdvList}">
				<c:choose>
				<c:when test="${sqdv.sonValue eq 'zmt'}">
				<div class="person_box">
					
						<!--选项卡切换部分-->
						<div class="person_title">
							<div class="title_head title_green"><span class="title_active">全景</span></div>
							<div class="title_head"><span>中景</span></div>
							<div class="title_head"><span>近景</span></div>
						</div>
						<a href="${path}/skill_detail/getskill_detail.html?skillId=${sqdv.id}&classValue=${sqdv.fathValue}" class="skip">
						<div class="pic_content">
							<!--远景图-->
							<div class="about_Pic about_pic">
								<div class="swiper-container swiper_box0">
									<div class="swiper-wrapper">
										<c:forEach var="dairy" items="${sqdv.dairyList}">
											<div class="swiper-slide"><img class="search_pic" src="${dairy}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${sqdv.sonName}</span></span>
							</div>
							<!--中景图-->
							<div class="about_Pic about_pic about_pic2">
								<div class="swiper-container swiper_box1">
									<div class="swiper-wrapper">
										<c:forEach var="dairy" items="${sqdv.dairyList}">
											<div class="swiper-slide"><img class="search_pic" src="${dairy}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${sqdv.sonName}</span></span>
							</div>
							<!--近景图-->
							<div class="about_Pic about_pic about_pic2">
								<div class="swiper-container swiper_box2">
									<div class="swiper-wrapper">
										<c:forEach var="dairy" items="${sqdv.dairyList}">
											<div class="swiper-slide"><img class="search_pic" src="${dairy}" /></div>
										</c:forEach>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<span class="consult"><span>${sqdv.sonName}</span></span>
							</div>
						</div>
						<div class="about_sell">
							<span class="person_service">${sqdv.skillName}</span>
							<span class="about_dance"><span>
							<c:choose>
								<c:when test="${sqdv.serviceType eq 1}">线上</c:when>
								<c:otherwise>线下</c:otherwise>
							</c:choose>
							</span></span>

							<div class="service_right">
								<div class="about_diamond">
									<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/icon_diamond.png" class="search_pic" />
								</div>
								<div class="num1"><span>${sqdv.mapDetail.skillPrice}</span>元/${sqdv.unit}</div>
								<div class="about_number">
									<span>[已售${sqdv.sellNo}]</span>
								</div>
							</div>

							<p class="message">服务介绍:${sqdv.skillDepict}</p>
						</div>
					</a>
					<c:if test="${isMyself eq false}">
						<div class="about_box">
							<a href="javascript:void(0);" onclick="joinse(this)">加入已选</a>
							<a href="<%=contextPath%>/skillOrder/toBuySkill.html?skillId=${sqdv.id}">立即下单</a>
							<input type="hidden" value="${sqdv.id}" name="skillId">
							<input type="hidden" value="${userInfo.openId}" name="openId_sel">
							<input type="hidden" value="0" name="isPreferred">
							<input type="hidden" value="${sqdv.fathValue}" name="enterType">
						</div>
					</c:if>
				</div>
				</c:when>
				<c:otherwise>
				<a href="${path}/skill_detail/getskill_detail.html?skillId=${sqdv.id}&classValue=${sqdv.fathValue}" class="skip">
					<div class="pic_content">
						<!--远景图-->
						<div class="about_Pic about_pic">
							<div class="swiper-container swiper_box0">
								<div class="swiper-wrapper">
									<c:forEach var="dairy" items="${sqdv.dairyList}">
											<div class="swiper-slide"><img class="search_pic" src="${dairy}" /></div>
									</c:forEach>
								</div>
								<div class="swiper-pagination"></div>
							</div>
							<span class="consult"><span>${sqdv.sonName}</span></span>
						</div>
						<!--中景图-->
						<div class="about_Pic about_pic about_pic2">
							<div class="swiper-container swiper_box1">
								<div class="swiper-wrapper">
									<c:forEach var="dairy" items="${sqdv.dairyList}">
											<div class="swiper-slide"><img class="search_pic" src="${dairy}" /></div>
									</c:forEach>
								</div>
								<div class="swiper-pagination"></div>
							</div>
							<span class="consult"><span>${sqdv.sonName}</span></span>
						</div>
						<!--近景图-->
						<div class="about_Pic about_pic about_pic2">
							<div class="swiper-container swiper_box2">
								<div class="swiper-wrapper">
									<c:forEach var="dairy" items="${sqdv.dairyList}">
											<div class="swiper-slide"><img class="search_pic" src="${dairy}" /></div>
									</c:forEach>
								</div>
								<div class="swiper-pagination"></div>
							</div>
							<span class="consult"><span>${sqdv.sonName}</span></span>
						</div>
					</div>
					<div class="about_sell">
						<span class="person_service">${sqdv.skillName}</span>
						<span class="about_dance"><span>
							<c:choose>
								<c:when test="${sqdv.serviceType eq 1 }">线上</c:when>
								<c:otherwise>线下</c:otherwise>
							</c:choose>
						</span></span>

						<div class="service_right">
							<div class="about_diamond">
								<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/icon_diamond.png" class="search_pic" />
							</div>
							<div class="num1"><span>${sqdv.mapDetail.skillPrice}</span>元/${sqdv.unit}</div>
							<div class="about_number">
								<span>[已售${sqdv.sellNo}]</span>
							</div>
						</div>

						<p class="message">服务介绍:${sqdv.skillDepict}</p>
					</div>
				</a>
					<c:if test="${isMyself eq false}">
						<div class="about_box">
							<a href="javascript:void(0);" onclick="joinse(this)">加入已选</a>
							<a href="<%=contextPath%>/skillOrder/toBuySkill.html?skillId=${sqdv.id}">立即下单</a>
							<input type="hidden" value="${sqdv.id}" name="skillId">
							<input type="hidden" value="${userInfo.openId}" name="openId_sel">
							<input type="hidden" value="0" name="isPreferred">
							<input type="hidden" value="${sqdv.fathValue}" name="enterType">
						</div>
					</c:if>
				</c:otherwise>
				</c:choose>
				</c:forEach>
			</div>
			</c:if>
		</div>
		</c:when>
			<c:otherwise>
				<div class="noService">暂无更多服务</div>
			</c:otherwise>
		</c:choose>
		<input type="hidden" id="basId" name="id" value="${userInfo.id}" />

	<!-- 06-19加入已选弹窗 -->
	<div class="popupWindow_content">
       	<div class="popupWindow_title">
       		<span class="popupWindow_close"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/cha.png" alt="" class="search_pic" /></span>
       		<span class="poopupWindow_choose">已选</span>
       		<a class="popupWindow_car" href="${path}/skillOrder/SelectedList.html">
       		   <img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/buyCar.png" class="search_pic" />
       		   <img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/redCar.png" class="redCar_pic" />
       		</a>
       	</div>
       	<ul class="poopupWindow_box">
       	<!-- 因为点击加入已选时才触发所以foreach 功能暂时删除 -->
       	</ul>
       	<span class="popupWindow_more popupWindow_up"></span>
       </div>
		
		<!--立即咨询出来的弹框-->
		<div class="popup_mask"></div>
		<div class="consult_popup">
			<div class="close"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/close.png" class="search_pic" /></div>
			<div class="popup_content">
				<h3>温馨提示</h3>
				<span class="popup_pic"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/style.png" class="search_pic" /></span>
				<span class="told">为避免打扰用户，如需在线咨询，请先下单支付咨询费用。</span>
				<span class="sold">￥&nbsp;${jzzxPrice}元</span>
				<span class="sold1">(支付后即可与卖家直接沟通哦~)</span>
				<span class="go"><span class="immediately">立即<br/>支付</span></span>
				<input type="hidden" id="sellerOpenId" value="">
				<input type="hidden" id="skillId" value="">
			</div>
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/swiper-3.3.1.jquery.min.js"></script>
	
	
	<script type="text/javascript">
	 var baseP = "<%=basePath%>";
	 /*更换背景图片 */
	 function changeBack() {
	 	
	 	var path="${path}";
	     var clientUrl = window.location.href;
	     var reqPath= path+'/skillUser/getJSConfig.html';
	 	//请求后台，获取jssdk支付所需的参数
	 	$.ajax({
	 		type : 'post',
	 		url : reqPath,
	 		dataType : 'json',
	 		data : {
	 			"clientUrl" : clientUrl
	 		//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
	 		},
	 		cache : false,
	 		error : function() {
	 			alert("系统错误，请稍后重试");
	 			return false;
	 		},
	 		success : function(data) {
	 			//微信支付功能只有微信客户端版本大于等于5.0的才能调用
	 			var return_date = eval(data);
	 			if (parseInt(data[0].agent) < 5) {
	 				alert("您的微信版本低于5.0无法使用微信支付");
	 				return;
	 			}
	 			//JSSDK支付所需的配置参数，首先会检查signature是否合法。
	 			wx.config({
	 				debug : false, //开启debug模式，测试的时候会有alert提示
	 				appId : return_date[0].appId, //公众平台中-开发者中心-appid
	 				timestamp : return_date[0].config_timestamp, //时间戳
	 				nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
	 				signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
	 				jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
	 			});
	 			//上方的config检测通过后，会执行ready方法
	 			wx.ready(function() {
	 				wx.chooseImage({
	 				    count: 1, // 默认9
	 				    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
	 				    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
	 				    success: function (res) {
	 				        var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
	 				       // alert("localIds"+localIds[0]);
	 				        var localArr= res.localIds; 
	 				        
	 				        if(localArr.length >0){
	 					   	 wx.uploadImage({
	 					           localId: localIds[0], // 需要上传的图片的本地ID，由chooseImage接口获得
	 					           isShowProgressTips: 1, // 默认为1，显示进度提示
	 					           success: function (res) {
	 					               var serverId = res.serverId; // 返回图片的服务器端ID
	 					              // alert("serverId="+serverId);
	 					               
	 					               $.ajax({
	 					              	type : "post",
	 					              	url :  path+"/skillUser/downloadImageBackPic.html",
	 					       			dataType : "json",
	 					       			data : {
	 					       				serverId : serverId,
	 					       				id : $("#basId").val()
	 					       			},
	 					       			success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
	 					       				//alert("path="+data.path);
	 					       				$("#backPicUrl").attr("src", data.path);
	 					       			}
	 					       		});
	 					           }
	 					       });
	 				        }
	 				    }
	 				});
	 			});
	 			wx.error(function(res) {
	 				alert(res.errMsg);
	 			});
	 		}
	 	});
	 }
  	</script>
  	<script>
		var baseP = "<%=basePath%>";
		var path = "<%=basePath%>";
		window.onload=function(){
			
			var nickname = "${userInfo.nickname}";
			var schoolName = "${userInfo.schoolName}";
			
			//获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
							 title: schoolName+nickname+"正在校咖汇进行资源分享，还不速来围观？", // 分享标题
						    desc: "校园汇，你想要的Ta都有！", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: schoolName+nickname+"正在校咖汇进行资源分享，还不速来围观？", // 分享标题
						    desc: "校园汇，你想要的Ta都有！", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () {
						        // 用户取消分享后执行的回调函数
						    }
						});
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
  	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/person_detail.js"></script>
  	<script src="${path}/xkh_version_2.2/js/commen.js"></script>  
</html>
