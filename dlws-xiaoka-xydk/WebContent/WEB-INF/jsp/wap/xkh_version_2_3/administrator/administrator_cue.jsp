<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/wap/xkh_version_2/image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>管理员更换</title>
	</head>
	<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/administrator.css" />
	<body>
		<div class="cue_content">
			<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/xuanzhuan.gif" class="search_pic" />
			<!--发布提交提示页面-->
			<p>提交成功，等待新管理员确认</p>
			<!--点击接受，提示正在为您跳转到我的页面，点击拒绝。提示正在为您跳转跳转到首页-->
			<a href="javascript:void(0);">正在为您跳转...</a>
		</div>
	</body>
	<script type="text/javascript">
		onload=function(){ 
			setTimeout(go, 2000); 
		}; 
		function go(){
			location.href="${path}/home/getmyInform.html"; 
		}
	</script>
</html>
