<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="/WEB-INF/jsp/wap/xkh_version_2/image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>校咖汇</title>
	</head>
	<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/administrator.css" />
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js"></script>
	<body>
		<c:if test="${isAgree == 0 }">
			<div class="adminiNew_content">
				<span class="admini_new"><span class="admini_green">${contactUser }</span>邀请您成为<span class="admini_green">${nickname}</span>的新管理员，您是否接受转权？</span>
			    <!--点击接受跳转页面，提示正在为您跳转，跳转到我的页面-->
			    <a id="accept" style="display: block" href="javascript:accept('${recordId }')" class="introduce">接受</a>
			    <a id="refuse" style="display: block" href="${path}/change/refuse.html?recordId=${recordId }" class="intro">拒绝</a>
			    <a id="accepted" style="display: none" href="javascript:void()" class="introduce">已接受</a>
			</div>
		</c:if>
		<c:if test="${isAgree == 1 }">
			<div class="adminiNew_content">
				<span class="admini_new"><span class="admini_green">${contactUser }</span>已成为<span class="admini_green">${nickname}</span>的新管理员</span>
			</div>
		</c:if>
		<c:if test="${isAgree == 2 }">
			<div class="adminiNew_content">
				<span class="admini_new"><span class="admini_green">${contactUser }</span>拒绝成为<span class="admini_green">${nickname}</span>的新管理员</span>
			</div>
		</c:if>
		
	</body>
</html>
<script>
function accept(recordId){
	 $.ajax({
         type: 'post',
         url: 'accept.html',
         dataType: 'json',
         data:{"recordId":recordId},
         success: function(data){
         	if(data.ajax_status == "ajax_status_success"){
         		document.getElementById("accept").style.display="none";
         		document.getElementById("refuse").style.display="none";
         		document.getElementById("accepted").style.display="block";
         	}else{
         		alert("出现异常");
         	}
         },
         error: function(xhr, type){
             alert('Ajax error!');
         }
     });
}
</script>