<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>校咖汇</title>
	</head>
	<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.picker.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/mui.poppicker.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/iconfont/iconfont.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/dropload.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/search.css" />

	<body>
		<header class="ctt_headerSearch">
			<form class="ctt_hot">
				<a href="javascript:search()" class="hot_pic"><img class="search_pic" src="${path}/xkh_version_2/img/search.png" /></a>
				<c:choose>
					<c:when test="${fid!='' &&fid!=null && map.seachValue==null}">
						<input type="text" onclick="search();" name="searchValue" id="searchValue" value="${map.seachValue }" placeholder="搜索" class="hot_inp" />
					</c:when>
					<c:otherwise>
						<input type="text" onclick="search();" name="searchValue" id="searchValue" value="${map.seachValue }" placeholder="搜索" class="hot_inp" />
					</c:otherwise>
				</c:choose>
			    <span class="icon_remove"></span>  
			</form>
			<span class="ctt_cancel">取消</span>
		</header>
		<div class="searchSource_content">
			<span class="searchSource_left searchSource_border"><span class="searchSource_green">搜资源</span></span>
			<span class="searchSource_left"><span>搜大咖</span></span>
		</div>
		<div class="list_content">
			<c:if test="${empty fid}"> 
				<span class="hot_word">搜索热词</span>
				<div class="search_list">
					<c:forEach items="${hotWords }" var="one">
							<a href="javascript:void(0)"><span>${one.dicName}</span></a>
					</c:forEach>
				</div>
			 </c:if> 
		</div>
		<section class="search_content" >
			<ul class="class_choose">
				<li class="choose choose_sort">
					<span class="class_active">默认排序</span>
					<span class="icon_down iconSort"></span>
				</li>
				<li class="choose choose1">
					<span>销量优先</span>
				</li>
				<li class="choose choose2">
					<span>筛选</span>
					<span class="filter"></span>
				</li>
				<div class="search_bg ctt_none"></div>
				<!--点击排序出来的部分-->
				<div class="class_sortContent">
					<div class="search_bg"></div>
					<ul class="class_sort">
						<li id="def" data-name="" class="class_active">默认排序<span class="iconfont icon-gou class_check"></span></li>
						<li id="desc" data-name="desc">价格由高到低<span class="iconfont class_check"></span></li>
						<li id="asc" data-name="asc">价格由低到高<span class="iconfont class_check"></span></li>
					</ul>
				</div>
				<!--点击筛选出来的样式-->
				<div class="class_filterContent">
			<div style="min-height: 100%;">
			<div class="class_price">
				<h3>价&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格</h3>
				<div class="price_box">
					<input id="minPrice" placeholder="最低价" type="number" onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))">
					<span>—</span>
					<input id="maxPrice" placeholder="最高价" type="number" onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))">
				</div>
			</div>
			<div class="address">
				<h3>所&nbsp;在&nbsp;&nbsp;地</h3>
				<div class="choose_address choose_address1 mui-input-row" id="chooseCity">
					<span class="address_text address_text1 result-tips">选择所在地</span>
					<span class="address_text show-result"></span>
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right_jiantou.png" class="ctt_jiantou" />
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/Tcha.png" class="ctt_cha" />
				</div>
			</div>
			<div class="service_box">
				<h3>服务类型</h3>
				<div class="service_style">
					<div data-name="1">线上</div>
					<div data-name="2">线下</div>
				</div>	
			</div>
			<div class="address address1">
				<h3>院校等级</h3>
				<%-- <div class="choose_address mui-input-row" id="chooseSchool">
					<span style="margin:0 " class="address_text result-tips">选择院校等级</span>
					<span class="address_text show-result"></span>
					<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right_jiantou.png" class="ctt_jiantou" />
				</div> --%>
				<div class="service_style schoolStyle">
					<div>985/211</div>
					<div>本科</div>
					<div>专科</div>
				</div>
			</div>
			
			</div>
			<div class="enure_content">
			   <div class="reset_box">重置</div>
			   	<div class="enure_box">确定</div>
			</div>
		
		</div>
			</ul>
			<!--搜索资源出来的列表页-->
			<!--<div class="searchResource_Content ">-->
			<ul class="searchResource_content ctt_searchBox" id="searchResource_content">
			<%-- <c:forEach items="${userList}" var="one">
				<li>
					<div class="search_bg"></div>
					<div class="searchList_content">
						<div class="searchList_title">
							<span class="searchList_pic"><img src="${one.headImgUrl}" class="search_pic" /></span>
							<span class="searchList_name">${one.userName}</span>
							<div class="searchList_city">
								<span class="iconfont icon-zuobiao"></span>
								<span>${one.cityName}</span>
							</div>
						</div>
						<div class="ctt_Piccontent">
							<div class="ctt_pic">
								<img src="${one.image}" class="search_pic">
								<span class="ctt_consult"><span>${one.schoolName}</span></span>
							</div>
							<span class="ctt_serviceTitle"><span>服务标题&nbsp;:&nbsp;${one.skillName }</span></span>
						</div>
					</div>
					<div class="ctt_Pricecontent">
						<div class="Pricecontent_left">
							<span>￥${one.skillPrice }元/次</span>
							<span>[已售${one.sellNo}]</span>
						</div>
						<span class="Pricecontent_right">加入已选</span>
					</div>
				</li>
			</c:forEach> --%>
			</ul>
			<!--</div>-->
				
			<!--搜索大咖出来的列表页-->
				<ul class="ctt_lists cttRecommend_content ctt_searchBox">
					
				</ul>
		</section>
       <div class="mask"></div>
       <!-- 06-08加入已选弹窗 -->
       <!-- <div class="window_Mask"></div>
	<div class="success_choose">成功加入已选</div>  -->
	<!-- 06-19加入已选弹窗 -->
	<div class="popupWindow_content">
       	<div class="popupWindow_title">
       		<span class="popupWindow_close"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/cha.png" alt="" class="search_pic" /></span>
       		<span class="poopupWindow_choose">已选</span>
       		<span class="popupWindow_car">
       		<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/buyCar.png" class="search_pic" />
       		<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/redCar.png" class="redCar_pic" />
       		</span>
       	</div>
       	<ul class="poopupWindow_box" id="selectSkill">
       		
       	</ul>
       	<!--这一部分当是搜索结果为单项时候就不出现，只需要去掉popupWindow_up这个类名就可以-->
       	<span class="popupWindow_more popupWindow_up"></span>
       </div>
       	<input type="hidden" id="cityName" value="">
		<input type="hidden" id="schoolLabel" value="">
		<input type="hidden" id="serviceType" value="">
		<input type="hidden" id="page" name="page" value="1">
		<input type="hidden" name="orderPrice" id="orderPrice"  value="">
		<input type="hidden" name="ordersellNo" id="ordersellNo"  value="">
		<input type="hidden" name="fid" id="fid" value="${fid}">
	</body>
	
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript">
	var baseP = "<%=basePath%>";
   	var proList = ${proList};
   	var labelList = ${labelList};
   	var seachValue=$("#seachValue").val();
   	var path = "${path}";
   	
   	var dicName = "${hotWords[0].dicName}";
   	
   	var fid = "${fid}";
   	
   	</script> 
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.picker.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/mui.poppicker.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/city.data.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/shcool.data.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/dropload.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/search.js"></script>
	
	
	
</html>
