<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
java.util.Date createDate = (java.util.Date)request.getAttribute("createDate");
%>
<!DOCTYPE html>
<html>
<%-- 订单详情展示页面    --%>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publish.css" />
		<title>商家发布-订单详情-批量咨询</title>
	</head>
	<body>
		<c:import url="common_status_jzzx.jsp">
			<c:param name="status" value="${orderStatus}"></c:param>
			<c:param name="time" value="${timmer}"></c:param>
		</c:import>
		<!--所有卖家都没有接单-->
		<div class="detail">
			<h3>批量咨询</h3>
			<span class="edit">${statuStr}</span>
		</div>
		<div class="substance_content seller_content">
			<div class="substance_left"><img src="${list[0].orderDetil.imgUrl }" class="search_pic" /></div>
			<div class="substance_Right">
				<span class="substance_Music">${list[0].orderDetil.skillName }</span>
				<span class="substance_Line"><span>
				<c:choose>
						<c:when test="${list[0].orderDetil.serviceType == '1' }">
							<span>线上</span>
						</c:when>
						<c:otherwise>
							<span>线下</span>
						</c:otherwise>
				</c:choose></span></span>
				<span class="orderDetail_info">${list[0].orderDetil.skillDepict }</span>
				<span class="substance_price">${list[0].orderDetil.skillPrice }</span><span class="substance_price">元/次</span>
				<!-- <span class="sell_Num">[已售4567]</span> -->
			</div>
		</div>
		<!--没有接单-->
		<div class="info_content">
			<span class="detail_info">${hint}</span>
		</div>
		<!--已经接单出现在卖家接单，完成服务，订单超时，退款成功  待服务不出现 和 卖家自动取消订单不会出现-->
		<c:if test="${orderStatus ne 1 || orderStatus ne 7}">
			
			<c:forEach items="${list }" var="base" varStatus="status">
					<span>
						<c:choose>
							<c:when test="${base.orderMessage.orderStatus eq 0 ||base.orderMessage.orderStatus eq 3 ||base.orderMessage.orderStatus eq 4}">
								<c:set var="have" value="true"></c:set>
							</c:when>
						</c:choose>	
					</span>
			</c:forEach>
			<c:if test="${have eq true}">
				<div class="information_content">
					<div class="info_content">
						<div class="info_pic detail_pic"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/order.png" class="search_pic" /></div>
						<span class="info">已接单</span>
					</div>
					<div class="picture_content">
						<c:forEach items="${list }" var="base" varStatus="status">
								<c:choose>
								<c:when test="${base.orderMessage.orderStatus eq 0 ||base.orderMessage.orderStatus eq 3 ||base.orderMessage.orderStatus eq 4}">
									<span>
										<a href="${path}/personInfoDetail/detail.html?id=${base.userInfo.id}">
											<img src="${base.userInfo.headPortrait}" class="search_pic">
										</a>
									</span>
								</c:when>
							</c:choose>	
						</c:forEach>
					</div>
				</div>
				<div class="selected_bg"></div>
			</c:if>
		</c:if>
		<div class="selected_bg"></div>
		<!--没接单，完成服务时候不出现-->
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic detail_pic"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/none_order.png" class="search_pic" /></div>
				<span class="info">未接单</span>
			</div>
			<div class="picture_content">
				<c:forEach items="${list }" var="base" varStatus="status">
					
						<c:choose>
						<c:when test="${base.orderMessage.orderStatus ne 0 && base.orderMessage.orderStatus ne 3 && base.orderMessage.orderStatus ne 4}">
							<span>
								<a href="${path}/chatMesseage/queryOrderDetail.html?orderId=${base.orderMessage.orderId}&channel=buyer">
									<img src="${base.userInfo.headPortrait}" class="search_pic">
									<div class="pic_mask"></div>
								</a>
							</span>
						</c:when>
					</c:choose>	
					
				</c:forEach>
			</div>
		</div>
		<div class="selected_bg"></div>
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic detail_pic"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/consult.png" class="search_pic" /></div>
				<span class="info">订单信息</span>
			</div>
			<div class="info-content">
				<span class="number">订单编号&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${list[0].orderMessage.pid}</span>
			</div>
			<div class="info-content info-content2">
				<span class="number">下单时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span><fmt:formatDate type="both" value="${list[0].orderMessage.createDate }" /></span>
			</div>
			<div class="total_content">
				<div class="total">
					<div class="total_pic"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/diamond.png" class="search_pic" /></div>
					<span>合计&nbsp;:&nbsp;</span>
					<span>${sumMoney}元</span>
				</div>
			</div>
		</div>
		<!--取消订单出现在没有接单时候-->
		<c:if test="${isMyself}">
				<c:choose>
					<c:when test="${orderStatus eq 2}">
							<div class="total_content none_border">
							<div class="order_content publishPut_bottom">
								<a href="${path}/ordersea/searchOrderSell.html?tabNum=0" >取消订单</a>
<%-- 								<a href="${path}/ordersea/searchOrderSell.html?tabNum=0" onclick="cancelOrder(${list[0].orderMessage.pid})">取消订单</a>
 --%>							</div>
						</div>
					</c:when>
				</c:choose>
		</c:if>
	</body>
</html>
    <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/publish/publishPut_detail.js" ></script>
    <script type="text/javascript">
    	var path = "<%=basePath%>";
   		var baseP = "<%=basePath%>";
    	window.onload = function(){
    		//生成订单的时间
    		var createDate = '<%=createDate%>';
    		var startTime = new Date(); 
    	}
    	function cancelOrder(pid){
    		//alert(pid);
    		//window.location.href = "";
    	}
    	function payNow(pid,payMoney){
    		//alert(pid);
    		//alert(payMoney);
    	}
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/commen.js" ></script>
</html>