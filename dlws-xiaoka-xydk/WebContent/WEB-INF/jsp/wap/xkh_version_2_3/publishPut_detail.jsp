<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String contextPath = request.getContextPath();
	java.util.Date createDate = (java.util.Date) request
			.getAttribute("createDate");
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta
	content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1"
	id="viewport" name="viewport" />
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link rel="stylesheet"
	href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/common.css" />
<link rel="stylesheet"
	href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/publish.css" />
<title>订单详情</title>
</head>

<body>
	<div class="consult_head">
		<div class="head_content">
			<div class="head_left">
				<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/watch.png"
					class="search_pic">
			</div>
			<span class="head_right">订单已生成，<span>23小时59分59秒后将自动取消</span></span>
		</div>
	</div>
	<div class="publish_case">
		<div class="publish_content">
			<!--待支付时候-->
			<div class="detail">
				<h3>批量投放</h3>
				<span class="edit">待付款</span>
			</div>
		</div>
		<div class="substance_content">
			<div class="substance_left">
				<img src="${paramsMap.imgUrl }" class="search_pic" />
			</div>
			<div class="substance_Right">
				<span class="substance_Music">${paramsMap.serviceName }</span> <span
					class="substance_Line"> <c:choose>
						<c:when test="${paramsMap.serviceType == '1' }">
							<span>线上</span>
						</c:when>
						<c:otherwise>
							<span>线下</span>
						</c:otherwise>
					</c:choose>
				</span>
				<p>${paramsMap.introduce }</p>
				<span class="substance_price">${paramsMap.price }</span><span
					class="substance_price">元/次</span> <span class="sell_Num">[已售${paramsMap.salesVolume }]</span>
			</div>

		</div>
		<div class="selected_bg accurate_bg"></div>
	</div>
	<!--投放对象-->
	<div class="information_content">
		<div class="info_content">
			<div class="info_pic detail_pic">
				<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/object.png"
					class="search_pic">
			</div>
			<span class="info">投放对象</span>
		</div>
		<!--这一部分图片最多放12个-->
		<div class="picture_content picture_Content1">
			<c:forEach items="${list }" var="base" varStatus="status">
				<c:choose>
					<c:when test="${status.index < 12 }">
						<a href="seller_list.html"> <img src="${base.headPortrait}"
							class="search_pic">
						</a>
					</c:when>
				</c:choose>
			</c:forEach>
		</div>
		<!--超过12个的部分-->
		<div class="picture_content picture_Content2 put_none">
			<c:forEach items="${list }" var="base" varStatus="status">
				<c:choose>
					<c:when test="${status.index > 11 } ">
						<a href="seller_list.html"> <img src="${base.headPortrait}"
							class="search_pic">
						</a>
					</c:when>
				</c:choose>
			</c:forEach>
		</div>
		<div class="publishLook_more">
			<span class="publishLook publish_see">查看全部</span> <span
				class="publishLook publish_see2 put_none">收起全部</span> <span
				class="publishLook_pic"></span>
		</div>
	</div>
	<div class="selected_bg accurate_bg"></div>
	<!--投放内容-->
	<div class="information_content">
		<div class="info_content">
			<div class="info_pic">
				<img
					src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/accurtate.png"
					class="search_pic" />
			</div>
			<span class="info">投放内容</span>
		</div>
		<p class="accurtate_title">标题</p>
		<div class="introduce_new">
			<div class="about_content p" id="box">${paramsMap.title }</div>
			<span class="watch"></span>
		</div>
		<!--添加的图片-->
		<div class="webservice_content">
			<!--添加的图片-->
			<div class="picture_box">
				<div class="add_picture">
					<img
						src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/per_banner01.jpg"
						class="search_pic" />
				</div>
				<div class="add_picture">
					<img
						src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/per_banner01.jpg"
						class="search_pic" />
				</div>
				<div class="add_picture">
					<img
						src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/per_banner01.jpg"
						class="search_pic" />
				</div>
			</div>

		</div>
		<span class="lianjie">链接:${paramsMap.link }</span>
	</div>
	<div class="selected_bg accurate_bg"></div>
	<!--买家信息-->
	<div class="information_content">
		<div class="info_content">
			<div class="info_pic">
				<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/buyer.png"
					class="search_pic" />
			</div>
			<span class="info">买家信息</span>
		</div>
		<div class="info-content">
			<span>联&nbsp;&nbsp;系&nbsp;&nbsp;人&nbsp;&nbsp;:&nbsp;&nbsp;</span> <span>${paramsMap.name }</span>
		</div>
		<div class="info-content info-content2">
			<span>联系电话&nbsp;&nbsp;:&nbsp;&nbsp;</span> <span>${paramsMap.phone }</span>
		</div>
		<div class="info-content info-content2 none_border">
			<span>买家留言&nbsp;&nbsp;:&nbsp;&nbsp;</span>
			<div class="introduce_new">
				<div class="about_content p" id="box">${paramsMap.note }</div>
				<strong class="watch"></strong>
			</div>
		</div>
	</div>
	<!--订单信息-->
	<div class="selected_bg accurate_bg"></div>
	<div class="information_content">
		<div class="info_content">
			<div class="info_pic">
				<img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/consult.png"
					class="search_pic" />
			</div>
			<span class="info">订单信息</span>
		</div>
		<div class="info-content">
			<span>订单编号&nbsp;&nbsp;:&nbsp;&nbsp;</span> <span>${pid }</span>
		</div>
		<div class="info-content info-content2 none_border">
			<span>下单时间&nbsp;&nbsp;:&nbsp;&nbsp;</span> <span>${createDate }</span>
		</div>
	</div>
	<div class="selected_bg accurate_bg"></div>
	<div class="information_content">
		<div class="info-content info-content2">
			<div class="info_content3">
				<span class=" accurate_left">投放时间&nbsp;&nbsp;:&nbsp;&nbsp;</span> <span>${paramsMap.time }</span>
			</div>
		</div>
		<div class="total_content">
			<div class="total">
				<div class="total_pic">
					<img
						src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/diamond.png"
						class="search_pic">
				</div>
				<span>合计&nbsp;:&nbsp;</span> <span>${paramsMap.totalPrice }元</span>
			</div>
		</div>
		<div class="total_content none_border">
			<div class="order_content publishPut_bottom">
				<a href="javascript:void(0);" onclick="cancelOrder(${pid})">取消订单</a>
				<a href="javascript:void(0);" class="green Green"
					onclick="payNow(${pid},${paramsMap.totalPrice })">立即付款</a>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript"
	src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/jquery-2.1.0.js"></script>
<script type="text/javascript"
	src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/publish/publishPut_detail.js"></script>
<script type="text/javascript">
    	window.onload = function(){
    		//生成订单的时间
    		var createDate = '<%=createDate%>';
    		var startTime = new Date(); 
    	}
    	function cancelOrder(pid){
    		//alert(pid);
    		//window.location.href = "";
    	}
    	function payNow(pid,payMoney){
    		//alert(pid);
    		//alert(payMoney);
    	}
    </script>
</html>