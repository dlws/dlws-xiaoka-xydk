<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2/css/mui.picker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2/css/swiper-3.3.1.min.css" />
		
		<script type="text/javascript" src="${path}/xkh_version_2/js/jquery-2.1.0.js" ></script>
		<script type="text/javascript" src="${path}/xkh_version_2/js/mui.min.js" ></script>
		<script type="text/javascript" src="${path}/xkh_version_2/js/mui.picker.js" ></script>
		<script type="text/javascript" src="${path}/xkh_version_2/js/mui.poppicker.js" ></script>
		<script type="text/javascript" src="${path}/xkh_version_2/js/swiper-3.3.1.jquery.min.js"></script>
		<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<title>发布服务</title>
	</head>
	
	<body>
		<!--遮罩-->
		<div class="webservice_mask mask_none"></div>
		<div class="webservice_container">
       <!--轮播图-->
		<%-- <div class="lunbo personal_lunbo">
				<div class="swiper-container personal_swiper">
		            <div class="swiper-wrapper">
		             <c:forEach items="${skillBannerInfo}" var="skillBannerInfo">
				    	<div class="swiper-slide"><img src="${skillBannerInfo.picUrl }?${addskill_banner}"/></div>
				    </c:forEach> 
		   			</div>
      		   </div>
       </div> --%>
       <c:if test="${not empty  skillBannerInfo }">
       	<div class="enterInfo_top">
			<img src="${skillBannerInfo.picUrl }?${addskill_banner}" class="search_pic"  />
		</div>
       </c:if>
       <c:if test="${empty  skillBannerInfo }">
       	<div class="enterInfo_top">
			<img src="${path}/xkh_version_2/img/user_banner.jpg?${addskill_banner}" class="search_pic"  />
		</div>
       </c:if>
       
		<div class="webservice_box1">
		<form method="post" action="<%=basePath %>skillUser/addSkillInformation.html" id="formId">
			<ul class="message_content">
				<li>
					<span class="enter_name">资源类型</span>
					<div class="choose_webservice mui-input-row" id="choose_webservice">
						<span class="myInfo_name webs_style result-tips">请选择资源类型</span>
						<span class="myInfo_name show-result"></span>
						<span class="iconfont icon-youjiantou enter_youjiantou"></span>
					</div>
				</li>
				<li>
					<span class="enter_name">服务名称</span>
					<input name="skillName" id="skillName" type="text" placeholder="请填写服务名称" />
				</li>
				<li>
					<span class="enter_name">图&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;片</span>
					<div class="webservice_content">
						<!--添加的图片-->
						<div class="picture_box" id ="picture_box">
							
							
						</div>
						<!--<添加的图片完-->
						
						<div class="add_pic" onclick="uploadImage()">
							<span class="iconfont icon-pic"></span>
							<span class="size">+添加图片</span>
						</div>
					</div>
					<p class="limit">*最多可上传九张服务照片</p>
				</li>
				<li>
					<span class="enter_name">服务价格</span>
					<input name="skillPrice" id="skillPrice" type="text" placeholder="输入价格" onkeyup="value=value.replace(/[^\d.]/g,'')" />
					<p class="webservvice_intro">*(其他同类型服务者所报均价 : 43.00元/小时)</p>
				</li>
				<li>
					<span class="enter_name">单&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;位</span>
					<div id="choose_price" class="mui-input-row">
						<span class="myInfo_name result-tips">请选择服务价格单位</span>
							<span class="myInfo_name show-result"></span>
					   <span class="iconfont icon-youjiantou enter_youjiantou"></span>
					</div>
					
				</li>
				<li>
					<span class="enter_name1">服务类型</span>
					<div class="enter_choose">
						<div class="enter_left" data-name="1">
							<span class="iconfont icon-xianshang icon_color"></span>
							<span class="team">线上</span>
						</div>
						<span class="enter_line"></span>
						<div class="enter_right" data-name="2">
							<span class="iconfont icon-xianxia"></span>
							<span class="team">线下</span>
						</div>
					</div>
				</li>
				<li>
					<span class="enter_nameT">服务介绍</span>
					<div class="textarea">
					  <textarea name="skillDepict" id="skillDepict" value='' onkeydown="showLen(this);" onkeyup="showLen(this);" class="introduce" placeholder="请简要描述服务优势,过往案例等"></textarea>
					 <span id="span" class="span"></span>
					</div>
					
				</li>
			</ul>
			<input type="hidden" name="skillFatId" id="skillFatId" value="">
			<input type="hidden" name="skillSonId" id="skillSonId" value="">
			<input type="hidden" name="serviceType" id="serviceType" value="1">
			<input type="hidden" name="company" id="company" value="">
			<input type="hidden" name="imageUrl" id="imageUrl" value="">
			</form>
			<!--底部提交-->
			<form class="enter_form"  onsubmit="return false">
				<button onclick="javascript:addInfo()" >提交</button>
			</form>
		</div>
		</div>
	</body>
	
	<script id="classSonId" type="text/html">
			{{each list as value i}}
				<li id="{{value.value}}"><span>{{value.text}}</span></li>
		    {{/each}}
	</script>
	
	<script type="text/javascript" src="${path}/xkh_version_2/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/swiper-3.3.1.jquery.min.js"></script>
	<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
	</script>
	<script>
		//字数限制
		showLen(document.getElementById("skillDepict"));
		function showLen(obj){
		   document.getElementById('span').innerHTML = ''+ (obj.value.length) +'/200';
		   
		  if(obj.value.length>200){
			  obj.value=obj.value.substring(0,200);
			  return false;
			  }
		} 		
			
		//轮播部分
		var mySwiper = new Swiper('.swiper-container',{
			loop:true,
			autoplay:3000,
			autoplayDisableOnInteraction:false, 
			pagination:'.swiper-pagination',
			paginationClickable:true
		})
	
	  /*  var style="";
		$(".choose_webservice").click(function(){
			$(".webservice_box1").hide();
			$(".webservice_box2").show();
		});
		//选择资源类型中的一级
		$(".webservice_styleContent li").click(function(){
			var index=$(this).index();
			$("span",this).addClass("webservice_color");
		    $(this).siblings().find("span").removeClass("webservice_color");
		    $(".webservice_styleContent1").css("display","block");
		    var fid = this.id;
		    $("#skillFatId").val(fid);
		    goToTwoList(fid);
		    
		    //选择资源类型中的二级
			$(".webservice_styleContent1 li").click(function(){
			   var index=$(this).index();
			   $("span",this).addClass("webservice_color");
			   $(this).siblings().find("span").removeClass("webservice_color");
			   style=$(this).text();
			   $("#skillSonId").val(this.id);
			   $(".webservice_form").css("background","#1D9243");
			   
				$(".webservice_form button").css("background","#1D9243");
				$(".webservice_form").click(function(){
					setTimeout(function(){
						$(".webservice_box2").hide();
						$(".webservice_box1").show();
						$(".webs_style").html(style);
					},1000);
				});
			});
		    
		}); */
		
		$(".enter_choose div").click(function(){
		     $(".enter_choose span").removeClass("icon_color");
			 $(">span:first-child",this).addClass("icon_color");
			 var status = this.dataset.name;
			 $("#serviceType").val(status);
		});
		//点击图片上的删除按钮删除图片
	/* 	$(".icon-shanchu").click(function(){
			
		     $(this).parent().hide();
			
		}); */
		
		function removeImg(imageId){
			$("#xkh_"+imageId).remove();
			//$("#xkh_"+imageId).hide();
		}
			
		    
		//微信上传 全局变量
		var wxSelf;
		var localArr = new Array();
		var serverArr = new Array();

		/*001 选择图片*/
		function uploadImage() {
			
			var clientUrl = window.location.href;
			var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
					//请求后台，获取jssdk支付所需的参数
					$.ajax({
						type : 'post',
						url : reqPath,
						dataType : 'json',
						data : {
							"clientUrl" : clientUrl
						//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
						},
						cache : false,
						error : function() {
							alert("系统错误，请稍后重试");
							return false;
						},
						success : function(data) {
							//微信支付功能只有微信客户端版本大于等于5.0的才能调用
							var return_date = eval(data);
							/* alert(return_date ); */
							if (parseInt(data[0].agent) < 5) {
								alert("您的微信版本低于5.0无法使用微信支付");
								return;
							}
							//JSSDK支付所需的配置参数，首先会检查signature是否合法。
							wx.config({
								debug : false, //开启debug模式，测试的时候会有alert提示
								appId : return_date[0].appId, //公众平台中-开发者中心-appid
								timestamp : return_date[0].config_timestamp, //时间戳
								nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
								signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
								jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
							});
							//上方的config检测通过后，会执行ready方法
							wx.ready(function() {
								wxSelf=wx;
								wx.chooseImage({
								    count: 9, // 默认9
								    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
								    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
								    success: function (res) {
										
								    	localArr= res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片

								        if(localArr.length > 0){
									       
						        			var result = "";
						        			for(var j = 0; j < localArr.length; j++){
						        				result += "<div id='xkh_"+j+"' class='add_picture'>"+
															"<img src='"+localArr[j]+"' class='search_pic' />"+
															"<span onclick='removeImg("+j+")' class='iconfont icon-shanchu' ></span>"+
															"<span class='cover'>封面1</span>"+
														  "</div>"
						        			}
						        			
						        			$("#picture_box").append(result);
								        	
								        	
								        }
								        
								    }
								});

							});
							wx.error(function(res) {
								alert(res.errMsg);
							});
						}
					});
				}
				// var serverStr="";
				/* 使用递归的方式进行上传图片*/
				function uploadImages() {
		            if (localArr.length == 0) {
		            	
		            	var serverStr="";
			            for(var j=0;j<serverArr.length;j++){
			            	serverStr+=serverArr[j]+";" ;
			            }
		                $.ajax({
			        		type : "post",
			        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
			        		dataType : "json",
			        		data : {serverId:serverStr},
			        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
			        			
			        			//alert("+++++++++data++++++:"+data);	
			        			var imageList = data;
			        			//var pathList = data.
			        			
			        			var imgPath="";
			        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
								for(var f=0;f<imageList.length;f++){
							//		alert("+++++++++++++++:"+imageList[f]);
									var imgPathTemp = imageList[f];
									imgPath += imgPathTemp+";";
								}
								
								$("#imageUrl").val(imgPath);
							//	alert("===========最终赋值的imgUrl============"+$("#imageUrl").val());
			        			$("#formId").submit();
			        		}
			        	});	
		            }
		            var localId = localArr[0];
		            //tmd 一定要加     解决IOS无法上传的坑 
		            if (localId.indexOf("wxlocalresource") != -1) {
		                localId = localId.replace("wxlocalresource", "wxLocalResource");
		            }
		            wxSelf.uploadImage({
		                localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
		                isShowProgressTips: 0, // 默认为1，显示进度提示
		                success: function (res) {
		                   // serverIds.push(res.serverId); // 返回图片的服务器端ID
		                   var serverId = res.serverId; // 返回图片的服务器端ID
		                    serverArr.push(serverId);
		                    localArr.shift();
		                    uploadImages(localArr);
		                    serverStr+=serverId+";" ;
		                   // alert("localArr的长度："+localArr.length);
		                },
		                fail: function (res) {
		                    alert("上传失败，请重新上传！");
		                }
		            });
		        }
		
				
				/*003 从微信下载到自定义服务器*/
				function downloadImageSelf(){
					//alert("执行上传 count"+serverArr);  
		            var serverStr="";
		            for(var j=0;j<serverArr.length;j++){
		            	serverStr+=serverArr[j]+";" ;
		            }
		            
					//alert("server Str is "+serverStr);
		            $.ajax({
			        		type : "post",
			        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
			        		dataType : "json",
			        		data : {serverId:serverStr},
			        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
			        			var resultImgPath = data.path;
			        		//	alert(resultImgPath +"++++++++++++++++++++");
			        			$("#imageUrl").val(resultImgPath);
			        			//submit
			        			$("#formId").submit();
			        		}
			        	});	
				   }
				//选择资源类型
				
				mui.init();
					var picker2 = new mui.PopPicker({
						   layer:2
							});
					 picker2.setData(${skilList});  
		 var showCityPickerButton2 = document.getElementById('choose_webservice');
 							showCityPickerButton2.addEventListener('tap', function(event) {
 								if($("div").hasClass("mui-active")){
 									
 								}else{
 									picker2.show(function(selectItems) {
 										document.querySelector('#choose_webservice .result-tips').style.display = "none";
 										document.querySelector('#choose_webservice .show-result').style.display = "block";
 										document.querySelector('#choose_webservice .show-result').innerText = selectItems[0].text;		
 										$("#skillFatId").val(selectItems[0].value);
 										$("#skillSonId").val(selectItems[1].value);
 									});
 								}
								
							}, false);
			
		//选择服务价格
		mui.init();
			var picker = new mui.PopPicker({
					
					});
			 picker.setData(${company});  
 var showCityPickerButton = document.getElementById('choose_price');
					showCityPickerButton.addEventListener('tap', function(event) {
						if($("div").hasClass("mui-active")){
							
						}else{
							picker.show(function(selectItems) {
								document.querySelector('#choose_price .result-tips').style.display = "none";
								document.querySelector('#choose_price .show-result').style.display = "block";
								document.querySelector('#choose_price .show-result').innerText = selectItems[0].text;	
								$("#company").val(selectItems[0].value);
							});
						}
						
					}, false);
					
							
					//获取二级分类
		function goToTwoList(fid){
						
			$.ajax({
                url:"${path}/skillUser/getClassSon.html",
                type:"post",
                data:{fid:fid},
                dataType : "json",
 				async : false,
                success:function(data){
                	 
        			var classSon=data.classSon;
        			var classSonStr = template('classSonId',JSON.parse(classSon));
        			document.getElementById('webservice_styleContent1').innerHTML = classSonStr; 
        			/* var result = '';
	                for(var j = 0; j < data.classList.length; j++){
	                	result += '<li><span>发发发</span></li>';
	               	} 
	                document.getElementById('webservice_styleContent1').innerHTML = result;*/
                },
                error:function(e){
                    alert("错误！！");
                }
            });				
		}
		
		function addInfo(){
			var skillFatId=$("#skillFatId").val();
			var skillSonId=$("#skillSonId").val();
			var skillName=$("#skillName").val();
			var imageUrl=$("#imageUrl").val();
			var skillPrice=$("#skillPrice").val();
			var company=$("#company").val();
			var skillDepict=$("#skillDepict").val();
			if((skillFatId==""||skillFatId==null) || (skillSonId==""||skillSonId==null)){
				alert("请选择二级分类");
				return;
			}
			if(skillName==""||skillName==null){
				alert("请输入服务名称");
				return;
			}
			if(skillPrice==""||skillPrice==null){
				alert("请输入服务价格");
				return;
			}
			if(company==""||company==null){
				alert("请选择价格单位");
				return;
			}
			if(skillDepict==""||skillDepict==null){
				alert("请输入服务介绍");
				return;
			} 
			//uploadImageSelf();
			var h=$(".webservice_container").height();
		    $(".webservice_mask").css("height",h);
		 	
			$(".webservice_mask").toggleClass("mask_none");
			
			uploadImages();
		};
		
	</script>
</html>

