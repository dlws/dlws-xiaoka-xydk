<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css"/>
		<title>校咖汇</title>
	</head>
	<body>
		<div class="transit_content">
			<ul class="transit_box">
				<li>在校咖，我们将帮助您找到最优质的的高校技能及人才</li>
				<li>校咖平台的技能社团及人才可为你提供以下服务:</li>
				<li>1.注册推广(app注册下载、app评论、微信关注 )</li>
				<li>2.校园媒体合作(公众号宣传、微博宣传、广播站宣传、校刊杂志)</li>
				<li>3.校园代理(销售代理、品牌大使)</li>
				<li>4.传单派发</li>
				<li>5.海报张贴</li>
				<li>6.活动承办(宣讲会、摊位宣传、晚会冠名、比赛赞助)</li>
				<li>7.活动人员(群众演员、促销兼职、机动人员)</li>
				<li>8.活动场地(场地申请、场地布置)</li>
				<li>9.活动商演(主持人、模特礼仪、舞蹈、声乐)</li>
				<li>10.问卷调查(线上线下问卷调查)</li>
			</ul>
			
		</div>
		<!-- 跳转入驻基本信息填写页面 
			暂定传入ID信息
		-->
		<a href="#" class="transit_a">朕已阅</a>
		<div class="transit_bottom">
			<img src="${path}/xkh_version_2/img/release.png" class="search_pic"  />
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2/js/jquery-2.1.0.js" ></script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
</html>