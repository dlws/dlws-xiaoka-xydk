<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css"/>
	    
		<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<title>校咖汇</title>
	</head>
	<style>
		html,
		body {
			width: 100vw;
			height: 100vh;
			display: flex;
			justify-content: space-between;
			flex-flow: column nowrap;
		}
	</style>
	<body>
	<section>
		<input type="hidden" id="id" name="id" value="${map.id}">
		<div class="user_top">
			<img id="backPicUrl" src="${path}/xkh_version_2/img/myBg.png" class="search_pic"/>
			<div class="personal_pic">
			<c:choose>
				<c:when test="${reduMap.headImgUrl!=''&&reduMap.headImgUrl!= null}">
					<img style="border-radius:100%;" src="${reduMap.headImgUrl}" class="search_pic"/>
				</c:when>
				<c:otherwise>
					<img style="border-radius:100%;" src="${path}/xkh_version_2/img/wxHead.png" class="search_pic"/>
				</c:otherwise>			
			</c:choose>
			</div>
			
			<c:choose>
				<c:when test="${reduMap.wxname!=''&&reduMap.wxname!=null}">
					<span class="user_name">${reduMap.wxname}</span>
				</c:when>
				<c:otherwise>
					<span class="user_name">游客</span>
				</c:otherwise>			
			</c:choose>
			
			<a href="editor.html">
	  			<span class="iconfont icon-tianxiexinxi user_message"></span>
	  		</a>
	  </div>
	  <!-- 仅${status=='1'}时通过 -->
	  <c:if test="${map.auditState=='1'}">
	  		<!--服务管理部分-->
						<div class="service_manage">
							<div class="user_function">
							<h3 class="user_h3">我的服务</h3>
							<a href="${path }/comunityBigV/getPersonUrl.html?id=${map.id}&enterId=${map.enterId}" class="iconfont icon-youjiantou user_a"></a>
							</div>
						<div class="user_content">
							<a href="${path }/skillUser/toAddSkill.html">
								<div class="user_picContent">
									<span class="user_picture"><img src="${path}/xkh_version_2/img/user_pic1.png" class="search_pic" /></span>
									<span class="user_size">发布服务</span>
								</div></a>
							<a href="${path }/skillUser/toSkillInfoV2.html">
								<div class="user_picContent">
									<span class="user_picture"><img src="${path}/xkh_version_2/img/user_pic2.png" class="search_pic" /></span>
									<span class="user_size">服务管理</span>
								</div></a>
							<a href="${path }/highSchoolSkill/toAllUpdateUser.html?myself=yes">
							<div class="user_picContent">
								<span class="user_picture"><img src="${path}/xkh_version_2/img/user_pic3.png" class="search_pic" /></span>
								<span class="user_size">我的信息</span>
							</div></a>
							<%-- <a href="">
							<div class="user_picContent user_picR">
								<span class="user_picture"><img src="${path}/xkh_version_2/img/user_pic4.png" class="search_pic" /></span>
								<span class="user_size">管理员更换</span>
							</div></a> --%>
						</div>
						<div class="user_bg"></div>
						</div>
	  </c:if>
	  <!--功能管理部分-->
			<div class="user_function">
				<h3 class="user_h3">功能管理</h3></div>
			<div class="user_content">
				<a href="https://jinshuju.net/f/W4u6uC" class="user_picContent">
					<span class="user_picture"><img src="${path}/xkh_version_2/img/user_img1.png" class="search_pic" /></span>
					<span class="user_size">用户反馈</span>
				</a>
				<!-- http://p.qiao.baidu.com/cps/chat?siteId=9581947  百度商桥更换链接为咔咔熊 -->
				<a href="https://static.meiqia.com/dist/standalone.html?eid=50806" class="user_picContent">
					<span class="user_picture"><img src="${path}/xkh_version_2/img/user_img2.png" class="search_pic" /></span>
					<span class="user_size">在线咨询</span>
				</a>
				<a href="http://mp.weixin.qq.com/s/wDEs84EoA2p4iovtQVDXJg" class="user_picContent">
					<span class="user_picture"><img src="${path}/xkh_version_2/img/user_img3.png" class="search_pic" /></span>
					<span class="user_size">常见问题</span>
				</a>
				<a href="http://mp.weixin.qq.com/s/t2a6PYa574RwnWX1WBnhJQ" class="user_picContent">
					<span class="user_picture"><img src="${path}/xkh_version_2/img/user_img4.png" class="search_pic" /></span>
					<span class="user_size">关于平台</span>
				</a>
			</div>
			<div class="user_bg"></div>
			<!--审核中 部分-->
			 <c:choose> 
				<c:when test="${map.auditState=='0'}"> 
				
					<div class="no_review">
					<a href="${path }/highSchoolSkill/toAllUpdateUser.html?myself=no">
						<p>+ 校园资源入驻</p>
						<p>(审核中)</p>
						
					</a>
					</div>
				</c:when> 
				<c:when test="${map.auditState=='1'}"> 
					<!-- <div class="in_review">校园资源入驻(审核成功)</div> -->
				</c:when> 
				<c:when test="${map.auditState=='2'}"> 
					<div class="no_review">
						<a href="${path }/highSchoolSkill/toAllUpdateUser.html?myself=no">
							<p>+ 校园资源入驻</p>
							<p>(审核不通过,请重新提交)</p>
						</a>
					</div>
				</c:when> 
				<c:when test="${map.auditState=='3'}"> 
					<div class="no_review">
						<a href="${path }/skillUser/chooseStyle.html">
							<p>+ 校园资源入驻</p>
							<p>(未入驻)</p>
						</a>
					</div>
				</c:when> 
				<c:otherwise>
						<div class="in_review">当前用户异常</div>
				</c:otherwise>
	    </c:choose>
		</section>

		<!--底部条-->
		<nav class="foot-bar-tab">
			<a class="foot-tab-item" href="${path }/home/index.html">
				<span class="foot-icon foot-icon-home"></span>
				<span class="foot-tab-label">首页</span>
			</a>
			<a class="foot-tab-item" href="${path}/prompt/prompt.html">
				<span class="foot-icon foot-icon-pub"></span>
				<span class="foot-tab-label">发布</span>
			</a>
			<a class="foot-tab-item" href="${path}/prompt/prompt.html">
				<span class="foot-icon foot-icon-bus"></span>
				<span class="foot-tab-label">已选</span>
			</a>
			<a class="foot-tab-item foot-active" href="${path }/home/getmyInform.html">
				<span class="foot-icon foot-icon-person"></span>
				<span class="foot-tab-label">我的</span>
			</a>
		</nav>
	  <!--1为审核成功   0 为未审核  2为审核不通过   3.为未入驻  other 其他状态为用户异常状态 
	  <a href="getUserInfo.html?id=${id}">传输ID跳转个人空间</a>
	    <c:choose> 
	    
				<c:when test="${status=='0'}"> 
					<div >	未入驻用户  </div>
				</c:when> 
				<c:when test="${status=='2'}"> 
						<div>审核不通过的用户</div>
				</c:when> 
				<c:when test="${status=='1'}">
						
						<p>我的服务</p>    <a style="float:right;" href="${path }/home/getUserInfo.html?id=${map.id}">》</a>
						<table width="100%" border="1px;">
							<tr>
							
								<td background="#66ffcc">
									<a href="${path }/skillUser/toAddSkill.html">发布服务</a>
								</td>
								<td>
									<a href="${path }/skillUser/toSkillInfo.html">服务管理</a>
								</td>
								<td>
									<a>订单管理</a>
								</td>
								<td>
									<a>我的钱包</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href="${path }/skillUser/toAddSkillUser.html">我的信息</a>
								</td>
								<td>
									<a>管理员更换</a>
								</td>
								<td>
									<a></a>
								</td>
								<td>
									<a></a>
								</td>
							</tr>
						</table>
				
				</c:when> 
				<c:otherwise>
						<div>审核中的用户</div>
				</c:otherwise>
	    </c:choose>-->
	</body>
<script type="text/javascript" src="${path}/xkh_version_2/js/jquery-2.1.0.js" ></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">


</script>
<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
</html>
