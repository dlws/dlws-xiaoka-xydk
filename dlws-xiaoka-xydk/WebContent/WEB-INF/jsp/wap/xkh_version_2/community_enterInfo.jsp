<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/iconfont/iconfont.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/mui.picker.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/mui.poppicker.css"/>
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/style.css" />
		<!-- <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script> -->
		<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
		<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
		<title>社群大V入驻</title>
	</head>
	<body>
	<!--遮罩-->
		<div class="webservice_mask mask_none"></div>
		<div class="webservice_container">
		<div class="enterInfo_top">
		<c:forEach items="${picUrl}" var="img">
			<img src="${img.picUrl}" class="search_pic" />
		</c:forEach>
		<c:if test="${picUrl==null||picUrl==''}">
			<img src="${path}/xkh_version_2.1/img/community_topV.jpg" class="search_pic" />
		</c:if>
		</div>
		<form action="<%=basePath%>comunityBigV/addSettleUser.html" id="addPartForm" method="post" enctype="multipart/form-data">
		<ul class="message_content">
			<li>
				<span class="enter_name">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span>
				<div id="activeCity" class="mui-input-row">
					<span class="myInfo_name enter_city result-tips">选择您所在的城市</span>
					<span class="myInfo_name show-result">${basicInfo.cityName}</span>
					<input type="hidden" name="cityId" id="cityId" value="${basicInfo.cityId}"/>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;校</span>
				<div id="activeShcool" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">选择您所在的学校</span>
					<span class="myInfo_name show-result">${basicInfo.schoolName}</span>
					<input type="hidden" name="schoolId" id="schooId" value="${basicInfo.schoolId}"/>
				   <span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
			
				<p class="place_name">资源类型</p>
				<div class="icon_box">
				
				<c:if test="${paramsMap.addFlag=='edit'}">
				   <ul class="zylxUl">
					<c:forEach items="${settleList}" var="one">
						<c:if test="${one.typeName=='zylx'}">
							<c:forEach items="${resourceList}" var="item" varStatus="status">
							<c:choose>
								<c:when test="${one.typeValue==item.classValue}">
									<c:if test="${item.classValue!='zmt'}">
										<div class="community_title choose_card1">
										<input type="hidden" class="zylxImg active" id="zylx" name='zylx' value="${item.classValue}"/>
											<span class="community_icon"><img src="${path}/xkh_version_2.1/img/community_g${status.index+1}.png" class="search_pic" /></span>
											<span class="community_size">${item.className}</span>
										</div>
									</c:if>
									<c:if test="${item.classValue=='zmt'}">
										<div class="community_title choose_card2">
										<input type="hidden" class="light" id="light">
										<input type="hidden" class="zylxImg active" id="zylx" name='zylx' value="${item.classValue}"/>
										<input type="hidden" id="checkType"  value="1"/>
											<span class="community_icon"><img src="${path}/xkh_version_2.1/img/community_g${status.index+1}.png" class="search_pic" /></span>
											<span class="community_size">${item.className}</span>
										</div>
									</c:if> 
								</c:when>
								<c:otherwise>
								 	<c:if test="${item.classValue!='zmt'}">
										<div class="community_title choose_card1">
										<input type="hidden" class="zylxImg" id="zylx" value="${item.classValue}"/>
											<span class="community_icon"><img src="${path}/xkh_version_2.1/img/community_0${status.index+1}.png" class="search_pic" /></span>
											<span class="community_size">${item.className}</span>
										</div>
									</c:if>
									<c:if test="${item.classValue=='zmt'}">
										<div class="community_title choose_card2">
										<input type="hidden" class="zylxImg" id="zylx" value="${item.classValue}"/>
											<span class="community_icon"><img src="${path}/xkh_version_2.1/img/community_0${status.index+1}.png" class="search_pic" /></span>
											<span class="community_size">${item.className}</span>
										</div>
									</c:if>
								</c:otherwise>
							</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
				     </ul>
					</c:if>
					
					<c:if test="${paramsMap.addFlag=='add'}">
				    <ul class="zylxUl">
					<c:forEach items="${resourceList}" var="item" varStatus="status">
						<c:if test="${item.classValue!='zmt'}">
							<div class="community_title choose_card1">
							<input type="hidden" class="zylxImg" id="zylx" value="${item.classValue}"/>
								<span class="community_icon"><img src="${path}/xkh_version_2.1/img/community_0${status.index+1}.png" class="search_pic" /></span>
								<span class="community_size">${item.className}</span>
							</div>
						</c:if>
						<c:if test="${item.classValue=='zmt'}">
							<div class="community_title choose_card2">
							<input type="hidden" class="zylxImg" id="zylx" value="${item.classValue}"/>
								<span class="community_icon"><img src="${path}/xkh_version_2.1/img/community_0${status.index+1}.png" class="search_pic" /></span>
								<span class="community_size">${item.className}</span>
							</div>
						</c:if>
					</c:forEach>
				    </ul>
					</c:if>
				</div>
			</li>
			
			<li>
				<span class="enter_name userName">群&nbsp;&nbsp;名&nbsp;&nbsp;称</span>
				<input class="inp" type="text" id="userName" name="userName" value="${basicInfo.userName}" placeholder="请填写群名称" required="required"/>
				<p class="warn_name"></p>
			</li>
			<!--前三个兼职选择图-->
			<div class="message_content community_content1">
				
			<div id="parTime">
			<c:forEach items="${settleList}" var="one" varStatus="stat">
				<c:if test="${one.typeName=='groupNum'}">
					<ul class="groupNumUl">
					<li>
						<span class="enter_name">群&nbsp;&nbsp;人&nbsp;&nbsp;数</span>
						<input class="inp" type="number" onkeyup="value=value.replace(/[^\d]/g,'')" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))"  onkeyup="value=value.replace(/[^\d]/g,'') "onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"
 name="groupNum" id="groupNum" value="${one.typeValue }" placeholder="请填写社群人数" required="required"/>
 						<p class="warn_groupNum"></p>
					</li>
					</ul>
				</c:if>
			</c:forEach>
					<ul class="groupNumUl">
					<li>
						<span class="enter_name">群&nbsp;&nbsp;人&nbsp;&nbsp;数</span>
						<input class="inp" type="number" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))"  onkeyup="value=value.replace(/[^\d]/g,'') "onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" name="groupNum" id="groupNum" placeholder="请填写社群人数" required="required"/>
						<p class="warn_groupNum"></p>
					</li>
					</ul>
			</div>
						
				
				
			</div>
			<!--自媒体选择出来的图-->
			<div class="message_content community_content2 community_none">
			<c:forEach items="${settleList}" var="one">
				<c:if test="${one.typeName=='pubNumId'}">
				<ul class="pubNumIdUl">
					<li>
						<span class="community_enterName">公众号ID</span>
						<input class="inp wechat_public" type="text" name="pubNumId" id="pubNumId" value="${one.typeValue}" placeholder="请填写公众号微信ID" required="required"/>
						<p class="warn_public"></p>
			  		 </li>
				</ul>
				</c:if>
			</c:forEach>
				<ul class="pubNumIdUl">
				   <li>
						<span class="community_enterName">公众号ID</span>
						<input class="inp wechat_public" type="text" name="pubNumId" id="pubNumId" placeholder="请填写公众号微信ID" required="required"/>
						<p class="warn_public"></p>
				   </li>
			   </ul>
			   
			   <c:forEach items="${settleList}" var="one">
					<c:if test="${one.typeName=='fansNum'}">
						<ul class="fansNumUl">
							<li>
							<span class="community_enterName">粉丝人数</span>
							<input class="inp" type="text" name="fansNum" id="fansNum" value="${one.typeValue}" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" placeholder="请填写公众号粉丝数" required="required"/>
							<p class="warn_fansNum"></p>
					  		</li>
						</ul>
					</c:if>
				</c:forEach>
			   <ul class="fansNumUl">
				   <li>
						<span class="community_enterName">粉丝人数</span>
						<input class="inp" type="text" name="fansNum" id="fansNum" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" placeholder="请填写公众号粉丝数" required="required"/>
						<p class="warn_fansNum"></p>
				   </li>
			   </ul>
			   
			   
			   <c:forEach items="${settleList}" var="one">
					<c:if test="${one.typeName=='readNum'}">
						<ul class="readNumUl">
							<li>
								<span class="community_enterName">平均阅读量</span>
								<input class="inp" type="text" name="readNum" id="readNum" value="${one.typeValue}" placeholder="请填写单篇平均阅读量" required="required"/>
								<p class="warn_readNum"></p>
						   </li>
						</ul>
					</c:if>
				</c:forEach>
				<ul class="readNumUl">
				   <li>
						<span class="community_enterName">平均阅读量</span>
						<input class="inp" type="text" name="readNum" id="readNum" placeholder="请填写单篇平均阅读量" required="required"/>
						<p class="warn_readNum"></p>
				   </li>
			   </ul>
			   
			   <c:forEach items="${settleList}" var="one">
					<c:if test="${one.typeName=='topLinePrice'}">
					<ul class="topLinePriceUl">
					   <li>
					   	  <span class="community_enterName">头条报价</span>
					   	  <input type="text" class="community_inp" name="topLinePrice" id="topLinePrice" value="${one.typeValue}"  placeholder="请填写头条报价" />
					   	  <span class="community_span">元/篇</span>
					   	  <p class="warn_prohibit"></p>
					   </li>
					</ul>
					</c:if>
				</c:forEach>
			   <ul class="topLinePriceUl">		
				    <li>
				   	  <span class="community_enterName">头条报价</span>
				   	  <input type="text" class="community_inp" name="topLinePrice" id="topLinePrice" value="${one.typeValue}"  placeholder="请填写头条报价" />
				   	  <span class="community_span">元/篇</span>
				   	  <p class="warn_prohibit"></p>
				   </li>
			   </ul>
			   
			   <c:forEach items="${settleList}" var="one">
					<c:if test="${one.typeName=='lessLinePrice'}">
					<ul class="lessLinePriceUl">
						<li>
					   	  <span class="community_enterName">次条报价</span>
					   	  <input type="text" class="community_inp" name="lessLinePrice" id="lessLinePrice" value="${one.typeValue}" placeholder="请填写次条报价" required="required"/>
					   	  <span class="community_span">元/篇</span>
					   	  <p class="warn_lowbit"></p>
					   </li>
						</ul>
					</c:if>
				</c:forEach>
				<ul class="lessLinePriceUl">		
				    <li>
				   	  <span class="community_enterName">次条报价</span>
				   	  <input type="text" class="community_inp" name="lessLinePrice" id="lessLinePrice" placeholder="请填写次条报价" required="required"/>
				   	  <span class="community_span">元/篇</span>
				   	  <p class="warn_lowbit"></p>
				   </li>
			   </ul>
			</div>
			
			    <li>
				<span class="community_enterName headPic">公众号头像</span>
						<c:if test="${basicInfo.headPortrait!=''&&basicInfo.headPortrait!=null}">
							<span class="enter_jia myInfo_photo">
							<img id="myAvatar" name="" class="search_pic" src="${basicInfo.headPortrait}"/>
							<input type="hidden" id="myAvatarVal" value="${basicInfo.headPortrait}">
							</span>
						</c:if>
						<c:if test="${basicInfo.headPortrait==''||basicInfo.headPortrait==null}">
							<span class="enter_jia">
							<span id="addText">点击加号上传头像</span>
							<img id="myAvatar" name="" src="" class="search_pic" style="display:none;"/>
							<input type="hidden" id="myAvatarVal">
							</span>
						</c:if>
				<span class="iconfont icon-jiahao new_jia enter_youjiantou icon_pic" onclick="uploadImage(1,1);"></span>
				</li>
				
			 <li>
				<span class="community_enterName diaryPic">日常推文图</span>
				<span class="myInfo_name diaryPic_rd">照片需体现阅读量哦</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="picture_box_daily">
						
						<c:if test="${paramsMap.addFlag=='edit'}">
						<c:forEach items="${settleList}" var="one">
							<c:choose>
								<c:when test="${one.typeName=='dairyTweetUrl'}">
								<c:if test="${not empty dairyTweetImg}">
									<c:forEach items="${dairyTweetImg}" var="img" >
									 <div class="add_picture">
										<img src="${img}" class="search_pic" />
										<span onclick="delImgDairyTUrl('${basicInfo.xkh_dairyImgId}','${img}',this)" class="iconfont icon-shanchu"></span>
										<span class="cover"></span>
									</div>
									</c:forEach>
								</c:if>
								</c:when>
							</c:choose>
						</c:forEach>
						</c:if>
					
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage(2,9);">
						<span class="iconfont icon-pic"></span>
						<span class="size" >+添加图片</span>
					</div>
				</div>
				<p class="limit diaryPicLimit">*请上传2-9张日常推文图</p>
			</li>
			
			<li>
				<p class="community_name">活动图片(选填)</p>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="picture_box_activity">
					<c:if test="${paramsMap.addFlag=='edit'}">
					<c:forEach items="${settleList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='activtyUrl'}">
							<c:if test="${not empty imgArr}">
								<c:forEach items="${imgArr }" var="img" >
								 <div class="add_picture">
									<img src="${img}" class="search_pic" />
									<span onclick="delImgActivtyUrl('${basicInfo.xkh_imgId }','${img}',this)" class="iconfont icon-shanchu"></span>
									<span class="cover"></span>
								</div>
								</c:forEach>
							</c:if>
							</c:when>
						</c:choose>
					</c:forEach>
					</c:if>
					
					
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage(3,9);">
						<span class="iconfont icon-pic"></span>
						<span class="size" >+添加图片</span>
					</div>
				</div>
				<p class="limit">*请上传2-9张活动图片</p>
			</li>
			
			
			<c:forEach items="${settleList}" var="one">
				<c:choose>
					<c:when test="${one.typeName=='activCase'}">
					<ul class="activCaseUl">
						<li class="activCase">
							<p class="community_name" >活动案例(选填)</p>
							<div class="textarea">
								<textarea class="introduce" name="activCase" id="activCase" onkeyup="load()"  placeholder="请填写社群组织开展过的活动，群成员参与情况等" required="required">${one.typeValue }</textarea>
								<div id="span" class="span"><span>0</span>/200</div>
							</div>
						</li>
						</ul>
					</c:when>
				</c:choose>
			</c:forEach>
					<ul class="activCaseUl">
						<li class="activCase">
							<p class="community_name" >活动案例(选填)</p>
							<div class="textarea">
								<textarea class="introduce" name="activCase" id="activCase" onkeyup="load()"  placeholder="请填写社群组织开展过的活动，群成员参与情况等" required="required"></textarea>
								<div id="span" class="span"><span>0</span>/200</div>
							</div>
						</li>
						</ul>
								
			<li>
				<span class="enter_nameT">我的介绍</span>
				<div class="textarea">
					<textarea class="introduce2" name="aboutMe" id="aboutMe" onkeyup="load2()"  placeholder="请简述您个人或团队的优势，资源经验等" required="required">${basicInfo.aboutMe}</textarea>
					<div id="span2" class="span"><span>0</span>/200</div>
				</div>
			</li>
			<li>
				<span class="enter_name">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
				<input class="inp" type="text" name="contactUser" id="contactUser" value="${basicInfo.contactUser}" placeholder="填写您的姓名" required="required"/>
				<p class="warn_conect"></p>
			</li>
			<li>
				<span class="enter_name">联系电话</span>
				<input class="phone inp" type="text" name="phoneNumber" id="phoneNumber" value="${basicInfo.phoneNumber}" placeholder="填写您的手机号码" required="required"/>
				<p class="warn_tel"></p>
			</li>
			<li>
				<span class="enter_name">联系邮箱</span>
				<input class="letter inp" type="text" name="email" id="email" value="${basicInfo.email}" placeholder="填写您的联系邮箱" required="required" />
				<p class="warn_letter"></p>
			</li>
			<li>
				<span class="enter_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号</span>
				<input class="wechat inp" type="text" name="wxNumber" id="wxNumber" value="${basicInfo.wxNumber}" placeholder="填写您的微信账号" required="required"/>
				<p class="warn_wechat"></p>
			</li>
			
		</ul>
		<input type="hidden" name='myself' id='' class='input' value="${paramsMap.myself }"/>
		<input type="hidden" id="id" name="id" value="${basicInfo.id}" required="required"/>
		<input type="hidden" id="enterId" name="enterId" value="${paramsMap.enterId}" required="required"/>
		<input type="hidden" id="enterType" name="enterType" value="${paramsMap.enterTypeName}" required="required"/>
		<input type="hidden" name="picUrl" id="picUrl" value="${basicInfo.headPortrait}" required="required"/>
		<input type="hidden" name="dairyTweetUrl" id="dairyTweetUrl" value="${basicInfo.dairyTweetImg}" required="required"/>
		<input type="hidden" name="activtyUrl" id="activtyUrl" value="${basicInfo.imgString}" required="required"/>
		<input type="hidden" id="checkType"   value="0"/>
		</form>
		
		<form class="enter_form" onsubmit="return false" onclick="sub()">
			<button>提交</button>
		</form> 
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.min.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.picker.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.poppicker.js"></script>
	<script src="${path}/xkh_version_2.1/js/city.data.js"></script>
	<script src="${path}/xkh_version_2.1/js/shcool.data.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/enterInfo.js"></script>
	<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
	<script>
	
	function sub(){
		var h=$(".webservice_container").height();
		$(".webservice_mask").css("height",h);
		var phone=$(".phone").val();//电话号码
		var letter=$(".letter").val();//邮箱
		var numzReg = /^\+?[1-9][0-9]*$/;//群人数正则
		var schooId = $("#schooId").val();
		if(schooId==""||schooId==null){
			alert("请选择学校");
			return;
		}
		
		
		var checkType = $("#checkType").val();
		if(checkType==0){
			var userName = $("#userName").val();
			if(userName==""){
			$(".warn_name").html("请输入群名称").css("display","block");
			return;
			}else if(userName.length>10){
				alert("群名称不得超过10个字");	
				$(".warn_name").html("群名称不得超过10个字").css("display","block");
				return;
			}
			
			var groupNum = $("#groupNum").val();
			if(groupNum==""){
				$(".warn_groupNum").html("请输入群人数").css("display","block");
				$(".webservice_mask").addClass("mask_none");
				return
			}
			
			if(!numzReg.test(groupNum)){
				$(".warn_groupNum").html("群人数必须是数字").css("display","block");
				return
			}
			
			var myAvatarVal = $("#myAvatarVal").val();
			if(myAvatarVal==""){
				alert("请上传头像");
				return;
			}
			
			var dailyLh = $("#picture_box_daily .add_picture").length; 
			if(dailyLh<1){
				alert("请上传群成员图");
				return;
			} else if(dailyLh<2||dailyLh>9){
				alert("请上传2-9张图片！");	/* 最多上传2张群成员图 */
				return
			}
		}else if(checkType==1){
			var userName = $("#userName").val();
			if(userName==""){
				$(".warn_name").html("请输入名称").css("display","block");
				return
			}
			
			var pubNumId = $("#pubNumId").val();
			var rePub=/[a-zA-Z][a-zA-Z0-9]+/;
			if(pubNumId==""){
				$(".warn_public").html("请输入公众号ID").css("display","block")
				return
			}
			if(pubNumId!=""){
				if(!rePub.test(pubNumId)){
					$(".warn_public").html("输入格式有误，请重新输入").css("display","block")
					return
				}
			}
			
			
			//粉丝人数
			var fansNum = $("#fansNum").val();
			if(fansNum==""){
				$(".warn_fansNum").html("请输入粉丝人数").css("display","block")
				return
			}
			if(fansNum!=""){
				if(!numzReg.test(fansNum)){
					$(".warn_fansNum").html("粉丝人数只能是数字").css("display","block")
					return
				}
			}
			
			var readNum = $("#readNum").val();
			if(readNum==""){
				$(".warn_readNum").html("请输入平均阅读量").css("display","block")
				return
			}
			
			if(readNum!=""){
				if(!numzReg .test(readNum)){
				$(".warn_readNum").html("平均阅读量必须是数字").css("display","block")
				return
				}
			}
		
			var topLinePrice = $("#topLinePrice").val();
			var tpriceRs = /^\d*\.{0,1}\d{0,1}$/;
			if(topLinePrice==""){
				$(".warn_prohibit").html("请输入头条报价").css("display","block")
				return
			}
			if(topLinePrice!=""){
				if(!tpriceRs.test(topLinePrice)){
					$(".warn_prohibit").html("头条报价必须是数字").css("display","block")
					return
				}
			}
			
			var lessLinePrice = $("#lessLinePrice").val();
			if(lessLinePrice==""){
				$(".warn_lowbit").html("请输入次条报价").css("display","block");
				return
			}
			if(lessLinePrice!=""){
				if(!tpriceRs.test(lessLinePrice)){
					$(".warn_lowbit").html("次条报价必须是数字").css("display","block");
					return
				}
			}
			
			var dailyLh = $("#picture_box_daily .add_picture").length; 
			if(dailyLh<1){
			alert("请上传日常推文图");			
			return;
			}else if(dailyLh>9){
			alert("日常推文图不得超过9张");			
			return;
			}
			
		}
		/* var activLh = $("#picture_box_activity .add_picture").length;
		if(activLh<1){
			alert("请上传活动图片");			
			$(".webservice_mask").addClass("mask_none");
			return;
		}else if(activLh>9){
			alert("活动图片不得超过9张");			
			$(".webservice_mask").addClass("mask_none");
			return;
		} */
		
		
		//电话验证
   		var re=/^1[3|4|7|5|8]\d{9}$/;
   		if(phone==""||!phone){
   			$(".warn_tel").html("请输入手机号").css("display","block");
   			return;
   		}else if(phone!=""){
    		if(!re.test(phone)){
    			$(".warn_tel").html("输入手机格式有误，请重新输入").css("display","block")
    			return;
    		}else{
    			$(".warn_tel").html("").css("display","none")
    		}
   		}
   		
   		
		
   		var contactUser = $("#contactUser").val();//联系人
		if(contactUser==""){
			$(".warn_conect").html("请输入联系人").css("display","block");
			return;
		}
		if(contactUser.length>10){
			$(".warn_conect").html("联系人不得超过10个字").css("display","block");
			return;
		}
   		var activCase = $("#activCase").val();//活动案列
   		if(activCase!=""){
   			if(activCase.length<20||activCase.length>200){
			alert("活动案列请在20——200字之间");
			return
   			}
		}
   		
   		var aboutMe = $("#aboutMe").val();//介绍
   		if(aboutMe==""){
			alert("请输入介绍");
			return
		}
   		if(aboutMe.length<20||aboutMe.length>200){
   			alert("介绍请在20——200字之间");
			return
   		}
   		
		//邮箱验证
		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
		if(letter==""||!letter){
			$(".warn_letter").html("请输入邮箱号").css("display","block");
			return;
		}else if(letter!=""&&letter){
			if(!reg.test(letter)){
				$(".warn_letter").html("输入邮箱格式有误，请重新输入").css("display","block");	
				return;
			}else{
				$(".warn_letter").html("").css("display","none");
			}
	   } 
		
		
        //验证微信号
	 	var wechat=$(".wechat").val()
		var re=/^[a-zA-Z]{1}[-_a-zA-Z0-9]{5,19}$/;
	 	if(wechat==""||!wechat){
	 		$(".warn_wechat").html("请输入微信号").css("display","block");
			return
	 	}
	 	
	 	/* else if(wechat!=""&&wechat){
	 		if(!re.test(wechat)){
	 			$(".warn_wechat").html("输入微信号格式有误，请重新输入").css("display","block");
	 			return
	 		}else{
	 			$(".warn_wechat").html("").css("display","none");
	 		}
	 	} */
	 	
	 	$(".webservice_mask").removeClass("mask_none"); 
	    uploadHeadImageSelf();
	}
	
	$(function(){
		$(".groupNumUl li").css("margin-bottom","0");
		$(".pubNumIdUl li").css("margin-bottom","0");
		$(".fansNumUl li").css("margin-bottom","0");
		$(".readNumUl li").css("margin-bottom","0");
		$(".topLinePriceUl li").css("margin-bottom","0");
		$(".lessLinePriceUl li").css("margin-bottom","0");
		$(".community_inp").css("width","38.2666vw");
		
		var activLh =$(".active").length;
		var arr=["${path}/xkh_version_2.1/img/community_g1.png","${path}/xkh_version_2.1/img/community_g2.png","${path}/xkh_version_2.1/img/community_g3.png","${path}/xkh_version_2.1/img/community_g4.png"];
	    if(activLh<1){
	    	$(".icon_box div").eq(0).find("input").attr("name","zylx").parent().siblings().find("input").removeAttr("name");
	    	$(".icon_box div").eq(0).find("img").attr("src",arr[0]);
	    	
	    	$("#pubNumId").attr("disabled",true);
			$("#fansNum").attr("disabled",true);
			$("#readNum").attr("disabled",true);
			$("#topLinePrice").attr("disabled",true);
			$("#lessLinePrice").attr("disabled",true);
			$("#groupNum").removeAttr("disabled");
			$("#checkType").val(0);
			$(".diaryPic").html("群成员图");
	    }
	    
	    //点亮自媒体
	    var checkType = $("#checkType").val();
	    if(checkType==1){
	    	//$(".activCase").show();
			$(".userName").removeClass("enter_name").addClass("community_enterName").html("公众号名称");
			$(".headPic").html("公众号头像");
			$(".headPic").removeClass("enter_name").addClass("community_enterName");
			$(".diaryPic").html("日常推文图");
			$(".diaryPicLimit").html("*请上传2-9张日常推文图");
			$(".diaryPic_rd").html("照片需体现阅读量哦");
			$(".community_content2").show();
			$(".community_content1").hide();
			
	    	$("#pubNumId").removeAttr("disabled",true);
			$("#fansNum").removeAttr("disabled",true);
			$("#readNum").removeAttr("disabled",true);
			$("#topLinePrice").removeAttr("disabled",true);
			$("#lessLinePrice").removeAttr("disabled",true);
			$("#groupNum").attr("disabled",true);
			$("#checkType").val(1);
	    }else if(checkType==0){
	    	$(".diaryPic").html("群成员图");
	    	$(".headPic").html("社群头像");
	    	$(".headPic").addClass("enter_name").removeClass("community_enterName");
	    	$(".diaryPicLimit").html("请上传2-9张图片！");  /*  *请上传1~2张群成员图 */
			$(".diaryPic_rd").html("照片需体现群人数哦");
	    }
	    
	    
	     $(".groupNumUl").eq(1).remove();
		 $(".pubNumIdUl").eq(1).remove();
		 $(".fansNumUl").eq(1).remove();
		 $(".readNumUl").eq(1).remove();
		 $(".topLinePriceUl").eq(1).remove();
		 $(".lessLinePriceUl").eq(1).remove();
		 $(".activCaseUl").eq(1).remove();
		 $(".zylxUl:gt(3)").remove();
		 
		 
			
			if(cityId!=""&&cityId){
			   	document.querySelector('#activeShcool .result-tips').style.display = "none";
				document.querySelector('#activeShcool .show-result').style.display = "block"; 
				
			 	document.querySelector('#activeCity .result-tips').style.display = "none";
				document.querySelector('#activeCity .show-result').style.display = "block";  
			}
			
	});
	
	
	 //字数限制
     function load2(){
      	 var length=$(".introduce2").val().length;
      	 var html=$(".introduce2").val();
      	 if(length>199){
      	 $(".introduce2").val(html.substring(0,200))
      	 	 $("#span2 span").html(200);
      	 }else{
      	 	 $("#span2 span").html(length)
      	 }
      	
     }
	 
	     //微信公众号正则验证
	     //当文本框失去焦点的时候
		 $(".wechat_public").blur(function(){
		 	var val1=$(".wechat_public").val()
    		var re=/[a-zA-Z][a-zA-Z0-9]+/;
    		if(!re.test(val1)){
    			$(".warn_public").html("输入格式有误，请重新输入").css("display","block")
    		}
    		
    		if(val1==""){
    			$(".warn_public").html("请输入微信公众号").css("display","block");
    		}
    		
		 })
		 
        //当文本框重新获得焦点的时候
        $(".wechat_public").focus(function(){
        	var val1=$(".wechat_public").val()
    		var re=/^[a-zA-Z]{1}[-_a-zA-Z0-9]{5,19}$/;
    		if(!re.test(val1)){
    			$(".warn_public").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_public").html("").css("display","none");
    		}
        })
	 
	
		//入驻名称 失去焦点
		 $("#userName").blur(function(){
		 	var val1=$("#userName").val();
    		if(val1==""){
    			$(".warn_name").html("请输入名称").css("display","block");
    		}
		 })
       
		 //入驻名称获得焦点
        $("#userName").focus(function(){
        	var val1=$("#userName").val();
    		if(val1==""){
    			$(".warn_name").html("").css("display","none");
    		}
        })
        
        var foucsNum = /^\+?[1-9][0-9]*$/;//数字正则 
        
		//群人数 失去焦点
		 $("#groupNum").blur(function(){
		 	var val1=$("#groupNum").val();
	   		if(val1==""){
	   			$(".warn_groupNum").html("请输入群人数").css("display","block");
	   		}
	   		if(val1!=""){
	   		if(!foucsNum.test(val1)){
	   			$(".warn_groupNum").html("群人数只能是数字").css("display","block");
	   	     }
	   		}
	   		if(foucsNum.test(val1)){
	   			$(".warn_groupNum").html("").css("display","none");
	   	     }
		 })
      
		//群人数获得焦点
       $("#groupNum").focus(function(){
       	var val1=$("#groupNum").val();
   		if(val1==""){
   			$(".warn_groupNum").html("").css("display","none");
   		}
   		
   		if(foucsNum.test(val1)){
   			$(".warn_groupNum").html("").css("display","none");
   	     }
   		
       })
        
		
         //粉丝人数失去焦点
		 $("#fansNum").blur(function(){
		 	var val1=$("#fansNum").val();
	   		if(val1==""){
	   			$(".warn_fansNum").html("请输入粉丝人数").css("display","block");
	   		}
	   		if(val1!=""){
		   		if(!foucsNum.test(val1)){
	    			$(".warn_fansNum").html("粉丝人数只能是数字").css("display","block");
	    		}
	   		}
	   		if(foucsNum.test(val1)){
    			$(".warn_fansNum").html("").css("display","none");
    		}
	   		
		 })
      
		//粉丝人数获得焦点
       $("#fansNum").focus(function(){
	       	var val1=$("#fansNum").val();
	   		if(val1==""){
	   			$(".warn_fansNum").html("").css("display","none");
	   		}
	   		
	   		if(foucsNum.test(val1)){
	   			$(".warn_fansNum").html("").css("display","none");
    		}
       })
       
       
     //平均阅读量失去焦点
	 $("#readNum").blur(function(){
	   var readReg = /^\+?[1-9][0-9]*$/;//数字正则 
	 	var val1=$("#readNum").val();
   		if(val1==""){
   			$(".warn_readNum").html("请输入平均阅读量").css("display","block");
   		}
   		if(val1!=""){
	   		if(!readReg.test(val1)){
	 			$(".warn_readNum").html("平均阅读量必须是数字").css("display","block");
	 			}
   		}
   		if(readReg.test(val1)){
   			$(".warn_readNum").html("").css("display","none");
		}
	 })
    
	 //平均阅读量获得焦点
     $("#readNum").focus(function(){
    	var readReg = /^\+?[1-9][0-9]*$/;//数字正则 
       	var val1=$("#readNum").val();
   		if(val1==""){
   			$(".warn_readNum").html("").css("display","none");
   		}
   		if(readReg.test(val1)){
   			$(".warn_fansNum").html("").css("display","none");
		}
   		
     })
     
     
     var priReg = /^\d*\.{0,1}\d{0,1}$/;
 	 //头条报价失去焦点
	 $("#topLinePrice").blur(function(){
	 	var val1=$("#topLinePrice").val();
   		if(val1==""){
   			$(".warn_prohibit").html("请输入头条报价").css("display","block");
   			
   		}
   		if(!priReg.test(val1)){
 			$(".warn_prohibit").html("头条报价必须是数字").css("display","block")
 		}
   		if(priReg.test(val1)){
 			$(".warn_prohibit").html("").css("display","none")
 		}
   		
	 })
    
	 //头条报价获得焦点
     $("#topLinePrice").focus(function(){
       	var val1=$("#topLinePrice").val();
   		if(val1==""){
   			$(".warn_prohibit").html("").css("display","none");
   		}
   		if(priReg.test(val1)){
 			$(".warn_prohibit").html("").css("display","none");
 		}
     })
     
     //次条报价失去焦点
	 $("#lessLinePrice").blur(function(){
	 	var val1=$("#lessLinePrice").val();
   		if(val1==""){
   			$(".warn_lowbit").html("请输入次条报价").css("display","block");
   		}
   		if(!priReg.test(val1)){
 			$(".warn_lowbit").html("次条报价必须是数字").css("display","block");
 		}
   		if(priReg.test(val1)){
 			$(".warn_lowbit").html("").css("display","none");
 		}
   		
	 })
    
	 //次条报价获得焦点
     $("#lessLinePrice").focus(function(){
       	var val1=$("#lessLinePrice").val();
   		if(val1==""){
   			$(".warn_lowbit").html("").css("display","none");
   		}
   		if(priReg.test(val1)){
 			$(".warn_lowbit").html("").css("display","none");
 		}
     })
     
     
     //联系人失去焦点
	 $("#contactUser").blur(function(){
	 	var val1=$("#contactUser").val();
   		if(val1==""){
   			$(".warn_conect").html("请输入联系人").css("display","block");
   		}
   		if(val1.length>10){
   			$(".warn_conect").html("联系人不得超过10个字").css("display","block");
   		}
	 })
    
	 //联系人获得焦点
     $("#contactUser").focus(function(){
       	var val1=$("#contactUser").val();
   		if(val1==""){
   			$(".warn_conect").html("").css("display","none");
   		}
     })
     
	//选择资源类型
	var arr1=["${path}/xkh_version_2.1/img/community_01.png","${path}/xkh_version_2.1/img/community_02.png","${path}/xkh_version_2.1/img/community_03.png","${path}/xkh_version_2.1/img/community_04.png"];
	var arr=["${path}/xkh_version_2.1/img/community_g1.png","${path}/xkh_version_2.1/img/community_g2.png","${path}/xkh_version_2.1/img/community_g3.png","${path}/xkh_version_2.1/img/community_g4.png"];
		$(".icon_box div").click(function(){
			var index=$(this).index();
				$(".icon_box img").each(function(i){
					$(this).attr("src",arr1[i])
				})
				$(this).find("img").attr("src",arr[index]);
				//向input 标签追加active
				$(this).find("input").addClass("active").parent().siblings().find("input").removeClass("active");
				$(this).find("input").attr("name","zylx").parent().siblings().find("input").removeAttr("name");
				
		})
	//选项卡切换
	$(".choose_card1").click(function(){
		$("#checkType").val(0);
		$(".community_content1").show();
		$(".community_content2").hide();
		$(".warn_name").html("").css("display","none");
// 		$(".activCase").hide();
		$(".headPic").html("社群头像");
		$(".headPic").addClass("enter_name").removeClass("community_enterName");
		$(".userName").removeClass("community_enterName").addClass("enter_name").html("群&nbsp;&nbsp;名&nbsp;&nbsp;称");
    	$(".userName").html("群&nbsp;&nbsp;名&nbsp;&nbsp;称");
    	$(".diaryPic").html("群成员图");
    	$(".diaryPicLimit").html("请上传2-9张图片！");   /* *请上传1~2张群成员图 */
		$(".diaryPic_rd").html("照片需体现群人数哦");
		
		$("#pubNumId").attr("disabled",true);
		$("#fansNum").attr("disabled",true);
		$("#readNum").attr("disabled",true);
		$("#topLinePrice").attr("disabled",true);
		$("#lessLinePrice").attr("disabled",true);
		$("#groupNum").removeAttr("disabled");
		
		$(".warn_name").html("").css("display","none");
		$(".warn_conect").html("").css("display","none");
		$(".warn_lowbit").html("").css("display","none");
		$(".warn_prohibit").html("").css("display","none");
		$(".warn_readNum").html("").css("display","none");
		$(".warn_fansNum").html("").css("display","none");
		$(".warn_public").html("").css("display","none");
		$(".warn_groupNum").html("").css("display","none");
		$(".warn_tel").html("").css("display","none");
		$(".warn_letter").html("").css("display","none");
		$(".warn_wechat").html("").css("display","none");
		
	})
	$(".choose_card2").click(function(){
		$("#checkType").val(1);
// 		$(".activCase").show();
		$(".userName").removeClass("enter_name").addClass("community_enterName").html("公众号名称");
		$(".headPic").html("公众号头像");
		$(".headPic").removeClass("enter_name").addClass("community_enterName");
		$(".diaryPic").html("日常推文图");
		$(".diaryPicLimit").html("*请上传2-9张日常推文图");
		$(".diaryPic_rd").html("照片需体现阅读量哦");
		$(".community_content2").show();
		$(".community_content1").hide();
		
    	$("#pubNumId").removeAttr("disabled",true);
		$("#fansNum").removeAttr("disabled",true);
		$("#readNum").removeAttr("disabled",true);
		$("#topLinePrice").removeAttr("disabled",true);
		$("#lessLinePrice").removeAttr("disabled",true);
		$("#groupNum").attr("disabled",true);
		
		$(".warn_name").html("").css("display","none");
		$(".warn_conect").html("").css("display","none");
		$(".warn_lowbit").html("").css("display","none");
		$(".warn_prohibit").html("").css("display","none");
		$(".warn_readNum").html("").css("display","none");
		$(".warn_fansNum").html("").css("display","none");
		$(".warn_public").html("").css("display","none");
		$(".warn_groupNum").html("").css("display","none");
		$(".warn_tel").html("").css("display","none");
		$(".warn_letter").html("").css("display","none");
		$(".warn_wechat").html("").css("display","none");
	})
	
		//省名称
		   var proName = "${proMap.province}";
		   //市名称
		   var cityName = "${basicInfo.cityName}";
		   //院校类别--id
		   var schoolType = "${sholType.VALUE}";
		   //院校名称--id
		   var schoolName = "${basicInfo.schoolId}";
		 
		   var cityId="${basicInfo.cityId}";
	
			//选择所在地
	    	mui.init();
			var schoolData="";
	    	var cityPicker = new mui.PopPicker({
						layer: 2
			});
	    	var shcoolPicker = new mui.PopPicker({
				layer:2
			});
	    	var city = ${proList};
			cityPicker.setData(city);
			var showCityPickerButton = document.getElementById('activeCity');
			
			showCityPickerButton.addEventListener('tap', function(event) {
				$("#schooId").val("");
				//城市回显
				cityPicker.pickers[0].setSelectedValue(proName, 0, function() {
					setTimeout(function() {
					cityPicker.pickers[1].setSelectedValue(cityName);
					}, 100);
				});
				
				cityPicker.show(function(items) {
					
					document.querySelector('#activeCity .result-tips').style.display = "none";
					document.querySelector('#activeCity .show-result').style.display = "block";
					document.querySelector('#activeCity .show-result').innerText = items[1].text;
					$("#cityId").val(items[1].value);
					
					document.querySelector('#activeShcool .result-tips').style.display = "block";
					document.querySelector('#activeShcool .show-result').style.display = "none";  
					//返回 false 可以阻止选择框的关闭
					//return false;
					$.ajax({
		        		type : "post",
		        		url : "<%=contextPath%>/skillUser/chooseSchool.html",
		        		dataType : "json",
		        		data : {cityId:items[1].value},
		        		success : function(data) {
		        			
		        			shcoolPicker.setData(data);
		        			var showShcoolPickerButton = document.getElementById('activeShcool');
		        			showShcoolPickerButton.addEventListener('tap', function(event){
		        				$(".mui-backdrop").remove();
	        				//学校回显
			        		shcoolPicker.pickers[0].setSelectedValue(schoolType, 0, function() {
	        					shcoolPicker.pickers[1].setSelectedValue(schoolName);
			        		});
		        			shcoolPicker.show(function(items) {
		        				document.querySelector('#activeShcool .result-tips').style.display = "none";
		        				document.querySelector('#activeShcool .show-result').style.display = "block";
		        				document.querySelector('#activeShcool .show-result').innerText = items[1].text;
		        				$("#schooId").val(items[1].value);
		        				//返回 false 可以阻止选择框的关闭
		        				//return false;
		        			});
		        		}, false);
		        		/* var muiPickerLh = $(".mui-poppicker").length;
			        		if(muiPickerLh>2){
			        		$(".mui-poppicker").eq(muiPickerLh-2).remove();
			        		} */
		        		}
		        	});
				});
			}, false);
			
				
			   //微信上传 全局变量
				var wxSelf;
				var localArr = new Array();
				var localHeadArr = new Array();//头像
				var localDailyArr = new Array();//日常推文的组合  
				var localActivityArr = new Array();//存放活动的图片
				var serverArr = new Array();
				
				var serverHeadArr = new Array();//头像
				var serverDailyArr = new Array();//日常推文的组合  
				var serverActivityArr = new Array();//存放活动的图片

				/*001 选择图片   日常推文选择图片*/
				function uploadImage(flag,num) {
					var clientUrl = window.location.href;
					var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
					//请求后台，获取jssdk支付所需的参数
					$.ajax({
						type : 'post',
						url : reqPath,
						dataType : 'json',
						data : {
							"clientUrl" : clientUrl
							//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
						},
						cache : false,
						error : function() {
							alert("系统错误，请稍后重试");
							return false;
						},
						success : function(data) {
							//微信支付功能只有微信客户端版本大于等于5.0的才能调用
							var return_date = eval(data);
							/* alert(return_date ); */
							if (parseInt(data[0].agent) < 5) {
								alert("您的微信版本低于5.0无法使用微信支付");
								return;
							}
							//JSSDK支付所需的配置参数，首先会检查signature是否合法。
							wx.config({
								debug : false, //开启debug模式，测试的时候会有alert提示
								appId : return_date[0].appId, //公众平台中-开发者中心-appid
								timestamp : return_date[0].config_timestamp, //时间戳
								nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
								signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
								jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
							});
							//上方的config检测通过后，会执行ready方法
							wx.ready(function() {
								wxSelf=wx;
								wx.chooseImage({
									count: num, // 默认9
									sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
									sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
									success: function (res) {
										
										if(flag =="1"){//头像
											localHeadArr = res.localIds;
											if(localHeadArr.length > 0){
										        for(var j = 0; j < localHeadArr.length; j++){
										        	 var myAvatar=localHeadArr[0];
												        $("#addText").html("");
												        $(".enter_jia").addClass("myInfo_photo");
												        $("#myAvatar").attr("src",myAvatar);
												        $("#myAvatarVal").val(myAvatar);
												        $("#myAvatar").css('display','block');
												        headFlag=false; 
										        }
											}
										}
										if(flag =="2"){//推文图片
											localDailyArr=appandArray(localDailyArr,res.localIds);
											if(localDailyArr.length > 0){
												var result = "";
										        for(var j = 0; j < res.localIds.length; j++){
										        	result += "<div id='xkh_daily_"+j+"' class='add_picture'>"+
															"<img src='"+res.localIds[j]+"' class='search_pic' />"+
																"<span onclick='removeImgDairy("+j+","+res.localIds[j]+")' class='iconfont icon-shanchu' ></span>"+
																"<span class='cover'></span>"+
															"</div>"; 
															
										        }
										        
										        var imgNum = $("#picture_box_daily img").length;
										        var picNum =  parseInt(imgNum) + parseInt(localDailyArr.length);
										        if(picNum>9){
										        	alert("请上传2-9张图片！");
										        }else{
										        	$("#picture_box_daily").append(result);
										        } 
										        
											}
										}
										if(flag =="3"){//活动图片
											//localActivityArr.push(res.localIds)
											localActivityArr=appandArray(localActivityArr,res.localIds);
											if(localActivityArr.length > 0){
												var result = "";
										        for(var j = 0; j < res.localIds.length; j++){
										        	result += "<div id='xkh_activity_"+j+"' class='add_picture'>"+
															"<img src='"+res.localIds[j]+"' class='search_pic' />"+
																"<span onclick='removeImgActiv("+j+","+res.localIds[j]+")' class='iconfont icon-shanchu' ></span>"+
																"<span class='cover'></span>"+
															"</div>";
										        }
										        var imgNum = $("#picture_box_activity img").length;
										        var picNum =  parseInt(imgNum) + parseInt(localDailyArr.length);
										        if(picNum>9){
										        	alert("请上传2-9张图片！");
										        }else{
										        	$("#picture_box_activity").append(result);
										        } 
										        
											}
										}
										
										//localArr= res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
										
									}
								 });

								});
								 wx.error(function(res) {
									alert(res.errMsg);
								});
							}
					});
				}		
				
				
				
				
				/*002 头像 点击提交后上传到微信*/
				function uploadHeadImageSelf(){
					 if(0==localHeadArr.length){
						 uploadlocalDailyImageSelf();
					 }else{
						 for(var i=0;i<localHeadArr.length;i++){
							 wxSelf.uploadImage({
							        localId: localHeadArr[i], // 需要上传的图片的本地ID，由chooseImage接口获得
							        isShowProgressTips: 1, // 默认为1，显示进度提示
							        success: function (res) {
							            var serverId = res.serverId; // 返回图片的服务器端ID
							            serverHeadArr.push(serverId);
							            
							            if(i == localHeadArr.length){
							            	 //alert("执行上传 count"+serverArr);  
							                var serverStr="";
							                if(serverHeadArr[0].length >0 ){
							    	            serverStr=serverHeadArr[0] ;
							    	           // alert(serverStr);
							    	            $.ajax({
							    		        		type : "post",
							    		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
							    		        		dataType : "json",
							    		        		data : {serverId:serverStr},
							    		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
							    		        			//alert("data="+data.path);
							    		        			$("#picUrl").val(data.path);
							    		        			uploadlocalDailyImageSelf();
							    		        			//isUpdate = true
							    		        			
							    		        		}
							    		        	});	
							                	
							                }else{
							                	alert("上传头像失败");
							                }
							                            	
							            }
							         }
							    }); 
						 	}
					 }
				}
				
				
				
				/*群成员图片或者日常推文图片 点击提交后上传到微信*/
				/*使用递归的方式进行上传群成员图片*/
				function uploadlocalDailyImageSelf() {
		            if (localDailyArr.length == 0) {
		            	var serverStr="";
		            	if(0 == serverDailyArr.length ){
		            		uploadActivityImageSelf();
		            	}else{
				            for(var j=0;j<serverDailyArr.length;j++){
				            	serverStr+=serverDailyArr[j]+";" ;
				            }
			            	
			                $.ajax({
				        		type : "post",
				        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
				        		dataType : "json",
				        		data : {serverId:serverStr},
				        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
				        			var imageList = data;
				        			var imgPath="";
									for(var f=0;f<imageList.length;f++){
										var imgPathTemp = imageList[f];
										imgPath += imgPathTemp+",";
									}
									var u = $("#dairyTweetUrl").val()+imgPath;
									$("#dairyTweetUrl").val(u);
									
									var imgNum = $("#picture_box_daily img").length;
									if(imgNum<2||imgNum>9){
										alert("请上传2-9张图片");
										$(".webservice_mask").addClass("mask_none");
										return;
									}
									uploadActivityImageSelf();
				        		}
				        	});	
		            	}
		            }
		            var localId = localDailyArr[0];
		            //tmd 一定要加     解决IOS无法上传的坑 
		            if (localId.indexOf("wxlocalresource") != -1) {
		                localId = localId.replace("wxlocalresource", "wxLocalResource");
		            }
		            wxSelf.uploadImage({
		                localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
		                isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
		                success: function (res) {
		                   // serverIds.push(res.serverId); // 返回图片的服务器端ID
		                   var serverId = res.serverId; // 返回图片的服务器端ID
		                   serverDailyArr.push(serverId);
		                   localDailyArr.shift();
		                   uploadlocalDailyImageSelf();
		                    serverStr+=serverId+";" ;
		                },
		                fail: function (res) {
		                    alert("上传失败，请重新上传！");
		                }
		            });
		         
		        }
				
				
				
				/*活动图片 点击提交后上传到微信*/
				/*使用递归的方式进行上传图片*/
				function uploadActivityImageSelf() {
		            if (localActivityArr.length == 0) {
		            	var serverStr="";
		            	if(0 == serverActivityArr.length ){
		            		$("#addPartForm").submit();
		            	}else{
				            for(var j=0;j<serverActivityArr.length;j++){
				            	serverStr+=serverActivityArr[j]+";" ;
				            }
			            	
			                $.ajax({
				        		type : "post",
				        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
				        		dataType : "json",
				        		data : {serverId:serverStr},
				        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
				        			
				        			//alert("+++++++++data++++++:"+data);	
				        			var imageList = data;
				        			//var pathList = data.
				        			
				        			var imgPath="";
				        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
									for(var f=0;f<imageList.length;f++){
								//		alert("+++++++++++++++:"+imageList[f]);
										var imgPathTemp = imageList[f];
										imgPath += imgPathTemp+",";
									}
									
									var u = $("#activtyUrl").val()+imgPath;
									$("#activtyUrl").val(u)
									
									var imgNum = $("#picture_box_activity img").length;
									//alert("imgNum:"+imgNum);
									if(imgNum<2||imgNum>9){
										alert("活动图片个数为2-9");
										$(".webservice_mask").addClass("mask_none");
										return;
									}
									
				        			$("#addPartForm").submit();
									$(".webservice_mask").removeClass("mask_none");
				        		}
				        	});	
		            	}
		            }
		            var localId = localActivityArr[0];
		            //tmd 一定要加     解决IOS无法上传的坑 
		            if (localId.indexOf("wxlocalresource") != -1) {
		                localId = localId.replace("wxlocalresource", "wxLocalResource");
		            }
		            wxSelf.uploadImage({
		                localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
		                isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
		                success: function (res) {
		                   // serverIds.push(res.serverId); // 返回图片的服务器端ID
		                   var serverId = res.serverId; // 返回图片的服务器端ID
		                   serverActivityArr.push(serverId);
		                    localActivityArr.shift();
		                    uploadActivityImageSelf();
		                    serverStr+=serverId+";" ;
		                },
		                fail: function (res) {
		                    alert("上传失败，请重新上传！");
		                }
		            });
		        }
				
				function appandArray(a,b){

					for(var i=0;i<b.length;i++){
						 a.push(b[i]);
					}
					return a;
				}
				
				
				
				function removeImgActiv(imageId,val){
					$("#xkh_activity_"+imageId).remove();
					localActivityArr.remove(val);
				}
				
				function removeImgDairy(imageId,val){
					$("#xkh_daily_"+imageId).remove();
					localDailyArr.remove(val);
				}
				
				//点击删除推文图片
				function delImgDairyTUrl(imgId,val,obj){
					if(confirm("确定删除?")){
							//表示修改需要数据库删除
							$.ajax({
								type : "post",
								url : "<%=basePath%>highSchoolSkill/delImage.html",
								dataType : "json",
								data : {imgeId:imgId,imageUrl:val},
								success : function(data) {
									if(data.flag){
										$("#dairyTweetUrl").val(data.typeValue);
										obj.parentElement.remove();
									}
								}
							});
				  }
			   }
				
				//点击删除活动图片
				function delImgActivtyUrl(imgId,val,obj){
					if(confirm("确定删除?")){
							//表示修改需要数据库删除
							$.ajax({
								type : "post",
								url : "<%=basePath%>highSchoolSkill/delImage.html",
								dataType : "json",
								data : {imgeId:imgId,imageUrl:val},
								success : function(data) {
									if(data.flag){
										$("#activtyUrl").val(data.typeValue);
										obj.parentElement.remove();
									}
								}
							});
				  }
			   }
		</script>
