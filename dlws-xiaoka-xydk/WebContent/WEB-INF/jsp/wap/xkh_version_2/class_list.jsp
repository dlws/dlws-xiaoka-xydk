<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta
	content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1"
	id="viewport" name="viewport" />
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link rel="stylesheet"
	href="${path}/xkh_version_2/iconfont/iconfont.css" />
<link rel="stylesheet" type="text/css"
	href="${path}/xkh_version_2/css/mui.picker.css" />
<link rel="stylesheet" type="text/css"
	href="${path}/xkh_version_2/css/mui.poppicker.css" />
<link rel="stylesheet" type="text/css"
	href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/dropload.css" />
<link rel="stylesheet" type="text/css"
	href="${path}/xkh_version_2/css/style.css" />
<link rel="stylesheet" type="text/css"
	href="${path}/xkh_version_2.2/css/style.css" />
<link rel="stylesheet"
	href="${path}/xkh_version_2.3/xkh_version_2.3_1/css/search.css" />
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="${path}/wapstyle/js/template.js"></script>
<script src="${path}/xkh_version_2/js/jquery-2.1.0.js"></script>

<title>校咖汇</title>
</head>
<style>
nav {
	position: fixed;
	left: 0;
	bottom: 0;
}

.wraper {
	margin-bottom: 50px;
}
</style>
<body>
	<!-- 彈出一個遮罩層和彈出框45667 -->
	<div class="sort_mask"></div>

	<div class="menu">
		<p class="menu_child1">点击重复</p>
		<p class="menu_child2">好</p>
	</div>
	<!-- 彈出一個遮罩層和彈出框 -->
	<div class="wraper content">
		<div class="container">

			<!--隐藏的搜索列表部分-->
			<div class="list_content">
				<span class="hot_word">搜索热词</span>
				<div class="search_list">
					<c:forEach varStatus="hotWord" items="${hotWords }" var="hot">
						<a href="${path}/home/getClassListSeach.html?seachValue=${hot.dicName }&fid=${fid }"><span>${hot.dicName }</span></a>
					</c:forEach>
				</div>
			</div>
			<!-- 2.4版本新换 -->
			<header class="ctt_headerSearch">
				<form class="ctt_hot" id="tosearch" method="post">
					<a href="javascript:void(0)" class="hot_pic"><img
						class="search_pic" src="${path}/xkh_version_2/img/search.png"></a>
					<input type="text" onclick="search('${fid}');" name="searchValue"
						id="searchValue" value="" placeholder="搜索" class="hot_inp">
					<span class="icon_remove"></span>
				</form>
				<span class="ctt_cancel">取消</span>
			</header>
			<ul class="secondIndex_title">
				<ul class="class-title1">
					<c:forEach varStatus="status" items="${skillClass }" var="cla">
						<li <c:if test="${status.index==0}">class="title_active"</c:if>
							data-name="${status.index}" id="${cla.id }" value="1"><span
							<c:if test="${status.index==0}">class="title_color"</c:if>>${cla.className }</span></li>
					</c:forEach>
				</ul>
			</ul>
			<!-- 2.4版本新換 -->

			<ul class="class_choose">
				<li class="choose choose_sort"><span class="class_active">默认排序</span>
					<span class="icon_down iconSort"></span></li>
				<li class="choose choose1"><span class="sales">销量优先</span></li>
				<li class="choose choose2"><span>筛选</span> <span class="filter"></span>
				</li>
				<div class="search_bg ctt_none"></div>
				<!--点击排序出来的部分-->
				<div class="class_sortContent">
					<div class="search_bg"></div>
					<ul class="class_sort">
						<li id="def" data-name="" class="class_active">默认排序<span
							class="iconfont icon-gou class_check"></span></li>
						<li id="desc" data-name="desc">价格由高到低<span
							class="iconfont class_check"></span></li>
						<li id="asc" data-name="asc">价格由低到高<span
							class="iconfont class_check"></span></li>
					</ul>
				</div>
				<!--点击筛选出来的样式-->
				<div class="class_filterContent">
					<div style="min-height: 100%;">
						<div class="class_price">
							<h3>价&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格</h3>
							<div class="price_box">
								<input id="minPrice" placeholder="最低价" type="number"
									onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))">
								<span>—</span> <input id="maxPrice" placeholder="最高价"
									type="number"
									onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))">
							</div>
						</div>
						<div class="address">
							<h3>所&nbsp;在&nbsp;&nbsp;地</h3>
							<div class="choose_address choose_address1 mui-input-row" id="chooseCity">
								<span class="address_text address_text1 result-tips">选择所在地</span>
								<span class="address_text show-result"></span> <img
									src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/right_jiantou.png"
									class="ctt_jiantou" /> <img
									src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/Tcha.png"
									class="ctt_cha" />
							</div>
						</div>
						<div class="service_box">
							<h3>服务类型</h3>
							<div class="service_style serviceLabel">
								<div data-name="1">线上</div>
								<div data-name="2">线下</div>
							</div>
						</div>
						<div class="address address1">
							<h3>院校等级</h3>
							<div class="service_style schoolStyle">
								<c:forEach items="${labelList }" var="label">
									<div id="${label.value }">${label.text }</div>
								</c:forEach>
							</div>
						</div>

					</div>
					<div class="enure_content">
						<div class="reset_box">重置</div>
						<div class="enure_box">确定</div>
					</div>

				</div>
			</ul>

			<c:forEach varStatus="status" items="${skillClass }" var="cla">
				<ul class="lists"></ul>
			</c:forEach>

			<%-- <!--点击筛选出现之前隐藏的那一部分-->
				<div class="class_sortContent show">
					<div class="class_bg"></div>
					<ul class="class_sort">
						<li id="def" data-name=""  class="class_active">默认排序<span class="iconfont icon-gou class_check"></span></li>
						<li id="desc" data-name="desc"  >价格由高到低<span class="iconfont class_check"></span></li>
						<li id="asc" data-name="asc" >价格由低到高<span class="iconfont class_check"></span></li>
					</ul>
				</div>

				<!--图片展示那一大部分
				<ul class="lists">
				</ul>
				<ul class="lists"></ul>
				<ul class="lists"></ul>
				<ul class="lists"></ul>
				<ul class="lists"></ul>-->
				<c:forEach varStatus="status" items="${skillClass }" var="cla" >
					<ul class="lists"></ul>
				</c:forEach>
		</div>
		<!--筛选右侧滑出来的那一部分-->
		<div class="class_filterContent moved">
		<div class="class_bg"></div>
			<div style="min-height: 100%;">
			<div class="class_price">
				<h3>价格</h3>
				<div class="price_box">
					<input id="minPrice" placeholder="最低价" type="number" onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" />
					<span>—</span>
					<input id="maxPrice" placeholder="最高价" type="number" onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" />
				</div>
			</div>
			<div class="address">
				<h3>所在地</h3>
				<div class="choose_address choose_address1 mui-input-row" id="chooseCity">
					<span class="address_text address_text1 result-tips">选择所在地</span>
					<span class="address_text show-result"></span>
					<span class="iconfont icon-arrfill_d-copy-copy"></span>
				</div>
			</div>
			<div class="service_box">
				<h3>服务类型</h3>
				<div class="service_style">
					<div data-name="1">线上</div>
					<div data-name="2">线下</div>
				</div>	
			</div>
			<div class="address address1">
				<h3>院校等级</h3>
				<!--<div class="choose_address"><span class="address_text">选择院校等级</span><span class="iconfont icon-arrfill_d-copy-copy"></span></div>-->
				<div class="choose_address mui-input-row" id="chooseSchool">
					<span class="address_text result-tips">选择院校等级</span>
					<span class="address_text show-result"></span>
					<span class="iconfont icon-arrfill_d-copy-copy"></span>
				</div>
			</div>
			</div>
			<div class="enure_content">
			   <div class="reset_box">重置</div>
			   	<div class="enure_box">确定</div>
			</div>
		</div> --%>
			<!--遮罩层-->
			<div class="mask moved"></div>
			<c:forEach varStatus="status" items="${skillClass }" var="cla">
				<input type="hidden" id="orderp${cla.id }" value="">
			</c:forEach>
			<input type="hidden" id="fid2" value="${fid }"> 
			<input type="hidden" id="cityName" value=""> 
			<input type="hidden" id="schoolLabel" value=""> 
			<input type="hidden" id="serviceType" value="">
			<!-- 用于记录最后一次的页数和最后一次的tab的id -->
			<input type="hidden" id="xkh_class_id" value="0"> 
			<input type="hidden" id="xkh_page_id" value="0"> 
			<input type="hidden" id="xkh_page_num" value="0"> 
			<input type="hidden" id="xkh_lock" value="off">
			<input type="hidden" id="ordersellNo" value="">
		</div>
		<%@ include file="foot.jsp"%>
		<!-- 06-19加入已选弹窗 -->
		<div class="popupWindow_content">
			<div class="popupWindow_title">
				<span class="popupWindow_close"><img
					src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/cha.png" alt=""
					class="search_pic" /></span> <span class="poopupWindow_choose">已选</span>
				<a class="popupWindow_car" href="${path}/skillOrder/SelectedList.html">
	       		   <img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/buyCar.png" class="search_pic" />
	       		   <img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/redCar.png" class="redCar_pic" />
	       		</a>
			</div>
			<ul class="poopupWindow_box" id="selectSkill">
				<%-- <li>
					<span class="queen"><img src="${path}/xkh_version_2.3/xkh_version_2.3_1/img/queen.png" class="search_pic" /></span>
					<div class="popupWindow_message">
						<span>精准咨询，答您所需</span> <span>商家选定特定校园资源，直接付费咨询...</span>
					</div>
					<div class="popupWindow_number">
						<span class="reduce">-</span> <span class="value">1</span> 
						<span class="Add">+</span>
					</div>
				</li> --%>
			</ul>
			<!--这一部分当是搜索结果为单项时候就不出现，只需要去掉popupWindow_up这个类名就可以-->
			<span class="popupWindow_more popupWindow_up"></span>
		</div>
</body>

<script type="text/javascript"
	src="${path}/xkh_version_2/js/jquery-2.1.0.js"></script>
<script src="${path}/xkh_version_2/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${path}/xkh_version_2/js/mui.min.js"></script>
<script type="text/javascript"
	src="${path}/xkh_version_2/js/mui.picker.js"></script>
<script type="text/javascript"
	src="${path}/xkh_version_2/js/mui.poppicker.js"></script>
<script type="text/javascript"
	src="${path}/xkh_version_2/js/city.data.js"></script>

<script type="text/javascript"
	src="${path}/xkh_version_2/js/dropload.min.js"></script>
<script type="text/javascript"
	src="${path}/xkh_version_2/js/jquery.cookie.js"></script>
<script type="text/javascript">
    	var baseP = "<%=basePath%>";
    	var proList = ${proList };
    	
    	function search(fid){
        	//$("#tosearch").submit();
        	console.log(fid);
        	window.location.href="${path}/home/getClassListSeach.html?fid="+fid;
    	}
    	var path = "${path}";
    	//消息在第一序列
    	var index = 0;
</script>
<script>
		var baseP = "<%=basePath%>";
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: "校咖汇", // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: "校咖汇", // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () {
						        // 用户取消分享后执行的回调函数
						    }
						});
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
	</script>
<script type="text/javascript" src="${path}/xkh_version_2/js/class_list.html.js"></script>
<script type="text/javascript" src="${path}/xkh_version_2.2/js/foot.js" ></script>
</html>
