<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<%@ include file="image_data.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" href="${path}/xkh_version_2/css/swiper-3.3.1.min.css" />
	    <link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css"/>
		<title>校咖汇</title>
	</head>
	<body>
		<!--轮播图-->
		<div class="lunbo personal_lunbo">
				<div class="swiper-container personal_swiper">
		            <div class="swiper-wrapper">
				    	<div class="swiper-slide"><img src="${skillInfo.imgUrl }?${index_banner}"/></div>
		   			</div>
                    <div class="swiper-pagination personal_pagination"></div>
                    <div class="personal_text">
	      		   	  
	      		   	   <span><img src="${path}/xkh_version_2/img/leter.png" class="search_pic" /></span>
	      		   	    <span>1</span>
      		        </div>
      		      <div class="eye_content">
      		           <span class="personal_eye"><img src="${path}/xkh_version_2/img/eye.png" class="search_pic" /></span>
      		           <span class="personal_number">${skillInfo.salesVolume}</span>
      		      </div>
      		   </div>
       </div>
      	<!--个人信息-->
      	<a href="${path }/comunityBigV/getPersonUrl.html?id=${baseInfo.id}&enterId=${baseInfo.enterId}" class="personal_content">
      		<div class="personal_left">
      			<img src="${baseInfo.headPortrait }" class="search_pic" style="border-radius:100%;"  />   
      		</div>
      		<div class="personal_right">
      			<c:if test="${empty baseInfo.nickname }">
      				<p>${baseInfo.userName }</p>
      			</c:if>
      			<c:if test="${ not empty baseInfo.nickname }">
      				<p>${baseInfo.nickname }</p>
      			</c:if>
      			<div class="personal_box">
	      			<span class="personal_college">${baseInfo.schoolName }</span>
	      			<div class="city">
	      				<span class="iconfont icon-zuobiao personal_zuobiao"></span>
	      			    <span class="personal_city">${baseInfo.cityName }</span> 
	      			</div>
      			</div>
      		</div>
      	</a>
		<div class="about_bg"></div>
		<div class="personal_service">
			<div class="Pservice_pic">
				<img src="${path}/xkh_version_2/img/icon_type.png" class="search_pic"  />
			</div>
			<span class="P_style">服务类型 :</span>
			<span class="P_line">
				<c:if test="${skillInfo.serviceType == 1 }">线上</c:if>
				<c:if test="${skillInfo.serviceType == 2 }">线下</c:if>
			</span>
		</div>
		<div class="personal_ablity">
			<div class="Pablity_pic">
				<img src="${path}/xkh_version_2/img/icon_diamond.png" class="search_pic"  />
			</div>
			<span class="Pablity_price">服务价格 :</span>
			<span class="Pablity_number">${skillInfo.price }</span>
				<span class="Pablity_yuan">元/次</span>
		</div>
		<div class="about_bg"></div>
		<div class="info_content">
			<span>${skillInfo.serviceName }</span>
			<p>${skillInfo.introduce }</p>
		</div>
	</body>

	<script src="${path}/xkh_version_2/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/swiper-3.3.1.jquery.min.js"></script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
</html>

