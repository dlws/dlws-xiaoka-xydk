<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>正在开发ing!</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css" />
	<title>提示</title>
  </head>
  
  	<body >
		<div class="error_content cue_content">
			<img src="${path}/xkh_version_2/img/error.png" class="search_pic" />
			<div class="error_img"></div>
			<p>稍安勿躁，技术小哥正在拼命开发此功能！</p>
			<a href="${path}/home/index.html">正在为您跳转...</a>
		</div>
	</body>
	<script>
	onload=function(){ //在进入网页的时候加载该方法 
		setTimeout(go, 3000); /*在js中是ms的单位*/ 
	}; 
	function go(){ 
		location.href="${path}/home/index.html"; 
	} 
	</script>
</html>


