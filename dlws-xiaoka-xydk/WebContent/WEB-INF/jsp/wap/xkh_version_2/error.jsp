<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css" />
		<title>出错啦！</title>
	</head>
	<body>
		<div class="error_content">
			<img src="${path}/xkh_version_2/img/error.png" class="search_pic" />
			<div class="error_img"></div>
			<a href="${path}/xkh_version_2/index.html">出错啦！点此回到首页...</a>
		</div>
	</body>
</html>
