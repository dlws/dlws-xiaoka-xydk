<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/iconfont/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/mui.picker.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/mui.poppicker.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/style.css" />
		<title>场地资源入驻</title>
		</head>
		<!--遮罩-->
		<div class="webservice_mask mask_none"></div>
		<div class="webservice_container">
		<div class="enterInfo_top">
			<img src="${path}/xkh_version_2.1/img/place_top.gif" class="search_pic" />
		</div>
		<form action="${path}/fieldEnter/addEnter.html" id="enterForm" method="post" enctype="multipart/form-data">
		<ul class="message_content">
			<li>
				<span class="enter_name">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span>
				<div id="activeCity" class="mui-input-row">
					<span class="myInfo_name enter_city result-tips">选择你所在的城市</span>
					<span class="myInfo_name show-result"></span>
					<input type="hidden" name="cityId" id="cityId" value="${basicInfo.cityId}" >
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;校</span>
				<div id="activeShcool" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">选择您所在的学校</span>
					<span class="myInfo_name show-result"></span>
					<input type="hidden" name="schoolId" id="schoolId" value="">
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>

			</li>
			<li>
				<span class="enter_name">入驻名称</span>
				<input class="inp" name="userName" id="userName" type="text" placeholder="请填写入驻名称" maxlength="10" />
			</li>
			<li>
				<span class="community_enterName headPic">入驻头像</span>
				<!-- <span class="enter_jia">点击加号上传头像</span> -->
				<span class="enter_jia">
					<span id="addText">点击加号上传头像</span>
					<img id="myAvatar"  class="search_pic" style="display: none;" />
					<input type="hidden" value="" id="headPortrait" name="picUrl">
				</span>
				<span class="iconfont icon-jiahao new_jia enter_youjiantou icon_pic" onclick="uploadImage(1,1);"></span>
				</li>
			<li>
				<span class="enter_name">场地类型</span>
				<div id="placeStyle" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">请选择场地类型</span>
					<span class="myInfo_name show-result"></span>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">场地规模</span>
				<div id="placeSize" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">请选择场地规模</span>
					<span class="myInfo_name show-result"></span>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<p class="place_name">基本物资配备(可选)</p>
				<c:forEach items="${materialList}" var="material">
						<div class="check">
							<span>${material.dic_name}</span>
							<input type="hidden" value="${material.dic_value}" id="material" name="material"  disabled="disabled">
							<span class="check_pic" id="checkbox" ></span>
						</div>			
				</c:forEach>
				<p class="enter_name">使用时间</p>
				<div id="placeTime" class="mui-input-row place_inp">
					<span class="result-tips" >8:00</span>
					<span class="show-result" id="result"></span>
				</div>
				<span class="place_span">—</span>
				<div id="placeTime2" class="mui-input-row place_inp">
					<span class="result-tips" >12:00</span>
					<span class="show-result" id="checkbox"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">提供兼职</span>
				<div id="provide" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">是否提供兼职方式</span>
					<span class="myInfo_name show-result"></span>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">场地报价</span>
				<input class="inp" name="planprice" type="number" placeholder="请填写场地单次使用费用" />
			</li>
			<!--场地场景照片图空出来的部分-->
			<li>
				<span class="enter_name enterName">场地照片</span>
				<span class="enter_name enterName2">全景照片</span>
				<span class="myInfo_name">点击加号上传全景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="allPhoto">
						
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage(4,9);">
						<span class="iconfont icon-pic"></span>
						<input type="hidden" value="" name="allPhoto" id="allPhoto_img">
						<span class="size">+添加图片</span>
					</div>
				</div>
				<span class="enter_name add_photo enterName2">中景照片</span>
				<span class="myInfo_name add_place">点击加号上传中景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="middlePho">
					
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage(3,9)">
						<span class="iconfont icon-pic"></span>
						<input type="hidden" value="" name="middlePho" id="middlePho_img">
						<span class="size">+添加图片</span>
					</div>
				</div>
				<span class="enter_name add_photo enterName2">近景照片</span>
				<span class="myInfo_name add_place">点击加号上传近景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="closePhoto">
						
					</div>
					<!--<添加的图片完-->
					<div class="add_pic" onclick="uploadImage(2,9)">
						<span class="iconfont icon-pic"></span>
						<input type="hidden" value="" name="closePhoto" id="closePhoto_img">
						<span class="size">+添加图片</span>
					</div>
				</div>
				<p class="limit">*请上传2-9张场地照片</p>
			</li>
			<!--场地描述-->
			<li>
				<span class="enter_nameT">场地描述</span>
				<div class="textarea">
					<textarea class="introduce" id="currentDec" name="aboutMe" onkeyup="load()"  required="required" placeholder="请填写和场地有关的信息，譬如场地大小，过往举办过的活动等等"></textarea>
					<div id="span" class="span"><span>0</span>/200</div>
				</div>
			</li>
			<li>
				<span class="enter_name">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
				<input class="inp" type="text" id="contactUser" name="contactUser" maxlength="10" placeholder="填写您的姓名" />
			</li>
			<li>
				<span class="enter_name">联系电话</span>
				<input class="phone inp" type="number" id="phoneNumber"  name="phoneNumber" placeholder="填写您的手机号码" maxlength="15" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))"/>
				<p class="warn_tel"></p>
			</li>
			<li>
				<span class="enter_name">联系邮箱(选填)</span>
				<input class="letter inp" type="text" id="email" name="email" placeholder="填写您的联系邮箱" onkeyup="this.value=this.value.replace(/\s+/g,'')"/>
				<p class="warn_letter"></p>
			</li>
				<li>
					<span class="enter_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号(选填)</span>
					<input class="wechat inp" type="text" id="wxNumber" name="wxNumber" placeholder="填写您的微信账号" />
					<p class="warn_wechat"></p>
				</li>
		</ul>
		<input type="hidden" value="" id="begin" name="begin"><!-- 开始时间 -->
		<input type="hidden" value="" id="end"  name="end"><!-- 结束时间 -->
		<input type="hidden" value=""  id="fieldSize" name="filedSize"><!-- 场地大小  注意filedSize大小和当前数据库中一致 -->
		<input type="hidden" value=""  id="fieldType" name="fieldType"><!-- 场地类型 -->
		<input type="hidden" value=""  id="partimejob" name="partimejob"><!-- 是否提供兼职 -->
		<input type="hidden" value="${paramsMap.enterId}" name="enterId"><!-- 类型暂时写死，合板的时候将以参数形式传递 -->
		</form>
		<div class="enter_form" onclick="addEnter()">
			<button >提交</button>
		</div>
		</div>
	</body>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js"></script>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/mui.min.js"></script>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/mui.picker.js"></script>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/mui.poppicker.js"></script>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/validateMessage.js"></script>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/enterInfo.js"></script>
		<!-- <script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script> -->
		<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
		<script type="text/javascript" src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
		<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
		<script>
			//提交前的参数验证
		    var cityData = ${proList}; 
			var fieldTypeList = ${fieldTypeList};
			var filedSizeList = ${filedSizeList};
		    var labelList = ${labelList};
		    var path = "${path}";
			var serverAlllArr = new Array(); //存放活动的图片
			var localAlllArr = new Array(); //存放活动的图片
			$(".check span:last-child").click(function() {
		
					if($(this).hasClass("check_pic")) {
						$(this).removeClass("check_pic").addClass("check_img")
					} else {
						$(this).removeClass("check_img").addClass("check_pic")
					}
			})
			//选择场地类型
			var placeStyle = new mui.PopPicker({
				layer: 1
			});
			placeStyle.setData(fieldTypeList);
			
			var showplaceStyleButton = document.getElementById('placeStyle');
			showplaceStyleButton.addEventListener('tap', function(event) {
				placeStyle.show(function(items) {
					document.querySelector('#placeStyle .result-tips').style.display = "none";
					document.querySelector('#placeStyle .show-result').style.display = "block";
					document.querySelector('#placeStyle .show-result').innerText = items[0].text;
					$("#fieldType").val(items[0].value);					
					//返回 false 可以阻止选择框的关闭
					//return false;
					placeStyle.hide(function(items) {
						if(document.querySelector('#placeStyle .show-result').innerText = "") {
							$(".mui-backdrop").css("display", "none")
						}
		
					})
				});
			}, false);
			//选择场地规模
			var placeSize = new mui.PopPicker({
				layer: 1
			});
			//存入场地规模
			placeSize.setData(filedSizeList);
			var showplaceSizeButton = document.getElementById('placeSize');
			showplaceSizeButton.addEventListener('tap', function(event) {
				placeSize.show(function(items) {
					document.querySelector('#placeSize .result-tips').style.display = "none";
					document.querySelector('#placeSize .show-result').style.display = "block";
					document.querySelector('#placeSize .show-result').innerText = items[0].text;
					$("#fieldSize").val(items[0].value);	
					//返回 false 可以阻止选择框的关闭
					//return false;
				});
			}, false);
			//是否提供兼职方式
			var provide = new mui.PopPicker({
				layer: 1
			});
			provide.setData([{
				value: 0,
				text: "是"
			}, {
				value: 1,
				text: "否"
			}]);
			var showprovideButton = document.getElementById('provide');
			showprovideButton.addEventListener('tap', function(event) {
				provide.show(function(items) {
					document.querySelector('#provide .result-tips').style.display = "none";
					document.querySelector('#provide .show-result').style.display = "block";
					document.querySelector('#provide .show-result').innerText = items[0].text;
					$("#partimejob").val(items[0].value);	
					//返回 false 可以阻止选择框的关闭
					//return false;
				});
			}, false);
			//时间选择器
			var a = "";
			var c = ""
			var time = [{
				value: 0,
				text: "0:00"
			}, {
				value: 1,
				text: "1:00"
			}, {
				value: 2,
				text: "2:00"
			}, {
				value: 3,
				text: "3:00"
			}, {
				value: 4,
				text: "4:00"
			}, {
				value: 5,
				text: "5:00"
			}, {
				value: 6,
				text: "6:00"
			}, {
				value: 7,
				text: "7:00"
			}, {
				value: 8,
				text: "8:00"
			}, {
				value: 9,
				text: "9:00"
			}, {
				value: 10,
				text: "10:00"
			}, {
				value: 11,
				text: "11:00"
			}, {
				value: 12,
				text: "12:00"
			}, {
				value: 13,
				text: "13:00"
			}, {
				value: 14,
				text: "14:00"
			}, {
				value: 15,
				text: "15:00"
			}, {
				value: 16,
				text: "16:00"
			}, {
				value: 17,
				text: "17:00"
			}, {
				value: 18,
				text: "18:00"
			}, {
				value: 19,
				text: "19:00"
			}, {
				value: 20,
				text: "20:00"
			}, {
				value: 21,
				text: "21:00"
			}, {
				value: 22,
				text: "22:00"
			}, {
				value: 23,
				text: "23:00"
			}]
			var placeTime = new mui.PopPicker({
				layer: 1
			});
			var placeTime2 = new mui.PopPicker({
				layer: 1
			});
			placeTime.setData(time);
			var showplaceTimeButton = document.getElementById('placeTime');
			showplaceTimeButton.addEventListener('tap', function(event) {
				placeTime.show(function(items) {
					document.querySelector('#placeTime .result-tips').style.display = "none";
					document.querySelector('#placeTime .show-result').style.display = "block";
					document.querySelector('#placeTime .show-result').innerText = items[0].text;
					$("#begin").val(items[0].text);
					a = document.querySelector('#placeTime .show-result').innerText;
					var v = parseInt(a)
					c = time.slice(v)
					
					placeTime2.setData(c);
					var showplaceTimeButton2 = document.getElementById('placeTime2');
					showplaceTimeButton2.addEventListener('tap', function(event) {
					placeTime2.show(function(items) {
					document.querySelector('#placeTime2 .result-tips').style.display = "none";
					document.querySelector('#placeTime2 .show-result').style.display = "block";
					document.querySelector('#placeTime2 .show-result').innerText = items[0].text;
					$("#end").val(items[0].text);
							//返回 false 可以阻止选择框的关闭
							//return false;
					});
					}, false);
				});
		
			}, false);
			
		//选择所在地
       	mui.init();
       			var cityPicker = new mui.PopPicker({
					layer: 2
				});
       			var shcoolPicker = new mui.PopPicker({
    				layer:2
    			});
				cityPicker.setData(cityData);
				var showCityPickerButton = document.getElementById('activeCity');
				showCityPickerButton.addEventListener('tap', function(event) {
					cityPicker.show(function(items) {
						document.querySelector('#activeCity .result-tips').style.display = "none";
						document.querySelector('#activeCity .show-result').style.display = "block";
						document.querySelector('#activeCity .show-result').innerText = items[1].text;
						$("#cityId").val(items[1].value);
						$.ajax({
			        		type : "post",
			        		url : "<%=contextPath%>/skillUser/chooseSchool.html",
			        		dataType : "json",
			        		data : {cityId:items[1].value},
			        		success : function(data) {
			        			
			        			shcoolPicker.setData(data);
					        	$("#schoolId").val(""); 
			        			var showShcoolPickerButton = document.getElementById('activeShcool');
			        			showShcoolPickerButton.addEventListener('tap', function(event){
			        			shcoolPicker.show(function(items) {
			        				document.querySelector('#activeShcool .result-tips').style.display = "none";
			        				document.querySelector('#activeShcool .show-result').style.display = "block";
			        				document.querySelector('#activeShcool .show-result').innerText = items[1].text;
			        				$("#schoolId").val(items[1].value);
			        				//返回 false 可以阻止选择框的关闭
			        				//return false;
			        			});
			        		}, false);
			        		var muiPickerLh = $(".mui-poppicker").length;
			        		}
			        	});
					});
		}, false);
	   $(".check_pic").click(function(){
		   var chekPiClas = $(this).attr('class');
		 if(chekPiClas=='check_img'){
			 $(this).siblings("input").removeAttr("disabled");
		 }else{
			 $(this).siblings("input").attr("disabled",true);
			 
		 }
	  });
	  
	  //获取当前的开始时间和结束时间
	  $(document).ready(function(){
		  var begin = $("#placeTime span:first-child").html();
		  $("#begin").val(begin);
		  var end = $("#placeTime2 span:first-child").html();
		  $("#end").val(end);
	  });
	  
	/* 远景图片上传*/
	function uploadAllImages() {
		//alert("All*****function****localAlllArr.length："+localAlllArr.length);
        if (localAlllArr.length == 0) {
        	
        	var serverStr="";
        	if(0 == serverAlllArr.length ){
        		$("#enterForm").submit();
        	}else{
            for(var j=0;j<serverAlllArr.length;j++){
            	serverStr+=serverAlllArr[j]+";" ;
            }
            $.ajax({
        		type : "post",
        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
        		dataType : "json",
        		data : {serverId:serverStr},
        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
        			
        			//alert("+++++++++data++++++:"+data);	
        			var imageList = data;
        			//var pathList = data.
        			
        			var imgPath="";
        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
					for(var f=0;f<imageList.length;f++){
				//		alert("+++++++++++++++:"+imageList[f]);
						var imgPathTemp = imageList[f];
						imgPath += imgPathTemp+",";
					}
					$("#allPhoto_img").val(imgPath);
				//    alert("===========activity的imgUrl============"+$("#activityImage").val());
				//	alert("All*****function****data"+imgPath);
        			$("#enterForm").submit();
        		}
        	});	
        }
        }
        var localId = localAlllArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 0, // 默认为1，显示进度提示
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
                serverAlllArr.push(serverId);
                localAlllArr.shift();
                uploadAllImages(localAlllArr);
                serverStr+=serverId+";";
               // alert("localArr的长度："+localArr.length);
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
                $(".webservice_mask").addClass("mask_none");
            }
        });
    }
</script>
<script type="text/javascript" src="${path}/xkh_version_2.1/js/uploadImage.js"></script>
</html>
