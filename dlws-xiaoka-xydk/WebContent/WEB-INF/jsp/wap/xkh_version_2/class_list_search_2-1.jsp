<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/mui.picker.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/mui.poppicker.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.2/css/style.css" />
		<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<script src="${path}/xkh_version_2/js/jquery-2.1.0.js"></script>
		<title>校咖汇</title>
	</head>
    <style>
     nav{
        position:fixed;
        left:0;
        bottom:0;
        
     }
     .wraper{
       margin-bottom:50px;
     }
    </style>
	<body>
	<!-- 彈出一個遮罩層和彈出框-->
	<div class="sort_mask"></div>
	<div class="menu">
	   <p class="menu_child1">点击重复</p>
	   <p class="menu_child2">好</p>
	</div>
	<!-- 彈出一個遮罩層和彈出框 -->
		<div class="wraper content">
			<div class="container">
				<ul class="class_choose">
					<li class="choose choose_sort">
						<span class="class_active">默认排序</span>
						<span class="iconfont icon-triangle class_active icon_sort"></span>
					</li>
					<li class="choose choose1">
						<span>销量优先</span>
					</li> 
					<li class="choose choose2">
						<span>筛选</span>
						<span class="iconfont icon-shaixuan"></span>
					</li>
				</ul>
				<!--点击筛选出现之前隐藏的那一部分-->
				<div class="class_sortContent show">
					<div class="class_bg"></div>
					<ul class="class_sort">
						<li id="def" data-name=""  class="class_active">默认排序<span class="iconfont icon-gou class_check"></span></li>
						<li id="desc" data-name="desc"  >价格由高到低<span class="iconfont class_check"></span></li>
						<li id="asc" data-name="asc" >价格由低到高<span class="iconfont class_check"></span></li>
					</ul>
				</div>
				<!--图片展示那一大部分-->
				<ul class="lists">
				</ul>
				<!--图片展示那一大部分
				<ul class="lists">
				</ul>
				<ul class="lists"></ul>
				<ul class="lists"></ul>
				<ul class="lists"></ul>
				<ul class="lists"></ul>-->
		</div>
		

		<!--筛选右侧滑出来的那一部分-->
		<div class="class_filterContent moved">
			<div style="min-height: 100%;">
			<div class="class_price">
				<h3>价格</h3>
				<div class="price_box">
					<input id="minPrice" placeholder="最低价" type="text" />
					<span>—</span>
					<input id="maxPrice" placeholder="最高价" type="text" />
				</div>
			</div>
			<div class="address">
				<h3>所在地</h3>
				<div class="choose_address choose_address1 mui-input-row" id="chooseCity">
					<span class="address_text address_text1 result-tips">选择所在地</span>
					<span class="address_text show-result"></span>
					<span class="iconfont icon-arrfill_d-copy-copy"></span>
				</div>
			</div>
			<div class="service_box">
				<h3>服务类型</h3>
				<div class="service_style">
					<div data-name="1">线上</div>
					<div data-name="2">线下</div>
				</div>	
			</div>
			<div class="address address1">
				<h3>院校等级</h3>
				<!--<div class="choose_address"><span class="address_text">选择院校等级</span><span class="iconfont icon-arrfill_d-copy-copy"></span></div>-->
				<div class="choose_address mui-input-row" id="chooseSchool">
					<span class="address_text result-tips">选择院校等级</span>
					<span class="address_text show-result"></span>
					<span class="iconfont icon-arrfill_d-copy-copy"></span>
				</div>
			</div>
			
			</div>
			<div class="enure_content">
			   <div class="reset_box">重置</div>
			   	<div class="enure_box">确定</div>
			</div>
		
		</div>
		<!--遮罩层-->
		<div class="mask moved"></div>
		<c:forEach varStatus="status" items="${skillClass }" var="cla" >
			<input type="hidden" id="orderp${cla.id }" value="">
		</c:forEach>
		<input type="hidden" id="fid2" value="${fid }">
		
		<input type="hidden" id="cityName" value="">
		<input type="hidden" id="schoolLabel" value="">
		<input type="hidden" id="serviceType" value="">
		<input type="hidden" id="page" name="page" value="1">
		<input type="hidden" id="seachValue" name="seachValue" value="${map.seachValue }">
		</div>
		<!--底部条-->
		<nav class="foot-bar-tab">
			<a class="foot-tab-item foot-active" href="${path}/home/index.html;">
				<span class="foot-icon foot-icon-home"></span>
				<span class="foot-tab-label">首页</span>
			</a>
			<a class="foot-tab-item " href="${path}/chatMesseage/queryChatList.html">
				<span class="foot-icon foot-icon-pub"></span>
				<span class="foot-tab-label">消息</span>
			</a>
			<a class="foot-tab-item refuse_publish" href="${path}/publish/publishInfo.html">
				<span class="foot-icon foot-icon-release"></span>
				<span class="foot-tab-label">发布</span>
			</a>
			<a class="foot-tab-item" href="${path}/skillOrder/SelectedList.html">
				<span class="foot-icon foot-icon-bus"></span>
				<span class="foot-tab-label">已选</span>
			</a>
			<a class="foot-tab-item" href="${path}/home/getmyInform.html">
				<span class="foot-icon foot-icon-person"></span>
				<span class="foot-tab-label">我的</span>
			</a>
		</nav>
	</body>
	
	<script type="text/javascript" src="${path}/xkh_version_2/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/mui.picker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/mui.poppicker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/city.data.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/class_list_search2-1.html.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2/js/dropload.min.js" ></script>
    <script type="text/javascript">
   	var proList = ${proList };
   	var labelList = ${labelList };
   	
   	var seachValue=$("#seachValue").val();
   	</script>
    	<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
   

</html>
