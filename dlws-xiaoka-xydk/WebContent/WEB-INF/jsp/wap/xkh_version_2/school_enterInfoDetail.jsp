<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/iconfont/iconfont.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/mui.picker.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/mui.poppicker.css"/>
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/style.css" />
			<title>我的信息</title>
	</head>
	
	
	
	
	<body>
	<!--遮罩-->
		<div class="webservice_mask mask_none"></div>
		<div class="webservice_container">
		<div class="enterInfo_top">
			<!-- 技能达人顶部图片 -->
				<c:if test="${paramsMap.enterTypeName =='jndr'}">
					<c:if test="${not empty  jnImg }">
							<img src="${jnImg.picUrl }?${person_top}" class="search_pic"  />
				    </c:if>
				    <c:if test="${empty jnImg }">
							<img src="${path}/xkh_version_2.1/img/skill_top.jpg?${person_top}" class="search_pic"  />
			       </c:if>
				</c:if>
				<!-- 高校社团顶部图片 -->
				<c:if test="${paramsMap.enterTypeName =='gxst'}">	
					<c:if test="${not empty  gxImg }">
							<img src="${gxImg.picUrl }?${person_top}" class="search_pic"  />
				    </c:if>
				    <c:if test="${empty gxImg }">
							<img src="${path}/xkh_version_2.1/img/school_top.jpg?${person_top}" class="search_pic"  />
			       </c:if>
				</c:if>	
		</div>
		<!--公有信息部分-->
		<form id="formId" action="${path }/highSchoolSkill/updateSettleUser.html" method="post" enctype="multipart/form-data">
		<ul class="message_content">
			<li>
				<span class="enter_name">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span>
				<div id="activeCity" class="mui-input-row">
					<span class="myInfo_name enter_city result-tips">${basicInfo.cityName}</span>
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;校</span>
				<div id="activeShcool" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">${basicInfo.schoolName}</span>
				   <span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">入驻名称</span>
				<input class="inp" name="userName" id="userName" type="text"   value="${basicInfo.userName}" disabled="disabled" />
			</li>
			
			<li>
				<span class="enter_name headPic">入驻头像</span>
				<span class="enter_jia myInfo_photo">
				<span id="addText"></span>
						<img id="myAvatar" name="myAvatar" src="${basicInfo.headPortrait }" class="search_pic"  />
				</span>
				<span class="iconfont icon-jiahao new_jia enter_youjiantou icon_pic" ></span>
			</li>
			
			
			<c:forEach items="${settleList}" var="one">
				<c:choose>
					<c:when test="${one.typeName=='activtyUrl'}">
						<li>
							<span class="enter_name">活动图片</span>
							<span class="myInfo_name "></span>
							<div class="webservice_content">
								<!--添加的图片-->
								<div class="picture_box" id="picture_box_activity">
									<c:if test="${not empty imgArr}">
										<c:forEach items="${imgArr }" var="everyimg" >
											<div class="add_picture">
												<img src="${everyimg}" class="search_pic" />
												
												<span onclick="delImg('${objMap.xkh_imgId }','${everyimg}',this)" class="iconfont icon-shanchu"></span>
												<span class="cover"></span>
											</div>
										</c:forEach>
									</c:if>
								</div>
								<!--<添加的图片完-->
								<div class="add_pic">
									<span class="iconfont icon-pic"></span>
									<span class="size">+添加图片</span>
								</div>
							</div>
							<p class="limit">*请上传2-9张活动图片</p>
						</li>
					</c:when>
				</c:choose>
			</c:forEach>
			
			
			<c:forEach items="${settleList}" var="one">
				<c:choose>
					<c:when test="${one.typeName=='activCase'}">
						<li>
							<span class="enter_nameT">活动案例</span>
							<div class="textarea">
								<textarea class="introduce" name="activCase" id="activCase" onkeyup="load()" disabled="disabled" >${one.typeValue }</textarea>
								<div id="span" class="span"><span>0</span>/200</div>
							</div>
						</li>
					</c:when>
				</c:choose>
			</c:forEach>
			<li>
				<span class="enter_nameT">
				<c:if test="${paramsMap.enterTypeName =='gxst'}">社团介绍</c:if>
				<c:if test="${paramsMap.enterTypeName =='jndr'}">个人介绍</c:if></span>
				<div class="textarea">
					<c:if test="${paramsMap.enterTypeName =='gxst'}">
						<textarea class="school_introduce" id="aboutMe" name="aboutMe" disabled="disabled">${basicInfo.aboutMe}</textarea>
					</c:if>
					<c:if test="${paramsMap.enterTypeName =='jndr'}">
						<textarea class="school_introduce" id="aboutMe" name="aboutMe" disabled="disabled">${basicInfo.aboutMe}</textarea>
					</c:if>
					<div id="span4" class="span"><span>0</span>/200</div>
				</div>
			</li>
			<li>
				<span class="enter_name">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
				<input class="inp" name="contactUser" id="contactUser" type="text"  value="${basicInfo.contactUser}" disabled="disabled" />
			</li>
			<li>
				<span class="enter_name">联系电话</span>
				<input class="phone inp" type="text" name="phoneNumber" id="phoneNumber"  value="${basicInfo.phoneNumber}" disabled="disabled" />
				<p class="warn_tel"></p>
			</li>
			<li>
				<span class="enter_name">联系邮箱</span>
				<input class="letter inp" type="text" name="email" id="email"  value="${basicInfo.email}" disabled="disabled" />
				<p class="warn_letter"></p>
			</li>
			<li>
				<span class="enter_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号</span>
				<input class="wechat inp" type="text" name="wxNumber" id="wxNumber"  value="${basicInfo.wxNumber}" disabled="disabled" />
				<p class="warn_wechat"></p>
			</li>
		</ul>
		<input type="hidden" name='picUrl' id='picUrl' class='input' value="${basicInfo.headPortrait }" required="required" />
		<input type="hidden" name="activtyUrl" id="activtyUrl" value="${basicInfo.imgString }" required="required" />
		<input type="hidden" name='id' id='id' class='input' value="${basicInfo.id }" required="required" />
		<input type="hidden" name='openId' id='openId' class='input' value="${basicInfo.openId }" required="required" />
		<input type="hidden" name='enterId' id='enterId' class='input' value="${basicInfo.enterId }" required="required" />
		<input type="hidden" name='viewNum' id='viewNum' class='input' value="${basicInfo.viewNum }" required="required" />
		</form>
		<!-- <form class="enter_form" onsubmit="return false">
			<button onclick="sub()">提交</button>
		</form> -->
		<div class="window_mask"></div>
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.min.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.picker.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.poppicker.js"></script>
	<script src="${path}/xkh_version_2.1/js/city.data.js"></script>
	<script src="${path}/xkh_version_2.1/js/shcool.data.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/enterInfo.js"></script>
	<!-- <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script> -->
	<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
	<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
	<script>
     //字数限制
	function sub(){
		var h=$(".webservice_container").height();
		$(".webservice_mask").css("height",h);
		$(".webservice_mask").removeClass("mask_none");
		
		 var cityId=$("#cityId").val();//城市id
    	 var schooId=$("#schooId").val();//学校id
    	 var userName=$("#userName").val();//入住名称
    	 var activCase=$("#activCase").val();//活动案例
    	 var aboutMe=$("#aboutMe").val();//个人介绍
    	 var activtyUrl=$("#activtyUrl").val();//活动图片
    	 var contactUser=$("#contactUser").val();//创建人 
    	 var phoneNumber=$("#phoneNumber").val();//电话
    	 var email=$("#email").val();//邮箱
    	 var wxNumber=$("#wxNumber").val();//微信号
    	 
    	 if(cityId==""||cityId==null){
 			alert("请选择城市");
 			$(".webservice_mask").addClass("mask_none");
 			return;
 		}
    	 if(schooId==""||schooId==null){
 			alert("请选择学校");
 			$(".webservice_mask").addClass("mask_none");
 			return;
 		}
    	 if(userName==""||userName==null){
  			alert("请输入入驻名称");
  			$(".webservice_mask").addClass("mask_none");
  			return;
  		}
		  
    	 if(activCase.length<20||activCase.length>200){
  			alert("活动案例长度大于20小于200");
  			$(".webservice_mask").addClass("mask_none");
  			return;
  		}
    	 var entertype="${paramsMap.enterTypeName}";
    	 if(aboutMe.length<20 || aboutMe.length>200){
    		 if(entertype=='gxst'){
	   			alert("团队介绍长度大于20小于200");
	   			$(".webservice_mask").addClass("mask_none");
    		 }
    		 if(entertype=='jndr'){
    			alert("个人介绍长度大于20小于200");
    			$(".webservice_mask").addClass("mask_none");
    		 }
   			return;
   		}
    	 if(contactUser=="" || contactUser.length>10){
   			alert("联系人不为空且10字以内");
   			$(".webservice_mask").addClass("mask_none");
   			return;
   		}
    	 var regNb=/^1[3|7|5|8]\d{9}$/;
    	 if(phoneNumber==""||!regNb.test(phoneNumber)){
   			alert("电话号码格式有误或不能为空");
   			$(".webservice_mask").addClass("mask_none");
   			return;
   		}
    	 var regEmail=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
    	 if(email==""||!regEmail.test(email)){
   			alert("邮箱格式有误或不能为空");
   			$(".webservice_mask").addClass("mask_none");
   			return;
   		}
    	 var regEma=/^[a-zA-Z\d_]{5,}$/;
    	 if(wxNumber==""){//||!regEma.test(wxNumber)
    			alert("微信号格式有误或不能为空");
    			$(".webservice_mask").addClass("mask_none");
    			return;
    	}
    		 uploadHeadImageSelf();
     }
    
     
     
   //选择所在地，学校部分
   
   //省名称
   var proName = "${proMap.province}";
   //市名称
   var cityName = "${objMap.cityName}";
   //院校类别--id
   var schoolType = "${sholType.VALUE}";
   //院校名称--id
   var schoolName = "${objMap.schoolId}";
   
   
 	<%-- mui.init();
	var schoolData="";
 	var cityPicker = new mui.PopPicker({
		layer: 2
	});
 	var city = ${proList};
		cityPicker.setData(city);
		var showCityPickerButton = document.getElementById('activeCity');
		
		showCityPickerButton.addEventListener('tap', function(event) {
			//document.querySelector('#activeCity .show-result').innerText = cityName;
			//城市回显
			$("#schoolId").val("");
			cityPicker.pickers[0].setSelectedValue(proName, 0, function() {
				setTimeout(function() {
				cityPicker.pickers[1].setSelectedValue(cityName);
				}, 100);
			});
			cityPicker.show(function(items) {
				document.querySelector('#activeCity .result-tips').style.display = "none";
				document.querySelector('#activeCity .show-result').style.display = "block";
				document.querySelector('#activeCity .show-result').innerText = items[1].text;
				//items[1].value = cityId;
				$("#cityId").val(items[1].value);
				
				//alert("cityId=:"+cityId);
				
				document.querySelector('#activeShcool .result-tips').style.display = "block";
				document.querySelector('#activeShcool .show-result').style.display = "none";  
				//返回 false 可以阻止选择框的关闭
				//return false;
				$.ajax({
	        		type : "post",
	        		url : "<%=contextPath%>/skillUser/chooseSchool.html",
	        		dataType : "json",
	        		data : {cityId:items[1].value},
	        		success : function(data) {
	        			var shcoolPicker = new mui.PopPicker({
	        				layer:2
	        			});
	        			shcoolPicker.setData(data);
	        			var showShcoolPickerButton = document.getElementById('activeShcool');
	        			
	        			showShcoolPickerButton.addEventListener('tap', function(event){
	        				//学校回显
		        		shcoolPicker.pickers[0].setSelectedValue(schoolType, 0, function() {
		        			//alert("222"+schoolType)
        					shcoolPicker.pickers[1].setSelectedValue(schoolName);
		        		});
	        			shcoolPicker.show(function(items) {
	        				document.querySelector('#activeShcool .result-tips').style.display = "none";
	        				document.querySelector('#activeShcool .show-result').style.display = "block";
	        				document.querySelector('#activeShcool .show-result').innerText = items[1].text;
	        				$("#schooId").val(items[1].value);
	        				
	        				//
	        				//返回 false 可以阻止选择框的关闭
	        				//return false;
	        			});
	        		}, false);
	        		var muiPickerLh = $(".mui-poppicker").length;
	        		if(muiPickerLh>2){
	        		$(".mui-poppicker").eq(muiPickerLh-2).remove();
	        		}
	        		}
	        	});
			});
		}, false); --%>
		
		
		
		//图片上传部分，头像
		
		//微信上传 全局变量
		//判断是否修改图片
		var isHeade = false;
		var isActivity = false;
		
		//微信上传 全局变量
		var wxSelf;
		var localArr = new Array();
		var localHeadArr = new Array();//头像
		var localActivityArr = new Array();//存放活动的图片
		/* var localHeadArr = "${objMap.picUrl }";
		var localActivityArr = "${objMap.imgString }"; */
		var serverArr = new Array();
		
		var serverHeadArr = new Array();
		var serverActivityArr = new Array();

		var headFlag = true;
		
		/*001 选择图片   日常推文选择图片*/
		function uploadImage(flag,num) {
			var clientUrl = window.location.href;
			var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : reqPath,
				dataType : 'json',
				data : {
					"clientUrl" : clientUrl
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					/* alert(return_date ); */
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : false, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
					});
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						wxSelf=wx;
						wx.chooseImage({
							count: num, // 默认9
							sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
							sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
							success: function (res) {
								
								if(flag =="1"){//头像
									localHeadArr = res.localIds;
									if(localHeadArr.length > 0){
								        for(var j = 0; j < localHeadArr.length; j++){
								        	 var myAvatar=localHeadArr[0];
										        $("#addText").html("");
										        $("#myAvatar").attr("src",myAvatar);
										        $("#myAvatar").css('display','block');
										        headFlag=false; 
										        isHeade = true;
								        }
									}
								}
							
								if(flag =="3"){//活动图片
									localActivityArr= res.localIds;
									if(localActivityArr.length > 0){
										var result = "";
								        for(var j = 0; j < localActivityArr.length; j++){
								        	result += "<div id='xkh_activity_"+j+"' class='add_picture'>"+
													"<img src='"+localActivityArr[j]+"' class='search_pic' />"+
														"<span onclick='removeImg("+j+","+localActivityArr[j]+")' class='iconfont icon-shanchu' ></span>"+
														"<span class='cover'></span>"+
													"</div>";
								        	var imgNum = $("#picture_box_activity img").length;
									        var picNum =  parseInt(imgNum) + parseInt(localActivityArr.length);
									        if(picNum>9){
									        	alert("请上传2-9张图片！");
									        	$(".webservice_mask").addClass("mask_none");
									        }else{
									        	$("#picture_box_activity").append(result);
										        isActivity = true;
									        } 
										}
								}
								//localArr= res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
							}
						 });

						});
						 wx.error(function(res) {
							alert(res.errMsg);
						});
					}
			});
		}
		
		
		/*002 头像 点击提交后上传到微信*/
		function uploadHeadImageSelf(){
			 //alert("pic count="+localArr.length); 

			 if(0==localHeadArr.length){
				 uploadActivityImageSelf();
			 }else{
				 for(var i=0;i<localHeadArr.length;i++){
					 wxSelf.uploadImage({
					        localId: localHeadArr[i], // 需要上传的图片的本地ID，由chooseImage接口获得
					        isShowProgressTips: 1, // 默认为1，显示进度提示
					        success: function (res) {
					            var serverId = res.serverId; // 返回图片的服务器端ID
					            serverHeadArr.push(serverId);
					            
					            if(i == localHeadArr.length){
					            	 //alert("执行上传 count"+serverArr);  
					                var serverStr="";
					                if(serverHeadArr[0].length >0 ){
					    	            serverStr=serverHeadArr[0] ;
					    	           // alert(serverStr);
					    	            $.ajax({
					    		        		type : "post",
					    		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
					    		        		dataType : "json",
					    		        		data : {serverId:serverStr},
					    		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
					    		        			//alert("data="+data.path);
					    		        			$("#picUrl").val(data.path);
					    		        			//alert("这是头像的url:"+data.path)
					    		        			uploadActivityImageSelf();
					    		        		}
					    		        	});	
					                	
					                }else{
					                	alert("上传头像失败");
					                }
					                            	
					            }
					         }
					    }); 
				 	}
			 }
			 //downloadImageSelf(serverArr);
		}
		
		/*活动图片 点击提交后上传到微信*/
		/*使用递归的方式进行上传图片*/
		function uploadActivityImageSelf() {
			
            if (localActivityArr.length == 0) {
            	var serverStr="";
            	if(0 == serverActivityArr.length ){
            		$("#formId").submit();
            		$(".webservice_mask").removeClass("mask_none");
            	}else{
		            for(var j=0;j<serverActivityArr.length;j++){
		            	serverStr+=serverActivityArr[j]+";" ;
		            }
	            	
	                $.ajax({
		        		type : "post",
		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
		        		dataType : "json",
		        		data : {serverId:serverStr},
		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
		        			
		        			//alert("+++++++++data++++++:"+data);	
		        			var imageList = data;
		        			//var pathList = data.
		        			
		        			var imgPath="";
		        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
							for(var f=0;f<imageList.length;f++){
						//		alert("+++++++++++++++:"+imageList[f]);
								var imgPathTemp = imageList[f];
								imgPath += imgPathTemp+",";
							}
							
							var u = $("#activtyUrl").val()+imgPath;
							//alert("===========imgUrl============"+$("#activtyUrl").val());
							$("#activtyUrl").val(u)
							//alert("===========最终赋值的imgUrl============"+u);
							
							var imgNum = $("#picture_box_activity img").length;
							if(imgNum<2||imgNum>9){
								alert("活动图片个数为2-9");
								$(".webservice_mask").addClass("mask_none");
								return;
							}
		        			$("#formId").submit();
		        			$(".webservice_mask").removeClass("mask_none");
		        		}
		        	});	
            	}
            }
            var localId = localActivityArr[0];
            //tmd 一定要加     解决IOS无法上传的坑 
            if (localId.indexOf("wxlocalresource") != -1) {
                localId = localId.replace("wxlocalresource", "wxLocalResource");
            }
            wxSelf.uploadImage({
                localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
                isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
                success: function (res) {
                   // serverIds.push(res.serverId); // 返回图片的服务器端ID
                   var serverId = res.serverId; // 返回图片的服务器端ID
                   serverActivityArr.push(serverId);
                    localActivityArr.shift();
                    uploadActivityImageSelf(localActivityArr);
                    serverStr+=serverId+";" ;
                   // alert("localArr的长度："+localActivityArr.length);
                },
                fail: function (res) {
                    alert("上传失败，请重新上传！");
                }
            });
        }
		
		function removeImg(imageId,val){
			$("#xkh_activity_"+imageId).remove();
			//$("#xkh_"+imageId).hide();
			localActivityArr.remove(val);
		}
		
		//点击删除图片
		function delImg(imgId,val,obj){
			/* var imId = this.get(id); */
			//alert("这是图片的id:"+imgId);
			if(confirm("确定删除?")){
					//表示修改需要数据库删除
					$.ajax({
						type : "post",
						url : "<%=basePath%>highSchoolSkill/delImage.html",
						dataType : "json",
						data : {imgeId:imgId,imageUrl:val},
						success : function(data) {
							if(data.flag){
								$("#activtyUrl").val(data.typeValue);
								obj.parentElement.remove();
							}
						}
					});
		  }
	   }
    </script>
</html>
