
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/style.css" />
		<%--引入版本二的样式  暂时注释掉--%>
		<%-- <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.2/css/style.css" /> --%>
		<script src="${path}/wapstyle/js/template.js"></script>
		<title>服务管理</title>
	</head>
    <style>
    
    	nav{
    		position: fixed;
    		bottom: 0;
    	}
    	.publish_box .publish_case:last-child{
    		margin-bottom: 50px;
    	}
    	.foot-bar-tab {
	position: fixed;
	right: 0;
	left: 0;
	bottom: -1px;
	display: table;
	width: 100%;
	padding: 0;
	table-layout: fixed;
	z-index: 999;
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
	background: url(${path}/xkh_version_2.2/img/footer_bg.png) no-repeat;
	background-size: 100% 100%;
}

.foot-bar-tab .foot-tab-item {
	display: table-cell;
	overflow: hidden;
	width: 1%;
	text-align: center;
	vertical-align: middle;
	white-space: nowrap;
	text-overflow: ellipsis;
	color: #808080;
	padding-top: 29.5px;
	padding-bottom: 6px;
}

.foot-bar-tab .foot-tab-item .foot-icon {
	top: 0px;
	width: 6.4vw;
	height: 6.4vw;
	padding-top: 0;
	padding-bottom: 0;
	position: relative;
	z-index: 10;
	display: inline-block;
	line-height: 1;
}

.foot-icon {
	background-repeat: no-repeat;
	background-position: top center;
	background-size: 6.4vw auto;
}

.foot-icon-home {
	background-image: url(${path}/xkh_version_2.2/img/icon_home.png);
}

.foot-icon-pub {
	background-image: url(${path}/xkh_version_2.2/img/icon_pub.png);
}

.foot-icon-release {
	background-image: url(${path}/xkh_version_2.2/img/icon_release.png);
}

.foot-icon-bus {
	background-image: url(${path}/xkh_version_2.2/img/icon_bus.png);
}

.foot-icon-person {
	background-image: url(${path}/xkh_version_2.2/img/icon_person.png);
}

.foot-icon-advice {
	background-image: url(${path}/xkh_version_2.2/img/icon_advice.png);
}

.foot-icon-tel {
	background-image: url(${path}/xkh_version_2.2/img/icon_tel.gif);
}

.foot-active>span:first-child {
	background-position: center -6.4vw;
}

.foot-bar-tab .foot-tab-item.foot-active {
	color: #1d9243;
}

.foot-bar-tab .foot-tab-item .foot-icon~.foot-tab-label {
	font-size: 2.933vw;
	display: block;
	overflow: hidden;
	text-overflow: ellipsis;
}

.foot-bar-tab a:nth-of-type(3) {
	padding-top: 10px;
}

.foot-bar-tab a:nth-of-type(3) span:first-child {
	padding: 0;
	width: 11.2vw !important;
	height: 11.2vw !important;
	background-size: 11.2vw auto;
}

.foot-bar-tab a:nth-of-type(3) span:last-child {
	padding-top: 1px;
}  	
    </style>
	<body>
		<c:if test="${not empty skillBannerInfo }">
       		<div class="enterInfo_top noPublish_head">
				<img src="${skillBannerInfo.picUrl }?${addskill_banner}" class="search_pic"  />
			</div>
       	</c:if>
       	<c:if test="${empty skillBannerInfo }">
       		<div class="enterInfo_top noPublish_head">
				<img src="${path}/xkh_version_2.1/img/infomation.png?${addskill_banner}" class="search_pic"  />
			</div>
       	</c:if>
		
		<div class="switch_content">
			<div>
				<h3 class="switch">服务开关</h3></div>
			<span class="switch_pic"></span>
		</div>
		<!--开关以下部分-->
		<!--遮罩层-->
		<div class="publish_mask"></div>
		
		<div class="publish_box">
			<c:if test="${prefersize!=0}">
				<div class="service_bg">优选服务</div>
				<div class="service_consult">
					<span><img src="${path}/xkh_version_2.1/img/consult.png" class="search_pic" /></span>
					<span>精准咨询，答你所需</span>
				</div>
			</c:if>
			<div id="perferServe" class="service_wrapper"></div>
			<script id="perfer" type="text/html">
			{{each preferList as value j }}        
                <div class="substance_content">
				<a  href="${path}/skill_detail/getskill_detail.html?skillId={{value.id}}&classValue={{value.enterTypeValue}}&personId=${basicId}">
					<div class="substance_left"><img src="{{value.imgUrl}}" class="search_pic" /></div>
				</a>
				<div class="substance_right">
					<span class="service_money">{{value.introduce}}</span>
                    <div class="online_Box">
                       <span class="service_num">{{value.price}}元/次<span></span></span>
                       <span class="online">{{if value.serviceType==0}}线上{{/if}}
                                         {{if value.serviceType==1}}线下{{/if}}</span>
                    </div>
				</div>
                <div class="service_bground"></div>
				</div>	
              {{/each}}
              </script>
			
			<!-- 服务列表 -->
			<c:if test="${skillsize!=0}">
			<div class="service_bg">更多服务</div>
			</c:if>
			<div id ="skillListInfo"></div>
			<script id="skills" type="text/html">
			{{each skillList as value i }}
			<div class="publish_case">	
				<div class="publish_content">
					<div class="publish_title">
						<span class="title_pic"><img src="{{value.headPortrait}}" class="search_pic" style="border-radius:100%;"/></span>
						<h3>{{value.className }}</h3>
						<a class="edit" href="${path}/pubSkill/chosePubSkill.html?skillId={{value.id}}&classValue={{value.classValue}}&sid={{value.skillFatId}}&basicId=${basicId}">编辑</a>
					</div>
				</div>
				<div  class="substance_content">
					<a  href="${path}/skill_detail/getskill_detail.html?skillId={{value.id}}&classValue={{value.classValue}}">
						<div class="substance_left"><img src="{{value.imageStr}}" class="search_pic" /></div>
					</a>
			    		<div class="substance_right">
			    			<span class="substance_music">{{value.subTitle }}</span>
			    			<span class="substance_line">
								{{if value.serviceType==1}}线上{{/if}}
								{{if value.serviceType==2}}线下{{/if}}</span>
			    			<p>{{value.details }}</p>
                           <div class="add_Price">
                              <span class="substance_price">{{value.price }}</span>
							  <span class="substance_price">元/{{value.priceName}}</span>
                           </div>
			    			
			    		</div>
					
			    		<!--出来的删除-->
			    		<div class="move_del" data-name="{{value.id}}">删除</div>					
				</div>
			   			<div class="substance_bg"></div>
			</div>
			{{/each}}
		</script>
		</div>
		<%-- <nav class="foot-bar-tab">
			<a class="foot-tab-item foot-active" href="${path}/home/index.html;">
				<span class="foot-icon foot-icon-home"></span>
				<span class="foot-tab-label">首页</span>
			</a>
			<a class="foot-tab-item " href="${path}/chatMesseage/queryChatList.html">
				<span class="foot-icon foot-icon-pub"></span>
				<span class="foot-tab-label">消息</span>
			</a>
			<a class="foot-tab-item refuse_publish" href="../prompt/prompt.html">
				<span class="foot-icon foot-icon-release"></span>
				<span class="foot-tab-label">发布</span>
			</a>
			<a class="foot-tab-item" href="${path}/skillOrder/SelectedList.html">
				<span class="foot-icon foot-icon-bus"></span>
				<span class="foot-tab-label">已选</span>
			</a>
			<a class="foot-tab-item" href="${path}/home/getmyInform.html">
				<span class="foot-icon foot-icon-person"></span>
				<span class="foot-tab-label">我的</span>
			</a>
		</nav> --%>
		<!-- 底部 -->
		 <%@ include file="../xkh_version_2/foot.jsp"%>
		
	</body>
	
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script>
		var path = "${path}";
		//消息在第一序列
		var index = 4;
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
	<script>
		
		
		//获取优选服务    模板
		var preferList=${preferList };
		var perfer = template('perfer',preferList);
		document.getElementById('perferServe').innerHTML = perfer;
		
		
		//获取页面列表
		var skillsinfo=${spec };
		var special = template('skills',skillsinfo);
		document.getElementById('skillListInfo').innerHTML = special;
	
	
		//获取开启状态
		var isDisplay=${isDisplay}
		var basicId = ${basicId};
		if(isDisplay==0){
			//展示publish_mask mask_none   switch_pic switch_pic1
			$(".publish_mask").addClass("mask_none");
			$(".switch_pic").addClass("switch_pic1");
			
		}
		if(isDisplay==1){
			//隐藏publish_mask  switch_pic
			$(".publish_mask").addClass("");
			$(".switch_pic").addClass("");
		} 
		
		//遮罩层的高度
		var wh = $(".publish_box").height();
		$(".publish_mask").css("height", wh);
		//点击服务开关的时候
		if(0!=${skillsize}){
			$(document).on("click",".switch_pic",function() {
				$.ajax({
	                url:"${path}/skillUser/updateDisplay.html",
	                type:"post",
	                data:{
	                	basicId:basicId,
	                	isDisplay:isDisplay
	                },
	                success:function(data){
	                  
						$(".switch_pic").toggleClass("switch_pic1");
						$(".publish_mask").toggleClass("mask_none")
	                },
	                error:function(e){
	                    alert("错误！！");
	                }
	            });
			})
		}

		//左滑删除功能
		var h=$(".substance_content").height();
		$(".move_del").css({"line-height":h+"px","height":h});
		var onoff = true;
		$(".substance_right").on("touchstart", function(e) {
			onoff = true;
			e.preventDefault();
			var X = 0,Y = 0;
			   startX = e.originalEvent.changedTouches[0].pageX,
				startY = e.originalEvent.changedTouches[0].pageY;
				$(".substance_right").on("touchmove", function(e) {

					    moveEndX = e.originalEvent.changedTouches[0].pageX;
						moveEndY = e.originalEvent.changedTouches[0].pageY;
						X = moveEndX - startX;
						Y = moveEndY - startY;
					    onoff = false;
				});
				$(".substance_right").on("touchend", function(e) {
					var windowHeight = $(window).height(),
					  $body = $("body");
				  $body.css("height", windowHeight);
					e.preventDefault();
					if(Math.abs(X) > Math.abs(Y) && X < 0) {
						$(this).siblings(".move_del").removeClass("del_moved1").addClass("del_moved")
					}
					 if ( Math.abs(X) > Math.abs(Y) && X > 0 ) {
					   $(this).siblings(".move_del").removeClass("del_moved").addClass("del_moved1")
			         }

			         if(onoff){
			        	 var href = $(this).siblings("a").attr('href');
						 window.location.href=href;
							
					 };
		       });

		});
		
		$(".move_del").click(function() {
			var skillId=this.dataset.name;
			$(this).parents().siblings(".publish_content").hide();
			$(this).parent().hide();
			$(this).parent().removeAttr("href");
			$.ajax({
                url:"${path}/skillUser/deleteSkillInfo.html",
                type:"post",
                data:{skillId:skillId},
                success:function(data){
                   //alert(data.flag)
                  // window.location.reload();
                },
                error:function(e){
                    alert("error!");
                }
            });
		});
		
	</script>
	 <script type="text/javascript" src="${path}/xkh_version_2.2/js/foot.js" ></script>
</html>
