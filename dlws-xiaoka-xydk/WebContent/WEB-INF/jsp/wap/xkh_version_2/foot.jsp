<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<nav class="foot-bar-tab">
	<a class="foot-tab-item foot-active" href="${path}/home/index.html;">
		<span class="foot-icon foot-icon-home"></span> <span
		class="foot-tab-label">首页</span>
	</a> 
	<a class="foot-tab-item "
		href="${path}/chatMesseage/queryChatList.html"> <span
		class="foot-icon foot-icon-pub"></span>
		<span class="cfz_circle" id="message"></span>
		 <span class="foot-tab-label">消息</span>
	</a> 
	<a class="foot-tab-item refuse_publish"

		href="${path}/publish/publishInfo.html"> <span
		class="foot-icon foot-icon-release"></span> 
		<span class="foot-tab-label">发布</span>
	</a> 
	<a class="foot-tab-item" href="${path}/skillOrder/SelectedList.html">
		<span class="foot-icon foot-icon-bus"></span>
		<span class="cfz_circle" id="select"></span>
		 <span class="foot-tab-label">已选</span>
	</a> 
	<a class="foot-tab-item" href="${path}/home/getmyInform.html"> <span
		class="foot-icon foot-icon-person"></span>
		<span class="cfz_circle circle"></span> <span
		class="foot-tab-label">我的</span>
	</a>
</nav>

