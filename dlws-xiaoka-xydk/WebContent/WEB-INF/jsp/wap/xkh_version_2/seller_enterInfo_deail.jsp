<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<%@ include file="image_data.jsp"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/iconfont/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/mui.picker.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/mui.poppicker.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/style.css" />
		<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<script type="text/javascript" src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
		<title>我的信息-校内商家入驻</title>
		<style>
				.shade{
				    width:100%;
				    background-color:#000;
				    position:absolute;
				    top:0;
				    left:0;
				    z-index:2;
				    opacity:0.0;
				    /*兼容IE8及以下版本浏览器*/
				    filter: alpha(opacity=00);
				    display:block;
				}
		</style>
	</head>
	<body>
	<!--遮罩-->
		<div class="webservice_mask mask_none"></div>
		<div class="webservice_container">
		<div class="enterInfo_top">
					<c:if test="${not empty  jnImg }">
							<img src="${jnImg.picUrl }?${person_top}" class="search_pic"  />
				    </c:if>
				    <c:if test="${empty jnImg }">
							<img src="${path}/xkh_version_2.1/img/sell_top.jpg${addenter_banner}" class="search_pic" />
			       </c:if>
		</div>
		<ul class="message_content">
			<li>
				<span class="enter_name">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span>
				<div id="activeCity" class="mui-input-row">
					<span class="myInfo_name enter_city result-tips">${objMap.cityName}</span>
					<span class="myInfo_name show-result"></span>
					<input type="hidden" name="cityId" id="cityId" value="${objMap.cityId}" >
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;校</span>
				<div id="activeShcool" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">${objMap.schoolName}</span>
					<span class="myInfo_name show-result"></span>
					<input type="hidden" name="schoolId" id="schooId" value="${objMap.schoolId}">
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>

			</li>
			<li>
				<span class="enter_name">入驻名称</span>
				<input class="inp" type="text" id="userName" name="userName"  maxlength="10" value="${objMap.userName}" placeholder="请填写入驻名称" />
				<input type="hidden" name="id" id="id" value="${objMap.id}" > 
			</li>
				<li>
						<span class="enter_name headPic">入驻头像</span>
						<span class="enter_jia myInfo_photo">
							<img id="myAvatar" name="" src="${objMap.headPortrait}" class="search_pic" style="display:block;" />
							<input type="hidden" value="${objMap.headPortrait}" name="picUrl" id="headPortrait">
						</span>
			    </li>
			<li>
				<span class="enter_name">商家类型</span>
				<c:forEach items="${businTypeLists}" var="businType">
					<c:if test="${businType.dic_value == echoMap.shopType}">
						<c:set value="${businType.dic_name}" var="buType"></c:set>
					</c:if>
				</c:forEach>
				<div id="sellStyle" class="mui-input-row">
							<span class="myInfo_name enter_school result-tips">${buType}</span>
							<span class="myInfo_name show-result"></span>
							<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<p class="place_name">合作形式</p>
				<div class="sellCheck_box">
				<c:forEach items="${cooperaTypeList}" var="cooperaType">
					
					<c:forEach items="${cooperaTypes}" var="selectCoopera">
						<c:if test="${cooperaType.dic_value == selectCoopera.key}">
							<c:set var="flage" value="trues" scope="request"></c:set>
						</c:if>
					</c:forEach>
					<c:choose>
						<c:when test="${flage=='trues'}">
							<div class="sell_check">
								<span class="check_img"></span>
								<span>${cooperaType.dic_name}</span>
							</div>
						</c:when>
						<c:otherwise>
							<div class="sell_check">
								<span class="check_pic"></span>
								<span>${cooperaType.dic_name}</span>
							</div>
						</c:otherwise>
					</c:choose>
					<c:set var="flage" value="false" scope="request"></c:set> 
				</c:forEach>
				</div>

				<div class="sellNum_box">
				<c:forEach items="${cooperaTypeList}" var="cooperaType">
					
					<c:forEach items="${cooperaTypes}" var="selectCoopera">
						<c:if test="${cooperaType.dic_value == selectCoopera.key}">
							<c:set var="fl" value="trues" scope="request"></c:set>
							<c:set var="price" value="${selectCoopera.value}" scope="request"></c:set>
						</c:if>
					</c:forEach>
					<c:choose>
						<c:when test="${fl=='trues'}">
								<div class="sell_num">
									<span class="sell_span1">${cooperaType.dic_name}</span>
									<div class="num_right">
										<div class="sell_bottom" disabled="disabled">
											<input type="hidden" value="${cooperaType.dic_value}" name="cooperaType">
											<input type="number" value="${price}" class="inpNum" name="cooperaPrice" onKeypress="return (/[\d*\.*\d]/.test(String.fromCharCode(event.keyCode)))" placeholder="请填写报价" />
										</div>
										<span class="sell_span2">元/次</span>
									</div>
								</div>
						</c:when>
						<c:otherwise>
								<div class="sell_num sellNum_none">
									<span class="sell_span1">${cooperaType.dic_name}</span>
									<div class="num_right">
										<div class="sell_bottom" disabled="disabled">
											<input type="hidden" value="${cooperaType.dic_value}" name="cooperaType">
											<input type="number" name="cooperaPrice" class="inpNum" onKeypress="return (/[\d*\.*\d]/.test(String.fromCharCode(event.keyCode)))" placeholder="请填写报价" />
										</div>
										<span class="sell_span2">元/次</span>
									</div>
								</div>
						</c:otherwise>
					</c:choose>
					<c:set var="fl" value="false" scope="request"></c:set> 
				</c:forEach>
				</div>
			</li>
			<li>
				<span class="enter_name enterName">门店照片</span>
				<span class="enter_name enterName2">全景照片</span>
				<span class="myInfo_name">点击加号上传全景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="allPhoto">
						<c:forEach items="${listAll}" var="all" varStatus="bs">
							<div class="add_picture">
							<img src="${all}" class="search_pic" />
							<span onclick="delImg('${storageId.allPhoto}','${all}',this,'allPhoto_img')" class="iconfont icon-shanchu"></span>
							<span class="cover"></span>
							</div>
						</c:forEach>
					</div>
				</div>
				<span class="enter_name add_photo enterName2">中景照片</span>
				<span class="myInfo_name add_place">点击加号上传中景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="middlePho">
						<c:forEach items="${listMiddle}" var="middle" varStatus="bs">
						 <div class="add_picture">
							<img src="${middle}" class="search_pic" />
							<span class="cover"></span>
						 </div>
						</c:forEach>
					</div>
				</div>
				<span class="enter_name add_photo enterName2">近景照片</span>
				<span class="myInfo_name add_place">点击加号上传近景照片</span>
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="closePhoto">
						<c:forEach items="${listClose}" var="close" varStatus="bs">
							<div class="add_picture">
								<img src="${close}" class="search_pic" />
								<span onclick="delImg('${storageId.closePhoto}','${close}',this,'closePhoto_img')" class="iconfont icon-shanchu"></span>
								<span class="cover"></span>
							</div>
						</c:forEach>
					</div>
				</div>
				<p class="limit">*请上传2-9张场地照片</p>
			</li>
			<li>
				<span class="enter_nameT">门店描述</span>
				<div class="textarea">
					<textarea class="introduce"  name="storeDec" id="storeDec" onkeyup="load()"  required="required" placeholder="请填写和门店相关的信息，譬如门店销售品类，人流量优势等等">${echoMap.storeDec}</textarea>
					<div id="span" class="span"><span>0</span>/200</div>
				</div>
			</li>
			<li>
				<span class="enter_name">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
				<input class="inp" maxlength="10" type="text" name="contactUser" id="contactUser" value="${objMap.contactUser}" placeholder="填写您的姓名" />
			</li>
			<li>
				<span class="enter_name">联系电话</span>
				<input class="phone inp" type="number" id="phoneNumber" name="phoneNumber" value="${objMap.phoneNumber}" placeholder="填写您的手机号码" maxlength="15" onKeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))"/>
				<p class="warn_tel"></p>
			</li>
			<li>
				<span class="enter_name">联系邮箱</span>
				<input class="letter inp" type="text" name="email" id="email" value="${objMap.email}" placeholder="填写您的联系邮箱"  onKeypress="return (/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(String.fromCharCode(event.keyCode)))"/>
				<p class="warn_letter"></p> 
			</li>
			<li>
				<span class="enter_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号</span>
				<input class="wechat inp" type="text" name="wxNumber" id="wxNumber" value="${objMap.wxNumber}" placeholder="填写您的微信账号" />
				<p class="warn_wechat"></p>
			</li>
		</ul>
		 <input type="hidden"  value="${echoMap.shopType}" id="shopType" name="shopType">
	</div>
	<div class="shade"></div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.min.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.picker.js"></script>
	<script src="${path}/xkh_version_2.1/js/mui.poppicker.js"></script>
	<script src="${path}/xkh_version_2.1/js/city.data.js"></script>
	<script src="${path}/xkh_version_2.1/js/shcool.data.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/enterInfo.js"></script>
	<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
	<script>
	 	var cityData = ${proList}; 
	    var labelList = ${labelList};
	    var serverAlllArr = new Array(); //存放活动的图片
		var localAlllArr = new Array(); //存放活动的图片
	    var path = "${path}";
	    
	    
		$(".sell_check").click(function() {
			var index = $(this).index();
			console.log(index)
			if($(">span:first-child", this).hasClass("check_pic")) {
				$(">span:first-child", this).removeClass("check_pic").addClass("check_img");
				$(".sellNum_box .sell_num").eq(index).removeClass("sellNum_none");
				$(".sellNum_box .sell_num .inpNum").eq(index).val("");
			} else {
				$(">span:first-child", this).removeClass("check_img").addClass("check_pic");
				$(".sellNum_box .sell_num").eq(index).addClass("sellNum_none");
				$(".sellNum_box .sell_num .inpNum").eq(index).val("");
				
			}
		});
		  //选择商家类型
	var businTypeList = ${businTypeList};
    var sellStyle=new mui.PopPicker({
    	layer:1
    });
    sellStyle.setData(businTypeList);
					var showsellStyleButton = document.getElementById('sellStyle');
					showsellStyleButton.addEventListener('tap', function(event) {
					sellStyle.show(function(items) {
						
							document.querySelector('#sellStyle .result-tips').style.display = "none";
							document.querySelector('#sellStyle .show-result').style.display = "block";
							document.querySelector('#sellStyle .show-result').innerText = items[0].text;
							$("#shopType").val(items[0].value);
							//返回 false 可以阻止选择框的关闭
							//return false;
						});
	}, false);
					
	//选择所在地
    mui.init();
    	var cityPicker = new mui.PopPicker({
		layer: 2
	});
    	var shcoolPicker = new mui.PopPicker({
			layer:2
	});
	cityPicker.setData(cityData);
	var showCityPickerButton = document.getElementById('activeCity');
	showCityPickerButton.addEventListener('tap', function(event) {
		cityPicker.show(function(items) {
			document.querySelector('#activeCity .result-tips').style.display = "none";
			document.querySelector('#activeCity .show-result').style.display = "block";
			document.querySelector('#activeCity .show-result').innerText = items[1].text;
			$("#cityId").val(items[1].value);
			document.querySelector('#activeShcool .result-tips').style.display = "block";
			document.querySelector('#activeShcool .show-result').style.display = "none"; 
			$.ajax({
        		type : "post",
        		url : "<%=contextPath%>/skillUser/chooseSchool.html",
			        		dataType : "json",
			        		data : {cityId:items[1].value},
			        		success : function(data) {
			        			
			        			// 新增城市与学校联动
			        			$("#schooId").val(""); 
			        			//清空Id值
			        			shcoolPicker.setData(data);
			        			var showShcoolPickerButton = document.getElementById('activeShcool');
			        			showShcoolPickerButton.addEventListener('tap', function(event){
			        			shcoolPicker.show(function(items) {
			        				document.querySelector('#activeShcool .result-tips').style.display = "none";
			        				document.querySelector('#activeShcool .show-result').style.display = "block";
			        				document.querySelector('#activeShcool .show-result').innerText = items[1].text;
			        				$("#schooId").val(items[1].value);
			        				//返回 false 可以阻止选择框的关闭
			        				//return false;
			        			});
			        		}, false);
			        		var muiPickerLh = $(".mui-poppicker").length;
			        		}
			        	});
			});
	}, false);
	
	/* 远景图片上传*/
	function uploadAllImages() {
		//alert("All*****function****localAlllArr.length："+localAlllArr.length);
        if (localAlllArr.length == 0) {
        	
        	var serverStr="";
        	if(0 == serverAlllArr.length ){
        		$("#addBusi").submit();
        	}else{
            for(var j=0;j<serverAlllArr.length;j++){
            	serverStr+=serverAlllArr[j]+";" ;
            }
            $.ajax({
        		type : "post",
        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
        		dataType : "json",
        		data : {serverId:serverStr},
        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
        			
        			//alert("+++++++++data++++++:"+data);	
        			var imageList = data;
        			//var pathList = data.
        			
        			var imgPath=$("#allPhoto_img").val();
        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
					for(var f=0;f<imageList.length;f++){
				//		alert("+++++++++++++++:"+imageList[f]);
						var imgPathTemp = imageList[f];
						imgPath += imgPathTemp+",";
					}
					$("#allPhoto_img").val(imgPath);
				//    alert("===========activity的imgUrl============"+$("#activityImage").val());
					//alert("All*****function****data"+imgPath);
        			$("#addBusi").submit();
        		}
        	});	
        }
        }
        var localId = localAlllArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 0, // 默认为1，显示进度提示
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
                serverAlllArr.push(serverId);
                localAlllArr.shift();
                uploadAllImages(localAlllArr);
                serverStr+=serverId+";";
               // alert("localArr的长度："+localArr.length);
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
                $(".webservice_mask").addClass("mask_none");
            }
        });
    }
	
	$(document).ready(function(){
		var h=$(document).height();
		$(".shade").css("height",h);
	});
	</script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/uploadImage.js"></script>
	<%--<script type="text/javascript" src="${path}/xkh_version_2.1/js/uploadImageEdit.js"></script>--%>	
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/validateMessage.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/updateImage.js"></script>
</body>
</html>
