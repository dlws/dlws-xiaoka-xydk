<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2/css/weui.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2/css/swiper-3.3.1.min.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css" />
		
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/mui.picker.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/mui.poppicker.css"/>
		<title>校园资源入驻</title>
	</head>
	<body>
		<c:if test="${not empty  skillBannerInfo }">
       		<div class="enterInfo_top">
			   <img src="${skillBannerInfo.picUrl }?${addskill_banner}" class="search_pic"  />
		    </div>
       	</c:if>
       	<c:if test="${empty skillBannerInfo}">
	       	<div class="enterInfo_top">
				<img src="${path}/xkh_version_2/img/user_banner.jpg?${addskill_banner}" class="search_pic"  />
			</div>
       	</c:if>
		<!--公有信息部分-->
	<form id="income" name="incomeform" action="${path}/skillUser/addSkillUserBaseInfo.html" method="post">
		<ul class="message_content">
			<li>
				<span class="enter_name">姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名</span>
				<input type="text" id="userName" name="userName" value="${basicInfo.userName}" placeholder="填写您的姓名" />
			</li>
			<li>
				<span class="enter_name">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span>
				<div id="activeCity" class="mui-input-row">
					<span class="myInfo_name enter_city result-tips">选择您所在的城市</span>
					<span class="myInfo_name show-result">${basicInfo.cityName}</span>
					<input type="hidden" name="cityId" id="cityId" value="${basicInfo.cityId}" >
					<span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
			</li>
			<li>
				<span class="enter_name">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;校</span>
				<div id="activeShcool" class="mui-input-row">
					<span class="myInfo_name enter_school result-tips">选择您所在的学校</span>
					<span class="myInfo_name show-result">${basicInfo.schoolName}</span>
					<input type="hidden" name="schoolId" id="schooId" value="${basicInfo.schoolId}">
				   <span class="iconfont icon-youjiantou enter_youjiantou"></span>
				</div>
				
			</li>
			<li>
				<span class="enter_name1">入驻类型</span>
				<div class="enter_choose">
					<div class="enter_left">
						<span class="iconfont icon-tuandui icon_color"></span>
						<span class="team">团队</span>
					</div>
					<span class="enter_line"></span>
					<div class="enter_right">
						<span class="iconfont icon-geren"></span>
						<span class="team">个人</span>
					</div>
				</div>
			</li>
			
			<li>
				<span  class="enter_name nickname">团队名称</span>
				<input type="text" id="nicknameT" name="nickname" value="${basicInfo.nickname}" placeholder="填写名称" />
			</li>
			<li>
				<span class="enter_name headPhoto">团队头像</span>
				<span class="enter_jia myInfo_photo">
				<c:choose>
					<c:when test="${basicInfo.headPortrait!=''&&basicInfo.headPortrait!=null}">
							 <img id="myAvatar" name="" src="${basicInfo.headPortrait}" class="search_pic" />
					 </c:when>
					 <c:otherwise>
					 <span id="addText">点击加号上传图片</span>
					  			<img id="myAvatar" name="" src="" class="search_pic" style="display: none;" />
					 </c:otherwise>
				</c:choose>
				</span>
				<span class="iconfont icon-jiahao enter_youjiantou icon_pic">
				</span>
			</li>
			
			<li>
				<span class="enter_name">联系电话</span>
				<input class="phone" id="phoneNumberT" name="phoneNumber" value="${basicInfo.phoneNumber}" type="number" placeholder="填写您的手机号码" />
				<p class="warn_tel"></p>
			</li>
			<li>
				<span class="enter_name">联系邮箱</span>
				<input class="letter" id="emailT" name="email" type="text" value="${basicInfo.email}" placeholder="填写您的联系邮箱" />
				<p class="warn_letter"></p>
			</li>
			<li>
				<span class="enter_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号</span>
				<input class="weixin" type="text" id="wxNumberT" name="wxNumber" value="${basicInfo.wxNumber}" placeholder="填写您的微信账号" />
				<p class="warn_weixin"></p>
			</li>
			<li>
				<span class="enter_nameT temPro">团队介绍</span>
				<div class="textarea">
					<textarea class="introduce" id="aboutMeT" name="aboutMe" onkeydown="showLen(this);" onkeyup="showLen(this);"  placeholder="请填写您的团队优势,案例介绍等">${basicInfo.aboutMe}</textarea>
					 <span id="span" class="span"></span>
				</div>
			</li>
			
			
		</ul>
		<div class="enter_box">
		<input type="hidden" id="id" name="id" value="${basicInfo.id}"/>
		<input type="hidden" id="checkType" name="checkType" value="${basicInfo.checkType}"/>
		<input type="hidden" name="headPortrait" id="headPortrait" value="${basicInfo.headPortrait}" >
		</div>
		<!--底部提交-->
		<!-- <div class="enter_form" onsubmit="return false"> -->
			<button type="button" class="btn w144" onclick="sub()">提交</button>
		<!-- </div> -->
	</form>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/swiper-3.3.1.jquery.min.js"></script>
	<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
	
	
	<script src="${path}/xkh_version_2/js/mui.min.js"></script>
	<script src="${path}/xkh_version_2/js/mui.picker.js"></script>
	<script src="${path}/xkh_version_2/js/mui.poppicker.js"></script>
	<script src="${path}/xkh_version_2/js/city.data.js"></script>
	<script src="${path}/xkh_version_2/js/shcool.data.js"></script>
	<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
	<script>
	//字数限制
	showLen(document.getElementById("aboutMeT"));
	function showLen(obj){
		   document.getElementById('span').innerHTML = ''+ (obj.value.length) +'/200';
		   
		  if(obj.value.length>200){
			  obj.value=obj.value.substring(0,200);
			  return false;
			  }
		} 
	//轮播部分
	var mySwiper = new Swiper('.swiper-container',{
		loop:true,
		autoplay:3000,
		autoplayDisableOnInteraction:false, 
		pagination:'.swiper-pagination',
		paginationClickable:true
	})
	
	
	
	$(function(){
		var cityId="${basicInfo.cityId}";
		//debugger;
		if(cityId!=""&&cityId){
		   	document.querySelector('#activeShcool .result-tips').style.display = "none";
			document.querySelector('#activeShcool .show-result').style.display = "block"; 
			
		 	document.querySelector('#activeCity .result-tips').style.display = "none";
			document.querySelector('#activeCity .show-result').style.display = "block";  
		}
		 
		
		var checkType = $("#checkType").val();
		if(checkType!=""&&checkType){
			if(checkType=='0'){
				//个人
				$(".enter_left span").removeClass("icon_color");
			 	$(".enter_right span").addClass("icon_color");
				$("#checkType").val(0);
				$(".nickname").html("昵 称");
				$(".headPhoto").html("个人头像");
				$(".temPro").html("个人介绍");
			}else if(checkType=='1'){
				//团队
				$(".enter_left span").addClass("icon_color");
			 	$(".enter_right span").removeClass("icon_color");
				$("#checkType").val(1);
				
			 	$(".nickname").html("团队名称");
				$(".temPro").html("团队介绍");
				$(".headPhoto").html("团队头像");
			}else{
				$("#checkType").val(1);
			}
		}
	});
	function sub(){
		var checkType = $("#checkType").val();
		var phone=$(".phone").val();//电话号码
		var letter=$(".letter").val();//邮箱
		//0 0是个人
		if(checkType==""||!checkType){
			$("#checkType").val(1);
		}
		//电话验证
		
   		var re=/^1[3|4|7|5|8]\d{9}$/;
   		if(phone==""||!phone){
   			$(".warn_tel").html("请输入手机号").css("display","block");
   			return;
   		}else if(phone!=""){
    		if(!re.test(phone)){
    			$(".warn_tel").html("输入手机格式有误，请重新输入").css("display","block")
    			return;
    		}else{
    			$(".warn_tel").html("").css("display","none")
    		}
   		}
   		
   		//邮箱验证
		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
		if(letter==""||!letter){
			$(".warn_letter").html("请输入邮箱号").css("display","block");
			return;
		}else if(letter!=""&&letter){
			if(!reg.test(letter)){
				$(".warn_letter").html("输入邮箱格式有误，请重新输入").css("display","block");	
				return;
			}else{
				$(".warn_letter").html("").css("display","none");
			}
		} 
		var schooId = $("#schooId").val();
		if(schooId==""||schooId==null){
			alert("请选择学校");
			return;
		} 
		var headPortrait = $("#myAvatar")[0].src;
		if(headPortrait==""||headPortrait==null){
			alert("请选择头像");
			return;
		}  
		
		var aboutMe = $("#aboutMeT").val();
		if(aboutMe==""||aboutMe==null){
			alert("请填写介绍");
			return;
		} 
		
		if(headFlag==true){
			$("#income").submit();
		}else{
			uploadImageSelf();
		}
		
	}
		//点击图像切换团队和个人
		var Index=0;
		 $(".enter_choose div").click(function(){
		     $(".enter_choose span").removeClass("icon_color");
			 $(">span:first-child",this).addClass("icon_color");
			 Index=$(this).index();
					if(Index==0){
						$("#checkType").val(1);
						$(".nickname").html("团队名称");
						$(".headPhoto").html("团队头像");
						$(".temPro").html("团队介绍");
					}else if(Index==2){
						$("#checkType").val(0);
						$(".nickname").html("昵 称");
						$(".headPhoto").html("个人头像");
						$(".temPro").html("个人介绍");
					}
				
		})
		 //点击加号上传头像图标变换部分
		 $(".icon_pic").click(function(){
			 uploadImage();
			 	/* if($('.icon_pic').hasClass('icon-jiahao')){
				$('.icon_pic').removeClass("icon-jiahao").addClass("icon-del")
			}else{
				 $('.icon_pic').removeClass("icon-del").addClass("icon-jiahao") 
			} */
		 })
		
        
      	//选择所在地
    	mui.init();
		var schoolData="";
    	var cityPicker = new mui.PopPicker({
					layer: 2
		});
    	var city = ${proList}
		cityPicker.setData(city);
		var showCityPickerButton = document.getElementById('activeCity');
		
		showCityPickerButton.addEventListener('tap', function(event) {
			cityPicker.show(function(items) {
				document.querySelector('#activeCity .result-tips').style.display = "none";
				document.querySelector('#activeCity .show-result').style.display = "block";
				document.querySelector('#activeCity .show-result').innerText = items[1].text;
				$("#cityId").val(items[1].value);
				
				document.querySelector('#activeShcool .result-tips').style.display = "block";
				document.querySelector('#activeShcool .show-result').style.display = "none";  
				//返回 false 可以阻止选择框的关闭
				//return false;
				$.ajax({
	        		type : "post",
	        		url : "<%=contextPath%>/skillUser/chooseSchool.html",
	        		dataType : "json",
	        		data : {cityId:items[1].value},
	        		success : function(data) {
	        			var shcoolPicker = new mui.PopPicker({
	        				layer:2
	        			});
	        			shcoolPicker.setData(data);
	        			var showShcoolPickerButton = document.getElementById('activeShcool');
	        			showShcoolPickerButton.addEventListener('tap', function(event){
	        			shcoolPicker.show(function(items) {
	        				document.querySelector('#activeShcool .result-tips').style.display = "none";
	        				document.querySelector('#activeShcool .show-result').style.display = "block";
	        				document.querySelector('#activeShcool .show-result').innerText = items[1].text;
	        				$("#schooId").val(items[1].value);
	        				//返回 false 可以阻止选择框的关闭
	        				//return false;
	        			});
	        		}, false);
	        		var muiPickerLh = $(".mui-poppicker").length;
	        		if(muiPickerLh>2){
	        		$(".mui-poppicker").eq(muiPickerLh-2).remove();
	        		}
	        		}
	        	});
			});
		}, false);
		
		//微信上传 全局变量
		var wxSelf;
		var localArr=new Array();
		var serverArr=new Array();
		
		var headFlag = true;
		
		/*001 选择图片*/
		function uploadImage() {
		    var clientUrl = window.location.href;
		    var reqPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : reqPath,
				dataType : 'json',
				data : {
					"clientUrl" : clientUrl
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					/* alert(return_date ); */
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : false, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'chooseImage','uploadImage','downloadImage']
					});
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						wxSelf=wx;
						wx.chooseImage({
						    count: 1, // 默认9
						    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						    success: function (res) {
						    	localArr= res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
						        if(localArr.length >0){
							        var myAvatar=localArr[0];
							        $("#myAvatar").attr("src",myAvatar);
				        			//var inputImgVal = '<img id="headPortrait" name="headPortrait" src="'+myAvatar+'" onclick="delImg();"/>';
							        //$(".myInfo_photo").append(inputImgVal);
							        $("#myAvatar").css('display','block');
							        headFlag=false;
							        $("#addText").html("");
							        
						        }
						    }
						});

					});
					wx.error(function(res) {
						alert(res.errMsg);
					});
				}
			});
		}
		
		
		/*002 点击提交后上传到微信*/
		function uploadImageSelf(){
			 //alert("pic count="+localArr.length); 
			 for(var i=0;i<localArr.length;i++){
				 wxSelf.uploadImage({
				        localId: localArr[i], // 需要上传的图片的本地ID，由chooseImage接口获得
				        isShowProgressTips: 1, // 默认为1，显示进度提示
				        success: function (res) {
				            var serverId = res.serverId; // 返回图片的服务器端ID
				            serverArr.push(serverId);
				            
				            if(i == localArr.length){
				            	 //alert("执行上传 count"+serverArr);  
				                var serverStr="";
				                if(serverArr[0].length >0 ){
				    	            serverStr=serverArr[0] ;
				    	           // alert(serverStr);
				    	            $.ajax({
				    		        		type : "post",
				    		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
				    		        		dataType : "json",
				    		        		data : {serverId:serverStr},
				    		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
				    		        			//alert("data="+data.path);
				    		        			$("#headPortrait").val(data.path);
				    		        			$("#income").submit();
				    		        		}
				    		        	});	
				                	
				                }else{
				                	alert("上传头像失败");
				                }
				                            	
				            }
				         }
				    }); 
			 }
			 //downloadImageSelf(serverArr);
		}
		
		
		/*003 从微信下载到自定义服务器*/
		<%-- function downloadImageSelf(serverArr){
			 //alert("执行上传 count"+serverArr);  
            var serverStr="";
            
            if(serverArr[0].length >0 ){
	            serverStr=serverArr[0] ;
	            alert(serverStr);
	            $.ajax({
		        		type : "post",
		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
		        		dataType : "json",
		        		data : {serverId:serverStr},
		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
		        			alert("data="+data.path);
		        			$("#headPortrait").val(data.path);
		        		}
		        	});	
            	
            }else{
            	alert("上传头像失败");
            }
            
			
			
		        	
		   } --%>
		//点击删除图片
	/* 	function delImg(id,imgId){
			if(confirm("确定删除?")){
				$("#headPortrait").remove();		
			}else{
				return ;
			}
			
		} */
		
		 //验证手机号码
		 //当文本框失去焦点的时候
		 $(".phone").blur(function(){
		 	var val1=$(".phone").val()
   			var re=/^1[3|5|8]\d{9}$/;
	   		if(!re.test(val1)){
	   			$(".warn_tel").html("输入手机格式有误，请重新输入").css("display","block")	
	   		}
	   		if(val1==""){
	   			$(".warn_tel").html("请输入手机号").css("display","block");
	   		}
		 })
       //当文本框重新获得焦点的时候
       $(".phone").focus(function(){
	       	var val1=$(".phone").val()
	   		var re=/^1[3|5|8]\d{9}$/;
	   		if(!re.test(val1)){
	   			$(".warn_tel").html("").css("display","none")
	   		}
	   		if(val1==""){
	   			$(".warn_tel").html("").css("display","none");
	   		}
   		
       })
		//验证邮箱
		//当文本框失去焦点的时候
		 $(".letter").blur(function(){
		 	var val1=$(".letter").val()
	   		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		if(!reg.test(val1)){
	   			$(".warn_letter").html("输入邮箱格式有误，请重新输入").css("display","block")
	   		}
	   		if(val1==""){
	   			$(".warn_letter").html("请输入邮箱号").css("display","block");
	   		}
		 })
       //当文本框重新获得焦点的时候
       $(".letter").focus(function(){
	       	var val1=$(".letter").val()
	   		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		if(!reg.test(val1)){
	   			$(".warn_letter").html("").css("display","none")
	   		}
	   		if(val1==""){
	   			$(".warn_letter").html("").css("display","none");
	   		}
       })
        $(".weixin").blur(function(){
		 	var val1=$(".weixin").val()
	   		var reg=/^[a-zA-Z\d_]{5,}$/;
	   		if(!reg.test(val1)){
	   			$(".warn_weixin").html("微信号码格式有误，请重新输入").css("display","block")
	   		}
	   		if(val1==""){
	   			$(".warn_weixin").html("请输入微信号").css("display","block");
	   		}
		 })
       //当文本框重新获得焦点的时候
       $(".weixin").focus(function(){
	       	var val1=$(".weixin").val()
	   		var reg=/^[a-zA-Z\d_]{5,}$/;
	   		if(!reg.test(val1)){
	   			$(".warn_weixin").html("").css("display","none")
	   		}
	   		if(val1==""){
	   			$(".warn_weixin").html("").css("display","none");
	   		}
       })
		
	</script>
</html>
