<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/swiper-3.3.1.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/dropload.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/iconfont/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.2/css/style.css" />
		<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js"></script>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/swiper-3.3.1.jquery.min.js"></script>
		<script type="text/javascript" src="${path}/xkh_version_2.1/js/dropload.min.js" ></script>
		<title>校咖汇</title>
	</head>
	<%@ include file="allshare.jsp"%>
	<body>
		<!--头部-->
		<header class="header-bar">
			<div class="status-bar">
				<div class="come-num-box">
					<span class="come-num">
						<b>
							<i>0034</i>
							<i>0111</i>
							<i>1237</i>
							<i>2123</i>
							<i>2456</i>
							<i>${allNum}</i>
						</b>
					</span>
					<span  class="come-font">个校园资源已入驻2.4</span>
				</div>
				<a href="<%=contextPath%>/home/getClassListSeach.html" class="header_search">
					<img class="search_pic" src="${path}/xkh_version_2.1/img/icon_search.png" />
				</a>
			</div>
			<!--点击搜索图标出现热门搜索-->
			<form id="tosearch" class="ctt_hot" method="post" action="${path}/home/getClassListSeach.html">
				<a href="javaScript:search()" class="hot_pic"><img class="search_pic" src="${path}/xkh_version_2/img/search.png" /></a>
				<input type="text" name="seachValue" id="searchValue" placeholder="搜索需要的资源类型" class="hot_inp" />
				<span class="iconfont "></span>
			</form>
			<span class="ctt_cancel">取消</span>
		</header>
		<!--头部搜索栏-->
		<div class="list_content">
			<span class="hot_word">搜索热词</span>
			<div class="search_list">
				<c:forEach items="${hotWords}" var ="hotWord" varStatus="status">
					<a href="${path}/home/getClassListSeach.html?seachValue=${hotWord.dicName }"><span>${hotWord.dicName }</span></a>
				</c:forEach>
			</div>
		</div>

		<!--轮播图部分-->
		<section>
			<div class="lunbo">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<c:forEach items="${bannerInfos}" var ="bannerInfo" varStatus="status">
							
								<div class="swiper-slide">
									<a href="${bannerInfo.redirectUrl }"> 
										<img src="${bannerInfo.picUrl }?${index_banner}" />
										<p>${bannerInfo.name}</p>
									</a> 
								</div>
							
					    </c:forEach>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			</div>
			<!--小标题部分-->
			<ul class="ctt_title" id = "gallery">
				
			</ul>
			<div class="ctt_bg"></div>
			<!--推荐服务-->
			<div class="ctt_resource">
				<span class="resource_pic"><img class="search_pic" src="${path}/xkh_version_2.1/img/resource.png" /></span>
				<span class="good_resource">推荐服务</span>
			</div>
			<div class="resource_content" id="promoteSkills">
			
			</div>
			<div class="ctt_bg"></div>
			<!--推荐资源-->
			<div class="ctt_resource">
				<span class="resource_pic"><img class="search_pic" src="${path}/xkh_version_2.1/img/call.png" /></span>
				<span class="good_resource">推荐资源</span>
			</div>
			<div class="recommend_content">
				<div class="lists">
				
		    	</div>
			<input type="hidden" id="page" name="page" value="1">
			</div>
		</section>
		<!--底部-->
		
		<%@ include file="foot.jsp"%>
	</body>
	<!-- 一级分类 -->
	<script id="skills" type="text/html">
			{{each classItem as value i }}
				<a href="${path}/home/getClassList.html?fid={{value.id}}">
				<li class="ctt_li">
					<span><img class="search_pic" src="{{value.imageURL}}?${index_skillClass}" /></span>
					<span>{{value.className}}</span>
				</li></a>
			{{/each}}
	</script>
	
	<script id="promote" type="text/html">
		{{each promoteSkills as value i }}
					<a href="javascript:getSkillInfo({{value.id}},'{{value.classValueF}}')" class="resource_detail">
					<div class="detail_content">
						<img class="search_pic" src="{{value.image}}" />

						<input type="hidden" class ="skills" name="skills" src="{{value.image}}">
						
						<div class="resource_marsk">
							<span class="resource_title">{{value.skillName}}</span>
							<div class="place_box">
								<span class="resource_zuobiao"><img src="${path}/xkh_version_2.1/img/place.png" class="search_pic" /></span>
								<span>{{value.cityName}}</span>
							</div>
						</div>
					</div>
					
					
					<div class="about_sell about_sellNew resource_sell">
					<span class="about_title">{{value.userName}}</span>
					<div class="diamond_box">
						<div class="diamond_new">
							<img src="${path}/xkh_version_2.1/img/icon_diamond.png" class="search_pic" />
						</div>
						<div class="num1"><span>{{value.priceStr}}</span>元/{{value.companyStr}}</div>
					</div>
                    <div class="content_p">
					<div class="p">{{value.skillDepict}}</div>
					<span class="index_watch class_up" style="display:none;"></span>
					</a>
                    
                   </div>
				</div>
			
		{{/each}}
	</script>
	<script>
		function search(){
			//window.location.href="${path}/home/getClassListSeach.html?seachValue="+$("#searchValue").val();
			 $("#tosearch").submit();
		}
		$(function(){
			
			//一级分类
			var skillClass=${skillClass };
			var ery = template('skills',skillClass);
			document.getElementById('gallery').innerHTML = ery;
			
			//推荐服务
			var promoteSkills=${promoteSkills };
			
			var promote = template('promote',promoteSkills);
			document.getElementById('promoteSkills').innerHTML = promote;
			//查看图片详情进行注释
			<%-- $('.detail_content').click(function(){
				var myArray=new Array();
				var obj_this = this.children[0].src;
				var obj_child = this.getElementsByTagName("input");
				for(var i=0;i<obj_child.length;i++){
					myArray[i] = obj_child[i].src;
				} 
				
				//获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
		        var clientUrl = window.location.href;
		        var reqPath='<%=contextPath%>/skillUser/getJSConfig.html';
				//请求后台，获取jssdk支付所需的参数
				$.ajax({
					type : 'post',
					url : reqPath,
					dataType : 'json',
					data : {
						"clientUrl" : clientUrl
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
					},
					cache : false,
					error : function() {
						alert("系统错误，请稍后重试");
						return false;
					},
					success : function(data) {
						//微信支付功能只有微信客户端版本大于等于5.0的才能调用
						var return_date = eval(data);
						
						if (parseInt(data[0].agent) < 5) {
							alert("您的微信版本低于5.0无法使用微信支付");
							return;
						}
						//JSSDK支付所需的配置参数，首先会检查signature是否合法。
						wx.config({
							debug : false, //开启debug模式，测试的时候会有alert提示
							appId : return_date[0].appId, //公众平台中-开发者中心-appid
							timestamp : return_date[0].config_timestamp, //时间戳
							nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
							signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
							jsApiList : [ 'previewImage' ]
						});

						//上方的config检测通过后，会执行ready方法
						wx.ready(function() {
							wx.previewImage({
							    current: obj_this, // 当前显示图片的http链接
							    urls: myArray // 需要预览的图片http链接列表
							});
						});
						wx.error(function(res) {
							//alert(res.errMsg);
						});
					}
				});         
				
			}); --%>
			
		});
		
		</script>
	
	
	<script>
		function search(){
			//window.location.href="${path}/home/getClassListSeach.html?seachValue="+$("#searchValue").val();
			 $("#tosearch").submit();
		}
		
		function getSkillInfo(id,fathValue){
			window.location.href="../skill_detail/getskill_detail.html?skillId="+id+"&&classValue="+fathValue;
		}
		
	
	    $("#page").val(1);
		//点击搜索图标出现隐藏部分
		/* $(".header_search").click(function() {
			$(".status-bar").hide();
			$("section").css("visibility", "hidden");
			$("nav").css("visibility", "hidden");
			$(".ctt_hot").css("display", "block");
			$(".ctt_cancel").css("display", "block");
			$(".list_content").css("display", "block");
		}) */
		
		/* $(".header_search").click(function(){
			$(".status-bar").hide();
			$("section").css("visibility", "hidden");
			$("nav").css("visibility", "hidden");
			$(".ctt_hot").css("display", "block");
			$(".ctt_cancel").css("display", "block");
			$(".list_content").css("display", "block");
		})  */
		$(".ctt_cancel").click(function() {
			$(".hot_inp").val("");
		});
		//轮播部分
		var mySwiper = new Swiper('.swiper-container', {
			loop: true,
			autoplay: 3000,
			autoplayDisableOnInteraction: false,
			pagination: '.swiper-pagination',
			paginationClickable: true
		});
		/* 获取header高度 */
		var h = $(".header-bar").height();
		var hh = $(".ctt_hot").outerHeight(true);
		console.log(h)
		console.log(hh)
		$(".lunbo").css("margin-top", h);
		$(".list_content").css("margin-top", hh);
		//下拉刷新
		
    var counter = 0;
    // 每页展示25个
    var num = 25;
    var pageStart = 0;
    var pageEnd = 0;
	var page = 0;
    // dropload
   /*  $('.recommend_content').dropload({
        scrollArea : window,
            domDown : {
            domClass   : 'dropload-down',
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
            domNoData  : '<div class="dropload-noData">没有更多内容</div>'
        },
        loadDownFn : function(me){
        	page=parseInt($("#page").val());
            $.ajax({
                type: 'GET',
                url: 'getUserBasicinfo.html',
                dataType: 'json',
                data:{currentPage:page},
                success: function(data){
                    var result = '';
                    for(var i = 0; i < data.list.length; i++){
                    	result +="<a href='${path}/comunityBigV/getPersonUrl.html?id="+data.list[i].id+"&enterId="+data.list[i].enterId+"' class='ctt_new'>"+
	                          '<div class="recommend">'+
										'<div class="recommend_left">';
										if(!data.list[i].headPortrait){
											result +='<img src="${path}/xkh_version_2.1/img/logoaaa.jpg" class="search_pic" />'	
										}else{
											result +='<img src="'+data.list[i].headPortrait+'" class="search_pic" />'		
										}
										result +='</div>'+
										  '<div class="recommend_right">'+
											'<div class="recommend_title1">'+
											'<span class="recommend_name">'+data.list[i].nickname+'</span>'+
										'<div class="recommend_place">'+
												'<span class="resource_zuobiao"><img src="${path}/xkh_version_2.1/img/place.png" class="search_pic" /></span>'+
												'<span>'+data.list[i].cityName+'</span>'+
											'</div>'+
											'</div>'+
											'<div class="recommend_title1">'+
											'<span class="recommend_name recommend_name1">'+data.list[i].schoolName+'</span>'+
											'<div class="recommend_place">'+
												'<span class="resource_zuobiao resource_z"><img src="${path}/xkh_version_2.1/img/flag.png" class="search_pic" /></span>'
												if(data.list[i].typeName){
													result +='<span class="resource_t">'+data.list[i].typeName+'</span>';
												}else{
												}
												result +='</div>'+
											'</div>'+
										'</div>'+ 
										
									'</div> </a>';
                    }
                    
                    if(data.list.length < num){
                        // 锁定
                        me.lock();
                        // 无数据
                        me.noData();
                    }
               	 
               	    if(data.list.length == num){
               		  $("#page").val(page+1);
                     }
                    
                    // 为了测试，延迟1秒加载
                    setTimeout(function(){
                    	 $('.lists').append(result);
                    	 // 每次数据加载完，必须重置
                         me.resetload();
                    },1000); 
                },
                error: function(xhr, type){
                    alert('Ajax error!');
                    // 即使加载出错，也得重置
                    me.resetload();
                }
            });
        }
     }); 
 */

	$('.recommend_content').dropload({
	 scrollArea : window,
	     domDown : {
	     domClass   : 'dropload-down',
	     domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
	     domNoData  : '<div class="dropload-noData">没有更多内容</div>'
	 },
	 loadDownFn : function(me){
	 	page=parseInt($("#page").val());
	     $.ajax({
	         type: 'GET',
	         url: 'getUserBasicinfo.html',
	         dataType: 'json',
	         data:{currentPage:page},
	         success: function(data){
	             var result = '';
	             for(var i = 0; i < data.list.length; i++){
	             	result +="<a href='${path}/personInfoDetail/detail.html?id="+data.list[i].id+"' class='ctt_new'>"+
				             	'<div class="recommend">'+
								'<div class="recommend_left">';
									if(!data.list[i].headPortrait){
										result +='<img src="${path}/xkh_version_2.1/img/logoaaa.jpg" class="search_pic" />'	
									}else{
										result +='<img src="'+data.list[i].headPortrait+'" class="search_pic" />'		
									}
								result +='</div>'+
								'<div class="recommend_right">'+
									'<div class="recommend_title1">'+
									/* '<span class="recommend_name"></span>'+
									'<span class="recommend_line">-</span>'+ */
									'<span class="recommend_name2">'+data.list[i].schoolName+'-'+data.list[i].nickname+'</span>'+
									'</div>'+
									'<p class="recommend_p">'+data.list[i].aboutMe+'</p>'+
								'</div>'+
							   '</div>'+
							'</a>';
	             }
	             
	             if(data.list.length < num){
	                 // 锁定
	                 me.lock();
	                 // 无数据
	                 me.noData();
	             }
	        	 
	        	 if(data.list.length == num){
	        		  $("#page").val(page+1);
	              }
	             
	             // 为了测试，延迟1秒加载
	             setTimeout(function(){
	             	 $('.lists').append(result);
	             	 // 每次数据加载完，必须重置
	                  me.resetload();
	             },1000); 
	         },
	         error: function(xhr, type){
	             alert('Ajax error!');
	             // 即使加载出错，也得重置
	             me.resetload();
	         }
	     });
	 }
	}); 

	//消息在第一序列
	var index = 0;
	var path = "${path}";
	
  //控制字数显示的
	/* $(document).ready(function(){
                 function show(){
					var btn = document.getElementsByClassName("index_watch");
					var p = document.getElementsByClassName("p");
					
					var arr=[];
					for(var i=0;i<btn.length;i++){
						(function(i) {
							var text = p[i].innerHTML;
							arr.push(text)
							var length=$(".p").eq(i).html().length;
							var html=$(".p").eq(i).html();
							if(length>45){
								 $(".p").eq(i).html(html.substring(0,45)+"...")
							}
						btn[i].onclick = function(){
								var newBox = document.createElement("div");
							 $(this).toggleClass("class_up").toggleClass("class_down")
							newBox.innerHTML=arr[i];
							if($(this).hasClass("class_down")) {
								newBox.innerHTML = arr[i];
							} else{
								if($(".p").eq(i).html().length>45){
									newBox.innerHTML = arr[i].substring(0, 45) + "...";
								}else{
									newBox.innerHTML = arr[i];
								}								
							}
						 	  p[i].innerHTML = ""; 
							 p[i].appendChild(newBox); 
							
						} 
						
						})(i)
					}
				}
				show();
			}) */
			
			
	
	</script>
<%-- <script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
	</script> --%>
	<%-- <script src="${path}/xkh_version_2.1/js/share.js" ></script>  --%>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/foot.js" ></script>
</html>
