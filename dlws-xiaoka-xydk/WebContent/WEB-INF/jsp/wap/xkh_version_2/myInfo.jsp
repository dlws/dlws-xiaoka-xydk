<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<%@ include file="image_data.jsp"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2/css/swiper-3.3.1.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css" />
		<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<title>我的信息</title>
	</head>
	<style>
	 nav{
        position:fixed;
        left:0;
        bottom:0;
        
     }
     .enter_box{
       margin-bottom:50px;
     }
     </style>
	<body>
		<!--轮播图-->
		<%-- <div class="lunbo personal_lunbo">
				<div class="swiper-container personal_swiper">
		            <div class="swiper-wrapper">
		             <c:forEach items="${userBannerInfo}" var="userBannerInfo">
				    	<div class="swiper-slide"><img src="${userBannerInfo.picUrl }?${addskill_banner}"/></div>
				    </c:forEach> 
		   			</div>
      		   </div>
       </div> --%>
		<c:if test="${not empty  skillBannerInfo }">
       		<div class="enterInfo_top">
			   <img src="${skillBannerInfo.picUrl }?${addskill_banner}" class="search_pic"  />
		    </div>
       	</c:if>
       	<c:if test="${empty skillBannerInfo}">
	       	<div class="enterInfo_top">
				<img src="${path}/xkh_version_2/img/user_banner.jpg?${addskill_banner}" class="search_pic"  />
			</div>
       	</c:if>
		<!--公有信息部分-->
		<ul class="message_content">
			<li>
				<span class="enter_name">姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名</span>
				<%-- <input type="text" placeholder="请输入姓名" value="${basicInfo.userName}" disabled="disabled"/> --%>
				<span class="myInfo_name">${basicInfo.userName}</span>
			</li>
			<li>
				<span class="enter_name">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span>
				<span class="myInfo_name">${basicInfo.cityName}</span>
				<span class="iconfont icon-youjiantou enter_youjiantou"></span>
			</li>
			<li>
				<span class="enter_name">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;校</span>
				<span class="myInfo_name">${basicInfo.schoolName}</span>
				<span class="iconfont icon-youjiantou enter_youjiantou"></span>
			</li>
			<li>
				<span class="enter_name1">入驻类型</span>
				<div class="enter_choose">
					<div class="enter_left">
						<span class="tuandui"></span>
						<span class="team">团体</span>
					</div>
					<span class="enter_line"></span>
					<div class="enter_right">
						<span class="geren"></span>
						<span class="team">个人</span>
					</div>
				</div>
			</li>
		</ul>
		<div class="enter_box">
		<!--团队信息部分-->
		<ul class="team_content message_content">
			<li>
				<span class="enter_name" disabled="disabled">
					<c:if test="${basicInfo.checkType==0}">昵称</c:if>
					<c:if test="${basicInfo.checkType==1}">团队名称</c:if>
				</span>
				<%-- <input type="text" placeholder="请填写团队名称" value="${basicInfo.nickname}" disabled="disabled"/> --%>
				<span class="myInfo_name">${basicInfo.nickname}</span>
			</li>
			<li>
				<span class="enter_name">
					<c:if test="${basicInfo.checkType==0}">个人头像</c:if>
					<c:if test="${basicInfo.checkType==1}">团队头像</c:if></span>
				<span class="enter_jia myInfo_photo"><img src="${basicInfo.headPortrait}" class="search_pic" /></span>
				<!-- <span class="iconfont icon-del enter_youjiantou icon_pic"></span> -->
			</li>
			<li>
				<span class="enter_name">联系电话</span>
				<%-- <input class="phone" type="number" placeholder="请填写联系电话" value="${basicInfo.phoneNumber}" disabled="disabled"/> --%>
				<span class="myInfo_name">${basicInfo.phoneNumber}</span>
				<p class="warn_tel"></p>		
			</li>
			<li>
				<span class="enter_name">联系邮箱</span>
				<%-- <input class="letter" type="text" placeholder="请填写电子邮箱" value="${basicInfo.email}" disabled="disabled"/> --%>
				<span class="myInfo_name">${basicInfo.email}</span>
				<p class="warn_letter"></p>	
			</li>
			<li>
				<span class="enter_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号</span>
					<%-- <input type="text" placeholder="请输入微信号" value="${basicInfo.wxNumber}" disabled="disabled"/> --%>
			        <span class="myInfo_name">${basicInfo.wxNumber}</span>
			</li>
			<li>
				<span class="enter_nameT">
					<c:if test="${basicInfo.checkType==0}">个人介绍</c:if>
					<c:if test="${basicInfo.checkType==1}">团队介绍</c:if></span>
					
				<div class="textarea">
					<textarea class="introduce" id="skillDepict" onkeydown="showLen(this);" onkeyup="showLen(this);" placeholder="请填写团队介绍" value="" disabled="disabled">${basicInfo.aboutMe}</textarea>
				 	<span id="span" class="span"></span>
				</div>
			</li>
		</ul>
		<%-- <!--个人信息页面-->
		<ul class="geren_content message_content">
			<li>
				<span class="enter_name">昵&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;称</span>
				<input type="text" placeholder="请填写昵称" value="${basicInfo.nickname}"/>
				<!--<span class="myInfo_name">校咖</span>-->
			</li>
			<li>
				<span class="enter_name">个人头像</span>
					<span class="enter_jia myInfo_photo"><img src="${path}/xkh_version_2/img/per_banner01.jpg" class="search_pic" /></span>
				<span class="iconfont icon-jiahao enter_youjiantou icon_pic"></span>
			</li>
			<li>
				<span class="enter_name">联系电话</span>
				<input class="phone" type="text" placeholder="请填写联系电话" value="${basicInfo.phoneNumber}"/>
				<p class="warn_tel"></p>		
			</li>
			<li>
				<span class="enter_name">联系邮箱</span>
				<input class="letter" type="text" placeholder="请填写电子邮箱" value="${basicInfo.email}"/>
				<p class="warn_letter"></p>		
			</li>
			<li>
				<span class="enter_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号</span>
				<input type="text" placeholder="请输入微信号" value="${basicInfo.wxNumber}"/>
			</li>
			<li>
				<span class="enter_nameT">个人介绍</span>
				<textarea class="introduce" placeholder="请填写个人介绍">${basicInfo.aboutMe}</textarea>
			</li>
		</ul> --%>
		</div>
		<!--底部提交-->
		<!-- <form class="enter_form" onsubmit="return false">
			<button>保存</button>
		</form> -->
		<!--底部条-->
		<nav class="foot-bar-tab">
			<a class="foot-tab-item" href="${path }/home/index.html">
				<span class="foot-icon foot-icon-home"></span>
				<span class="foot-tab-label">首页</span>
			</a>
			<a class="foot-tab-item" href="${path}/prompt/prompt.html">
				<span class="foot-icon foot-icon-pub"></span>
				<span class="foot-tab-label">发布</span>
			</a>
			<a class="foot-tab-item" href="${path}/prompt/prompt.html">
				<span class="foot-icon foot-icon-bus"></span>
				<span class="foot-tab-label">已选</span>
			</a>
			<a class="foot-tab-item foot-active" href="${path }/home/getmyInform.html">
				<span class="foot-icon foot-icon-person"></span>
				<span class="foot-tab-label">我的</span>
			</a>
		</nav>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2/js/swiper-3.3.1.jquery.min.js"></script>
	
	<script>
		function loading(){
		var baseP = "<%=basePath%>";
		var title=document.title;
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
	</script>
	<script>
		//字数限制
		showLen(document.getElementById("skillDepict"));
		function showLen(obj){
			 if(obj.value.length > 200){
				 $('#skillDepict').attr('disabled',true);
			}else{
		    	document.getElementById('span').innerHTML = ''+ (obj.value.length) +'/200';
			} 
		} 
	
		// 亮 iconfont icon-tuandui icon_color  暗
		window.onload=function(){
			loading();
			//服务
			var checkType=${basicInfo.checkType};
			if(checkType==1){
				//tuandui iconfont icon-tuandui icon_color---iconfont icon-geren
				$(".tuandui").toggleClass("iconfont icon-tuandui icon_color");
    			$(".geren").toggleClass("iconfont icon-geren")
			}
			if(checkType==0){
				//iconfont icon-tuandui--iconfont icon-geren icon_color
				$(".tuandui").toggleClass("iconfont icon-tuandui");
    			$(".geren").toggleClass("iconfont icon-geren icon_color")
			}
		}
			
		//轮播部分
		var mySwiper = new Swiper('.swiper-container',{
			loop:true,
			autoplay:3000,
			autoplayDisableOnInteraction:false, 
			pagination:'.swiper-pagination',
			paginationClickable:true
		})
	
		
		/* //点击图像切换团队和个人
		var Index=0;
		$(".enter_choose div").click(function(){
		     $(".enter_choose span").removeClass("icon_color");
			 $(">span:first-child",this).addClass("icon_color");
			 Index=$(this).index();
//				console.log(Index)
				if(Index==0){
					$(".enter_box ul").eq(0).show().siblings().hide();
				}else if(Index==2){
					$(".enter_box ul").eq(1).show().siblings().hide();
				}
		})
		 //验证手机号码
		 //当文本框失去焦点的时候
		 $(".phone").blur(function(){
		 	var val1=$(".phone").val()
    		var re=/^1[3|4|7|5|8]\d{9}$/;
    		if(!re.test(val1)){
    			$(".warn_tel").html("输入手机格式有误，请重新输入").css("display","block")
    			
    		}
    		
    		if(val1==""){
    			$(".warn_tel").html("请输入手机号").css("display","block");
    		}
    		
		 })
        //当文本框重新获得焦点的时候
        $(".phone").focus(function(){
        	var val1=$(".phone").val()
    		var re=/^1[3|5|8]\d{9}$/;
    		if(!re.test(val1)){
    			$(".warn_tel").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_tel").html("").css("display","none");
    		}
        })
		//验证邮箱
		//当文本框失去焦点的时候
		 $(".letter").blur(function(){
		 	var val1=$(".letter").val()
    		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
    		if(!reg.test(val1)){
    			$(".warn_letter").html("输入邮箱格式有误，请重新输入").css("display","block")
    			
    		}
    		
    		if(val1==""){
    			$(".warn_letter").html("请输入邮箱号").css("display","block");
    		}
    		
		 })
        //当文本框重新获得焦点的时候
        $(".letter").focus(function(){
        	var val1=$(".letter").val()
    		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
    		if(!reg.test(val1)){
    			$(".warn_letter").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_letter").html("").css("display","none");
    		}
    		}) */
	</script>
</html>

