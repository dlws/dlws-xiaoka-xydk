<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ include file="image_data.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" href="${path}/xkh_version_2/iconfont/iconfont.css" />
	    <link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css"/>
	    <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<title>校咖汇</title>
	</head>
	<body>
		<form method="post" id="form" action="${path}/home/updatePersonInform.html">
			<header style="min-height:36vw;">
				<img src="${path}/xkh_version_2.2/img/editor_banner.png" class="search_pic"  />
			</header>
			<ul class="team_content message_content">
			<li>
				<span class="enter_name">您的姓名</span>
				<input class="name" type="text" id="name" name="name" value="${map.name}" placeholder="填写您的姓名" />
				<p class="warn_name"></p>
			</li>
			<li>
				<span class="enter_name">您的电话</span>
				<input class="phone" id="phoneNumber" name="phoneNumber" value="${map.phoneNumber}" type="number" placeholder="填写您的电话" />
				<p class="warn_tel"></p>
			</li>
			<li>
				<span class="enter_name">您的邮箱</span>
				<input class="letter" id="email" name="email" value="${map.email}" type="text" placeholder="填写您的邮箱" />
				<p class="warn_letter"></p>
			</li>
		</ul>
		</form>
		<!--底部确认-->
		<div   class="enter_form editor_bottom"  method="post">
			<button id="submite">确认</button>
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2/js/jquery-2.1.0.js" ></script>
	<script>
		var baseP = "<%=basePath%>";
		var title=document.title;
		window.onload=function(){
			 //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
	        var client = window.location.href;
	        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : rPath,
				dataType : 'json',
				data : {
					"clientUrl" : client
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : !true, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
					});
	
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						
						wx.onMenuShareAppMessage({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
						wx.onMenuShareTimeline({
						    title: title, // 分享标题
						    desc: "校咖汇,校园资源共享平台!", // 分享描述
						    link: client, // 分享链接
						    imgUrl: baseP+'wapstyle/img/logosss.png', // 分享图标
						    type: '', // 分享类型,music、video或link，不填默认为link
						    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
						    success: function () { 
						        // 用户确认分享后执行的回调函数
						    },
						    cancel: function () { 
						        // 用户取消分享后执行的回调函数
						    }
						});
	
	
					});
					wx.error(function(res) {
						//alert(res.errMsg);
					});
				}
			});
			
		};
		
	</script>
	<script>
		var flageName = false;
		var flagePhone = false;
		var flageEmail = false;
		//当文本框失去焦点的时候
		  //当文本框失去焦点的时候
		 $(".name").blur(function(){
		 	var val1=$(".name").val()
    		if(val1==""){
    			$(".warn_name").html("请输入姓名").css("display","block");
    		}
    		
		 })
        //当文本框重新获得焦点的时候
        $(".name").focus(function(){
        	var val1=$(".name").val()
    		if(val1==""){
    			$(".warn_name").html("").css("display","none");
    		}
    		
        })
		 //验证手机号码
		 //当文本框失去焦点的时候
		 $(".phone").blur(function(){
		 	var val1=$(".phone").val()
    		var re=/^1[3|5|8]\d{9}$/;
    		if(!re.test(val1)){
    			$(".warn_tel").html("输入手机格式有误，请重新输入").css("display","block")	
    		}
    		if(val1==""){
    			$(".warn_tel").html("请输入手机号").css("display","block");
    		}
		 })
        //当文本框重新获得焦点的时候
        $(".phone").focus(function(){
        	var val1=$(".phone").val()
    		var re=/^1[3|5|8]\d{9}$/;
    		if(!re.test(val1)){
    			$(".warn_tel").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_tel").html("").css("display","none");
    		}
    		
        })
		//验证邮箱
		//当文本框失去焦点的时候
		 $(".letter").blur(function(){
		 	var val1=$(".letter").val()
    		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
    		if(!reg.test(val1)){
    			$(".warn_letter").html("输入邮箱格式有误，请重新输入").css("display","block")
    		}
    		
    		if(val1==""){
    			$(".warn_letter").html("请输入邮箱号").css("display","block");
    		}
		 })
        //当文本框重新获得焦点的时候
        $(".letter").focus(function(){
        	var val1=$(".letter").val()
    		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
    		if(!reg.test(val1)){
    			$(".warn_letter").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_letter").html("").css("display","none");
    		}
        })
        //提交所有参数
        $("#submite").click(function(){
        	
        	if($("#email").val()!=""&&$("#phoneNumber").val()!=""&&$("#letter").val()!=""){
        		$("#form").submit();
        	}else{
        		alert("请完整填写以上信息");
        	}
        	
        });
		
	</script>
</html>
