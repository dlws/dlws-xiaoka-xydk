<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.min.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/style.css"/>
		<title>校咖汇</title>
	</head>
	<body id="allModel">
		<script src="${path}/wapstyle/js/mui.min.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<script src="${path}/wapstyle/js/personal_details.html.js"></script>
		<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
		<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<!-- <script id="modelContent" type="text/html"> -->
			<nav class="foot-bar-tab">
				<a class="foot-tab-item" href="http://p.qiao.baidu.com/cps/chat?siteId=9581947">
					<span class="foot-icon foot-icon-advice"></span>
					<span class="foot-tab-label">在线咨询</span>
				</a>
				<a class="foot-tab-item" href="${path}/sub/toSub.html?id=${skillInfo.id}">
					<span class="foot-icon foot-icon-tel"></span>
					<span class="foot-tab-label">联系TA</span>
				</a>
			</nav><!--底部条-->
			<div class="mui-content mui-scroll-wrapper">
				<div class="mui-scroll">
					<div id="slider" class="mui-slider">
						
					</div><!--焦点图-->
					<figure class="base-info" onclick="sub(${skillInfo.basicId })">
						<img src="${skillInfo.headImgUrl }" />
						<figcaption>
							<h1>${skillInfo.userName }</h1>
							<p>${skillInfo.schoolName }</p>
						</figcaption>
						<span class="mui-icon mui-icon-location-filled">${skillInfo.cityName }</span>
					</figure><!--基本信息-->
					
					<ul class="mui-table-view serve-list">
						<c:if test="${skillInfo.serviceType == 1 }">
							<li class="mui-table-view-cell">
					        	<img src="${path}/wapstyle/img/icon_diamond.png" />
					        	服务类型：线上
					        </li>
						</c:if>
						<c:if test="${skillInfo.serviceType != 1 }">
							<li class="mui-table-view-cell">
					        	<img src="${path}/wapstyle/img/icon_diamond.png" />
					        	服务类型：线下
					        </li>
						</c:if>
				        
				        <li class="mui-table-view-cell">
				        	<img src="${path}/wapstyle/img/icon_type.png" />
				        	技能价格：${skillInfo.skillPrice }元
				        </li>
				    </ul><!--服务说明列表-->
			    	<div class="opus-box" style ="display: block;">
				    	<h2 onclick="sub(${skillInfo.basicId })">${skillInfo.className }</h2>
				    	
				    	<div class="opus-content">
				    		<div id="slidervideo" class="mui-slider">
							</div><!--视频列表-->
				    		<h1>${skillInfo.skillName }</h1>
					    	<p>${skillInfo.skillDepict }</p>
				    		<!-- <span class="mui-icon mui-icon-eye">{{opus[i].num}}</span> -->
				    	</div>
				    	
				    	<div class="other-opus">
				    	<c:if test="${not empty skillInfo.opus}">
				    		<h1>其他作品</h1>
				    		<ul class="mui-table-view">
					    		<c:forEach var="opus" items="${skillInfo.opus}" varStatus="o">
				    		        <li class="mui-table-view-cell">
				    		            <a href="${opus.otherUrl }">
				    		                <img src="${path}/wapstyle/img/icon_opus.png"/>
				    		               	 作品${opus.opusName}
				    		            </a>
				    		        </li>
				    		     </c:forEach>
			    		    </ul>
			    		 </c:if>
				    	</div>
				    </div><!--个人作品-->
				</div>
			</div>
		<!-- </script> -->
		<script id="sliderDataVideo" type="text/html">
				<div class="mui-slider-group mui-slider-loop">
									<div class="mui-slider-item mui-slider-item-duplicate">
										<a href="{{v_lastPic.videoUrl}}">
											<img src="{{v_lastPic.imgUrl}}" style="height:150px;"/>
										</a>
									</div><!--最后一张图利-->
						    		{{each v_list as value i}}
						    			<div class="mui-slider-item">
											<a href="{{v_list[i].videoUrl}}">
												<img src="{{v_list[i].imgUrl}}" style="height:150px;"/>
											</a>
										</div>
						    		{{/each}}
						    		<div class="mui-slider-item mui-slider-item-duplicate">
										<a href="{{v_firstPic.videoUrl}}">
											<img src="{{v_firstPic.imgUrl}}" style="height:150px;"/>
										</a>
									</div><!--第一张图片-->
								</div>
								<div class="mui-slider-indicator">
									{{each v_list as value i}} {{if i==0}}
										<div class='mui-indicator mui-active'></div>
									{{/if}} {{if i>0}}
										<div class='mui-indicator'></div>
									{{/if}}{{/each}}
								</div>

		</script>
		<script id="sliderData" type="text/html">
			<div class="mui-slider-group mui-slider-loop">
							<div class="mui-slider-item mui-slider-item-duplicate">
								<a href="">
									<img src="{{lastPic.imgUrl}}" style="height:200px;"/>
								</a>
							</div><!--最后一张图利-->
				    		{{each list as value i }}
				    			<div class="mui-slider-item">
									<a href="">
										<img src="{{list[i].imgUrl}}" style="height:200px;"/>
									</a>
								</div>
				    		{{/each}}
				    		<div class="mui-slider-item mui-slider-item-duplicate">
								<a href="">
									<img src="{{firstPic.imgUrl}}" style="height:200px;"/>
								</a>
							</div><!--第一张图片-->
						</div>
						<div class="mui-slider-indicator">
							{{each list as value i}} {{if i==0}}
								<div class='mui-indicator mui-active'></div>
							{{/if}} {{if i>0}}
								<div class='mui-indicator'></div>
							{{/if}}{{/each}}
						</div>
		</script>
		<script>
			//判断个人信息状态
			function sub(basicId){
				window.location.href="${path}/home/getUserInfo.html?id="+basicId;
			}
			(function($){
				mui.init();
				//页面数据
				var bannerInfo=${bannerInfo};
				
				var flag = ${skillInfo.flag};
				var data = template('sliderData',bannerInfo);
				document.getElementById('slider').innerHTML = data;
				
				if(flag == 1){
					var v_imgs=${v_imgs};
					var dataVideo = template('sliderDataVideo',v_imgs);
					document.getElementById('slidervideo').innerHTML = dataVideo;
				}
				
				//禁用滚动条
				document.getElementsByTagName('body')[0].addEventListener('touchmove', function (e) {
				  e.preventDefault();
				});
				
				//自定义滚动条
				var deceleration = mui.os.ios ? 0.003 : 0.0009;
				$('.mui-scroll-wrapper').scroll({
					bounce: true,
					indicators: true, //是否显示滚动条
					deceleration: deceleration
				});
				
				//焦点图
				var slider = mui("#slider");
				slider.slider({
					interval: 3000
				});
				
			})(mui);
		</script>
		<script>
		    var baseP = "<%=basePath%>";
		    function toShare() {
		        //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
		        var client = window.location.href;
		        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
				//请求后台，获取jssdk支付所需的参数
				$.ajax({
					type : 'post',
					url : rPath,
					dataType : 'json',
					data : {
						"clientUrl" : client
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
					},
					cache : false,
					error : function() {
						alert("系统错误，请稍后重试");
						return false;
					},
					success : function(data) {
						//微信支付功能只有微信客户端版本大于等于5.0的才能调用
						var return_date = eval(data);
						if (parseInt(data[0].agent) < 5) {
							alert("您的微信版本低于5.0无法使用微信支付");
							return;
						}
						//JSSDK支付所需的配置参数，首先会检查signature是否合法。
						wx.config({
							debug : !true, //开启debug模式，测试的时候会有alert提示
							appId : return_date[0].appId, //公众平台中-开发者中心-appid
							timestamp : return_date[0].config_timestamp, //时间戳
							nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
							signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
							jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
						});
		
						//上方的config检测通过后，会执行ready方法
						wx.ready(function() {
							wx.onMenuShareAppMessage({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
							wx.onMenuShareTimeline({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
		
		
						});
						wx.error(function(res) {
							//alert(res.errMsg);
						});
					}
				});
			}
		    window.onload = toShare();
		</script>
	</body>
</html>
