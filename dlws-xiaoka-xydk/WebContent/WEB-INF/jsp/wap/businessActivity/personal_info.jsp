<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.min.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/style.css"/>
	    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	    <script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	    
		<title>校咖汇</title>
	</head>
	<body id="allModel">
		<script src="${path}/wapstyle/js/mui.min.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<script src="${path}/wapstyle/js/personal_info.html.js"></script>
			<nav class="foot-bar-tab">
				<a class="foot-tab-item" href="http://p.qiao.baidu.com/cps/chat?siteId=9581947">
					<span class="foot-icon foot-icon-advice"></span>
					<span class="foot-tab-label">在线咨询</span>
				</a>
				<a class="foot-tab-item" href="${path}/sub/toSub.html?id=${skillId}">
					<span class="foot-icon foot-icon-tel"></span>
					<span class="foot-tab-label">联系TA</span>
				</a>
			</nav><!--底部条-->
			<div class="mui-content mui-scroll-wrapper per-content">
				<div class="mui-scroll">
				    <header class="person-header">
				    	<figure>
				    		<img src="${userInfo.headImgUrl }" />
				    		<figcaption>
				    			<h1>${userInfo.schoolName }</h1>
				    			<p>${userInfo.userName }</p>
				    		</figcaption>
				    	</figure>
				    	<div class="site">
				    		<p class="location">
				    			<span class="mui-icon mui-icon-location-filled"></span>
				    			${userInfo.cityName }
				    		</p>
				    		<p class="num">
				    		<span class="mui-icon mui-icon-eye"></span>
				    			${userInfo.viewNum }
				    		</p>
				    	</div>
				    </header><!--头部-->
				    <div id = "skillInfo">
					    <div class="class-module">
							<h1>
								<span class="class-module-intro">关于TA</span>
								<div class="line"></div>
							</h1><!--标题-->
							<div class="per-intro">
								<p>${userInfo.aboutMe }</p>
							</div>
						</div><!--关于ta-->
						<div class="class-module">
							<h1>
								<span class="class-module-skill">TA的技能</span>
								<div class="line"></div>
							</h1><!--标题-->
							<div class="skill-list">
									<c:forEach var="skillInfo" items="${userInfo.skillInfos}">
									<div class="skill-list-item" >
										<h2><a href="${path}/home/getSkillInfo.html?id=${skillInfo.id}">${skillInfo.className }</a></h2>
										<div id="sliderSegmentedControl" class="mui-scroll-wrapper mui-slider-indicator mui-segmented-control mui-segmented-control-inverted">
											<div class="mui-scroll">
												<c:forEach var="image" items="${skillInfo.images}">
													<span class="mui-control-item">
														<img src="${image.imgUrl }"  />
													</span>
												</c:forEach> 
											</div>
										</div><!--列表-->
										<div class="skill-intro" onclick = "sub('${skillInfo.id}')">
											<header>
												<a href="${path}/home/getSkillInfo.html?id=${skillInfo.id}"><h4>${skillInfo.skillName }</h4></a>
												<p><span class="icon-price"></span>技能价格：${skillInfo.skillPrice }元</p>
											</header>
											<p class="skill-intro-body">${skillInfo.skillDepict }</p>
											<span class="mui-icon mui-icon-eye">${skillInfo.viewNum }</span>
										</div>
										
									</div>
									</c:forEach>
							</div>
						</div><!--TA的技能-->
				</div>
			</div><!--主体内容-->
		<script>
			function sub(id){
				window.location.href="${path}/home/getSkillInfo.html?id="+id;
			}
			(function($){
				mui.init();
				//禁用滚动条
				document.getElementsByTagName('body')[0].addEventListener('touchmove', function (e) {
				  e.preventDefault();
				});
				
				//自定义滚动条
				var deceleration = mui.os.ios ? 0.003 : 0.0009;
				$('.mui-scroll-wrapper').scroll({
					bounce: true,
					indicators: true, //是否显示滚动条
					deceleration: deceleration
				});
				
				//视频轮播
				var slider = mui("#slider");
				slider.slider({
					interval: 3000
				});
				 var basePath = "<%=contextPath%>";
				mui('#sliderSegmentedControl').on('tap','.mui-control-item',function(){
					
					var myArray=new Array();
					var obj_this = this.children[0].src;
					//console.log(obj_this)
					var obj_parent = this.parentNode;
					var obj_child =obj_parent.getElementsByTagName("img");
					for(var i=0;i<obj_child.length;i++){
						//console.log(obj_child[i].src );	
						myArray[i]=obj_child[i].src;
					}
					
					//获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
			        var clientUrl = window.location.href;
			        var reqPath='<%=contextPath%>/skillUser/getJSConfig.html';
					//请求后台，获取jssdk支付所需的参数
					$.ajax({
						type : 'post',
						url : reqPath,
						dataType : 'json',
						data : {
							"clientUrl" : clientUrl
						//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
						},
						cache : false,
						error : function() {
							alert("系统错误，请稍后重试");
							return false;
						},
						success : function(data) {
							//微信支付功能只有微信客户端版本大于等于5.0的才能调用
							var return_date = eval(data);
							if (parseInt(data[0].agent) < 5) {
								alert("您的微信版本低于5.0无法使用微信支付");
								return;
							}
							//JSSDK支付所需的配置参数，首先会检查signature是否合法。
							wx.config({
								debug : false, //开启debug模式，测试的时候会有alert提示
								appId : return_date[0].appId, //公众平台中-开发者中心-appid
								timestamp : return_date[0].config_timestamp, //时间戳
								nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
								signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
								jsApiList : [ 'previewImage' ]
							});
			
							//上方的config检测通过后，会执行ready方法
							wx.ready(function() {
								wx.previewImage({
								    current: obj_this, // 当前显示图片的http链接
								    urls: myArray // 需要预览的图片http链接列表
								});
							});
							wx.error(function(res) {
								//alert(res.errMsg);
							});
						}
					});				        
					
			//		console.log(this.children[0]);
				});
			})(mui);
		</script>
		<script>
			var baseP = "<%=basePath%>";
		    function toShare() {
		        //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
		        var client = window.location.href;
		        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
				//请求后台，获取jssdk支付所需的参数
				$.ajax({
					type : 'post',
					url : rPath,
					dataType : 'json',
					data : {
						"clientUrl" : client
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
					},
					cache : false,
					error : function() {
						alert("系统错误，请稍后重试");
						return false;
					},
					success : function(data) {
						//微信支付功能只有微信客户端版本大于等于5.0的才能调用
						var return_date = eval(data);
						if (parseInt(data[0].agent) < 5) {
							alert("您的微信版本低于5.0无法使用微信支付");
							return;
						}
						//JSSDK支付所需的配置参数，首先会检查signature是否合法。
						wx.config({
							debug : !true, //开启debug模式，测试的时候会有alert提示
							appId : return_date[0].appId, //公众平台中-开发者中心-appid
							timestamp : return_date[0].config_timestamp, //时间戳
							nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
							signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
							jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
						});
		
						//上方的config检测通过后，会执行ready方法
						wx.ready(function() {
							wx.onMenuShareAppMessage({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
							wx.onMenuShareTimeline({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
		
		
						});
						wx.error(function(res) {
							//alert(res.errMsg);
						});
					}
				});
			}
		    window.onload = toShare();
		</script>
	</body>
</html>
