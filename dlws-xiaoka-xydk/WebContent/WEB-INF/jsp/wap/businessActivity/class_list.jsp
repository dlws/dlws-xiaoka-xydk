<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.picker.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.poppicker.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.min.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/icons-extra.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/style.css"/>
		<title>校咖汇</title>
	</head>
	<body id="allModel">
		<script src="${path}/wapstyle/js/mui.min.js"></script>
		<script src="${path}/wapstyle/js/mui.picker.js"></script>
		<script src="${path}/wapstyle/js/mui.poppicker.js"></script>
		<script src="${path}/wapstyle/js/mui.pullToRefresh.js"></script>
		<script src="${path}/wapstyle/js/mui.pullToRefresh.material.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<script src="${path}/wapstyle/js/class_list.html.js"></script>
		<%-- <script src="${path}/wapstyle/js/area.data.js"></script> --%>
		<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
		<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<script>
		//分类列表数据
		
		var skillClass = ${skillClass};
		var myData = {
			selectItem:["默认排序","筛选"],
		}
		//上拉加载数据
		var upData = {
			list:[
				{
					route:"img/class_picture.png",
					title:"亮剑工作室qq",
					shcool:"广州商学院",
					listImg:[
						"img/class_image1.png",
						"img/class_image2.png",
						"img/class_image3.png"
					],
					name:"精品手绘",
					num:"2687",
					intro:"(独家原创设计手绘) 23元头像半身30全身100，专业定制，情侣预约两份，按人数算，满10减2，请联系我"
				}
			]
		};
		</script>
		<script>
		    var baseP = "<%=basePath%>";
		    function toShare() {
		        //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
		        var client = window.location.href;
		        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
				//请求后台，获取jssdk支付所需的参数
				$.ajax({
					type : 'post',
					url : rPath,
					dataType : 'json',
					data : {
						"clientUrl" : client
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
					},
					cache : false,
					error : function() {
						alert("系统错误，请稍后重试");
						return false;
					},
					success : function(data) {
						//微信支付功能只有微信客户端版本大于等于5.0的才能调用
						var return_date = eval(data);
						if (parseInt(data[0].agent) < 5) {
							alert("您的微信版本低于5.0无法使用微信支付");
							return;
						}
						//JSSDK支付所需的配置参数，首先会检查signature是否合法。
						wx.config({
							debug : !true, //开启debug模式，测试的时候会有alert提示
							appId : return_date[0].appId, //公众平台中-开发者中心-appid
							timestamp : return_date[0].config_timestamp, //时间戳
							nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
							signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
							jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
						});
		
						//上方的config检测通过后，会执行ready方法
						wx.ready(function() {
							wx.onMenuShareAppMessage({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
							wx.onMenuShareTimeline({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
		
		
						});
						wx.error(function(res) {
							//alert(res.errMsg);
						});
					}
				});
			}
		    window.onload = toShare();
		</script>
		
		<script id="dataNav" type="text/html">
				<div class="class-nav">
					{{each navItem as value i}}
						<a onclick ="aa('{{value.id}}')">{{value.className}}</a>
					{{/each}}
				</div>
		</script>
		<script id="selectBar" type="text/html">
				{{each selectItem as value i}} {{if i==0}}
							<a class="mui-icon mui-icon-arrowthindown">{{value}}</a>
							{{/if}} {{if i==1}}
							<a class="mui-icon-extra mui-icon-extra-filter">{{value}}</a>
							{{/if}}
						{{/each}}
						<div class="sel-box">
							<div class="sel-box-child sel-box-order">
								<span id="desc">价格由高到低</span>
								<span id="asc">价格由低到高</span>
							</div>
							<div class="sel-box-child sel-box-sel">
								<span>
									<label>价格区间（元）</label>
									<div class="sel-box-input">
										<input type="text" id="minPrice" placeholder="最低价" /><i>-</i><input type="text" id="maxPrice" placeholder="最高价" />
									</div>
								</span>
								<span>
									<div id="area" class="mui-input-row">
							           <label>所在地</label>
							           <b class="result-tips">请选择地区</b>
							           <b class="show-result"></b>
							           <i class="mui-icon mui-icon-arrowdown"></i>
							        </div>
								</span>
								<button class="mui-btn com-btn">完成</button>
							</div>
						</div>
		</script>
		<script id="classListBox" type="text/html">
								{{each list as value i}}
						        <li class="mui-table-view-cell">
									<a href="${path}/home/getSkillInfo.html?id={{value.id}}">
					                <header>
					                	<img src={{value.headImgUrl}} />
					                	<div>
					                		<h2>{{value.userName}}</h2>
					                		<p>{{value.schoolName}}</p>
					                	</div>
					                </header>
									</a>
					                <figure>
					                	{{each value.images as value i}}
					                	<span>
					                		<img src={{value.imgUrl}} onclick="ff(this,'{{value.imgUrl}}')" style="height:100px;">
					                	</span>
					                	{{/each}}
					                </figure>
									<a href="${path}/home/getSkillInfo.html?id={{value.id}}">
					                <div class="class-name">
					                	<h2>{{value.skillName}}</h2>
					                	<p>
					                		<span class="mui-icon mui-icon-eye"></span>
					                		{{value.viewNum}}
					                	</p>
					                </div>
					                <p class="class-intro" style="text-overflow:ellipsis;white-space: nowrap;overflow:hidden;">
					                	<span class="marks"></span>
					                	{{value.skillDepict}}
					                </p>
									</a>
						        </li>
							{{/each}}
						
		</script>
		
			<div class="mui-content class-page">
				<div class="top-bar">
					<header class="header-bar">
						<form class="mui-input-row mui-search search-bar" action="getClassList.html" onsubmit="searchSub()" >
							<input type="search" id="searchValue" name="searchValue" class="mui-input-clear" value="${searchValue }" placeholder="搜索用户，技能">
							<input type="hidden"  name="fid" class="mui-input-clear" value="${fid }">
							<input type="hidden" id="orderp" value="">
						</form>
						<!--搜索栏-->
					</header><!--头部搜索栏-->
					<div class="class-nav-title">
						<h1>全部分类</h1>
						<span class="mui-icon mui-icon-arrowup"></span>
					</div>
					<nav class="classNav" id="navBar">
						<div class="class-nav-box" id ="class-nav-box-data">
							
						</div>
						<span class="mui-icon mui-icon-arrowdown"></span>
					</nav><!--导航-->
					<div class="select-bar" id="select-bar-data">
						
					</div><!--筛选栏-->
				</div><!--头部-->
				<div class="classList-box" >
					<div class="mui-scroll-wrapper class-list class-list-active">
						<div class="mui-scroll">
							<ul class="mui-table-view" id="classList-box-data">
							
							
					    	</ul>
						</div>
					</div>
				</div><!--列表容器-->
				
			</div>
			<div class="mask"></div><!--遮罩-->
			<input type="hidden" name = "currentPage" id="currentPage" value="1">
			<input type="hidden" name = "sid" id="sid" value="0">
			
			<input type="hidden" id="currentid" value="0">
			<input type="hidden" id="orderp" value="">
			<input type="hidden" id="fid2" value="${fid }">
			<input type="hidden" id="topValue" value="${topValue }">
			<input type="hidden" id="ispage" value="${ispage }">
			<input type="hidden" id="schoolName" name="schoolName" >
		</script>
		
		<script id="loadData" type="text/html">
			{{each list as value i}}
				<li class="mui-table-view-cell">
					<a href="${path}/home/getSkillInfo.html?id={{value.id}}">
	                <header>
	                	<img src={{value.route}} />
	                	<div>
	                		<h2>{{value.title}}</h2>
	                		<p>{{value.shcool}}</p>
	                	</div>
	                </header>
					</a>
	                <figure>
	                	{{each value.listImg as value i}}
	                	<span>
	                		<img src={{value}} onclick="ff(this,'{{value.imgUrl}}')" style="height:100px;"/>
	                	</span>
	                	{{/each}}
	                </figure>
					<a href="${path}/home/getSkillInfo.html?id={{value.id}}">
	                <div class="class-name">
	                	<h2>{{value.name}}</h2>
	                	<p>
	                		<span class="mui-icon mui-icon-eye"></span>
	                		{{value.num}}
	                	</p>
	                </div>
	                <p class="class-intro" style="text-overflow:ellipsis;white-space: nowrap;overflow:hidden;">
	                	<span class="marks"></span>
	                	{{value.intro}}
	                </p>
					</a>
		        </li>
		    {{/each}}
		</script>
		
		<script> 
		//var ispage = "${isPage}";
		var isPage =  $("#isPage").val();
		var orderp = jQuery("#orderp").val(); //排序
		var max = jQuery("#maxPrice").val();  //最大价格
		var min = jQuery("#minPrice").val();  //最低交割
		var cname = jQuery("#cityName").val(); //城市
		var svalue = jQuery("#searchValue").val(); //顶部搜索关键词
		var tvalue = jQuery("#topValue").val(); //首页搜索关键词
		var schoolName = jQuery("#schoolName").val(); //获取学校
		
		
		var areaData = ${citySchoolList};
		
			///搜索
			function searchSub(){
				jQuery("#topValue").val("");//本页搜索后设置首页传入的关键词为空
				getBy();///获取参数
				//searchBy();///搜索
				return true;
			}
		
			function getBy(){
				orderp = jQuery("#orderp").val();
				max = jQuery("#maxPrice").val();
				min = jQuery("#minPrice").val();
				cname = jQuery("#cityName").val();
				svalue = jQuery("#searchValue").val();
				tvalue = jQuery("#topValue").val();
				schoolName = jQuery("#schoolName").val(); 
			}
		
			function aa(id){
				ispage = "1";
				getBy();
				jQuery("#searchValue").val("");////清除搜索条件
				//当前页
				var currentPage =  1;
				$("#currentPage").attr("value",currentPage);//页数设置为1
				var fid2 =  $("#fid2").val();
				$.ajax({
					type : "post",
					url : "getUserInfoByClassId.html",
					dataType : "json",
					async : false,
					data : {currentPage:currentPage,fid:fid2,sid:id,orderPrice:orderp,maxPrice:max,minPrice:min,cityName:cname,schoolName:schoolName},
					success : function(data) {
						/* if(data.num>0){
							var cu = currentPage+1;
							$("#currentPage").attr("value",cu);
						}  */
						$("#sid").attr("value",data.sid);
						var proData = data.userInfoList;
						var proHtml = template('classListBox',JSON.parse(proData));
						document.getElementById('classList-box-data').innerHTML = proHtml;
					},
					error : function(XMLHttpRequest, textStatus, errorThrown){
			          //通常情况下textStatus和errorThrown只有其中一个包含信息
						//window.location.href="";
			          alert("失败！");
			       }
				});
				
				///记录当前点击的tab
				$("#currentid").val(id);
			 	var class_nav = document.querySelector('.classNav');
				class_nav.style.cssText += 'height:44px;top:44px;'; 
			}
			function ff(obj,ur){
				
				var myArray=new Array();
				var obj_parent = obj.parentNode.parentNode;
				var obj_child =obj_parent.getElementsByTagName("img");
				//console.log(obj_child );	
				for(var i=0;i<obj_child.length;i++){
					myArray[i]=obj_child[i].src;
				} 
				
				//获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
		        var clientUrl = window.location.href;
		        var reqPath='<%=contextPath%>/skillUser/getJSConfig.html';
				//请求后台，获取jssdk支付所需的参数
				$.ajax({
					type : 'post',
					url : reqPath,
					dataType : 'json',
					data : {
						"clientUrl" : clientUrl
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
					},
					cache : false,
					error : function() {
						alert("系统错误，请稍后重试");
						return false;
					},
					success : function(data) {
						//微信支付功能只有微信客户端版本大于等于5.0的才能调用
						var return_date = eval(data);
						
						if (parseInt(data[0].agent) < 5) {
							alert("您的微信版本低于5.0无法使用微信支付");
							return;
						}
						//JSSDK支付所需的配置参数，首先会检查signature是否合法。
						wx.config({
							debug : false, //开启debug模式，测试的时候会有alert提示
							appId : return_date[0].appId, //公众平台中-开发者中心-appid
							timestamp : return_date[0].config_timestamp, //时间戳
							nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
							signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
							jsApiList : [ 'previewImage' ]
						});
		
						//上方的config检测通过后，会执行ready方法
						wx.ready(function() {
							wx.previewImage({
							    current: ur, // 当前显示图片的http链接
							    urls: myArray // 需要预览的图片http链接列表
							});
						});
						wx.error(function(res) {
							//alert(res.errMsg);
						});
					}
				});	
				
			}
			function bb(ul){
				
				getBy();
				//当前页
				var currentPage =  parseInt($("#currentPage").val())+1;
				var id =  $("#sid").val();
				var fid2 =  $("#fid2").val();
				$.ajax({
					type : "post",
					url : "getUserInfoByClassId.html",
					dataType : "json",
					async : false,
					data : {currentPage:currentPage,fid:fid2,sid:id,orderPrice:orderp,maxPrice:max,minPrice:min,cityName:cname,searchValue:svalue,schoolName:schoolName},
					success : function(data) {
						if(data.num>0){
							var cu = currentPage;
							$("#currentPage").attr("value",cu);
						} 
						var proData = data.userInfoList;
						var proHtml = template('classListBox',JSON.parse(proData));
						
						$('#classList-box-data').append(proHtml);
					},
					error : function(XMLHttpRequest, textStatus, errorThrown){
			          //通常情况下textStatus和errorThrown只有其中一个包含信息
						//window.location.href="";
			          alert("失败！");
			       }
				});
			}
			
			//筛选搜索调用
			function searchBy(){
				
				getBy();
				var id = $("#currentid").val();
				
				var schoolName = jQuery("#schoolName").val(); //获取学校
				//当前页
				var currentPage =  1;
				//var currentPage =  parseInt($("#currentPage").val())+1;
				$("#ispage").attr("value","1");//开启分页
				var fid2 =  $("#fid2").val();
				$.ajax({
					type : "post",
					url : "getUserInfoByClassId.html",
					dataType : "json",
					async : false,
					data : {currentPage:currentPage,fid:fid2,sid:id,orderPrice:orderp,maxPrice:max,minPrice:min,cityName:cname,schoolName:schoolName,searchValue:svalue,topValue:tvalue},
					success : function(data) {
						/* if(data.num>0){
							var cu = currentPage+1;
							$("#currentPage").attr("value",cu);
						} */
						$("#sid").attr("value",data.sid);
						var proData = data.userInfoList;
						var proHtml = template('classListBox',JSON.parse(proData));
						document.getElementById('classList-box-data').innerHTML = proHtml;
					},
					error : function(XMLHttpRequest, textStatus, errorThrown){
			          //通常情况下textStatus和errorThrown只有其中一个包含信息
						//window.location.href="";
			          alert("失败！");
			       }
				});
				
			 	var class_nav = document.querySelector('.classNav');
				class_nav.style.cssText += 'height:44px;top:44px;'; 
			}
		
			(function($,doc){
				mui.init();
				//分类数据
				var skillClass = ${skillClass};
				var focusHtml = template('dataNav', skillClass);
				document.getElementById('class-nav-box-data').innerHTML = focusHtml;
				//筛选数据
				var dataBar = template('selectBar',myData);
				document.getElementById('select-bar-data').innerHTML = dataBar;
				//入住用户的信息数据
				var userInfoList = ${userInfoList};
				var userInfoHtml = template('classListBox', userInfoList);
				document.getElementById('classList-box-data').innerHTML = userInfoHtml;
				
				
				
				var loadData = template('loadData',new Object());//上拉加载数据
				
				
				//禁用滚动条
				document.getElementsByTagName('body')[0].addEventListener('touchmove', function (e) {
				  e.preventDefault();
				});
				
				//自定义滚动条
				var deceleration = mui.os.ios ? 0.003 : 0.0009;
				$('.mui-scroll-wrapper').scroll({
					bounce: true,
					indicators: true, //是否显示滚动条
					deceleration: deceleration
				});
				
				//显示顶部搜索栏
				document.querySelector('.search-bar').style.display = "block";
				//获取屏幕宽度减去下箭头宽度
				var screenWith = window.screen.availWidth-44;
				//设定导航栏宽度为屏幕宽度减去下箭头的宽度
				document.querySelector('.class-nav-box').style.width = screenWith + "px";
				
				//选项卡
				var nav = document.getElementById('navBar');
				var nav_tag = nav.getElementsByTagName('a');
				var list_tag = document.getElementsByClassName('class-list');
				var class_nav = document.querySelector('.classNav');
				var nav_title = document.querySelector('.class-nav-title');
				var nav_box = document.querySelector('.class-nav-box');
				var arrowdown = document.querySelector('.mui-icon-arrowdown');
				var mask = document.querySelector('.mask');
				var len = nav_tag.length;
				var i = 0;
				list_tag[0].className = "mui-scroll-wrapper class-list class-list-active";//第一个列表显示
				nav_tag[0].className = "active";//导航栏第一个项目高亮
				for(var i=0;i<len;i++){
					nav_tag[i].index = i;
					nav_tag[i].addEventListener('tap',function(){
						for(i=0; i<len; i++){
						    nav_tag[i].className = '';
						    nav_tag[i].parentNode.style.display = 'none';
						 // list_tag[i].className = 'mui-scroll-wrapper class-list';
						   // class_nav.style.cssText += 'height:44px;top:44px;';
							nav_title.style.display = 'none';
							nav_box.style.width = screenWith + "px";
							arrowdown.style.display = "block";
							mask.style.display = 'none';
					    } 
					   nav_tag[this.index].className = 'active';
					   nav_tag[this.index].parentNode.style.display = 'block';
					   //list_tag[0].className = "mui-scroll-wrapper class-list class-list-active";
					});
				}
				
				//显示隐藏的导航
				document.querySelector('.mui-icon-arrowdown').addEventListener('tap',function(){
					for(i=0; i<len; i++){
					    nav_tag[i].parentNode.style.display = 'block';
						class_nav.style.cssText += 'height:auto;top:88px';
						nav_title.style.display = 'block';
						nav_box.style.width = '100%';
						arrowdown.style.display = "none";
						mask.style.display = 'block';
						sel_sel.style.display = 'none';
						sel_order.style.display = "none";
						sel_order_bar.style.color = "#333";
						sel_sel_bar.style.color = "#333";
				    }
				});
				
				//地区选择
				$.ready(function() {
					//地区
					var areaPicker = new $.PopPicker({
						layer:2
					});
					areaPicker.setData(areaData);
					var showAreaPickerButton = doc.getElementById('area');
					showAreaPickerButton.addEventListener('tap', function(event) {
						areaPicker.show(function(items) {
							document.querySelector('#area .result-tips').style.display = "none";
							document.querySelector('#area .show-result').innerText = items[1].text;
							//返回 false 可以阻止选择框的关闭
							//return false;
							//$("#schoolName").attr("value",items[1].text);
							document.getElementById("schoolName").value=items[1].text;//设置选中的学校
						});
					}, false);
				});
				
				//隐藏导航
				document.querySelector('.mui-icon-arrowup').addEventListener('tap',function(){
					class_nav.style.cssText += 'height:44px;top:44px;';
					nav_title.style.display = 'none';
					nav_box.style.width = screenWith + "px";
					arrowdown.style.display = "block";
					mask.style.display = 'none';
				});
				document.querySelector('.mask').addEventListener('tap',function(){
					class_nav.style.cssText += 'height:44px;top:44px;';
					nav_title.style.display = 'none';
					nav_box.style.width = screenWith + "px";
					arrowdown.style.display = "block";
					mask.style.display = 'none';
					sel_sel.style.display = 'none';
					sel_order.style.display = "none";
					sel_order_bar.style.color = "#333";
					sel_sel_bar.style.color = "#333";
				});
				
				//筛选框
				var sel_order = document.querySelector('.sel-box-order');
				var sel_sel = document.querySelector('.sel-box-sel');
				var sel_order_bar = document.querySelector('.select-bar .mui-icon-arrowthindown');
				var sel_sel_bar = document.querySelector('.select-bar .mui-icon-extra-filter');
				
				//排序显示
				sel_order_bar.addEventListener('tap',function(){
					sel_order_bar.style.color = "#2bbfff";
					sel_sel_bar.style.color = "#333";
					sel_sel.style.display = 'none';
					sel_order.style.display = "block";
					mask.style.display = 'block';
				});
				
				//排序隐藏
				mui('.sel-box-order').on('tap','span',function(){
					sel_order_bar.style.color = "#333";
					sel_sel.style.display = 'none';
					sel_order.style.display = "none";
					mask.style.display = 'none';
					sel_order_bar.innerHTML = this.innerHTML;
					
					jQuery("#orderp").val(this.id);
					searchBy();
				});
				
				//筛选显示
				sel_sel_bar.addEventListener('tap',function(){
					sel_sel_bar.style.color = "#2bbfff";
					sel_order_bar.style.color = "#333";
					sel_sel.style.display = 'block';
					sel_order.style.display = "none";
					mask.style.display = 'block';
				});
				
				//筛选隐藏
				document.querySelector('.com-btn').addEventListener('tap',function(){
					sel_sel_bar.style.color = "#333";
					setTimeout(function(){
						sel_sel.style.display = 'none';
						sel_order.style.display = "none";
						mask.style.display = 'none';
					},320);
					
					searchBy();
				});
				
				//循环初始化所有上拉加载。
				$.ready(function() {
					$.each(document.querySelectorAll('.mui-scroll-wrapper .mui-scroll'), function(index, pullRefreshEl) {
						$(pullRefreshEl).pullToRefresh({
							up: {
								callback: function() {
									var self = this;
									setTimeout(function() {
										var ul = self.element.querySelector('.mui-table-view');
										createFragment(ul, index, 5);
										self.endPullUpToRefresh();
									}, 1000);
								}
							}
						});
					});
					var createFragment = function(ul, index, count, reverse) {
						loadData = template('loadData', upData);
						//ul.innerHTML += loadData;
						if(ispage == "0"){///添加判断，屏蔽搜索时的分页
						}else{
							bb(ul);
						}
					};
				});
				
			})(mui,document);
			
			function initPage(){
				//循环初始化所有上拉加载。
				$.ready(function() {
					$.each(document.querySelectorAll('.mui-scroll-wrapper .mui-scroll'), function(index, pullRefreshEl) {
						$(pullRefreshEl).pullToRefresh({
							up: {
								callback: function() {
									var self = this;
									setTimeout(function() {
										var ul = self.element.querySelector('.mui-table-view');
										createFragment(ul, index, 5);
										self.endPullUpToRefresh();
									}, 1000);
								}
							}
						});
					});
					var createFragment = function(ul, index, count, reverse) {
						loadData = template('loadData', upData);
						//ul.innerHTML += loadData;
						bb(ul);
					};
				});
			}
		</script>
	</body>
</html>
