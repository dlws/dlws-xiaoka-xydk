<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.min.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/style.css"/>
	    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<title>校咖汇</title>
	</head>
	</head>
	<body id="allModel">
		<script src="${path}/wapstyle/js/mui.min.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
			<nav class="foot-bar-tab">
				<a class="foot-tab-item foot-active" href="${path}/home/index.html">
					<span class="foot-icon foot-icon-home"></span>
					<span class="foot-tab-label">首页</span>
				</a>
				<a class="foot-tab-item" href="${path}/sub/toPub.html">
					<span class="foot-icon foot-icon-pub"></span>
					<span class="foot-tab-label">发布</span>
				</a>
			</nav><!--底部条-->
			<div class="mui-content mui-scroll-wrapper">
				<div class="mui-scroll">
					<header class="header-bar">
						<div class="status-bar" id="status-bar-data">
							
						</div><!--顶部状态栏-->
						<form id="sform" class="mui-input-row mui-search search-bar" action="" method="post" onsubmit="return sub();">
							<input type="search" id="top" name="topValue" class="mui-input-clear" placeholder="搜索用户，技能">
						</form>
						<!--搜索栏-->
					</header><!--头部搜索栏-->
					
					<div id="slider" class="mui-slider">
						
					</div><!--焦点图-->
					
					<div id="Gallery" class="mui-slider gallery">
						
					</div><!--快捷入口-->
						<div class="class-module">
							<h1>
								<span class="class-module-new">最新大咖</span>
								<div class="line"></div>
							</h1><!--标题-->
							<div id="marquee" class="marquee">
								<div style="width:8000px;">
									<ul id="marquee_1">
										
									</ul>
									<ul id="marquee_2"></ul>
								</div>
							</div><!--marqueeleft--><!--列表-->
							<!-- <div id="sliderSegmentedControl" class="mui-scroll-wrapper mui-slider-indicator mui-segmented-control mui-segmented-control-inverted">
								<div class="mui-scroll" id="mui-scroll-new">
									
								</div>
							</div> --><!--列表-->
						</div><!--最新大咖-->
						<div class="class-module">
							<h1>
								<span class="class-module-hot">推荐大咖</span>
								<div class="line"></div>
							</h1><!--标题-->
							 <ul class="mui-table-view mui-grid-view" id="loadList">
			    				
			    			</ul><!--列表--> 
			    			<button class="mui-btn load-btn" id ="load-btn">查看更多</button>
						</div><!--推荐大咖-->
						<input type="hidden" name = "currentPage" id="currentPage" value="1">
				</div>
				
			</div><!--主体内容-->
			
			<script id="loData" type="text/html">
			{{each list as value i}}
				<li class="mui-table-view-cell mui-media mui-col-xs-4">
		            <a href="${path}/home/getUserInfo.html?id={{value.id}}">
						<img style="height: 105px;" src="{{list[i].imageUrl}}" />
						<div class="item-info">
							<p>{{list[i].className}}</p>
							<p>{{list[i].userName}}</p>
							<p>{{value.schoolName}}</p>
						</div>
					</a>
		        </li>
		    {{/each}}
			</script>
			
			<script id="muiScrollNew" type="text/html">
					{{each list as value i}}
											<li>
												<a href="${path}/home/getUserInfo.html?id={{value.id}}">
													<img style = "height: 105px;" src="{{list[i].imageUrl}}" />
													<div class="item-info">
														<p>{{list[i].className}}</p>
														<p>{{list[i].userName}}</p>
														<p>{{value.schoolName}}</p>
													</div>
												</a>
											</li>
										{{/each}}

			</script>
			<script id="gallery" type="text/html">
					<div class="mui-slider-group">
							{{each sort1 as value i}}
								<div class="mui-slider-item">
									<ul class="mui-table-view mui-grid-view mui-grid-9">
										{{each sort1[i].classItem as value i}}
											<li class="mui-table-view-cell mui-media mui-col-xs-3 mui-col-sm-3">
												<a href="${path}/home/getClassList.html?fid={{value.id}}">
													<span class="mui-icon"><img src={{value.imageURL}}></span>
													<div class="mui-media-body">{{value.className}}</div>
												</a>
											</li>
										{{/each}}
									</ul>
								</div>
							{{/each}}
						</div>
						<div class="mui-slider-indicator">
							{{each sort1 as value i}}
								{{if i==0}}
									<div class='mui-indicator mui-active'></div>
								{{/if}} {{if i>0}}
									<div class='mui-indicator'></div>
								{{/if}}
							{{/each}}
						</div>
			</script>	
			
			<script id="muiSliderData" type="text/html">
			<div class="mui-slider-group mui-slider-loop">
							<div class="mui-slider-item mui-slider-item-duplicate">
								<a href="{{lastPic.redirectUrl}}">
									<img src="{{lastPic.picUrl}}"/>
								</a>
							</div><!--最后一张图利-->
				    		{{each list as value i }}
				    			<div class="mui-slider-item">
									<a href="{{list[i].redirectUrl}}">
										<img src="{{list[i].picUrl}}"/>
									</a>
								</div>
				    		{{/each}}
				    		<div class="mui-slider-item mui-slider-item-duplicate">
								<a href="{{firstPic.redirectUrl}}">
									<img src="{{firstPic.picUrl}}"/>
								</a>
							</div><!--第一张图片-->
						</div>
						<div class="mui-slider-indicator">
							{{each list as value i}} {{if i==0}}
								<div class='mui-indicator mui-active'></div>
							{{/if}} {{if i>0}}
								<div class='mui-indicator'></div>
							{{/if}}{{/each}}
						</div>
		</script>	
		<script id="statusBarData" type="text/html">
				<div class="come-num-box">
								<span class="come-num">
									<b>
										{{each numList as value i}}
											<i>{{value}}</i>
										{{/each}}
										<i>${allNum}</i>
									</b>
								</span>
								<span class="come-font">{{comeFont}}</span>
							</div>
							<span class="mui-icon mui-icon-search"></span>
		</script>	
		<script id="loadListContent" type="text/html">

			{{each list as value i}}
				<li class="mui-table-view-cell mui-media mui-col-xs-4">
		            <a href="${path}/home/getUserInfo.html?id={{value.id}}">
						<img src="{{list[i].imageUrl}}" style="height: 105px;"/>
						<div class="item-info">
							<p>{{list[i].className}}</p>
							<p>{{list[i].userName}}</p>
							<p>{{value.schoolName}}</p>
						</div>
					</a>
		        </li>
		    {{/each}}

		</script>
		<script src="${path}/wapstyle/js/index.html.js"></script><!--页面数据-->
		<script>
		    var baseP = "<%=basePath%>";
		    function toShare() {
		        //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
		        var client = window.location.href;
		        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
				//请求后台，获取jssdk支付所需的参数
				$.ajax({
					type : 'post',
					url : rPath,
					dataType : 'json',
					data : {
						"clientUrl" : client
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
					},
					cache : false,
					error : function() {
						alert("系统错误，请稍后重试");
						return false;
					},
					success : function(data) {
						//微信支付功能只有微信客户端版本大于等于5.0的才能调用
						var return_date = eval(data);
						if (parseInt(data[0].agent) < 5) {
							alert("您的微信版本低于5.0无法使用微信支付");
							return;
						}
						//JSSDK支付所需的配置参数，首先会检查signature是否合法。
						wx.config({
							debug : !true, //开启debug模式，测试的时候会有alert提示
							appId : return_date[0].appId, //公众平台中-开发者中心-appid
							timestamp : return_date[0].config_timestamp, //时间戳
							nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
							signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
							jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
						});
		
						//上方的config检测通过后，会执行ready方法
						wx.ready(function() {
							
							wx.onMenuShareAppMessage({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
							wx.onMenuShareTimeline({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
		
		
						});
						wx.error(function(res) {
							//alert(res.errMsg);
						});
					}
				});
			}
		    window.onload = toShare();
		</script>
		<script>
			function sub(){
				if($("#top").val() == ""){
					return false;
				}else{
					$("#sform").attr("action","getClassList.html");
				}
			}
			
			function aa(){
				var currentPage =  parseInt($("#currentPage").val())+1;
				//ajax
				$.ajax({
					type : "post",
					url : "<%=basePath%>/home/getUserBasicinfo.html",//得到商品的数量
					dataType : "json",
					async : false,
					data : {currentPage:currentPage},
					success : function(data) {
						if(data.num>0){
							var cu = currentPage;
							$("#currentPage").attr("value",cu);
						} 
						if(data.num==0){
							document.getElementById('load-btn').innerHTML = '已没有更多';
						}
						
						var proData = data.usersOrdJson;
						var loadData = template('loadListContent',JSON.parse(proData));//点击加载更多数据
						document.getElementById('loadList').innerHTML += loadData;
					},
					error : function(XMLHttpRequest, textStatus, errorThrown){
			          //通常情况下textStatus和errorThrown只有其中一个包含信息
						//window.location.href="";
			          alert("失败！");
			       }
				});
			}
			(function($){
				mui.init();
				
		
				var skillClass=${ss };
				var usersJson=${usersJson};
				var usersOrdJson=${usersOrdJson};
				var bannerInfo=${bannerInfo};
				
				var statusBarData = template('statusBarData',myData);
				document.getElementById('status-bar-data').innerHTML = statusBarData;
				
				//轮播图
				var muiSliderData = template('muiSliderData',bannerInfo);
				document.getElementById('slider').innerHTML = muiSliderData;
				
				var ery = template('gallery',skillClass);
				document.getElementById('Gallery').innerHTML = ery;
				
				var muiScroll = template('muiScrollNew',usersJson);
				document.getElementById('marquee_1').innerHTML = muiScroll;
				
				
				var load = template('loData',usersOrdJson);
				document.getElementById('loadList').innerHTML = load;
				
				
				//禁用滚动条
				document.getElementsByTagName('body')[0].addEventListener('touchmove', function (e) {
				  e.preventDefault();
				});
				
				//自定义滚动条
				var deceleration = mui.os.ios ? 0.003 : 0.0009;
				$('.mui-scroll-wrapper').scroll({
					bounce: true,
					indicators: true, //是否显示滚动条
					deceleration: deceleration
				});
				
				//焦点图
				var slider = mui("#slider");
				slider.slider({
					interval: 3000
				});
				
				//点击加载更多
				document.querySelector('.load-btn').addEventListener('tap',function(){
					setTimeout(function(){
						aa();
					},320);
				});
				
				//点击跳转链接
				mui('.mui-table-view-cell').on('tap','a',function(){
					var id = this.getAttribute('href');
					var href = this.href;
					mui.openWindow({
						id: id,
						url: href,
						waiting:{
							auto:false
						}
					});
				});
				
				mui('.mui-scroll').on('tap','a',function(){
					var id = this.getAttribute('href');
					var href = this.href;
					mui.openWindow({
						id: id,
						url: href,
						waiting:{
							auto:false
						}
					});
				});
				
				//搜索框出现
				document.querySelector('.status-bar .mui-icon-search').addEventListener('tap',function(){
					document.querySelector('.status-bar').style.display = "none";
					document.querySelector('.search-bar').style.display = "block";
				});
				
			})(mui);
			//无缝滚动
			function marquee(){
				var obj = document.getElementById("marquee");
				var obj1 = document.getElementById("marquee" + "_1");
				var obj2 = document.getElementById("marquee" + "_2");
				if (obj2.offsetWidth - obj.scrollLeft <= 0){
					obj.scrollLeft -= obj1.offsetWidth;
				}else{
					obj.scrollLeft++;
				}
			}
			function marqueeStart(){
				var obj = document.getElementById("marquee");
				var obj1 = document.getElementById("marquee_1");
				var obj2 = document.getElementById("marquee_2");
				obj2.innerHTML = obj1.innerHTML;
				var marqueeVar = window.setInterval("marquee()", 20);
			}
			marqueeStart();
		</script>
	</body>
</html>
