<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.min.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/style.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.picker.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.poppicker.css"/>
	    <script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	    
		<title>校咖汇</title>
		<script src="${path}/wapstyle/js/mui.min.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<script src="${path}/wapstyle/js/mui.picker.js"></script>
		<script src="${path}/wapstyle/js/mui.poppicker.js"></script>
		<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
		var cityData = [];
		function loadcity(){
			$.ajax({
				type : "post",
				url : "cityInfo.html",
				dataType : "json",
				data:{goodsId:""},
				async : false,
				success : function(data) {
					cityData = data.list;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
		          alert("出错了！");
		       }
			});
		}
		
		function sub(){
			if(jQuery("#startDate").val()=="" || jQuery("#endDate").val()==""){
				alert("请选择活动时间");
				return;
			}
			
			if(jQuery("#cityId").val()==""){
				alert("请选择活动城市");
				return;
			}
			
			if(jQuery("#phone").val()==""){
				alert("请填写联系电话");
				return;
			}
			
			if(jQuery("#demand").val()==""){
				alert("请填写活动需求");
				return;
			}
			
			jQuery(".tiaozhuan").show();
			setTimeout(function(){
				jQuery("#sform").submit();
			},3000);
			
		}
		</script>
		<script>
		    var baseP = "<%=basePath%>";
		    function toShare() {
		        //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
		        var client = window.location.href;
		        var rPath='<%=contextPath%>/skillUser/getJSConfig.html';
				//请求后台，获取jssdk支付所需的参数
				$.ajax({
					type : 'post',
					url : rPath,
					dataType : 'json',
					data : {
						"clientUrl" : client
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
					},
					cache : false,
					error : function() {
						alert("系统错误，请稍后重试");
						return false;
					},
					success : function(data) {
						//微信支付功能只有微信客户端版本大于等于5.0的才能调用
						var return_date = eval(data);
						if (parseInt(data[0].agent) < 5) {
							alert("您的微信版本低于5.0无法使用微信支付");
							return;
						}
						//JSSDK支付所需的配置参数，首先会检查signature是否合法。
						wx.config({
							debug : !true, //开启debug模式，测试的时候会有alert提示
							appId : return_date[0].appId, //公众平台中-开发者中心-appid
							timestamp : return_date[0].config_timestamp, //时间戳
							nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
							signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
							jsApiList : [ 'onMenuShareAppMessage','onMenuShareTimeline' ]
						});
		
						//上方的config检测通过后，会执行ready方法
						wx.ready(function() {
							
							wx.onMenuShareAppMessage({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
							wx.onMenuShareTimeline({
							    title: "校咖汇", // 分享标题
							    desc: "欢迎使用校咖汇", // 分享描述
							    link: client, // 分享链接
							    imgUrl: baseP+'wapstyle/img/logo2.png', // 分享图标
							    type: '', // 分享类型,music、video或link，不填默认为link
							    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							    success: function () { 
							        // 用户确认分享后执行的回调函数
							    },
							    cancel: function () { 
							        // 用户取消分享后执行的回调函数
							    }
							});
		
		
						});
						wx.error(function(res) {
							//alert(res.errMsg);
						});
					}
				});
			}
		    window.onload = toShare();
		</script>
		<style>
			.tiaozhuan{
				width:100%;
				height:100%;
				position:fixed;
				top:0;
				z-index:10000;
				display:none;
			}
			.tiaozhuan img{
				width:100%;
				display:block;
				margin:0 auto;
			}
		</style>
	</head>
	<body id="allModel">
		
		<script id="modelContent" type="text/html">
			<div class="tiaozhuan">
				<img src="${path}/wapstyle/img/tiaozhuan_02.jpg">
			</div>
			<div class="mui-content mui-scroll-wrapper contact-content">
			    <div class="mui-scroll">
			    	<header class="head-bar">
			    		<img src={{photos}}>
			    		<h1>{{title}}</h1>
			    	</header><!--头部-->
			    	<section class="summariy">
			    		<img src={{perImg}} />
			    		<div class="summariy-body">
			    			<h1>{{perTitle}}<span>线上</span></h1>
			    			<p>{{perIntro}}</p>
			    			<h2>{{perPrice}}</h2>
			    		</div>
			    	</section><!--概要-->
			    	<form id="sform" class="mui-input-group" action="sub.html" method="post">
						{{each form as value i}} {{if value.titleValue ==0 }}	
					        <div id={{value.id}} class="mui-input-row">
					           <label>{{value.title}}</label>
					           <span class="result-tips">{{value.tips}}</span>
					           <span class="show-result"></span>
					           <i class="mui-icon mui-icon-arrowdown"></i>
					           <div id="activeTypeResult"></div>
					        </div>{{/if}} {{if value.titleValue == 1}}
							<div class="mui-input-row">
					           <label>{{value.title}}</label>
					           <div class="time-box">
					           		<input type="text" id="startDate" name="startDate" onClick="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')||\'%y-%M-%d\'}'})" value={{value.startTimeTips}} />
									<span>-</span>
									<input type="text" id="endDate" name="endDate" onClick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')||\'%y-%M-%d\'}'})" value={{value.endTimeTips}}>
					           </div>
					      	</div> {{/if}} {{if value.titleValue ==2}}
					      	<div class="mui-input-row textarea-cell">
					           <label>{{value.title}}</label>
					           <textarea rows="3" id="demand" name="demand" placeholder={{value.tips}}></textarea>
					      	</div>{{/if}} {{if value.titleValue == 3}}
					      	<div class="mui-input-row">
					           <label>{{value.title}}</label>
					           <input type="tel" name="phone" id="phone" placeholder={{value.tips}} onkeyup="value=value.replace(/[^\-?\d.]/g,'')">
					      	</div>
					      	{{/if}} {{if value.titleValue ==4}}
					      	<div class="mui-input-row textarea-cell">
					           <label>{{value.title}}</label>
					           <textarea id="remark" name="remark" rows="3" placeholder={{value.tips}}></textarea>
					      	</div>{{/if}} 
				        {{/each}} 
						<input type="hidden" id="cityId" name="cityId" value=""/>
						<input type="hidden"  name="userId" value="${binfo.id}"/>
					</form>
					<button class="mui-btn submit-btn" onclick="sub()">提交订单</button>
			    </div>
			</div>
		</script>
		<script>
	     
		Date.prototype.pattern=function(fmt) {         
		    var o = {         
		    "M+" : this.getMonth()+1, //月份         
		    "d+" : this.getDate(), //日         
		    "h+" : this.getHours()%12 == 0 ? 12 : this.getHours()%12, //小时         
		    "H+" : this.getHours(), //小时         
		    "m+" : this.getMinutes(), //分         
		    "s+" : this.getSeconds(), //秒         
		    "q+" : Math.floor((this.getMonth()+3)/3), //季度         
		    "S" : this.getMilliseconds() //毫秒         
		    };         
		    var week = {         
		    "0" : "/u65e5",         
		    "1" : "/u4e00",         
		    "2" : "/u4e8c",         
		    "3" : "/u4e09",         
		    "4" : "/u56db",         
		    "5" : "/u4e94",         
		    "6" : "/u516d"        
		    };         
		    if(/(y+)/.test(fmt)){         
		        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));         
		    }         
		    if(/(E+)/.test(fmt)){         
		        fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "/u661f/u671f" : "/u5468") : "")+week[this.getDay()+""]);         
		    }         
		    for(var k in o){         
		        if(new RegExp("("+ k +")").test(fmt)){         
		            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));         
		        }         
		    }         
		    return fmt;         
		};       
		     
		
		
		
		(function($,doc){
				mui.init();
				
				loadcity();
				var date = new Date();      
				var time1 = date.pattern("yyyy-MM-dd hh:mm:ss");
				//页面数据
				var myData = {
					photos:"${binfo.headImgUrl}",
					title:"${binfo.userName}",
					perImg:"${skinfo.imgURL}",
					perTitle:"${skinfo.skillName}",
					perIntro:"${skinfo.skillDepict}",
					perPrice:"${skinfo.skillPrice}元/次",
					form:[
						{
							id:"activeTime",
							title:"活动时间",
							startTimeTips:time1,
							endTimeTips:time1,
							titleValue:"1"
						},{
							id:"activeCity",
							title:"活动城市",
							tips:"选择活动展开城市",
							titleValue:"0"
						},{
							title:"活动需求",
							tips:"请在此处填写活动详细信息",
							titleValue:"2"
						},{
							title:"联系电话",
							tips:"填写您的联系电话",
							titleValue:"3"
						},{
							title:"备注",
							tips:"请写备注",
							titleValue:"4"
						}
					]
				}
				var data = template('modelContent',myData);
				document.getElementById('allModel').innerHTML = data;
				
				//禁用滚动条
				document.getElementsByTagName('body')[0].addEventListener('touchmove', function (e) {
				  e.preventDefault();
				});
				
				//自定义滚动条
				var deceleration = mui.os.ios ? 0.003 : 0.0009;
				$('.mui-scroll-wrapper').scroll({
					bounce: true,
					indicators: true, //是否显示滚动条
					deceleration: deceleration
				});
				
				//弹窗
				$.ready(function() {
					//活动城市
					var cityPicker = new $.PopPicker();
					cityPicker.setData(cityData);
					var showCityPickerButton = doc.getElementById('activeCity');
					showCityPickerButton.addEventListener('tap', function(event) {
						cityPicker.show(function(items) {
							document.querySelector('#activeCity .result-tips').style.display = "none";
							document.querySelector('#activeCity .show-result').innerText = items[0].text;
							jQuery("#cityId").val(items[0].value);
							//返回 false 可以阻止选择框的关闭
							//return false;
						});
					}, false);
				});
			})(mui,document);
		</script>
	</body>
</html>
