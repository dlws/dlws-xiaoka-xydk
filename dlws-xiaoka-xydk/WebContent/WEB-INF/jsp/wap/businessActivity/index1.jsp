<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
	    <meta content="yes" name="apple-mobile-web-app-capable" />
	    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
	    <meta content="telephone=no" name="format-detection" />
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/mui.min.css"/>
	    <link rel="stylesheet" type="text/css" href="${path}/wapstyle/css/style.css"/>
		<title>校咖汇</title>
	</head>
	<body id="allModel">
		<script src="${path}/wapstyle/js/mui.min.js"></script>
		<script src="${path}/wapstyle/js/template.js"></script>
		<script id="modelContent" type="text/html">
			<nav class="foot-bar-tab">
				<a class="foot-tab-item foot-active" href="index.html">
					<span class="foot-icon foot-icon-home"></span>
					<span class="foot-tab-label">首页</span>
				</a>
				<a class="foot-tab-item" href="publish.html">
					<span class="foot-icon foot-icon-pub"></span>
					<span class="foot-tab-label">发布</span>
				</a>
			</nav><!--底部条-->
			<div class="mui-content mui-scroll-wrapper">
				<div class="mui-scroll">
					<header class="header-bar">
						<div class="status-bar">
							<div class="come-num-box">
								<span class="come-num">
									<b>
										{{each numList as value i}}
											<i>{{value}}</i>
										{{/each}}
										<i>{{comeNum}}</i>
									</b>
								</span>
								<span class="come-font">{{comeFont}}</span>
							</div>
							<span class="mui-icon mui-icon-search"></span>
						</div><!--顶部状态栏-->
						<form class="mui-input-row mui-search search-bar">
							<input type="search" class="mui-input-clear" placeholder="搜索用户，技能">
						</form>
						<!--搜索栏-->
					</header><!--头部搜索栏-->
					
					<div id="slider" class="mui-slider">
						<div class="mui-slider-group mui-slider-loop">
							<div class="mui-slider-item mui-slider-item-duplicate">
								<a href={{banner[banner.length-1].link}}>
									<img src={{banner[banner.length-1].route}}/>
								</a>
							</div><!--最后一张图利-->
				    		{{each banner as value i }}
				    			<div class="mui-slider-item">
									<a href={{banner[i].link}}>
										<img src={{banner[i].route}}/>
									</a>
								</div>
				    		{{/each}}
				    		<div class="mui-slider-item mui-slider-item-duplicate">
								<a href={{banner[0].link}}>
									<img src={{banner[0].route}}/>
								</a>
							</div><!--第一张图片-->
						</div>
						<div class="mui-slider-indicator">
							{{each banner as value i}} {{if i==0}}
								<div class='mui-indicator mui-active'></div>
							{{/if}} {{if i>0}}
								<div class='mui-indicator'></div>
							{{/if}}{{/each}}
						</div>
					</div><!--焦点图-->
					
					<div id="Gallery" class="mui-slider gallery">
						<div class="mui-slider-group" id = "skillClass">
						<div class="mui-slider-group">
							{{each sort as value i}}
								<div class="mui-slider-item">
									<ul class="mui-table-view mui-grid-view mui-grid-9">
										{{each sort[i].classItem as value i}}
											<li class="mui-table-view-cell mui-media mui-col-xs-3 mui-col-sm-3">
												<a href="${path}/home/getClassList.html?fid={{value.id}}">
													<span class="mui-icon"><img src={{value.imageURL}}></span>
													<div class="mui-media-body">{{value.className}}</div>
												</a>
											</li>
										{{/each}}
									</ul>
								</div>
							{{/each}}
						</div>	
						</div>
						<div class="mui-slider-indicator">
							{{each sort as value i}}
								{{if i==0}}
									<div class='mui-indicator mui-active'></div>
								{{/if}} {{if i>0}}
									<div class='mui-indicator'></div>
								{{/if}}
							{{/each}}
						</div>
					</div><!--快捷入口-->
					{{each module as value i}} {{if i==0}}
						<div class="class-module">
							<h1>
								<span class="class-module-new">{{value.title}}</span>
								<div class="line"></div>
							</h1><!--标题-->
							<div id="sliderSegmentedControl" class="mui-scroll-wrapper mui-slider-indicator mui-segmented-control mui-segmented-control-inverted">
								<div class="mui-scroll">
									{{each module[i].list as value i}}
										<a class="mui-control-item" href="${path}/home/getUserInfo.html?id={{value.id}}">
											<img src="http://xiaoka2016.oss-cn-qingdao.aliyuncs.com/xydk/1476434449823000466.jpg" />
											<div class="item-info">
												<p>{{value.userName}}</p>
												<p>{{value.userName}}</p>
												<p>{{value.schoolName}}</p>
											</div>
										</a>
									{{/each}}
								</div>
							</div><!--列表-->
						</div><!--最新大咖-->
					{{/if}} {{if i==1}}
						<div class="class-module">
							<h1>
								<span class="class-module-hot">{{value.title}}</span>
								<div class="line"></div>
							</h1><!--标题-->
							 <ul class="mui-table-view mui-grid-view" id="loadList">
			    				{{each module[i].list as value i}}
									<li class="mui-table-view-cell mui-media mui-col-xs-4">
							            <a href="${path}/home/getUserInfo.html?id={{value.id}}">
											<img src="http://xiaoka2016.oss-cn-qingdao.aliyuncs.com/xydk/1476434449823000466.jpg" />
											<div class="item-info">
												<p>{{value.userName}}</p>
												<p>{{value.userName}}</p>
												<p>{{value.schoolName}}</p>
											</div>
										</a>
							        </li>
							    {{/each}}
			    			</ul><!--列表--> 
			    			<button class="mui-btn load-btn">查看更多</button>
						</div><!--推荐大咖-->
					{{/if}}{{/each}}
				</div>
			</div><!--主体内容-->
		</script>
		
		<script id="loadListContent" type="text/html">
			{{each list as value i}}
				<li class="mui-table-view-cell mui-media mui-col-xs-4">
		            <a href={{list[i].link}}>
						<img src={{list[i].route}} />
						<div class="item-info">
							<p>{{list[i].className}}</p>
							<p>{{list[i].userName}}</p>
							<p>{{list[i].shcool}}</p>
						</div>
					</a>
		        </li>
		    {{/each}}
		</script>
		<script>
		
		var skillClass=${skillClass};
		var usersJson=${usersJson};
		var usersOrdJson=${usersOrdJson};
		var myData = {
				numList:["0000","1521","5214","5032","7854","0257","3541","4520","9780"],
				comeNum:"9234",
				comeFont:"名技能大咖已经入驻",
				banner:[
					{
						link:"111.html",
						route:"img/banner01.jpg"
					},{
						link:"2222.html",
						route:"img/banner02.jpg"
					},{
						link:"333.html",
						route:"img/banner03.jpg"
					}
				],
				sort:[
						skillClass
				],
				module:[
					usersJson,usersOrdJson
				]
			}

			//点击加载更多数据

			var myLoadData = {
				list:[
						{
							link:"personal_info.html",
							route:"img/pro_img.jpg",
							className:"街舞",
							userName:"丁冬丁冬",
							shcool:"湖北美术学院"
						},{
							link:"personal_info.html",
							route:"img/pro_img.jpg",
							className:"街舞",
							userName:"丁冬丁冬",
							shcool:"湖北美术学院"
						},{
							link:"personal_info.html",
							route:"img/pro_img.jpg",
							className:"街舞",
							userName:"丁冬丁冬",
							shcool:"湖北美术学院"
						},{
							link:"personal_info.html",
							route:"img/pro_img.jpg",
							className:"街舞",
							userName:"丁冬丁冬",
							shcool:"湖北美术学院"
						},{
							link:"personal_info.html",
							route:"img/pro_img.jpg",
							className:"街舞",
							userName:"丁冬丁冬",
							shcool:"湖北美术学院"
						},{
							link:"personal_info.html",
							route:"img/pro_img.jpg",
							className:"街舞",
							userName:"丁冬丁冬",
							shcool:"湖北美术学院"
						}
					]
			}
		</script><!--页面数据-->
		
		
		<script>
		
			(function($){
				mui.init();
				//页面数据
				var data = template('modelContent',myData);
				var loadData = template('loadListContent',new Object());//点击加载更多数据
				document.getElementById('allModel').innerHTML = data;
				
				//禁用滚动条
				document.getElementsByTagName('body')[0].addEventListener('touchmove', function (e) {
				  e.preventDefault();
				});
				
				//自定义滚动条
				var deceleration = mui.os.ios ? 0.003 : 0.0009;
				$('.mui-scroll-wrapper').scroll({
					bounce: true,
					indicators: true, //是否显示滚动条
					deceleration: deceleration
				});
				
				//焦点图
				var slider = mui("#slider");
				slider.slider({
					interval: 3000
				});
				
				//点击加载更多
				document.querySelector('.load-btn').addEventListener('tap',function(){
					setTimeout(function(){
						var loadData = template('loadListContent',myLoadData);//点击加载更多数据
						document.getElementById('loadList').innerHTML += loadData;
					},320);
				});
				
				//点击跳转链接
				mui('.mui-table-view-cell').on('tap','a',function(){
					var id = this.getAttribute('href');
					var href = this.href;
					mui.openWindow({
						id: id,
						url: href,
						waiting:{
							auto:false
						}
					});
				});
				
				mui('.mui-scroll').on('tap','a',function(){
					var id = this.getAttribute('href');
					var href = this.href;
					mui.openWindow({
						id: id,
						url: href,
						waiting:{
							auto:false
						}
					});
				});
				
				//搜索框出现
				document.querySelector('.status-bar .mui-icon-search').addEventListener('tap',function(){
					document.querySelector('.status-bar').style.display = "none";
					document.querySelector('.search-bar').style.display = "block";
				});
			})(mui);
		</script>
	</body>
</html>
