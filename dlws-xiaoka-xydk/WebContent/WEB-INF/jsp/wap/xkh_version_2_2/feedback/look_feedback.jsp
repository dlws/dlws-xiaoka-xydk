<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>校咖汇</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/feedback.css" />
	</head>
	<body>
		<header class="lookFeedback_head">
		    <img src="${path}/xkh_version_2.4/img/publish_banner.png" class="search_pic">
		</header>
		<section class="lookFeedback_content">
			<ul>
				<li>
					<p class="feedback_title">反馈文件标题</p>
					<span class="fedback_pic">${feedBack.title}</span>
				</li>
				<li>
					<p class="feedback_title">反馈文件连接地址</p>
					<div class="feedback_address">
						<span class="fedback_pic">${feedBack.feedbackurl}</span>
						<span>复制</span>
					</div>
				</li>
				<li>
					<p class="feedback_title">反馈描述</p>
					<span class="fedback_pic">${feedBack.describe}</span>
					<!--当没有反馈描述时候出现-->
					<!--<span class="fedback_pic">暂无反馈描述</span>-->
				</li>
			</ul>
		</section>
		<footer class="lookFeedback_foot">
			<a href="${path}/feedback/orderConfirmShow.html?orderId=${orderId}" class="feedback_green"><span class="feedback_color">确认收货</span></a>
			<a href="${path}/feedback/showReject.html?orderId=${orderId}"><span>驳回修改</span></a>
		</footer>
	</body>
</html>