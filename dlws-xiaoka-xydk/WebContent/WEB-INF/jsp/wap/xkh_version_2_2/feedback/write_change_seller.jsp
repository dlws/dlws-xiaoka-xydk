<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>拒绝修改</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/feedback.css" />
	</head>

	<body>
		<form action="${path}/feedback/addReason.html" method="post" id="formFeed">
		<div class="lookFeedback_content">
			 <!-------------------------------卖家端拒绝修改页面------------------------------>
            <div class="sellerWrite_content">
				<p class="feedback_title">请填写拒绝修改原因</p>
				<div class="textarea_change">
					<textarea class="textarea" name="reseon" placeholder="请填写拒绝修改理由，比如反馈没问题，图片清晰等等">${feedBean.reseon}</textarea>
				</div>
			</div>
			<div class="write_foot">提交</div>
		</div>
		<input type="hidden" name="orderId" value="${orderId}">
		</form>
	</body>
	
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
		  $(".write_foot").click(function(){
			  $("#formFeed").submit();
		  });
		});
	</script>
</html>
