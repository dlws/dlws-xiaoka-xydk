<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>校咖汇</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/feedback.css" />
	</head>
	<body>
		<form action="${path}/feedback/orderConfirm.html" method="post" id="formFeed"> 
			<input type="hidden" value="${orderId}" name="orderId">
			<ul class="lookFeedback_content">
				<li>
					<span class="receipt_title">服务效率</span>
					<div class="star_content" id="service_efficiency">
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
					</div>
				</li>
				<li>
					<span class="receipt_title">服务质量</span>
					<div class="star_content" id="service_quality">
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
					</div>
				</li>
				<li class="recepit_bottom">
					<span class="receipt_title" >服务态度</span>
					<div class="star_content" id="service_attitude">
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
						<span class="star"></span>
					</div>
				</li>
				<li class="bottom_none">
					<span class="star_numer choose_pic"></span>
					<span class="star_num">全五分</span>
				</li>
			</ul>
			<div class="bg"></div>
			<div class="lookFeedback_content">
				<p class="feedback_title">服务评价</p>
				<div class="textarea_change">
					<textarea class="textarea" name="evaluate" placeholder="本次服务您还满意吗？有什么服务感受分享给其他用户吧"></textarea>
				</div>
				<div class="write_foot receipt_foot">提交</div>
			</div>
			<!-- 服务效率  default : 0 -->
			<input type="hidden" name="efficiencyGrade" value="0" id="efficiencyGrade">
			<!-- 服务质量 default : 0 -->
			<input type="hidden" name="qualityGrade" value="0" id="qualityGrade">
			<!-- 服务态度 default : 0 -->
			<input type="hidden" name="attitudeGrade" value="0" id="attitudeGrade">
		</form>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/Service feedback/Confirm_receipt.js" ></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
		  $(".receipt_foot").click(function(){
			  //服务效率
			  var service_efficiency = $("#service_efficiency").children(".already_star");
			  //服务质量
			  var service_quality = $("#service_quality").children(".already_star");
			  //服务态度
			  var service_attitude = $("#service_attitude").children(".already_star");
			  //用户评价 
			  $("#efficiencyGrade").val(service_efficiency.length);
			  
			  $("#qualityGrade").val(service_quality.length);
			  
			  $("#attitudeGrade").val(service_attitude.length);
			  
			  $("#formFeed").submit();
		  });
		});
	</script>
</html>
