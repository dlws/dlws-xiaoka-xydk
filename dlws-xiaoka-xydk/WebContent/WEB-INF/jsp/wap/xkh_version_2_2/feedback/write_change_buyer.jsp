<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>订单详情</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/feedback.css" />
	</head>

	<body>
		<div class="lookFeedback_content">
			<!------------------------买家端填写修改意见------------------------------>
			<form action="${path}/feedback/addSuggestion.html" method="post" id="formFeed">
			<input type="hidden" name="orderId" value="${orderId}">
			<div class="buyerWrite_content">
				<p class="feedback_title">请填写修改意见</p>
				<div class="textarea_change">
					<textarea class="textarea" name="suggestion" required="required" placeholder="请准确直接指出不足之处，比如照片不清晰，超链接失效，文件打不开等等">${feedback.suggestion}</textarea>
				</div>
			</div>
			  <!-- <input type="submit" value="提交" class="write_foot"  /> -->
				<div class="write_foot">提交</div> 
			</form>
		</div>
	</body>
	
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
		  $(".write_foot").click(function(){
			  $("#formFeed").submit();
		  });
		});
	</script>
</html>
