<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>校咖汇</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/feedback.css" />
	</head>
	<body>
		<c:choose>
			<c:when test="${!flage}">
				<form action="${path}/feedback/updateToCustInter.html" method="post" id="formFeed">
			</c:when>
			<c:otherwise>
				<form action="${path}/feedback/insertToCustInter.html" method="post" id="formFeed">
			</c:otherwise>
		</c:choose>
		
		<div class="lookFeedback_content">
			<p class="feedback_title">请填写存在争议的订单号</p>
			<input type="text" class="customer_inp" value="${orderId}" readonly="readonly"  placeholder="请输入存在争议的订单号" />
			<p class="feedback_title">请填写申诉理由</p>
			<div class="textarea_change">
				<textarea class="textarea" name="Reason" placeholder="请详细的填写申诉理由以便我们提供更好的帮助~">${custInter.reason}</textarea>
			</div>
			<div class="write_foot">提交</div>
		</div>	
		<input type="hidden" name="feedbackId" value="${feedbackId}">
		<input type="hidden" name="orderId" value="${orderId}">
		<input type="hidden" name="id" value="${custInter.id}">
		</form>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript">
		$(document).ready(function(){
		  $(".write_foot").click(function(){
			  $("#formFeed").submit();
		  });
		});
	</script>
</html>
