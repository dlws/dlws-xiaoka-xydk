<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<c:set var="falseStr" value="false"></c:set>
<c:set var="trueStr" value="true"></c:set>
<%-- 当前的seller对于订单详情中用户的操作 --%>
<c:if test="${dkOpenId eq param.openId}">
		<c:choose>
			<c:when  test="${param.orderStatus eq 1}">
				<div class="total_content none_border">
					<div class="order_content">
						<a href="javascript:void(0);" onclick="delOrder(this)">取消订单</a>
						<a href="#" onclick="toPay('${param.orderId}','${param.payMoney }')" class="green Green">立即付款</a>
						<input type="hidden" name="orderId" value="${param.orderId}">
					</div>
				</div>
			</c:when>
			<c:when  test="${param.orderStatus eq 2}">
				<div class="total_content none_border">
					<div class="order_content">
						<input type="hidden" name="orderId" value="${param.orderId}">
						<a href="javascript:void(0);" class="cfz_border" onclick="refuse(this)">取消订单</a>
					</div>
				</div>
			</c:when>
			<c:when test="${param.orderStatus eq 3}">
				<div class="buyerContent">
					<c:if test="${param.sellerRefuse eq 1}"><%--此处应该判定 当前最新的订单买家是否有驳回的现象 判定后做 --%>
						<div class="selected_bg accurate_bg border_none"></div>
						<%--卖家拒绝修改出现，卖家同意修改不出现--%>
						<div class="reject_content">
							<p class="reject">卖家拒绝修改原因</p>
							<span class="reject_substance">${param.reseon }</span>
						</div>
					</c:if>
					<%--卖家拒绝修改出现卖家同意修改不出现完--%>
					<div class="reject_total">
						<a href="${param.path}/feedback/orderConfirmShow.html?orderId=${param.orderId}" class="green Green">确认收货</a>
						<a href="${param.path}/feedback/showFeedBackBuyer.html?orderId=${param.orderId}">查看反馈</a>
						<c:if test="${param.feedbackstatus eq 1}">
							<span class="cfz_red"></span>
						</c:if>
						<c:if test="${param.sellerRefuse eq 1}">
							<a href="${param.path}/feedback/jumpCustInter.html?orderId=${param.orderId}&feedbackId=${param.id}">客服介入</a>
						</c:if>
					</div>
				</div>
			</c:when>
			<c:when test="${param.orderStatus eq 0}">
					<div class="total_content none_border">
						<div class="order_content">
							<a href="javascript:void(0);" onclick="refund(this)">申请退款</a>
							<a href="javascript:void(0);" onclick="accept(this)" class="green Green">确认收货</a>
							<input type="hidden" name="orderId" value="${param.orderId}">
						</div>
					</div>
			</c:when>
			<c:when test="${param.orderStatus eq 12}">
			   <div class="total_content none_border">
					<div class="order_content">
						<a href="${param.path}/feedback/jumpCustInterRefund.html?orderId=${param.orderId}&feedbackId=${param.id}">客服介入</a>
						<a href="javascript:void(0);"  class="green Green" onclick="accept(this)">取消退款</a>
						<input type="hidden" name="orderId" value="${param.orderId}">
					</div>
			    </div>  	
			</c:when>
			
		</c:choose>
</c:if>
