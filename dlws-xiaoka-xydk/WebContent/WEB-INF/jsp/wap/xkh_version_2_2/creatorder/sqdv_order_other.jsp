<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.picker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.dtpicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style1.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/iconfont/iconfont.css" />
		<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
		<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
		<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
		<title>校咖—社群大V—${skillInfo.sonName}</title>
	</head>
	<body>
	<form action="<%=basePath%>skillOrder/subOrder.html" id="OrderForm" method="post">
		<div class="publish_case">
				<div class="publish_content none_border">
				<div class="publish_title">
					<span class="title_pic"><img src="${baseInfo.headPortrait}" class="search_pic" /></span>
					<input type="hidden" name="sellerHeadPortrait" value="${baseInfo.headPortrait}">
					<h3>${baseInfo.nickname}</h3>
					<input type="hidden" name="sellerNickname" value="${baseInfo.nickname}">
					<input type="hidden" name="sellPhone" value="${baseInfo.phoneNumber}">
				</div>
			</div>
		<div class="substance_content border">
				<div class="substance_left"><img src="${skillImg}" class="search_pic" /></div>
				<input type="hidden" name="imageUrl" value="${skillImg}">
				<div class="substance_Right">
					<span class="substance_Music">${skillInfo.skillName}</span>
					<input type="hidden" name="serviceName" value="${skillInfo.skillName}">
					<c:if test="${skillInfo.serviceType==1}">
						<!-- <span class="substance_Line">线上</span> -->
						<span class="substance_Line"><span>线上</span></span>
					</c:if>
					<c:if test="${skillInfo.serviceType==2}">
					
						<span class="substance_Line"><span>线下</span></span>
					</c:if>
					<input type="hidden" name="serviceType" value="${skillInfo.serviceType}">
					<p>${skillInfo.skillDepict}</p>
					<input type="hidden" name="skillDepict" value="${skillInfo.skillDepict}">
					<div class="add_Price">
					<span class="substance_price">${detailMap.skillPrice}</span><span class="substance_price">元/次</span>
					 </div>
					<input type="hidden" name="skillPrice" value="${detailMap.skillPrice}">
                    <span class="sell_Num">[已售${skillInfo.sellNo}]</span>
				</div>

			</div>
		</div>
			<!--主内容区-->
			<ul class="accurate_content">
			<li>
					<span class="accurate-title">购买数量</span>
					<div class="placeOrder_num">
						<span class="jian"><img src="${path}/xkh_version_2.2/img/jian.png" class="search_pic"></span>
						<input placeholder="${buyNum}" name="tranNumber" id="num" value="${buyNum}">
						<span class="add"><img src="${path}/xkh_version_2.2/img/add.png" class="search_pic"></span>
					</div>
				</li>
				<li>
					<span class="accurate_title">投放内容</span>
					<span class="accurate-title">标&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;题</span>
					<input type="text"  placeholder="请输入投放标题" class="accurate_inp" name="title" id="title" value="" />
				</li>
				<li>
					<span class="accurate-title">文&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本</span>
				<div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce" onkeyup="load()" onkeydown="load()" name="textContent" id="textContent" placeholder="请输入文本内容"></textarea>
					</div>
					<div id="span" class="Span"><span>0</span>/200</div>
				</div>
				<span class="accurate-title accurate_title2">图片<span class="choose_title">(&nbsp;选填&nbsp;)</span></span>
				<!--添加的图片-->
				<div class="webservice_content">
					<!--添加的图片-->
					<div class="picture_box" id="picture_box">
					
					
					</div> 
					<!--<添加的图片完-->
					
					<div class="add_pic">
						<span class="iconfont icon-pic"></span>
						<span class="size" onclick="uploadImage();">+添加图片</span>
					</div>
				</div>
				<span class="accurate-title accurate_title2">链接<span class="choose_title">(&nbsp;选填&nbsp;)</span></span>
				<input type="text" placeholder="请输入链接地址" name="linke" id="linke" class="accurate_inp"  />
				</li>
				<li>
					<span class="accurate-title accurate_title2">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
					<input type="text"  placeholder="请填写您的姓名" name="linkman" id="linkman" class="accurate_inp"  />
				</li>
				<li>
					<span class="accurate-title accurate_title2">联系电话</span>
					<input type="text"  placeholder="请填写您的联系电话" name="phone" id="phone" class="accurate_inp"  />
					<p class="warn_tel"></p>
				</li>
				<li>
					<span class="accurate-title accurate_title3">买家留言<span class="choose_title">(&nbsp;选填&nbsp;)</span></span>
				   <div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce Introduce2" name="message" onkeyup="load2()" onkeydown="load2()"  placeholder="请输入文本内容"></textarea>
					</div>
					<div id="span2" class="span"><span>0</span>/200</div>
				</div>
				</li>
			</ul>
			<div class="substance_Bg"></div>
			<div class="accurate_time">
				<span>投放时间&nbsp;:&nbsp;</span>
				 <input type="hidden" name="endDate" id="endDate" value="2017-10-01 10:00">
				<span data-options='{}' class="green Btn  mui-btn mui-btn-block" id="result" style="width:40vw">2017-10-01&nbsp;&nbsp;10:00</span>
				
				<!-- <span>2016-02-10&nbsp;10:00</span>
				<input type="hidden" name="endDate" id="endDate" value=""> -->
				<span>投放内容需经平台审核，投放时间必须在48小时以后</span>
			</div>
			<div class="accurate_total">
				<span>合计&nbsp;:&nbsp;</span>
				<span id="suMoney"><fmt:formatNumber type="number"  pattern="0.0" maxFractionDigits="2" value="${detailMap.skillPrice*buyNum}" />元</span>
				<input type="hidden" id="payMoney" name="payMoney" value="${detailMap.skillPrice*buyNum}">
			</div>
			<div class="substance_Bg"></div>
			<input type="hidden" name="enterType" value="${skillInfo.fathValue}">
			<input type="hidden" name="serviceRatio" value="${skillInfo.serviceRatio}">
			<input type="hidden" name="diplomatRatio" value="${skillInfo.diplomatRatio}">
			<input type="hidden" name="platformRatio" value="${skillInfo.platformRatio}">
			<input type="hidden" name="sellerOpenId" value="${baseInfo.openId}">
			<input type="hidden" name="picUrl" id="picUrl" value="${basicInfo.imgString}">
			<input type="hidden" name="skillId" value="${skillInfo.id}">
			<input type="hidden" name="cityId" value="${baseInfo.cityId}">
			<input type="hidden" name="schoolId" value="${baseInfo.schoolId}">
			<input type="hidden" name="tranNumber" value="${buyNum}">
			<input type="hidden" name="dic_name" value="次">
			<div class="footer" onclick="sub()"><span>提交订单</span></div>
		</form>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.picker.min.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.dtpicker.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/common_rule.js"></script>
	<script>
	function sub(){
		
		var phoneNumber=$("#phone").val()
		var re=/^1[3|5|7|8]\d{9}$/;//更改手机号验证问题
		if(phoneNumber==""){
			alert("请输入手机号");
			return;
		}else if(!re.test(phoneNumber)){
			alert("请输入正确的手机号");
			return;
		}
		uploadPic();
		//$("#OrderForm").submit();
	}
	var tfTime="";
	//时间选择
	(function($) {
				$.init();
			     var now=new Date();
			    var obj1 = opt()
			    var time = document.getElementById("result");
			    time.innerText = obj1.beginYear + "-" +(obj1.beginMonth >=10 ? obj1.beginMonth : "0" + obj1.beginMonth ) + "-" + obj1.beginDay +"  "+obj1.beginHour+":"+obj1.beginMinutes;             
			    var obj = getTime([obj1.beginYear,obj1.beginMonth,obj1.beginDay])
                var later48 = opt( (obj1.beginYear + "-" +(obj.month >=10 ? obj.month : "0" + obj.month ) + "-" + obj.day +"  "+obj.hour+":"+obj.second),obj1.beginYear,obj.month,obj.day,obj.hour,obj.second)
		console.log(later48)
        time.setAttribute('data-options',JSON.stringify(later48));
			    var result = $('#result')[0];
				var btns = $('.Btn');
			btns.each(function(i, btn) {
					btn.addEventListener('tap', function() {
						var optionsJson = this.getAttribute('data-options') || '{}';
						var options = JSON.parse(optionsJson);
						var id = this.getAttribute('id');
						
						 var today = new Date();
						 var picker = new $.DtPicker(options);
						picker.show(function(rs) {
							result.innerText = rs.text;
							tfTime=rs.text;
					changeTime(tfTime)
							picker.dispose();
						});
						
					}, false);
				})
			})(mui);
	
	
		 function changeTime(tfTime){
			$("#endDate").val(tfTime);
		 }
		 function getTime(arr) {
				//arr 的格式是["2017","06","29"]

				// 是不是闰年
				var year = 0;
				// 记录加48小时后的日期
				var day = 0;
				// 记录当前的月份
				var month = 0;
				// 一月是 31 天的数组
				var day31 = [1,3,5,7,8,10,12];
				// 一月30天的数组
				var day30 = [4,6,9,11];
				var hour=new Date().getHours();
				var second=new Date().getMinutes();
				// 判断是不是闰年
			    function isLeapYear(year) {
			    	return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
			    }
			    // 闰年二月要加1
			    year = isLeapYear(parseInt(arr[0])) ? 1 : 0;

				if($.inArray(parseInt(arr[1]),day31) != -1){
					day = parseInt(arr[2]) + 2 > 31 ? (parseInt(arr[2])+2 - 31) : parseInt(arr[2]) + 2;
			        month = parseInt(arr[2]) + 2 > 31 ? parseInt(arr[1]) +1 : parseInt(arr[1])
			    }else if($.inArray(parseInt(arr[1]),day30) != -1){
					day = parseInt(arr[2]) + 2 > 30 ? (parseInt(arr[2])+2 - 30) : parseInt(arr[2]) + 2;
			        month = parseInt(arr[2]) + 2 > 30 ? parseInt(arr[1]) +1 : parseInt(arr[1])
				}else{
			        if(year){
			        	day = parseInt(arr[2]) + 2 + year > 29 ? parseInt(arr[2]) + 2 + year - 29 : parseInt(arr[2]) +2
			            month = parseInt(arr[2]) + 2 + year > 29 ? parseInt(arr[1]) +1 : parseInt(arr[1])
					}else{
			            day = parseInt(arr[2]) + 2 + year > 28 ? parseInt(arr[2]) + 2 + year - 28 : parseInt(arr[2]) +2
			            month = parseInt(arr[2]) + 2 > 28 ? parseInt(arr[1]) +1 : parseInt(arr[1])
					}
				}

				// 将日给处理成需要的格式
			    day = day >= 10 ? day : "0" + day
			    		return {month:month,day:day,hour:hour,second:second};
			}

			// 格式化时间选择器的配置
			function opt(value,year,mouth,day,hour,second) {
				
			    var obj = {
			        type:"datetime"
			    }
			    
			    if(arguments[1]){
			    	
			        obj.value = value;
					obj.beginYear=year;
					obj.beginMonth=parseInt(mouth);
			        obj.beginDay=parseInt(day);
			        obj.beginHours=parseInt(hour);
					obj.beginMinutes=parseInt(second);
			       
				}else{
			    	var now = new Date();
			    	
			        obj.beginYear=now.getFullYear();
			        obj.beginMonth=now.getMonth()+1;;
			        obj.beginDay=now.getDate() >= 10 ? now.getDate() : "0" + now.getDate();
			        obj.beginHour=now.getHours() >= 10 ? now.getHours() : "0" + now.getHours();
				       obj.beginMinutes=now.getMinutes() >= 10 ? now.getMinutes() : "0" + now.getMinutes();
				        obj.value = obj.beginYear + "-" + obj.beginMonth + "-" + obj.beginDay+"  "+ obj.beginHour+":"+ obj.beginMinutes
			        
				}


			    return obj;
			}
	
	
		
		 function load(){
      	 var length=$(".Introduce").val().length;
      	 var html=$(".Introduce").val();
      	 
      	 if(length>199){
      	 $(".Introduce").val(html.substring(0,200))
      	 	
      	 	 $("#span span").html(200);
      	 	 
      	 }else{
      	 	 $("#span span").html(length)
      	 }
      	
     }
	 function load2(){
      	 var length=$(".Introduce2").val().length;
      	 var html=$(".Introduce2").val();
      	 
      	 if(length>199){
      	 $(".Introduce2").val(html.substring(0,200))
      	 	
      	 	 $("#span2 span").html(200);
      	 	 
      	 }else{
      	 	 $("#span2 span").html(length)
      	 }
      	
     }	 
	 
		//微信上传 全局变量
		var wxSelf;
		var localActivityArr = new Array();//存放活动的图片
		
		var serverActivityArr = new Array();//存放活动的图片
	 	/*001 选择图片 */
		function uploadImage() {
			var clientUrl = window.location.href;
			var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
			//请求后台，获取jssdk支付所需的参数
			$.ajax({
				type : 'post',
				url : reqPath,
				dataType : 'json',
				data : {
					"clientUrl" : clientUrl
					//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
				},
				cache : false,
				error : function() {
					alert("系统错误，请稍后重试");
					return false;
				},
				success : function(data) {
					//微信支付功能只有微信客户端版本大于等于5.0的才能调用
					var return_date = eval(data);
					/* alert(return_date ); */
					if (parseInt(data[0].agent) < 5) {
						alert("您的微信版本低于5.0无法使用微信支付");
						return;
					}
					//JSSDK支付所需的配置参数，首先会检查signature是否合法。
					wx.config({
						debug : false, //开启debug模式，测试的时候会有alert提示
						appId : return_date[0].appId, //公众平台中-开发者中心-appid
						timestamp : return_date[0].config_timestamp, //时间戳
						nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
						signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
						jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
					});
					//上方的config检测通过后，会执行ready方法
					wx.ready(function() {
						wxSelf=wx;
						wx.chooseImage({
							count: 9, // 默认9
							sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
							sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
							success: function (res) {
									localActivityArr=appandArray(localActivityArr,res.localIds);
									if(localActivityArr.length > 0){
										var result = "";
								        for(var j = 0; j < res.localIds.length; j++){
								        	result += "<div id='pic_"+j+"' class='add_picture'>"+
													"<img src='"+res.localIds[j]+"' class='search_pic' />"+
														"<span onclick='removeImgActiv("+j+","+res.localIds[j]+")' class='iconfont icon-shanchu' ></span>"+
													"</div>";
								        	}
								        var imgNum = $("#picture_box img").length;
								        var picNum =  parseInt(imgNum) + parseInt(localActivityArr.length);
								        if(picNum>9){
								        	alert("图片最多能上传9张！");
								        }else{
								        	$("#picture_box").append(result);
								        } 
									}
							}
						 });

						});
						 wx.error(function(res) {
							alert(res.errMsg);
						});
					}
			});
		}		
		
		
		
		/* 点击提交后上传到微信*/
		/*使用递归的方式进行上传图片*/
		function uploadPic() {
         if (localActivityArr.length == 0) {
         	var serverStr="";
         	if(0 == serverActivityArr.length ){
         		$("#OrderForm").submit();
         	}else{
		            for(var j=0;j<serverActivityArr.length;j++){
		            	serverStr+=serverActivityArr[j]+";" ;
		            }
	            	
	                $.ajax({
		        		type : "post",
		        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
		        		dataType : "json",
		        		data : {serverId:serverStr},
		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
		        			var imageList = data;
		        			var imgPath="";
							for(var f=0;f<imageList.length;f++){
						//		alert("+++++++++++++++:"+imageList[f]);
								var imgPathTemp = imageList[f];
								imgPath += imgPathTemp+",";
							}
							
							var u = $("#picUrl").val()+imgPath;
							$("#picUrl").val(u)
		        			$("#OrderForm").submit();
							$(".webservice_mask").removeClass("mask_none");
		        		}
		        	});	
         	}
         }
         var localId = localActivityArr[0];
         //tmd 一定要加     解决IOS无法上传的坑 
         if (localId.indexOf("wxlocalresource") != -1) {
             localId = localId.replace("wxlocalresource", "wxLocalResource");
         }
         wxSelf.uploadImage({
             localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
             isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
             success: function (res) {
                // serverIds.push(res.serverId); // 返回图片的服务器端ID
                var serverId = res.serverId; // 返回图片的服务器端ID
                serverActivityArr.push(serverId);
                 localActivityArr.shift();
                 uploadPic();
                 serverStr+=serverId+";" ;
             },
             fail: function (res) {
                 alert("上传失败，请重新上传！");
             }
         });
     }
		
		function appandArray(a,b){

			for(var i=0;i<b.length;i++){
				 a.push(b[i]);
			}
			return a;
		}
		
		
		
		function removeImgActiv(imageId,val){
			$("#pic_"+imageId).remove();
			//localActivityArr.remove(val);
		}
		
		//点击删除活动图片
		function delImgActivtyUrl(imgId,val,obj){
			if(confirm("确定删除?")){
					//表示修改需要数据库删除
					$.ajax({
						type : "post",
						url : "<%=basePath%>highSchoolSkill/delImage.html",
						dataType : "json",
						data : {imgeId:imgId,imageUrl:val},
						success : function(data) {
							if(data.flag){
								$("#picUrl").val(data.typeValue);
								obj.parentElement.remove();
							}
						}
					});
		  }
	   }
		//关于加减的动作
		//当是默认1时候
		var t = $("#num").attr('placeholder');
		document.getElementById("num").value=t;
//		console.log(document.getElementById("num").value)
		//当不是placeholder时候
		$(".add").click(function() {
			//获取当前合计的钱数
				var suMoney = $("#suMoney").html();
				suMoney.replace('元','');
                t=document.getElementById("num").value;
                var singel = parseFloat(suMoney)/t;
				t=parseInt(t)+1;
				$("#num").attr("placeholder",t);
				$(".add").removeAttr("disabled");
				document.getElementById("num").value=t;
				$("#tranNumber").val(t);
				$("#payMoney").val(singel*t);
				$("#suMoney").html(singel*t+"元");
		})
		
		$(".jian").click(function() {
				var suMoney = $("#suMoney").html();
				suMoney.replace('元','');
				var singel = parseFloat(suMoney)/t;
				t=parseInt(t)-1;
				if(document.getElementById("num").value > 1) {
					t=document.getElementById("num").value;
					t=parseInt(t)-1;
					$("#num").attr("placeholder",t);
					document.getElementById("num").value=t;
					$("#payMoney").val(singel*t);
					$("#suMoney").html(singel*t+"元");
					$("#tranNumber").val(t);
				} else {
					$(".jian").attr("disabled", "disabled")
				}
				
		})
		
		
		
		
		
	</script>
</html>
