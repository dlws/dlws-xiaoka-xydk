<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.picker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.dtpicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style1.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/iconfont/iconfont.css" />
		<title>高校社团-下单</title>
	</head>

	<body>
		<!--说明高校社团和技能达人一样-->
		<div class="publish_case">
			<div class="publish_content none_border">
				<div class="publish_title">
					<span class="title_pic"><img src="${selUserInfo.headPortrait }" class="search_pic" style="border-radius:50%;" /></span>
					<h3>${selUserInfo.nickname }</h3>
				</div>
			</div>
			<div class="substance_content border">
			<%-- 更改图片展示 --%>
				<div class="substance_left"><img src="${skillImg}" class="search_pic" /></div>
				<div class="substance_Right">
					<span class="substance_Music">${skillSonMap.skillName }</span>
					<span class="substance_Line">
						<span>
							<c:if test="${skillSonMap.serviceType==1 }">线上</c:if>
							<c:if test="${skillSonMap.serviceType==2 }">线下</c:if>
						</span>
					</span>
					<span class="orderDetail_info">${skillSonMap.skillDepict }</span>
					<div class="add_Price">
					<span class="substance_price">${detailMap.skillPrice }</span><span class="substance_price">元/${unit }</span>
					</div>
					<span class="sell_Num">[已售${skillSonMap.sellNo }]</span>
				</div>
			</div>
		</div>
		<form id="gxstform" action="${path }/placeHighSchoolSkillOrder/insertHighSchoolSkillOrder.html" method="post">
			<ul class="accurate_content">
				<li>
					<span class="accurate-title">购买数量</span>
					<div class="placeOrder_num">
						<span class="jian"><img src="${path}/xkh_version_2.2/img/jian.png" class="search_pic" /></span>
						<input placeholder="${buyNum}" name="tranNumber" id="num" value="${buyNum}"/>
						<span class="add"><img src="${path}/xkh_version_2.2/img/add.png" class="search_pic" /></span>
					</div>
				</li>
				<li>
					<span class="accurate-title">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
					<input placeholder="请填写您的姓名" class="accurate_inp" id="linkman" name="linkman" value="" />
				</li>
				<li>
					<span class="accurate-title">联系电话</span>
					<input placeholder="请填写您的联系电话" class="accurate_inp" id="phone" name="phone" value="" />
					<p class="warn_tel"></p>
				</li>
				<!--这一部分校内商家和社群大V下单没有-->
				<li>
					<span class="accurate-title">需求描述</span><!-- 存入字表 -->
					<div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce placeOrder_mes" name="aboutNeed" id="aboutNeed" onkeyup="load()" onkeydown="load()" placeholder="请简要描述所需服务地点，服务具体内容及所需达成的目标吧!"></textarea>
					</div>
					<div id="span" class="Span"><span>0</span>/200</div>
				</div>
				</li>
				<!--这一部分校内商家和社群大V下单没有完-->
				<li>
					<span class="accurate-title placeOrder_word">买家留言<span class="choose_title">(&nbsp;选填&nbsp;)</span></span>
					<div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce placeOrder_mes Introduce2" name="message" onkeyup="load2()" onkeydown="load2()" placeholder=""></textarea>
					</div>
					<div id="span2" class="Span"><span>0</span>/200</div>
				</div>
				</li>
				<li>
				<!-- 卖家信息 -->
				<input type="hidden" name="sellerOpenId" value="${selUserInfo.openId }">
				<input type="hidden" name="sellerHeadPortrait" value="${selUserInfo.headPortrait }">
				<input type="hidden" name="sellerNickname" value="${selUserInfo.nickname }">
				<input type="hidden" name="schoolId" value="${selUserInfo.schoolId }">
				<input type="hidden" name="cityId" value="${selUserInfo.cityId }">
				<input type="hidden" name="sellPhone" value="${selUserInfo.phoneNumber }">
				<!-- 服务信息 -->
				<input type="hidden" name="skillId" value="${skillSonMap.id }">
				<input type="hidden" name="imgUrl" value="${skillImg}"><%-- 更改当前图片路径 --%>
				<input type="hidden" name="skillName" value="${skillSonMap.skillName }">
				<input type="hidden" name="serviceType" value="${skillSonMap.serviceType }">
				<input type="hidden" name="skillDepict" value="${skillSonMap.skillDepict }">
				<input type="hidden" name="skillPrice" value="${detailMap.skillPrice }">
				<input type="hidden" name="dic_name" value="${unit}">
				<%-- <input type="hidden" name="sellNo" value="${skillMap.sellNo }"> --%>
				<input type="hidden" name="enterType" value="${skillSonMap.fathValue}">
				
				<input type="hidden" name="payMoney" id="payMoney" value="${detailMap.skillPrice }">
				
				<input type="hidden" name="serviceRatio" id="" value="${skillSonMap.serviceRatio }">
				<input type="hidden" name="diplomatRatio" id="" value="${skillSonMap.diplomatRatio }">
				<input type="hidden" name="platformRatio" id="" value="${skillSonMap.platformRatio }">
				
				<input type="hidden" name="startDate" id="startDate" value="">
				<input type="hidden" name="endDate" id="endDate" value="">
				</li>
			</ul>
			<div class="selected_bg accurate_bg"></div>
			<!--服务下单出现-->
			<div class="accurate_box">
			<div class="info-content info-content2">
				<span class=" accurate_left">服务时间&nbsp;:&nbsp;</span>
				<span data-options='{}' class="green Btn1 mui-btn mui-btn-block" id="resultStart">2017-01-01&nbsp;00:01&nbsp;</span>
				<span>&nbsp;-&nbsp;</span>
				<span data-options='{}' class="green Btn2 mui-btn mui-btn-block" id="resultEnd">2017-01-01&nbsp;00:01&nbsp;</span>
			</div>
			<div class="accurate_total">
				<span>合计&nbsp;:&nbsp;</span>
				<span id="zj">
					<fmt:formatNumber type="number"  pattern="0.0" maxFractionDigits="2" value="${detailMap.skillPrice*buyNum}" />
				</span>
				<span >元</span>
			</div>
			<div class="selected_bg accurate_bg"></div>
				<div class="footer"><span onclick="sub()">提交订单</span></div>
			</div>
		</form>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.picker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.poppicker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.dtpicker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/enterInfo.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/common_rule.js"></script>
	<script>
		function sub(){
			var linkman= $.trim($("#linkman").val());
			var phone= $.trim($("#phone").val());
			var aboutNeed= $.trim($("#aboutNeed").val());
			var startDate= $.trim($("#startDate").val());
			var endDate= $.trim($("#endDate").val());
			if(linkman==""){
				alert("请输入联系人姓名!")
				return;
			}
			var phoneNumber=$("#phone").val()
			var re=/^1[3|5|7|8]\d{9}$/;//更改手机号验证问题
			if(phoneNumber==""){
				alert("请输入手机号");
				return;
			}else if(!re.test(phoneNumber)){
				alert("请输入正确的手机号");
				return;
			}
			if(aboutNeed==""){
				alert("请输入需求描述!")
				return;
			}
			if(startDate==""){
				alert("请选择服务开始时间!")
				return;
			}
			if(endDate==""){
				alert("请选择服务结束时间!")
				return;
			}
			$("#gxstform").submit();
		}
	
		//当是默认1时候
		var t = $("#num").attr('placeholder');
		console.log(t)
		document.getElementById("num").value=t;
//		console.log(document.getElementById("num").value)
		//当不是placeholder时候
		$(".add").click(function() {
                t=document.getElementById("num").value;
				t=parseInt(t)+1;
				$("#num").attr("placeholder",t)
				var a=document.getElementById ("zj");
				var p=${detailMap.skillPrice};
				a.innerHTML = t*p;
				$("#num").attr("value",t)
				$("#payMoney").attr("value",a.innerHTML)
				
				$(".add").removeAttr("disabled");
				document.getElementById("num").value=t;
			})
		
		$(".jian").click(function() {
				if(document.getElementById("num").value > 1) {
					t=document.getElementById("num").value;
					t=parseInt(t)-1;
					var a=document.getElementById ("zj");
					var p=${skillSonMap.skillPrice };
					a.innerHTML = t*p;
					$("#num").attr("value",t)
					$("#payMoney").attr("value",a.innerHTML)
					document.getElementById("num").value=t;
				} else {
					$(".jian").attr("disabled", "disabled")
				}
			})

		function load() {
			var length = $(".Introduce").val().length;
			var html = $(".Introduce").val();

			if(length > 199) {
				$(".Introduce").val(html.substring(0, 200))

				$("#span span").html(200);

			} else {
				$("#span span").html(length)
			}
		}

		function load2() {
			var length = $(".Introduce2").val().length;
			var html = $(".Introduce2").val();

			if(length > 199) {
				$(".Introduce2").val(html.substring(0, 200))

				$("#span2 span").html(200);

			} else {
				$("#span2 span").html(length)
			}
		}
		
		
		//开始时间
		(function($) {
				$.init();
				var resultStart = $('#resultStart')[0];
				var btns = $('.Btn1');
				btns.each(function(i, btn) {
						btn.addEventListener('tap', function() {
							var optionsJson = this.getAttribute('data-options') || '{}';
							var options = JSON.parse(optionsJson);
							var id = this.getAttribute('id');
							/*
							 * 首次显示时实例化组件
							 * 示例为了简洁，将 options 放在了按钮的 dom 上
							 * 也可以直接通过代码声明 optinos 用于实例化 DtPicker
							 */
//							var picker = new $.DtPicker(options);
							var today = new Date();
							var picker = new mui.DtPicker({
								beginDate: new Date(today.getFullYear(), today.getMonth(), today.getDay())
							})
							picker.show(function(rs) {
								resultStart.innerText = rs.text;
								getTime(rs.text);
								picker.dispose();
							});
						}, false);
					})
				})(mui);
		function getTime(time){
			$("#startDate").val(time);
		}
		//结束时间
		(function($) {
			$.init();
			var resultEnd = $('#resultEnd')[0];
			var btns = $('.Btn2');
			btns.each(function(i, btn) {
					btn.addEventListener('tap', function() {
						var optionsJson = this.getAttribute('data-options') || '{}';
						var options = JSON.parse(optionsJson);
						var id = this.getAttribute('id');
						var today = new Date();
						var picker = new mui.DtPicker({
							beginDate: new Date(today.getFullYear(), today.getMonth(), today.getDay())
						})
						picker.show(function(rs) {
							resultEnd.innerText = rs.text;
							getEndTime(rs.text);
							picker.dispose();
						});
					}, false);
				})
			})(mui);
		function getEndTime(time){
			$("#endDate").val(time);
		}
	</script>
</html>
