<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.picker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style1.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.dtpicker.css" />
		<title>校咖-社群大V-自媒体</title>
	</head>

	<body>
	<form action="<%=basePath%>sqdvOrder/subSqdvOrder.html" id="OrderForm" method="post">
		<!--说明高校社团和技能达人一样-->
		<div class="publish_case">
			<div class="publish_content none_border">
				<div class="publish_title">
					<span class="title_pic"><img src="${baseInfo.headPortrait}" class="search_pic" style="border-radius:50%;"/></span>
					<input type="hidden" name="sellerHeadPortrait" value="${baseInfo.headPortrait}">
					<h3>${baseInfo.nickname}</h3>
					<input type="hidden" name="sellerNickname" value="${baseInfo.nickname}">
					<input type="hidden" name="sellPhone" value="${baseInfo.phoneNumber}">
				</div>
			</div>
			<div class="substance_content border">
				<div class="substance_left"><img src="${skillImg}" class="search_pic" /></div>
				<input type="hidden" name="imageUrl" value="${skillImg}">
				<div class="substance_Right">
					<span class="substance_Music">${skillInfo.skillName}</span>
					<input type="hidden" name="skillName" value="${skillInfo.skillName}">
					<c:if test="${skillInfo.serviceType==1}">
						<span class="substance_Line"><span>线上</span></span>
					</c:if>
					<c:if test="${skillInfo.serviceType==2}">
						<span class="substance_Line"><span>线下</span></span>
					</c:if>
					<input type="hidden" name="serviceType" value="${skillInfo.serviceType}">
					<span class="orderDetail_info">${skillInfo.skillDepict}</span>
					<input type="hidden" name="skillDepict" value="${skillInfo.skillDepict}">
					<div class="add_Price">
					<span class="substance_price" id="priceShow">${detailMap.lessLinePrice}~${detailMap.topLinePrice}</span><span class="substance_price">元/次</span>
					</div>
					<input type="hidden" name="skillPrice" id="skillPrice" value="${skillInfo.skillPrice}">
					<input type="hidden" name="sellNo" value="${skillInfo.sellNo}">
				</div>
			</div>
		</div>
		<ul class="accurate_content">
			<li>
				<span class="accurate-title">购买数量</span>
				<div class="placeOrder_num">
					<span class="jian"><img src="${path}/xkh_version_2.2/img/jian.png" class="search_pic" /></span>
					<input placeholder="${buyNum}" id="num" name="tranNumber" value="${buyNum}"/>
					<span class="add"><img src="${path}/xkh_version_2.2/img/add.png" class="search_pic" /></span>
				</div>
			</li>
		
			<!--社群大V下单出现的-->
			<li>
				<span class="accurate-title">投放版位</span>
				<!-- <div id="accurate" class="mui-input-row">
					<span class="accurate_inp result-tips gray">请选择投放版位</span>
					<span class="accurate_inp show-result gray"></span>
					
				</div> -->
                <!-- 06-29新换样式 -->
                <input type="hidden" name="accurateVal" id="accurateVal" value=""> 
                <div class="nature_box">
					<div class="nature">
						<span class="nature_choose"></span>
						<span class="groom_Green">头条</span>
					</div>
					<div class="nature">
						<span></span>
						<span>次条</span>
					</div>
				</div>
			</li>
			<!--社群大V下单出现的完-->
			<li>
				<span class="accurate-title">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
				<input placeholder="请填写您的姓名" name="linkman" id="linkman" class="accurate_inp" />
			</li>
			<li>
				<span class="accurate-title">联系电话</span>
				<input placeholder="请填写您的联系电话" name="phone" id="phone" class="accurate_inp" />
				<p class="warn_tel"></p>
			</li>
			
			<li>
				<span class="accurate-title placeOrder_word">参考软文链接</span>
			<div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce placeOrder_mes Introduce2" name="message" onkeyup="load2()" onkeydown="load2()" placeholder="请输入提供给运营者可直接复制的微信文章链接，同时若有其他要求可在此说明"></textarea>
					</div>
					<div id="span2" class="Span"><span>0</span>/200</div>
				</div>
			</li>
		</ul>
		<div class="selected_bg accurate_bg"></div>
		<input type="hidden" name="enterType" value="${skillInfo.fathValue}">
		<input type="hidden" name="serviceRatio" value="${skillInfo.serviceRatio}">
		<input type="hidden" name="diplomatRatio" value="${skillInfo.diplomatRatio}">
		<input type="hidden" name="platformRatio" value="${skillInfo.platformRatio}">
		<input type="hidden" name="sellerOpenId" value="${baseInfo.openId}">
		<input type="hidden" name="skillId" value="${skillInfo.id}">
		<input type="hidden" name="cityId" value="${baseInfo.cityId}">
		<input type="hidden" name="basiId" value="${baseInfo.basiId}">
		<input type="hidden" name="schoolId" value="${baseInfo.schoolId}">
		<input type="hidden" name="dic_name" value="次">
			
		<!--社群大V下单出现-->
		<div class="info-content info-content2">
			<span class=" accurate_left">投放时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
			<span data-options='{}' class="green Btn  mui-btn mui-btn-block" id="result" style="width:50vw">2017-10-03&nbsp;&nbsp;10:00</span>
			<input type="hidden" name="endDate" id="endDate" value="2017-10-03 10:00">
		</div>
		<div class="accurate_total">
			<span>合计&nbsp;:&nbsp;</span>
			<span id="suMoney">0元</span>
			<input type="hidden" id="payMoney" name="payMoney" value="${skillInfo.skillPrice}">
		</div>
		<div class="selected_bg accurate_bg"></div>
		<div class="footer" onclick="sub();"><span>提交订单</span></div>
		</form>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.picker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.poppicker.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.dtpicker.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/common_rule.js"></script>
	<script>
	function sub(){
		var accurateVal=$.trim($("#accurateVal").val());
		var linkman=$.trim($("#linkman").val());
		var phone=$.trim($("#phone").val());
		var endDate=$.trim($("#endDate").val());
		if(accurateVal==""){
			alert("请选择投放版位!")
			return;
		}
		if(linkman==""){
			alert("请输入联系人姓名!")
			return;
		}
		var phoneNumber=$("#phone").val()
		var re=/^1[3|5|7|8]\d{9}$/;//更改手机号验证问题
		if(phoneNumber==""){
			alert("请输入手机号");
			return;
		}else if(!re.test(phoneNumber)){
			alert("请输入正确的手机号");
			return;
		}
		if(endDate==""){
			alert("请选择投放时间!")
			return;
		}
		$("#OrderForm").submit();
	}
	var tfTime="";
	//时间选择
	(function($) {
				$.init();
			     var now=new Date();
			    var obj1 = opt()
			    var time = document.getElementById("result");
			    time.innerText = obj1.beginYear + "-" +(obj1.beginMonth >=10 ? obj1.beginMonth : "0" + obj1.beginMonth ) + "-" + obj1.beginDay +"  "+obj1.beginHour+":"+obj1.beginMinutes;             
			    var obj = getTime([obj1.beginYear,obj1.beginMonth,obj1.beginDay])
                var later48 = opt( (obj1.beginYear + "-" +(obj.month >=10 ? obj.month : "0" + obj.month ) + "-" + obj.day +"  "+obj.hour+":"+obj.second),obj1.beginYear,obj.month,obj.day,obj.hour,obj.second)
		console.log(later48)
        time.setAttribute('data-options',JSON.stringify(later48));
			    var result = $('#result')[0];
				var btns = $('.Btn');
			btns.each(function(i, btn) {
					btn.addEventListener('tap', function() {
						var optionsJson = this.getAttribute('data-options') || '{}';
						var options = JSON.parse(optionsJson);
						var id = this.getAttribute('id');
						
						 var today = new Date();
						 var picker = new $.DtPicker(options);
						picker.show(function(rs) {
							result.innerText = rs.text;
							tfTime=rs.text;
					changeTime(tfTime)
							picker.dispose();
						});
						
					}, false);
				})
			})(mui);
	
	
		 function changeTime(tfTime){
			$("#endDate").val(tfTime);
		 }
		 function getTime(arr) {
				//arr 的格式是["2017","06","29"]

				// 是不是闰年
				var year = 0;
				// 记录加48小时后的日期
				var day = 0;
				// 记录当前的月份
				var month = 0;
				// 一月是 31 天的数组
				var day31 = [1,3,5,7,8,10,12];
				// 一月30天的数组
				var day30 = [4,6,9,11];
				var hour=new Date().getHours();
				var second=new Date().getMinutes();
				// 判断是不是闰年
			    function isLeapYear(year) {
			    	return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
			    }
			    // 闰年二月要加1
			    year = isLeapYear(parseInt(arr[0])) ? 1 : 0;

				if($.inArray(parseInt(arr[1]),day31) != -1){
					day = parseInt(arr[2]) + 2 > 31 ? (parseInt(arr[2])+2 - 31) : parseInt(arr[2]) + 2;
			        month = parseInt(arr[2]) + 2 > 31 ? parseInt(arr[1]) +1 : parseInt(arr[1])
			    }else if($.inArray(parseInt(arr[1]),day30) != -1){
					day = parseInt(arr[2]) + 2 > 30 ? (parseInt(arr[2])+2 - 30) : parseInt(arr[2]) + 2;
			        month = parseInt(arr[2]) + 2 > 30 ? parseInt(arr[1]) +1 : parseInt(arr[1])
				}else{
			        if(year){
			        	day = parseInt(arr[2]) + 2 + year > 29 ? parseInt(arr[2]) + 2 + year - 29 : parseInt(arr[2]) +2
			            month = parseInt(arr[2]) + 2 + year > 29 ? parseInt(arr[1]) +1 : parseInt(arr[1])
					}else{
			            day = parseInt(arr[2]) + 2 + year > 28 ? parseInt(arr[2]) + 2 + year - 28 : parseInt(arr[2]) +2
			            month = parseInt(arr[2]) + 2 > 28 ? parseInt(arr[1]) +1 : parseInt(arr[1])
					}
				}

				// 将日给处理成需要的格式
			    day = day >= 10 ? day : "0" + day
			    		return {month:month,day:day,hour:hour,second:second};
			}

			// 格式化时间选择器的配置
			function opt(value,year,mouth,day,hour,second) {
				
			    var obj = {
			        type:"datetime"
			    }
			    
			    if(arguments[1]){
			    	
			        obj.value = value;
					obj.beginYear=year;
					obj.beginMonth=parseInt(mouth);
			        obj.beginDay=parseInt(day);
			        obj.beginHours=parseInt(hour);
					obj.beginMinutes=parseInt(second);
			       
				}else{
			    	var now = new Date();
			    	
			        obj.beginYear=now.getFullYear();
			        obj.beginMonth=now.getMonth()+1;;
			        obj.beginDay=now.getDate() >= 10 ? now.getDate() : "0" + now.getDate();
			        obj.beginHour=now.getHours() >= 10 ? now.getHours() : "0" + now.getHours();
				       obj.beginMinutes=now.getMinutes() >= 10 ? now.getMinutes() : "0" + now.getMinutes();
				        obj.value = obj.beginYear + "-" + obj.beginMonth + "-" + obj.beginDay+"  "+ obj.beginHour+":"+ obj.beginMinutes
			        
				}


			    return obj;
			}
	
	
		 function changeTime(tfTime){
			$("#endDate").val(tfTime);
		 }
		//截取前三十个字
		/* var html = $(".placeOrder_info").html();
		var length = $(".placeOrder_info").html().length;
		if(length > 30) {
			$(".placeOrder_info").html(html.substring(0, 30) + '...')
		} */
		//数量增减

		//当是默认1时候
		var t = $("#num").attr('placeholder');
		document.getElementById("num").value=t;
//		console.log(document.getElementById("num").value)
		//当不是placeholder时候
		$(".add").click(function() {
			//获取当前合计的钱数
				var suMoney = $("#suMoney").html();
				suMoney.replace('元','');
                t=document.getElementById("num").value;
                var singel = parseFloat(suMoney)/t;
				t=parseInt(t)+1;
				$("#num").attr("placeholder",t);
				$(".add").removeAttr("disabled");
				document.getElementById("num").value=t;
				$("#payMoney").val(singel*t);
				$("#suMoney").html(singel*t+"元");
			})
		
		$(".jian").click(function() {
				var suMoney = $("#suMoney").html();
				suMoney.replace('元','');
				var singel = parseFloat(suMoney)/t;
				t=parseInt(t)-1;
				if(document.getElementById("num").value > 1) {
					t=document.getElementById("num").value;
					t=parseInt(t)-1;
					$("#num").attr("placeholder",t);
					document.getElementById("num").value=t;
					$("#payMoney").val(singel*t);
					$("#suMoney").html(singel*t+"元");
				} else {
					$(".jian").attr("disabled", "disabled")
				}
				
			})

		function load() {
			var length = $(".Introduce").val().length;
			var html = $(".Introduce").val();

			if(length > 199) {
				$(".Introduce").val(html.substring(0, 200))

				$("#span span").html(200);

			} else {
				$("#span span").html(length)
			}

		}

		function load2() {
			var length = $(".Introduce2").val().length;
			var html = $(".Introduce2").val();

			if(length > 199) {
				$(".Introduce2").val(html.substring(0, 200))

				$("#span2 span").html(200);

			} else {
				$("#span2 span").html(length)
			}

		}
		$(".nature_box .nature").click(function(){
			var num =$("#num").val();
			//0:表示头条
			if($(this).index() == 0){
				
				var topLinePrice = parseInt(${detailMap.topLinePrice});
				$("#accurateVal").val(0);
				$("#skillPrice").val(topLinePrice);
				$("#priceShow").html(topLinePrice);
				$("#suMoney").html(topLinePrice*num+'元');
				$("#payMoney").val(topLinePrice*num);
			}
			//1:表示次条
			if($(this).index()== 1){
				
				var topLinePrice = parseInt(${detailMap.lessLinePrice});
				
				$("#accurateVal").val(1);
				$("#skillPrice").val(topLinePrice);
				$("#priceShow").html(topLinePrice);
				$("#suMoney").html(topLinePrice*num+'元');
				$("#payMoney").val(topLinePrice*num);
				
			}
			
			$(".nature").eq($(this).index()).find("span").eq(0).addClass("nature_choose");
			$(".nature").eq($(this).index()).find("span").eq(1).addClass("groom_Green");
			
			$(this).siblings().find("span").eq(0).removeClass("nature_choose");
			$(this).siblings().find("span").eq(1).removeClass("groom_Green");
			
			
			
		})
		
		/* //选择投放版位
		
		var provide = new mui.PopPicker({
			layer: 1
		});
		provide.setData([{
			value: 0,
			text: "头条",
			price:${detailMap.topLinePrice}
		}, {
			value: 1,
			text: "次条",
			price:${detailMap.lessLinePrice}
		}]);
		var showprovideButton = document.getElementById('accurate');
		showprovideButton.addEventListener('tap', function(event) {
			provide.show(function(items) {
				document.querySelector('#accurate .result-tips').style.display = "none";
				document.querySelector('#accurate .show-result').style.display = "block";
				document.querySelector('#accurate .show-result').innerText = items[0].text;
				var num =$("#num").val();
				$("#accurateVal").val(items[0].value);
				$("#skillPrice").val(items[0].price);
				$("#priceShow").html(items[0].price);
				$("#suMoney").html(items[0].price*num+'元');
				$("#payMoney").val(items[0].price*num);
				//返回 false 可以阻止选择框的关闭
				//return false;
			});
		}, false); */
		$(document).ready(function(){
			//加载时默认选中头条
			var num =$("#num").val();
			var topLinePrice = parseInt(${detailMap.topLinePrice});
			$("#accurateVal").val(0);
			$("#skillPrice").val(topLinePrice);
			$("#priceShow").html(topLinePrice);
			$("#suMoney").html(topLinePrice*num+'元');
			$("#payMoney").val(topLinePrice*num);
		})
		
		
		
	</script>

</html>
