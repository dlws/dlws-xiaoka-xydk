<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/mui.picker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.dtpicker.css" />
		<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2.1/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style1.css" />
		<title>场地资源-下单</title>
	</head>

	<body>
		<!--说明高校社团和技能达人一样-->
		<form id="form1" action="${path}/fileOrder/beforFileOrder.html" method="post">
		<div class="publish_case">
			<div class="publish_content none_border">
				<div class="publish_title">
					<span class="title_pic"><img src="${objMap.headPortrait}" class="search_pic" style="border-radius:50%;"/></span>
					<h3>${objMap.nickname}</h3>
				</div>
			</div>
			<div class="substance_content border">
				<div class="substance_left"><img src="${skillImg}" class="search_pic" /></div>
				<div class="substance_Right">
					<span class="substance_Music">${map.skillName}</span>
					<%-- <span class="substance_Line">${map.serviceType}</span> --%>
					<span class="orderDetail_info">${map.skillDepict}</span>
					<div class="add_Price">
					<span class="substance_price">${detailMap.skillPrice}</span><span class="substance_price">元/${unit}</span>
					</div>
					<span class="sell_Num">[已售${map.sellNo}]</span>
				</div>
			</div>
		</div>
		<ul class="accurate_content">
			<li>
				<span class="accurate-title">购买数量</span>
				<div class="placeOrder_num">
					<span class="jian"><img src="${path}/xkh_version_2.2/img/jian.png" class="search_pic" /></span>
					<input placeholder="${buyNum}" id="num" name="number"/>
					<span class="add"><img src="${path}/xkh_version_2.2/img/add.png" class="search_pic" /></span>
				</div>
			</li>
			<li>
				<span class="accurate-title">联&nbsp;&nbsp;系&nbsp;&nbsp;人</span>
				<input placeholder="请填写您的姓名" class="accurate_inp" name="linkman" id="linkman"/>
			</li>
			<li>
				<span class="accurate-title">联系电话</span>
				<input placeholder="请填写您的联系电话" class="accurate_inp" name="phone" id="phone"/>
				<p class="warn_tel"></p>
			</li>
			<!--这一部分校内商家和社群大V下单没有-->
			<li>
				<span class="accurate-title">需求描述</span>
				<div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce placeOrder_mes" id="demand" name="demand" onkeyup="load()" onkeydown="load()" placeholder="请简要描述所需服务地点，服务具体内容及所需达成的目标吧!"></textarea>
					</div>
					<div id="span" class="Span"><span>0</span>/200</div>
				</div>
			</li>
			<!--这一部分校内商家和社群大V下单没有完-->
			<li>
				<span class="accurate-title placeOrder_word">买家留言<span class="choose_title">(&nbsp;选填&nbsp;)</span></span>
				<div class="textarea">
					<div class="textarea_info">
						<textarea class="Introduce placeOrder_mes Introduce2" id="message" name="message" onkeyup="load2()" onkeydown="load2()" placeholder=""></textarea>
					</div>
					<div id="span2" class="Span"><span>0</span>/200</div>
				</div>
			</li>
		</ul>
		<div class="selected_bg accurate_bg"></div>
		<!--场地下单出现
		<div class="info-content info-content2">
			<span class=" accurate_left">服务时间&nbsp;:&nbsp;</span>
			<span class="gray"><input type="date" id="startDate" name="startDate" required="required" size="10"/></span>
			<span>-</span>
			<span class="gray"><input type="date" id="endDate" name="endDate" required="required" size="10"/></span>
		</div>-->
		<div class="accurate_box">
		<div class="info-content info-content2">
				<span class=" accurate_left">服务时间&nbsp;:&nbsp;</span>
				<span data-options='{}' class="green Btn1 mui-btn mui-btn-block" id="resultStart">2017-01-01&nbsp;00:01&nbsp;</span>
				<span>&nbsp;-&nbsp;</span>
				<span data-options='{}' class="green Btn2 mui-btn mui-btn-block" id="resultEnd">2017-01-01&nbsp;00:01&nbsp;</span>
		</div>
		</div>
		<div class="accurate_total">
			<span>合计&nbsp;:&nbsp;</span>
			<span id="countPrice"><fmt:formatNumber type="number"  pattern="0.0" maxFractionDigits="2" value="${detailMap.skillPrice*buyNum}" />元</span>
		</div>
		<!-- 当前技能的单价 -->
		<input type="hidden" value="${detailMap.skillPrice}" name="skillPrice" id="singlePrice">
		<input type="hidden" value="${map.id}" name="skillId">
		<input type="hidden" value="${objMap.id}" name="id">
		<input type="hidden" value="${unit}" name="dic_name">
		<input type="hidden" name="sellerHeadPortrait" value="${objMap.headPortrait}">
		<input type="hidden" name="sellerNickname" value="${objMap.nickname}">
		<input type="hidden" name="imageUrl" value="${skillImg}">
		<input type="hidden" name="skillName" value="${map.skillName}">
		<input type="hidden" name="serviceType" value="${map.serviceType}">
		<input type="hidden" name="skillDepict" value="${map.skillDepict}">
		<input type="hidden" name="sellPhone" value="${objMap.phoneNumber}">
		<input type="hidden" name="startDate" value="" id="startDate">
		<input type="hidden" name="endDate" value="" id="endDate">
		</form>
		<div class="selected_bg accurate_bg"></div>
		<div class="footer" id="submit"><span>提交订单</span></div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/mui.picker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.1/js/mui.poppicker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.dtpicker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.3/xkh_version_2.3_1/js/common_rule.js"></script>
	<script>
		//截取前三十个字
		/* var html = $(".placeOrder_info").html();
		var length = $(".placeOrder_info").html().length;
		if(length > 30) {
			$(".placeOrder_info").html(html.substring(0, 30) + '...')
		} */
		//产品的单价获取
		//数量增减
		var singlePrice = parseFloat($("#singlePrice").val());
		
		//当是默认1时候
		var t = $("#num").attr('placeholder');
		console.log(t)
		document.getElementById("num").value=t;
//		console.log(document.getElementById("num").value)
		//当不是placeholder时候
		$(".add").click(function() {
                t=parseInt(document.getElementById("num").value);
				t=t+1;
				var count = singlePrice*t;
				$("#num").attr("placeholder",t)
				$(".add").removeAttr("disabled");
				document.getElementById("num").value=t;
				$("#countPrice").html(count.toFixed(2)+"元");
			})
		
		$(".jian").click(function() {
				if(document.getElementById("num").value > 1) {
					t=parseInt(document.getElementById("num").value);
					t=t-1;
					var count = singlePrice*t;
					document.getElementById("num").value=t;
					$("#countPrice").html(count.toFixed(2)+"元");
				} else {
					$(".jian").attr("disabled", "disabled")
				}
			})

		function load() {
			var length = $(".Introduce").val().length;
			var html = $(".Introduce").val();

			if(length > 199) {
				$(".Introduce").val(html.substring(0, 200))

				$("#span span").html(200);

			} else {
				$("#span span").html(length)
			}

		}

		function load2() {
			var length = $(".Introduce2").val().length;
			var html = $(".Introduce2").val();

			if(length > 199) {
				$(".Introduce2").val(html.substring(0, 200))

				$("#span2 span").html(200);

			} else {
				$("#span2 span").html(length)
			}

		}
		
		//提交参数方法
		$("#submit").click(function(){
			
			//1.合作形式必选
			if(""==$.trim($("#linkman").val())){
				alert("请填写联系人名称");
				return
			}
			//2.联系人联系电话
			var phoneNumber=$("#phone").val()
			var re=/^1[3|5|7|8]\d{9}$/;//更改手机号验证问题
			if(phoneNumber==""){
				alert("请输入手机号");
				return;
			}else if(!re.test(phoneNumber)){
				alert("请输入正确的手机号");
				return;
			}
			//3.需求描述 
			if(""==$.trim($("#demand").val())){
				alert("请填写联系人电话");
				return
			}
			//开始使用时间，结束使用时间必选
			if(""==$.trim($("#startDate").val())){
				alert("开始时间必填");
				return
			}
			if(""==$.trim($("#endDate").val())){
				alert("结束时间必填");
				return
			}
			
			$("#form1").submit();
		});
		
		//校验 当前参数是否符合规范
		function checkPamater(){
			
			//需求描述
			if(""==$.trim($("#demand").val())){
				alert("请填写需求描述");
				return
			}
			if(""==$.trim($("#linkman").val())){
				alert("请填写联系人名称");
				return
			}
			//2.联系人联系电话
			if(""==$.trim($("#phone").val())){
				alert("请填写联系人电话");
				return
			}
			//开始使用时间，结束使用时间必选
			if(""==$.trim($("#startDate").val())){
				alert("开始时间必填");
				return
			}
			if(""==$.trim($("#endDate").val())){
				alert("结束时间必填");
				return
			}
			$("#form1").submit();
		}
		
		
		//开始时间 （场地 ）
		(function($) {
				$.init();
				var resultStart = $('#resultStart')[0];
				var btns = $('.Btn1');
				btns.each(function(i, btn) {
						btn.addEventListener('tap', function() {
							var optionsJson = this.getAttribute('data-options') || '{}';
							var options = JSON.parse(optionsJson);
							var id = this.getAttribute('id');
							/*
							 * 首次显示时实例化组件
							 * 示例为了简洁，将 options 放在了按钮的 dom 上
							 * 也可以直接通过代码声明 optinos 用于实例化 DtPicker
							 */
//							var picker = new $.DtPicker(options);
							var today = new Date();
							var picker = new mui.DtPicker({
								beginDate: new Date(today.getFullYear(), today.getMonth(), today.getDay())
							})
							picker.show(function(rs) {
								resultStart.innerText = rs.text;
								getTime(rs.text);
								picker.dispose();
							});
						}, false);
					})
				})(mui);
		function getTime(time){
			$("#startDate").val(time);
		}
		//结束时间
		(function($) {
			$.init();
			var resultEnd = $('#resultEnd')[0];
			var btns = $('.Btn2');
			btns.each(function(i, btn) {
					btn.addEventListener('tap', function() {
						var optionsJson = this.getAttribute('data-options') || '{}';
						var options = JSON.parse(optionsJson);
						var id = this.getAttribute('id');
						var today = new Date();
						var picker = new mui.DtPicker({
							beginDate: new Date(today.getFullYear(), today.getMonth(), today.getDay())
						})
						picker.show(function(rs) {
							resultEnd.innerText = rs.text;
							getEndTime(rs.text);
							picker.dispose();
						});
					}, false);
				})
			})(mui);
		function getEndTime(time){
			$("#endDate").val(time);
		}
	</script>

</html>
