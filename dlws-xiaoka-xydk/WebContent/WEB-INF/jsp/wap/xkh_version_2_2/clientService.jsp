<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title></title>
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<script type="text/javascript" src="${path}/xkh_version_2.3/js/orderStatus.js"></script>
	</head>
		<style>
		   body{
		      min-height:100vh;
		      background:#ebebeb;
		   }
		</style>
	<body>
		<div class="communicate_content">
			<p class="communicate_title">请勿通过微信/支付宝等其他途径付款交易，谨防被骗!444</p>
			<div class="communicate_box" id="communicate_box"></div>
		
		</div>
		<footer class="communicate_foot">
			<div class="communication_foot">
				<img src="${path}/xkh_version_2.2/img/say.png" class="foot_pic foot_photo" />
			<input type="text" value="" class="input" id="sendContent"/>
			<img src="${path}/xkh_version_2.2/img/look.png" class="foot_pic" />
			<a href="javascript:void(0);">
				<img src="${path}/xkh_version_2.2/img/pic.png" class="communicate_pic" />
			</a>
			<span class="send">发送</span>
			</div>
			<div class="communication_bottom">
				<div class="bottom_pic">
					<img src="${path}/xkh_version_2.2/img/picture.png"  />
					<span>图片</span>
				</div>
				<div class="bottom_pic">
					<img src="${path}/xkh_version_2.2/img/said.png"  />
					<span>语音聊天</span>
				</div>
			</div>
		</footer>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js"></script>
	<script>
	//解决第三方软键盘唤起时底部input输入框被遮挡问题
    var bfscrolltop = document.body.scrollTop;//获取软键盘唤起前浏览器滚动部分的高度
    $(".communicate_foot input").focus(function(){
        interval = setInterval(function(){//设置一个计时器，时间设置与软键盘弹出所需时间相近
        document.body.scrollTop = document.body.scrollHeight;//获取焦点后将浏览器内所有内容高度赋给浏览器滚动部分高度
        },100)
    }).blur(function(){//设定输入框失去焦点时的事件
        clearInterval(interval);//清除计时器
        document.body.scrollTop = bfscrolltop;
    });
	
		document.title='${otherInfo.nickname}';
		
	      /*   function BlurOrFocus(){
	    	     var obj = document.getElementsByClassName('input')[0];
	    	     var obj1 = document.getElementsByClassName('communicate_pic')[0];
	    	     var obj2 = document.getElementsByClassName('send')[0];
	    	   var  docTouchend = function(event){
	    	       if(event.target!=obj&&event.target!=obj1&&event.target!=obj2){
	    	           setTimeout(function(){
	    	               obj.blur();
	    	               document.removeEventListener('touchend',docTouchend,false);
	    	           },300);
	    	       }
	    	   }
	    	     if(obj&&obj1&&obj2){
	    	         obj.addEventListener('touchstart',function(){
	    	             document.addEventListener('touchend',docTouchend,false);
	    	         },false);
	    	     }
	    	}

	    	BlurOrFocus(); */	
	    	$(document).bind("click",function(e){ 
	            var target = $(e.target); 
	            if(target.closest(".communicate_content").length == 0&&target.closest(".communicate_foot").length == 0){ 
	             $(".communicate_foot input").blur();
	            } 
	         }) 
	      $(".send")[0].addEventListener("touchend",function(e){
	        	 e.preventDefault(); 
	        	 
                    sendMessage();
                    
                    $(".send")[0].style.display = "none"
                    $(".communication_foot a")[0].style.display = "block";
                    setTimeout(function(){ 
                 	   try{
                 		   $(".communicate_foot input").focus();
                 		  } catch(e){}
                     },200)  
                    
          }); 
		//聊天发送信息
		//newsId，myOpenId，otherOpenId，contentChat
		function sendMessage(){
			var newsId='0';
			var myOpenId='${myOpenId}';
			var otherOpenId='${myOpenId}';
			var content = $.trim($("#sendContent").val());
			if(content!=""){
				$.ajax({
					type:"POST",
					url:"${path}/chatMesseage/weChat.html",
					data:{
						newsId:newsId,
						myOpenId:myOpenId,
						otherOpenId:otherOpenId,
						contentChat:content
					},
					success:function(data){
						document.getElementById("communicate_box").value="";
						 $("#sendContent").val(""); 
						 $(".communicate_content").scrollTop($(".communicate_title")[0].offsetHeight+$(".communicate_box")[0].offsetHeight);
					}
				})
				 $("#sendContent").val(""); 
				
			}else{
				alert("发送内容不能为空！");
				return
			}
			
			
		}
		
		var charLh = 0;
		//定时器
	    setInterval(function() {Push();}, 1000);
	 	function Push(){ 
	 		var newsId='${newsId}';
	 		var headImgUrl = '${userMap.headPortrait}';
			    $.ajax({
			        type: "POST",  
			        url: "${path}/chatMesseage/clientService.html",
			        data: {  
			            id: newsId
			        },  
			        success:function(data){ 
			            //需要执行的操作 execute it  
			          var map= data.model;
			          var resultHtml = "";
			          for(var i=0;i<map.sysList.length;i++){
			        	  if(map.myOpenId==map.sysList[i].myOpenId){
			        		  resultHtml +="<div class='communicate_time'><span class='timeVal'>"+map.sysList[i].messageTimeStr+"</span></div>";
			        		  resultHtml +="<div class='sell_box'>"+
								"<div class='sell_communicate'>"+
									"<div class='sell_new sell_New'><strong><i></i></strong><span>"+map.sysList[i].contentChat+"</span></div>";
									if(!map.sysList[i].headPortrait||map.sysList[i].headPortrait==''){
										if(!headImgUrl||headImgUrl==''){
											resultHtml += "<img src='${path}/xkh_version_2/img/wxHead.png' class='sell_photo' />";		
										}else{
											resultHtml += "<img src='"+headImgUrl+"' class='sell_photo' />";
										}
									}else{
											resultHtml +="<img src='"+map.sysList[i].headPortrait+"' class='sell_photo' />";
									}
									resultHtml +="</div>"+
							"</div>";
							
			        	  }
			        	  if(map.myOpenId!=map.sysList[i].myOpenId){
			        		  resultHtml +="<div class='communicate_time'><span class='timeVal'>"+map.sysList[i].messageTimeStr+"</span></div>";
			        		  resultHtml +="<div class='sell_box'>"+
								"<div class='sell_communicate buy_communicate'>";
							  resultHtml +="<img src='${path}/xkh_version_2.2/img/official_pic.png' class='sell_photo' />";
							  if(map.sysList[i].myOpenId == 0 && map.sysList[i].otherOpenId == 0 ){
								  resultHtml +="<div class='sell_new buy_new'><strong><i></i></strong><span>"+map.sysList[i].contentChat+"";
							  }else{
							  	  resultHtml +="<div class='sell_new buy_new'><strong><i></i></strong><span>您的订单:"+map.sysList[i].orderId+"";
							  }
							  
							  orderStatus(map.sysList[i].contentChat);
								
								resultHtml +="</span>";
								resultHtml +="<span style='text-align:center;font-size:3vw;'><a href='${path}/chatMesseage/queryOrderDetail.html?orderId="+map.sysList[i].orderId+"'>";
								if(map.sysList[i].myOpenId == 0 && map.sysList[i].otherOpenId == 0 ){
									resultHtml +="</a></span>";
								}else{
									resultHtml +="查看更多>></a></span>";
								}
								//resultHtml +="查看更多>></a></span>";
								resultHtml +="</div>"+
								"</div>"+
							"</div>";
			        	  }
			          }
			          if(map.sysList.length>charLh){
			        	  charLh = map.sysList.length;
				          document.getElementById("communicate_box").innerHTML = resultHtml;
				          delTime();
// 			          $('html, body').animate({scrollTop: $(document).height()},60);
					//滚动条默认到达底部
					 var scrHeigh = document.getElementById('communicate_box').scrollHeight;
			          $(".communicate_content").scrollTop(scrHeigh);
		        	  }
			        }
			    });   
		} 
	
		//获取底部高度
		var height = $(".communicate_foot").outerHeight(true);
		var height1 = $(".communication_foot").outerHeight(true);
	/* 	$(".communicate_content .communicate_box:last-child").css("margin-bottom", height); */
		
	   //调用发图片的
	   $(".communicate_pic").click(function(){
	   	$(".communication_bottom").toggle();
	   	var height = $(".communicate_foot").outerHeight(true);
		$(".communicate_content .communicate_box:last-child").css("margin-bottom", height); 
	   })
	  
	    //input获取焦点时候
	   $(".communication_foot input").focus(function(){
	   	$(".communicate_content .communicate_box:last-child").css("margin-bottom", height1);
	   	   $(".communication_bottom").hide();
	   })
	   $(".communication_foot input").bind("input propertychange",function(){
	   	 $(".communicate_content .communicate_box:last-child").css("margin-bottom", height1);
	   	var length=$(this).val().length;
	   	if(length==0){
	   		$(".send").hide();
     	$(".communication_foot a").show();
	   	}else{
	   		  $(".communication_foot a").hide();
                  $(".send").show();
	   	}
          
   });
	   
	    function delTime(){
		   var timeLh = $(".timeVal").length;
		   for(var i=0;i<timeLh;i++){
		   var time = $(".timeVal").eq(i).html();
		   var nexTime = $(".timeVal").eq(i+1).html();
		    nexTime= new Date(nexTime);
		    time= new Date(time);
	    	 if(nexTime-time>60000){
	    		 $(".timeVal").parent().eq(i+1).show();
	    	 }else{
	    		 $(".timeVal").parent().eq(i+1).hide();
	    	 }
		   }
	   } 
	</script>

</html>
