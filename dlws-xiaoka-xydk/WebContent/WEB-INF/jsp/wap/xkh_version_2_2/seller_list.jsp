<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<%-- 订单中间的提示语样式未修改 --%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<!-- <meta http-equiv="refresh" content="5" /> -->
		<title>订单管理</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>

	<body>
		<header class="seller_header">
			<div class="seller_green"><span class="green">全部</span></div>
			<div><span>待付款</span></div>
			<div><span>待服务</span></div>
			<div><span>待确认</span></div>
			<div style="margin-right:0px;"><span>待退款</span></div>
		</header>
		<!--全部出来的样式a-->
		<section>
		  <ul class="seller_Box">
		<c:forEach items="${ordList}" var="ord">
			<!-- 增加个性化详情 -->
			<li>
				<div class="seller_bg"></div>
				<%-- 嵌入订单状态页面 --%>
				<c:import url="orderStatus_sell.jsp" >
					<c:param name="orderStatu" value="${ord.order.orderStatus}"></c:param>
					<c:param name="timeDiffer" value="${ord.order.timeDiffer}"></c:param>
					<c:param name="timmer" value="${ord.timmer}"></c:param>
				</c:import>
				<%-- 控制当前样式名称--%>
				<c:choose>
					<%-- 已关闭 或者是待付款 --%>
					<c:when test="${ord.order.orderStatus eq 1||ord.order.orderStatus eq 5||ord.order.orderStatus eq 10 }">
						<c:set var="className" value="substance_Right buyer_Right"></c:set>	
					</c:when>
					<c:otherwise>
						<c:set var="className" value="substance_Right"></c:set>	
					</c:otherwise>
				</c:choose>
				<%-- 只有电话的时候className 追加 call_containt --%>
				<c:choose>
					<%-- 标记有电话时候的样式 --%>
					<c:when test="${ord.order.orderStatus==0||ord.order.orderStatus ==3||ord.order.orderStatus==6||ord.order.orderStatus==12||ord.order.orderStatus==9}">
						<c:set var="substanceClass" value="substance_content seller_content call_content"></c:set>	
					</c:when>
					<c:otherwise>
						<c:set var="substanceClass" value="substance_content seller_content "></c:set>	
					</c:otherwise>
				</c:choose>
				
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait }" class="search_pic" /></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">${status}</span>
				</div>
				<div class="${substanceClass}">
					<c:if test="${ord.order.orderStatus==2||ord.order.orderStatus ==4||ord.order.orderStatus==8||ord.order.orderStatus==9||ord.order.orderStatus==7}">
						<a href="${path}/chatMesseage/queryOrderDetail.html?orderId=${ord.order.orderId}&channel=sell">
					</c:if>
					<div class="substance_left">
						<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="${className}">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					</span></span>
						<!-- 0 与 3为确认 -->
						<c:if test="${ord.order.orderStatus eq 0 || ord.order.orderStatus eq 3||ord.order.orderStatus eq 6||ord.order.orderStatus eq 12}">
							<div class="seller_picture">
									<img src="${path}/xkh_version_2.2/img/tell.png" class="seller_pic">
									<a href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${ord.order.openId}"><img src="${path}/xkh_version_2.2/img/chart.png" class="seller_pic seller_pic2"></a>
									<input type="hidden" name="sellPhone" value="${ord.order.phone}">
							</div>
						</c:if>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<div class="add_Price">
						   <span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						</div>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<!--出来的删除-->
					<c:if test="${ord.order.orderStatus eq 1||ord.order.orderStatus eq 5||ord.order.orderStatus eq 10}">
						<div class="selected_del">删除</div>
						<input type="hidden" name="orderId" value="${ord.order.orderId}">
					</c:if>
					<input type="hidden" value="${ord.order.openId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
					
					<c:if test="${ord.order.orderStatus==2||ord.order.orderStatus ==4||ord.order.orderStatus==8||ord.order.orderStatus==9||ord.order.orderStatus==7}">
						</a>
					</c:if>
				</div>
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
					<c:choose>
							<c:when test="${ord.order.orderStatus eq 2}">
								<div class="total_content none_border seller_border">
									<div class="order_content seller_refuse">
										<a href="javascript:void(0);" onclick="refund(this)">拒绝</a>
										<a href="javascript:void(0);" class="green Green" onclick="accept(this)">接单</a>
										<input type="hidden" value="${ord.order.orderId}" name="orderId">
									</div>
								</div>
							</c:when>
							<c:when test="${ord.order.orderStatus eq 6}">
								<div class="total_content none_border seller_border">
									<div class="order_content seller_Refuse">
										<a href="javascript:void(0);" onclick="reject(this)">拒绝退款</a>
										<a href="javascript:void(0);" class="green Green" onclick="agree(this)">同意退款</a>
										<input type="hidden" value="${ord.order.orderId}" name="orderId">
									</div>
								</div>
							</c:when>
							<%-- <c:when test="${ord.order.orderStatus eq 0}">
								<div class="total_content none_border seller_border">
									<div class="order_content buyer_Refuse">
										<input type="hidden" name="orderId" value="${ord.order.orderId}" style="display: none">
										<a href="javascript:void(0);" onclick="finish(this)" class="cfz_border" style="border-color:green;color: #3DA166;" >服务完成</a>
									</div>
								</div>
							</c:when> --%>
					</c:choose>
				</div>
			</li>
			</c:forEach>
			</ul>
			<!--待付款页面  -->
			<ul class="seller_Box none">
			<c:forEach items="${ordList}" var="ord">
			<c:if test="${ord.order.orderStatus eq 1}">
			
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<div class="head-content">
						<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
						<span class="head_right">等待买家付款，<span class="timmer">${ord.order.timeDiffer}</span>后将自动取消</span>
						<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
					</div>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" style="border-radius:100%;" class="search_pic" /></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待付款</span>
				</div>
				
				<div class="substance_content seller_content">
					<div class="substance_left">
							<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="substance_Right">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					
					</span></span>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
						<!--出来的删除-->
						<div class="selected_del">删除</div>
						<input type="hidden" name="orderId" value="${ord.order.orderId}">
					</div>
					<input type="hidden" value="${ord.order.openId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
				</div>
				
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
				</div>
			</li>
			</c:if>
			</c:forEach>
			</ul>
			<!--待服务页面-->  
			<ul class="seller_Box none">
			<c:forEach items="${ordList}" var="ord">
			<!-- 查看当前订单的状态 -->
			<c:if test="${ord.order.orderStatus eq 2}">
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<div class="head_content">
						<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
						<span class="head_right">买家已付款，<span class="timmer">${ord.order.timeDiffer}</span>分后将自动关闭</span>
						<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
					</div>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait }" class="search_pic" /></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待服务</span>
				</div>
				<div class="substance_content seller_content" >
					<a href="${path}/chatMesseage/queryOrderDetail.html?orderId=${ord.order.orderId}&channel=sell">
					<div class="substance_left">
						<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="substance_Right">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					
					</span></span>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<input type="hidden" value="${ord.order.openId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
					</a>
				</div>
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
				</div>
				<div class="total_content none_border seller_border">
					<div class="order_content seller_refuse">
						<a href="javascript:void(0);" onclick="refund(this)">拒绝</a>
						<a href="javascript:void(0);" class="green Green" onclick="accept(this)">接单</a>
						<input type="hidden" value="${ord.order.orderId}" name="orderId">
					</div>
				</div>
			</li>
			</c:if>
			</c:forEach>
			</ul>
			<!--待确认-->
			<ul class="seller_Box none">
			<c:forEach items="${ordList}" var="ord">
			<!-- 0是已接单 -->
			<c:if test="${ord.order.orderStatus eq 0}">
			
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<div class="seller_Content">
						<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
						<span class="head_right">您已接单，服务中<span></span></span>
					</div>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" style="border-radius:100%;" class="search_pic" /></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待确认</span>
				</div>
				<div class="substance_content seller_content call_content">
					<div class="substance_left">
						<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="substance_Right">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line">
						<span>
							<c:choose>
								<c:when test="${ord.map.serviceType==1}">线上</c:when>
								<c:otherwise>线下</c:otherwise>
							</c:choose>
						</span>
					</span>
					<div class="seller_picture">
						<img src="${path}/xkh_version_2.2/img/tell.png" class="seller_pic call_tel">
						<a class="call_communicate" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${ord.order.openId}"><img src="${path}/xkh_version_2.2/img/chart.png" class="seller_pic seller_pic2"></a>
						<input type="hidden" name="sellPhone" value="${ord.order.phone}">
					</div>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<input type="hidden" value="${ord.order.openId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
				</div>
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
					<!-- 服务已完成待确认通知买家 -->
					<%-- <div class="total_content none_border seller_border">
						<div class="order_content buyer_Refuse">
							<input type="hidden" name="orderId" value="${ord.order.orderId}" style="display: none">
							<a href="javascript:void(0);" onclick="finish(this)" class="cfz_border" style="border-color:green;color: #3DA166;" >服务完成</a>
						</div>
					</div> --%>
				</div>
			</li>
			</c:if>
			<!-- 3是以服务待确认 -->
			<c:if test="${ord.order.orderStatus eq 3}">
			
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<div class="sellerComit_content">
						<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
						<span class="head_right">等待买家确认收货<span class="timmer">${ord.order.timeDiffer}</span>后将自动确认</span>
						<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
					</div>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" style="border-radius:100%;" class="search_pic" /></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待确认</span>
				</div>
				<div class="substance_content seller_content call_content">
					<div class="substance_left">
						<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="substance_Right">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					</span></span>
					<div class="seller_picture">
						<img src="${path}/xkh_version_2.2/img/tell.png" class="seller_pic call_tel">
						<a class="call_communicate" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${ord.order.openId}"><img src="${path}/xkh_version_2.2/img/chart.png" class="seller_pic seller_pic2"></a>
						<input type="hidden" name="sellPhone" value="${ord.order.phone}">
					</div>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<input type="hidden" value="${ord.order.openId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
				</div>
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
				</div>
			</li>
			</c:if>
			</c:forEach>
			</ul>
			<!--待退款页面-->
			<ul class="seller_Box none">
			<c:forEach items="${ordList}" var="ord">
			<c:if test="${ord.order.orderStatus eq 6}">
			
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<div class="sellerComit_content2">
						<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
						<span class="head_right">买家申请退款，<span class="timmer">${ord.order.timeDiffer}</span>后将自动确认</span>
						<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
						
					</div>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" style="border-radius:100%;" class="search_pic" /></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待退款</span>
				</div>
				<div class="substance_content seller_content call_content">
					<div class="substance_left">
						<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="substance_Right">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					</span>
					</span>
					<div class="seller_picture">
						<img src="${path}/xkh_version_2.2/img/tell.png" class="seller_pic call_tel">
						<a class="call_communicate" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${ord.order.openId}"><img src="${path}/xkh_version_2.2/img/chart.png" class="seller_pic seller_pic2"></a>
						<input type="hidden" name="sellPhone" value="${ord.order.phone}">
					</div>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<input type="hidden" value="${ord.order.openId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
				</div>
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
					<div class="total_content none_border seller_border">
					<div class="order_content seller_Refuse">
						<a href="javascript:void(0);" onclick="reject(this)">拒绝退款</a>
						<a href="javascript:void(0);" class="green Green" onclick="agree(this)">同意退款</a>
						<input type="hidden" value="${ord.order.orderId}" name="orderId">
					</div>
				</div>
				</div>
			</li>
			</c:if>
			<!-- 卖家拒绝退款 -->
			<c:if test="${ord.order.orderStatus eq 12}">
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<div class="headerRefuse_content">
						<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
						<span class="head_right">已拒绝，等待买家重新处理<span class="timmer">${ord.order.timeDiffer}</span>后将自动关闭</span>
						<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
					</div>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" style="border-radius:100%;" class="search_pic" /></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待退款</span>
				</div>
				<div class="substance_content seller_content call_content">
					<div class="substance_left">
						<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="substance_Right">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					</span>
					</span>
					<div class="seller_picture">
						<img src="${path}/xkh_version_2.2/img/tell.png" class="seller_pic call_tel">
						<a class="call_communicate" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${ord.order.openId}"><img src="${path}/xkh_version_2.2/img/chart.png" class="seller_pic seller_pic2"></a>
						<input type="hidden" name="sellPhone" value="${ord.order.phone}">
					</div>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<input type="hidden" value="${ord.order.openId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
				</div>
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;:&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
				</div>
			</li>
			</c:if>
			</c:forEach>
			</ul>
			
			
		</section>
		<!-- 當前沒有訂單時候出現 -->
			<div class="bank_content">
			   <span class="bank_photo">
			     <img src="${path}/xkh_version_2.2/img/bank_order.png" class="search_pic" />
			   </span>
			   <span class="bank_message">您还没有相关订单</span>
			</div>
		<!--出来的电话弹框-->
		<a href="tel:15036693872" class="telephone_content">点此拨打：15036693872</a>
		<div class="popup_mask"></div>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js" ></script>
    <script>
    	//初始加载页面的时候读取 sessionStrange中的内容
    	var path = "${path}";
    	$(document).ready(function(){
    		
    		//加载时先对其隐藏 
    		var liArray = $("section ul").eq(0).children("li");
    		//先对空提示页面进行隐藏
    		$(".bank_content").hide();
    		//获取当前元素是否存在子元素
    		if(liArray.length == 0){
    			$(".bank_content").show();
    		}
    		var tabNum="${tabNum}";
    		sessionStorage.tabNum = 0;
	    	//如果 加载全部则 去 sessionStrange中进行寻找 加载sessionStange中的内容
	    	if(tabNum==""){//传参为0则默认为全部
	    		if(typeof(sessionStorage.tabNum)=="undefined"){
	    			tabNum = 0;
	    		}else{
	    			tabNum = parseInt(sessionStorage.tabNum); 
	    		}
	    	}
	    	var object = $(".seller_header div").eq(tabNum);
	    	object.addClass("seller_green").siblings().removeClass("seller_green");
	    	$(".seller_header span").removeClass("green");	
	    	object.children("span").addClass("green");
	    	//$(">span",object).addClass("green");	
	    	$("section ul").eq(tabNum).show().siblings().hide();	
		});
    	
    	//点击切换 tab页 
    	$(".seller_header div").click(function(){
    		var index=$(this).index();
    		$(this).addClass("seller_green").siblings().removeClass("seller_green");
    		$(".seller_header span").removeClass("green")
    		$(">span",this).addClass("green");
    		$("section ul").eq(index).show().siblings().hide();
    		var liArray = $("section ul").eq(index).children("li");
    		//先对空提示页面进行隐藏
    		$(".bank_content").hide();
    		//获取当前元素是否存在子元素
    		if(liArray.length == 0){
    			$(".bank_content").show();
    		}
    		//存入最后一次选中的 Tab 
    		sessionStorage.tabNum = index;
    	})
    	//截取前三十个字
    	/* var htmlArray = $(".orderDetail_info");
    	
    	for(var i=0;i<htmlArray.length;i++){
    		
    		var html = $(htmlArray[i]).html();
    		var length = $(htmlArray[i]).html().length;
    		if(length > 30) {
    			$(htmlArray[i]).html(html.substring(0, 30) + '...');
    		}
    		
    	} */
		//点击拒绝或者接单时候
		$(".seller_refuse a").click(function(){
			var index=$(this).index();
			$(this).addClass("green Green").siblings().removeClass("green Green");
			$(this).siblings().css("border","1px solid #939393");
			if(index==0){
				$(this).parents("li").find(".edit").html("已关闭");
			}else{
				$(this).parents("li").find(".edit").html("待确认");
			}
		})
		
		var h=$("body").height();
		
		//点击拒绝退款同意退款
		$(".seller_Refuse a").click(function(){
			var index=$(this).index();
			$(this).addClass("green Green").siblings().removeClass("green Green");
			$(this).siblings().css("border","1px solid #939393");
			if(index==0){
			$(this).parents("li").find(".edit").html("待退款")
			}else{
				$(this).parents("li").find(".edit").html("已退款")
			}
		})
		//获取删除的高度
		var height = $(".substance_content").height();
		$(".selected_del").css({
			"height": height,
			"line-height": height + 'px'
		});
		var onoff = true;
		$(".buyer_Right").on("touchstart", function(e) {
			onoff = true;
			e.preventDefault();
			startX = e.originalEvent.changedTouches[0].pageX,
				startY = e.originalEvent.changedTouches[0].pageY;
		});
		var X = 0;
		var Y = 0;
		$(".buyer_Right").on("touchmove", function(e) {
			moveEndX = e.originalEvent.changedTouches[0].pageX,
				moveEndY = e.originalEvent.changedTouches[0].pageY,
				X = moveEndX - startX,
				Y = moveEndY - startY;
			onoff = false;
		});
		$(".buyer_Right").on("touchend", function(e) {
			var windowHeight = $(window).height(),
			$body = $("body");
			$body.css("height", windowHeight);
			e.preventDefault();
				if(Math.abs(X) > Math.abs(Y) && X < 0) {
					$(this).siblings(".selected_del").removeClass("del1").addClass("del")
				}
				if(Math.abs(X) > Math.abs(Y) && X > 0) {
					$(this).siblings(".selected_del").removeClass("del").addClass("del1")
				}
				if(onoff){
					//获取当前同等级下的 样式名为 substance_left的 a 标签 
					var orderId = $(this).siblings("input[name='orderId']").val();
					 window.location.href=path+"/chatMesseage/queryOrderDetail.html?orderId="+orderId+"&channel=buyer"; ;
				};
				
		});
		/**待付款  删除操作 */
		$(".selected_del").click(function() {
			$(this).parents('li').hide();
			console.log($(this).html());
			//获取同级元素
			var orderId = $(this).siblings("input[name='orderId']").val();
			var json ={"orderId":orderId}; 
			$.ajax({
				  url: "${path}/orderseaSell/deleteOrderSell.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();//刷新当前请求  
				    // ${path }/orderseaSell/searchOrderSell.html?tabNum=0
				    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			$(this).parent().removeAttr("href");
		})
		/**取消订单则将订单状态更改为删除*/
		function delOrder(obj){
			console.log(obj.parentNode);
			$(obj).parents('li').hide();
			
			//获取同级元素
			var orderId = $(obj).siblings("input[name='orderId']").val();
			var json ={"orderId":orderId}; 
			$.ajax({
				  url: "${path}/orderseaSell/deleteOrderSell.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();刷新当前请求  
				    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			$(this).parent().removeAttr("href");
			
		}
		/**待 服 务 */
		//1.拒绝接单
		function  refuse(obj){
			$(obj).parents('li').hide();
			var value = $(obj).siblings("input[name='orderId']").val();
			var json = {"orderId":value};
			$.ajax({
				  url: "${path}/orderseaSell/updateOrderSellDfuRefuse.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();//刷新当前请求  
				    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err);
				  }
				})
			$(obj).parent().removeAttr("href");
		}
		//2. 接单 
		function  accept(obj){
			$(obj).parents('li').hide();
			var value = $(obj).siblings("input[name='orderId']").val();
			var json = {"orderId":value};
			$.ajax({
				  url: "${path}/orderseaSell/updateOrderSellAccept.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();//刷新当前请求  
				    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err);
				  }
				})
			$(obj).parent().removeAttr("href");
		}
		/**待确认 */
		//1.拒绝接单 
		function  refund(obj){
			
			$(obj).parents('li').hide();
			var value = $(obj).siblings("input[name='orderId']").val();
			var json = {"orderId":value};
			$.ajax({
				  url: "${path}/orderseaSell/updateOrderSellDfuRefuse.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();//刷新当前请求  
				    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			$(obj).parent().removeAttr("href");
			
		}
		//1.拒绝退款 
		function  reject(obj){
			$(obj).parents('li').hide();
			var value = $(obj).siblings("input[name='orderId']").val();
			var json = {"orderId":value};
			$.ajax({
				  url: "${path}/orderseaSell/updateOrderSellReject.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();//刷新当前请求  
				    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			$(obj).parent().removeAttr("href");
			
		}
		//2.同意退款 
		function  agree(obj){
			$(obj).parents('li').hide();
			var value = $(obj).siblings("input[name='orderId']").val();
			var json = {"orderId":value};
			$.ajax({
				  url: "${path}/orderseaSell/updateOrderSellAgree.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();//刷新当前请求  
				    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err);
				  }
				})
			$(obj).parent().removeAttr("href");
			
		}
		//取消退款  此处调用  确认收货按钮 
		
		//5.服务完成
		<%--更改当前订单状态为3待确认 --%>
		function  finish(obj){
			$(obj).parents('li').hide();
			var value = $(obj).siblings("input[name='orderId']").val();
			var json = {"orderId":value};
			$.ajax({
				  url: "${path}/orderseaSell/updateOrderFinish.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();//刷新当前请求
				    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			$(obj).parent().removeAttr("href");
			
		}
		
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/commen.js" ></script>
</html>
