<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>消息</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>

	<body>
		<ul class="news_content">
			<li>
			   <a href="${path }/chatMesseage/getSysList.html?id=0">
				<div class="news_pic">
					<img src="${path}/xkh_version_2.2/img/official_pic.png" class="news_left" />
				
						<c:if test="${ not empty sysMap.sysNo}">
							<c:if test="${sysMap.sysNo!='0'}">
								<div class="news_num">
									<div class="news_number">${sysMap.sysNo}</div>
								</div>
								<input type="hidden" class="sysNumber" value="${sysMap.sysNo}">
							</c:if>
							<c:if test="${sysMap.sysNo=='0'}">
								<input type="hidden" class="sysNumber" value="${sysMap.sysNo}">
							</c:if>
						</c:if>
				</div>
				<div class="news_right">
					<div class="new_top">
						<span>官方消息</span>
						<span><fmt:formatDate value="${sysMap.createDate}" pattern="yyyy-MM-dd"/></span>
					</div>
					<p class="new_bottom">${sysMap.orderId}</p>
				</div></a>
			</li>
			
			<c:forEach items="${listmap}" var ="person" varStatus="status">
				<c:if test="${person.id!=0}">
				<li>
				<a href="${path}/chatMesseage/getChatInfoById.html?id=${person.id}&chatType=${person.chatType}" class="selhref">
					<div class="news_pic">
					<c:choose>
						<c:when test="${empty person.headpic}">
						  <c:if test="${empty headMap.headImgUrl}">
						     <img src="${path}/xkh_version_2/img/wxHead.png" class="news_left" />
						  </c:if>
						  <c:if test="${not empty headMap.headImgUrl}">
						 	 <img src="${headMap.headImgUrl}" class="news_left" />
						  </c:if>
						</c:when>
						<c:otherwise>
							<img src="${person.headpic}" class="news_left" />
						</c:otherwise>
					</c:choose>
						<!-- 显示未读条数 -->
						<c:if test="${0!=person.unReadNum}">
							<div class="news_num">
								<div class="news_number">${person.unReadNum}</div>
							</div>
							<input type="hidden" class="normal" value="${person.unReadNum}">
						</c:if>
						<!-- 不显示未读条数 -->
						<c:if test="${0==person.unReadNum}">
							<input type="hidden" class="normal" value="${person.unReadNum}">
						</c:if>
					</div>
	
					<div class="news_right slide">
						<div class="new_top">
							<c:if test="${not empty person.nickname&&person.nickname!='null'}">
								<span>${person.nickname}</span>
							</c:if>
							<c:if test="${empty person.nickname||person.nickname=='null'}">
								<span>游客</span>
							</c:if>
							<c:if test="${empty person.createDate}">
								<span><fmt:formatDate value="${person.mycreateDate}" pattern="yyyy-MM-dd "/></span>
							</c:if>
							<c:if test="${not empty person.createDate}">
								<span><fmt:formatDate value="${person.createDate}" pattern="yyyy-MM-dd "/></span>
							</c:if>
						</div>
						<p class="new_bottom">${person.contentChat}</p>
					</div>
				</a>
					<!--出来的删除-->
					<input type="hidden" class="delMusic" value="${person.id}">
					<div class="selected_del" >删除</div>
				</li>
				</c:if>
			</c:forEach>
		</ul>
		<%@ include file="../xkh_version_2/foot.jsp"%>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/formatTime.js" ></script>
    <script>
    
	  /*   var newTime = new Date("${listmap[1].createDate}"); //就得到普通的时间了
		var newTimeTwo =newTime.formate("yyyy-MM-dd EEE HH:mm:ss"); */
    
		//获取删除的高度
		var height = $(".news_content li").outerHeight(true);
		$(".selected_del").css({
			"height": height,
			"line-height": height + 'px'
		});
		$(".slide").on("touchstart", function(e) {
			e.preventDefault();
			startX = e.originalEvent.changedTouches[0].pageX,
		    startY = e.originalEvent.changedTouches[0].pageY;
		});
		$(".slide").on("touchmove", function(e) {
			moveEndX = e.originalEvent.changedTouches[0].pageX,
				moveEndY = e.originalEvent.changedTouches[0].pageY,
				X = moveEndX - startX,
				Y = moveEndY - startY;
		});
		var X = 0;
		var Y = 0;
		$(".slide").on("touchend", function(e) {
			var windowHeight = $(window).height(),
				$body = $("body");
			$body.css("height", windowHeight);
			e.preventDefault();
			if(X!=0&&(X>3||X<-3)){
				if(Math.abs(X) > Math.abs(Y) && X < 0) {
					$(this).parents("li").children(".selected_del").removeClass("del1").addClass("del");
					X=0;
				}
				if(Math.abs(X) > Math.abs(Y) && X > 0) {
					$(this).parents("li").children(".selected_del").removeClass("del").addClass("del1")
					X=0;
				}
			}else{
				var href = $(this).parents(".selhref").attr("href");
				window.location.href= href;
			}
			
		});
		$(".selected_del").click(function() {
			var idVal = $(".delMusic").eq($(this).index(".selected_del")).val();
			 if(confirm("确定删除?")){
				//表示修改需要数据库删除
				$.ajax({
					type : "post",
					url : "<%=basePath%>chatMesseage/deletChat.html",
					dataType : "json",
					data : {id:idVal},
					success : function(data) {
						
					}
				});
			$(this).parents("li").hide();
			$(this).parent().removeAttr("href");
	 		 }
			
		})
    	var h = $(".foot-bar-tab").height() - 2;
		$(".selected_content li:last-child").css("margin-bottom", h);
		$(".selected_content li:last-child").find(".selected_bg").css("border-bottom", 0);
		//底部选项卡切换
		$(".foot-bar-tab a:not(.refuse_publish)").click(function() {
			$(this).addClass("foot-active").siblings().removeClass("foot-active");
		})
		/* 获取底部高度 */
		var footHeight=$(".foot-bar-tab").outerHeight(true);
		console.log(footHeight)
		$(".news_content li:last-child").css("margin-bottom",footHeight);
		
		//定时器
	    setInterval(function() {reload();}, 1000);
		
		function reload(){
			var sysNumber = $(".sysNumber").val();
			$.ajax({
		        type: "POST",  
		        url: "${path}/chatMesseage/queryStatus.html",
		        success:function(data){
		        	var sysNo = data.model.sysMap.sysNo;
		        	if(parseInt(sysNumber)>parseInt(sysNo)){
		        		location.reload();
		        	}
		        	
		        	for(var i=0;i<data.model.listmap.length;i++){
		        		var unReadNumChu = $(".normal").eq(i).val();
		        		unReadNumChu = parseInt(unReadNumChu);
		        		var unReadNum = data.model.listmap[i].unReadNum;
		        		if(unReadNum<unReadNumChu){
		        			location.reload();
		        		}
		        	}
		        }
			});
		}
		var path = "${path}";
    	//消息在第一序列
    	var index = 1;
    </script>
     <script type="text/javascript" src="${path}/xkh_version_2.2/js/foot.js" ></script>
</html>
