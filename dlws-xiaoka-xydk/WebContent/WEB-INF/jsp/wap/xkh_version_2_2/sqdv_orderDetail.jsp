<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>社群大V-订单详情</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/style2.2.1.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/style2.2.css" />
	</head>

	<body>
		<div class="publish_case">
			<div class="publish_content">
				<div class="publish_title">
					<span class="title_pic"><img src="${path}/xkh_version_2.4/img/per_photo.png" class="search_pic" /></span>
					<h3>${baseInfo.nickname}</h3>
				</div>
			</div>
			
			<div class="substance_content">
				<div class="substance_left"><img src="${path}/xkh_version_2.4/img/pro_img.jpg" class="search_pic" /></div>
				<div class="substance_Right">
					<span class="substance_Music">${skillInfo.skillName}</span>
					<span class="substance_Line"><span>
					<c:if test="${skillInfo.serviceType==1}">线上</c:if>
					<c:if test="${skillInfo.serviceType==2}">线下</c:if>
					</span></span>
					<span class="orderDetail_info">${skillInfo.skillDepict}</span>
					<span class="substance_price">${skillInfo.skillPrice}</span>
					<%-- 下单页面暂无单位 --%>
					<span class="substance_price">元/次</span>
					<span class="sell_Num">[已售${skillInfo.sellNo}]</span>
				</div>

			</div>
		</div>
		<div class="orderDetail_num">
			<span class="accurate-title orderDetail_buy">购买数量&nbsp;:</span>
			<span class="accurate_inp orderDetail_number">${skillInfo.tranNumber}</span>
		</div>
		<%--这一部分是卖家拒绝或者同意修改后的买家订单详情页出现--%>
		<div class="refund_box">
			<div class="fefund_content">
			<span>退款</span>
		</div>
		<div class="selected_bg accurate_bg"></div>
		</div>
		
		<%--这一部分是卖家拒绝或者同意修改后的买家订单详情页出现--%>
			
		<%--这一部分只有当买家已付款卖家已接单时候才出现--%>
		<c:if test="${orderMap.orderStatus eq 0 || orderMap.orderStatus eq 3 || orderMap.orderStatus eq 6 || orderMap.orderStatus eq 7 || orderMap.orderStatus eq 12|| orderMap.orderStatus eq 9}">
			<%--这一部分只有当买家已付款卖家已接单时候才出现--%>
			<div class="chart_content">
				<div class="telphone">
					<span><img src="${path}/xkh_version_2.2/img/tell.png" class="search_pic" /></span>
					<span>打电话</span>
				</div>
				<div class="telphone chat">
					<span><img src="${path}/xkh_version_2.2/img/chart.png" class="search_pic" /></span>
					<span>聊一聊</span>
				</div>
			</div>
		</c:if>
		<%--社群大v出现--%>
		<div class="order_bg order-none"></div>
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.4/img/accurtate.png" class="search_pic" /></div>
				<span class="info">投放版位</span>
			</div>
			<div class="place_content">
				<c:forEach items="${settleList}" var="one">
					<c:if test="${one.typeName=='accurateVal'}">
						<c:if test="${one.typeValue=='0'}">
							<div><span>头条</span></div>
						</c:if>
						<c:if test="${one.typeValue=='1'}">
							<div><span>次条</span></div>
						</c:if>
					</c:if>
				</c:forEach>
			</div>
		</div>
		<div class="order_bg"></div>
		<%--买家信息--%>
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.4/img/buyer.png" class="search_pic" /></div>
				<span class="info">买家信息</span>
			</div>
			<div class="info-content">
				<span>联&nbsp;&nbsp;系&nbsp;&nbsp;人&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.linkman}</span>
			</div>
			<div class="info-content info-content2">
				<span>联系电话&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.phone}</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span>买家留言</span>
				<div class="introduce_new">
					<div class="about_content p" id="box">${orderMap.message}</div>
					<strong class="class_up watch"></strong>
				</div>
			</div>
		</div>
		<div class="order_bg"></div>
		<%--订单信息--%>
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.4/img/consult.png" class="search_pic" /></div>
				<span class="info">订单信息</span>
			</div>
			<div class="info-content">
				<span>订单编号&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.orderId}</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span>下单时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span><fmt:formatDate  value="${orderMap.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></span>
			</div>
		</div>
		<div class="selected_bg accurate_bg"></div>

		<div class="accurate_box">
			<div class="info-content info-content2">
				<span class=" accurate_left">投放时间&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<span class="green"><fmt:formatDate  value="${orderMap.endDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></span>
			</div>
			<div class="info-content info-content2">
				<span class=" accurate_left">合计&nbsp;:&nbsp;</span>
				<span class="green">${orderMap.payMoney}元</span>
			</div>
		</div>
   <%---------------------------这一部分为买家端特有-----------------------------------------%>
	<c:if test="${dkOpenId eq orderMap.openId}">
		<c:if test="${orderMap.orderStatus eq 3}">
		<div class="buyerContent">
			<c:if test="${feedBackBean.sellerRefuse eq 1}"><%--此处应该判定 当前最新的订单买家是否有驳回的现象 判定后做 --%>
				<div class="selected_bg accurate_bg border_none"></div>
				<%--卖家拒绝修改出现，卖家同意修改不出现--%>
				<div class="reject_content">
					<p class="reject">卖家拒绝修改原因</p>
					<span class="reject_substance">${feedBackBean.reseon }</span>
				</div>
			</c:if>
			<%--卖家拒绝修改出现卖家同意修改不出现完--%>
			<div class="reject_total">
				<a href="${path}/feedback/orderConfirmShow.html?orderId=${orderMap.orderId}" class="green Green">确认收货</a>
				<a href="${path}/feedback/showFeedBackBuyer.html?orderId=${orderMap.orderId}">查看反馈</a>
				<c:if test="${feedBackBean.feedbackstatus eq 1}">
					<span class="cfz_red"></span>
				</c:if>
				<c:if test="${feedBackBean.sellerRefuse eq 1}">
					<a href="${path}/feedback/jumpCustInter.html?orderId=${orderMap.orderId}&feedbackId=${feedBackBean.id}">客服介入</a>
				</c:if>
			</div>
		</div>
		</c:if>
	</c:if>
<%-- -------------------------------这一部分为卖家端特有-------------------- --%>
 	<c:if test="${dkOpenId eq orderMap.sellerOpenId}"> 
<%--	<c:if test="${orderMap.sellerOpenId eq orderMap.sellerOpenId}">--%>
		
		<c:choose>
			<c:when test="${orderMap.orderStatus eq 0}">
				<div class="sellerContent">
					<c:if test="${feedBack eq false}">
						<%--卖家端订单详情反馈入口开始默认消失，加个类名hide.添加移除hide操作是否消失出现seller_reject1这样的类名是为了操作控制消失出现--%>
						<div class="reject_total seller_reject1">
							<a href="${path}/feedback/showFeedBack.html?orderId=${orderMap.orderId}" class="green Green">提交反馈</a>
						</div>
					</c:if>
					<c:if test="${feedBack eq true}">
						<%--卖家提交订单反馈后的订单详情页--%>
						<div class="reject_total seller_reject2">
							<a href="${path}/feedback/showFeedBack.html?orderId=${orderMap.orderId}" class="green Green">查看反馈</a>
						</div>
					</c:if>
				</div>
			</c:when>
			<c:when test="${orderMap.orderStatus eq 6}">
				<div class="sellerContent">
					<%--买家申请退款，卖家端页面出现--%>
					<div class="reject_total seller_reject3">
						<a href="Confirm_receipt.html" class="green Green">提交反馈</a>
						<a href="#">同意退款</a>
						<a href="customer_service.html">拒绝退款</a>
					</div>
				</div>
			</c:when>
		<%--卖家提交反馈，买家驳回--%>
			<c:when test="${orderMap.orderStatus eq 3}">
				<c:choose>
					<c:when test="${feedBackBean.buyerReject eq 1}">
						<div class="sellerComit">
							<div class="reject_content">
								<p class="reject">买家驳回原因</p>
								<span class="reject_substance">${feedBackBean.suggestion}</span>
						   </div>
						   <div class="reject_total">
							<a href="${path}/feedback/showFeedBack.html?orderId=${orderMap.orderId}" class="green Green">重新修改</a>
							<%--拒绝修改跳转到全局通用的提交成功页面，之前版本有--%>
							<a href="${path}/feedback/sellerRefuseShow.html?orderId=${orderMap.orderId}">拒绝修改</a>
						  </div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="sellerContent">
							<%--卖家提交订单反馈后的订单详情页--%>
							<div class="reject_total seller_reject2">
								<a href="${path}/feedback/showFeedBack.html?orderId=${orderMap.orderId}" class="green Green">查看反馈</a>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
			</c:when>
		</c:choose>
	</c:if>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/Service feedback/common.js" ></script>
</html>
