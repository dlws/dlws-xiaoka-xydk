<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>校咖汇</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>
	<body>
		<div class="content_wraper">
			<header>
				
				<c:forEach items="${settleList}" var="one">
				<c:if test="${one.typeName=='title'}">
					<p class="putContent_title">${one.typeValue}</p>
				</c:if>
			</c:forEach>
				<span class="putContent_time">${orderMap.invalidTime}</span>
			</header>
			<section>
			<c:forEach items="${settleList}" var="one">
				<c:if test="${one.typeName=='textContent'}">
					<p class="putContent_content">${one.typeValue}</p>
				</c:if>
			</c:forEach>
			    <div class="putContent_picBox">
			    	<c:forEach items="${settleList}" var="one">
					<c:choose>
						<c:when test="${one.typeName=='picUrl'}">
							<c:if test="${not empty picImg}">
								<c:forEach items="${picImg}" var="img" >
									<img src="${img}"/>
								</c:forEach>
							</c:if>
						</c:when>
					</c:choose>
				</c:forEach>
			    </div>
			</section>
			
			<footer>
				<c:if test="${not empty one  && not empty one.typeValue }">
					<div class="putContent_Foot">
						<a href="${one.typeValue}" class="putContent_foot">
						<span>点击链接查看更多&nbsp;&nbsp;:</span>
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='linke'}">
								<span>${one.typeValue}</span>
							</c:if>
						</c:forEach>
					</a>
					</div>
				</c:if>
				<%-- <c:if test="${orderMap.orderStatus == 4 }">
					<div class="read_content already_read">
						<span class="already_color ctt_color">已阅</span>
					</div>
				</c:if>
				<c:if test="${orderMap.orderStatus != 4 }">
					<div class="read_content read">
						<span class="put_color ctt_color">已阅</span>
					</div>
				</c:if> --%>
				<div class="read_content read">
					<span class="put_color ctt_color">已阅</span>
				</div>
				<div class="put_handContent">
					<img src="${path}/xkh_version_2.2/img/head.png"  />
					<span>点击可获得分成哦!</span>
				</div>
			</footer>
			
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
	<script>
		//点击已阅时候
		$(".read_content").click(function(){
			
			var orderId = ${orderId}
			$.ajax({
        	    url:'../skillOrder/updateOrderSuccess.html',
        	    type:'POST', //GET
        	    async:false,    //或false,是否异步
        	    data:{"orderId":orderId,"orderStatus":"4"},
        	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
        	    success:function(data){
        	    	
        	    	var classVal = document.getElementsByClassName("read_content")[0].getAttribute("class");
        			classVal = " " + classVal + " ";
        			classVal = classVal.replace(" read ", " ");
        			document.getElementsByClassName("read_content")[0].setAttribute("class", classVal);
        			classVal = classVal.concat(" already_read");
        			document.getElementsByClassName("read_content")[0].setAttribute("class", classVal);
        			var classval = document.getElementsByClassName("ctt_color")[0].getAttribute("class");
        			classval = " " + classval + " ";
        			classval = classval.replace(" put_color ", " already_color ");
        			document.getElementsByClassName("ctt_color")[0].setAttribute("class", classval);
        	    },
        	    error:function(){
        	    	alert('Ajax error!');
                    // 即使加载出错，也得重置
                    me.resetload();
        	    }
        	})
		})
		
		
		
	</script>
</html>
