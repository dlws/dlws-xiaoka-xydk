<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>我的钱包</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>

	<body>
		<header class="wallet_head">
			<%-- <c:if test="${not empty walletMap }">
				<p><span>￥</span>${walletMap.get("balance") }</p>
				<span class="purse">钱包余额</span>
				<a href="deposit.html" class="deposit">提现</a>
			</c:if> --%>
			<input type="hidden" id="openId" value="${openId }">
			<c:choose>
				<c:when test="${not empty walletMap }">
					<p><span>￥</span>${vBalance }</p>
					<span class="purse">钱包余额</span>
					<c:choose>
						<c:when test="${vBalance !=0 }">
							<a href="<%=basePath%>wallet/deposit.html?openId=${walletMap.get('openId')}" class="deposit">提现</a>
							<%-- <a href="javascript:void(0);" onclick="doDraw()" class="deposit">提现</a> --%>
						</c:when>
						<c:otherwise>
							<a href="javascript:void(0);" class="deposit">提现</a>
						</c:otherwise>
					</c:choose>
					
				</c:when>
				<c:otherwise>
					<p><span>￥</span>0.00</p>
					<span class="purse">钱包余额</span>
					<a href="javascript:void(0);" class="deposit">提现</a>
				</c:otherwise>
			</c:choose>
		</header>
		<section class="deposit_content">
			<!--h3中的交易内容可替换，写成两个h3是为了验收-->
			<c:choose>
				<c:when test="${not empty divideDrawList }">
					<h3><img src="${path}/xkh_version_2.2/img/consult.png" class="deposit_pic" />交易记录</h3>
					<%-- <span>未审核金额${withdrawMoney }</span> --%>
					<div class="wallet_box">
						<c:forEach items="${divideDrawList }" var="divideDraw">
							<a href="<%=basePath%>wallet/orderSharingDetail.html?openId='${divideDraw.openId }'&balance=${divideDraw.divideOrWithdraw}&state=${divideDraw.state}&orderId=${divideDraw.orderId}" class="wallet_deposit">
								<c:choose>
									<c:when test="${divideDraw.state == 1 }">
										<div class="deposit_left">
											<span>提现</span>
											<span><fmt:formatDate  value="${divideDraw.divideOrWithdrawTime }" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></span>
										</div>	
										<span class="deposit_right">-${divideDraw.divideOrWithdraw }</span>
									</c:when>
									<c:otherwise>
										<div class="deposit_left">
											<span>订单分成</span>
											<span><fmt:formatDate  value="${divideDraw.divideOrWithdrawTime }" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></span>
										</div>
										<span class="deposit_right"><span>+${divideDraw.divideOrWithdraw }</span></span>
									</c:otherwise>
								</c:choose>
							</a>
						</c:forEach>
					</div>
				</c:when>
				<c:otherwise>
					<%-- <h3><img src="${path}/xkh_version_2.2/img/consult.png" class="deposit_pic" />暂无交易记录</h3> --%>
					<span class="wallet_photo">
			          <img src="${path}/xkh_version_2.2/img/wallet_invoice.png">
			          <span class="wallet_message">暂无交易记录</span>
			      </span>
				</c:otherwise>
			</c:choose>
		</section>
	</body>
<script type="text/javascript">
	function doDraw(){
		var openId = $("#openId").val();
	}
</script>
</html>