<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
		<!-- 订单状态需要引入的Jsp页面 -->
		<div class="consult_head">
				<c:choose>
						<c:when test="${param.orderStatu eq 1}">
								<div class="head-content">
									<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
									<span class="head_right">等待买家付款，<span class="timmer">${param.timeDiffer}</span>后将自动取消</span>
									<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
								</div>
								<c:set var="status" value="待付款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 2}">
								<div class="head_content">
									<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
									<span class="head_right">买家已付款<span class="timmer">${param.timeDiffer}</span>后将自动取消</span>
									<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
								</div>
								<c:set var="status" value="待服务" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 3}">
								<div class="sellerComit_content">
									<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
									<span class="head_right">等待买家确认收货<span class="timmer">${param.timeDiffer}</span>后将自动确认</span>
									<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
								</div>
								<c:set var="status" value="待确认" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 0}">
							<div class="seller_Content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
									<span class="head_right">您已接单<span>服务中</span></span>
							</div>
							<c:set var="status" value="待确认" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 6}">
								<div class="sellerComit_content2">
									<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
									<span class="head_right">买家申请退款，<span class="timmer">${param.timeDiffer}</span>后将自动确认</span>
									<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
								</div>
								<c:set var="status" value="待退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 11}">
							<div class="header_content4">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
								<span class="head_right"><span>您已拒绝订单，服务已关闭</span></span>
							</div>
							<c:set var="status" value="已关闭" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 9}"><!-- 暂时同意退款也用9来表示 -->
							<div class="header_content3">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
								<span class="head_right"><span>买家已取消，服务已关闭</span></span>
							</div>
							<c:set var="status" value="已关闭" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 7}">
						     	<div class="header_content2">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
								<span class="head_right"><span>您已同意退款</span></span>
							</div>
							<c:set var="status" value="已退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 4}">
							<div class="header_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right"><span>服务已完成</span></span>
							</div>
							<c:set var="status" value="已完成" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 10}">
							<div class="close_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right"><span>买家已取消订单，服务已关闭</span></span>
							</div>
							<c:set var="status" value="已关闭" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 12}">
							<div class="headerRefuse_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
								<span class="head_right scale">已拒绝等待买家重新处理，<span class="timmer">${param.timeDiffer}</span>后将自动关闭</span>
								<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
							</div>
							<c:set var="status" value="待退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 8}">
						<div class="consult_head">
							<div class="success_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right">您已同意退款</span>
							</div>
						</div>
						<c:set var="status" value="已退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 5}">
							<div class="already_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right">已关闭</span><%--订单超时的状态更改为已关闭 --%>
							</div>
							<c:set var="status" value="已关闭" scope="session"></c:set>
						</c:when>
				</c:choose>
			</div>
