<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>交易详情</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>

	<body>
		<!-- <div class="substance_content seller_content">
			<a href="orderDetail.html">
				<div class="substance_left"><img src="img/pro_img.jpg" class="search_pic" /></div>
				<div class="substance_Right">
					<span class="substance_Music">歌声无敌棒</span>
					<span class="substance_Line"><span>线上</span></span>
					<span class="orderDetail_info">广州大二萌妹子一枚，擅长唱流行歌曲,美声,以及民族歌曲,喜欢我就联系我吧。么么...</span>
					<span class="substance_price">20</span><span class="substance_price">元/次</span>
					<span class="sell_Num">[购买数量&nbsp;:&nbsp;1]</span>
				</div>
		</div>
		</a> -->
		<ul class="deposit_detail">
			<li>
				<span>交易金额&nbsp;:&nbsp;${withdrawMoney }元</span>
			</li>
			<c:choose>
				<c:when test="${state == 0 }">
					<li>
						<span>交易类型&nbsp;:&nbsp;订单分成</span>
					</li>
				</c:when>
				<c:otherwise>
					<li>
						<span>交易类型&nbsp;:&nbsp;提现</span>
					</li>
				</c:otherwise>
			</c:choose>
			<li>
				<span>交易时间&nbsp;:&nbsp;${nowDate }</span>
			</li>
			<!-- <li>
				<span>交易单号&nbsp;:&nbsp;45646789987</span>
			</li> -->
		</ul>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js"></script>
	<script>
		//截取前三十个字
		//var html = $(".orderDetail_info").html();
		//var length = $(".orderDetail_info").html().length;
		//if(length > 30) {
		//	$(".orderDetail_info").html(html.substring(0, 30) + '...')
		//}
	</script>

</html>