<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
		<!-- 订单状态需要引入的Jsp页面 -->
		
				<c:choose>
						<c:when test="${param.orderStatu=='1'}">等待买家付款</c:when>
						<c:when test="${param.orderStatu=='2'}">
									等待卖家接单
						</c:when>
						<c:when test="${param.orderStatu=='3'}">
									卖家已服务
						</c:when>
						<c:when test="${param.orderStatu=='0'}">
									卖家已接单
						</c:when>
						<c:when test="${param.orderStatu=='6'}">
									退款申请已提交
						</c:when>
						<c:when test="${param.orderStatu=='11'}">
							         卖家拒绝了您的订单，退款将在一个工作日内退回您的账户
						</c:when>
						<c:when test="${param.orderStatu=='9'}"><!-- 暂时同意退款也用9来表示 -->
								您已取消订单，退款将在一个工作日内退回您的账户
						</c:when>
						<c:when test="${param.orderStatu=='7'}">
								您已取消订单，退款将在一个工作日内退回您的账户
						</c:when>
						<c:when test="${param.orderStatu=='4'}">
								服务已完成
						</c:when>
						<c:when test="${param.orderStatu=='10'}">
								您已取消订单，服务已关闭
						</c:when>
						<c:when test="${param.orderStatu=='12'}">
								卖家拒绝退款，请重新处理
						</c:when>
						<c:when test="${param.orderStatu=='8'}">
								已退款
						</c:when>
						<c:when test="${param.orderStatu=='5'}">
								已超时
						</c:when>
				</c:choose>
