<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>提现页面</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.min.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.picker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>
	<style>
		body{
			background: #f4f4f4;
		}
	</style>
	<body>
		<header class="deposit_head">
			<span>提现金额将会转入您的个人微信账户，请在微信零钱查收。</span>
		</header>
		<section class="deposit_box">
			<h3>提现金额</h3>
			<div class="deposit_price">
				<span>￥</span>
				<input placeholder="请输入提现金额" id="drawMoney" onkeyup="deposit(this.value);"/>
				<input type="hidden" name = "openId" id = "openId" value="${openId }">
			</div>
			<div class="price_content">
				<span class="sum-warn" id="msgError" style="display:none;">输入金额超过零钱余额</span>
				
				<span class="sum" id="walletSpan">钱包余额&nbsp;<span id="walletMoney">${vBalance }</span>元</span>
				<span class="amount" id="allSpan"><span id="allDraw">全部提现</span></span>
			</div>
		</section>
		<div class="bank_content">
			<div class="bank_box mui-input-row" id="bankStyle">
			   <span class="bank">开&nbsp;&nbsp;户&nbsp;&nbsp;银&nbsp;&nbsp;&nbsp;行&nbsp;:&nbsp;</span>
			   <span class="Bank result-tips" name="bank" id="bank" maxlength="50" /></span>
			   <span class="Bank show-result" id="banks"></span>
			</div>
			<div class="bank_box"><span class="bank">开户银行账号&nbsp;:&nbsp;</span><input name="bankCardNumber" id="bankCardNumber" maxlength="50" onkeyup="value=value.replace(/[^\d]/g,'')" /></div>
			<div class="bank_box"><span class="bank">开户银行全称&nbsp;:&nbsp;</span><input name="accountFullName" id="accountFullName" maxlength="50"/></div>
		</div>
		<footer class="deposit_ensure" id="footer">确认提现</footer>
		<!--出来的弹框-->
		<div class="popup_mask"></div>
		<div class="consult_popup">
			<div class="close"><img src="${path}/xkh_version_2.2/img/close.png" class="search_pic" /></div>
			<div class="popup_content">
				<h3>温馨提示</h3>
				<span class="popup_pic"><img src="${path}/xkh_version_2.2/img/style.png" class="search_pic" /></span>
			    <span class="told deposit_told">是否确定提现?<span class="deposit_sold">￥<span id="realMoney"></span>元</span></span>
			   <%-- <a href="<%=basePath%>wallet/depositDetail.html?openId=${openId}&balance=${walletMap.get('balance') }" class="go buyer_go">确定</a> --%>
			   <a href="javascript:void(0);" onclick="draw();" class="go buyer_go">确定</a>
			</div>
		</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.picker.min.js" ></script>
	<script>
	     var h=$(document).height();
		$(".deposit_ensure").click(function(){
			//获取弹出框的高度
					var dm = $("#drawMoney").val();
					dm=Number(dm);
					var openId = $("#openId").val().trim();
					var bankCardNumber = $("#bankCardNumber").val().trim();
					var bank = $("#banks").text().trim();
					var accountFullName = $("#accountFullName").val();
					if(dm == ""){
						alert("请填写金额");
						return ;
					}
					if(dm<0){
						alert("提现金额不能小于零!");
						return ;
					}
					if(bankCardNumber == ""){
						alert("请输入银行卡号!");
						return;
					}
					if(bank == ""){
						alert("请选择银行!");
						return ;
					}
					if(accountFullName == ""){
						alert("请输入开户行全称");
						return ;
					}
					if(dm>0 && bankCardNumber != "" && bank != "" && accountFullName != "" ){
						$.ajax({
							url : 'getByOpenIdAndDrawMoney.html',
							data : {
								"drawMoney" : dm,
								"openId" : openId
							},
							type : 'post',
							cache : false,
							dataType : 'json',
							success : function(data) {
								var flag = data.flag;
								var vBalance = data.vBalance;
								if(flag){
									var popupHeight=$(".consult_popup").height()/2;
									$(".consult_popup").css("margin-top",-popupHeight);
									$(".popup_mask").css("height",h)
									$(".popup_mask").show();
							        $(".consult_popup").show();
							        $("#realMoney").text($("#drawMoney").val());
								}else{
									alert("数据有误请刷新重试!");
									$("#walletMoney").text(vBalance);
								}
							},
							error : function() {
								alert("异常！");
							}
						});
					}else{
						$("#drawMoney").focus();
					}
		});
		$(".popup_mask").click(function(){
			$(".popup_mask").hide();
			$(".consult_popup").hide();
		});
		$(".close").click(function(){
			$(this).parent().hide();
			$(".popup_mask").hide();
		});	
		function deposit(val){
			var drawMoney = val.replace(/[^\d.]/g,"");
			$("#drawMoney").val(drawMoney);
			if(drawMoney == ""){
				return;
			}
			var walletMoney = $("#walletMoney").text();
			drawMoney = Number(drawMoney);
			walletMoney = Number(walletMoney);
			if(drawMoney > walletMoney){
				$("#msgError").css("display","block");
				$("#walletSpan").css("display","none");
				$("#allSpan").css("display","none");
				$("#footer").css("display","none");
			}else{
				$("#msgError").css("display","none");
				$("#walletSpan").css("display","block");
				$("#allSpan").css("display","block");
				$("#footer").css("display","block");
			}
			if(drawMoney.toString().indexOf(".")>0){
				var inVal = drawMoney.toString().split(".");
				var ss = inVal[1];
				if(ss.length>2){
					var aa = ss.substring(0,2);
					var s = inVal[0]+"."+aa;
					$("#drawMoney").val(s);
				}
			}
		}
		$(function(){
			$("#allDraw").click(function(){
				var money = $("#walletMoney").text();
				money = Number(money);
				if(money > 0){
					$("#drawMoney").val(money);
				}
			});
		}); 
		function draw(){
			var balance = $("#drawMoney").val();
			var openId = $("#openId").val();
			var bank = $("#bank").val().trim();
			var bankCardNumber = $("#bankCardNumber").val();
			var accountFullName = $("#accountFullName").val().trim();
			window.location.href = "<%=basePath%>wallet/depositDetail.html?openId="+openId+"&balance="+balance+
					"&bank="+bank+"&bankCardNumber="+bankCardNumber+"&accountFullName="+accountFullName;
		}
		/*  选择开户银行*/
var banklD = new mui.PopPicker({
	layer: 1
});
banklD.setData([{
	value: 0,
	text: "中国农业银行"
}, {
	value: 1,
	text: "中国建设银行"
}, {
	value: 2,
	text: "中国工商银行"
}]);
var showbankStyleButton = document.getElementById('bankStyle');
showbankStyleButton.addEventListener('tap', function(event) {
	banklD.show(function(items) {
		document.querySelector('#bankStyle .result-tips').style.display = "none";
		document.querySelector('#bankStyle .show-result').style.display = "block";
		document.querySelector('#bankStyle .show-result').innerText = items[0].text;
		$(".Bank").css("margin-top",0)
		//返回 false 可以阻止选择框的关闭
		//return false;
		banklD.hide(function(items) {
			if(document.querySelector('#bankStyle .show-result').innerText = "") {
				$(".mui-backdrop").css("display", "none")
			}

		})
	});

}, false);
$("#drawMoney").on("input", function() {
	price = $(this).val();

	var length = $(this).val().length
	if(length != 0) {
		$(this).css({
			"fontSize": "9.6vw",
			"margin": "2px 0 8px"
		})
		$(".deposit_price span").css("margin", "18px 0 0")
	} else {
		$(this).css({
			"fontSize": "3.733vw",
			"marginTop": "6px"
		})
		$(".deposit_price span").css("margin", "0 0 11px")
	}

});
//钱包余额四舍五入保留两位小数
$("#walletMoney").html(Number($("#walletMoney").html()).toFixed(2))
	</script>
</html>
