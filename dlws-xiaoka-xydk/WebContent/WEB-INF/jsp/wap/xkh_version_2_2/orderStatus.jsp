<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
		<!-- 订单状态需要引入的Jsp页面 -->
		<div class="consult_head">
				<c:choose>
						<c:when test="${param.orderStatu eq 1}">
								<div class="head_content">
									<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
									<span class="head_right">订单已生成，<span class="timmer">${param.timeDiffer}</span>后将自动取消</span>
									<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
								</div>
								<c:set var="status" value="待付款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 2}">
								<div class="orderDetail_wait">
									<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
									<span class="head_right">等待卖家接单剩余<span class="timmer">${param.timeDiffer}</span>后将自动取消</span>
									<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
								</div>
								<c:set var="status" value="待服务" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 3}">
								<div class="orderDetail_date">
									<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
									<span class="head_right">卖家已服务<span>${param.timeDiffer}后将自动确认收货</span></span>
									<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
								</div>
								<c:set var="status" value="待确认" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 0}">
							<div class="orderDetail_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right">卖家已接单，<span>服务中</span></span>
								<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
							</div>
							<c:set var="status" value="待确认" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 6}">
								<div class="headerComit_content">
									<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
									<span class="head_right">退款申请已提交，<span class="timmer">${param.timeDiffer}</span>后将自动同意退款</span>
									<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
								</div>
								<c:set var="status" value="待退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 11}">
							<div class="orderDetail_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
								<span class="head_right scale">卖家拒绝了您的订单</span>
								<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
							</div>
							<c:set var="status" value="待退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 9}"><!-- 暂时同意退款也用9来表示 -->
							<div class="headerBuy_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
								<span class="head_right"><span>您已取消订单，退款将在一个工作日内退回您的账户</span></span>
							</div>
							<c:set var="status" value="待退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 7}">
						     	<div class="headerBuy_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
								<span class="head_right"><span>您已取消订单，退款将在一个工作日内退回您的账户</span></span>
							</div>
							<c:set var="status" value="待退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 4}">
							<div class="already_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right"><span>服务已完成</span></span>
							</div>
							<c:set var="status" value="已完成" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 10}">
							<div class="close_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right"><span>您已取消订单，服务已关闭</span></span>
							</div>
							<c:set var="status" value="已关闭" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 12}">
							<div class="headerRefuse_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/plaint.png" class="search_pic" /></div>
								<span class="head_right scale">卖家拒绝退款，请重新处理，<span class="timmer">${param.timeDiffer}</span>后将自动关闭</span>
								<input type="hidden" value="${param.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
							</div>
							<c:set var="status" value="待退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 8}">
						<div class="consult_head">
							<div class="success_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right">退款成功，请留意查收</span>
							</div>
						</div>
						<c:set var="status" value="已退款" scope="session"></c:set>
						</c:when>
						<c:when test="${param.orderStatu eq 5}">
							<div class="already_content">
								<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
								<span class="head_right">订单超时</span><%--订单超时的状态更改为已关闭 --%>
							</div>
							<c:set var="status" value="已关闭" scope="session"></c:set>
						</c:when>
				</c:choose>
			</div>
