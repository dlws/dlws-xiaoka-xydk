<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>系统消息</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>
	<body style="background: #ebebeb;">
		<div class="communicate_content">
			<div class="newsInfo_content">
			<c:forEach items="${sysList }" var="sys">
			
				<!--系统消息 消息详情-->
				<c:if test="${sys.otherOpenId=='0' }">
					<%-- <div class="newsInfo_message">
						<span class="order">系统消息${sys.newsId }，系统消息--暂时未做详情--待确定</span>
						<div class="searchLook">
							<a href="orderDetail.html" class="green">查看更多</a>
							<a href="orderDetail.html"><img src="${path}/xkh_version_2.2/img/right.png" /></a>
						</div>
					</div> --%>
				</c:if>
				<!--订单消息 订单详情 newsId 作为orderId 查订单详情 根据订单的状态 -->
				<c:if test="${sys.otherOpenId==myOpenId }">
				 <c:if test="${sys.orderId!=0}">
				 	<div class="communicate_time newsInfo_content"><span>${sys.messageTimeStr }</span></div>
					<div class="newsInfo_message">
						<span class="order">您的订单${sys.orderId}，
							 <c:import url="sysOrderStatus.jsp" >
								<c:param name="orderStatu" value="${sys.contentChat}"></c:param>
							</c:import>
						</span>
						<div class="searchLook">
							<a href="${path }/chatMesseage/queryOrderDetail.html?orderId=${sys.orderId}" class="green">查看更多</a>
							<a href="${path }/chatMesseage/queryOrderDetail.html?orderId=${sys.orderId}"><img src="${path}/xkh_version_2.2/img/right.png" /></a>
						</div>
					</div>
				 </c:if>
				</c:if>
			</c:forEach>
				<%-- <div class="communicate_time newsInfo_content"><span>18:00</span></div>
				<!--针对买家-->
				<div class="newsInfo_message">
					<span class="order">您的订单2017004061016，卖家已接单</span>
					<div class="searchLook">
						<a href="orderDetail.html" class="green">查看更多</a>
						<a href="orderDetail.html"><img src="${path}/xkh_version_2.2/img/right.png" /></a>
					</div>
				</div>
				<!--针对卖家-->
				<div class="communicate_time newsInfo_content"><span>18:00</span></div>
				<div class="newsInfo_message">
					<span class="order">您有新的订单，请及时处理</span>
					<div class="searchLook">
						<a href="orderDetail.html" class="green">查看更多</a>
						<a href="orderDetail.html"><img src="${path}/xkh_version_2.2/img/right.png" /></a>
					</div>
				</div> --%>
			</div>
		</div>
	</body>
</html>
