<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<title>我的</title>
	</head>

	<body>
		<input type="hidden" id="id" name="id" value="${map.id}">
		<header class="user_top">
			<img src="${path}/xkh_version_2.2/img/myBg.png" class="search_pic" />
			<c:choose>
				<c:when test="${reduMap.headImgUrl!=''&&reduMap.headImgUrl!= null}">
					<div class="user_pic"><img style="border-radius:100%;" src="${reduMap.headImgUrl}" class="search_pic" /></div>
				</c:when>
				<c:otherwise>
					<div class="user_pic"><img  src="${path}/xkh_version_2/img/wxHead.png"  style="border-radius:100%;" class="search_pic"/></div>
				</c:otherwise>			
			</c:choose>
			
			<c:choose>
				<c:when test="${reduMap.wxname!=''&&reduMap.wxname!=null}">
					<span class="user_name">${reduMap.wxname}</span>
				</c:when>
				<c:otherwise>
					<span class="user_name">游客</span>
				</c:otherwise>			
			</c:choose>
			
			
			<a href="editor.html">
				<span class="user_message"><img src="${path}/xkh_version_2.2/img/edit.png" class="search_pic" /></span>
			</a>
		</header>
		<section>
		<!--服务管理这一部分只有当审核通过以后才会出现-->
			<c:if test="${map.auditState=='1'}">
			<div class="user_function" style="border:0;">
				<h3 class="user_h3">我的服务</h3>
				<a href="${path }/personInfoDetail/detail.html?id=${map.id}" class="buyer_look">
					<span><img src="${path}/xkh_version_2.2/img/right.png" class="search_pic" /></span>
				</a>
			</div>
			<div class="user_bg"></div>
			<div class="buyer_Box">
				<div class="user_function">
					<h3 class="user_h3">服务管理</h3></div>
				<div class="user_content">
					<div class="user_picContent buyer_border">
						<a href="${path }/pubSkill/toPubSkillMan.html?basicId=${map.id}">
						<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/person_05.png" class="search_pic" /></span>
						<span class="user_size">发布服务</span>
						</a>
					</div>
					<div class="user_picContent buyer_border">
						<a href="${path }/skillUser/toSkillInfoV2.html">
							<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/person_06.png" class="search_pic" /></span>
							<span class="user_size">服务管理</span>
						</a>
					</div>
					<div class="user_picContent buyer_border">
						<a href="${path }/orderseaSell/searchOrderSell.html?tabNum=0">
						<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/person_07.png" class="search_pic" /></span>
						<span class="cfz_circle ctt_circle1"></span>
						<span class="user_size">订单管理</span>
						</a>
					</div>
					<div class="user_picContent buyer_border right_none">
						<a href="${path }/wallet/myWallet.html">
						<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/person_08.png" class="search_pic" /></span>
						<span class="user_size">我的钱包</span>
						</a>
					</div>
					<div class="user_picContent buyer_border">
						<a href="${path}/userSettled/toEnterUser.html?myself=yes">
						<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/person_09.png" class="search_pic" /></span>
						<span class="user_size">我的信息</span>
						</a>
					</div>
					
					<div class="user_picContent buyer_border">
						<a href="${path}/change/changeManager.html">
							<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/person_10.png" class="search_pic" /></span>
							<span class="user_size">管理员更换</span>
						</a>
					</div>
				</div>
			</div>
			<div class="user_bg"></div>
			</c:if>
			<!--我的订单部分-->
			<div class="service_manage">
				<div class="user_function">
					<h3 class="user_h3">我的订单</h3>
					<a href="<%=basePath%>ordersea/searchOrderSell.html?tabNum=0" class="buyer_look">
						<span>查看全部订单</span>
						<span><img src="${path}/xkh_version_2.2/img/right.png" class="search_pic" /></span>
					</a>
				</div>
				<div class="user_content">
					
					<div class="user_picContent">
						<a href="<%=basePath%>ordersea/searchOrderSell.html?tabNum=1">
						<span class="user_picture"><img src="${path}/xkh_version_2.2/img/person_01.png" class="search_pic" /></span>
						<span class="cfz_circle ctt_circle"></span>
						<span class="user_size">待付款</span>
						</a>
					</div>
					<div class="user_picContent">
						<a href="<%=basePath%>ordersea/searchOrderSell.html?tabNum=2">
						<span class="user_picture"><img src="${path}/xkh_version_2.2/img/person_02.png" class="search_pic" /></span>
						<span class="cfz_circle ctt_circle"></span>
						<span class="user_size">待服务</span>
						</a>
					</div>
					
					<div class="user_picContent">
						<a href="<%=basePath%>ordersea/searchOrderSell.html?tabNum=3">
						<span class="user_picture"><img src="${path}/xkh_version_2.2/img/person_03.png" class="search_pic" /></span>
						<span class="cfz_circle ctt_circle circle1"></span>
						<span class="user_size">待确认</span>
						</a>
					</div>
					<div class="user_picContent user_picR">
						<a href="<%=basePath%>ordersea/searchOrderSell.html?tabNum=4">
						<span class="user_picture"><img src="${path}/xkh_version_2.2/img/person_04.png" class="search_pic" /></span>
						<span class="cfz_circle ctt_circle"></span>
						<span class="user_size">待退款</span>
						</a>
					</div>
				</div>
				<div class="user_bg"></div>
			</div>
			<div class="buyer_Box">
				<!--功能管理部分-->
				<div class="user_function">
					<h3 class="user_h3">功能管理</h3></div>
				<div class="user_content buyer_content">
					<div class="user_picContent buyer_border">
						<a href="${path }/invoice/invoice.html">
							<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/user_img0.png" class="search_pic" /></span>
							<span class="user_size">我的发票</span>
						</a>
					</div>
					<div class="user_picContent buyer_border">
						<a href="https://jinshuju.net/f/W4u6uC">
							<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/user_img1.png" class="search_pic" /></span>
							<span class="user_size">用户反馈</span>
						</a>
					</div>
					<a href="https://static.meiqia.com/dist/standalone.html?eid=50806" class="user_picContent buyer_border">
						<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/user_img2.png" class="search_pic" /></span>
						<span class="user_size">在线咨询</span>
					</a>
					<a href="http://mp.weixin.qq.com/s/wDEs84EoA2p4iovtQVDXJg" class="user_picContent buyer_border right_none">
						<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/user_img3.png" class="search_pic" /></span>
						<span class="user_size">常见问题</span>
					</a>
					<a href="http://mp.weixin.qq.com/s/t2a6PYa574RwnWX1WBnhJQ" class="user_picContent buyer_border">
						<span class="user_picture ctt_userPic"><img src="${path}/xkh_version_2.2/img/user_img4.png" class="search_pic" /></span>
						<span class="user_size">关于平台</span>
					</a>
				</div>
				<!-- <div class="user_bg"></div> -->
			</div>
			
			
			<!--审核中 部分-->
			 <c:choose> 
				<c:when test="${map.auditState=='0'}"> 
					<div class="buyer_Box">
							<a href="${path }/highSchoolSkill/toAllUpdateUser.html?myself=no">
								<div class="in_review">校园资源入驻(审核中)</div>
							</a>
					</div>
				</c:when> 
				 <c:when test="${map.auditState=='1'}"> 
					 <!-- <div class="buyer_Box">
						 <div class="no_review">
						 <p>校园资源入驻(审核成功)</p>
						 </div>
					 </div> -->
				</c:when>  
				<c:when test="${map.auditState=='2'}"> 
					<div class="buyer_Box">
						<a href="${path }/userSettled/toEnterUser.html?myself=no">
							<div class="no_review">
								<p>+ 校园资源入驻</p>
								<p>(审核不通过,请重新提交)</p>
							</div>
						</a>
					</div>
				</c:when> 
				<c:when test="${map.auditState=='3'}"> 
					<a href="${path }/userSettled/showAgreement.html">
						<div class="buyer_Box">
							<div class="no_review">
								<p>+ 校园资源入驻</p>
								<p>(未入驻)</p>
							</div>
						</div>
					</a>
				</c:when> 
				<c:otherwise>
				<%-- <a href="${path }/skillUser/chooseStyle.html"> --%>
						<div class="buyer_Box">
							<div class="no_review">
								<p>当前用户异常</p>
								<p>(未入驻)</p>
							</div>
						</div>
				<!-- </a> -->
				</c:otherwise>
	    </c:choose>
		</section>
		<!-- 底部 -->
		 <%@ include file="../xkh_version_2/foot.jsp"%>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
    <script>
    	var path = "${path}";
    	//消息在第一序列
    	var index = 4;
    	var h = $(".foot-bar-tab").height();
		$("section .buyer_Box:last-child").css("margin-bottom", h);
		//底部选项卡切换
		$(".foot-bar-tab a:not(.refuse_publish)").click(function() {
			$(this).addClass("foot-active").siblings().removeClass("foot-active");
		})
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/foot.js" ></script>
</html>
