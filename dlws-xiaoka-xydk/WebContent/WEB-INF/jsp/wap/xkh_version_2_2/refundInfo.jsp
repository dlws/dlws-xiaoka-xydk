<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.picker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.dtpicker.css" />
		<title>退款信息录入</title>
	</head>

	<body>
	<form action="<%=basePath%>ordersea/addRefundInfo.html" id="OrderForm" method="post">
		
		<ul class="accurate_content">
		
			<li>
				<span class="accurate-title">退款金额</span>
				<input  name="refundsMoney" id="refundsMoney" class="accurate_inp" value="${refundMoney }"/>
			</li>
			
			<li>
				<span class="accurate-title">开户银行</span>
				<div id="accurate" class="mui-input-row">
					<span class="accurate_inp result-tips gray">请选择开户银行</span>
					<span class="accurate_inp show-result gray"></span>
					<input type="hidden" name="bank" id="bank" value=""> 
				</div>

			</li>
			
			<li>
				<span class="accurate-title">银行卡号</span>
				<input placeholder="请输入银行卡号" name="bankCardNumber" id="bankCardNumber" class="accurate_inp" />
			</li>
			<li>
				<span class="accurate-title">开户支行</span>
				<input placeholder="请输入开户行" name="AccountFullName" id="AccountFullName" class="accurate_inp" />
			</li>
			
		</ul>
		<div class="selected_bg accurate_bg"></div>
		<div class="selected_bg accurate_bg"></div>
		<div class="footer" onclick="sub();"><span>提交</span></div>
		
		<input type="hidden" name="refundsOrderId" id="refundsOrderId" value="${orderId }">
		<input type="hidden" name="checkStatus" id="checkStatus" value="${checkStatus }">
		<%-- 增加传参信息url --%>
		<input type="hidden" name="url" id="url" value="${url}">
		</form>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.min.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.picker.js"></script>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.poppicker.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.dtpicker.js" ></script>
	<script>
	function sub(){
		var bank=$.trim($("#bank").val());
		var bankCardNumber=$.trim($("#bankCardNumber").val());
		var phone=$.trim($("#phone").val());
		var endDate=$.trim($("#endDate").val());
		if(bank==""){
			alert("请选择开户银行!")
			return;
		}
		if(bankCardNumber==""){
			alert("请输入银行卡号!")
			return;
		}
		if(AccountFullName==""){
			alert("请输入开户支行!")
			return;
		}
		$("#OrderForm").submit();
	}

	var provide = new mui.PopPicker({
			layer: 1
		});
	var bankList = ${jsonList};
	provide.setData(bankList);
		var showprovideButton = document.getElementById('accurate');
		showprovideButton.addEventListener('tap', function(event) {
			provide.show(function(items) {
				document.querySelector('#accurate .result-tips').style.display = "none";
				document.querySelector('#accurate .show-result').style.display = "block";
				document.querySelector('#accurate .show-result').innerText = items[0].text;
				$("#bank").val(items[0].text);
				//返回 false 可以阻止选择框的关闭
				//return false;
			});
		}, false);
		//选择投放版位
	</script>

</html>
