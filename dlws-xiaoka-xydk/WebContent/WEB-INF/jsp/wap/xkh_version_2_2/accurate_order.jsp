<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/iconfont/iconfont.css"/>
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
		<title>下单详情</title>
	</head>

	<body>
	<input type="hidden" name="orderStatus" id="orderStatus" value="${orderMap.orderStatus}">
	<input type="hidden" name="orderId" id="orderId" value="${orderMap.orderId}">
		<!--订单未支付时候出现-->
		<c:if test="${orderMap.orderStatus==1}">
			<div id="statusF">
			<div class="consult_head">
				<div class="head_content">
					<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
					<span class="head_right">订单已生成，<span class="timmer">${orderMap.timeDiffer}</span>后将自动关闭</span>
					<input type="hidden" value="${timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
				</div>
			</div>
			</div>
		</c:if>
		<!-- 1：下单成功，2：支付成功（也就是待服务）、0：已接单、11：拒绝接单、 3：待确认（已服务）、 4：已确认（已完成），5：超时失效，
		 6：申请退款、7：待退款、8：退款成功 9 取消订单、10已关闭 -->
		
		<!--订单支付审核时候出现-->
		<c:if test="${orderMap.orderStatus==2}">
			<div class="consult_head">
				<div class="auditing_content">
					<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
					<span class="head_right">正在审核内容，通过后将自动投放</span></span>
				</div>
			</div>
		</c:if>
		
		<!--已审核未到时间-->
		<c:if test="${orderMap.orderStatus==3}">
			<div class="consult_head">
				<div class="auditing_content">
					<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
					<span class="head_right">内容已审核通过，届时将自动投放</span></span>
				</div>
			</div>
		</c:if>

		<!--已完成内容-->
		<c:if test="${orderMap.orderStatus==4}">
			<div class="consult_head">
				<div class="already_content">
					<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
					<span class="head_right">内容已投放</span></span>
				</div>
			</div>
		</c:if>
		
		
		<!--超时失效-->
		<c:if test="${orderMap.orderStatus==5}">
			<div class="consult_head">
				<div class="close_content">
					<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
					<span class="head_right">已取消订单，服务已关闭</span></span>
				</div>
			</div>
		</c:if>
		
		<!--内容审核不通过-->
		<c:if test="${orderMap.orderStatus==6}">
		<div class="consult_head">
			<div class="fail_content">
				<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
				<span class="head_right">内容审核不通过，退款将在一个工作日内退回到您的账户</span></span>
			</div>
		</div>
		</c:if>
		<!--退款完成时候-->
		<c:if test="${orderMap.orderStatus==8}">		
			<div class="consult_head">
				<div class="success_content">
					<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
					<span class="head_right">退款成功，请留意查收</span></span>
				</div>
			</div>
		</c:if>
		<!--尚未支付就取消-->
		<c:if test="${orderMap.orderStatus==9}">
		<div class="consult_head">
			<div class="close_content">
				<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
					<span class="head_right">您已取消订单，服务已关闭</span>
			</div>
		</div>
		</c:if>
		
		
		<div class="publish_case">
			<div class="publish_content">
                <!--待支付时候-->
				<div class="publish_title">
					<span class="title_pic"><img src="${baseInfo.headPortrait}" class="search_pic" /></span>
					<h3>${baseInfo.nickname}</h3>
					<c:if test="${orderMap.orderStatus==1}">
						<span class="edit">待付款</span>
					</c:if>
					<c:if test="${orderMap.orderStatus==2}">
						<span class="edit">待服务</span>
					</c:if>
					<c:if test="${orderMap.orderStatus==3}">
						<span class="edit">已服务</span>
					</c:if>
					<c:if test="${orderMap.orderStatus==4}">
						<span class="edit">已完成</span>
					</c:if>
					<c:if test="${orderMap.orderStatus==5}">
						<span class="edit">已关闭</span>
					</c:if>
					<c:if test="${orderMap.orderStatus==6}">
						<span class="edit">待退款</span>
					</c:if>
					<c:if test="${orderMap.orderStatus==8}">
						<span class="edit">已退款</span>
					</c:if>
					<c:if test="${orderMap.orderStatus==9}">
						<span class="edit">已关闭</span>
					</c:if>
				</div>
			</div>
			<input type="hidden" name="skillId" value="${skillInfo.skillId}">
			<input type="hidden" name="skillPrice" value="${skillInfo.skillPrice}">
			<input type="hidden"   name="sellerOpenId" id="sellerOpenId" value="${orderMap.sellerOpenId}">
			<div class="substance_content">
				<div class="substance_left"><img src="${skillInfo.imageUrl}" class="search_pic" /></div>
				<div class="substance_Right">
					<span class="substance_Music">${skillInfo.skillName}</span>
					<c:if test="${skillInfo.serviceType==1}">
						<span class="substance_Line"><span>线上</span></span>
					</c:if>
					<c:if test="${skillInfo.serviceType==2}">
						<span class="substance_Line"><span>线下</span></span>
					</c:if>
					<p>${skillInfo.skillDepict}</p>
					<span class="substance_price">${skillInfo.skillPrice}</span><span class="substance_price">元/次</span>
					<span class="sell_Num">[已售${skillInfo.salesVolume}]</span>
				</div>

			</div>
			<div class="selected_bg accurate_bg"></div>
		</div>
		<!-- 增加展示打电话和聊一聊功能 -->
		<c:if test="${orderMap.enterType == 'sqdv'}">
		<c:if test="${orderMap.orderStatus==0 ||orderMap.orderStatus==3||orderMap.orderStatus==6||orderMap.orderStatus==7||orderMap.orderStatus==12}">
				<p class="buyConsult">咨询权限保留24小时，可直接与卖家沟通</p>
				<div class="chart_content">
				<c:forEach items="${settleList}" var="one">
					<c:choose>
						<c:when test="${one.typeName=='sellPhone'}">
							<span class="accurate_inp orderDetail_number"></span>
							<div class="telphone">
								<a href="tel:${one.typeValue }">
								<span><img src="${path}/xkh_version_2.2/img/tell.png" class="search_pic" /></span>
								<span>打电话</span>
								</a>
							</div>
							<a class="telphone chat" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${orderMap.sellerOpenId}">
								<span><img src="${path}/xkh_version_2.2/img/chart.png" class="search_pic" /></span>
								<span>聊一聊</span>
							</a>
						</c:when>
					</c:choose>
				</c:forEach>
				</div>
		</c:if>
		</c:if>
		<!--投放内容-->
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/accurtate.png" class="search_pic" /></div>
				<span class="info">投放内容</span>
			</div>
			<c:forEach items="${settleList}" var="one">
				<c:if test="${one.typeName=='title'}">
					<p class="accurtate_title">${one.typeValue}</p>
				</c:if>
			</c:forEach>
			<div class="introduce_new">
			<c:forEach items="${settleList}" var="one">
				<c:if test="${one.typeName=='textContent'}">
					<div class="about_content p" id="box">${one.typeValue}</div>
				</c:if>
			</c:forEach>
				<span class="watch"></span>
			</div>
			
			<!--添加的图片-->
			<div class="webservice_content">
				<!--添加的图片-->
				<c:forEach items="${settleList}" var="one">
					<c:choose>
						<c:when test="${one.typeName=='picUrl'}">
							<c:if test="${not empty picImg}">
								<c:forEach items="${picImg}" var="img" >
									<div class="picture_box">
										<div class="add_picture">
											<img src="${img}" class="search_pic" />
											<span class="iconfont icon-shanchu"></span>
										</div>
									</div>
								</c:forEach>
							</c:if>
						</c:when>
					</c:choose>
				</c:forEach>
				<!--<添加的图片完-->
				<!-- <div class="add_pic">
					<span class="iconfont icon-pic"></span>
					<span class="size">+添加图片</span>
				</div> -->
			</div>
			<c:forEach items="${settleList}" var="one">
				<c:if test="${one.typeName=='linke'}">
					<span class="lianjie">链接:&nbsp;${one.typeValue}</span>
				</c:if>
			</c:forEach>
		</div>
		<div class="selected_bg accurate_bg"></div>
		<!--买家信息-->
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/buyer.png" class="search_pic" /></div>
				<span class="info">买家信息</span>
			</div>
			<div class="info-content">
				<span >联&nbsp;&nbsp;系&nbsp;&nbsp;人&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.linkman}</span>
			</div>
			<div class="info-content info-content2">
				<span >联系电话&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.phone}</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span >买家留言&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<div class="introduce_new">
				<div class="about_content p" id="box">${orderMap.message}</div>
				<strong class="watch"></strong>
			</div>
			</div>
		</div>	
		<!--订单信息-->
		<div class="selected_bg accurate_bg"></div>
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/consult.png" class="search_pic" /></div>
				<span class="info">订单信息</span>
			</div>
			<div class="info-content">
				<span >订单编号&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.orderId}</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span >下单时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span><fmt:formatDate  value="${orderMap.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></span>
			</div>
		</div>
		<div class="selected_bg accurate_bg"></div>

		<div class="accurate_box">
			<div class="info-content info-content2">
				<span class=" accurate_left">投放时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span class="green" ><fmt:formatDate  value="${orderMap.startDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></span>
				
			</div>
			<div class="info-content info-content2">
				<span class=" accurate_left">合计&nbsp;:&nbsp;</span>
				<span class="green">${orderMap.payMoney}元</span>
			</div>
			<c:if test="${orderMap.enterType == 'jztf'}">
			<!--提交订单未支付时候出现-->
			<c:if test="${isMySelf}">
				<c:if test="${orderMap.orderStatus==1}">
					<div class="total_content none_border">
						<div class="order_content">
							<a href="#" onclick="cancelOrder();">取消订单</a>
							<a href="#" onclick="toPay('${orderMap.orderId}','${orderMap.payMoney }')" class="green Green">立即付款</a>
						</div>
					</div>
				</c:if>
			</c:if>
			<%-- 如果当前OpenId与OrderSellerId相同时 --%>
			<c:import url="seller_operation.jsp">
				<c:param name="sellerOpenId" value="${orderMap.sellerOpenId}"></c:param>
				<c:param name="orderStatus" value="${orderMap.orderStatus}"></c:param>
				<c:param name="orderId" value="${orderMap.orderId}"></c:param>
			</c:import>
			</c:if>
			<!-- 共用社群大V其他的页面 -->
			<c:if test="${orderMap.enterType == 'sqdv'}">
				<%-- 如果当前OpenId与OrderSellerId相同时 --%>
				<c:import url="buyer_operation.jsp">
					<c:param name="openId" value="${orderMap.openId}"></c:param>
					<c:param name="orderStatus" value="${orderMap.orderStatus}"></c:param>
					<c:param name="orderId" value="${orderMap.orderId}"></c:param>
					<c:param name="payMoney" value="${orderMap.payMoney}"></c:param>
					<c:param name="sellerRefuse" value="${feedBackBean.sellerRefuse}"></c:param>
					<c:param name="reseon" value="${feedBackBean.reseon}"></c:param>
					<c:param name="feedbackstatus" value="${feedBackBean.feedbackstatus}"></c:param>
					<c:param name="sellerRefuse" value="${feedBackBean.sellerRefuse}"></c:param>
					<c:param name="id" value="${feedBackBean.id}"></c:param>
					<c:param name="path" value="${path}"></c:param>
				</c:import>
				
				<%-- 如果当前OpenId与OrderSellerId相同时 --%>
				<c:import url="seller_operation.jsp">
					<c:param name="sellerOpenId" value="${orderMap.sellerOpenId}"></c:param>
					<c:param name="orderStatus" value="${orderMap.orderStatus}"></c:param>
					<c:param name="orderId" value="${orderMap.orderId}"></c:param>
					<c:param name="feedBack" value="${feedBack}"></c:param>
					<c:param name="buyerReject" value="${feedBackBean.buyerReject}"></c:param>
					<c:param name="suggestion" value="${feedBackBean.suggestion}"></c:param>
					<c:param name="path" value="${path}"></c:param>
				</c:import>
			</c:if>
		</div>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.min.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.picker.min.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/mui.dtpicker.js" ></script>
    <script>
    
    var path = "${path}";
    function cancelOrder(){
    		var orderId=$("#orderId").val();
    		var sellerOpenId=$("#sellerOpenId").val();
			if(confirm("确定取消订单?")){
					//表示修改需要数据库删除
					$.ajax({
						type : "post",
						url : "<%=basePath%>skillOrder/cancelOrder.html",
						dataType : "json",
						data : {orderId:orderId,orderStatus:9,nowOpenId:sellerOpenId},
						success : function(data) {
							/* $(".head_right").html("您已取消订单，服务已关闭");
							$(".head_right").parents(".head_content").addClass("close_content").removeClass("head_content");
							$(".edit").html("已关闭");
							$(".order_content").hide(); */
							location.reload();
						}
					});
		  }
    }
    
    function toPay(orderId,payMoney){
//     	alert("orderId:"+orderId+"**********payMoney:"+payMoney)
		window.location.href="${path}/ordersea/updateOrderSelltoPay.html?orderId="+orderId+"&payMoney="+payMoney;
	}
    
    function pay(){
    	var orderId=$("#orderId").val();
    	var skillId=$("input[name='skillId']").val();
    	$.ajax({
			type : "post",
			url : "<%=basePath%>skillOrder/skillOrderPay.html",
			dataType : "json",
			data : {orderId:orderId,skillId:skillId,solnum:1,enterType:'jztf',orderStatus:2},
			success : function(data) {
				/* $(".head_right").parents(".head_content").addClass("auditing_content").removeClass("head_content");
				$(".edit").html("待服务"); */
				location.reload();
			}
		});
    }
    //控制字数显示的
	$(document).ready(function(){
                 function show(){
					var btn = document.getElementsByClassName("watch");
					var p = document.getElementsByClassName("p");
					
					var arr=[];
					for(var i=0;i<btn.length;i++){
						(function(i) {
							var text = p[i].innerHTML;
							arr.push(text)
							var length=$(".p").eq(i).html().length;
							var html=$(".p").eq(i).html();
							if(length>60){
								$(".p").eq(i).siblings(".watch").addClass("class_up");
								 $(".p").eq(i).html(html.substring(0,60)+"...");
								 btn[i].onclick = function(){
										var newBox = document.createElement("div");
									 $(this).toggleClass("class_up").toggleClass("class_down")
									newBox.innerHTML=arr[i];
									if($(this).hasClass("class_down")) {
										newBox.innerHTML = arr[i];
									} else{
										if($(".p").eq(i).html().length>60){
											newBox.innerHTML = arr[i].substring(0, 60) + "...";
										}else{
											newBox.innerHTML = arr[i];
										}								
									}
								 	  p[i].innerHTML = ""; 
									 p[i].appendChild(newBox); 
									
								} 
							}
						
						
						})(i)
					}
				}
				show();
	})	
	
	/**待 服 务 */
	//1.拒绝接单
	function  refuseSell(obj){
		$(obj).parents('li').hide();
		var value = $(obj).siblings("input[name='orderId']").val();
		var json = {"orderId":value};
		$.ajax({
			  url: "${path}/orderseaSell/updateOrderSellDfuRefuse.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data);
			    //location.reload();//刷新当前请求  
			    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
			  },
			  fail: function (err, status) {
			    console.log(err);
			  }
			})
		$(obj).parent().removeAttr("href");
	}
	//2. 接单 
	function  accept(obj){
		$(obj).parents('li').hide();
		var value = $(obj).siblings("input[name='orderId']").val();
		var json = {"orderId":value};
		$.ajax({
			  url: "${path}/orderseaSell/updateOrderSellAccept.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data);
			    //location.reload();//刷新当前请求  
			    window.location.href="${path}/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
			  },
			  fail: function (err, status) {
			    console.log(err);
			  }
			})
		$(obj).parent().removeAttr("href");
	}
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/commen.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/orderDetailSeller.js"></script>
</html>
