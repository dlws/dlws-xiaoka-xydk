﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<title>已选</title>
	</head>

	<body>
		<ul class="selected_content">
		<c:forEach items="${list}" var="one" varStatus="stat">
<%-- 	${one.skillId}	//${one.openId}	  --%>				
			<li>
				<a href='${path}/personInfoDetail/detail.html?id=${one.basiId}' class="a">
					<div class="publish_content">
						<div class="publish_title">
							<span class="title_pic"><img src="${one.headPortrait}" class="search_pic" style="border-radius:50%;"/></span>
							<c:choose>
							 <c:when test="${one.nickname=='null'||one.nickname==''}">
								<h3>${one.userName}</h3>
							 </c:when>
							 <c:otherwise>
								<h3>${one.nickname}</h3>
							 </c:otherwise>
							</c:choose>
						</div>
					</div>
				</a>
				
					<c:choose>
						<c:when test="${one.isPreferred eq 1}">
							<div href="${path}/skill_detail/getskill_detail.html?skillId=${one.skillId}&classValue=${one.enterType}&personId=${one.basiId}" class="a selhref">
						</c:when>
						<c:otherwise>
							<div href="${path}/skill_detail/getskill_detail.html?skillId=${one.skillId}&classValue=${one.enterType}" class="a selhref">
						</c:otherwise>
					</c:choose>
					<div class="substance_content">
						<div class="substance_left"><img src="${one.imgUrl}" class="search_pic" /></div>
						<div class="substance_Right">
							<span class="substance_Music" >${one.skillName}</span>
							<input type="hidden" class="delMusic" value="${one.seledId}">
							<c:if test="${one.serviceType==1}">
								<span class="substance_Line"><span>线上</span></span>
							</c:if>
							<c:if test="${one.serviceType==2}">
								<span class="substance_Line"><span>线下</span></span>
							</c:if>
							<p>${one.skillDepict}</p>
							<div class="add_Price">
							<span class="substance_price">${one.price}</span><span class="substance_price">元/次</span>
							</div>
							<span class="sell_Num">[已售${one.salesVolume}]</span>
						</div>
						<!--出来的删除-->
						<div class="selected_del" >删除</div>
					</div>
				</div>
				<div class="balance_content">
					<a href="javascript:void(0);" class="immediate balance">结算</a>
				</div>
				<input type="hidden" class="enterTypeValue" value="${one.typeValue}">
				<input type="hidden" class="skillId" value="${one.skillId}">
				<input type="hidden" class="enterId" value="${one.enterId}">
				<input type="hidden" class="openId" value="${one.openId}">
				<input type="hidden" class="price" value="${one.price}">
				<input type="hidden" class="isPreferred" value="${one.isPreferred}">
				<input type="hidden" class="basiId" value="${one.basiId}">
				<div class="selected_bg"></div>
			</li>
		</c:forEach>
		</ul>
		<%@ include file="../xkh_version_2/foot.jsp" %>
		<!-- 全局变量 -->
		<input type="hidden" id="skillIdTwo" name="skillIdTwo" value="">
		<input type="hidden" id="enterIdTwo" name="enterIdTwo" value="">
		<input type="hidden" id="openIdTwo" name="openIdTwo" value="">
		<input type="hidden" id="priceTwo" name="priceTwo" value="">
		<input type="hidden" id="enterTypeValueTwo" name="enterTypeValueTwo" value="">
		<input type="hidden" id="isPreferred" name="isPreferred" value="">
		<input type="hidden" id="basiId" name="basiId" value="">
		<!--2.2.版本出来的弹出框-->
			<div class="popup_mask"></div>
			<div class="consult_popup">
				<div class="close"><img src="${path }/xkh_version_2.2/img/close.png" class="search_pic" /></div>
				<div class="popup_content">
					<h3>温馨提示</h3>
					<span class="popup_pic"><img src="${path }/xkh_version_2.2/img/style.png" class="search_pic" /></span>
				    <span class="told">为避免打扰用户，如需在线咨询，请先下单支付咨询费用。</span>
				    <span class="sold">￥&nbsp;
				    	<span id="asd">0</span>元</span>
				    <span class="sold1">(支付后即可与卖家直接沟通哦~)</span>
				   <span class="go"><span class="immediately" href="javascript:void(0);" onclick="gotoPayMoney()">立即<br/>支付</span></span>
				</div>
			</div>
			<!-- 06-13當前沒有選擇時候出現 -->
			<div class="bank_content">
			   <span class="bank_photo">
			     <img src="${path}/xkh_version_2.2/img/bank_choose.png" class="search_pic" />
			   </span>
			   <span class="bank_message">空空如也~</span>
			   <a href="${path}/home/index.html" class="bank_go">去逛逛</a>
			</div>
			<!-- 07-07是否刪除確認框 -->
			<div class="cfz_removeContent">
			   <p>确定删除?</p>
			   <div class="cfz_choose">
			     <span>取消</span>
			     <span>确定</span>
			   </div>
			</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js"></script>
	<script>
		var path = "${path}";
		//消息在第一序列
		var index = 3;
		//结算，立即咨询使用
		$(document).ready(function(){
			$(".bank_content").hide();
			var liArray = $(".selected_content").children("li");
			if(liArray.length == 0){
				$(".bank_content").show();
			}
		});
		//获取弹出框的高度
		var popupHeight=$(".consult_popup").height()/2;
		$(".consult_popup").css("margin-top",-popupHeight);
		//点击立即咨询
		$(".immediate").click(function(){
			
			//赋值
			var vskillId=$(this).parent().siblings(".skillId").val();
			var venterId=$(this).parent().siblings(".enterId").val();
			var vopenId=$(this).parent().siblings(".openId").val();
			var vprice=$(this).parent().siblings(".price").val();
			var venterTypeValue=$(this).parent().siblings(".enterTypeValue").val();
			var isPreferred=$(this).parent().siblings(".isPreferred").val();
			var basiId=$(this).parent().siblings(".basiId").val();
			$("#skillIdTwo").attr("value",vskillId);
			$("#enterIdTwo").attr("value",venterId);
			$("#openIdTwo").attr("value",vopenId);
			$("#enterTypeValueTwo").attr("value",venterTypeValue);
			$("#isPreferred").attr("value",isPreferred);
			$("#basiId").attr("value",basiId);
			//赋价格
			var a=document.getElementById ("asd");
            a.innerHTML = vprice;
          //  alert("**********************"+venterTypeValue);
            if("jzzx"==venterTypeValue){
            	$(".popup_mask").show();
    			$(".consult_popup").show();
            }
            if("jztf"==venterTypeValue){
            	
            	var skillIdTwo=$("#skillIdTwo").val();
    			var enterIdTwo=$("#enterIdTwo").val();
    			var openIdTwo=$("#openIdTwo").val();
    			var enterTypeValueTwo=$("#enterTypeValueTwo").val();
    			var isPreferred	=$("#isPreferred").val();
    			var basiId=$("#basiId").val();
            	window.location.href="${path}/skillOrder/toBuySkill.html?skillId="+vskillId+"&id="+basiId;
            }
            if("jndr"==venterTypeValue ||"gxst"==venterTypeValue||"sqdv"==venterTypeValue||"cdzy"==venterTypeValue||"xnsj"==venterTypeValue){
            	
            	//window.location.href="${path}/placeHighSchoolSkillOrder/toPlaceHighSchoolSkill.html?enterId="+venterId+"&skillId="+vskillId+"&selOpenId="+vopenId+"&basiId="+basiId;
            	window.location.href="${path}/skillOrder/toBuySkill.html?skillId="+vskillId;
            }
            
		})
		$(".popup_mask").click(function(){
			$(".popup_mask").hide();
			$(".consult_popup").hide();
		})
		$(".close").click(function(){
			$(this).parent().hide();
			$(".popup_mask").hide();
		})
		
		function gotoPayMoney(){
			var skillIdTwo=$("#skillIdTwo").val();
			var enterIdTwo=$("#enterIdTwo").val();
			var openIdTwo=$("#openIdTwo").val();
			var enterTypeValueTwo=$("#enterTypeValueTwo").val();
			var isPreferred=$("#isPreferred").val();
			var basiId=$("#basiId").val();
			if("jzzx"==enterTypeValueTwo){
				window.location.href="${path}/placeOrder/toPlaceOrder.html?enterId="+enterIdTwo+"&skillId="+skillIdTwo+"&sellerOpenId="+openIdTwo;
			}
			/* else if("sqdv"==enterTypeValueTwo){
				window.location.href="${path}/skillOrder/toAddSelected.html?enterId="+enterIdTwo+"&skillId="+skillIdTwo+"&openId="+openIdTwo+"&isPreferred="+isPreferred+"&basiId="+basiId;
			} */
			/* else if("jztf"==enterTypeValueTwo){
				window.location.href="${path}/skillOrder/toAddSelected.html?enterId="+enterIdTwo+"&skillId="+skillIdTwo+"&openId="+openIdTwo+"&isPreferred="+isPreferred+"&basiId="+basiId+"&enterTypeValue="+enterTypeValueTwo;
			} */
		}
	
	
		//获取删除的高度
		var height = $(".substance_content").height();
		$(".selected_del").css({
			"height": height,
			"line-height": height + 'px'
		});
		$(".substance_Right").on("touchstart", function(e) {
			e.preventDefault();
			startX = e.originalEvent.changedTouches[0].pageX,
		    startY = e.originalEvent.changedTouches[0].pageY;
		});
		$(".substance_Right").on("touchmove", function(e) {
			moveEndX = e.originalEvent.changedTouches[0].pageX,
				moveEndY = e.originalEvent.changedTouches[0].pageY,
				X = moveEndX - startX,
				Y = moveEndY - startY;
		});
		var X = 0;
		var Y = 0;
		$(".substance_Right").on("touchend", function(e) {
			var windowHeight = $(window).height(),
				$body = $("body");
			$body.css("height", windowHeight);
			e.preventDefault();
			if(X!=0&&(X>3||X<-3)){
				if(Math.abs(X) > Math.abs(Y) && X < 0) {
					$(this).siblings(".selected_del").removeClass("del1").addClass("del");
					X=0;
				}
				if(Math.abs(X) > Math.abs(Y) && X > 0) {
					$(this).siblings(".selected_del").removeClass("del").addClass("del1")
					X=0;
				}
			}else{
				var href = $(this).parents(".selhref").attr("href");
				window.location.href= href;
			}
			
		});
		var $this="";
		var idVal="";
		$(".selected_del").click(function() {
			$this=$(this);
			$(".popup_mask").show();
			$(".cfz_removeContent").show();
			 idVal = $(".delMusic").eq($(this).index(".selected_del")).val();
		})
		//點擊取消
		$(".cfz_choose span:nth-of-type(1)").click(function(){
			$(".popup_mask").hide();
			$(".cfz_removeContent").hide();
			$this.removeClass("del").addClass("del1")
		})
		//點擊確定
		$(".cfz_choose span:nth-of-type(2)").click(function(){
			$(".popup_mask").hide();
			$(".cfz_removeContent").hide();
			$.ajax({
				type : "post",
				url : "<%=basePath%>skillOrder/deleteOrder.html",
				dataType : "json",
				data : {id:idVal},
				success : function(data) {
					
				}
			});
		 $this.parents("li").hide();
		$this.parent().removeAttr("href");
			
		})
		var h = $(".foot-bar-tab").height() - 2;
		$(".selected_content li:last-child").css("margin-bottom", h);
		$(".selected_content li:last-child").find(".selected_bg").css("border-bottom", 0);
		//底部选项卡切换
		/* $(".foot-bar-tab a").click(function() {
			$(this).addClass("foot-active").siblings().removeClass("foot-active");
		}) */
	</script>
 <script type="text/javascript" src="${path}/xkh_version_2.2/js/foot.js" ></script>
</html>
