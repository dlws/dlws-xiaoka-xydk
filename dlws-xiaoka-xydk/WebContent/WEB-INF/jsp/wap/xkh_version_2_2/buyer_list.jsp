<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<!-- <meta http-equiv="refresh" content="5" /> -->
		<title>我的-查看全部订单</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>

	<body>
		<header class="seller_header">
			<div class="seller_green"><span class="green">全部</span></div>
			<div><span>待付款</span></div>
			<div><span>待服务</span></div>
			<div><span>待确认</span></div>
			<div style="margin-right:0px;"><span>待退款</span></div>
		</header>
		<!--全部出来的样式 TODOq-->
		<section>
		<ul class="seller_Box">
		<c:forEach items="${ordList}" var="ord">
			<!-- 增加个性化详情 -->
			<li>
				<div class="seller_bg"></div>
				<%-- 嵌入订单状态页面 --%>
				<c:import url="orderStatus.jsp" >
					<c:param name="orderStatu" value="${ord.order.orderStatus}"></c:param>
					<c:param name="timeDiffer" value="${ord.order.timeDiffer}"></c:param>
					<c:param name="timmer" value="${ord.timmer}"></c:param>
				</c:import>
				<%-- 控制当前样式名称--%>
				<c:choose>
					<%-- 已关闭 或者是待付款 --%>
					<c:when test="${not empty ord.order.pid}">
						 <c:set var="className" value="substance_Right"></c:set>	
					</c:when>
					<c:when test="${ord.order.orderStatus eq 1||ord.order.orderStatus eq 5||ord.order.orderStatus eq 10 }">
						<c:set var="className" value="substance_Right buyer_Right"></c:set>	
					</c:when>
					<c:otherwise>
						<c:set var="className" value="substance_Right"></c:set>	
					</c:otherwise>
				</c:choose>
				<%-- 只有电话的时候className 追加 call_containt --%>
				<c:choose>
					<%-- 标记有电话时候的样式 --%>
					<c:when test="${not empty ord.order.pid}">
						<c:set var="substanceClass" value="substance_content seller_content "></c:set>
					</c:when>
					<c:when test="${ord.order.orderStatus==0||ord.order.orderStatus ==3||
					ord.order.orderStatus==6||ord.order.orderStatus==12||ord.order.orderStatus==9||not empty ord.order.pid}">
						<c:set var="substanceClass" value="substance_content seller_content call_content"></c:set>	
					</c:when>
					<c:otherwise>
						<c:set var="substanceClass" value="substance_content seller_content "></c:set>	
					</c:otherwise>
				</c:choose>
				
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" class="search_pic" style="border-radius:100%;"/></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">${status}</span>
				</div>
				<div class="${substanceClass}" >
					<c:if test="${ord.order.orderStatus==2||ord.order.orderStatus ==4||ord.order.orderStatus==8||
						ord.order.orderStatus==9||ord.order.orderStatus==7|| not empty ord.order.pid}">
						<c:choose>
							<c:when test="${not empty ord.order.pid}">
								<a href="${path}/publish/seachPubDetail.html?pid=${ord.order.pid}">
							</c:when>
							<c:otherwise>
								<a href="${path}/chatMesseage/queryOrderDetail.html?orderId=${ord.order.orderId}&channel=buyer">
							</c:otherwise>
						</c:choose>
					</c:if>
					<div class="substance_left">
							<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="${className}">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					</span></span>
						<c:if test="${ord.order.orderStatus==0||ord.order.orderStatus ==3||ord.order.orderStatus==6||ord.order.orderStatus==12}">
							<div class="seller_picture">
									<img src="${path}/xkh_version_2.2/img/tell.png" class="seller_pic call_tel">
									<a class="call_communicate" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${ord.order.sellerOpenId}">
										<img src="${path}/xkh_version_2.2/img/chart.png" class="seller_pic seller_pic2">
									</a>
									<input type="hidden" name="sellPhone" value="${ord.map.sellPhone}">
							</div>
						</c:if>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<!--待付款中的删除出来的删除  待付款  超时  已关闭 订单状态-->
					<c:if test="${ord.order.orderStatus eq 1||ord.order.orderStatus eq 5||ord.order.orderStatus eq 10}"> 
						<div class="selected_del">删除</div>
						<input type="hidden" name="orderId" value="${ord.order.orderId}">
					</c:if>
					<input type="hidden" value="${ord.order.sellerOpenId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
					<c:if test="${ord.order.orderStatus==2||ord.order.orderStatus ==4||ord.order.orderStatus==8||ord.order.orderStatus==9||ord.order.orderStatus==7||not empty ord.order.pid}">
						</a>
					</c:if>					
				</div>
						<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
							<div class="info-content info-content2">
								<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
								<span>
									<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
								</span>
							</div>
						</c:if>
						<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
							<div class="info-content info-content2">
								<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
								<span>
									<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
								</span>
							</div>
						</c:if>
						<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
							<div class="info-content info-content2">
								<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
								<span>
									<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
								</span>
							</div>
						</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
					
					<c:if test="${empty ord.order.pid}">
					<c:choose>
							<c:when test="${ord.order.orderStatus eq 1}">
									<div class="total_content none_border seller_border">
										<div class="order_content seller_Refuse">
											<a href="javascript:void(0);" onclick="delOrder(this)" >取消订单</a>
											<a href="#" onclick="toPay('${ord.order.orderId}','${ord.order.payMoney}')" class="green Green">立即付款</a>
											<input type="hidden" name="orderId" value="${ord.order.orderId}">
										</div>
									</div>
							</c:when>
							<c:when test="${ord.order.orderStatus eq 2||ord.order.orderStatus eq 11}">
								<div class="total_content none_border seller_border">
									<div class="order_content seller_Refuse">
										<input type="hidden" name="orderId" value="${ord.order.orderId}">
										<a href="javascript:void(0);" onclick="refuse(this)" class="cfz_border" >取消订单</a>
									</div>
								</div>
							</c:when>
							<c:when test="${ord.order.orderStatus eq 3||ord.order.orderStatus eq 0}">
									<div class="total_content none_border seller_border">
										<div class="order_content seller_refuse">
											<a href="javascript:void(0);" id="apply">申请退款</a>
											<a href="javascript:void(0);" onclick="accept(this)" class="green Green">确认收货</a>
											<input type="hidden" value="${ord.order.orderId}" name="orderId">
										</div>
									</div>
							</c:when>
							<c:when test="${ord.order.orderStatus eq 12}">
							   <div class="total_content none_border seller_border">
									<div class="order_content">
										<a href="javascript:void(0);">客服介入</a><!-- 介入百度聊天窗口 -->
										<a href="javascript:void(0);"  class="green Green" onclick="accept(this)">取消退款</a>
										<input type="hidden" value="${ord.order.orderId}" name="orderId">
									</div>
							    </div>  	
							</c:when>
					</c:choose>
					</c:if>
				</div>
			</li>
			</c:forEach>
			</ul>
			<!--待付款页面     TODO-->
			<ul class="seller_Box none">
			<c:forEach items="${ordList}" var="ord">
			<c:if test="${ord.order.orderStatus eq 1}">
			
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<div class="head-content">
						<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
						<span class="head_right">订单已生成<span>${ord.order.timeDiffer}</span>后将自动取消</span>
						<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
					</div>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" class="search_pic" style="border-radius:100%;"/></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待付款</span>
				</div>
				<div class="substance_content seller_content">
					<div class="substance_left">
							<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<c:choose>
						<c:when test="${not empty ord.order.pid}">
							<div class="substance_Right">					
						</c:when>
						<c:otherwise>
							<div class="substance_Right buyer_Right">
						</c:otherwise>
					</c:choose>
					
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					</span></span>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<!--出来的删除-->
					<div class="selected_del">删除</div>
					<input type="hidden" value="${ord.order.sellerOpenId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
				</div>
				
				
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
						<div class="info-content info-content2">
							<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
							<span>
								<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
							</span>
						</div>
					</c:if>
					<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
						<div class="info-content info-content2">
							<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
							<span>
								<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
							</span>
						</div>
					</c:if>
					<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
						<div class="info-content info-content2">
							<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
							<span>
								<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
							</span>
						</div>
					</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
					<c:if test="${empty ord.order.pid}">
					<div class="total_content none_border seller_border">
						<div class="order_content seller_Refuse">
							<a href="javascript:void(0);" onclick="delOrder(this)" >取消订单</a>
							<a href="#" onclick="toPay('${ord.order.orderId}','${ord.order.payMoney}')" class="green Green">立即付款</a>
							<input type="hidden" name="orderId" value="${ord.order.orderId}">
						</div>
					</div>
					</c:if>
				</div>
			</li>
			</c:if>
			</c:forEach>
			</ul>
			<!-- 待服务页面  TODO-->
			<ul class="seller_Box none">
			<c:forEach items="${ordList}" var="ord">
			<c:if test="${ord.order.orderStatus eq 2||ord.order.orderStatus eq 11}">
			
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<c:if test="${ord.order.orderStatus eq 2}">
						<div class="orderDetail_wait">
							<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic"  /></div>
							<span class="head_right">等待卖家接单剩余<span class="timmer">${ord.order.timeDiffer}</span>后将自动取消</span>
							<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
						</div>
					</c:if>
					<c:if test="${ord.order.orderStatus eq 11}">
						<div class="orderDetail_content">
							<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
							<span class="head_right">卖家拒绝了您的订单</span>
							<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
						</div>
					</c:if>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" class="search_pic" style="border-radius:100%;"/></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待服务</span>
				</div>
				<div class="substance_content seller_content">
					<c:choose>
							<c:when test="${not empty ord.order.pid}">
								<a href="${path}/publish/seachPubDetail.html?pid=${ord.order.pid}">
							</c:when>
							<c:otherwise>
								<a href="${path}/chatMesseage/queryOrderDetail.html?orderId=${ord.order.orderId}&channel=buyer">
							</c:otherwise>
						</c:choose>
						<div class="substance_left">
							<img src="${ord.map.imgUrl}" class="search_pic" />
						</div>
						<div class="substance_Right buyer_Right">
							<span class="substance_Music">${ord.map.skillName}</span>
						<span class="substance_Line"><span>
						<c:choose>
							<c:when test="${ord.map.serviceType==1}">线上</c:when>
							<c:otherwise>线下</c:otherwise>
						</c:choose>
						</span></span>
							<span class="orderDetail_info">${ord.map.skillDepict}</span>
							<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
							<span class="sell_Num">[已售${ord.sellNo}]</span>
						</div>
						<input type="hidden" value="${ord.order.sellerOpenId}" name="otherOpenId">
						<input type="hidden" value="${ord.order.orderId}" name="orderId">
					</a>
				</div>
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
					<c:if test="${empty ord.order.pid}">
					<div class="total_content none_border seller_border">
						<div class="order_content buyer_Refuse">
							<input type="hidden" name="orderId" value="${ord.order.orderId}" style="display: none">
							<a href="javascript:void(0);" onclick="refuse(this)" class="cfz_border" >取消订单</a>
						</div>
					</div>
					</c:if>
				</div>
			</li>
			</c:if>
			</c:forEach>
			</ul>
			<!--待确认页面 TODO-->  
			<ul class="seller_Box none">
			<c:forEach items="${ordList}" var="ord">
			<!-- 卖家已接单服务中 -->
			<c:if test="${ord.order.orderStatus eq 0 ||ord.order.orderStatus eq 3}">
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<c:if test="${ord.order.orderStatus eq 0}">
						<div class="orderDetail_content">
							<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
							<span class="head_right">卖家已接单服务中</span>
						</div>
					</c:if>
					<c:if test="${ord.order.orderStatus eq 3}">
						<div class="orderDetail_date">
							<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
							<span class="head_right">卖家已服务，<span class="timmer">${ord.order.timeDiffer}</span>后将自动确认</span>
							<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
						</div>
					</c:if>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" class="search_pic" style="border-radius:100%;"/></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待确认</span>
				</div>
				<c:choose>
					<c:when test="${not empty ord.order.pid}">
						<div class="substance_content seller_content">
					</c:when>
					<c:otherwise>
						<div class="substance_content seller_content call_content">
					</c:otherwise>
				</c:choose>
				
					<div class="substance_left">
						<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="substance_Right">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					</span></span>
					<div class="seller_picture">
						<img src="${path}/xkh_version_2.2/img/tell.png" class="seller_pic call_tel" />
						<a class="call_communicate" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${ord.order.sellerOpenId}" ><img src="${path}/xkh_version_2.2/img/chart.png" class="seller_pic seller_pic2" /></a>
						<input type="hidden" name="sellPhone" value="${ord.map.sellPhone}">
					</div>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<input type="hidden" value="${ord.order.sellerOpenId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
				</div>
				<c:if test="${ord.order.startDate == null && ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null && ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null && ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
				</div>
				<c:if test="${empty ord.order.pid}">
				<div class="total_content none_border seller_border">
						<div class="order_content seller_refuse">
							<a href="javascript:void(0);" id="apply">申请退款</a>
							<a href="javascript:void(0);" onclick="accept(this)" class="green Green">确认收货</a>
							<input type="hidden" value="${ord.order.orderId}" name="orderId">
						</div>
				</div>
				</c:if>
			</li>
			</c:if>
			</c:forEach>
			</ul>
			<!--待退款页面  TODO-->
			<ul class="seller_Box none">
			<c:forEach items="${ordList}" var="ord">
			<c:if test="${ord.order.orderStatus eq 7||ord.order.orderStatus eq 6||ord.order.orderStatus eq 12}">
			
			<li>
				<div class="seller_bg"></div>
				<div class="consult_head">
					<c:if test="${ord.order.orderStatus eq 7}">
						<div class="headerBuy_content">
							<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
							<span class="head_right">您已取消订单，退款将在一个工作日内退回您的账户</span>
						</div>
						<c:set var="substanceClass" value="substance_content seller_content"></c:set>	
					</c:if>
					<c:if test="${ord.order.orderStatus eq 6}">
					<div class="headerComit_content">
						<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
						<span class="head_right">退款申请已提交，<span class="timmer">${ord.order.timeDiffer}</span>后将自动同意退款</span>
						<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
					</div>
					<c:set var="substanceClass" value="substance_content seller_content call_content"></c:set>	
					</c:if>
					<c:if test="${ord.order.orderStatus eq 12}">
						<div class="headerRefuse_content">
							<div class="head_left"><img src="${path}/xkh_version_2.2/img/watch.png" class="search_pic" /></div>
							<span class="head_right">卖家拒绝退款，请重新处理<span class="timmer">${ord.order.timeDiffer}</span>后将自动关闭</span>
							<input type="hidden" value="${ord.timmer}" name="timmer"><!-- js解析当前时间获取年月日时分秒 -->
						</div>
						<c:set var="substanceClass" value="substance_content seller_content call_content"></c:set>	
					</c:if>
					<c:if test="${not empty ord.order.pid}">
						<c:set var="substanceClass" value="substance_content seller_content"></c:set>	
					</c:if>
				</div>
				<div class="publish_title">
					<span class="title_pic"><img src="${ord.map.sellerHeadPortrait}" class="search_pic" style="border-radius:100%;"/></span>
					<h3>${ord.map.sellerNickname}</h3>
					<span class="edit">待退款</span>
				</div>
				<div class="${substanceClass}" >
					<c:if test="${ord.order.orderStatus eq 7||not empty ord.order.pid}">
						<c:choose>
							<c:when test="${not empty ord.order.pid}">
								<a href="${path}/publish/seachPubDetail.html?pid=${ord.order.pid}">
							</c:when>
							<c:otherwise>
								<a href="${path}/chatMesseage/queryOrderDetail.html?orderId=${ord.order.orderId}&channel=buyer">
							</c:otherwise>
						</c:choose>
					</c:if>
					
					<div class="substance_left">
						<img src="${ord.map.imgUrl}" class="search_pic" />
					</div>
					<div class="substance_Right">
						<span class="substance_Music">${ord.map.skillName}</span>
					<span class="substance_Line"><span>
					<c:choose>
						<c:when test="${ord.map.serviceType==1}">线上</c:when>
						<c:otherwise>线下</c:otherwise>
					</c:choose>
					</span></span>
					<c:if test="${ord.order.orderStatus eq 6||ord.order.orderStatus eq 12}">
						<div class="seller_picture">
							<img src="${path}/xkh_version_2.2/img/tell.png" class="seller_pic call_tel">
							<a class="call_communicate" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${ord.order.sellerOpenId}"><img src="${path}/xkh_version_2.2/img/chart.png" class="seller_pic seller_pic2" /></a>
							<input type="hidden" name="sellPhone" value="${ord.map.sellPhone}">
						</div>
					</c:if>
						<span class="orderDetail_info">${ord.map.skillDepict}</span>
						<span class="substance_price">${ord.singPrice}</span><span class="substance_price">元/${ord.map.dic_name}</span>
						<span class="sell_Num">[已售${ord.sellNo}]</span>
					</div>
					<input type="hidden" value="${ord.order.sellerOpenId}" name="otherOpenId">
					<input type="hidden" value="${ord.order.orderId}" name="orderId">
					<c:if test="${ord.order.orderStatus eq 7||not empty ord.order.pid}">
						</a>
					</c:if>
				</div>
				<c:if test="${ord.order.startDate == null&&ord.order.endDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate == null&&ord.order.startDate !=null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<c:if test="${ord.order.endDate != null&&ord.order.startDate != null}">
					<div class="info-content info-content2">
						<span class=" accurate_left">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
						<span>
							<fmt:formatDate value="${ord.order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${ord.order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
						</span>
					</div>
				</c:if>
				<div class="seller_box">
					<div class="seller_left">
						<img src="${path}/xkh_version_2.2/img/money.png" />
						<span class="seller_num">购买数量&nbsp;:</span>
						<span class="seller_number">${ord.map.tranNumber}</span>
					</div>
					<div class="seller_right">
						<img src="${path}/xkh_version_2.2/img/diamond.png" />
						<span class="accurate_left seller_total">合计&nbsp;:&nbsp;</span>
						<span class="seller_total green seller-total">${ord.order.payMoney}元</span>
					</div>
					<c:if test="${empty ord.order.pid}">
					<c:if test="${ord.order.orderStatus eq 12}">
						<div class="total_content none_border seller_border">
							<div class="order_content">
								<a href="javascript:void(0);">客服介入</a><!-- 介入百度聊天窗口 -->
								<a href="javascript:void(0);"  class="green Green" onclick="accept(this)">取消退款</a>
								<input type="hidden" value="${ord.order.orderId}" name="orderId">
							</div>
						</div>
					</c:if>
					</c:if>
				</div>
			</li>
			</c:if>
			</c:forEach>
			</ul>
		</section>
		<!-- 當前沒有訂單時候出現 -->
			<div class="bank_content">
			   <span class="bank_photo">
			     <img src="${path}/xkh_version_2.2/img/bank_order.png" class="search_pic" />
			   </span>
			   <span class="bank_message">您还没有相关订单</span>
			</div>
		<!--出来的电话弹框-->
		<a href="tel:15036693872"  class="telephone_content">点此拨打：15036693872</a>
		<div class="popup_mask"></div>
		
		<!--申请退款-->
		<div class="consult_popup">
			<div class="close"><img src="${path}/xkh_version_2.2/img/close.png" class="search_pic" /></div>
			<div class="popup_content">
				<h3>温馨提示</h3>
				<span class="popup_pic"><img src="${path}/xkh_version_2.2/img/style.png" class="search_pic" /></span>
			    <span class="told">为保障卖家权益，卖家同意退款后平台将代收<span class="sold buyer_sold">${amount }元</span>咨询费用</span>
			   <span class="go buyer_go" onclick="refund(this)">确定</span>
			   <input type="hidden" name="orderId" value="" id="applayId">
			</div>
		</div>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js" ></script>
    <script>
    	var path = "${path}";
   		var url = window.location.href;
	    $(document).ready(function(){
	    	//加载时判定显示隐藏问题
	    	//先对空提示页面进行隐藏
    		$(".bank_content").hide();
    		var liArray = $("section ul").eq(0).children("li");
    		//获取当前元素是否存在子元素
    		if(liArray.length == 0){
    			$(".bank_content").show();
    		}
	    	var tabNum="${tabNum}";
	    	sessionStorage.tabNum = 0;
	    	//如果 加载全部则 去 sessionStrange中进行寻找 加载sessionStange中的内容
	    	if(tabNum==""){//传参为0则默认为全部
	    		if(typeof(sessionStorage.tabNum)=="undefined"){
	    			tabNum = 0;
	    		}else{
	    			tabNum = parseInt(sessionStorage.tabNum); 
	    		}
	    	}
	    	//如果sessionStrange中的内容为空
	    	var object = $(".seller_header div").eq(tabNum);
	    	object.addClass("seller_green").siblings().removeClass("seller_green");
	    	$(".seller_header span").removeClass("green");	
	    	object.children("span").addClass("green");
	    	//$(">span",object).addClass("green");	
	    	$("section ul").eq(tabNum).show().siblings().hide();	
		});
    
    	$(".seller_header div").click(function(){
    		var index=$(this).index();
    		$(this).addClass("seller_green").siblings().removeClass("seller_green");
    		$(".seller_header span").removeClass("green")
    		$(">span",this).addClass("green");
    		$("section ul").eq(index).show().siblings().hide();
    		//先对空提示页面进行隐藏
    		$(".bank_content").hide();
    		var liArray = $("section ul").eq(index).children("li");
    		//获取当前元素是否存在子元素
    		if(liArray.length == 0){
    			$(".bank_content").show();
    		}
    		//将当前信息存入sessionStorage中
    		sessionStorage.tabNum = index;
    	})
	
		//点击拒绝或者接单时候
		$(".seller_refuse a").click(function(){
			var index=$(this).index();
			$(this).addClass("green Green").siblings().removeClass("green Green");
			$(this).siblings().css("border","1px solid #939393");
			if(index==0){
				$(this).parents("li").find(".edit").html("已关闭");
			}else{
				$(this).parents("li").find(".edit").html("待确认");
			}
		})
		
		
		//点击拒绝退款同意退款
		$(".seller_Refuse a").click(function(){
			var index=$(this).index();
			$(this).addClass("green Green").siblings().removeClass("green Green");
			$(this).siblings().css("border","1px solid #939393");
			if(index==0){
			$(this).parents("li").find(".edit").html("待退款")
			}else{
				$(this).parents("li").find(".edit").html("已退款")
			}
		})
		//获取删除的高度
		var height = $(".substance_content").height();
		$(".selected_del").css({
			"height": height,
			"line-height": height + 'px'
		});
		var onoff = true;
		$(".buyer_Right").on("touchstart", function(e) {
			onoff = true;
			e.preventDefault();
			startX = e.originalEvent.changedTouches[0].pageX,
				startY = e.originalEvent.changedTouches[0].pageY;
		});
		var X = 0;
		var Y = 0;
		$(".buyer_Right").on("touchmove", function(e) {
			moveEndX = e.originalEvent.changedTouches[0].pageX,
				moveEndY = e.originalEvent.changedTouches[0].pageY,
				X = moveEndX - startX,
				Y = moveEndY - startY;
			onoff = false;
		});
		$(".buyer_Right").on("touchend", function(e) {
			var windowHeight = $(window).height(),
				$body = $("body");
			$body.css("height", windowHeight);
			e.preventDefault();
				if(Math.abs(X) > Math.abs(Y) && X < 0) {
					$(this).siblings(".selected_del").removeClass("del1").addClass("del")
				}
				if(Math.abs(X) > Math.abs(Y) && X > 0) {
					$(this).siblings(".selected_del").removeClass("del").addClass("del1")
				}
				if(onoff){
					//获取当前同等级下的 样式名为 substance_left的 a 标签 
		        	 var orderId = $(this).siblings("input[name='orderId']").val();
					 window.location.href=path+"/chatMesseage/queryOrderDetail.html?orderId="+orderId+"&channel=buyer"; ;
				};
		});
		/**待付款  删除操作 */
		$(".selected_del").click(function() {
			$(this).parents('li').hide();
			console.log($(this).html());
			//获取同级元素
			var orderId = $(this).siblings("input[name='orderId']").val();
			var json ={"orderId":orderId}; 
			$.ajax({
				  url: "${path}/ordersea/deleteOrderSell.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();刷新当前请求  
				    window.location.href="${path}/ordersea/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			$(this).parent().removeAttr("href");
		})
		/*更新支付 */
		function toPay(orderId,payMoney){
	    	//alert("orderId:"+orderId+"**********payMoney:"+payMoney)
			window.location.href="${path}/ordersea/updateOrderSelltoPay.html?orderId="+orderId+"&payMoney="+payMoney;
		}
		/**取消订单则将订单状态更改为已关闭 */
		function delOrder(obj){
			console.log(obj.parentNode);
			$(obj).parents('li').hide();
			
			//获取同级元素
			var orderId = $(obj).siblings("input[name='orderId']").val();
			var json ={"orderId":orderId}; 
			$.ajax({
				  url: "${path}/ordersea/updateOrderSellQxdd.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    // location.reload();刷新当前请求  
				    window.location.href="${path}/ordersea/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				    
				  },
				  fail: function (err, status) {
				    console.log(err);
				  }
				})
			$(this).parent().removeAttr("href");
			
		}
		/**待 服 务 */
		//1.取消订单
		function  refuse(obj){
			var value = $(obj).siblings("input[name='orderId']").val();
			window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=0&&url="+url;
				    //window.location.href="${path}/ordersea/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
			
		}
		/**待确认 */
		//1.申请退款
		function  refund(obj){
			var value = $(obj).siblings("input[name='orderId']").val();
			window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=4&&url="+url;
			
		}
		//2.确认收货
		function  accept(obj){
			$(obj).parents('li').hide();
			var value = $(obj).siblings("input[name='orderId']").val();
			var json = {"orderId":value};
			$.ajax({
				  url: "${path}/ordersea/updateOrderSellAccept.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data);
				    //location.reload();//刷新当前请求  
				    window.location.href="${path}/ordersea/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
				  },
				  fail: function (err, status) {
				    console.log(err);
				  }
				})
			$(obj).parent().removeAttr("href");
			
		}
		//取消退款  此处调用  确认收货按钮 
		<%--控制刷新页面  ajax --%>
		$(function () {
            
            (function longPolling() {
            
                $.ajax({
                    url: "${path}/ordersea/toSerchAccept.html",
                    data: {"timed": new Date().getTime()},
                    dataType: "text",
                    timeout: 5000,
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#state").append("[state: " + textStatus + ", error: " + errorThrown + " ]<br/>");
                        if (textStatus == "timeout") { // 请求超时
                                longPolling(); // 递归调用
                            } else { 
                                longPolling();
                            }
                        },
                    success: function (data, textStatus) {
                        if (data == "true") {  // 请求成功
                        	//执行刷新方法 
                        	//location.reload();  //刷新当前请求  
                        	window.location.href="${path}/ordersea/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
                            longPolling();
                        }else{
                        	longPolling();
                        }
                    }
                });
            })();
            
        });
		var h=$(document).outerHeight(true)
		//当点击申请退款的时候出现弹出框
		$("#apply").click(function() {
				var index = $(this).index();
				$(this).addClass("green Green").siblings().removeClass("green Green");
				$(this).siblings().css("border", "1px solid #939393");
				if(index == 0) {
					$(this).parents("li").find(".edit").html("待退款");
					//获取弹出框的高度
					var popupHeight=$(".consult_popup").height()/2;
					$(".consult_popup").css("margin-top",-popupHeight);
					$(".popup_mask").css("height",h)
					$(".popup_mask").show();
			        $(".consult_popup").show();
				} else {
					$(this).parents("li").find(".edit").html("已完成")
				}
			// 将当前订单Id值给小熊弹窗
			var value = $(this).siblings("input[name='orderId']").val();
			$("#applayId").val(value);
		})
		
		// 关闭小熊
		$(".close").click(function(){
			$(this).parent().hide();
			$(".popup_mask").hide();
		})
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/commen.js" ></script>
    
</html>
