<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%-- 当前的seller对于订单详情中用户的操作  param 形式获取参数都为String 此方法下册--%>
<c:set var="falseStr" value="false"></c:set>
<c:set var="trueStr" value="true"></c:set>
<c:if test="${dkOpenId eq param.sellerOpenId}">
		<c:choose>
			<c:when test="${param.orderStatus eq 2}">
				<div class="total_content none_border">
					<div class="order_content order_Content">
						<input type="hidden" name="orderId" value="${param.orderId}">
						<a href="#" onclick="refuseSell(this)">拒绝</a>
						<a href="#" class="green Green" onclick="accept(this)">接单</a>
					</div>
				</div>
			</c:when>
			<c:when test="${param.orderStatus eq 0}">
				<div class="sellerContent">
					<c:if test="${param.feedBack eq falseStr}">
						<%--卖家端订单详情反馈入口开始默认消失，加个类名hide.添加移除hide操作是否消失出现seller_reject1这样的类名是为了操作控制消失出现--%>
						<div class="reject_total seller_reject1">
							<a href="${param.path}/feedback/showFeedBack.html?orderId=${param.orderId}" class="green Green">提交反馈</a>
						</div>
					</c:if>
					<c:if test="${param.feedBack eq trueStr}">
						<%--卖家提交订单反馈后的订单详情页--%>
						<div class="reject_total seller_reject2">
							<a href="${param.path}/feedback/showFeedBack.html?orderId=${param.orderId}" class="green Green">查看反馈</a>
						</div>
					</c:if>
				</div>
			</c:when>
			<c:when test="${param.orderStatus eq 6}">
				<div class="sellerContent">
					<%--买家申请退款，卖家端页面出现--%>
					<div class="reject_total seller_reject3">
						<a href="${param.path}/feedback/showFeedBack.html?orderId=${param.orderId}" class="green Green">提交反馈</a>
						<a href="javascript:void(0);" onclick="agree(this)">同意退款</a>
						<a href="javascript:void(0);" onclick="reject(this)">拒绝退款</a>
						<input type="hidden" value="${param.orderId}" name="orderId">
					</div>
				</div>
			</c:when>
			<%--卖家提交反馈，买家驳回--%>
			<c:when test="${param.orderStatus eq 3}">
				<c:choose>
					<c:when test="${param.buyerReject eq 1}">
						<div class="sellerComit">
							<div class="reject_content">
								<p class="reject">买家驳回原因</p>
								<span class="reject_substance">${param.suggestion}</span>
						   </div>
						   <div class="reject_total">
							<a href="${param.path}/feedback/showFeedBack.html?orderId=${param.orderId}" class="green Green">重新修改</a>
							<%--拒绝修改跳转到全局通用的提交成功页面，之前版本有--%>
							<a href="${param.path}/feedback/sellerRefuseShow.html?orderId=${param.orderId}">拒绝修改</a>
						  </div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="sellerContent">
							<%--卖家提交订单反馈后的订单详情页--%>
							<div class="reject_total seller_reject2">
								<a href="${param.path}/feedback/showFeedBack.html?orderId=${param.orderId}" class="green Green">查看反馈</a>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
			</c:when>
		</c:choose>
</c:if>
