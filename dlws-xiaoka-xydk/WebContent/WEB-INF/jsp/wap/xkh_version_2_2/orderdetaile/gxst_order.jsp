<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style1.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/mui.dtpicker.css" />
		<title>高校社团-订单详情</title>
	</head>
	<body>
		<c:import url="../orderStatus.jsp" >
			<c:param name="orderStatu" value="${orderDetail.orderStatus}"></c:param>
			<c:param name="timeDiffer" value="${invalidTime}"></c:param>
			<c:param name="timmer" value="${timmer}"></c:param>
		</c:import>
		<div class="publish_case">
			<div class="publish_content">
                <!--待支付时候-->
				<div class="publish_title">
					<c:forEach items="${orderDetailList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='sellerHeadPortrait'}">
								<span class="title_pic"><img src="${one.typeValue }" class="search_pic" style="border-radius:50%;"/></span>
							</c:when>
						</c:choose>
					</c:forEach>
					<c:forEach items="${orderDetailList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='sellerNickname'}">
								<h3>${one.typeValue }</h3>
							</c:when>
						</c:choose>
					</c:forEach>
					<c:if test="${orderDetail.orderStatus==1 }">
						<span class="edit">待付款</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==3 ||orderDetail.orderStatus==0}">
						<span class="edit">待确认</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==2 }">
						<span class="edit">待服务</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==4}">
						<span class="edit">已完成</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==9 || orderDetail.orderStatus==6 ||orderDetail.orderStatus==7 ||orderDetail.orderStatus==11||orderDetail.orderStatus==12}">
						<span class="edit">待退款</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==8 }">
						<span class="edit">已退款</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==5 }">
						<span class="edit">已超时</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==10 }">
						<span class="edit">已关闭</span>
					</c:if>
				</div>
			</div>
			<div class="substance_content">
				<c:forEach items="${orderDetailList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='imgUrl'}">
								<div class="substance_left"><img src="${one.typeValue }" class="search_pic" /></div>
							</c:when>
						</c:choose>
				</c:forEach>
				<div class="substance_Right">
					<c:forEach items="${orderDetailList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='skillName'}">
								<span class="substance_Music">${one.typeValue }</span>
							</c:when>
						</c:choose>
					</c:forEach>
					<span class="substance_Line">
						<span>
						<c:forEach items="${orderDetailList}" var="one">
							<c:choose>
								<c:when test="${one.typeName=='serviceType'}">
										<c:if test="${one.typeValue==1 }">线上</c:if>
										<c:if test="${one.typeValue==2 }">线下</c:if>
								</c:when>
							</c:choose>
						</c:forEach>
						</span>
					</span>
					<c:forEach items="${orderDetailList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='skillDepict'}">
								<p>${one.typeValue }</p>
							</c:when>
						</c:choose>
					</c:forEach>
					<c:forEach items="${orderDetailList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='skillPrice'}">
								<div class="add_Price"><span class="substance_price">${one.typeValue}</span><span class="substance_price">元/次</span></div>
							</c:when>
						</c:choose>
					</c:forEach>
					<span class="sell_Num">[已售${skillInfo.sellNo}]</span>
				</div>
			</div>
		</div>
		<div class="orderDetail_num">
			<span class="accurate-title orderDetail_buy">购买数量&nbsp;:</span>
			<c:forEach items="${orderDetailList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='tranNumber'}">
								<span class="accurate_inp orderDetail_number">${one.typeValue }</span>
							</c:when>
						</c:choose>
			</c:forEach>
		</div>
	     <!-- 06-20新增退款按钮 -->
	     <c:if test="${orderDetail.orderStatus==3}">
			<div class="refund_box">
				<div class="fefund_content">
				<span onclick="refund(this)">退款</span>
				<input type="hidden" name="orderId" value="${orderDetail.orderId}">
			</div>
			<div class="selected_bg accurate_bg"></div>
			</div>
		</c:if>
			<c:if test="${orderDetail.orderStatus==0 ||orderDetail.orderStatus==3||orderDetail.orderStatus==6||orderDetail.orderStatus==7||orderDetail.orderStatus==12}">
				<p class="buyConsult">咨询权限保留24小时，可直接与卖家沟通</p>
				<div class="chart_content">
				<c:forEach items="${orderDetailList}" var="one">
					<c:choose>
						<c:when test="${one.typeName=='sellPhone'}">
							<span class="accurate_inp orderDetail_number"></span>
							<div class="telphone">
								<a href="tel:${one.typeValue }">
								<span><img src="${path}/xkh_version_2.2/img/tell.png" class="search_pic" /></span>
								<span>打电话</span>
								</a>
							</div>
							<a class="telphone chat" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${orderDetail.sellerOpenId}">
								<span><img src="${path}/xkh_version_2.2/img/chart.png" class="search_pic" /></span>
								<span>聊一聊</span>
							</a>
						</c:when>
					</c:choose>
				</c:forEach>
					
				</div>
			</c:if>

		<div class="order_bg"></div>
		<!--买家信息-->
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/buyer.png" class="search_pic" /></div>
				<span class="info">买家信息</span>
			</div>
			<div class="info-content">
				<span>联&nbsp;&nbsp;系&nbsp;&nbsp;人&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderDetail.linkman }</span>
			</div>
			<div class="info-content info-content2">
				<span>联系电话&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderDetail.phone }</span>
			</div>
			<div class="info-content info-content2">
				<span>需求描述&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<div class="introduce_new">
					<c:forEach items="${orderDetailList}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='aboutNeed'}">
								<div class="about_content p" id="box">${one.typeValue }</div>
							</c:when>
						</c:choose>
					</c:forEach>
					<strong class="watch"></strong>
				</div>
			</div>
			<div class="info-content info-content2 none_border">
				<span>买家留言&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<div class="introduce_new">
					<div class="about_content p" id="box">${orderDetail.message }</div>
					<strong class="watch"></strong>
				</div>
			</div>
		</div>
		
		<!--订单信息-->
		<div class="selected_bg accurate_bg"></div>
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/consult.png" class="search_pic" /></div>
				<span class="info">订单信息</span>
			</div>
			<div class="info-content">
				<span >订单编号&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderDetail.orderId }</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span >下单时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span><fmt:formatDate  value="${orderDetail.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></span>
			</div>
		</div>
		<div class="selected_bg accurate_bg"></div>
		<!--服务时间-->
		<div class="info-content info-content2">
			<span class=" accurate_left">服务时间&nbsp;:&nbsp;</span>
			<span class="green">${orderDetail.startTime}&nbsp;-&nbsp;${orderDetail.endTime}</span>
		</div>
		<div class="accurate_box">
			<div class="info-content info-content2">
				<span class=" accurate_left">合计&nbsp;:&nbsp;</span>
				<span class="green">${orderDetail.payMoney }元</span>
			</div>
			<%-- 如果当前OpenId与OrderSellerId相同时 --%>
			<c:import url="../buyer_operation.jsp">
				<c:param name="openId" value="${orderDetail.openId}"></c:param>
				<c:param name="orderStatus" value="${orderDetail.orderStatus}"></c:param>
				<c:param name="orderId" value="${orderDetail.orderId}"></c:param>
				<c:param name="payMoney" value="${orderDetail.payMoney}"></c:param>
				<c:param name="sellerRefuse" value="${feedBackBean.sellerRefuse}"></c:param>
				<c:param name="reseon" value="${feedBackBean.reseon}"></c:param>
				<c:param name="feedbackstatus" value="${feedBackBean.feedbackstatus}"></c:param>
				<c:param name="sellerRefuse" value="${feedBackBean.sellerRefuse}"></c:param>
				<c:param name="id" value="${feedBackBean.id}"></c:param>
				<c:param name="path" value="${path}"></c:param>
			</c:import>
			
			<%-- 如果当前OpenId与OrderSellerId相同时 --%>
			<c:import url="../seller_operation.jsp">
				<c:param name="sellerOpenId" value="${orderDetail.sellerOpenId}"></c:param>
				<c:param name="orderStatus" value="${orderDetail.orderStatus}"></c:param>
				<c:param name="orderId" value="${orderDetail.orderId}"></c:param>
				<c:param name="feedBack" value="${feedBack}"></c:param>
				<c:param name="buyerReject" value="${feedBackBean.buyerReject}"></c:param>
				<c:param name="suggestion" value="${feedBackBean.suggestion}"></c:param>
				<c:param name="path" value="${path}"></c:param>
			</c:import>
	</div>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
    <script>
    //1.未支付时 取消订单
    var url = window.location.href;
    var path = "${path}";
    function delOrder(obj){
			//console.log(obj.parentNode);
			//$(obj).parents('li').hide();
			//获取同级元素
			var orderId = $(obj).siblings("input[name='orderId']").val();
			var json ={"orderId":orderId}; 
			$.ajax({
				  url: "${path}/ordersea/updateOrderSellQxdd.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data)
				    location.reload();
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			//$(this).parent().removeAttr("href");
			
	}
    //2.支付时取消订单
     
    /**更改支付 调用方式 */
	function toPay(orderId,payMoney){
    	//alert("orderId:"+orderId+"**********payMoney:"+payMoney)
		window.location.href="${path}/ordersea/updateOrderSelltoPay.html?orderId="+orderId+"&payMoney="+payMoney;
	}
    
    
   //1.取消订单
	function  refuse(obj){
		var value = ${orderDetail.orderId};
		window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=0&&url="+url;
	}
	/**待确认 */
	//1.申请退款
	function  refund(obj){
		var value = ${orderDetail.orderId}
		window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=4&&url="+url;
	}
  	//2.确认收货
	function  accept(obj){
		var value = ${orderDetail.orderId};
		var json = {"orderId":value};
		$.ajax({
			  url: "${path}/ordersea/updateOrderSellAccept.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data);
			    location.reload();//刷新当前请求  
			  },
			  fail: function (err, status) {
			    console.log(err);
			  }
			})
		
	}
    	 //控制字数显示的
	$(document).ready(function(){
                 function show(){
					var btn = document.getElementsByClassName("watch");
					var p = document.getElementsByClassName("p");
					
					var arr=[];
					for(var i=0;i<btn.length;i++){
						(function(i) {
							var text = p[i].innerHTML;
							arr.push(text)
							var length=$(".p").eq(i).html().length;
							var html=$(".p").eq(i).html();
							if(length>60){
								$(".p").eq(i).siblings(".watch").addClass("class_up");
								 $(".p").eq(i).html(html.substring(0,60)+"...");
								 btn[i].onclick = function(){
										var newBox = document.createElement("div");
									 $(this).toggleClass("class_up").toggleClass("class_down")
									newBox.innerHTML=arr[i];
									if($(this).hasClass("class_down")) {
										newBox.innerHTML = arr[i];
									} else{
										if($(".p").eq(i).html().length>60){
											newBox.innerHTML = arr[i].substring(0, 60) + "...";
										}else{
											newBox.innerHTML = arr[i];
										}								
									}
								 	  p[i].innerHTML = ""; 
									 p[i].appendChild(newBox); 
									
								} 
							}
						
						
						})(i)
					}
				}
				show();
	})	
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/ordercommon.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/commen.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/orderDetailSeller.js" ></script>
    
