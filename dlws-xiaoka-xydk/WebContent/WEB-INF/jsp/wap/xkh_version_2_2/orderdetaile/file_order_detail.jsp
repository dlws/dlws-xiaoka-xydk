<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>场地资源-订单详情</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.1/css/style.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>

	<body>
			<c:import url="../orderStatus.jsp" >
				<c:param name="orderStatu" value="${order.orderStatus}"></c:param>
				<c:param name="timeDiffer" value="${overTimetoShow}"></c:param>
				<c:param name="timmer" value="${timmer}"></c:param>
			 </c:import>
		<!--状态类-->
		<div class="publish_case">
			<div class="publish_content">
				<div class="publish_title">
					<span class="title_pic"><img src="${orderDetail.sellerHeadPortrait}" class="search_pic" /></span>
					<h3>${orderDetail.sellerNickname}</h3>
					<span class="edit">${status}</span>
				</div>
			</div>
			<div class="substance_content">
				<div class="substance_left"><img src="${orderDetail.imgUrl}" class="search_pic" /></div>
				<div class="substance_Right">
					<span class="substance_Music">${orderDetail.skillName}</span>
					<span class="orderDetail_info">${orderDetail.skillDepict}</span>
					<div class="add_Price">
					  <span class="substance_price">${orderDetail.skillPrice}</span><span class="substance_price">元/${orderDetail.dic_name}</span>
					  
					</div>
					<span class="sell_Num">[已售${map.sellNo}]</span>
				</div>
			</div>
		</div>
		<div class="orderDetail_num">
			<span class="accurate-title orderDetail_buy">购买数量&nbsp;:</span>
			<span class="accurate_inp orderDetail_number">${orderDetail.tranNumber}</span>
		</div>
		<!-- 06-20新增退款按钮 -->
		<c:if test="${order.orderStatus eq 3}">
			<div class="refund_box">
				<div class="fefund_content">
					<span onclick="refund(this)">退款</span>
					<input type="hidden" name="orderId" value="${order.orderId}">
				</div>
				<div class="selected_bg accurate_bg"></div>
			</div>
		</c:if>
		<%-- 当订单状态为 卖家已接单 或卖家以服务  或待退款时 出现此号码 --%>
			<c:if test="${order.orderStatus eq 0 || order.orderStatus eq 3 || order.orderStatus eq 7||order.orderStatus eq 6||order.orderStatus eq 12}">
			<div class="chart_content">
				<div class="telphone" id="callPhone">
					<a href="tel:${order.sellPhone}">
						<span><img src="${path}/xkh_version_2.2/img/tell.png" class="search_pic" /></span>
						<span>打电话</span>
					</a>
				</div>
				<div class="telphone chat">
					<a href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${order.sellerOpenId}">
						<span><img src="${path}/xkh_version_2.2/img/chart.png" class="search_pic" /></span>
						<span>聊一聊</span>
					</a>
				</div>
			</div>
			</c:if>
<!-----------------------------以下页面2.5调换结构-----------------------------------  -->
		<!--买家信息-->
		<div class="information_content">
			<div class="info_content none_border">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/buyer.png" class="search_pic" /></div>
				<span class="info">买家信息</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span class="need">需求描述&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<div class="introduce_new">
					<div class="about_content p" id="box">${orderDetail.tranDemand}</div>
					<strong class="watch"></strong>
				</div>
			</div>
			<div class="info-content info-content2 none_border">
				<span class="need">买家留言&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<div class="introduce_new">
					<div class="about_content p" id="box">${order.message}</div>
					<strong class="watch"></strong>
				</div>
			</div>
			</div>
			<div class="selected_bg accurate_bg"></div>
			<div class="info-content info-content2 none_border">
				<span class=" accurate_left timeMargin">使用时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span class="timeMargin">
					<fmt:formatDate value="${order.startDate}" pattern="yyyy-MM-dd HH:mm"/>-<fmt:formatDate value="${order.endDate}" pattern="yyyy-MM-dd HH:mm"/>
				</span>
			</div>
			<div class="selected_bg accurate_bg"></div>
			<div class="information_content">
			<div class="info-content none_border">
				<span class="timeMargin">联&nbsp;&nbsp;系&nbsp;&nbsp;人&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span class="timeMargin">${order.linkman}</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span>联系电话&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${order.phone}</span>
			</div>
		</div>
		<div class="order_bg"></div>
		<!--订单信息-->
		<div class="information_content">
			<div class="info_content none_border">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/consult.png" class="search_pic" /></div>
				<span class="info">订单信息</span>
			</div>
			<div class="info-content none_border">
				<span>订单编号&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${order.orderId}</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span>下单时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span><fmt:formatDate value="${order.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></span>
			</div>
		</div>
		<div class="selected_bg accurate_bg"></div>
 <!------------------------ 07-19场地资源订单详情加入-------------------------------->
       <div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="/dlws-xiaoka-xydk/xkh_version_2.2/img/pay.png" class="search_pic"></div>
				<span class="info">买家已支付</span>
			</div>
			<div class="info_content">
			<span>支付金额<span class="earnest">(&nbsp;定金&nbsp;)</span>&nbsp;&nbsp;:&nbsp;&nbsp;</span>
			<span class="green">${order.payMoney}元</span>
			</div>
		</div>
		<div class="selected_bg accurate_bg"></div>
		<div class="accurate_box">
			
			<%-- <div class="info-content info-content2">
				<span class=" accurate_left">合计&nbsp;:&nbsp;</span>
				<span class="green">${order.payMoney}元</span>
			</div> --%>
			
			<%-- 如果当前OpenId与OrderSellerId相同时 --%>
			<c:import url="../buyer_operation.jsp">
				<c:param name="openId" value="${order.openId}"></c:param>
				<c:param name="orderStatus" value="${order.orderStatus}"></c:param>
				<c:param name="orderId" value="${order.orderId}"></c:param>
				<c:param name="payMoney" value="${order.payMoney}"></c:param>
				<c:param name="sellerRefuse" value="${feedBackBean.sellerRefuse}"></c:param>
				<c:param name="reseon" value="${feedBackBean.reseon}"></c:param>
				<c:param name="feedbackstatus" value="${feedBackBean.feedbackstatus}"></c:param>
				<c:param name="sellerRefuse" value="${feedBackBean.sellerRefuse}"></c:param>
				<c:param name="id" value="${feedBackBean.id}"></c:param>
				<c:param name="path" value="${path}"></c:param>
			</c:import>
			
			<%-- 如果当前OpenId与OrderSellerId相同时 --%>
			<c:import url="../seller_operation.jsp">
				<c:param name="sellerOpenId" value="${order.sellerOpenId}"></c:param>
				<c:param name="orderStatus" value="${order.orderStatus}"></c:param>
				<c:param name="orderId" value="${order.orderId}"></c:param>
				<c:param name="feedBack" value="${feedBack}"></c:param>
				<c:param name="buyerReject" value="${feedBackBean.buyerReject}"></c:param>
				<c:param name="suggestion" value="${feedBackBean.suggestion}"></c:param>
				<c:param name="path" value="${path}"></c:param>
			</c:import>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.1/js/jquery-2.1.0.js" ></script>
    <script>
    //控制字数显示的
    var path="${path}";
    var url = window.location.href;
	$(document).ready(function(){
                 function show(){
					var btn = document.getElementsByClassName("watch");
					var p = document.getElementsByClassName("p");
					
					var arr=[];
					for(var i=0;i<btn.length;i++){
						(function(i) {
							var text = p[i].innerHTML;
							arr.push(text)
							var length=$(".p").eq(i).html().length;
							var html=$(".p").eq(i).html();
							if(length>60){
								$(".p").eq(i).siblings(".watch").addClass("class_up");
								 $(".p").eq(i).html(html.substring(0,60)+"...");
								 btn[i].onclick = function(){
										var newBox = document.createElement("div");
									 $(this).toggleClass("class_up").toggleClass("class_down")
									newBox.innerHTML=arr[i];
									if($(this).hasClass("class_down")) {
										newBox.innerHTML = arr[i];
									} else{
										if($(".p").eq(i).html().length>60){
											newBox.innerHTML = arr[i].substring(0, 60) + "...";
										}else{
											newBox.innerHTML = arr[i];
										}								
									}
								 	  p[i].innerHTML = ""; 
									 p[i].appendChild(newBox); 
									
								} 
							}
						
						
						})(i)
					}
				}
				show();
		
	})	
	//取消订单   即将订单状态更改为已关闭  或者删除当前的单子 
	function delOrder(obj){
			//console.log(obj.parentNode);
			//$(obj).parents('li').hide();
			
			//获取同级元素
			var orderId = $(obj).siblings("input[name='orderId']").val();
			alert(orderId);
			var json ={"orderId":orderId}; 
			$.ajax({
				  url: "${path}/ordersea/updateOrderSellQxdd.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data)
				    location.reload();
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			//$(this).parent().removeAttr("href");
			
	}
    //取消订单  当前未有人接单时 取消订单
	function  refuse(obj){
		//$(obj).parents('li').hide();
		var value = $(obj).siblings("input[name='orderId']").val();
		window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=0&&url="+url;
	}
    
    /**待确认 */
	//1.申请退款
	function  refund(obj){
		//$(obj).parents('li').hide();
		var value = $(obj).siblings("input[name='orderId']").val();
		window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=4&&url="+url;
		
	}
	//2.确认收货
	function  accept(obj){
		//$(obj).parents('li').hide();
		var value = $(obj).siblings("input[name='orderId']").val();
		var json = {"orderId":value};
		$.ajax({
			  url: "${path}/ordersea/updateOrderSellAccept.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data)
			    location.reload();
			  },
			  fail: function (err, status) {
			    console.log(err)
			  }
			})
		
	}
	function toPay(orderId,payMoney){
		window.location.href="${path}/ordersea/updateOrderSelltoPay.html?orderId="+orderId+"&payMoney="+payMoney;
	}
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/ordercommon.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/commen.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/orderDetailSeller.js"></script>
</html>
