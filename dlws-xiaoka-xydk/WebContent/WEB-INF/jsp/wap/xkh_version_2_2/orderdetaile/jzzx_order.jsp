<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/iconfont/iconfont.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style1.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
		<title>订单详情</title>
	</head>

	<body>
		<!-- 更改当前页面为注入方式公共样式-->
		<c:import url="../orderStatus.jsp" >
					<c:param name="orderStatu" value="${orderDetail.orderStatus}"></c:param>
					<c:param name="timeDiffer" value="${invalidTime}"></c:param>
					<c:param name="timmer" value="${timmer}"></c:param>
		</c:import>
		<div class="publish_case">
			<div class="publish_content">
                <!--待支付时候-->
				<div class="publish_title">
					<c:forEach items="${orderData}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='sellerHeadPortrait'}">
								<span class="title_pic"><img src="${one.typeValue }" class="search_pic" style="border-radius:50%;"/></span>
							</c:when>
						</c:choose>
					</c:forEach>
					<c:forEach items="${orderData}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='sellerNickname'}">
								<h3>${one.typeValue }</h3>
							</c:when>
						</c:choose>
					</c:forEach>
					<c:if test="${orderDetail.orderStatus==2 }">
						<span class="edit">待服务</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==0 }">
						<span class="edit">已接单</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==4 }">
						<span class="edit">已完成</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==11 }">
						<span class="edit">待退款</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==9 }">
						<span class="edit">待退款</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==8 }">
						<span class="edit">已退款</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==5 }">
						<span class="edit">已关闭</span>
					</c:if>
					<c:if test="${orderDetail.orderStatus==10 }">
						<span class="edit">已关闭</span>
					</c:if>
				</div>
			</div>
			<div class="substance_content">
				<c:forEach items="${orderData}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='imgUrl'}">
								<div class="substance_left"><img src="${one.typeValue }" class="search_pic" /></div>
							</c:when>
						</c:choose>
				</c:forEach>
				<div class="substance_Right">
					<c:forEach items="${orderData}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='skillName'}">
								<span class="substance_Music">${one.typeValue }</span>
							</c:when>
						</c:choose>
					</c:forEach>
					<span class="substance_Line">
						<span>
						<c:forEach items="${orderData}" var="one">
							<c:choose>
								<c:when test="${one.typeName=='serviceType'}">
										<c:if test="${one.typeValue==0 }">线上</c:if>
										<c:if test="${one.typeValue==1 }">线下</c:if>
								</c:when>
							</c:choose>
						</c:forEach>
						</span>
					</span>
					<c:forEach items="${orderData}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='skillDepict'}">
								<p>${one.typeValue }</p>
							</c:when>
						</c:choose>
					</c:forEach>
					<div class="add_Price"><span class="substance_price">${orderDetail.payMoney }</span><span class="substance_price">元/次</span></div>
					<span class="sell_Num">[已售${preferredService.salesVolume }]</span>
				</div>
			</div>
		<!-- 	<div class="selected_bg accurate_bg"></div> -->
		</div>
		<!-- 06-20新增退款按钮 -->
		<c:if test="${orderDetail.enterType eq 'sqdv' && orderDetail.orderStatus==3}">
			<div class="refund_box">
				<div class="fefund_content">
				<span onclick="refund(this)">退款</span>
				<input type="hidden" name="orderId" value="${orderDetail.orderId}">
			</div>
			<div class="selected_bg accurate_bg"></div>
			</div>
		</c:if>
			<c:if test="${orderDetail.orderStatus==0}">
				<c:forEach items="${orderData}" var="one">
						<c:choose>
							<c:when test="${one.typeName=='sellPhone'}">
								<p class="buyConsult">咨询权限保留24小时，可直接与卖家沟通</p>
								<div class="chart_content">
									<div class="telphone">
										<a href="tel:${one.typeValue }">
											<span><img src="${path}/xkh_version_2.2/img/tell.png" class="search_pic" /></span>
											<span>打电话</span>
										</a>
									</div>
									<a class="telphone chat" href="${path}/chatMesseage/createChatRelation.html?otherOpenId=${orderDetail.sellerOpenId}">
										<span><img src="${path}/xkh_version_2.2/img/chart.png" class="search_pic" /></span>
										<span>聊一聊</span>
									</a>
								</div>
							</c:when>
						</c:choose>
				</c:forEach>
			</c:if>
		<!--订单信息-->
		<div class="selected_bg accurate_bg"></div>
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/consult.png" class="search_pic" /></div>
				<span class="info">订单信息</span>
			</div>
			<div class="info-content">
				<span >订单编号&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderDetail.orderId }</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span >下单时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span><fmt:formatDate  value="${orderDetail.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></span>
			</div>
		</div>
		<div class="selected_bg accurate_bg"></div>

		<div class="accurate_box">
			<div class="info-content info-content2">
				<span class=" accurate_left">合计&nbsp;:&nbsp;</span>
				<span class="green">${orderDetail.payMoney }元</span>
			</div>
			<!--取消订单-->
			<c:if test="${orderDetail.enterType eq jzzx}">
				<c:if test="${isMySelf}">
					<c:if  test="${orderDetail.orderStatus eq 2}">
						<div class="total_content none_border">
							<div class="order_content">
								<input type="hidden" name="orderId" value="${orderDetail.orderId}">
								<a href="javascript:void(0);" class="cfz_border" onclick="refuse(this)">取消订单</a>
							</div>
						</div>
					</c:if>
				</c:if>
			</c:if>
			<%-- 如果当前OpenId与OrderSellerId相同时 --%>
			<c:if test="${dkOpenId eq orderDetail.sellerOpenId}">
			<c:choose>
				<c:when test="${orderDetail.orderStatus eq 2}">
					<div class="total_content none_border">
						<div class="order_content order_Content">
							<input type="hidden" name="orderId" value="${orderDetail.orderId}">
							<a href="#" onclick="refuseSell(this)">拒绝</a>
							<a href="#" class="green Green" onclick="accept(this)">接单</a>
						</div>
					</div>
				</c:when>
			</c:choose>
			</c:if>
			<!-- 4 11 9 8 四种情况 -->
			<c:if test="${orderDetail.orderStatus==4&&orderDetail.orderStatus==11&&orderDetail.orderStatus==9&&orderDetail.orderStatus==8 }"></c:if>
		</div>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
    <script>
    	 //控制字数显示的
    var url = window.location.href;
    var path = "${path}";
	$(document).ready(function(){
                 function show(){
					var btn = document.getElementsByClassName("watch");
					var p = document.getElementsByClassName("p");
					
					var arr=[];
					for(var i=0;i<btn.length;i++){
						(function(i) {
							var text = p[i].innerHTML;
							arr.push(text)
							var length=$(".p").eq(i).html().length;
							var html=$(".p").eq(i).html();
							if(length>60){
								$(".p").eq(i).siblings(".watch").addClass("class_up");
								 $(".p").eq(i).html(html.substring(0,60)+"...");
								 btn[i].onclick = function(){
										var newBox = document.createElement("div");
									 $(this).toggleClass("class_up").toggleClass("class_down")
									newBox.innerHTML=arr[i];
									if($(this).hasClass("class_down")) {
										newBox.innerHTML = arr[i];
									} else{
										if($(".p").eq(i).html().length>60){
											newBox.innerHTML = arr[i].substring(0, 60) + "...";
										}else{
											newBox.innerHTML = arr[i];
										}								
									}
								 	  p[i].innerHTML = ""; 
									 p[i].appendChild(newBox); 
									
								} 
							}
						
						
						})(i)
					}
				}
			show();
	})	
	
	//取消订单  当前未有人接单时 取消订单
	function  refuse(obj){
		
		var value = $(obj).siblings("input[name='orderId']").val();
		window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=0&&url="+url;
		
	}
	//1.申请退款
	function  refund(obj){
		var value = ${orderDetail.orderId}
		window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=4&&url="+url;
	}
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/ordercommon.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/commen.js" ></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/orderDetailSeller.js" ></script>
