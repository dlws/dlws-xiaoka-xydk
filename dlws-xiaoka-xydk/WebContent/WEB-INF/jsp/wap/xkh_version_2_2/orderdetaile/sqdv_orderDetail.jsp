<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>社群大V-自媒体-订单详情</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style1.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.2/css/style.css" />
	</head>

	<body>
			<c:import url="../orderStatus.jsp" >
					<c:param name="orderStatu" value="${orderMap.orderStatus}"></c:param>
					<c:param name="timeDiffer" value="${orderMap.timeDiffer}"></c:param>
					<c:param name="timmer" value="${timmer}"></c:param>
			</c:import>
		<!--状态类-->
		<div class="publish_case">
			<div class="publish_content">
				<!--待支付时候-->
				<div class="publish_title">
					<span class="title_pic"><img src="${baseInfo.headPortrait}" class="search_pic" /></span>
					<h3>${baseInfo.nickname}</h3>
					<span class="edit">${status}</span>
				</div>
			</div>
			<input type="hidden" name="skillId" value="${skillInfo.skillId}">
			<div class="substance_content">
				<div class="substance_left"><img src="${skillInfo.imageUrl}" class="search_pic" /></div>
				<div class="substance_Right">
					<span class="substance_Music">${skillInfo.skillName}</span>
					<c:if test="${skillInfo.serviceType==1}">
						<span class="substance_Line"><span>线上</span></span>
					</c:if>
					<c:if test="${skillInfo.serviceType==2}">
						<span class="substance_Line"><span>线下</span></span>
					</c:if>
					<span class="orderDetail_info">${skillInfo.skillDepict}</span>
					<div class="add_Price"><span class="substance_price">${skillInfo.skillPrice}</span><span class="substance_price">元/次</span></div>
					<span class="sell_Num">[已售${skillInfo.sellNo}]</span>
					<input type="hidden" name="solnum" id="solnum" value="${skillInfo.tranNumber}">
				</div>

			</div>
		</div>
		<div class="orderDetail_num">
			<span class="accurate-title orderDetail_buy">购买数量&nbsp;:</span>
			<span class="accurate_inp orderDetail_number">${skillInfo.tranNumber}</span>
		</div>
		<c:if test="${orderMap.orderStatus eq 3}">
			<div class="refund_box">
				<div class="fefund_content">
					<span onclick="refund(this)">退款</span>
					<input type="hidden" name="orderId" value="${orderDetail.orderId}">
				</div>
				<div class="selected_bg accurate_bg"></div>
			</div>
		</c:if>
			<c:if test="${orderMap.orderStatus eq 0 || orderMap.orderStatus eq 3 || orderMap.orderStatus eq 6 || orderMap.orderStatus eq 7 || orderMap.orderStatus eq 12|| orderMap.orderStatus eq 9}">
			<!--这一部分只有当买家已付款卖家已接单时候才出现-->
			<div class="chart_content" style="display: none">
				<div class="telphone">
					<span><img src="${path}/xkh_version_2.2/img/tell.png" class="search_pic" /></span>
					<span>打电话</span>
				</div>
				<div class="telphone chat">
					<span><img src="${path}/xkh_version_2.2/img/chart.png" class="search_pic" /></span>
					<span>聊一聊</span>
				</div>
			</div>
			</c:if>
		<!--社群大v出现-->
		<div class="order_bg order-none"></div>
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/accurtate.png" class="search_pic" /></div>
				<span class="info">投放版位</span>
			</div>
			<div class="place_content">
			
				<c:forEach items="${settleList}" var="one">
					<c:if test="${one.typeName=='accurateVal'}">
						<c:if test="${one.typeValue=='0'}">
							<div><span>头条</span></div>
						</c:if>
						<c:if test="${one.typeValue=='1'}">
							<div><span>次条</span></div>
						</c:if>
					</c:if>
				</c:forEach>
			</div>
		</div>
		<!--社群大v出现-->
		<div class="order_bg"></div>
		<!--买家信息-->
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/buyer.png" class="search_pic" /></div>
				<span class="info">买家信息</span>
			</div>
			<div class="info-content">
				<span>联&nbsp;&nbsp;系&nbsp;&nbsp;人&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.linkman}</span>
			</div>
			<div class="info-content info-content2">
				<span>联系电话&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.phone}</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span>参考软文链接&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<div class="introduce_new">
					<div class="about_content p" id="box">${orderMap.message}</div>
					<strong class="watch"></strong>
				</div>
			</div>
		</div>
		<div class="order_bg"></div>
		<!--订单信息-->
		<div class="information_content">
			<div class="info_content">
				<div class="info_pic"><img src="${path}/xkh_version_2.2/img/consult.png" class="search_pic" /></div>
				<span class="info">订单信息</span>
			</div>
			<div class="info-content">
				<span>订单编号&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span>${orderMap.orderId}</span>
			</div>
			<div class="info-content info-content2 none_border">
				<span>下单时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span><fmt:formatDate  value="${orderMap.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></span>
			</div>
		</div>
		<div class="selected_bg accurate_bg"></div>

		<div class="accurate_box">
			
			<!--社群大v出现-->
			<div class="info-content info-content2">
				<span class=" accurate_left">投放时间&nbsp;&nbsp;:&nbsp;&nbsp;</span>
				<span class="green"><fmt:formatDate  value="${orderMap.endDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></span>
			</div>
			<div class="info-content info-content2">
				<span class=" accurate_left">合计&nbsp;:&nbsp;</span>
				<span class="green">${orderMap.payMoney}元</span>
			</div>
			
			<%-- 如果当前OpenId与OrderSellerId相同时 --%>
			<c:import url="../buyer_operation.jsp">
				<c:param name="openId" value="${orderMap.openId}"></c:param>
				<c:param name="orderStatus" value="${orderMap.orderStatus}"></c:param>
				<c:param name="orderId" value="${orderMap.orderId}"></c:param>
				<c:param name="payMoney" value="${orderMap.payMoney}"></c:param>
				<c:param name="sellerRefuse" value="${feedBackBean.sellerRefuse}"></c:param>
				<c:param name="reseon" value="${feedBackBean.reseon}"></c:param>
				<c:param name="feedbackstatus" value="${feedBackBean.feedbackstatus}"></c:param>
				<c:param name="sellerRefuse" value="${feedBackBean.sellerRefuse}"></c:param>
				<c:param name="id" value="${feedBackBean.id}"></c:param>
				<c:param name="path" value="${path}"></c:param>
			</c:import>
			
			<%-- 如果当前OpenId与OrderSellerId相同时 --%>
			<c:import url="../seller_operation.jsp">
				<c:param name="sellerOpenId" value="${orderMap.sellerOpenId}"></c:param>
				<c:param name="orderStatus" value="${orderMap.orderStatus}"></c:param>
				<c:param name="orderId" value="${orderMap.orderId}"></c:param>
				<c:param name="feedBack" value="${feedBack}"></c:param>
				<c:param name="buyerReject" value="${feedBackBean.buyerReject}"></c:param>
				<c:param name="suggestion" value="${feedBackBean.suggestion}"></c:param>
				<c:param name="path" value="${path}"></c:param>
			</c:import>
		</div>
	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/jquery-2.1.0.js" ></script>
    <script>
    var path = "${path}";
    var url = window.location.href;
    //1.未支付时 取消订单
    function delOrder(obj){
			
			//获取同级元素
			var orderId = $(obj).siblings("input[name='orderId']").val();
			alert(orderId);
			var json ={"orderId":orderId}; 
			$.ajax({
				  url: "${path}/ordersea/updateOrderSellQxdd.html",
				  type: 'post',
				  dataType: 'json',
				  data:json,
				  timeout: 10000,
				  success: function (data, status) {
				    console.log(data)
				    location.reload();
				  },
				  fail: function (err, status) {
				    console.log(err)
				  }
				})
			//$(this).parent().removeAttr("href");
			
	}
    /** 待付款   支付当前的订单 */
	function toPay(orderId,payMoney){
// 		alert("orderId:"+orderId+"**********payMoney:"+payMoney)
		window.location.href="${path}/ordersea/updateOrderSelltoPay.html?orderId="+orderId+"&payMoney="+payMoney;
	}
    
   //1.取消订单
	function  refuse(obj){
		var value = ${orderMap.orderId};
		window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=0&&url="+url;
	}
	/**待确认 */
	//1.申请退款
	function  refund(obj){
		var value = ${orderMap.orderId}
		window.location.href="${path}/ordersea/toRefund.html?orderId="+value+"&&checkStatus=4&&url="+url;
	}
  	//2.确认收货
	function  accept(obj){
		var value = $("#orderId").val();
		var json = {"orderId":value};
		$.ajax({
			  url: "${path}/ordersea/updateOrderSellAccept.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data);
			    location.reload();//刷新当前请求  
			  },
			  fail: function (err, status) {
			    console.log(err);
			  }
			})
		
	}
    
    //控制字数显示的
	$(document).ready(function(){
                 function show(){
					var btn = document.getElementsByClassName("watch");
					var p = document.getElementsByClassName("p");
					
					var arr=[];
					for(var i=0;i<btn.length;i++){
						(function(i) {
							var text = p[i].innerHTML;
							arr.push(text)
							var length=$(".p").eq(i).html().length;
							var html=$(".p").eq(i).html();
							if(length>60){
								$(".p").eq(i).siblings(".watch").addClass("class_up");
								 $(".p").eq(i).html(html.substring(0,60)+"...");
								 btn[i].onclick = function(){
										var newBox = document.createElement("div");
									 $(this).toggleClass("class_up").toggleClass("class_down")
									newBox.innerHTML=arr[i];
									if($(this).hasClass("class_down")) {
										newBox.innerHTML = arr[i];
									} else{
										if($(".p").eq(i).html().length>60){
											newBox.innerHTML = arr[i].substring(0, 60) + "...";
										}else{
											newBox.innerHTML = arr[i];
										}								
									}
								 	  p[i].innerHTML = ""; 
									 p[i].appendChild(newBox); 
									
								} 
							}
						
						
						})(i)
					}
				}
				show();
		
	})	
    </script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/ordercommon.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/commen.js"></script>
    <script type="text/javascript" src="${path}/xkh_version_2.2/js/orderDetailSeller.js"></script>
</html>
