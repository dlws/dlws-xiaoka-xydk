<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>身份验证</title>
		
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/diplomat.css" />
	</head>
	<body style="background: #f5f5f5;">
		<div class="adminiSure_content">
			<span class="adminiSure_title">请完整输入管理员的手机号码</span>
			<div class="admniSure_tel">
				<span id="phone">${phone}</span>
				<div class="admminiSure_inp" onload="document.forms[0].elements[0].focus();">
					<form>
						<input maxlength="1" size="1" onkeyup="moveNext(this,0);" type="number" id="one"   onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" />
						<input maxlength="1" size="1" onkeyup="moveNext(this,1);" type="number" id="two"   onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" />
						<input maxlength="1" size="1" onkeyup="moveNext(this,2);" type="number" id="three" onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" />
						<input maxlength="1" size="1" onkeyup="moveNext(this,3);" type="number" id="four"  onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" />
						<input maxlength="1" size="1" onkeyup="moveNext(this,4);" type="number" id="five"  onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" />
						<input maxlength="1" size="1" onkeyup="moveNext(this,5);" type="number" id="six"   onkeypress="return (/[\d]/.test(String.fromCharCode(event.keyCode)))" />
					</form>
				</div>
				<span id="number">${number}</span>
			</div>
			<a href="javascript:void(0);" class="administratorEnsure">确定</a>
		</div>
		<!--弹框提示错误-->
		<div class="mask_common"></div>
		<div class="inform">输入错误</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js"></script>
	<script>
	var h=$(document).height();
	$(".mask").css("height",h)
	$(".administratorEnsure").click(function(){
		var one = $("#one").val();
		var two = $("#two").val();
		var three = $("#three").val();
		var four = $("#four").val();
		var five = $("#five").val();
		var six = $("#six").val();
		if(one == "" || two == "" || three == "" ||
				four == "" || five == "" || six == "" ){
			alert("请输入完整的手机号!");
		}else{
			var phoneNumber = $("#phone").text()+one+two+three+four+five+six+$("#number").text();
			$.ajax({
				url : 'makeSure.html',
				data : {
					"phoneNumber" : phoneNumber
				},
				type : 'post',
				cache : false,
				dataType : 'json',
				success : function(data) {
					var flag = data.flag;
					//alert(data.userId);
					if(flag){
						
						window.location.href = "${path}/diplomatController/changeInfo.html?userId="+data.userId+"&phoneNumber="+phoneNumber;
					}else{
						alert("手机号不匹配!");
					}
				},
				error : function() {
					alert("异常！");
				}
			});
		}
	});
		
		
		var h = $(document).height();
		var nowIndex = 0;
		function moveNext(object, index) {
			nowIndex = index;
			if(object.value.length == 1 && index < 5) {
				document.forms[0].elements[index + 1].focus();
			}
		}
		document.onkeyup = function(e) {
			if($(e.target).val()=="") {
                if(nowIndex == 0){
                    nowIndex = 1;
				}
                document.forms[0].elements[nowIndex - 1].focus();
				
			}else {
			    return false;
			}
		}
		
		
//		//如果输入的手机号和管理员手机号不一致则
//	$(".administratorEnsure").click(function(){
//		if(判断条件){
//			$(".mask_common").css("height",h).show();
//          $(".inform").show();
//		}else{
//			window.location.href="要跳转的页面"
//		}
//	})
$(".admminiSure_inp input:last-child").bind('input propertychange', function() {  
	 $(this).val($(this).val().substring(0,1))
})
   
	</script>

</html>