<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>管理员更换</title>
	</head>
	
	<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.4/css/mui.picker.min.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.4/css/mui.poppicker.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.4/css/diplomat.css" />
	<body>
		<ul class="verification_content">
			<li>
				<span class="verification_name">姓&nbsp;&nbsp;&nbsp;&nbsp;名</span>
				<input maxlength="10" type="text" id="userName" class="verification_inp" placeholder="请填写新管理员的的姓名" />
			</li>
			<li>
				<span class="verification_name">城&nbsp;&nbsp;&nbsp;&nbsp;市</span>
				<div id="activeCity" class="mui-input-row">
				<span class="verification_span result-tips">请选择新管理员所在的城市</span>
				<span class="verification_inp show-result">${basicInfo.cityName}</span>
				<input type="hidden" name="cityId" id="cityId" value="${basicInfo.cityId}" >
				<span class="zhishi"><img src="${path}/xkh_version_2.4/img/right.png" class="search_pic" /></span>
				</div>
			</li>
			<li>
				<span class="verification_name">学&nbsp;&nbsp;&nbsp;&nbsp;校</span>
				<div id="activeSchool" class="mui-input-row">
				<span class="verification_span result-tips">请选择新管理员所在院校</span> 
				<span class="verification_inp show-result">${basicInfo.schoolName}</span>
				<input type="hidden" name="schoolId" id="schooId" value="${basicInfo.schoolId}">
				<span class="zhishi"><img src="${path}/xkh_version_2.4/img/right.png" class="search_pic" /></span>
				</div>
			</li>
			
			<li>
				<span class="verification_name">手机号码</span>
				<input type="number" id="phoneNumber" class="verification_inp phone" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" placeholder="请填写新管理员手机号码" />
				<p class="warn_tel"></p>
			</li>
			<li>
				<span class="verification_name">联系邮箱</span>
				<input type="text" id="email" class="verification_inp letter"  placeholder="请填写新管理员联系邮箱" />
				<p class="warn_letter"></p>
			</li>
			<li>
				<span class="verification_name">微&nbsp;&nbsp;信&nbsp;号</span>
				<input type="text" id="wxNumber" class="verification_inp wechat" placeholder="请填写新管理员微信账号" />
				<p class="warn_wechat"></p>
			</li>
		</ul>
		<input type="hidden" name="userId" value="${userId }" id="userId">
		<input type="hidden" name="phoneOld" id="phoneOld" value="${paramsMap.phoneNumber }">
		<footer class="verification_foot">提交</footer>
		<!--用户提交了入驻的信息-->
		<div class="mask_common"></div>
		<div class="inform change_inform">该用户已入驻，转权失败</div>
		<!--未关注-->
		<div class="inform change_Inform">该用户未关注校咖汇公众号，转权失败</div>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/mui.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/mui.picker.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/mui.poppicker.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/city.data.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/shcool.data.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/diplomat/administrator_change.js" ></script>
	<script>
	//选择所在地
	mui.init();
	var schoolData="";
	var cityPicker = new mui.PopPicker({
				layer: 2
	});
	var shcoolPicker = new mui.PopPicker({
		layer:2
	});
	var city = ${proList}
	cityPicker.setData(city);
	var showCityPickerButton = document.getElementById('activeCity');
	
	showCityPickerButton.addEventListener('tap', function(event) {
		cityPicker.show(function(items) {
			document.querySelector('#activeCity .result-tips').style.display = "none";
			document.querySelector('#activeCity .show-result').style.display = "block";
			document.querySelector('#activeCity .show-result').innerText = items[1].text;
			$("#cityId").val(items[1].value);
			
			document.querySelector('#activeSchool .result-tips').style.display = "block";
			document.querySelector('#activeSchool .show-result').style.display = "none";  
			//返回 false 可以阻止选择框的关闭
			//return false;
			$.ajax({
        		type : "post",
        		url : "<%=contextPath%>/skillUser/chooseSchool.html",
        		dataType : "json",
        		data : {cityId:items[1].value},
        		success : function(data) {
        			
        			shcoolPicker.setData(data);
        			var showShcoolPickerButton = document.getElementById('activeSchool');
        			showShcoolPickerButton.addEventListener('tap', function(event){
        			shcoolPicker.show(function(items) {
        				document.querySelector('#activeSchool .result-tips').style.display = "none";
        				document.querySelector('#activeSchool .show-result').style.display = "block";
        				document.querySelector('#activeSchool .show-result').innerText = items[1].text;
        				$("#schooId").val(items[1].value);
        				//返回 false 可以阻止选择框的关闭
        				//return false;
        			});
        		}, false);
        		var muiPickerLh = $(".mui-poppicker").length;
        		if(muiPickerLh>2){
        		$(".mui-poppicker").eq(muiPickerLh-2).remove();
        		}
        		}
        	});
		});
	}, false);
	
	
	$(".verification_foot").click(function(){
		var userName = $("#userName").val();
		var phoneNumber = $("#phoneNumber").val();
		var email = $("#email").val();
		var wxNumber = $("#wxNumber").val();
		var userId = $("#userId").val();
		var cityId = $("#cityId").val();
		var schooId = $("#schooId").val();
		var phoneOld = $("#phoneOld").val();
		if(userName == "" || phoneNumber == "" || email == "" || wxNumber == ""||cityId==""||schooId==""){
			alert("请输入完整信息!");
			return
		}
		$.ajax({
			url : 'infoSubmit.html',
			data : {
				"phoneNumber" : phoneNumber,
				"userName" : userName,
				"email" : email,
				"wxNumber" : wxNumber,
				"userId" : userId,
				"cityId" : cityId,
				"schooId" : schooId,
				"phoneOld" : phoneOld,
			},
			type : 'post',
			cache : false,
			dataType : 'json',
			success : function(data) {
				var flag = data.msg;
				if(flag==true){
					window.location.href = "${path}/change/prompt.html";
				}else if("reapet"==flag){
					alert("不能与原管理员信息相同!");
				}
				else{
					alert("请核准信息!");
				}
			},
			error : function() {
				alert("异常！");
			}
		});
	});
	var h=$(document).height();
	$(".mask").css("height",h);
		//验证邮箱
		//当文本框失去焦点的时候
		 $(".letter").blur(function(){
		 	var val1=$(".letter").val()
    		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
    		if(!reg.test(val1)){
    			$(".warn_letter").html("输入邮箱格式有误，请重新输入").css("display","block")
    			
    		}
    		
    		if(val1==""){
    			$(".warn_letter").html("请输入邮箱号").css("display","block");
    		}
    		
		 })
        //当文本框重新获得焦点的时候
        $(".letter").focus(function(){
        	var val1=$(".letter").val()
    		var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
    		if(!reg.test(val1)){
    			$(".warn_letter").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_letter").html("").css("display","none");
    		}
        })
	</script>
</html>
