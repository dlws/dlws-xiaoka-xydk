<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>管理中心</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/diplomat.css" />
	</head>
	<body>
		<header class="lookFeedback_head administrationCenter_head">
		<img src="${userInfo.headImgUrl}" class="wx_pic" />
		  <div class="head_mask"></div>
		  <span class="administrationCenter_pic"><img src="${userInfo.headImgUrl}" class="search_pic" /></span>
		  <p class="administrationCenter_name">${userInfo.wxname }</p>
		</header>
		<section>
			<ul class="administrationCenter_content">
				<li>
					<span class="administrationCenter_title">我的资源库</span>
					<a  href="${path}/diplomatController/queryDiplomatSource.html"  class="administrationCenter_look">
						<span>查看全部资源</span>
						<span><img src="${path}/xkh_version_2.4/img/right.png" class="search_pic" /></span>
					</a>
				</li>
				<c:if test="${not empty sourceList[0].id}">
					<c:forEach var="list" items="${sourceList}">
						<li>
							<div class="administrationCenter_left">
								<span class="administrationCenter_photo">
									<img src="${list.headPortrait}" class="search_pic" />
								</span>
								<div class="substance_right">
									<span>${list.userName }</span>
									<span>${list.schoolName }</span>
								</div>
							</div>
							<span class="administrationCenter_right">
								<fmt:formatDate value="${list.createDate}" pattern="yyyy/MM/dd"/>入驻
							</span>
						</li>
					</c:forEach>
				</c:if>
			</ul>
			<div class="administrationCenter_bg"></div>
			<div class="administrationCenter_function">
				<p class="function_name">功能管理</p>
				<div class="function_content">
					<a class="function" href="${path}/diplomatController/toMyWallet.html">
						<span><img src="${path}/xkh_version_2.4/img/wallet.png" class="search_pic" /></span>
						<span>我的钱包</span>
					</a>
					<a class="function" href="${path}/diplomatController/changeManager.html">
						<span><img src="${path}/xkh_version_2.4/img/administaor.png" class="search_pic" /></span>
						<span>管理员更换</span>
					</a>
				</div>
				
			</div>
		</section>
		<a href="${path}/diplomatController/getPromotionalLinks.html" class="administrationCenter_foot">获取推广链接</a>
<%-- 		<footer class="${path}/diplomatController/getPromotionalLinks.html">获取推广链接</footer>--%>	</body>
</html>
