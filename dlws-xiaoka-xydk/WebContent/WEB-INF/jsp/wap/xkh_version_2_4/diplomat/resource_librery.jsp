<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>我的资源库</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/diplomat.css" />
	</head>
	<body>
         <header class="resource_head">
         	<span class="resourceLibrery">共&nbsp;:&nbsp;<span class="resource_number">${count}</span>个资源</span>
         </header>
         <ul class="administrationCenter_content">
         	<c:if test="${not empty list[0].id}">
         		<c:forEach items="${list}" var="mess">
	         		<li>
						<a href="${path}/personInfoDetail/detail.html?id=${id}">
							<div class="administrationCenter_left">
								<span class="administrationCenter_photo"><img src="${mess.headPortrait}" class="search_pic"></span>
								<div class="substance_right">
									<span>${mess.nickname }</span>
									<span>${mess.schoolName}</span>
								</div>
							</div>
							<span class="administrationCenter_right"><fmt:formatDate value="${mess.createDate}" pattern="yyyy/MM/dd"/>入驻</span>
						</a>
					</li>
         		</c:forEach>
         	</c:if>
			</ul>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js" ></script>
	<script>
		var length=$(".administrationCenter_content li").length;
		$(".resource_number").html(length)
	</script>
</html>
