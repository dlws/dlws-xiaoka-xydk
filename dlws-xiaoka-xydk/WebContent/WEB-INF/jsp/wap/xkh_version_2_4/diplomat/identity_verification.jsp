<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>身份验证</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/mui.picker.min.css" />
	    <link rel="stylesheet" href="${path}/xkh_version_2.4/css/mui.poppicker.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
	</head>
	<body>
		<!-- schoolId,schoolName,openId,cityName,phone,wxNumber -->
		<form action="${path}/diplomatController/addDiplomatInfo.html" method="post" id="formId">
			<ul class="verification_content">
				<li>
					<span class="verification_name">姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名</span>
					<input maxlength="10" type="text" name="userName" class="verification_inp" required="required" placeholder="请填写您的姓名" />
				</li>
				<li>
					<span class="verification_name">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span>
					<div id="activeCity" class="mui-input-row">
					<span class="verification_span result-tips">请选择您所在的城市</span>
					<span class="verification_inp show-result"></span>
					<span class="zhishi"><img src="${path}/xkh_version_2.4/img/right.png" class="search_pic" /></span>
					</div>
				</li>
				<li>
					<span class="verification_name">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;校</span>
					<div id="activeSchool" class="mui-input-row">
					<span class="verification_span result-tips">请选择您所在院校</span> 
					<span class="verification_inp show-result"></span>
					<span class="zhishi"><img src="${path}/xkh_version_2.4/img/right.png" class="search_pic" /></span>
					</div>
				</li>
				<li>
					<span class="verification_name">手机号码</span>
					<input type="number" name="phone" class="verification_inp phone" required="required" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" placeholder="请填写您的手机号码" />
					<p class="warn_tel"></p>
				</li>
				<li>
					<span class="verification_name">微&nbsp;&nbsp;信&nbsp;&nbsp;号</span>
					<input type="text" class="verification_inp wechat" name="wxNumber" placeholder="请填写您的微信账号"  required="required"/>
					<p class="warn_wechat"></p>
				</li>
			</ul>
			<input type="hidden" name="schoolId" value="" id="schoolId">
			<input type="hidden" name="schoolName" value="" id="schoolName">  
			<input type="hidden" name="cityName" value="" id="cityName"> 
			<input type="hidden" name="cityId" value="" id="cityId"> 
			<input type="submit" class="verification_foot"  value="提交" style="border:0px;">
		</form>
		
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/mui.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/mui.picker.min.js" ></script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/mui.poppicker.js" ></script>
	 <script type="text/javascript">
	 		var cityData = ${cityDateJson};
	 		var shcollData;
	 		//ajax 请求当前学校的数据
	 		//选择学校
			var shcoolPicker = new mui.PopPicker({
				layer: 2
			});
	 		
	 		function searchProvince(cityId){
	 			
		 		$.ajax({  
			         type:'post',      
			         url: '${path}/diplomatController/getAllSchoolByCityName.html',  
			         data: {'cityId':cityId},  
			         cache:false,  
			         dataType:'json',  
			         success:function(data){
			        	 //重置当前参数给 schoolDate
			        	 shcollData = JSON.parse(data);
			        	 shcoolPicker.setData(shcollData);
	         		 }  
	     		}); 
	 		}
	 </script>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/diplomat/administrator_change.js" ></script>
	
</html>