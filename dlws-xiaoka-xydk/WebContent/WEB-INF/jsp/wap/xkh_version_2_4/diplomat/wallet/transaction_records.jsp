<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>交易记录</title>
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
		<link rel="stylesheet" href="${path}/xkh_version_2.4/css/diplomat.css" />
	</head>

	<body style="background: #f5f5f5;">
		<ul class="deposit_content records_content">
			
			<li>
				<span class="records_name">交易金额</span>
				<span class="records_title size_green">${map.divideOrWithdraw }</span>
			</li>
			<li>
				<span class="records_name">交易类型</span>
				<c:choose>
					<c:when test="${map.state eq '0'}"> 
						<span class="records_title">分成</span>
					</c:when>
					<c:otherwise>
						<span class="records_title">提现</span>
					</c:otherwise>
				</c:choose>
			</li>
			<li>
				<span class="records_name">交易时间</span>
				<span class="records_title">${map.divideOrWithdrawTime}</span>
			</li>
			<li>
				<span class="records_name">交易单号</span>
				<span class="records_title">${map.id}</span>
			</li>
			<!--无交易记录详情时候如下部分不出现加个类名hide-->
			<c:if test="${map.state eq '0'}">
			<li>
				<span class="records_name">订单来源</span>
				<div class="transaction_content">
					<div class="transaction_head">
						<div class="head_left">
							<span><img src="${orderDeatil.sellerHeadPortrait }" class="search_pic" /></span>
							<span>${userInfo.nickname}</span>
						</div>
						<span class="head_right">${userInfo.schoolName}</span>
				    </div>
						<div class="transaction_box">
							<div class="transaction_left"><img src="${path}/xkh_version_2.4/img/pro_img.jpg" class="search_pic"></div>
							<div class="transaction_right">
								<span class="transaction_music">${orderDeatil.skillName }</span>
								<span class="transaction_style">
									<span>
										<c:choose>
											<c:when test="${orderDeatil.serviceType eq 1}">线上</c:when>
											<c:otherwise>线下</c:otherwise>
										</c:choose>
									</span>
								</span>
								<span class="transaction_info">${orderDeatil.skillDepict}</span>
								<span class="transaction_price">${orderDeatil.skillPrice }元/次</span>
								<span class="transaction_number">购买数量&nbsp;:&nbsp;${orderDeatil.tranNumber}</span>
							</div>

						</div>
					</div>
			</li>
			</c:if>
		</ul>

	</body>
    <script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js" ></script>
    <script>
    	//判断如果交易记录没有时候出来的样式
    	if($(".records_content li:last-child").hasClass("hide")){
    		$(".records_content li:nth-last-of-type(2)").css("marginBottom","35px")
    	}else{
    		$(".records_content li:nth-last-of-type(2)").css("marginBottom","0")
    	}
    </script>
</html>