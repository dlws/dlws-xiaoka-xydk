<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta content="width=device-width, minimum-scale=1,initial-scale=1, maximum-scale=1, user-scalable=1" id="viewport" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>校咖汇</title>
	</head>
	<link rel="stylesheet" href="${path}/xkh_version_2.4/css/common.css" />
	<link rel="stylesheet" href="${path}/xkh_version_2.4/css/diplomat.css" />

	<body style="background: #f5f5f5;">
		<span class="referral_title"><span>您的推广链接为&nbsp;:&nbsp;</span></span>
		<span class="referral_content" data-clipboard-action="copy" data-clipboard-target="#bar"><textarea id="bar"><%= basePath%>diplomatController/promotional.html?openId=${openId}</textarea></span>
		<span class="referral_message">*点击复制专属推广链接，通过专属推广链接入驻的用户，将进入您的资源库，永久获得收入分成</span>
		<!--弹窗提示-->
		<div class="mask_common"></div>
		<span class="success_link">成功复制链接</span>
	</body>
	<script type="text/javascript" src="${path}/xkh_version_2.4/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="https://clipboardjs.com/dist/clipboard.min.js"></script>
	<script>
	
	    var clipboard = new Clipboard('.referral_content');

	    clipboard.on('success', function(e) {
	    	
	    	var h=$(document).height();
	        //调用
	        $(".mask_common").css("height",h).show();
			$(".success_link").show();
			setTimeout(function(){
				$(".mask_common").hide();
				$(".success_link").hide();
			},2000)
	    });

	    clipboard.on('error', function(e) {
	        console.log(e);
	    });
	    </script>
		

</html>