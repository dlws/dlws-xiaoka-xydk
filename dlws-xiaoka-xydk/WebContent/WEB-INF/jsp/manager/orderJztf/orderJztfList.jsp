<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
				<div class="column-name">
					<h2>精准投放管理 ><b>精准投放列表</b></h2>
				</div>
				
				<form id = "form1" name="form1" action="${path}/orderJztf/orderJztfList.html" method="post">
				<div class="main-top">
					<div class="fr" align="center">
						<span class="c-name">订单编号</span>
						<input type="text" name="orderId" class="input mr10" value="${orderId}" placeholder="请输入订单编号订单"> 
						<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
					</div>
				</div>
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<!-- <th>轮播图名称</th> -->
								<th>订单编号</th>
								<th>卖家名称</th>
								<th>联系人</th>
								<th>联系电话</th>
								<th>投放标题</th>
								<th>创建时间</th>
								<th>投放时间</th>
								<th>审核状态</th>
								<th>审核</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="one">
							<!-- 遍历数据 -->
							<tr>
								<td>${one.orderId}</td>
								<td>${one.userName}</td>
								<td>${one.linkman}</td>
								<td>${one.phone}</td>
								<td>${one.title}</td>
								<td><fmt:formatDate value="${one.createDate}" pattern="yyyy-MM-dd HH:mm:ss" /> </td>
								<td><fmt:formatDate value="${one.endDate}" pattern="yyyy-MM-dd HH:mm:ss" /> </td>
								<c:choose>
									<c:when test="${one.orderStatus=='3'}">
										<td>已通过</td>
									</c:when>
									<c:otherwise>
										<td>未通过</td>
									</c:otherwise>
								</c:choose>
								<td>
									<a href="<%=basePath%>/orderJztf/auditOrder.html?orderId=${one.orderId}&orderStatus=3" class="">通过</a>
									<a href="<%=basePath%>/orderJztf/auditOrder.html?orderId=${one.orderId}&orderStatus=6" class="">不通过</a>
									<a href="<%=basePath%>/orderJztf/getOrderDetail.html?orderId=${one.orderId}" class="">查看详情</a>
								</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
		<div class="popBox chaLianjie-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="chaLianjie-con">
						<div class="tit" id="lookurl"></div>
						<a href="javascript:;" class="btn w144">确  认</a>
					</div>
				</div>
			</div>
		</div><!-- chaLianjie-pop -->
			</form>
		<script type="text/javascript">
		
				function goSearch(){
			      $("#form1").submit();
			    }
				function confirmDelete(ID){
					if (confirm("是否确认")) {
						location.href='<%=basePath%>v2BannerInfo/deleteV2BannerInfo.html?bannerId='+ID;
					}  else  { 
						return false;
					};
					
				}
				function lookUrl(ID){
					$.ajax({
						type : "post",
						url : "<%=basePath%>v2BannerInfo/getV2BannerInfo.html",
						dataType : "json",
						data : {bannerId:ID},
						success : function(data) {
							var $picurl=data.picUrl;
							var $url = $("#lookurl");
							$url.append($picurl);
						},
					});
					
				}
		</script>

				