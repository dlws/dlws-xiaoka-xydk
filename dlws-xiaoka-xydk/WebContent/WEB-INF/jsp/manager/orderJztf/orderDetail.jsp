<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>校咖微平台后台管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>精准投放 > <b>详情展示</b></h2>
				</div>
				<div class="form">
						<div class="form-item">
							<div class="form-name">标题：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="skillName"  value = "${skillInfo.skillName }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">投放时间：</div>
							<div class="form-input">
								<input type="text" class="input" id="endDate" name="endDate"  value = "${orderInfo.endDate }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">文本：</div>
							<div class="form-input">
								<input type="text" class="input" id="textContent" name="textContent" value="${skillInfo.textContent }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">连接：</div>
							<div class="form-input">
								<input type="text" class="input" id="linke" name="linke"  value = "${skillInfo.linke }">
							</div>
						</div>
						
					<div class="form-item form-up">
							<div class="form-name">投放图片：</div>
							<div class="form-input">
								<ul id="pics" class="up-list">
									<c:if test="${not empty picUrls}">
										<c:forEach items="${picUrls}" var="imgUrl">
											<li>
												<div class="img">
													<img src="${imgUrl }" style="width:100px;height:100px;" alt="">
												</div>
											</li>
										</c:forEach>
									</c:if>
								</ul>
							</div>
						</div>
					<button type="button" class="btn w144" onclick="location.href='javascript:history.go(-1);'">返回</button>
				</div>
			</div>
		</div>
	</div>
</body>

</html>