<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/base.css">
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/style.css">
<script src="<%=basePath%>/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="<%=basePath%>/v2/console/js/globle.js"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<div class="column-name">
	<h2>
		发布管理><b>发布管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="<%=basePath%>/managerPublish/list.html" method="post">
	<div class="main-top">
		<div class="fr" align="center">
			<span class="c-name">名称</span> 
			<input type="text" name="name" class="input mr10" value="${paramsMap.name }"placeholder="请输入名称">
			<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>咨询类型</th>
					<th>咨询时间</th>
					<th>名称</th>
					<th>电话</th>
					<th>城市</th>
					<th>学校</th>
					<th>投入金额</th>
					<th>创建时间</th>
					<th>公司</th>
					<th>预期产出</th>
					<th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="publish">
					<tr>
						<td>${publish.resourceType }</td>
						<td>${publish.time }</td>
						<td>${publish.name }</td>
						<td>${publish.phone }</td>
						<td>${publish.city }</td>
						<td>${publish.school }</td>
						<td>${publish.money }</td>
						<td>${publish.company }</td>
						<td>${publish.output }</td>
						<td><fmt:formatDate value="${publish.createDate }"
								type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>
<script type="text/javascript">
	
</script>

