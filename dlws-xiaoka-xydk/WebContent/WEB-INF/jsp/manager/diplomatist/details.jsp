<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	
				<div class="column-name">
					<h2>外交官推广 > <b>查看详情</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>diplomatist/audit.html" id="form111" method="post" >
						<input id="id" type="hidden" name="id" value="${diplomatist.id }">
						<div class="form-item">
							<div class="form-name">姓名:</div>
							<div class="form-input">
								<input name="userName" type="text" class="input" style="border:0px" readonly= "true" value="${diplomatist.userName }">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">城市:</div>
							<div class="form-input">
								<input name="cityName" type="text" style="border:0px" readonly= "true" class="input" value="${diplomatist.cityName }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">学校:</div>
							<div class="form-input">
								<input name="schoolName" type="text" style="border:0px" readonly= "true" class="input" value="${diplomatist.schoolName }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">联系电话:</div>
							<div class="form-input">
								<input name="phone" type="text" style="border:0px" readonly= "true" class="input" value="${diplomatist.phone }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">微信:</div>
							<div class="form-input">
								<input name="wxNumber" type="text" style="border:0px" readonly= "true" class="input" value="${diplomatist.wxNumber }">
							</div>
						</div>
						<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="返  回 " onclick="javascript:history.back(-1);">
						</div>
						</form>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
</div>
