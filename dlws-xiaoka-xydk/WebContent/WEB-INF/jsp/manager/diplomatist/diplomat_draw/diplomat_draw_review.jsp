<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>外交官提现> <b>外交官提现审核</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>diplomatDraw/updateApplyWithDraw.html" id="form111" method="post" >
						<input id="id" type="hidden" name="id" value="${draw.id }">
						<div class="form-item">
							<div class="form-name">提现用户:</div>
							<div class="form-input">
								<c:choose>
									<c:when test="${draw.userName != null }">
										<input name="name" type="text" readonly="readonly" class="input" value="${draw.userName }">
									</c:when>
									<c:otherwise>
										<input name="name" type="text" readonly="readonly" class="input" value="匿名">
									</c:otherwise>
								</c:choose>
								<input type="hidden" name="openId" value="${draw.openId }"/>
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">提现金额:</div>
							<div class="form-input">
								<input name="withdrawMoney" type="text" readonly="readonly" class="input" value="${draw.withdrawMoney }">
							</div>
						</div>
						<%-- <div class="form-item">
							<div class="form-name">开户银行:</div>
							<div class="form-input">
								<input name="bank" type="text" readonly="readonly" class="input" value="${draw.bank }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">开户银行账号:</div>
							<div class="form-input">
								<input name="bankCardNumber" type="text" readonly="readonly" class="input" value="${draw.bankCardNumber }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">开户银行全称:</div>
							<div class="form-input">
								<input name="AccountFullName" type="text" readonly="readonly" class="input" value="${draw.AccountFullName }">
							</div>
						</div> --%>
						<div class="form-item">
							<div class="form-name">申请时间:</div>
							<div class="form-input">
								<input name="applicationTime" type="text" readonly="readonly" class="input" value="<fmt:formatDate  value='${draw.applicationTime }' type='both'/>">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">创建时间:</div>
							<div class="form-input">
								<input name="createTime" type="text" readonly="readonly" class="input" value="<fmt:formatDate  value='${draw.createTime }' type='both'/>">
							</div>
						</div>
						<div class="form-item">
							<div class="form-input">
							<div style="margin:40px 0 10px;">
							<input style="width:30%;height:20px;margin-left:120px;margin-top:0;" type="radio" name="applicationStatus" class="input" checked="checked" value="1">通过 </br>
							</div>
							<div style="margin:10px 0 10px;">
							<input style="width:30%;height:20px;margin-left:120px;margin-top:0;" type="radio" name="applicationStatus" class="input" value="2">拒绝
							</div>
							</div>
					   </div>
						<div class="form-item" id="refuseReason" style="display: none;">
							<div class="form-name">拒绝理由：</div>
							<div class="form-input">
								<input id="refuse" name="refuseReason" type="text" class="input">
							</div>
					   </div>
						<br/>
						<div style="text-align:center; vertical-align:middel;margin-left:-370px;">
							<input style="margin-right:20px;" type="button" class="btn w144" value="审  核" onclick="audit()">
							<input type="button" class="btn w144" value="取  消 " onclick="javascript:history.back(-1);">
						</div>
						</form>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script>
	$(":radio").click(function(){
	   var val = $(this).val();
	   if(val == 1){
		   $("#refuseReason").attr("style","display:none");
		   //$("#refuseReason").hide();
		   $("#refuse").val("").trim();
	   }else{
		   $("#refuseReason").show();
	   }
	});
	function audit(){
		var radio = $("input[name='radio']:checked").val();
		var refuse = $("#refuse").val().trim();
		if(radio == 2){
			if(refuse == ""){
				alert("请输入拒绝理由!");
				return ;
			}
		}
		$("#form111").submit();
	}
</script>
</div>
