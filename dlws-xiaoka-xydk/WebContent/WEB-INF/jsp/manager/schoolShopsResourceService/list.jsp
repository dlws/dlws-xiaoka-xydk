<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
				<div class="column-name">
					<h2>资源服务 ><b>校内商家资源</b></h2>
				</div>
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>城市</th>
								<th>学校</th>
								<th>服务名称</th>
								<th>服务性质</th>
								<th>服务价格</th>
								<th>服务介绍</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="schoolShops">
							<!-- 遍历数据 -->
							<tr>
								<td>${schoolShops.cityName }</td>
								<td>${schoolShops.schoolName }</td>
								<td>${schoolShops.skillName }</td>
								<c:choose>
									<c:when test="${schoolShops.serviceType == 1 }">
										<td>线上服务</td>
									</c:when>
									<c:otherwise>
										<td>线下服务</td>
									</c:otherwise>
								</c:choose>
								<td>${schoolShops.skillPrice }</td>
								<td>${schoolShops.skillDepict }</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
	</form>