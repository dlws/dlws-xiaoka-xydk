<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>基本信管理> <b>基本信息修改</b></h2>
				</div>
				 <div class="form1" name="form1">
					<form action="<%=basePath%>userBasicinfo/updateCorpratSettle.html" id="addPartForm" method="post" enctype="multipart/form-data">
						<input id="id" type="hidden" name="id" value="${objMap.id }">
						<div class="form-item">
							<div class="form-name">选择省:</div>
								<div class="form-input">
									<select class="input" name="proId" id="proId" onchange="queryCityByPro(this.value);">
										 <option value="">请选择</option>
										<c:forEach items="${provnceList }" var="provc">
												<option value="${provc.province}">${provc.province }</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						 <div class="form-item">
							<div class="form-name">选择城市:</div>
								<div class="form-input">
									<select class="input" name="cityId" id="cityId" onchange="queryScholTypeByCity(this.value);">
									 <option value="">请选择</option>
										<!-- <option value="">全部</option> -->
										
										<c:forEach items="${cityList }" var="city">
										<c:choose>
											<c:when test="${objMap.cityId==city.value}">
											
													<option value="${city.value}" selected = "selected">${city.text }</option>
											</c:when>
											<c:otherwise>
												<option value="${city.value}">${city.text }</option>
											</c:otherwise>
											</c:choose>
										</c:forEach>
									</select> 
								</div>
						</div> 
						
						<div class="form-item">
							<div class="form-name">选择院校类别:</div>
								<div class="form-input">
									<select class="input" name="scholType" id="scholType" onchange="queryScholByType(this.value);">
										<!-- <option value="">全部</option> -->
										 <option value="">请选择</option>
										<c:forEach items="${scholTypeList }" var="stp">
												<option value="${stp.value}">${stp.text}</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						
						<div class="form-item">
								<div class="form-name">学校:</div>
								<div class="form-input">
									<select name="schoolId" class="input" id="schools">
										 <option value="${objMap.schoolId}">${sholMap.schoolName}</option>
									</select>
								</div>
						</div>
						
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='zylx'}">
								<div class="form-item">
										<div class="form-name">资源类型:</div>
										<div class="form-input">
											<select name="zylx" class="input" id="zylx">
												<c:forEach items="${resourceList}" var="item">
												<c:choose>
													<c:when test="${one.typeValue==item.id}">
														 <option value="${item.id}" selected="selected">${item.className }</option>
													</c:when>
													<c:otherwise>
													 	<option value="${item.id}">${item.className }</option>
													</c:otherwise>
												</c:choose>
												</c:forEach>
											</select>
										</div>
								</div>
							</c:if>
						</c:forEach>
						
								<div class="form-item">
									<div class="form-name">群名称:</div>
									<div class="form-input">
										<input name="userName" type="text" class="input" id="userName" value="${objMap.userName}">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
						
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='groupNum'}">
								<div class="form-item">
									<div class="form-name">群人数:</div>
									<div class="form-input">
										<input name="groupNum" type="text" class="input" id="groupNum" value="${one.typeValue }">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
							</c:if>
						</c:forEach>
						
						<div class="form-item form-item01">
							<div class="form-name">社群头像:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value="${objMap.keyValue }"/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${objMap.headPortrait}"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl" src="${objMap.headPortrait}">
    							</div>
							</div>
						</div>
						
						
						<c:forEach items="${settleList}" var="one">
							<c:choose>
								<c:when test="${one.typeName=='activCase'}">
									<div class="form-item">
										<div class="form-name">活动案例:</div>
										<div class="form-input">
											<input name="activCase" id="activCase" type="text" class="input" value="${one.typeValue }">
										</div>
									</div>
								</c:when>
							</c:choose>
						</c:forEach>
						
						<div class="form-item">
							<div class="form-name">我的介绍:</div>
							<div class="form-input">
								<input name="aboutMe" id="aboutMe" type="text" class="input" value="${objMap.aboutMe }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系电话:</div>
							<div class="form-input">
								<input name="phoneNumber" id="phoneNumber" type="text" class="input" value="${objMap.phoneNumber }"  placeholder="请输入11位电话号码">
								<div class="clearfix"></div>
								<div class="form-error red"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系人:</div>
							<div class="form-input">
								<input name="contactUser" id="contactUser" type="text" class="input" value="${objMap.contactUser }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系邮箱:</div>
							<div class="form-input">
								<input name="email" id="email" type="text" class="input" value="${objMap.email }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">微信号:</div>
							<div class="form-input">
								<input name="wxNumber" id="wxNumber" type="text" class="input" value="${objMap.wxNumber }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">访问量:</div>
							<div class="form-input">
								<input name="viewNum" id="viewNum" type="text" class="input" value="${objMap.viewNum}">
							</div>
						</div>
						<br/>
						<input type="hidden" name="activtyUrl"  class="input" id="activtyUrl" value="${objMap.imgString}">
						<input type="hidden" name="memNumberImg"  class="input" id="memNumberImg" value="${objMap.memNumberImg}">
						<input type="hidden" name="province" id="province" value="${proMap.province}">
						<input type="hidden" name="cityIds" id="cityIds" value="${objMap.cityId }">
						<input type="hidden" name="id" id="id" value="${objMap.id}">
						
						<div class="form-item form-up">
							<div class="form-name">群成员图：</div>
							<div class="form-input">
								<div class="up-btn">
								</div>
									<input type="file" name="memberImg" id="memberImg" multiple size="80" class="btn-blue" onchange="uploadPic(1)"/> 
								<ul id="ul1" class="up-list">
									<c:if test="${not empty memNumberImg}">
										<c:forEach var="imgUrl" items="${memNumberImg}">
											<li>
												<div class="txt">
													<span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage('${imgUrl}','${imgUrl}')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${imgUrl}" alt="">
													<input type='hidden' name='memNumberImgV' value='${imgUrl}'>
												</div>
											</li>
										</c:forEach>
									</c:if>
								</ul>
							</div>
						</div>
						
						<div class="form-item form-up">
							<div class="form-name">活动图片：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="activtyUrl" id="" multiple size="80" class="btn-blue" onchange="uploadPic(2)"/> 
								</div>
								<ul id="ul2" class="up-list">
									<c:if test="${not empty imgArr}">
										<c:forEach var="imgUrl" items="${imgArr}">
											<li>
												<div class="txt">
													<span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage1('${imgUrl}','${imgUrl}')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${imgUrl}" alt="">
													<input type='hidden' name='activtyUrlV' value='${imgUrl}'>
												</div>
											</li>
										</c:forEach>
									</c:if>
								</ul>
							</div>
						</div>
						</form>
						
						
					<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="upCorprat()">
						</div>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script>
	
		function upCorprat(){
			var obj=document.getElementsByName('activtyUrlV'); 
			   //取到对象数组后，来循环检测它是不是被选中
			   var s='';
			   for(var i=0; i<obj.length; i++){
			     s+=obj[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(s==''){
				s="";
			   }else{
				   s=s.substring(0, s.length-1);   
			   }
			   $("#activtyUrl").val(s);
			   
		    var NumberObj=document.getElementsByName('memNumberImgV');  //群成员
			   //取到对象数组后，来循环检测它是不是被选中
			   var snum='';
			   for(var i=0; i<NumberObj.length; i++){
				   snum+=NumberObj[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(snum==''){
				   snum="";
			   }else{
				   snum=snum.substring(0, snum.length-1);  
			   }
				  $("#memNumberImg").val(snum);
			$("#addPartForm").submit();
		}
			//上传图片
			function uploadPic(){
				//异步上传
				var options = {
						url : "<%=basePath %>bannerInfo/picture.html",
						dataType : "json",
						type : "post",
						success : function(data){
							//回调 路径  data.path
							$("#allUrl").attr("src",data.picUrl);
							$("#keyValue").val(data.key);
							$("#picUrl").val(data.picUrl);
						}
				}
				$("#addPartForm").ajaxSubmit(options);
			}
		
		
			//上传图片
			function uploadPic(obj){
				var ulId="";
				var url="";
				if(obj==1){
					url="memNumberImg";
					name="memNumberImgV";
					ulId ="ul1";
				}
				else if(obj==2){
					url="activtyUrl";
					name="activtyUrlV"
					ulId ="ul2";
				}
				var picListHtml ="";
				var options = {
						url : '<%=basePath %>enter/'+url+'.html',
						dataType : "json",
						type : "post",
						success : function(data){
							for(var i=0;i<data.length;i++){	
								picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick=deleteDetailImage("'+data[i]+'","'+name+'") class="remove">删除</a></div><div class="img"><img src="'+data[i]+'" width="200" height="200" alt=""></div></li>'+
								'<input type="hidden"  name="'+name+'" value='+data[i]+'>';
							}
							$("#"+ulId).append(picListHtml);
							$("#"+ulId).show();
						}
				}
				$("#addPartForm").ajaxSubmit(options); 
			}
			
			
			function deleteDetailImage(imgUrl){
				var imgLi = $("img[src='"+imgUrl+"']").parent().parent();
				var imgInput = $("input[name=imgUrl][value='"+imgUrl+"']");
				console.log(imgInput.prop("outerHTML"));
				//删除图片，父li移除，删除input
				console.log($("input[name=imgUrl]").size());
				imgLi.remove();
				imgInput.remove();
				alert("删除成功！");
				console.log($("input[name=imgUrl]").size());
				return true;
			}
			
			function deleteDetailImage1(imgUrl){
				var imgLi = $("img[src='"+imgUrl+"']").parent().parent();
				var imgInput = $("input[name=imgUrl][value='"+imgUrl+"']");
				console.log(imgInput.prop("outerHTML"));
				//删除图片，父li移除，删除input
				console.log($("input[name=imgUrl]").size());
				imgLi.remove();
				imgInput.remove();
				alert("删除成功！");
				console.log($("input[name=imgUrl]").size());
				return true;
			}
		
		
		//根据省查出市
		function queryCityByPro(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryCityByPro.html",
				dataType : "json",
				data : {proId:value},
				success : function(data) {
					 $("#cityId option").remove();
					 $("#scholType option").remove();
					 $("#schools option").remove();
					var $sel = $("#cityId");
					if(data.cityList.length>0){
						 $.each(data.cityList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholTypeByCity(data.cityList[0].value);
					}
				}
			});	
			
		}
		
		//根据市查出院校类别
		 function queryScholTypeByCity(value){
			 $("#scholType option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#scholType").append($option0)
			 
			  $("#schools option").remove();
			  var $option1 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option1)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholTypeByCity.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $sel = $("#scholType");
					if(data.scholTypeList.length>0){
						$.each(data.scholTypeList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholByType(data.scholTypeList[0].value);//根据院校类别查出院校
					}
					
				}
			});	
		}
		//根据院校类别查出院校
		 function queryScholByType(value){
			 $("#schools option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option0)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholByType.html",
				dataType : "json",
				data : {dcdaId:value,cityId:$("#cityIds").val()},
				success : function(data) {
					var $sel = $("#schools");
					if(data.scholList.length>0){
						$.each(data.scholList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		} 
</script>
</div>
