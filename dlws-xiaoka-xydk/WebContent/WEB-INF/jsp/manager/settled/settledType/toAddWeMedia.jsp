<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>基本信管理> <b>基本信添加</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>userBasicinfo/addSettleUser.html" id="addPartForm" method="post" enctype="multipart/form-data">
					<div class="form-item">
							<div class="form-name">选择省:</div>
								<div class="form-input">
									<select class="input" name="proId" id="proId" onchange="queryCityByPro(this.value);">
										<option value="">请选择</option>
										<c:forEach items="${provnceList }" var="provc">
												<option value="${provc.province}">${provc.province }</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						 <div class="form-item">
							<div class="form-name">选择城市:</div>
								<div class="form-input">
									<select class="input" name="cityId" id="cityId" onchange="queryScholTypeByCity(this.value);">
										<option value="">请选择</option>
										<c:forEach items="${cityList }" var="city">
											<c:if test="${city.id == objMap.citycode }">
												<option value="${city.id}" selected = "selected">${city.cityname }</option>
											</c:if>
											<c:if test="${city.id != objMap.citycode }">
												<option value="${city.id}">${city.cityname }</option>
											</c:if>
										</c:forEach>
									</select> 
								</div>
						</div> 
						
						<div class="form-item">
							<div class="form-name">选择院校类别:</div>
								<div class="form-input">
									<select class="input" name="scholType" id="scholType" onchange="queryScholByType(this.value);">
										<!-- <option value="">全部</option> -->
										<c:forEach items="${scholTypeList }" var="stp">
												<option value="${stp.value}">${stp.text}</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						
						<div class="form-item">
								<div class="form-name">学校:</div>
								<div class="form-input">
									<select name="schoolId" class="input" id="schools">
										 <option value="${objMap.schoolId}">${objMap.schoolName }</option>
									</select>
								</div>
						</div>
					
						<div class="form-item">
								<div class="form-name">资源类型:</div>
								<div class="form-input">
									<select name="zylx" class="input" id="zylx" onchange="changeContent()">
										<c:forEach items="${resourceList}" var="item">
											 <option value="${item.classValue}">${item.className}</option>
										</c:forEach>
									</select>
								</div>
						</div>
						
							<div class="form-item">
								<div class="form-name" id="gzhName">公众号名称:</div>
								<div class="form-input">
									<input name="userName" type="text" class="input" id="userName" value="" maxlength="10">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
						<div id="weMedio">
							<div class="form-item">
								<div class="form-name">公众号ID:</div>
								<div class="form-input">
									<input name="pubNumId" type="text" class="input" id="pubNumId" value=""  maxlength="10">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>	
							<div class="form-item">
								<div class="form-name">粉丝人数:</div>
								<div class="form-input">
									<input name="fansNum" type="text" class="input" id="fansNum" value="">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							
							<div class="form-item">
								<div class="form-name">平均阅读数量:</div>
								<div class="form-input">
									<input name="readNum" type="text" class="input" id="readNum" value="">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							
							<div class="form-item">
								<div class="form-name">头条报价:</div>
								<div class="form-input">
									<input name="topLinePrice" type="text" class="input" id="topLinePrice" value="">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							
							<div class="form-item">
								<div class="form-name">次条报价:</div>
								<div class="form-input">
									<input name="lessLinePrice" type="text" class="input" id="lessLinePrice" value="">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
						</div>
						
						<div id="parTime">
							<div class="form-item">
								<div class="form-name">群人数:</div>
								<div class="form-input">
									<input name="groupNum" type="text" class="input" id="groupNum" value="">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
						</div>
						
						<div class="form-item form-item01">
							<div class="form-name" id="headPic">公众号头像:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value="${key }"/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${picUrl }"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPicSim()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl"  src="${picUrl}" width="200" height="200">
    							</div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">活动案例:</div>
							<div class="form-input">
								<input name="activCase" id="activCase" type="text" class="input" value="">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">我的介绍:</div>
							<div class="form-input">
								<input name="aboutMe" id="aboutMe" type="text" class="input" value="${user.aboutMe }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系电话:</div>
							<div class="form-input">
								<input name="phoneNumber" id="phoneNumber" type="text" class="input" value="${user.phoneNumber }"  placeholder="请输入11位电话号码">
								<div class="clearfix"></div>
								<div class="form-error red"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系人:</div>
							<div class="form-input">
								<input name="contactUser" id="contactUser" type="text" class="input" value=""  maxlength="10">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系邮箱:</div>
							<div class="form-input">
								<input name="email" id="email" type="text" class="input" value="${user.email }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">微信号:</div>
							<div class="form-input">
								<input name="wxNumber" id="wxNumber" type="text" class="input" value="${user.wxNumber }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">访问量:</div>
							<div class="form-input">
								<input name="viewNum" id="viewNum" type="text" class="input">
							</div>
						</div>
						<input type="hidden" name="cityIds" id="cityIds" value="">
						<input name="enterType" type="hidden" class="input" id="enterType" value="${enterTypeName}">
						<input name="dairyTweetUrl" type="hidden" class="input" id="dairyTweetUrl" >
						<input type="hidden" name="activtyUrl"  class="input" id="activtyUrl" >
						<input name="enterId" type="hidden" class="input" id="enterId" value="${paramsMap.enterId }">
						
						
						<div class="form-item form-up">
							<div class="form-name" id="picDiary">日常推文图：</div>
							<div class="form-input">
								<div class="up-btn">
								</div>
									<input type="file" name="dairyTweet" id="dairyTweet" multiple size="80" class="btn-blue" onchange="uploadPic(1)"/> 
								<ul id="ul1" class="up-list hide">
									
								</ul>
							</div>
						</div>
						
						<br/> 
						<div class="form-item form-up">
							<div class="form-name">活动图片：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="activtyUrl"  multiple size="80" class="btn-blue" onchange="uploadPic(2)"/> 
								</div>
								<ul id="ul2" class="up-list hide">
									
								</ul>
							</div>
						</div>
						
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="addCorprat()">
						</div>
						</form>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script>
		function addCorprat(){
			var zylxVal = $("#zylx").val();
			
			var schools = $("#schools").val();
			if(schools==""||schools==0){
				alert("请选择学校！");
				return
			}
			
			var userName = $("#userName").val();
			if(userName==""){
				 if(zylxVal=='zmt'){
			     alert("请输入公众号名称");
				 return 
				 }else{
				 alert("请输入群名称");
				 return
				 }
			}
			
			if(zylxVal=='zmt'){
				var pubNumId = $("#pubNumId").val();
				if(pubNumId==""){
					 alert("请输入公众号Id");
					 return
				}
				
				var fansNum = $("#fansNum").val();
				if(fansNum==""){
					 alert("请输入粉丝数量");
					 return;
				}
				var readNum = $("#readNum").val();
				if(readNum==""){
					 alert("请输入平均阅读量");
					 return;
				}
				var topLinePrice = $("#topLinePrice").val();
				if(topLinePrice==""){
					 alert("请输入头条报价");
					 return;
				}
				var lessLinePrice = $("#lessLinePrice").val();
				if(lessLinePrice==""){
					 alert("请输入次条报价");
					 return;
				}
			}else{
				var groupNum = $("#groupNum").val();
				if(groupNum==''){
					 alert("请输入群人数");
					 return;
				}
			}
			
			var picUrl = $.trim($("#picUrl").val());
			if(picUrl==""){
				alert("头像不能为空！");
				return
			}
			
			var activCase = $("#activCase").val();
			if(activCase=="" || activCase.length<20 || activCase.length>200){
				alert("请输入活动案列,长度20-200");
				return
			}
			
			var aboutMe = $("#aboutMe").val();
			if(aboutMe=="" || aboutMe.length<20 || aboutMe.length>200){
				alert("请输入介绍,长度20-200");
				return;				
			}
			
			var phoneNumber=$("#phoneNumber").val()
   			var re=/^1[3|5|8|7]\d{9}$/;
	   		if(phoneNumber==""){
	   			alert("请输入手机号");
	   			return;
	   		}else if(!re.test(phoneNumber)){
	   			alert("请输入正确的手机号");
	   			return;
	   		}
	   		
	   		var contactUser = $("#contactUser").val();
	   		if(contactUser=="" || contactUser.length>10){
	   			alert("请输入联系人,最多10个字符");
	   			return;
	   		}
	   		
	   		var email=$("#email").val()
	   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		if(email==""){
	   			alert("请输入邮箱");
	   			return;
		   		
	   		}else if(!regEma.test(email)){
	   			alert("邮箱格式有误");
	   			return
	   		}
	   		
	   		var wxNumber=$("#wxNumber").val()
	   		var regWx=/^[a-zA-Z\d_]{5,}$/;
	   		
	   		if(wxNumber==""){
	   			alert("请输入微信号");
	   			return;
	   		}else if(!regWx.test(wxNumber)){
	   			alert("微信号格式有误");
	   			return
	   		}
	   		
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
			
			 var obj=document.getElementsByName('dairyTweetImg');  //日常推文图片
			   //取到对象数组后，来循环检测它是不是被选中
			   var s='';
			   for(var i=0; i<obj.length; i++){
			     s+=obj[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(s==''){
				s="";
			   }else{
				   s=s.substring(0, s.length-1);   
			   }
			   $("#dairyTweetUrl").val(s); 
			   
			   var aUrlLength=$("#ul1").children("li").length;
			   
			   if(s==''){
				   if(zylxVal=='zmt'){
					 alert("日常推文图不能为空");
			   		 return;
				   }else{
					 alert("群成员图不能为空");
				   	 return; 
				   }
			   }
			   if(s!=""){
				   if(zylxVal=='zmt'){
					   if(aUrlLength<1||aUrlLength>9){
						 alert("日常推文图个数为1-9");
				   		 return;
					   }
				   }else{
					   if(aUrlLength<1||aUrlLength>2){
						 alert("群成员图个数为1-2");
				   		 return;
					   }
				   }
			   }
			 //活动图片
			 var activ = document.getElementsByName('activtyUrlImg'); 
		
			   var actent='';
			   for(var i=0; i<activ.length; i++){
				   actent+=activ[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(actent==''){
				   actent="";
			   }else{
				   actent=actent.substring(0, actent.length-1);   
			   }
			   
			   $("#activtyUrl").val(actent); 
			   var acUrlLength=$("#ul2").children("li").length;
			   if(actent==''){
				   alert("活动图片不能为空");
			   		 return;
			   }
			   if(actent!=''&&acUrlLength<2||acUrlLength>9){
				   alert("活动图片个数为2-9张");
			   		return;
			   }
			$("#addPartForm").submit();
		}
		//上传图片
		function uploadPicSim(){
			//异步上传
			var options = {
					url : "<%=basePath %>bannerInfo/picture.html",
					dataType : "json",
					type : "post",
					success : function(data){
						$("#allUrl").attr("src",data.picUrl);
						$("#keyValue").val(data.key);
						$("#picUrl").val(data.picUrl);
					}
			}
			$("#addPartForm").ajaxSubmit(options);
		}
		
		
		//上传图片
		function uploadPic(obj){
			var ulId="";
			var url="";
			if(obj==1){
				url="dairyTweet";
				name="dairyTweetImg";
				ulId ="ul1";
			}
			else if(obj==2){
				url="activtyUrl";
				name="activtyUrlImg";
				ulId ="ul2";
			}
			var picListHtml ="";
			var options = {
					url : '<%=basePath %>enter/'+url+'.html',
					dataType : "json",
					type : "post",
					success : function(data){
						for(var i=0;i<data.length;i++){	
							picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick=deleteDetailImage("'+data[i]+'","'+name+'") class="remove">删除</a></div><div class="img"><img src="'+data[i]+'" width="200" height="200" alt=""></div></li>'+
							'<input type="hidden"  name="'+name+'" value='+data[i]+'>';
						}
						$("#"+ulId).append(picListHtml);
						$("#"+ulId).show();
					}
			}
			$("#addPartForm").ajaxSubmit(options); 
		}
		
		
		function deleteDetailImage(imgUrl){
			var imgLi = $("img[src='"+imgUrl+"']").parent().parent();
			var imgInput = $("input[name=imgUrl][value='"+imgUrl+"']");
			console.log(imgInput.prop("outerHTML"));
			//删除图片，父li移除，删除input
			console.log($("input[name=imgUrl]").size());
			imgLi.remove();
			imgInput.remove();
			alert("删除成功！");
			console.log($("input[name=imgUrl]").size());
			return true;
		}
		
		
		//根据省查出市
		function queryCityByPro(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryCityByPro.html",
				dataType : "json",
				data : {proId:value},
				success : function(data) {
					 $("#cityId option").remove();
					 $("#scholType option").remove();
					 $("#schools option").remove();
					var $sel = $("#cityId");
					if(data.cityList.length>0){
						 $.each(data.cityList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholTypeByCity(data.cityList[0].value);
					}
				}
			});	
			
		}
		
		//根据市查出院校类别
		 function queryScholTypeByCity(value){
			 $("#scholType option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#scholType").append($option0)
			 
			  $("#schools option").remove();
			  var $option1 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option1)
			$("#cityIds").val(value);
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholTypeByCity.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $sel = $("#scholType");
					if(data.scholTypeList.length>0){
						$.each(data.scholTypeList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholByType(data.scholTypeList[0].value);//根据院校类别查出院校
					}
					
				}
			});	
		}
		//根据院校类别查出院校
		 function queryScholByType(value){
			 $("#schools option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option0)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholByType.html",
				dataType : "json",
				data : {dcdaId:value,cityId:$("#cityIds").val()},
				success : function(data) {
					var $sel = $("#schools");
					if(data.scholList.length>0){
						$.each(data.scholList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		}  
		function changeContent(){
		 var zylxVal = $("#zylx").val();
		    if(zylxVal=='zmt'){
		    	$("#gzhName").html("公众号名称:");
		    	$("#pubNumId").removeAttr("disabled",true);
				$("#fansNum").removeAttr("disabled",true);
				$("#readNum").removeAttr("disabled",true);
				$("#topLinePrice").removeAttr("disabled",true);
				$("#lessLinePrice").removeAttr("disabled",true);
				$("#groupNum").attr("disabled",true);
				$("#picDiary").html("日常推文图：");
				$("#weMedio").show();
				$("#parTime").hide();
				$("#headPic").html("公众号头像：")
		    }else{
				$("#pubNumId").attr("disabled",true);
				$("#fansNum").attr("disabled",true);
				$("#readNum").attr("disabled",true);
				$("#topLinePrice").attr("disabled",true);
				$("#lessLinePrice").attr("disabled",true);
				$("#groupNum").removeAttr("disabled");
				$("#headPic").html("社群头像：")
		    	$("#gzhName").html("群名称:");
				$('#lessLinePrice').removeAttr("disabled");
		    	$("#picDiary").html("群成员图：");
			$("#weMedio").hide();
			$("#parTime").show();
		    }
		}
		
		$(function(){
			$("#pubNumId").attr("disabled",true);
			$("#fansNum").attr("disabled",true);
			$("#readNum").attr("disabled",true);
			$("#topLinePrice").attr("disabled",true);
			$("#lessLinePrice").attr("disabled",true);
			$("#groupNum").removeAttr("disabled");
			$("#headPic").html("社群头像：")
	    	$("#gzhName").html("群名称:");
			$('#lessLinePrice').removeAttr("disabled");
	    	$("#picDiary").html("群成员图：");
			$("#weMedio").hide();
			$("#parTime").show();
		})
</script>

