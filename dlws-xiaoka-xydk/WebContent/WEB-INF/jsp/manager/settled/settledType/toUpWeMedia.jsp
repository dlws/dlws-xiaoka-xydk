<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>基本信管理> <b>基本信息修改</b></h2>
				</div>
				 <div class="form1" name="form1">
					<form action="<%=basePath%>userBasicinfo/updateCorpratSettle.html" id="addPartForm" method="post" enctype="multipart/form-data">
						<input id="id" type="hidden" name="id" value="${objMap.id }">
						<div class="form-item">
							<div class="form-name">选择省:</div>
								<div class="form-input">
									<select class="input" name="proId" id="proId" onchange="queryCityByPro(this.value);">
										 <option value="">请选择</option>
										<c:forEach items="${provnceList }" var="provc">
												<option value="${provc.province}">${provc.province }</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						 <div class="form-item">
							<div class="form-name">选择城市:</div>
								<div class="form-input">
									<select class="input" name="cityId" id="cityId" onchange="queryScholTypeByCity(this.value);">
									 <option value="">请选择</option>
										<!-- <option value="">全部</option> -->
										
										<c:forEach items="${cityList }" var="city">
										<c:choose>
											<c:when test="${objMap.cityId==city.value}">
											
													<option value="${city.value}" selected = "selected">${city.text }</option>
											</c:when>
											<c:otherwise>
												<option value="${city.value}">${city.text }</option>
											</c:otherwise>
											</c:choose>
										</c:forEach>
									</select> 
								</div>
						</div> 
						
						<div class="form-item">
							<div class="form-name">选择院校类别:</div>
								<div class="form-input">
									<select class="input" name="scholType" id="scholType" onchange="queryScholByType(this.value);">
										<!-- <option value="">全部</option> -->
										 <option value="">请选择</option>
										<c:forEach items="${scholTypeList }" var="stp">
												<option value="${stp.value}">${stp.text}</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						
						<div class="form-item">
								<div class="form-name">学校:</div>
								<div class="form-input">
									<select name="schoolId" class="input" id="schools">
										 <option value="${objMap.schoolId}">${sholMap.schoolName}</option>
									</select>
								</div>
						</div>
						
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='zylx'}">
								<div class="form-item">
										<div class="form-name">资源类型:</div>
										<div class="form-input">
											<select name="zylx" class="input" id="zylx" onchange="changeContent()">
												<c:forEach items="${resourceList}" var="item">
												<c:choose>
													<c:when test="${one.typeValue==item.classValue}">
														 <option value="${item.classValue}" selected="selected">${item.className }</option>
													</c:when>
													<c:otherwise>
													 	<option value="${item.classValue}">${item.className }</option>
													</c:otherwise>
												</c:choose>
												</c:forEach>
											</select>
										</div>
								</div>
							</c:if>
						</c:forEach>
						
								<div class="form-item">
									<div class="form-name" id="gzhName">公众号名称:</div>
									<div class="form-input">
										<input name="userName" type="text" class="input" id="userName" value="${objMap.userName}" maxlength="10">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
								
						<div id="weMedio">		
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='pubNumId'}">
							<ul class="pubNumIdUl">
								<div class="form-item">
									<div class="form-name">公众号ID:</div>
									<div class="form-input">
										<input name="pubNumId" type="text" class="input" id="pubNumId" value="${one.typeValue}">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
							</ul>
							</c:if>
						</c:forEach>
							<ul class="pubNumIdUl">
								<div class="form-item">
									<div class="form-name">公众号ID:</div>
									<div class="form-input">
										<input name="pubNumId" type="text" class="input" id="pubNumId" value="">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
							</ul>	
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='fansNum'}">
							<ul class="fansNumUl">
								<div class="form-item">
									<div class="form-name">粉丝人数:</div>
									<div class="form-input">
										<input name="fansNum" type="text" class="input" id="fansNum" value="${one.typeValue}">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
							</ul>
							</c:if>
						</c:forEach>
							<ul class="fansNumUl">
								<div class="form-item">
									<div class="form-name">粉丝人数:</div>
									<div class="form-input">
										<input name="fansNum" type="text" class="input" id="fansNum" value="">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
							</ul>	
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='readNum'}">
							<ul class="readNumUl">
								<div class="form-item">
									<div class="form-name">平均阅读数量:</div>
									<div class="form-input">
										<input name="readNum" type="text" class="input" id="readNum" value="${one.typeValue}">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
							</ul>
							</c:if>
						</c:forEach>
							<ul class="readNumUl">
								<div class="form-item">
									<div class="form-name">平均阅读数量:</div>
									<div class="form-input">
										<input name="readNum" type="text" class="input" id="readNum" value="">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
							</ul>	
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='topLinePrice'}">
							<ul class="topLinePriceUl">
								<div class="form-item">
									<div class="form-name">头条报价:</div>
									<div class="form-input">
										<input name="topLinePrice" type="text" class="input" id="topLinePrice" value="${one.typeValue}">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
								</ul>
							</c:if>
						</c:forEach>
							<ul class="topLinePriceUl">
								<div class="form-item">
									<div class="form-name">头条报价:</div>
									<div class="form-input">
										<input name="topLinePrice" type="text" class="input" id="topLinePrice" value="">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
								</ul>	
						<c:forEach items="${settleList}" var="one">
							<c:if test="${one.typeName=='lessLinePrice'}">
							<ul class="lessLinePriceUl">
								<div class="form-item">
									<div class="form-name">次条报价:</div>
									<div class="form-input">
										<input name="lessLinePrice" type="text" class="input" id="lessLinePrice" value="${one.typeValue}">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
								</ul>
							</c:if>
						</c:forEach>
							<ul class="lessLinePriceUl">
								<div class="form-item">
									<div class="form-name">次条报价:</div>
									<div class="form-input">
										<input name="lessLinePrice" type="text" class="input" id="lessLinePrice" value="">
										<div class="clearfix"></div>
										<div class="form-msg"></div>
									</div>
								</div>
								</ul>
						</div>
						
						<div id="parTime">
							<c:forEach items="${settleList}" var="one" varStatus="stat">
								<c:if test="${one.typeName=='groupNum'}">
									<ul class="groupNumUl">
									<div class="form-item">
										<div class="form-name">群人数:</div>
										<div class="form-input">
											<input name="groupNum" type="text" class="input" id="groupNum" value="${one.typeValue }">
											<div class="clearfix"></div>
											<div class="form-msg"></div>
										</div>
									</div>
									</ul>
								</c:if>
							</c:forEach>
									<ul class="groupNumUl">
									<div class="form-item">
										<div class="form-name">群人数:</div>
										<div class="form-input">
											<input name="groupNum" type="text" class="input" id="groupNum" value="${one.typeValue }">
											<div class="clearfix"></div>
											<div class="form-msg"></div>
										</div>
									</div>
									</ul>
						</div>
						
						<div class="form-item form-item01">
							<div class="form-name" id="headPic">公众号头像:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value="${objMap.keyValue }"/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${objMap.headPortrait}"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPicSim()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl" src="${objMap.headPortrait}" width="200" height="200">
    							</div>
							</div>
						</div>
						
						
						<c:forEach items="${settleList}" var="one">
							<c:choose>
								<c:when test="${one.typeName=='activCase'}">
								<ul class="caseUl">
									<div class="form-item">
										<div class="form-name">活动案例:</div>
										<div class="form-input">
											<input name="activCase" id="activCase" type="text" class="input" value="${one.typeValue }">
										</div>
									</div>
									</ul>
								</c:when>
							</c:choose>
						</c:forEach>
								<ul class="caseUl">
								<div class="form-item">
										<div class="form-name">活动案例:</div>
										<div class="form-input">
											<input name="activCase" id="activCase" type="text" class="input" value="${one.typeValue }">
										</div>
									</div>
									</ul>
						
						<div class="form-item">
							<div class="form-name">我的介绍:</div>
							<div class="form-input">
								<input name="aboutMe" id="aboutMe" type="text" class="input" value="${objMap.aboutMe }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系电话:</div>
							<div class="form-input">
								<input name="phoneNumber" id="phoneNumber" type="text" class="input" value="${objMap.phoneNumber }"  placeholder="请输入11位电话号码">
								<div class="clearfix"></div>
								<div class="form-error red"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系人:</div>
							<div class="form-input">
								<input name="contactUser" id="contactUser" type="text" class="input" value="${objMap.contactUser }" maxlength="10">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系邮箱:</div>
							<div class="form-input">
								<input name="email" id="email" type="text" class="input" value="${objMap.email }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">微信号:</div>
							<div class="form-input">
								<input name="wxNumber" id="wxNumber" type="text" class="input" value="${objMap.wxNumber }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">访问量:</div>
							<div class="form-input">
								<input name="viewNum" id="viewNum" type="text" class="input" value="${objMap.viewNum}">
							</div>
						</div>
						<br/>
						<input type="hidden" name="activtyUrl"  class="input" id="activtyUrl" value="${objMap.imgString}">
						<input type="hidden" name="dairyTweetUrl" id="dairyTweetUrl" class="input"  >
						<input type="hidden" name="province" id="province" value="${proMap.province}">
						<input type="hidden" name="cityIds" id="cityIds" value="${objMap.cityId }">
						<input type="hidden" name="id" id="id" value="${objMap.id}">
						
						<div class="form-item form-up">
							<div class="form-name" id="picDiary">日常推文图：</div>
							<div class="form-input">
								<div class="up-btn">
								</div>
									<input type="file" name="dairyTweet" id="dairyTweet" multiple size="80" class="btn-blue" onchange="uploadPic(1)"/> 
								<ul id="ul1" class="up-list">
									<c:if test="${not empty dairyTweetImg}">
										<c:forEach var="imgUrl" items="${dairyTweetImg}">
											<li>
												<div class="txt">
													<span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage('${imgUrl}','${imgUrl}')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${imgUrl}" alt="">
													<input type='hidden' name='dairyTweetImg' value='${imgUrl}'>
												</div>
											</li>
										</c:forEach>
									</c:if>
								</ul>
							</div>
						</div>
						
						
						<div class="form-item form-up">
							<div class="form-name">活动图片：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="activtyUrl" id="" multiple size="80" class="btn-blue" onchange="uploadPic(2)"/> 
								</div>
								<ul id="ul2" class="up-list">
									<c:if test="${not empty imgArr}">
										<c:forEach var="imgUrl" items="${imgArr}">
											<li>
												<div class="txt">
													<span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage1('${imgUrl}','${imgUrl}')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${imgUrl}" alt="">
													<input type='hidden' name='activtyUrlV' value='${imgUrl}'>
												</div>
											</li>
										</c:forEach>
									</c:if>
								</ul>
							</div>
						</div>
						</form>
						
						
					<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="upCorprat()">
						</div>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script>
	
		function upCorprat(){
		var zylxVal = $("#zylx").val();
			var schools = $("#schools").val();
			if(schools==""||schools==0){
				alert("请选择学校！");
				return
			}
			
			var userName = $("#userName").val();
			if(userName==""){
				 if(zylxVal=='zmt'){
			     alert("请输入公众号名称");
				 return 
				 }else{
				 alert("请输入群名称");
				 return
				 }
			}
			
			if(zylxVal=='zmt'){
				var pubNumId = $("#pubNumId").val();
				if(pubNumId==""){
					 alert("请输入公众号Id");
					 return
				}
				
				var fansNum = $("#fansNum").val();
				if(fansNum==""){
					 alert("请输入粉丝数量");
					 return;
				}
				var readNum = $("#readNum").val();
				if(fansNum==""){
					 alert("请输入平均阅读量");
					 return;
				}
				var topLinePrice = $("#topLinePrice").val();
				if(fansNum==""){
					 alert("请输入头条报价");
					 return;
				}
				var lessLinePrice = $("#lessLinePrice").val();
				if(fansNum==""){
					 alert("请输入次条报价");
					 return;
				}
			}else{
				var groupNum = $("#groupNum").val();
				if(groupNum==''){
					 alert("请输入群人数");
					 return;
				}
			}
			
			var picUrl = $.trim($("#picUrl").val());
			if(picUrl==""){
				alert("头像不能为空！");
				return
			}
			
			var activCase = $("#activCase").val();
			if(activCase=="" || activCase.length<20 || activCase.length>200){
				alert("请输入活动案列,长度20-200");
				return
			}
			
			var aboutMe = $("#aboutMe").val();
			if(aboutMe=="" || aboutMe.length<20 || aboutMe.length>200){
				alert("请输入介绍,长度20-200");
				return;				
			}
			
			var phoneNumber=$("#phoneNumber").val()
   			var re=/^1[3|5|8|7]\d{9}$/;
	   		if(phoneNumber==""){
	   			alert("请输入手机号");
	   			return;
	   		}else if(!re.test(phoneNumber)){
	   			alert("请输入正确的手机号");
	   			return;
	   		}
	   		
	   		var contactUser = $("#contactUser").val();
	   		if(contactUser==""){
	   			alert("请输入联系人");
	   			return;
	   		}
	   		
	   		var email=$("#email").val()
	   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		if(email==""){
	   			alert("请输入邮箱");
	   			return;
		   		
	   		}else if(!regEma.test(email)){
	   			alert("邮箱格式有误");
	   			return
	   		}
	   		
	   		var wxNumber=$("#wxNumber").val()
	   		var regWx=/^[a-zA-Z\d_]{5,}$/;
	   		
	   		if(wxNumber==""){
	   			alert("请输入微信号");
	   			return;
	   		}else if(!regWx.test(wxNumber)){
	   			alert("微信号格式有误");
	   			return
	   		}
	   		
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
			
			
			
			
			var obj=document.getElementsByName('activtyUrlV'); //活动图片
			   //取到对象数组后，来循环检测它是不是被选中
			   var s='';
			   for(var i=0; i<obj.length; i++){
			     s+=obj[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(s==''){
				s="";
			   }else{
				   s=s.substring(0, s.length-1);   
			   }
				$("#activtyUrl").val(s);
				var acUrlLength=$("#ul2").children("li").length;
				
				 if(s=='' || acUrlLength<2 || acUrlLength>9){
					   alert("活动图片不能为空，个数为2-9");
				   		 return;
				   }
			   
			   var objImg=document.getElementsByName('dairyTweetImg');  //日常推文图片
			   //取到对象数组后，来循环检测它是不是被选中
			   var dair='';
			   for(var i=0; i<objImg.length; i++){
				   dair+=objImg[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(dair==''){
				   dair="";
			   }else{
				   dair=dair.substring(0, dair.length-1);   
			   }
			   $("#dairyTweetUrl").val(dair); 
			   var aUrlLength=$("#ul1").children("li").length;
			   if(dair==''){
				   if(zylxVal=='zmt'){
					 alert("日常推文图不能为空");
			   		 return;
				   }else{
					 alert("群成员图不能为空");
				   	 return; 
				   }
			   }
			   if(dair!=""){
				   if(zylxVal=='zmt'){
					   if(aUrlLength<1||aUrlLength>9){
						 alert("日常推文图个数为1-9");
				   		 return;
					   }
				   }else{
					   if(aUrlLength<1||aUrlLength>2){
						 alert("群成员图个数为1-2");
				   		 return;
					   }
				   }
			   }
			   
			$("#addPartForm").submit();
		}
			//上传图片
			function uploadPicSim(){
				//异步上传
				var options = {
						url : "<%=basePath %>bannerInfo/picture.html",
						dataType : "json",
						type : "post",
						success : function(data){
							//回调 路径  data.path
							$("#allUrl").attr("src",data.picUrl);
							$("#keyValue").val(data.key);
							$("#picUrl").val(data.picUrl);
						}
				}
				$("#addPartForm").ajaxSubmit(options);
			}
		
		
			//上传图片
			function uploadPic(obj){
				var ulId="";
				var url="";
				if(obj==1){
					url="dairyTweet";
					name="dairyTweetImg";
					ulId ="ul1";
				}
				else if(obj==2){
					url="activtyUrl";
					name="activtyUrlV"
					ulId ="ul2";
				}
				var picListHtml ="";
				var options = {
						url : '<%=basePath %>enter/'+url+'.html',
						dataType : "json",
						type : "post",
						success : function(data){
							for(var i=0;i<data.length;i++){	
								picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick=deleteDetailImage("'+data[i]+'","'+name+'") class="remove">删除</a></div><div class="img"><img src="'+data[i]+'" width="200" height="200" alt=""></div></li>'+
								'<input type="hidden"  name="'+name+'" value='+data[i]+'>';
							}
							$("#"+ulId).append(picListHtml);
							$("#"+ulId).show();
						}
				}
				$("#addPartForm").ajaxSubmit(options); 
			}
			
			
			function deleteDetailImage(imgUrl){
				var imgLi = $("img[src='"+imgUrl+"']").parent().parent();
				var imgInput = $("input[name=imgUrl][value='"+imgUrl+"']");
				console.log(imgInput.prop("outerHTML"));
				//删除图片，父li移除，删除input
				console.log($("input[name=imgUrl]").size());
				imgLi.remove();
				imgInput.remove();
				alert("删除成功！");
				console.log($("input[name=imgUrl]").size());
				return true;
			}
			
			function deleteDetailImage1(imgUrl){
				var imgLi = $("img[src='"+imgUrl+"']").parent().parent();
				var imgInput = $("input[name=imgUrl][value='"+imgUrl+"']");
				console.log(imgInput.prop("outerHTML"));
				//删除图片，父li移除，删除input
				console.log($("input[name=imgUrl]").size());
				imgLi.remove();
				imgInput.remove();
				alert("删除成功！");
				console.log($("input[name=imgUrl]").size());
				return true;
			}
		
		
		//根据省查出市
		function queryCityByPro(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryCityByPro.html",
				dataType : "json",
				data : {proId:value},
				success : function(data) {
					 $("#cityId option").remove();
					 $("#scholType option").remove();
					 $("#schools option").remove();
					var $sel = $("#cityId");
					if(data.cityList.length>0){
						 $.each(data.cityList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholTypeByCity(data.cityList[0].value);
					}
				}
			});	
			
		}
		
		//根据市查出院校类别
		 function queryScholTypeByCity(value){
			 $("#scholType option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#scholType").append($option0)
			 
			  $("#schools option").remove();
			  var $option1 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option1)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholTypeByCity.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $sel = $("#scholType");
					if(data.scholTypeList.length>0){
						$.each(data.scholTypeList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholByType(data.scholTypeList[0].value);//根据院校类别查出院校
					}
					
				}
			});	
		}
		//根据院校类别查出院校
		 function queryScholByType(value){
			 $("#schools option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option0)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholByType.html",
				dataType : "json",
				data : {dcdaId:value,cityId:$("#cityIds").val()},
				success : function(data) {
					var $sel = $("#schools");
					if(data.scholList.length>0){
						$.each(data.scholList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		} 
		
		 $(function(){
			 var zylxVal = $("#zylx").val();
			 $(".groupNumUl").eq(1).remove();
			 $(".pubNumIdUl").eq(1).remove();
			 $(".fansNumUl").eq(1).remove();
			 $(".readNumUl").eq(1).remove();
			 $(".topLinePriceUl").eq(1).remove();
			 $(".lessLinePriceUl").eq(1).remove();
			 $(".caseUl").eq(1).remove();
		 if(zylxVal=='zmt'){
		    	$("#gzhName").html("公众号名称:");
		    	$("#pubNumId").removeAttr("disabled",true);
				$("#fansNum").removeAttr("disabled",true);
				$("#readNum").removeAttr("disabled",true);
				$("#topLinePrice").removeAttr("disabled",true);
				$("#lessLinePrice").removeAttr("disabled",true);
				$("#groupNum").attr("disabled",true);
				$("#picDiary").html("日常推文图：");
				$("#weMedio").show();
				$("#parTime").hide();
				$("#headPic").html("公众号头像：")
		    }else{
				$("#pubNumId").attr("disabled",true);
				$("#fansNum").attr("disabled",true);
				$("#readNum").attr("disabled",true);
				$("#topLinePrice").attr("disabled",true);
				$("#lessLinePrice").attr("disabled",true);
				$("#groupNum").removeAttr("disabled");
				$("#headPic").html("社群头像：")
		    	$("#gzhName").html("群名称:");
				$("#groupNum").html("群人数:");
		    	$("#picDiary").html("群成员图：");
		    	
				$("#weMedio").hide();
				$("#parTime").show();
		    }	
		 
		
		})
			
		 function changeContent(){
			 var zylxVal = $("#zylx").val();
			    if(zylxVal=='zmt'){
			    	$("#gzhName").html("公众号名称:");
			    	$("#pubNumId").removeAttr("disabled",true);
					$("#fansNum").removeAttr("disabled",true);
					$("#readNum").removeAttr("disabled",true);
					$("#topLinePrice").removeAttr("disabled",true);
					$("#lessLinePrice").removeAttr("disabled",true);
					$("#groupNum").attr("disabled",true);
					$("#picDiary").html("日常推文图：");
					$("#weMedio").show();
					$("#parTime").hide();
					$("#headPic").html("公众号头像：")
			    }else{
					$("#pubNumId").attr("disabled",true);
					$("#fansNum").attr("disabled",true);
					$("#readNum").attr("disabled",true);
					$("#topLinePrice").attr("disabled",true);
					$("#lessLinePrice").attr("disabled",true);
					$("#groupNum").removeAttr("disabled");
					$("#headPic").html("社群头像：")
			    	$("#gzhName").html("群名称:");
			    	$("#picDiary").html("群成员图：");
			    	
					$("#weMedio").hide();
					$("#parTime").show();
			    }
			}
</script>
</div>

