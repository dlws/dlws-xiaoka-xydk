<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>基本信管理> <b>基本信添加</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>userBasicinfo/addSettleUser.html" id="addCorpraForm" method="post" enctype="multipart/form-data">
					<div class="form-item">
							<div class="form-name">选择省:</div>
								<div class="form-input">
									<select class="input" name="proId" id="proId" onchange="queryCityByPro(this.value);">
										<option value="">请选择</option>
										<c:forEach items="${provnceList }" var="provc">
												<option value="${provc.province}">${provc.province }</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						 <div class="form-item">
							<div class="form-name">选择城市:</div>
								<div class="form-input">
									<select class="input" name="cityId" id="cityId" onchange="queryScholTypeByCity(this.value);">
										<option value="">请选择</option>
										<c:forEach items="${cityList }" var="city">
											<c:if test="${city.id == objMap.citycode }">
												<option value="${city.id}" selected = "selected">${city.cityname }</option>
											</c:if>
											<c:if test="${city.id != objMap.citycode }">
												<option value="${city.id}">${city.cityname }</option>
											</c:if>
										</c:forEach>
									</select> 
								</div>
						</div> 
						
						<div class="form-item">
							<div class="form-name">选择院校类别:</div>
								<div class="form-input">
									<select class="input" name="scholType" id="scholType" onchange="queryScholByType(this.value);">
										<!-- <option value="">全部</option> -->
										<c:forEach items="${scholTypeList }" var="stp">
												<option value="${stp.value}">${stp.text}</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						
						<div class="form-item">
								<div class="form-name">学校:</div>
								<div class="form-input">
									<select name="schoolId" class="input" id="schools">
										 <option value="${objMap.schoolId}">${objMap.schoolName }</option>
									</select>
								</div>
						</div>
					
						<div class="form-item">
							<div class="form-name">入驻名称:</div>
							<div class="form-input">
								<input name="userName" type="text" class="input" id="userName" value="" maxlength="10">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						
						<div class="form-item form-item01">
							<div class="form-name">头像:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value="${key }"/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${picUrl }"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl"  src="${picUrl}" width="200" height="200">
    							</div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">活动案例:</div>
							<div class="form-input">
								<input name="activCase" id="activCase" type="text" class="input" value="">
							</div>
						</div>
						
						<div class="form-item">
							<c:choose>
								<c:when test="${enterTypeName=='jndr'}">
									<div class="form-name">个人介绍:</div>
								</c:when>
								<c:otherwise>
									<div class="form-name">社团介绍:</div>
								</c:otherwise>
							</c:choose>
							<div class="form-input">
								<input name="aboutMe" id="aboutMe" type="text" class="input" value="${user.aboutMe }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系电话:</div>
							<div class="form-input">
								<input name="phoneNumber" id="phoneNumber" type="text" class="input" value="${user.phoneNumber }"  placeholder="请输入11位电话号码">
								<div class="clearfix"></div>
								<div class="form-error red"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系人:</div>
							<div class="form-input">
								<input name="contactUser" id="contactUser" type="text" class="input" value="" maxlength="10">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系邮箱:</div>
							<div class="form-input">
								<input name="email" id="email" type="text" class="input" value="${user.email }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">微信号:</div>
							<div class="form-input">
								<input name="wxNumber" id="wxNumber" type="text" class="input" value="${user.wxNumber }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">访问量:</div>
							<div class="form-input">
								<input name="viewNum" id="viewNum" type="text" class="input">
							</div>
						</div>
						<input type="hidden" name="cityIds" id="cityIds" value="">
						<input name="enterType" type="hidden" class="input" id="enterType" value="${enterTypeName}">
						<input name="activtyUrl" type="hidden" class="input" id="activtyUrl" value="">
						<input name="enterId" type="hidden" class="input" id="enterId" value="${paramsMap.enterId }">
						</form>
						
						<form id="detailPicForm" onsubmit="return false;">
						<div class="form-item form-up">
							<div class="form-name">活动图片：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="imgs" id="morePic" multiple size="80" class="btn-blue" /> 
									<!-- <a href="javascript:void(0);" class="btn-blue">选择图片
									</a> -->
								</div>
								<ul id="pics" class="up-list hide">
									
								</ul>
								<div class="btns">
									<button class="btn-blue" onclick="uploadDetailImage()">开始上传</button>
								</div>
							</div>
						</div>
					</form>
						<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="addCorprat()">
						</div>
						
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
	<script>
		function addCorprat(){
			var schools = $("#schools").val();
			if(schools==""||schools==0){
				alert("请选择学校！");
				return
			}
			
			var userName = $("#userName").val();
			if(userName==""){
				alert("请填写入驻名称");
				return
			}
			var picUrl = $.trim($("#picUrl").val());
			if(picUrl==""){
				alert("入驻头像不能为空！");
				return
			}
			
			var activCase = $("#activCase").val();
			if(activCase=="" || activCase.length<20 || activCase.length>200){
				alert("请输入活动案列,长度20-200");
				return
			}
			
			var aboutMe = $("#aboutMe").val();
			if(aboutMe=="" || aboutMe.length<20 || aboutMe.length>200){
				alert("请输入介绍,长度20-200");
				return;				
			}
			
			var phoneNumber=$("#phoneNumber").val()
   			var re=/^1[3|5|8|7]\d{9}$/;
	   		if(phoneNumber==""){
	   			alert("请输入手机号");
	   			return;
	   		}else if(!re.test(phoneNumber)){
	   			alert("请输入正确的手机号");
	   			return;
	   		}
	   		
	   		var contactUser = $("#contactUser").val();
	   		if(contactUser==""){
	   			alert("请输入联系人");
	   			return;
	   		}
	   		
	   		var email=$("#email").val()
	   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		if(email==""){
	   			alert("请输入邮箱");
	   			return;
		   		
	   		}else if(!regEma.test(email)){
	   			alert("邮箱格式有误");
	   			return
	   		}
	   		
	   		var wxNumber=$("#wxNumber").val()
	   		var regWx=/^[a-zA-Z\d_]{5,}$/;
	   		
	   		if(wxNumber==""){
	   			alert("请输入微信号");
	   			return;
	   		}else if(!regWx.test(wxNumber)){
	   			alert("微信号格式有误");
	   			return
	   		}
	   		
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
			
			var obj=document.getElementsByName('activUrl');  //选择所有name="area"的对象，返回数组
			   //取到对象数组后，来循环检测它是不是被选中
			   var s='';
			   for(var i=0; i<obj.length; i++){
			     s+=obj[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(s==''){
				s="";
			   }else{
				   s=s.substring(0, s.length-1);   
			   }
			   $("#activtyUrl").val(s);
			   
			   var activtyUrlLength=$("#pics").children("li").length;
			   if(s=="" || activtyUrlLength<2 ||activtyUrlLength >9){
					alert("活动图片不能为空,个数为2-9");
					return
				}
			$("#addCorpraForm").submit();
		}
		//上传图片
		function uploadPic(){
			//异步上传
			var options = {
					url : "<%=basePath %>bannerInfo/picture.html",
					dataType : "json",
					type : "post",
					success : function(data){
						$("#allUrl").attr("src",data.picUrl);
						$("#keyValue").val(data.key);
						$("#picUrl").val(data.picUrl);
					}
			}
			$("#addCorpraForm").ajaxSubmit(options);
		}
		
		
		function uploadDetailImage(){
			var formData2 = new FormData($("#detailPicForm")[0]);
			$.ajax({
				type : "post",
				url : "<%=basePath %>skill/uploadImage.html",
				dataType : "json",
				data : formData2,
				async : false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data) {
					if(data.ajax_status == "ajax_status_success"){
						var picListHtml = '';
						var pics = jQuery.parseJSON(data.pics);
						for (var i = 0; i < pics.length; i++) {
							var pic = pics[i];
							picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick="deleteDetailImage(&quot;'+pic.imgUrl+'&quot;, &quot;'+pic.targetPath+'&quot;)" class="remove">删除</a></div><div class="img"><img src="'+pic.targetPath+'" style="width:100;height:100" alt=""></div></li>';
							$("#addCorpraForm").append("<input type='hidden' name='activUrl' value='"+pic.imgUrl+"'>");
						}
						$("#pics").append(picListHtml);
						$("#pics").show();
					}else{
						alert(data.msg);
					}
					return false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
			          alert("出错了！");
		        }
			});
			return false;;
		}
		
		function deleteDetailImage(imgUrl,targetPath){
			var imgLi = $("img[src='"+targetPath+"']").parent().parent();
			var imgInput = $("input[name=imgUrl][value='"+imgUrl+"']");
			console.log(imgInput.prop("outerHTML"));
			//删除图片，父li移除，删除input
			console.log($("input[name=imgUrl]").size());
			imgLi.remove();
			imgInput.remove();
			alert("删除成功！");
			console.log($("input[name=imgUrl]").size());
			
			/* $.ajax({
				type : "post",
				url : "deleteDetailImage.html",
				dataType : "json",
				data : {targetPath:targetPath},
				async : false,
				success : function(data) {
					
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
		          alert("出错了！");
		       }
			}); */
			return true;
		}
		
		function uploadThumbnailImage(){
			var formData3 = new FormData($("#thumbnailPicForm")[0]);
			$.ajax({
				type : "post",
				url : "${path}/platgoods/uploadFirstImage1.html",
				dataType : "json",
				data : formData3,
				async : false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data) {
					if(data.ajax_status == "ajax_status_success"){
						$("#thumbnailPic").find("img").attr("src",data.targetPath);
						$("#thumbnailUrl").val(data.picUrl);
						$("#thumbnailPic").show();
					}else{
						alert(data.msg);
					}
					return false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
		          alert("出错了！");
		       }
			});
			return false;
		}
		
		//根据省查出市
		function queryCityByPro(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryCityByPro.html",
				dataType : "json",
				data : {proId:value},
				success : function(data) {
					 $("#cityId option").remove();
					 $("#scholType option").remove();
					 $("#schools option").remove();
					var $sel = $("#cityId");
					if(data.cityList.length>0){
						 $.each(data.cityList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholTypeByCity(data.cityList[0].value);
					}
				}
			});	
			
		}
		
		//根据市查出院校类别
		 function queryScholTypeByCity(value){
			 $("#scholType option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#scholType").append($option0)
			 
			  $("#schools option").remove();
			  var $option1 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option1)
			$("#cityIds").val(value);
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholTypeByCity.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $sel = $("#scholType");
					if(data.scholTypeList.length>0){
						$.each(data.scholTypeList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholByType(data.scholTypeList[0].value);//根据院校类别查出院校
					}
					
				}
			});	
		}
		//根据院校类别查出院校
		 function queryScholByType(value){
			 $("#schools option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option0)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholByType.html",
				dataType : "json",
				data : {dcdaId:value,cityId:$("#cityIds").val()},
				success : function(data) {
					var $sel = $("#schools");
					if(data.scholList.length>0){
						$.each(data.scholList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		}  
</script>
</div>
