<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>修改income</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>专题管理 > <b>修改专题</b></h2>
				</div>
				<div class="form">
					<form id="updateincome" action="${path}/special/updateSpecial.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
								<div class="form-item">
									<div class="form-name">标题：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="title" name="title" value="${obj.title} " >
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
								<div class="form-item">
									<div class="form-name">内容：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="content" name="content" value="${obj.content} " >
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
								
								<div class="form-item form-item01">
									<div class="form-name">上传图片:</div>
									<div class="form-input">
										<button class="btn-blue" style="margin-left:0;">上传图片</button>
										<input type="hidden" name='keyValue' id='keyValue' class='input' value="${obj.keyValue }"/> 
										<input type="hidden" name='imgerUrl' id='imgerUrl' class='input' value="${obj.imgerUrl }"/>
		    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
		    							<div class="clearfix"></div>
		    							<div class="form-img">
		    								<img id="allUrl" src="${obj.imgerUrl}">
		    							</div>
									</div>
								</div>
								<div class="form-item">
									<div class="form-name">图片链接：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="jumpLink" name="jumpLink" value="${obj.jumpLink }" placeholder="请输入图片链接">
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
								
								<div class="form-item">
									<div class="form-name">显示顺序：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="ord" name="ord" value="${obj.ord} " >
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
								<div class="form-item">
									<div class="form-name">创建时间：</div>
									<div class="form-input">
										<input type="text"  class="input" name="createDate" id="createDate" onClick="WdatePicker({dateFmt: 'yyyy-MM-dd HH:mm:ss'})" value="${obj.createDate}" value="<fmt:formatDate value='${obj.createDate}' type='both' />" />
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
								<div class="form-item">
									<div class="form-name">是否显示：</div>
									<div class="form-input">
									<select class="input" name="isDisplay" id="isDisplay" >
										<c:if test="${obj.isDisplay==0 }">
											<option value="0" selected="selected">否</option>
										</c:if>
										<c:if test="${obj.isDisplay!=0 }">
											<option value="0">否</option>
										</c:if>
										<c:if test="${obj.isDisplay==1 }">
											<option value="1" selected="selected">是</option>
										</c:if>
										<c:if test="${obj.isDisplay!=1 }">
											<option value="1">是</option>
										</c:if>
									</select> 
									</div>
								</div>
								
					<button type="submit" class="btn w144" onclick="updateSpecial()">保  存</button>
				</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">

//submit
function updateSpecial(){
	var flag = true;
	var title = $.trim($("#title").val());//标题
	var content = $.trim($("#content").val());//内容
	var ord = $.trim($("#ord").val());//内容
	var imgerUrl = $("#imgerUrl").val();//图片
	var jumpLink = $("#jumpLink").val();//图片链接
	var createDate = $("#createDate").val();//创建时间
	if(title==""){
		alert("标题不能为空!");
		flag = false;
	}
	if(title.length>20){
		alert("标题名称长度最多为20个字符!");
		flag = false;
	}
	if(content==""){
		alert("内容不能为空!");
		flag = false;
	}
	if(imgerUrl==""){
		alert("请选择图片!");
		flag = false;
	}
	if(jumpLink==""){
		alert("请输入图片链接!");
		flag = false;
	}
	if(ord==""){
		alert("请输入图片显示排序!");
		flag = false;
	}
	if(createDate==""){
		alert("请选择创建时间!");
		flag = false;
	}
	if(flag){
		$("#updateincome").submit();
	}
	
}


//上传图片
function uploadPic(){
	//异步上传
	var options = {
			url : "${path}/special/picture.html",
			dataType : "json",
			type : "post",
			success : function(data){
				//回调 路径  data.path
				$("#allUrl").attr("src",data.imgerUrl);
				$("#keyValue").val(data.key);
				$("#imgerUrl").val(data.imgerUrl);
			}
	}
	$("#updateincome").ajaxSubmit(options);
}
</script>
