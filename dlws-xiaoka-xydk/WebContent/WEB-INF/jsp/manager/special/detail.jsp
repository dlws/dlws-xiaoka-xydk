<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>修改income</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>专题管理 > <b>查看专题详情</b></h2>
				</div>
				<div class="form">
					<form id="updateincome" action="/special/updateSpecial.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
						
						
						<div class="form-item">
							<div class="form-name">标题：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="title" name="title" value="${obj.title} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">内容：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="content" name="content" value="${obj.content} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">图片：</div>
							<div class="form-input">
								<img id="imgerUrl" name="imgerUrl" style="margin-top:20px;width:300px;height:200px;" alt="" src="${obj.imgerUrl}" readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">显示顺序：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="ord" name="ord" value="${obj.ord} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">创建时间：</div>
							<div class="form-input">
								<input type="text"  class="input" name="createDate" id="createDate"  value="${obj.createDate} " readonly="readonly"/>
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">是否显示：</div>
							<div class="form-input">
								<select class="input" name="isDisplay" id="isDisplay" disabled="disabled">
									<c:if test="${obj.isDisplay==0 }">
										<option value="0" selected="selected">否</option>
									</c:if>
									<c:if test="${obj.isDisplay!=0 }">
										<option value="0">否</option>
									</c:if>
									<c:if test="${obj.isDisplay==1 }">
										<option value="1" selected="selected">是</option>
									</c:if>
									<c:if test="${obj.isDisplay!=1 }">
										<option value="1">是</option>
									</c:if>
								</select> 
							</div>
						</div>
						
				</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
//上传图片
function uploadPic(){
	//异步上传
	var options = {
			url : "${path}/special/uploadImg.html",
			dataType : "json",
			type : "post",
			success : function(data){
				//回调 路径  data.path
				$("#allUrl").attr("src",data.url);
				$("#path").val(data.path);
			}
	}
	$("#income").ajaxSubmit(options);
}
</script>