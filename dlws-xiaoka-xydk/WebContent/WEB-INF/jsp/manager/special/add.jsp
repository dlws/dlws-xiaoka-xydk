<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>添加income</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
	
</head>
<script type="text/javascript"></script>
<script >
$(function(){
	
	$("#income").submit(function(){
		var isSubmit = false;
		//分两步：
		//第一步做必填字段的校验
		$(this).find("[reg2]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
				isSubmit = false;
				//在jQuery跳出循环不再是break;也部署return;是return false;
				return false;//跳出循环
			}else{
				var inputName = $(this).attr("name");
				if(inputName == "brandName"){
					if(validBrandName(val)){
						$(this).parents(".form-input").find(".form-error").html("<font color='red'>品牌名称已存在</font>");
						isSubmit = false;
						return false;//跳出循环
					}else{
						$(this).parents(".form-input").find(".form-error").html("");
					}
				}else{
					$(this).parents(".form-input").find(".form-error").html("");
				}
			}
		});
		
		$("#income").find("[reg2]").blur(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
			}else{
				$(this).parents(".form-input").find(".form-error").html("");
			}
		});
		
		var imgerUrl = $("#imgerUrl").val();//图片
		if(imgerUrl==""){
			alert("图片不能为空!")
			isSubmit = false;
		}
		return isSubmit;
	}); 
	
})
</script>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>专题管理 > <b>添加专题</b></h2>
				</div>
				<div class="form">
					<form id="income" name="incomeform" action="${path}/special/addSpecial.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
						
								<div class="form-item">
									<div class="form-name">标题：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="title" name="title" value="" placeholder="请输入标题" reg2="^[a-zA-Z0-9\_\u4e00-\u9fa5]{1,20}$" tip="标题为中英文或数字字符和下划线，长度1-20">
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">内容：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="content" name="content" value="" placeholder="请输入内容" reg2="^[a-zA-Z0-9\_\u4e00-\u9fa5]{1,200}$" tip="内容不能为空">
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
								
								<div class="form-item form-item01">
									<div class="form-name">上传图片:</div>
									<div class="form-input">
										<button class="btn-blue" style="margin-left: 0;">上传图片</button>
										<input type="hidden" name='keyValue' id='keyValue' class='input' value="${key }" /> 
										<input type="hidden" name='imgerUrl' id='imgerUrl' class='input' value="${imgerUrl }" /> 
										<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()" />
										<div class="clearfix"></div>
										<div class="form-img">
											<img id="allUrl" src="${imgerUrl}">
										</div>
										<div class="form-error red"></div>
									</div>
								</div>
								<div class="form-item">
									<div class="form-name">图片链接：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="jumpLink" name="jumpLink" value="" placeholder="请输入图片链接(http://xxx)" reg2="^[a-zA-Z0-9\_\u4e00-\u9fa5]{1,500}$" tip="图片链接不能为空" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>

								<div class="form-item">
									<div class="form-name">显示顺序：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="ord" name="ord" value="" placeholder="请输入显示顺序" reg2="^[a-zA-Z0-9\_\u4e00-\u9fa5]{1,500}$" tip="显示顺序不能为空" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
								
								<div class="form-item">
									<div class="form-name">创建时间：</div>
									<div class="form-input">
										<input type="text"    class="input" name="createDate" id="createDate" onClick="WdatePicker({dateFmt: 'yyyy-MM-dd HH:mm:ss'})"  placeholder="请选择创建时间" reg2="^[a-zA-Z0-9\_\:\-\ \u4e00-\u9fa5]{1,500}$" tip="创建时间不能为空"/>
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">是否显示：</div>
									<div class="form-input">
										<select class="input" name="isDisplay" id="isDisplay" >
											<option value="0" selected="selected">否</option>
											<option value="1" selected="selected">是</option>
										</select> 
									</div>
								</div>
						<!-- <button type="button" class="btn w144" onclick="addSpecial()">保  存</button> -->
						<input type="submit" class="btn w144" value="保  存">
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">

	//提交
	/* function addSpecial(){
		var flag = true;
		var title = $.trim($("#title").val());//标题
		var content = $.trim($("#content").val());//内容
		var ord = $.trim($("#ord").val());//内容
		var imgerUrl = $("#imgerUrl").val();//图片
		var jumpLink = $("#jumpLink").val();//图片链接
		var createDate = $("#createDate").val();//创建时间
		if(title==""){
			alert("标题不能为空!");
			return;
		}
		if(title.length>20){
			alert("标题名称长度最多为20个字符!");
			return;
		}
		if(content==""){
			alert("内容不能为空!");
			return;
		}
		if(imgerUrl==""){
			alert("请选择图片!");
			return;jumpLink
		}
		if(jumpLink==""){
			alert("请输入图片链接!");
			return;
		}
		if(ord==""){
			alert("请输入图片显示排序!");
			return;
		}
		if(createDate==""){
			alert("请选择创建时间!");
			return;
		}
		if(flag){
			$("#income").submit();
		} 
	} */


//上传图片
function uploadPic(){
	//异步上传
	var options = {
			url : "${path}/special/picture.html",
			dataType : "json",
			type : "post",
			success : function(data){
				//回调 路径  data.path
				$("#allUrl").attr("src",data.imgerUrl);
				$("#keyValue").val(data.key);
				$("#imgerUrl").val(data.imgerUrl);
				
			}
	}
	$("#income").ajaxSubmit(options);

}

</script>
