
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
       function goSearch(){
       $("#form1").submit();
       }
</script>
	<div class="column-name">
		<h2>
			专题管理 > <b>专题列表</b>
		</h2>
	</div>
<form id="form1" name="form1" action="${path}/special/specialList.html" method="post">

	<div class="main-top">
		<div class="fl">
			<a href="${path}/special/toAddSpecial.html" class="btn btn-p">新建专题</a>
		</div>
		<div class="fr" align="center">
			<span class="c-name">专题名称</span>
			<input type="text" name="title" class="input mr10" value="${paramsMap.title }" placeholder="请输入专题关键字"> 
			<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
						    <th>标题</th>
						    <th>内容</th>
						    <th>图片</th>
						    <th>图片链接</th>
						    <th>显示顺序(数字越小越靠前)</th>
						    <th>是否显示</th>
						    <th>创建时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="data">
				<tr>
							<td>${data.title}</td>
							<td>${data.content}</td>
							<td><img style="width:300px;height:200px;" alt="" src="${data.imgerUrl}">
							</td>
							<td>${data.jumpLink}</td>
							<td>${data.ord}</td>
							<td>
							<c:if test="${data.isDisplay ==0}">否</c:if>
							<c:if test="${data.isDisplay ==1}">是</c:if></td>
							
							<td><fmt:formatDate  value="${data.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				<td>
					<!-- 是否删除判断 -->
					<a href="javascript:;" data-id="${path}/special/delSpecial.html?id=${data.id}" class="remove">删除</a> 
					<!-- 修改--> 
					<a class="" href="${path}/special/toUpdateSpecial.html?id=${data.id}">修改</a>
					<!-- 详情--> 
					<a class="" href="${path}/special/detailSpecial.html?id=${data.id}">查看详情</a>
				</td>
				</tr>	
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a>
					<a href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
