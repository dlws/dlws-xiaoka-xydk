<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/base.css">
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/style.css">
<script src="<%=basePath%>/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="<%=basePath%>/v2/console/js/globle.js"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<div class="column-name">
	<h2>
		记录管理><b>记录管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="<%=basePath%>/dividedraw/divideDrawList.html" method="post">
	<div class="main-top">
		<div class="fr" align="center">
			<span class="c-name">用户名</span> <input type="text" name="name"
				class="input mr10" value="${paramsMap.name }" placeholder="请输入用户名">
			<span class="c-name">审核状态</span>
			<select name="state" class="input">
				<option value="">全部</option>
				<c:choose>
					<c:when test="${paramsMap.state == '0' }">
						<option value="0" selected="selected">分成</option>
					</c:when>
					<c:otherwise>
						<option value="0">分成</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${paramsMap.state == '1' }">
						<option value="1" selected="selected">提现</option>
					</c:when>
					<c:otherwise>
						<option value="1">提现</option>
					</c:otherwise>
				</c:choose>
			</select> 
			<span class="c-name">时间：</span> 
				<input type="text" value="${paramsMap.beginDate}" class="input"
				name="beginDate" id="begindate"
				onClick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')||\'%y-%M-%d\'}'})"
				readonly="readonly" />--
				<input type="text" value="${paramsMap.endDate}" class="input"
				name="endDate" id="enddate"
				onClick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\')}',maxDate:'%y-%M-%d'})"
				readonly="readonly" />
			<input type="button" class="btn w96" value="查  询"
				onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>用户</th>
					<th>金额</th>
					<th>账户余额</th>
					<th>状态</th>
					<th>时间</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="dividedraw">
					<tr>
						<c:choose>
							<c:when test="${refunds.name !=null}">
								<td>${refunds.name }</td>
							</c:when>
							<c:otherwise>
								<td>匿名</td>
							</c:otherwise>
						</c:choose>
						<td>${dividedraw.divideOrWithdraw }</td>
						<td>${dividedraw.balanceMoney }</td>
						<c:choose>
							<c:when test="${dividedraw.state == 0 }">
								<td>分成</td>
							</c:when>
							<c:otherwise>
								<td>提现</td>
							</c:otherwise>
						</c:choose>
						<td><fmt:formatDate
								value="${dividedraw.divideOrWithdrawTime }" type="both"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
	<div class="popBox chaLianjie-pop">
		<div class="popBox-back"></div>
		<div class="popBox-box">
			<div class="popBox-con">
				<div class="chaLianjie-con">
					<div class="tit" id="lookurl"></div>
					<a href="javascript:;" class="btn w144">确 认</a>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	
</script>

