<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/js/jquery.form.js"></script>
<script src="http://api.map.baidu.com/api?v=1.3"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<div class="column-name">
	<h2>
		商家分类 <b>修改分类</b>
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/publishtype/update.html" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="${map.id }"/>
		<div class="form-item">
			<div class="form-name">分类名称</div>
			<div class="form-input">
				<input type="text" class="input" id="className" value="${map.className }" name="className">
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">数据值</div>
			<div class="form-input">
				<input type="text" class="input" id="classValue" value="${map.classValue }" name="classValue">
			</div>
		</div>
		<div class="form-item form-item01">
			<div class="form-name">图片</div>
			<div class="form-input">
				<button class="btn-blue" style="margin-left:0;">上传图片</button>
				<input type="hidden" name='imageURL' id='path' value="${map.imageURL }"  class='input'/> 
				<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
				<div class="form-img">
					<img id="allUrl" src="${map.imageURL }">
				</div>
			</div>
		</div>
		<div class="form-item form-item01" id="aa">
			<div class="form-name">发布选中图片</div>
			<div class="form-input">
				<button class="btn-blue" style="margin-left:0;">上传图片</button>
				<input type="hidden" name='selectimage' id='selectimagePath' value="${map.selectimage }" class='input'/> 
				<input type="file" name="picselect" class="file input" onchange="selectPic()"/>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
				<div class="form-img">
					<img id="selectimageId" src="${map.selectimage }">
				</div>
			</div>
		</div> 
		<div class="form-item form-item01">
			<div class="form-name">发布未选中图片</div>
			<div class="form-input">
				<button class="btn-blue" style="margin-left:0;">上传图片</button>
				<input type="hidden" name='noSelectimage' id='noSelectimagePath' value="${map.noSelectimage }" class='input'/> 
				<input type="file" name="picnoselect" class="file input" onchange="noSelectPic()"/>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
				<div class="form-img">
					<img id="noSelectimageId" src="${map.noSelectimage }">
				</div>
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">序号</div>
			<div class="form-input">
				<input type="text" min="0" class="input" id="ord" value="${map.ord }" name="ord">
			</div>
		</div>	
		<div class="form-item">
			<div class="form-name">状态</div>
			<div class="form-input">
				<select class="input" id="status" name="status">
					<c:choose>
						<c:when test="${map.status == 0 }">
							<option value="0" selected="selected">不发布</option>
						</c:when>
						<c:otherwise>
							<option value="0" >不发布</option>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${map.status == 1 }">
							<option value="1" selected="selected">已发布</option>
						</c:when>
						<c:otherwise>
							<option value="1" >已发布</option>
						</c:otherwise>
					</c:choose>
				</select> 
			</div>
		</div>
		<div style="text-align:center; vertical-align:middel;">
			<input type="button" class="btn w144" value="保  存" onclick="save();">
        	<input type="button" class="btn w144" value="返  回" onclick="javascript:history.back(-1);">
		</div>
	</form>
</div>
<script type="text/javascript">
	//上传图片
	function uploadPic(){
		//异步上传
		var options = {
				url : "${path}/publishtype/uploadImg.html",
				dataType : "json",
				type : "post",
				success : function(data){
					$("#allUrl").attr("src",data.url);
					$("#path").val(data.path);
				}
		}
		$("#form111").ajaxSubmit(options);
	}
	function selectPic(){
		//异步上传
		var options = {
				url : "${path}/publishtype/uploadImg2.html",
				dataType : "json",
				type : "post",
				success : function(data){
					$("#selectimageId").attr("src",data.selecturl);
					$("#selectimagePath").val(data.selectpath);
				}
		}
		$("#form111").ajaxSubmit(options);
	}
	//未选中上传图片
	function noSelectPic(){
		//异步上传
		var options = {
				url : "${path}/publishtype/uploadImg3.html",
				dataType : "json",
				type : "post",
				success : function(data){
					$("#noSelectimageId").attr("src",data.noselecturl);
					$("#noSelectimagePath").val(data.noselectpath);
				}
		}
		$("#form111").ajaxSubmit(options);
	}
	function save(){
		var className = $("#className").val().trim();
		var classValue = $("#classValue").val().trim()
		var path = $("#path").val().trim();
		var ord = $("#ord").val();
		var selectimagePath = $("#selectimagePath").val();
		var noSelectimagePath = $("#noSelectimagePath").val();
		if(className == ""){
			alert("请输入名称!");
			return;
		}
		if(classValue == ""){
			alert("请输入值!");
			return;
		}
		if(path == ""){
			alert("请上传图片!");
			return;
		}
		if(selectimagePath == ""){
			alert("请上传发布选中图片!");
			return;
		}
		if(noSelectimagePath == ""){
			alert("请上传发布未选中图片!");
			return;
		}
		if(ord == ""){
			alert("请输入序号!");
			return;
		}
		$("#form111").submit();
	}
</script>
