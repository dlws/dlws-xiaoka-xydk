<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/base.css">
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/style.css">
<script src="<%=basePath%>/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="<%=basePath%>/v2/console/js/globle.js"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<div class="column-name">
	<h2>
		商家分类><b>商家分类</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="<%=basePath%>/publishtype/list.html" method="post">
	<div class="main-top">
		<div class="fl">
			<a href="<%=basePath%>/publishtype/add.html" class="btn btn-p">新建分类</a>
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>名称</th>
					<th>数值</th>
					<th>图标</th>
					<th>发布点击图片</th>
					<th>发布未点击图片</th>
					<th>状态</th>
					<th>序号</th>
					<th>创建时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="publish">
					<tr>
						<td>${publish.className }</td>
						<td>${publish.classValue }</td>
						<td>
							<img style="width:50px;height:30px;" alt="图标" src="${publish.imageURL }">
						</td>
						<td>
							<img style="width:50px;height:30px;" alt="图标" src="${publish.selectimage }">
						</td>
						<td>
							<img style="width:50px;height:30px;" alt="图标" src="${publish.noSelectimage }">
						</td>
						<c:choose>
							<c:when test="${publish.status == 0 }">
								<td>未发布</td>
							</c:when>
							<c:otherwise>
								<td>已发布</td>
							</c:otherwise>
						</c:choose>
						<td>${publish.ord }</td>
						<td><fmt:formatDate value="${publish.createDate }"
								type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td>
							<a class="" href="${path}/publishtype/edit.html?id=${publish.id}">修改</a>
							<a href="javascript:;" data-id="${path}/publishtype/delete.html?id=${publish.id}" class="remove">删除</a> 
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>
<script type="text/javascript">
	
</script>

