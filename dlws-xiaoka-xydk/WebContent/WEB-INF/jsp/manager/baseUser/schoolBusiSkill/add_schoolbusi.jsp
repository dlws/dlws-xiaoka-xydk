<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	<style type="text/css">
		.custom{
			position:relative; left:120px; 
			margin: 1% 1% 0% 0%;
		}
	</style>
	<body>
	<div class="column-name">
		<h2>校内商家入驻> <b>基本信息填写</b></h2>
	</div>
	<div class="form1">
		<form action="<%=basePath%>schoolBusiSkill/addSchoolBusi.html" id="bannerForm" method="post" enctype="multipart/form-data">
			<div class="form-item">
				<div class="form-name">商家名称:</div>
				<div class="form-input">
					<input name="skillName"  id="userName" type="text" class="input" value="${skillInfo.skillName }" maxlength="10">
				</div>
			</div>
			<div class="form-item">
				<div class="form-name">商家类型:</div>
				<div class="form-input">
					<select name="skillSonId" class="input">
								<c:forEach items="${list}" var="businType">
										<option value="${businType.id}">${businType.className}</option>
								</c:forEach>
  					</select>
				</div>
			</div>
			<!-- 此处应循环读取数据库 -->
			<div class="custom">
					<span>合作形式:</span>
								<c:forEach items="${cooperaTypeList}" var="cooperaType" varStatus="bs">
											<!-- 这里减一默认是 -->
											<input onclick="showPrice(${bs.index})" type="checkbox" value="${cooperaType.id}">${cooperaType.dic_name}
								</c:forEach>
					<div class="custom">
							<ul id="cooperaPrice" style="width:350px;">
								<c:forEach items="${cooperaTypeList}" var="cooperaType">
									<li style="display:none;">
									    <input type="hidden" value="${cooperaType.id}" name="cooperaType">
										${cooperaType.dic_name}:
										<select name="cooperaCompany" style="float: right;height: 21px;">
											<c:forEach var="unit" items="${allUnit}">
												<option value="${unit.id}">${unit.dic_name}</option>
											</c:forEach>
										</select> 
										<input placeholder="元/次" type="number" style="float: right" name="cooperaPrice" value="">
									</li>
								</c:forEach>
							</ul>
					</div>
					门店照片(每种类型最少上传两张图片):<br>
					<div class="form-item form-up">
							<div class="form-name">近景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picSmall" id="close" multiple size="80" class="btn-blue" onchange="uploadPic(1)"/> 
								</div>
								<ul id="ul1">
									
								</ul>
							</div>
					</div>
					<div class="form-item form-up">
							<div class="form-name">中景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picMiddle" id="middle" multiple size="80" class="btn-blue" onchange="uploadPic(2)"/> 
								</div>
								<ul id="ul2" >
									
								</ul>
							</div>
					</div>
					<div class="form-item form-up">
							<div class="form-name">全景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picAll" id="all" multiple size="80" class="btn-blue" onchange="uploadPic(3)"/> 
								</div>
								<ul id="ul3">
									
								</ul>
							</div>
					</div>
					<p style=" margin-top:20px;">门店描述：</p>
							<textarea id="aboutShop" style="width:400px;height:200px;margin-left:81px;" name="skillDepict" placeholder="场地描述将在20字以内200字以下"></textarea>
					<br>
				</div>
			<div class="form-item">
				<div class="form-name">访问量:</div>
				<div class="form-input">
					<input name="viewNum" placeholder="请输入初始访问量" id="viewNum" type="number" class="input" >
				</div>
			</div>
			<br/>
			<div style="text-align:center; vertical-align:middel;">
				<input type="button" class="btn w144" value="保    存" onclick="addSchoolBusi()">
			</div>
				<input type="hidden" name="basicId"  value="${basicId}">
				<input type="hidden" value="${list[0].fatherId}" name="skillFatId">
			</form>
			<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
	<script>
		function addSchoolBusi(){
			var userName = $("#userName").val();
			if(userName==""){
				alert("入驻名称不能为空");
				return
			}
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
			var aboutMe = $("#aboutShop").val();
			if(aboutMe==""){
				alert("门店描述不能为空");
				return;				
			}
			if(aboutMe.length<20){
				alert("门店描述不能少于20字");
				return;
			}
			if(aboutMe.length>200){
				alert("门店描述不能大于200字");
				return;
			}
			if($("#ul1 li").length<2||$("#ul2 li").length<2||$("#ul3 li").length<2){
				alert("每种 最少上传两张(近景 中景 远景)");
				return
			}
			if($("#ul1 li").length>9||$("#ul2 li").length>9||$("#ul3 li").length>9){
				alert("每种 最多上传九张(近景 中景 远景)");
				return
			}
			$("#bannerForm").submit();
			
		}
		//上传图片
		function uploadPic(obj){
			var ulId="";
			var url="";
			var name=""
			if(obj==1){
				url="pictureSmall";
				ulId ="ul1";
				name = "closePhoto";
			}
			else if(obj==2){
				url="pictureMiddle";
				ulId ="ul2";
				name = "middlePho";
			}else{
				url="pictureAll";
				ulId ="ul3";
				name = "allPhoto";
			}
			var picListHtml ="";
			var options = {
					url : '<%=basePath %>enter/'+url+'.html',
					dataType : "json",
					type : "post",
					success : function(data){
						for(var i=0;i<data.length;i++){	
							
							picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick=deleteDetailImage("'+data[i]+'","'+name+'") class="remove">删除</a></div><div class="img"><img src="'+data[i]+'" width="200" height="200" alt=""></div></li>'+
							'<input type="hidden"  name="'+name+'" value='+data[i]+'>';
						}
						$("#"+ulId).append(picListHtml);
					}
			}
			$("#bannerForm").ajaxSubmit(options); 
		}
		//根据省查出市
		function queryCityByPro(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryCityByPro.html",
				dataType : "json",
				data : {proId:value},
				success : function(data) {
					 $("#cityId option").remove();
					 $("#scholType option").remove();
					 $("#schools option").remove();
					var $sel = $("#cityId");
					if(data.cityList.length>0){
						 $.each(data.cityList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholTypeByCity(data.cityList[0].value);
					}
				}
			});	
			
		}
		//根据市查出院校类别
		 function queryScholTypeByCity(value){
			 $("#scholType option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#scholType").append($option0)
			 $("#schools option").remove();
			  var $option1 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option1)
			$("#cityIds").val(value);
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholTypeByCity.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $sel = $("#scholType");
					if(data.scholTypeList.length>0){
						$.each(data.scholTypeList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholByType(data.scholTypeList[0].value);//根据院校类别查出院校
					}
					
				}
			});	
		}
		//根据院校类别查出院校
		 function queryScholByType(value){
			 $("#schools option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option0)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholByType.html",
				dataType : "json",
				data : {dcdaId:value,cityId:$("#cityIds").val()},
				success : function(data) {
					var $sel = $("#schools");
					if(data.scholList.length>0){
						$.each(data.scholList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		}  
		//用户上传头像
		function uploadhead(){
			//异步上传
			var options = {
					url : "<%=basePath %>bannerInfo/picture.html",
					dataType : "json",
					type : "post",
					success : function(data){
						$("#allUrl").attr("src",data.picUrl);
						$("#keyValue").val(data.key);
						$("#picUrl").val(data.picUrl);
					}
			}
			$("#bannerForm").ajaxSubmit(options);
		}
		
		//缺少让其隐藏的一段代码
		function showPrice(index){
			//如果当前元素被选中则显示
			if($("input[type='checkbox']").eq(index).is(':checked')){
				$("#cooperaPrice").children("li").eq(index).css({'display':'block'});//显示当前的
			}else{
				$("#cooperaPrice").children("li").eq(index).css({'display':'none'});//未选中则隐藏
				//清空当前文本框中的内容
				$("#cooperaPrice").children("li").eq(index).children("input[name='cooperaPrice']").val("");
			}
			
		}
		//删除图片的方法
		function deleteDetailImage(imgUrl,name){
			var imgLi = $("img[src='"+imgUrl+"']").parent().parent();
			var imgInput = $("input[name='"+name+"'][value='"+imgUrl+"']");
			console.log(imgInput.prop("outerHTML"));
			//删除图片，父li移除，删除input
			console.log($("input[name=imgUrl]").size());
			imgLi.remove();
			imgInput.remove();
			alert("删除成功！");
			console.log($("input[name=imgUrl]").size());
			return true;
		}
		
	</script>
</div>
</body>
