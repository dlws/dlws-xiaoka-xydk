<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
    <title>校咖微平台后台管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>技能管理 > <b>技能列表</b></h2>
				</div>
				<form id = "form1" name="form1" action = "${pageContext.request.contextPath}/skill/list.html">
				<div class="main-top">
					<div class="fl">
						<a href="${pageContext.request.contextPath}/baseInfoSkill/tochooseType.html?basicId=${basicId}" class="btn btn-p">新建技能</a>
					</div>
				</div>
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<!-- <th width="35"><input type="checkbox" id="selectAll" onclick="selectAll()"> 全选</th> -->
								<th>技能名称</th>
								<!-- <th>技能价格</th>
								<th>价格单位</th> -->
								<th>服务类型</th>
								<th>技能描述</th>
								<th style="width: 100px">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="skill" items="${po.datasource}">
								<tr>
									<%-- <td><input type="checkbox" id="${platgoods.id}"></td> <!-- onclick="selectGoods()" --> --%>
									<td>${skill.skillName}</td>
									<%-- <td>${skill.skillPrice}</td>
									<td>
										<c:if test="${not empty skill.dic_name}">${skill.dic_name}</c:if>
										<c:if test="${empty skill.dic_name}">次</c:if>
									</td> --%>
									<td>${skill.className}</td>
									<td style="width: 300px">${skill.skillDepict}</td>
									
									<td>
										<a href="${pageContext.request.contextPath}/baseInfoSkill/toUpdatePushType.html?skillId=${skill.id}&basicId=${basicId}&id=${skill.skillFatId}">修改</a>
										<a href="${pageContext.request.contextPath}/baseInfoSkill/delete.html?id=${skill.id}&basicId=${basicId}" class="">删除</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
					<%-- <div id="page">
						<%@ include file="/v2/console/common/pageView.jsp"%>
					</div> --%>
				</div>
				</form>
			</div>
		</div>
		<div class="popBox modifyGoods-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="modifyGoods-con">
					<form id="excelForm" onsubmit="return false;">
						<div class="tit">
							Excel文件:<input type='text' disabled="disabled" style="width: 300px;" id='textExcel' class='input' placeholder="请您选择Excel文件"/> 
							<input type="file" name="excel" class="file" id="fileExcel" onchange="document.getElementById('textExcel').value=this.value" />
						</div>
						<div class="btns">
							<button class="btn w144" onclick="uploadExcel()">确  认</button>
							<!-- <a href="javascript:;" class="btn w144">确  认</a> -->
							<a href="javascript:;" class="btn-gray w144">取  消</a>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div><!-- modifyGoods-pop -->
		<div class="popBox chaLianjie-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="chaLianjie-con">
						<div class="tit" id="msg"></div>
						<a href="javascript:;" class="btn w144" id="okButton">确  认</a>
					</div>
				</div>
			</div>
		</div><!-- chaLianjie-pop -->
		<div class="popBox remove-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="remove-con">
						<div class="tit">你确定要删除吗？</div>
						<div class="btns">
							<a href="javascript:;" class="btn w144">删  除</a>
							<a href="javascript:;" class="btn-gray w144">取  消</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			remove();// 删除弹出框
			quxiao();// 弹出框
			selectGoods()
		});
		
		function ss(){
			alert("该功能尚未完善，如要修改，请删除后重新添加！！");
		}
		
		//全选框
		function selectAll(){
			var isChecked = $("#selectAll")[0].checked;
			$(".table").find("tbody").find("input[type='checkbox']").each(function(){{
					this.checked = isChecked;
				/* if(this.checked){
					this.checked = false;
				}else{
					this.checked = true;
				} */
			}});
		}
		
		//选择框
		function selectGoods(){
			$(".table").find("tbody").find("input[type='checkbox']").click(function(){
				if(this.checked){
					if($(".table").find("tbody").find('input:checked').size() == $(".table").find("tbody").find("input[type='checkbox']").size()){
						$("#selectAll")[0].checked = true;
					}
				}else{
					$("#selectAll")[0].checked = false;
				}
			});
		}
		
		//批量删除
		function deleteMore(){
			if($(".table").find("tbody").find("input:checked").size() > 0){
				var goodsIds = '';
				$(".table").find("tbody").find("input:checked").each(function(){
					 goodsIds += this.id+","
				});
				goodsIds = goodsIds.substring(0,goodsIds.length-1);
				window.location.href = "${pageContext.request.contextPath}/platgoods/delete.html?platGoodsId="+goodsIds;
			}else{
				alert("您没有选择商品！");
			}
			
		}
		
		// 弹出框
		function quxiao(){
			var $btn = $('.popBox').find('.btn-gray');
			$btn.click(function() {
				$('.popBox').hide();
			});
		}
		// 删除
		function remove(){
			$('.remove').click(function() {
				$('.remove-pop').show();
				$('.remove-pop .btn').attr({'href': '${pageContext.request.contextPath}/platgoods/delete.html?platGoodsId='+this.id });
			});	
		}
		
		//弹出导入框
		function importPop(){
			$('.modifyGoods-pop').show();
		}
		
		//导入excel数据
		function uploadExcel(){
			var formData = new FormData($("#excelForm")[0]);
			$.ajax({
				type : "post",
				url : "importByExcel.html",
				dataType : "json",
				data : formData,
				async : false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data) {
					if(data.flag){
						$("#msg").html("导入成功！");
						$('.chaLianjie-pop').show();
						$("#okButton").attr("onclick","succ()");
					}else{
						$("#msg").html(data.msg);
						$('.chaLianjie-pop').show();
						$("#okButton").attr("onclick","fail()");
					}
					return false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
		          alert("出错了！");
		       }
			});
			return false;;
		}
		
		function succ(){
			$('.chaLianjie-pop').hide();
			$('.modifyGoods-pop').hide();
			window.location.href = "${pageContext.request.contextPath}/platgoods/toPageList.html";
		}
		function fail(){
			$('.chaLianjie-pop').hide();
		}
		
		function loadPlagGoods(){
			$("#pageForm").submit();
		}
	</script>
</body>
</html>
