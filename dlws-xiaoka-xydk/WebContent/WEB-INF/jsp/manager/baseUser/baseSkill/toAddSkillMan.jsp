<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>基本信管理> <b>基本信添加</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>baseInfoSkill/add.html" id="addCorpraForm" method="post" enctype="multipart/form-data">
					<input type="hidden" name="basicId" value="${basicId}">
					<input type="hidden" name="skillFatId" value="${fatherId}">
						<div class="form-item">
						<c:choose>
							<c:when test="${enterTypeName=='jndr'}">
								<div class="form-name">技能名称:</div>
							</c:when>
							<c:otherwise>
								<div class="form-name">服务名称:</div>
						</c:otherwise>
						</c:choose>
							<div class="form-input">
								<input name="skillName" type="text" class="input" id="skillName" value="" maxlength="10">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						
						<div class="form-item">
						<c:choose>
							<c:when test="${enterTypeName=='jndr'}">
								<div class="form-name">技能类型:</div>
							</c:when>
							<c:otherwise>
								<div class="form-name">服务类型:</div>
							</c:otherwise>
						</c:choose>
							<div class="form-input">
								<select class="input" name="skillSonId" id="skillSonId">
									<c:forEach items="${sonList}" var="one">
											<option value="${one.id }">${one.className}</option>
									</c:forEach>
								</select> 
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">服务性质:</div>
							<div class="form-input">
								<select class="input" name="serviceType" id="serviceType">
									<option value="1">线上</option>
									<option value="2">线下</option>
								</select> 
							</div>
						</div>
						
						<div class="form-item">
						<c:choose>
							<c:when test="${enterTypeName=='jndr'}">
								<div class="form-name">技能价格:</div>
							</c:when>
							<c:otherwise>
								<div class="form-name">服务价格:</div>
							</c:otherwise>
						</c:choose>
							<div class="form-input">
								<input name="skillPrice" type="text" class="input" id="" value="" maxlength="8">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">单位:</div>
							<div class="form-input">
								<select class="input" name="company" id="company">
								<c:forEach items="${canpanyTypes}" var="one">
									<option value="${one.id}">${one.dic_name}</option>
								</c:forEach>
								</select> 
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">服务介绍:</div>
							<div class="form-input">
								<input name="skillDepict" id="skillDepict" type="text" class="input" value="">
							</div>
						</div>
						<input type="hidden" name="activtyUrl"  class="input" id="activtyUrl" >
						</form>
						
						<form id="detailPicForm" onsubmit="return false;">
						<div class="form-item form-up">
							<div class="form-name">图片：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="imgs" id="morePic" multiple size="80" class="btn-blue" /> 
								</div>
								<ul id="pics" class="up-list hide">
									
								</ul>
								<div class="btns">
									<button class="btn-blue" onclick="uploadDetailImage()">开始上传</button>
								</div>
							</div>
						</div>
					</form>
						<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="addCorprat()">
						</div>
						
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
	<script>
		function addCorprat(){
			
			 var skillName = $("#skillName").val();
			if(skillName==""){
				alert("请填写技能名称");
				return
			}
			/*var picUrl = $.trim($("#picUrl").val());
			if(picUrl==""){
				alert("入驻头像不能为空！");
				return
			}
			
			var activCase = $("#activCase").val();
			if(activCase=="" || activCase.length<20 || activCase.length>200){
				alert("请输入活动案列,长度20-200");
				return
			}
			
			var aboutMe = $("#aboutMe").val();
			if(aboutMe=="" || aboutMe.length<20 || aboutMe.length>200){
				alert("请输入介绍,长度20-200");
				return;				
			}
			
			var phoneNumber=$("#phoneNumber").val()
   			var re=/^1[3|5|8|7]\d{9}$/;
	   		if(phoneNumber==""){
	   			alert("请输入手机号");
	   			return;
	   		}else if(!re.test(phoneNumber)){
	   			alert("请输入正确的手机号");
	   			return;
	   		}
	   		
	   		var contactUser = $("#contactUser").val();
	   		if(contactUser==""){
	   			alert("请输入联系人");
	   			return;
	   		}
	   		
	   		var email=$("#email").val()
	   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		if(email==""){
	   			alert("请输入邮箱");
	   			return;
		   		
	   		}else if(!regEma.test(email)){
	   			alert("邮箱格式有误");
	   			return
	   		}
	   		
	   		var wxNumber=$("#wxNumber").val()
	   		var regWx=/^[a-zA-Z\d_]{5,}$/;
	   		
	   		if(wxNumber==""){
	   			alert("请输入微信号");
	   			return;
	   		}else if(!regWx.test(wxNumber)){
	   			alert("微信号格式有误");
	   			return
	   		}
	   		
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
			
			var obj=document.getElementsByName('activUrl');  //选择所有name="area"的对象，返回数组
			   //取到对象数组后，来循环检测它是不是被选中
			   var s='';
			   for(var i=0; i<obj.length; i++){
			     s+=obj[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(s==''){
				s="";
			   }else{
				   s=s.substring(0, s.length-1);   
			   }
			   $("#activtyUrl").val(s);
			   
			   var activtyUrlLength=$("#pics").children("li").length;
			   if(s=="" || activtyUrlLength<2 ||activtyUrlLength >9){
					alert("活动图片不能为空,个数为2-9");
					return
				} */
				
			 //活动图片
			 var activ = document.getElementsByName('activtyUrlV'); 
		
			   var actent='';
			   for(var i=0; i<activ.length; i++){
				   actent+=activ[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(actent==''){
				   actent="";
			   }else{
				   actent=actent.substring(0, actent.length-1);   
			   }
			   
			   $("#activtyUrl").val(actent); 
			   var acUrlLength=$("#pics").children("li").length;
			   if(actent==''){
				   alert("活动图片不能为空");
			   		 return;
			   }
			   if(actent!=''&&acUrlLength<2||acUrlLength>9){
				   alert("活动图片个数为2-9张");
			   		return;
			   }
			   
			$("#addCorpraForm").submit();
		}
		
		
		function uploadDetailImage(){
			var formData2 = new FormData($("#detailPicForm")[0]);
			$.ajax({
				type : "post",
				url : "<%=basePath %>skill/uploadImage.html",
				dataType : "json",
				data : formData2,
				async : false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data) {
					if(data.ajax_status == "ajax_status_success"){
						var picListHtml = '';
						var pics = jQuery.parseJSON(data.pics);
						for (var i = 0; i < pics.length; i++) {
							var pic = pics[i];
							picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick="deleteDetailImage(&quot;'+pic.imgUrl+'&quot;, &quot;'+pic.targetPath+'&quot;)" class="remove">删除</a></div><div class="img"><img src="'+pic.targetPath+'" style="width:100;height:100" alt=""></div></li>';
							$("#addCorpraForm").append("<input type='hidden' name='activtyUrlV' value='"+pic.imgUrl+"'>");
						}
						$("#pics").append(picListHtml);
						$("#pics").show();
					}else{
						alert(data.msg);
					}
					return false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
			          alert("出错了！");
		        }
			});
			return false;;
		}
		
		function deleteDetailImage(imgUrl,targetPath){
			var imgLi = $("img[src='"+targetPath+"']").parent().parent();
			var imgInput = $("input[name=imgUrl][value='"+imgUrl+"']");
			console.log(imgInput.prop("outerHTML"));
			//删除图片，父li移除，删除input
			console.log($("input[name=imgUrl]").size());
			imgLi.remove();
			imgInput.remove();
			alert("删除成功！");
			return true;
		}
		
	
	  
</script>
</div>