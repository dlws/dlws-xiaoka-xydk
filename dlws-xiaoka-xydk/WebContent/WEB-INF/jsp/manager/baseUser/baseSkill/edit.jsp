<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>校咖微平台后台管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>技能设置 > <b>技能修改</b></h2>
				</div>
				<div class="form">
					<form id="editPlatGoods" action="${pageContext.request.contextPath}/baseInfoSkill/update.html" method="post">
						<input type="hidden" name="id" value="${objMap.id}">
						<input type="hidden" name="ishome" value="${objMap.ishome}">
						<input type="hidden" name="ord" value="${objMap.ord}">
						<%-- <input type="hidden" name="company" value="${objMap.company}"> --%>
						<div class="form-item">
							<div class="form-name">技能名称：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="skillName" onblur="validName()"  value = "${objMap.skillName }">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">技能分类:</div>
								<div class="form-input">
									<select class="input" name="skillFatId" id="skillFatId" onchange="queryClass(this.value);">
										<c:forEach items="${classF }" var="classInfo">
										
											<c:if test="${classInfo.id == objMap.skillFatId }">
												<option value="${classInfo.id}" selected = "selected">${classInfo.className }</option>
											</c:if>
											<c:if test="${classInfo.id != objMap.skillFatId }">
												<option value="${classInfo.id}">${classInfo.className }</option>
											</c:if>
											<%-- <option value="${classInfo.id}" selected = "selected">${classInfo.className }</option> --%>
										</c:forEach>
									</select> 
								</div>
						</div>
						<div class="form-item">
								<div class="form-name">技能小类:</div>
								<div class="form-input">
									<select name="skillSonId" class="input" id="skillSonId">
									<c:forEach items="${classS }" var="info">
										
											<c:if test="${info.id == objMap.skillSonId }">
												<option value="${info.id}" selected = "selected">${info.className }</option>
											</c:if>
											<c:if test="${info.id != objMap.skillSonId }">
												<option value="${info.id}">${info.className }</option>
											</c:if>
											<%-- <option value="${classInfo.id}" selected = "selected">${classInfo.className }</option> --%>
										</c:forEach>
										 <%-- <option value="${objMap.schoolId}">${objMap.schoolName }</option> --%>
									</select>
								</div>
						</div>
						<div class="form-item">
							<div class="form-name">视屏地址：</div>
							<div class="form-input">
								<input type="text" class="input" name="videoUrl" id="videoUrl" value="${objMap.videoUrl }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">其他作品：</div>
							<div class="form-input">
								<input type="text" class="input" id="textURL" name="textURL" value="${objMap.textURL }">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">技能价格：</div>
							<div class="form-input">
								<input type="text" class="input" name="skillPrice" id="skillPrice" style="ime-mode:disabled;" onpaste="return false;" value="${objMap.skillPrice }"  onkeypress="keyPress()" placeholder="单位：元">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						<div class="form-item">
						<div class="form-name">价格单位</div>
							<div class="form-input">
								
								<select class="input" name="company" id="company">
									<c:forEach items="${canpanyMap }" var="info">
										<c:if test="${objMap.company==info.id }">
											<option value="${info.id }" selected="selected">${info.dic_name }</option>
										</c:if>
										<c:if test="${objMap.company!=info.id }">
											<option value="${info.id }">${info.dic_name }</option>
										</c:if>
									</c:forEach>
								</select> 
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">服务类型：</div>
							<div class="form-input">
								<select name="serviceType" class="input">
									<option value="1"<c:if test="${objMap.serviceType == 1}">selected="selected"</c:if>>线下服务</option>
									<option value="2"<c:if test="${objMap.serviceType == 2}">selected="selected"</c:if>>线上服务</option>
								</select>
							</div>
						</div>
						
						<br>
						<div class="form-item">
							<div class="form-name">技能描述：</div>
							<div class="form-input">
								<input name="skillDepict" class="input"  id="skillDepict" value="${objMap.skillDepict }" ></input>
							</div>
						</div>
						
						<input type="hidden" name="thumbnailUrl" id="thumbnailUrl">
						<input type="hidden" name = "basicId"  id = "basicId" value="${basicId }">
						
						<c:if test="${not empty images}">
							<c:forEach var="img" items="${images}">
								<input type="hidden" name="imgUrl" value="${img.imgURL}">
							</c:forEach>
						</c:if>
						
					</form>
				
					<form id="detailPicForm" onsubmit="return false;">
						<div class="form-item form-up">
							<div class="form-name">商品详情页图集：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="imgs" id="morePic" multiple size="80" class="btn-blue" /> 
									<!-- <a href="javascript:void(0);" class="btn-blue">选择图片
									</a> -->
								</div>
								<ul id="pics" class="up-list">
									<c:if test="${not empty images}">
										<c:forEach var="imgUrl" items="${images}">
											<li>
												<div class="txt">
													<span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage('${imgUrl.imgURL}','${projectPath}${imgUrl.imgURL}')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${projectPath}${imgUrl.imgURL}" style="width:100px;height:100px;" alt="">
												</div>
											</li>
										</c:forEach>
									</c:if>
								</ul>
								<div class="btns">
									<button class="btn-blue" onclick="uploadDetailImage()">开始上传</button>
									<!-- <a href="" class="btn-lightGray">全部取消</a> -->
								</div>
							</div>
						</div>
					</form>
					
					<button type="button" class="btn w144" onclick="add()">保  存</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
	<script type="text/javascript">
		function keyPress() {
			var keyCode = event.keyCode;
			if ((keyCode >= 48 && keyCode <= 57 || keyCode == 46)) {
		    	event.returnValue = true;
		    } else {
		    	event.returnValue = false;
		    }
		}
		
		KE.show({
			id : 'detail',
			height:'500px',
			imageUploadJson : '${path}/platgoods/uploadDescImage.html', //'${path}/kindeditor/jsp/upload_json.jsp',
			allowFileManager : true,
			afterCreate : function(id) {
				KE.event.ctrl(document, 13, function() {
					KE.util.setData(id);
				});
			}
		});
	
		$("input[name='goodsName']").bind("input propertychange", function() {
			validName();
		});
		$("input[name='costPrice']").bind("input propertychange", function() {
			 var money = $(this).val();
			 var msg = $("input[name='costPrice']").parent().find(".form-msg");
			 var regu =/^[0-9]+(\.[0-9]+)?$/;
			 var re = new RegExp(regu); 
			 if (!re.test(money)) { 
				 msg.html("(请输入正确的金额)");
				 msg.show();
			 	return false;
			 }else{
				msg.hide();
			 }
		});
		$("input[name='marketPrice']").bind("input propertychange", function() {
			 var money = $(this).val();
			 var msg = $("input[name='marketPrice']").parent().find(".form-msg");
			 var regu =/^[0-9]+(\.[0-9]+)?$/;
			 var re = new RegExp(regu); 
			 if (!re.test(money)) { 
				 msg.html("(请输入正确的金额)");
				 msg.show();
			 	return false;
			 }else{
				msg.hide();
			 }
		});
		$("input[name='wholesalePrice']").bind("input propertychange", function() {
			 var money = $(this).val();
			 var msg = $("input[name='wholesalePrice']").parent().find(".form-msg");
			 var regu =/^[0-9]+(\.[0-9]+)?$/;
			 var re = new RegExp(regu); 
			 if (!re.test(money)) { 
				 msg.html("(请输入正确的金额)");
				 msg.show();
			 	return false;
			 }else{
				msg.hide();
			 }
		});
		function validPrice(){
			var flag = false;
			var money = $("input[name='costPrice']").val();
			var msg = $("input[name='costPrice']").parent().find(".form-msg");
			var regu =/^[0-9]+(\.[0-9]+)?$/;
			var re = new RegExp(regu); 
			if (!re.test(money)) { 
				msg.html("(请输入正确的金额)");
				msg.show();
				flag = false;
			}else{
				msg.hide();
				flag = true;
			}
			money = $("input[name='marketPrice']").val();
			msg = $("input[name='marketPrice']").parent().find(".form-msg");
			regu =/^[0-9]+(\.[0-9]+)?$/;
			re = new RegExp(regu); 
			if (!re.test(money)) { 
				msg.html("(请输入正确的金额)");
				msg.show();
				flag = false;
			}else{
				msg.hide();
				flag = true;
			}
			money = $("input[name='wholesalePrice']").val();
			msg = $("input[name='wholesalePrice']").parent().find(".form-msg");
			regu =/^[0-9]+(\.[0-9]+)?$/;
			re = new RegExp(regu); 
			if (!re.test(money)) { 
				msg.html("(请输入正确的金额)");
				msg.show();
				flag = false;
			}else{
				msg.hide();
				flag = true;
			}
			return flag;
		}
		function validName(){
			var flag = false;
			var oldName = $("#categoryName").val();
			var name = $("input[name='goodsName']").val();
			var msg = $("input[name='goodsName']").parent().find(".form-msg");
			if(name==null || name.trim() == "" || name.trim().length < 1){
				msg.html("(商品名称不能为空)");
				msg.show();
				flag = false;
			}else if(name.length > 30) {
				msg.html("(商品名称只能为1-30个字)");
				msg.show();
				flag = false;
			}else if(oldName == name) {
				msg.hide();
				flag = true;
			}else{
				msg.hide();
				$.ajax({
					type : "post",
					url : "editExistName.html",
					dataType : "json",
					data : {oldName:oldName, newName:name},
					async : false,
					success : function(data) {
						if (data.ajax_status == 'ajax_status_success') {
							if(data.flag == '-1') {
								msg.html("(商品名称不能为空)");
								msg.show();
				  				//为空
								flag = false;
				  			} else if(data.flag == '0'){
				  				//不存在，可用
				  				msg.hide();
				  				flag = true;
				  			} else if(data.flag == '1'){
				  				//已经存在
								msg.html("(该商品名称已经存在)");
								msg.show();
								flag = false;
				  			} else if(data.flag == '2'){
								msg.html("(验证商品名称失败)");
								msg.show();
				  				//查询失败
								flag = false;
				  			} else if(data.flag == '3'){
				  				//未做任何修改
				  				msg.hide();
				  				flag = true;
				  			}
						}else if (data.ajax_status == 'ajax_status_failure'){
							flag = false;
						}
					},
					error : function(XMLHttpRequest, textStatus, errorThrown){
			          //通常情况下textStatus和errorThrown只有其中一个包含信息
			          alert("出错了！");
			          flag = false;
			       }
				});
			}
			return flag;
		}
		
		function uploadFirstImage(){
			var formData = new FormData($("#firstPicForm")[0]);
			$.ajax({
				type : "post",
				url : "uploadFirstImage.html",
				dataType : "json",
				data : formData,
				async : false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data) {
					if(data.ajax_status == "ajax_status_success"){
						$("#firstPic").find("img").attr("src",data.targetPath);
						$("#picUrl").val(data.picUrl);
						$("#firstPic").show();
					}else{
						alert(data.msg);
					}
					return false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
		          alert("出错了！");
		       }
			});
			return false;;
		}
		
		
		
		function uploadDetailImage(){
			var formData2 = new FormData($("#detailPicForm")[0]);
			$.ajax({
				type : "post",
				url : "${path}/skill/uploadImage.html",
				dataType : "json",
				data : formData2,
				async : false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data) {
					if(data.ajax_status == "ajax_status_success"){
						var picListHtml = '';
						var pics = jQuery.parseJSON(data.pics);
						for (var i = 0; i < pics.length; i++) {
							var pic = pics[i];
							picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick="deleteDetailImage(&quot;'+pic.imgUrl+'&quot;, &quot;'+pic.targetPath+'&quot;)" class="remove">删除</a></div><div class="img"><img src="'+pic.targetPath+'" style="width:100px;height:100px;" alt=""></div></li>';
							$("#editPlatGoods").append("<input type='hidden' name='imgUrl' value='"+pic.imgUrl+"'>");
						}
						$("#pics").append(picListHtml);
						$("#pics").show();
					}else{
						alert(data.msg);
					}
					return false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
			          alert("出错了！");
		        }
			});
			return false;;
		}
		
		function deleteDetailImage(imgUrl,targetPath) {
			var imgLi = $("img[src='"+targetPath+"']").parent().parent();
			var imgInput = $("input[name=imgUrl][value='"+imgUrl+"']");
			console.log(imgInput.prop("outerHTML"));
			//删除图片，父li移除，删除input
			console.log($("input[name=imgUrl]").size());
			imgLi.remove();
			imgInput.remove();
			alert("删除成功！");
			console.log($("input[name=imgUrl]").size());
			
			/* $.ajax({
				type : "post",
				url : "deleteDetailImage.html",
				dataType : "json",
				data : {targetPath:targetPath},
				async : false,
				success : function(data) {
					
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
		          alert("出错了！");
		       }
			}); */
			return true;
		}
		
		function uploadThumbnailImage(){
			var formData3 = new FormData($("#thumbnailPicForm")[0]);
			$.ajax({
				type : "post",
				url : "uploadFirstImage.html",
				dataType : "json",
				data : formData3,
				async : false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data) {
					if(data.ajax_status == "ajax_status_success"){
						$("#thumbnailPic").find("img").attr("src",data.targetPath);
						$("#thumbnailUrl").val(data.picUrl);
						$("#thumbnailPic").show();
					}else{
						alert(data.msg);
					}
					return false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
		          alert("出错了！");
		       }
			});
			return false;;
		}
		//提交
		function add(){
			/* var flag1 = validName();
			var flag2 = validPrice();
			getCategory();
			if(flag1 && flag2){
				$("#editPlatGoods").submit();
			} */
			var flag = true;
			var skillName = $("input[name='skillName']").val();
			if(skillName==null || skillName.trim() == "" || skillName.trim().length < 1){
				alert("请输入技能名称");
				flag = false;
			}
			var videoUrl = $("input[name='videoUrl']").val();
		/* 	if(videoUrl==null || videoUrl.trim() == "" || videoUrl.trim().length < 1){
				alert("请输入视频地址");
				flag = false;
				
			} */
			var textURL = $("input[name='textURL']").val();
		/* 	if(textURL==null || textURL.trim() == "" || textURL.trim().length < 1){
				alert("请输入其他地址");
				flag = false;
			} */
			var skillPrice = $("input[name='skillPrice']").val();
			if(skillPrice==null || skillPrice.trim() == "" || skillPrice.trim().length < 1){
				alert("请输入技能价格");
				flag = false;
			}
			var skillDepict = $("input[name='skillDepict']").val();
			if(skillDepict==null || skillDepict.trim() == "" || skillDepict.trim().length < 1){
				alert("请输入技能描述");
				flag = false;
			}
			var imgUrl = $("input[name='imgUrl']").val();
			if(imgUrl==null || imgUrl.trim() == "" || imgUrl.trim().length < 1){
				alert("请上传图片");
				flag = false;
			}
			if(flag){
				$("#editPlatGoods").submit();
			}
			
		}
		
		function getCategory(){
			var obj=document.getElementsByName('goodCategory');  //选择所有name="area"的对象，返回数组
			   //取到对象数组后，来循环检测它是不是被选中
			   var s='';
			   for(var i=0; i<obj.length; i++){
			     if(obj[i].checked) s+=obj[i].value+',';  //如果选中，将value添加到变量s中
			   }
			   //那么现在来检测s的值就知道选中的复选框的值了
			   if(s==''){
				s="";
			   }else{
				   s=s.substring(0, s.length-1);   
			   }
			   //alert(s);
			   $("#goodCategoryId").val(s);
		}
		
		function queryClass(value){
			$.ajax({
				type : "post",
				url : "${path}/skill/getClassSByFid.html",
				dataType : "json",
				data : {sid:value},
				success : function(data) {
					var $se = $("#skillSonId option").remove();
					var $sel = $("#skillSonId");
					for(var i=0;i<=data.classS.length;i++){
						var $option = $("<option value = '"+data.classS[i].id+"'>"+data.classS[i].className +"</option>");
						$sel.append($option);
					}
				}
			});	 
		}
	</script>
</body>
</html>