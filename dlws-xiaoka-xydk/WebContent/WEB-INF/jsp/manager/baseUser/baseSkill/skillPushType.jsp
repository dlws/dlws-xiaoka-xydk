<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
				<div class="column-name">
					<h2>类型选择 > <b>发布类型</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>/baseInfoSkill/toAdd.html" id="settle" method="post">
						
						<div class="form-item">
							<div class="form-name" style="margin-top:-5px;">选择发布类型：</div>
							<c:forEach items="${enTypelist}" var="info">
								<input type="radio"  name="classValue" value="${info.classValue}"  class="pubType"/>${info.className}
								<input type="hidden" name="father" class="fatherId" value="${info.id}">
							</c:forEach>
						</div>
						<br/>
						<input type="hidden" name="basicId" value="${basicId}">
						<input type="hidden" name="skillId" value="${paramsMap.skillId}">
						<input type="hidden" name="fatherId" id="father">
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="toAddSettle()">
						</div>
						</form>
				</div>
<script>
$(".pubType").click(function(){
	var index = $(this).index(".pubType") ;
	var pubType = $(".fatherId").eq(index).val();
	$("#father").val(pubType);
});
function toAddSettle(){
	$("#settle").submit();
}
</script>
