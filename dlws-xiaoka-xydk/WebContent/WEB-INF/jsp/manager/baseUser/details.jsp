<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	
<div class="column-name">
	<h2>基本信息管理> <b>查看详情</b></h2>
</div>
<div class="form1" name="form1">
	<form action="<%=basePath%>basInfoUser/userBasicinfoList.html" id="bannerForm" method="post" >
		<input id="id" type="hidden" name="id" value="${objMap.id }">
		<div class="form-item">
			<div class="form-name">入驻名称:</div>
			<div class="form-input">
				<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${objMap.userName }">
				<div class="clearfix"></div>
				<div class="form-msg"></div>
			</div>
		</div>
		
		<div class="form-item">
			<div class="form-name">联系人:</div>
			<div class="form-input">
				<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${objMap.contactUser}">
				<div class="clearfix"></div>
				<div class="form-msg"></div>
			</div>
		</div>
		
		<div class="form-item">
			<div class="form-name">电话:</div>
			<div class="form-input">
				
				<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${objMap.phoneNumber }">
			</div>
		</div>
		
		<div class="form-item">
			<div class="form-name">邮箱:</div>
			<div class="form-input">
				
				<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${objMap.email }">
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">微信号:</div>
			<div class="form-input">
				
				<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${objMap.wxNumber }">
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">入驻类型:</div>
			<div class="form-input">
			
					<c:if test="${objMap.checkType==0}">
					<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="个人">		
					</c:if>
					<c:if test="${objMap.checkType==1}">
					<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="团队">	
					</c:if>
			</div>
		</div>
		
		<div class="form-item">
			<div class="form-name">省:</div>
				<div class="form-input">
					
					<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${proMap.province}">
				</div>
		</div>
		
		 <div class="form-item">
			<div class="form-name">城市:</div>
				<div class="form-input">
					
					<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${objMap.cityName }">
				</div>
		</div> 
		
		
		<div class="form-item">
				<div class="form-name">学校:</div>
				<div class="form-input">
					
					<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${sholMap.schoolName}">
				</div>
		</div>
		
		<div class="form-item">
			<div class="form-name">访问量:</div>
			<div class="form-input">
				
				<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${objMap.viewNum}">
			</div>
		</div>
		
		<div class="form-item">
			<div class="form-name">主页介绍:</div>
			<div class="form-input">
				<input name="userName" type="text" class="input" style="border:0px" id="userName" readonly= "true" value="${objMap.aboutMe}">
			</div>
		</div>
		
		<div class="form-item">
			<div class="form-name">主页标签：</div>
			<div class="form-input" style="margin-top: 24px">
					<c:forEach var="map"  items="${Labelist}" varStatus="status">
							<input type="checkbox"  name="labelId" class="lx" 
							<c:forEach var="yx"  items="${labelArr}">
								<c:if test="${yx==map.id}">checked="checked" </c:if>
							</c:forEach>
							   value="${map.id}">${map.labelName }
					</c:forEach>
				<div class="clearfix"></div>
				<div class="form-msg hide" style="color: red;"></div>
			</div>
		</div>
		
		<div class="form-item form-item01">
			<div class="form-name">入驻头像:</div>
			<div class="form-input">
				<div class="form-img">
					<img id="allUrl" src="${objMap.headPortrait}">
				</div>
			</div>
		</div>
		<br/>
		<div style="text-align:center; vertical-align:middel;">
			<input type="button" class="btn w144" value="返 回" onclick="sub()">
		</div>
	</form>
<script>
	function sub(){
		$("#bannerForm").submit();
	}		
</script>
</div>
