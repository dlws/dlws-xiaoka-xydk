<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	
				<div class="column-name">
					<h2>基本信管理> <b>基本信息修改</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>basInfoUser/updateUserBasicinfo.html" id="bannerForm" method="post" enctype="multipart/form-data">
						<input id="id" type="hidden" name="id" value="${objMap.id }">
						<div class="form-item">
							<div class="form-name">入驻名称:</div>
							<div class="form-input">
								<input name="userName" type="text" class="input" id="userName" value="${objMap.userName }">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系人:</div>
							<div class="form-input">
								<input name="contactUser" type="text" class="input" id="contactUser" value="${objMap.contactUser}">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">电话:</div>
							<div class="form-input">
								<input name="phoneNumber" id="phoneNumber" type="text" class="input" value="${objMap.phoneNumber }" onkeyup="value=value.replace(/[^\-?\d.]/g,'')">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">邮箱:</div>
							<div class="form-input">
								<input name="email" id="email" type="text" class="input" value="${objMap.email }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">微信号:</div>
							<div class="form-input">
								<input name="wxNumber" id="wxNumber" type="text" class="input" value="${objMap.wxNumber }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">入驻类型:</div>
							<div class="form-input">
								<select class="input" name="checkType" id="checkType">
								<c:choose>
									<c:when test="${objMap.checkType==0}">
											<option value="0" selected="selected">个人</option>
											<option value="1">团队</option>
									</c:when>
									<c:otherwise>
											<option value="0">个人</option>
											<option value="1" selected="selected">团队</option>
									</c:otherwise>
									</c:choose>
								</select> 
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">选择省:</div>
								<div class="form-input">
									<select class="input" name="proId" id="proId" onchange="queryCityByPro(this.value);">
										 <option value="">请选择</option>
										<c:forEach items="${provnceList }" var="provc">
												<option value="${provc.province}">${provc.province }</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						 <div class="form-item">
							<div class="form-name">选择城市:</div>
								<div class="form-input">
									<select class="input" name="cityId" id="cityId" onchange="queryScholTypeByCity(this.value);">
									 <option value="">请选择</option>
										<!-- <option value="">全部</option> -->
										
										<c:forEach items="${cityList }" var="city">
										<c:choose>
											<c:when test="${objMap.cityId==city.value}">
											
													<option value="${city.value}" selected = "selected">${city.text }</option>
											</c:when>
											<c:otherwise>
												<option value="${city.value}">${city.text }</option>
											</c:otherwise>
											</c:choose>
										</c:forEach>
									</select> 
								</div>
						</div> 
						
						<div class="form-item">
							<div class="form-name">选择院校类别:</div>
								<div class="form-input">
									<select class="input" name="scholType" id="scholType" onchange="queryScholByType(this.value);">
										<!-- <option value="">全部</option> -->
										 <option value="">请选择</option>
										<c:forEach items="${scholTypeList }" var="stp">
												<option value="${stp.value}">${stp.text}</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						<div class="form-item">
								<div class="form-name">学校:</div>
								<div class="form-input">
									<select name="schoolId" class="input" id="schools">
										 <option value="${objMap.schoolId}">${sholMap.schoolName}</option>
									</select>
								</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">访问量:</div>
							<div class="form-input">
								<input name="viewNum" id="viewNum" type="text" class="input" value="${objMap.viewNum}">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">主页介绍:</div>
							<div class="form-input">
								<input name="aboutMe" id="aboutMe" type="text" class="input" value="${objMap.aboutMe}">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">主页标签：</div>
							<div class="form-input" style="margin-top: 24px">
									<c:forEach var="map"  items="${Labelist}" varStatus="status">
											<input type="checkbox"  name="labelId" class="lx" 
											<c:forEach var="yx"  items="${labelArr}">
												<c:if test="${yx==map.id}">checked="checked" </c:if>
											</c:forEach>
											   value="${map.id}">${map.labelName }
									</c:forEach>
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						<div class="form-item form-item01">
							<div class="form-name">入驻头像:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value="${objMap.keyValue }"/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${objMap.headPortrait }"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl" src="${objMap.headPortrait}">
    							</div>
							</div>
						</div>
						<br/>
						<input type="hidden" name="province" id="province" value="${proMap.province}">
						<input type="hidden" name="cityIdval" id="cityIdval" value="${objMap.cityId }">
						<input type="hidden" name="cityIds" id="cityIds" value="${objMap.cityId }">
						<input type="hidden" name="sholIdval" id="sholIdval" value="${objMap.schoolId}">
						<input type="hidden" name="yxId" id="yxId" value="${objMap.labelId}">
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="sub()">
						</div>
						</form>
		<script>
		function sub(){
			 var userName = $("#userName").val();
			if(userName==""){
				alert("请输入姓名");
				return
			}
			var phoneNumber=$("#phoneNumber").val()
   			var re=/^1[3|5|8]\d{9}$/;
	   		if(phoneNumber==""){
	   			alert("请输入手机号");
	   			return;
	   		}else if(!re.test(phoneNumber)){
	   			alert("请输入正确的手机号");
	   			return;
	   		}
	   		
	   		
	   		var email=$("#email").val()
	   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		
	   		if(email!=""){
		   		if(!regEma.test(email)){
		   			alert("邮箱格式有误");
		   			return;
		   		}
	   		}
	   		
	   		var wxNumber=$("#wxNumber").val()
	   		var regWx=/^[a-zA-Z\d_]{5,}$/;
	   		if(wxNumber!=""){
		   		if(!regWx.test(wxNumber)){
		   			alert("微信号格式有误");
		   			return;
		   		}
	   		}
	   		
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
	   		
			var nickname = $("nickname").val();
			if(nickname==""){
				alert("请输入昵称");
				return;
			}
			var picUrl = $.trim($("#picUrl").val());
		
			var schools = $("#schools").val();
			if(schools==""||schools==0){
				alert("请选择学校！");
				return
			}
			
			var aboutMe = $("#aboutMe").val();
			if(aboutMe==""){
				alert("请输入简介");
				return;				
			}
			
			if(picUrl==""){
				alert("上传图片不能为空！");
				return
			}
			
			
			var obj=document.getElementsByName('labelId');
			debugger
			var s=''; 
			for(var i=0; i<obj.length; i++){ 
				if(obj[i].checked){
					s+=obj[i].value+','; //如果选中，将value添加到变量s中 
				}
			}
			s.substring(0,s.length-1);
			$("#yxId").val(s);
			
			$("#bannerForm").submit();
		}
			//上传图片
			function uploadPic(){
				//异步上传
				var options = {
						url : "<%=basePath %>bannerInfo/picture.html",
						dataType : "json",
						type : "post",
						success : function(data){
							//回调 路径  data.path
							$("#allUrl").attr("src",data.picUrl);
							$("#keyValue").val(data.key);
							$("#picUrl").val(data.picUrl);
						}
				}
				$("#bannerForm").ajaxSubmit(options);
			}
		<%-- function querySchool(value){
			
			$.ajax({
				type : "post",
				url : "<%=basePath%>v2busyShopBasicinfo/querySchoolByCity.html",
				dataType : "json",
				data : {code:value},
				success : function(data) {
					var $se = $("#school option").remove();
					var $sel = $("#school");
					for(var i=0;i<=data.school.length;i++){
						var $option = $("<option value = '"+data.school[i].schoolId+"'>"+data.school[i].schoolName +"</option>");
						$sel.append($option);
					}
				},
			});
		}
		
		function queryGoods(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/querySchoolInfoByCityId.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $se = $("#schools option").remove();
					var $sel = $("#schools");
					for(var i=0;i<=data.schools.length;i++){
						var $option = $("<option value = '"+data.schools[i].schoolId+"'>"+data.schools[i].schoolName +"</option>");
						$sel.append($option);
					}
				}
			});	
		} --%>
		
		
		
		
		//根据省查出市
		function queryCityByPro(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryCityByPro.html",
				dataType : "json",
				data : {proId:value},
				success : function(data) {
					 $("#cityId option").remove();
					 $("#scholType option").remove();
					 $("#schools option").remove();
					var $sel = $("#cityId");
					if(data.cityList.length>0){
						 $.each(data.cityList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholTypeByCity(data.cityList[0].value);
					}
				}
			});	
			
		}
		
		//根据市查出院校类别
		 function queryScholTypeByCity(value){
			 $("#scholType option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#scholType").append($option0)
			 
			  $("#schools option").remove();
			  var $option1 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option1)
			$("#cityIds").val(value);
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholTypeByCity.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $sel = $("#scholType");
					if(data.scholTypeList.length>0){
						$.each(data.scholTypeList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholByType(data.scholTypeList[0].value);//根据院校类别查出院校
					}
					
				}
			});	
		}
		//根据院校类别查出院校
		 function queryScholByType(value){
			 $("#schools option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option0)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholByType.html",
				dataType : "json",
				data : {dcdaId:value,cityId:$("#cityIds").val()},
				success : function(data) {
					var $sel = $("#schools");
					if(data.scholList.length>0){
						$.each(data.scholList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		} 
</script>
</div>
