<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	<style type="text/css">
		.custom{
			position:relative; left:120px; 
			margin: 1% 1% 0% 0%;
		}
	</style>
	<body>
	<div class="column-name">
		<h2>用户入驻管理> <b>场地技能修改</b></h2>
	</div>
	<div class="form1" >
		<form action="<%=basePath%>/fieldSkill/upFieldSkill.html" id="bannerForm" method="post" enctype="multipart/form-data">
			<div class="form-item">
				<div class="form-name">场地名称:</div>
				<div class="form-input">
					<input name="skillName"  id="userName" type="text" class="input" value="${skillInfo.skillName }" maxlength="10">
				</div>
			</div>
			
			<div class="form-item">
				<div class="form-name">场地类型:</div>
				<div class="form-input"> 
					<select name="skillSonId" class="input">
						<c:forEach items="${list}" var="fieldType">
								<c:choose>
									<c:when test="${skillInfo.skillSonId eq fieldType.id}">
										<option value="${fieldType.id}" selected="selected">${fieldType.className}</option>
									</c:when>
									<c:otherwise>
										<option value="${fieldType.id}">${fieldType.className}</option>
									</c:otherwise>
								</c:choose>
						</c:forEach>
  					</select>
  				</div>
			</div>
			
			<div class="form-item">
				<div class="form-name">场地规模:</div>
				<div class="form-input">
					<select name="filedSize" class="input">
						<c:forEach items="${filedSizeList}" var="filedSize">
								<c:choose>
									<c:when test="${detailMap.filedSize eq filedSize.id}">
										<option value="${filedSize.id}" selected="selected">${filedSize.dic_name}</option>
									</c:when>
									<c:otherwise>
										<option value="${filedSize.id}">${filedSize.dic_name}</option>
									</c:otherwise>
								</c:choose>
						</c:forEach>
  					</select>
  				</div>
			</div>
			<!-- 此处应循环读取数据库 -->
			<div class="form-item">
				<div class="form-name">基本物资配备:</div>
					<div  class="form-input">
						<table class="input">
							<tr>
								<c:forEach items="${materialList}" var="material">
									<td style="border-width: 1px 1px 2px 3px;">
										<c:choose>
											<c:when test="${fn:contains(detailMap.material,material.id)}">
												<input type="checkbox" checked="checked" value="${material.id}" name="material">${material.dic_name}
											</c:when>
											<c:otherwise>
												<input type="checkbox" value="${material.id}" name="material">${material.dic_name}
											</c:otherwise>
										</c:choose>
									</td>
								</c:forEach>
							</tr>
						</table>
					</div>
			</div>
			<div class="form-item">
				<div class="form-name">使用时间:</div>
					<div class="form-input">
						<div class="input">
							<span>开始时间:</span>
								<input id="begin" name="beginDate" value="${detailMap.beginDate}" class="Wdate" type="text" onFocus="WdatePicker({dateFmt:'H:00'})"/>
							<span>结束时间:</span>
								<input id="end" name="endDate" value="${detailMap.endDate}" class="Wdate" type="text" onFocus="WdatePicker({dateFmt:'H:00'})"/>
						</div>
					</div>
			</div>
					<div class="custom">
					场地照片(每种类型最少上传两张图片):<br>
					<div class="form-item form-up">
							<div class="form-name">近景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picSmall" id="close" multiple size="80" class="btn-blue" onchange="uploadPic(1)"/> 
								</div>
								<ul id="ul1">
									<c:forEach items="${listClose}" var="close">
											<li>
												<div class="txt"><span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage('${close}','closePhoto')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${close}" width="200" height="200">
												</div>
											</li>
											<input type="hidden"  name="closePhoto" value="${close}">
									</c:forEach>
								</ul>
							</div>
					</div>
					<div class="form-item form-up">
							<div class="form-name">中景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picMiddle" id="middle" multiple size="80" class="btn-blue" onchange="uploadPic(2)"/> 
								</div>
								<ul id="ul2" >
									<c:forEach items="${listMiddle}" var="middle">
											<li>
												<div class="txt"><span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage('${middle}','closePhoto')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${middle}" width="200" height="200">
												</div>
											</li>
											<input type="hidden"  name="middlePho" value="${middle}">
									</c:forEach>
								</ul>
							</div>
					</div>
					<div class="form-item form-up">
							<div class="form-name">全景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picAll" id="all" multiple size="80" class="btn-blue" onchange="uploadPic(3)"/> 
								</div>
								<ul id="ul3">
									<c:forEach items="${listAll}" var="all">
											<li>
												<div class="txt"><span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage('${all}','closePhoto')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${all}" width="200" height="200">
												</div>
											</li>
											<input type="hidden"  name="allPhoto" value="${all}">	
									</c:forEach>
								</ul>
							</div>
					</div>
					<div>
						是否可提供兼职人员： 是：<input type="radio" name="partimejob" value="1" checked="checked">
										 否：<input type="radio" name="partimejob" value="0"><br/><br>
						<span>场地描述：</span>
							<textarea style="width:400px;height:200px;" id="currentDec" name="skillDepict" placeholder="场地描述将在20字以内200字以下">${skillInfo.skillDepict}</textarea>
						<br>
					</div>
			</div>
			<div class="form-item">
						<div class="form-name">场地报价:</div>
						<div class="form-input">
							<input name="skillPrice" type="number" class="input" value="${detailMap.skillPrice}" style="width:20%;">
						<select name="company" style="height:30px; margin-top: 20px;">
							<c:forEach var="unit" items="${allUnit}">
								<c:choose>
									<c:when test="${unit.id eq detailMap.company}">
										<option value="${unit.id}" selected="selected">${unit.dic_name}</option>
									</c:when>
									<c:otherwise>
										<option value="${unit.id}">${unit.dic_name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
						</div>
			</div>
			
			<div class="form-item">
				<div class="form-name">访问量:</div>
				<div class="form-input">
					<input name="viewNum" placeholder="请输入初始访问量" id="viewNum" type="number" class="input" value="${skillInfo.viewNum}">
				</div>
			</div>
			<br/>
			<div style="text-align:center; vertical-align:middel;">
				<input type="button" class="btn w144" value="保    存" onclick="addField()">
			</div>
			<input type="hidden" value="${id}" name="basicId">
			<input type="hidden" value="${list[0].fatherId}" name="skillFatId">
			<input type="hidden" value="${skillInfo.id}" name="skillId">
			</form>
			<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
	<script>
		function addField(){
			var userName = $("#userName").val();
			if(userName==""){
				alert("请输入入驻名称");
				return
			}
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
			var picUrl = $.trim($("#picUrl").val());
			var aboutMe = $("#currentDec").val();
			if(aboutMe==""){
				alert("请输入场地介绍");
				return;				
			}
			if(aboutMe.length<20){
				alert("场地描述不能少于20字");
				return;
			}
			if(aboutMe.length>200){
				alert("场地描述不能大于200字");
				return;
			}
			if($("#ul1 li").length<2||$("#ul2 li").length<2||$("#ul3 li").length<2){
				alert("每种 最少上传两张(近景 中景 远景)");
				return
			}
			if($("#ul1 li").length>9||$("#ul2 li").length>9||$("#ul3 li").length>9){
				alert("每种 最多上传九张(近景 中景 远景)");
				return
			}
			if($("#begin").val()==""||$("#end").val()==""){
				alert("开始时间，或者结束时间不能为空");
				return
			}
			if(parseInt($("#begin").val())>parseInt($("#end").val())){
				alert("场地开始使用时间不能大于场地结束使用时间");
				return
			}
			if($("#planprice").val()==""){
				alert("场地报价不能为空");
				return
			}
			$("#bannerForm").submit();
		}
		//上传图片
		function uploadPic(obj){
			var ulId="";
			var url="";
			var name=""
			if(obj==1){
				url="pictureSmall";
				ulId ="ul1";
				name = "closePhoto";
			}
			else if(obj==2){
				url="pictureMiddle";
				ulId ="ul2";
				name = "middlePho";
			}else{
				url="pictureAll";
				ulId ="ul3";
				name = "allPhoto";
			}
			var picListHtml ="";
			var options = {
					url : '<%=basePath %>enter/'+url+'.html',
					dataType : "json",
					type : "post",
					success : function(data){
						
						for(var i=0;i<data.length;i++){
							
							picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick=deleteDetailImage("'+data[i]+'","'+name+'") class="remove">删除</a></div><div class="img"><img src="'+data[i]+'" width="200" height="200" alt=""></div></li>'+
							'<input type="hidden"  name="'+name+'" value='+data[i]+'>';
						}
						
						$("#"+ulId).append(picListHtml);
					}
			}
			$("#bannerForm").ajaxSubmit(options); 
		}
		
		
		function deleteDetailImage(imgUrl,name){
			var imgLi = $("img[src='"+imgUrl+"']").parent().parent();
			var imgInput = $("input[name='"+name+"'][value='"+imgUrl+"']");
			console.log(imgInput.prop("outerHTML"));
			//删除图片，父li移除，删除input
			console.log($("input[name=imgUrl]").size());
			imgLi.remove();
			imgInput.remove();
			alert("删除成功！");
			console.log($("input[name=imgUrl]").size());
			return true;
		}
	</script>
</div>
</body>