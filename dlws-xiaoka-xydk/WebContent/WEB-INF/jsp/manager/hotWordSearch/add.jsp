<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>添加热搜词</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
	
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>热搜词管理 > <b>添加热搜词</b></h2>
				</div>
				<div class="form">
					<form id="income" name="incomeform" action="${path }/hotWordSearch/addHotWordSearch.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
						
								<div class="form-item">
									<div class="form-name">热搜词类型</div>
										<div class="form-input">
											<select class="input" name="classId" id="classId">
												<c:forEach items="${hotWordType }" var="info">
													<option value="${info.id}">${info.dic_name}</option>
												</c:forEach>							
											</select> 
										</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">热搜词：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="wordValue" name="wordValue" value="" placeholder="请输入热搜词"/>
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">排列顺序：</div>
									<div class="form-input">
										<input AutoComplete="off" type="number" class="input" id="ord" name="ord" value=""  maxlength="3" placeholder="排列顺序为整数" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" onafterpaste="this.value=this.value.replace(/[^0-9]/g,'')"/>
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
					<button type="button" class="btn w144" onclick="addBanner()">保  存</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
function addBanner(){
	
	var flag = true;
	var wordValue = $.trim($("#wordValue").val());
	var ord = $.trim($("#ord").val());
	if(wordValue==""){
		alert("热搜词不为空!");
		flag = false;
	}
	if(ord==""){
		alert("排列顺序不为空!");
		flag = false;
	}
	if(flag){
		$("#income").submit();
	}
}

</script>
