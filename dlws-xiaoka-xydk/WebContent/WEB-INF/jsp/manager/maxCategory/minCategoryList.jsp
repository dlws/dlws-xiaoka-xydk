<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<div class="column-name">
	<h2>
		分类管理> <b>二级分类管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="${path}/minCategory/minCategoryList.html" method="post">
	<div class="main-top">
		<div class="form-item">
			<div class="check-item">
				<span class="check-tit">名称：</span>
					<input type="text" value="${className }" class="input" name="className" placeholder="请输入小类名称关键字"/>
				<span class="check-tit">父级：</span>
				<select id="fatherId" name="fatherId" class="input">
					<option value="">无</option>
					<c:forEach items="${list}" var="m">
						<option value="${m.id}">${m.className }</option>
									<%-- <c:if test="${m.id==map.fatherId }">
										<option value="${m.id}" selected="selected">${m.className }</option>
									</c:if>
									<c:if test="${m.id!=map.fatherId }">
										<option value="${m.id}">${m.className }</option>
									</c:if> --%>
					</c:forEach>
				</select>
			</div>
				<input type="submit" class="btn w96" value="查  询" style="float:right">
		</div>
	</div>
	<div class="main-top">
			<div class="fl">
				<a class="btn btn-p" href="${pageContext.request.contextPath}/minCategory/toAddMinCategory.html" target="main">新建二级分类</a>
			</div>
	</div>
	</div>

	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
						<th>小类名称</th>
						<th>数据值</th>
						<th>状态</th>
						<th>序号</th>
						<th>创建时间</th>
						<th>服务商</th>
						<th>外交官</th>
						<th>平台</th>
						<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="map">
					<tr>
							<td>${map.className}</td>
							<td>${map.classValue}</td>
							<td>
								<c:if test="${map.status==0}">不发布</c:if>
								<c:if test="${map.status==1}">发布</c:if>
							</td>
							<td>${map.ord}</td>
							<td>${map.createDate}</td>
							<td>${map.serviceRatio}</td>
							<td>${map.diplomatRatio}</td>
							<td>${map.platformRatio}</td>
							<td>
								<a href="${path}/minCategory/toEditMinCategory.html?id=${map.id}">修改</a>
								<a href="#" onclick="del(${map.id});">删除</a>
							</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>
<script>
	function del(id){
		$.ajax({
			type : "post",
			url : "${path}/minCategory/deleteMinCategory.html",
			dataType : "json",
			data : {id:id},
			success : function(data) {
				if(data.flag==true){
					alert(" 删除成功!");
					window.location.reload();
				}else{
					alert("删除失败!");
				}
			}
		});
	}
</script>





		