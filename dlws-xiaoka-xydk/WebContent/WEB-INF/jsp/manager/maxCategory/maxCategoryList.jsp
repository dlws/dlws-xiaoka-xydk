<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<div class="column-name">
	<h2>
		分类管理> <b>一级分类管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="${path}/maxCategory/maxCategoryList.html" method="post">
	<div class="main-top">
		<div class="form-item">
			<div class="check-item">
				<span class="check-tit">名称</span>
					<input type="text" value="${endTime }" class="input" name="className"/>
				<!-- <span class="check-tit">状态：</span> -->
				<!-- <select id="school" name="schoolId" class="input">
					<option value="">无</option>
					<option value="0">未发布</option>
					<option value="1">已发布</option>
				</select> -->
			</div>
				<input type="submit" class="btn w96" value="查  询" style="float:right">
		</div>
	</div>
	<div class="main-top">
			<div class="fl">
				<a class="btn btn-p" href="${pageContext.request.contextPath}/maxCategory/toAddMaxCategory.html" target="main">新建一级分类</a>
			</div>
	</div>
	</div>

	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
						<th>大类名称</th>
						<th>数据值</th>
						<th>大类图标</th>
						<th>状态</th>
						<th>序号</th>
						<th>创建时间</th>
						<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="map">
					<tr>
							<td>${map.className}</td>
							<td>${map.classValue}</td>
							<td><img  height="50" width="100" src="${map.imageURL}"></img></td>
							<td>
								<c:if test="${map.status==0}">不发布</c:if>
								<c:if test="${map.status==1}">发布</c:if>
							</td>
							<td>${map.ord}</td>
							<td>${map.createDate}</td>
							<td>
								<a href="${path}/maxCategory/toEditMaxCategory.html?id=${map.id}">修改</a>
								<a href="#" onclick="del(${map.id});">删除</a>
							</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>
<script>
	function del(id){
		$.ajax({
			type : "post",
			url : "${path}/maxCategory/deleteMaxCategory.html",
			dataType : "json",
			data : {id:id},
			success : function(data) {
				if(data.flag==true){
					alert(" 删除成功!");
					window.location.reload();
				}else{
					alert("删除失败!");
				}
			}
		});
	}
	
</script>





		