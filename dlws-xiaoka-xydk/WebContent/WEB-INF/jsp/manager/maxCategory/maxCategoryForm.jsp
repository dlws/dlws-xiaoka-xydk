<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/js/jquery.form.js"></script>
<script src="http://api.map.baidu.com/api?v=1.3"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
	
</script>
<div class="column-name">
	<h2>
		分类管理 <b>新建一级分类</b>
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/maxCategory/addMaxCategory.html" method="post" enctype="multipart/form-data">
		<div class="form-item">
			<div class="form-name">分类名称</div>
			<div class="form-input">
				<input type="text" class="input"  name="className" value="${map.className}">
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">数据值</div>
			<div class="form-input">
				<input type="text" class="input"  name="classValue" value="${map.classValue}">
			</div>
		</div>
		<div class="form-item form-item01"  id="bb">
			<div class="form-name">图片</div>
			<div class="form-input">
				<button class="btn-blue" style="margin-left:0;">上传图片</button>
				<input type="hidden" name='imageURL' id='path' class='input' value="${map.imageURL }"/> 
				<input type="file" name="pic" class="file input" onchange="uploadPic()"/>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
				<div class="form-img">
					<img id="allUrl" src="${map.imageURL }">
				</div>
			</div>
		</div> 
		<div class="form-item form-item01" id="aa">
			<div class="form-name">发布选中图片</div>
			<div class="form-input">
				<button class="btn-blue" style="margin-left:0;">上传图片</button>
				<input type="hidden" name='selectimage' id='selectimagePath' class='input' value="${map.selectimage }"/> 
				<input type="file" name="picselect" class="file input" onchange="selectPic()"/>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
				<div class="form-img">
					<img id="selectimageId" src="${map.selectimage }">
				</div>
			</div>
		</div> 
		<div class="form-item form-item01">
			<div class="form-name">发布未选中图片</div>
			<div class="form-input">
				<button class="btn-blue" style="margin-left:0;">上传图片</button>
				<input type="hidden" name='noSelectimage' id='noSelectimagePath' class='input' value="${map.noSelectimage }"/> 
				<input type="file" name="picnoselect" class="file input" onchange="noSelectPic()"/>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
				<div class="form-img">
					<img id="noSelectimageId" src="${map.noSelectimage }">
				</div>
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">序号</div>
			<div class="form-input">
				<input type="text" class="input"  name="ord" value="${map.ord}">
			</div>
		</div>	
		<div class="form-item">
					<div class="form-name">状态</div>
						<div class="form-input">
							<select class="input" name="status" id="statusId">
								<c:if test="${map.status==0 }">
									<option value="0" selected="selected">不发布</option>
								</c:if>
								<c:if test="${map.status!=0 }">
									<option value="0">不发布</option>
								</c:if>
								<c:if test="${map.status==1 }">
									<option value="1" selected="selected">发布</option>
								</c:if>
								<c:if test="${map.status!=1 }">
									<option value="1">发布</option>
								</c:if>
									
							</select> 
				</div
		</div>
		


			<input type="hidden" name="id" value="${map.id }" id="cateId">
	        <input type="button" class="btn w144" onclick="subCate();" value="保  存">
	</form>
</div>
<script type="text/javascript">
	//上传图片
	function uploadPic(){
		//异步上传
		var options = {
				//url : "${path}/bannerInfo/picture.html",
				url : "${path}/maxCategory/uploadImg.html",
				dataType : "json",
				type : "post",
				success : function(data){
					//回调 路径  data.path
					//alert(data.path);
					$("#allUrl").attr("src",data.url);
					$("#path").val(data.path);
				}
		}
		$("#form111").ajaxSubmit(options);
	
	}
	//选中上传图片
	function selectPic(){
		//异步上传
		var options = {
				url : "${path}/maxCategory/uploadImg2.html",
				//url : "${path}/bannerInfo/picture2.html",
				dataType : "json",
				type : "post",
				success : function(data){
					//回调 路径  data.path
					//alert(data.path);
					$("#selectimageId").attr("src",data.selecturl);
					$("#selectimagePath").val(data.selectpath);
				}
		}
		$("#form111").ajaxSubmit(options);
	
	}
	//未选中上传图片
	function noSelectPic(){
		debugger;
		//异步上传
		var options = {
				url : "${path}/maxCategory/uploadImg3.html",
				dataType : "json",
				type : "post",
				success : function(data){
					//回调 路径  data.path
					$("#noSelectimageId").attr("src",data.noselecturl);
					$("#noSelectimagePath").val(data.noselectpath);
				}
		}
		$("#form111").ajaxSubmit(options);
	
	}
	
	function subCate(){
		var cId = $("#cateId").val();
		if(cId!=""){
			$("#form111").attr("action","${path}/maxCategory/editMaxCategory.html");
		}
		$("#form111").submit();
	}
	
	
	
	
	
	
	
</script>
