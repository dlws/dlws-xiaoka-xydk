<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/js/jquery.form.js"></script>
<script src="http://api.map.baidu.com/api?v=1.3"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
	
</script>
<div class="column-name">
	<h2>
		分类管理 <b>新建二级分类</b>
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/minCategory/addMinCategory.html" method="post" enctype="multipart/form-data">
		<div class="form-item">
					<div class="form-name">所属大类</div>
						<div class="form-input">
							<select class="input" name="fatherId" id="statusId">
								<c:forEach items="${list}" var="m">
									<c:if test="${m.id==map.fatherId }">
										<option value="${m.id}" selected="selected">${m.className }</option>
									</c:if>
									<c:if test="${m.id!=map.fatherId }">
										<option value="${m.id}">${m.className }</option>
									</c:if>
									
								</c:forEach>
							</select> 
				</div>
		</div>
		<div class="form-item">
			<div class="form-name">分类名称</div>
			<div class="form-input">
				<input type="text" class="input"  name="className" value="${map.className}">
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">数据值</div>
			<div class="form-input">
				<input type="text" class="input"  name="classValue" value="${map.classValue}">
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">序号</div>
			<div class="form-input">
				<input type="text" class="input"  name="ord" value="${map.ord}">
			</div>
		</div>	
		
		<div class="form-item">
					<div class="form-name">状态</div>
						<div class="form-input">
							<select class="input" name="status" id="statusId">
								<c:if test="${map.status==0 }">
									<option value="0" selected="selected">不发布</option>
								</c:if>
								<c:if test="${map.status!=0 }">
									<option value="0">不发布</option>
								</c:if>
								<c:if test="${map.status==1 }">
									<option value="1" selected="selected">发布</option>
								</c:if>
								<c:if test="${map.status!=1 }">
									<option value="1">发布</option>
								</c:if>
									
							</select> 
						</div>
		</div>
		<div class="form-item">
			<div class="form-name">服务器</div>
			<div class="form-input">
				<input type="number" step="0.1" class="input" id="serviceRatio" title="只能输入小数类型" pattern="^0.[\d]+" name="serviceRatio" value="${map.serviceRatio}" required>
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">外交官</div>
			<div class="form-input">
				<input type="number" step="0.1" class="input" id="diplomatRatio" title="只能输入小数类型" pattern="^0.[\d]+" name="diplomatRatio" value="${map.diplomatRatio}" required>
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">平台</div>
			<div class="form-input">
				<input type="number" step="0.1" class="input" id="platformRatio"  title="只能输入小数类型" pattern="^0.[\d]+" name="platformRatio" value="${map.platformRatio}" required>
			</div>
		</div>

			<input type="hidden" name="id" value="${map.id }" id="cateId">
	        <input type="button" class="btn w144" onclick="subCate();" value="保  存">
	</form>
</div>
<script type="text/javascript">
	//上传图片
	function uploadPic(){
		//异步上传
		var options = {
				url : "${path}/minCategory/uploadImg.html",
				dataType : "json",
				type : "post",
				success : function(data){
					//回调 路径  data.path
					$("#allUrl").attr("src",data.url);
					$("#path").val(data.path);
				}
		}
		$("#form111").ajaxSubmit(options);
	
	}
	
	function subCate(){
		var cId = $("#cateId").val();
		
		var serviceRatio = parseFloat($("#serviceRatio").val());
		var diplomatRatio =parseFloat($("#diplomatRatio").val());
		var platformRatio =parseFloat($("#platformRatio").val());
		
		if(serviceRatio<0||diplomatRatio<0||platformRatio<0){
			alert("所输入的比例必须大于0");
			return;
		}
		
		var count = serviceRatio+diplomatRatio+platformRatio;
		var all = parseFloat(1);
		if(all!=count){
			alert("请输入正确的比例 如 0.2 0.2 0.6")
			return;
		}
		
		if(cId!=""){
			$("#form111").attr("action","${path}/minCategory/editMinCategory.html");
		}
		$("#form111").submit();
	}
	
	
	
	
	
	
	
</script>
