<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>运营维护 ><b>链接管理</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>link/save.html" id="bannerForm" method="post">
						<div class="form-item">
							<div class="form-name">链接名称:</div>
							<div class="form-input">
								<input name="linkName" type="text" class="input" id="linkName">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">链接值:</div>
							<div class="form-input">
								<input name="linkValue" type="text" class="input" id="linkValue">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">是否启用:</div>
							<div class="form-input">
							<div class="clearfix"></div>
								<div class="form-msg"></div>
								<input name="isUse" type="radio" value=1>是</input>
								<input name="isUse" type="radio" value=0>否</input>
								
							</div>
						</div>
						
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="addBanner()">
						</div>
					</form>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>

</div>
<script type="text/javascript" >
		function addBanner(){
			var linkName = $("#linkName").val();
			if(linkName==""){
				alert("请输入名称");
				return
			}
			var linkValue = $("#linkValue").val();
			if(linkValue==""){
				alert("请输入值");
				return
			}
			$("#bannerForm").submit();
		}
</script>
