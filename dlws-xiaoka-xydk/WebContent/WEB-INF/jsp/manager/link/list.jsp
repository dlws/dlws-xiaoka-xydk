<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
				<div class="column-name">
					<h2>运营维护 ><b>链接管理</b></h2>
				</div>
				<div class="main-top">
					<div class="fl">
						<a href="<%=basePath%>link/add.html" class="btn btn-p">新建链接</a>
					</div>
				</div>
				<form id = "form1" name="form1">
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>链接名称</th>
								<th>链接值</th>
								<th>创建时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="link">
							<!-- 遍历数据 -->
							<tr>
								<%-- <td>${link.name }</td> --%>
								<td>${link.linkName}</td>
								<td>${link.linkValue}</td>
								<td>${link.createDate }</td>
								<td>
									<c:if test="${link.isUse == 0}">
										<a href="<%=basePath%>/link/change.html?id=${link.id}&isUse=1" class="">启用</a>
									</c:if>
									<c:if test="${link.isUse == 1}">
										<a href="<%=basePath%>/link/change.html?id=${link.id}&isUse=0" class="">停用</a>
									</c:if>
									<a href="<%=basePath%>/link/toUpdateLink.html?id=${link.id}&temp='update'" class="">修改</a>
									<a href="<%=basePath%>/link/deleteLink.html?id=${link.id}&isDelete=1" class="">删除</a>
								</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
		<div class="popBox chaLianjie-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="chaLianjie-con">
						<div class="tit" id="lookurl"></div>
						<a href="javascript:;" class="btn w144">确  认</a>
					</div>
				</div>
			</div>
		</div><!-- chaLianjie-pop -->
			</form>
		<script type="text/javascript">
				function confirmDelete(ID){
					if (confirm("是否确认")) {
						location.href='<%=basePath%>v2BannerInfo/deleteV2BannerInfo.html?bannerId='+ID;
					}  else  { 
						return false;
					};
					
				}
				function lookUrl(ID){
					$.ajax({
						type : "post",
						url : "<%=basePath%>v2BannerInfo/getV2BannerInfo.html",
						dataType : "json",
						data : {bannerId:ID},
						success : function(data) {
							var $picurl=data.picUrl;
							var $url = $("#lookurl");
							$url.append($picurl);
						},
					});
					
				}
		</script>

				