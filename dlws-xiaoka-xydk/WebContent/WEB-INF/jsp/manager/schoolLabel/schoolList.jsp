<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<div class="column-name">
	<h2>
		系统管理> <b>学校标签设置</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="${path}/school/schoolList.html" method="post">
	<div class="main-top">
		<div class="form-item">
			<div class="check-item">
				<span class="check-tit">学校名称</span>
				<input type="text"  value="${schoolName}" class="input" name="schoolName"/>
				<span class="check-tit">城市：</span>
				<select id="cityId" name="cityId" class="input">
					<option value="">全部</option>
					<c:forEach items="${cityList}" var="m">
						<c:if test="${m.cityId ==cityId}">
							<option selected="selected" value="${m.cityId}">${m.cityName }</option>
						</c:if>
						<c:if test="${m.cityId != cityId}">
							<option value="${m.cityId}">${m.cityName }</option>
						</c:if>
					</c:forEach>
				</select>
			</div>
				<input type="submit" class="btn w96" value="查  询" style="float:right">
		</div>
	</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>序号</th>
					<th>学校名称</th>
					<th>联系电话</th>
					<th>地址</th>
					<th>是否设置标签</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="map">
					<tr>
							<td>${map.id}</td>
							<td>${map.schoolName}</td>
							<td>${map.phone}</td>
							<td>${map.address}</td>
							<c:if test="${map.lebel==0}">
								<td>未设置</td>
							</c:if>
							<c:if test="${map.lebel!=0}">
								<td>已设置</td>
							</c:if>
							<td>
								<a href="${path}/school/toSchoolLabel.html?schoolId=${map.id}">标签设置</a>
							</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>
<script>
	function del(id){
		$.ajax({
			type : "post",
			url : "${path}/minCategory/deleteMinCategory.html",
			dataType : "json",
			data : {id:id},
			success : function(data) {
				if(data.flag==true){
					alert(" 删除成功!");
					window.location.reload();
				}else{
					alert("删除失败!");
				}
			}
		});
	}
</script>





		