<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
				<div class="column-name">
					<h2>活动管理><b>活动列表</b></h2>
				</div>
				<div class="main-top">
				</div>
				<form id = "form1" name="form1">
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>序号</th>
								<th>开始时间</th>
								<th>结束时间</th>
								<th>手机号码</th>
								<th>创建时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="info">
							<!-- 遍历数据 -->
							<tr>
								<td>${info.id }</td>
								<td>${info.startDate }</td>
								<td>${info.endDate }</td>
								<td>${info.phone }</td>
								<td>${info.createDate }</td>
								<td>
									<a href="<%=basePath%>/businessBasicinfo/getInfoById.html?id=${info.id}" class="">查看详情</a>
								</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
		<div class="popBox chaLianjie-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="chaLianjie-con">
						<div class="tit" id="lookurl"></div>
						<a href="javascript:;" class="btn w144">确  认</a>
					</div>
				</div>
			</div>
		</div><!-- chaLianjie-pop -->
			</form>
		<script type="text/javascript">
				function confirmDelete(ID){
					if (confirm("是否确认")) {
						location.href='<%=basePath%>v2BannerInfo/deleteV2BannerInfo.html?bannerId='+ID;
					}  else  { 
						return false;
					};
					
				}
				function lookUrl(ID){
					$.ajax({
						type : "post",
						url : "<%=basePath%>v2BannerInfo/getV2BannerInfo.html",
						dataType : "json",
						data : {bannerId:ID},
						success : function(data) {
							var $picurl=data.picUrl;
							var $url = $("#lookurl");
							$url.append($picurl);
						},
					});
					
				}
		</script>

				