<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
				<div class="column-name">
					<h2>钱包管理 ><b>钱包管理</b></h2>
				</div>
				<form id = "form1" name="form1" action="<%=basePath%>/managerWallet/list.html">
				<div class="main-top">
					<div class="fr" align="center">
						<span class="c-name">用户名</span> 
						<input type="text" name="name" class="input mr10" value="${paramsMap.name }"placeholder="请输入用户名">
						<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
					</div>
				</div>
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>推送用户</th>
								<th>接收用户</th>
								<th>手机号</th>
								<th>邮箱</th>
								<th>微信号</th>
								<th>状态</th>
								<th>创建时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="manage">
							<!-- 遍历数据 -->
							<tr>
								<td>${manage.userName }</td>
								<td>${manage.managerName }</td>
								<td>${manage.phone }</td>
								<td>${manage.email }</td>
								<td>${manage.wx_number }</td>
								<td>
									<c:choose>
										<c:when test="${manage.isAgree == '0' }">
											未操作
										</c:when>
										<c:when test="${manage.isAgree == '1'}">
											用户同意
										</c:when>
										<c:when test="${manage.isAgree == '2'}">
											用户拒绝
										</c:when>
										<c:when test="${manage.isAgree == '3'}">
											审核通过
										</c:when>
										<c:when test="${manage.isAgree == '4'}">
											审核不通过
										</c:when>
									</c:choose>
								</td>
								<td>${manage.createDate }</td>
								<td>
									<c:choose>
										<c:when test="${manage.isAgree == '0' }">
											<a href="<%=basePath%>/managechange/getManageById.html?id=${manage.id}"class="">审核</a>
										</c:when>
										<c:otherwise>
											审核
										</c:otherwise>
									</c:choose>
								</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
	</form>