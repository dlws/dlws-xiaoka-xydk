<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>更换管理员管理> <b>更换管理员审核</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>managechange/audit.html" id="form111" method="post" >
						<input id="id" type="hidden" name="id" value="${manage.id }">
						<div class="form-item">
							<div class="form-name">推送用户:</div>
							<div class="form-input">
								<c:choose>
									<c:when test="${manage.userName != null }">
										<input name="name" type="text" readonly="readonly" class="input" value="${manage.userName }">
										<input name="userId" type="hidden" value="${manage.userId }">
										<input name="basicId" type="hidden" value="${manage.basicId }">
									</c:when>
									<c:otherwise>
										<input name="name" type="text" readonly="readonly" class="input" value="匿名">
									</c:otherwise>
								</c:choose>
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">接收用户:</div>
							<div class="form-input">
								<input name="withdrawMoney" type="text" readonly="readonly" class="input" value="${manage.managerName }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">微信号:</div>
							<div class="form-input">
								<input name="bank" type="text" readonly="readonly" class="input" value="${manage.wx_number }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">邮箱:</div>
							<div class="form-input">
								<input name="bank" type="text" readonly="readonly" class="input" value="${manage.email }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">电话:</div>
							<div class="form-input">
								<input name="bankCardNumber" type="text" readonly="readonly" class="input" value="${manage.phone }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">创建时间:</div>
							<div class="form-input">
								<input name="createTime" type="text" readonly="readonly" class="input" value="<fmt:formatDate  value='${manage.createDate }' type='both'/>">
							</div>
						</div>
						<div class="form-item">
							<div class="form-input">
							<input type="radio" name="radio" class="input" checked="checked" value="1">通过 </br>
							<input type="radio" name="radio" class="input" value="2">拒绝
							</div>
					   </div>
						<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="审  核" onclick="audit()">
							<input type="button" class="btn w144" value="取  消 " onclick="javascript:history.back(-1);">
						</div>
						</form>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script>
	function audit(){
		$("#form111").submit();
	}
</script>
</div>
