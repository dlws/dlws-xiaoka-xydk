
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
       function goSearch(){
       		$("#form1").submit();
       }
       function toSend(id){
    	   var r=confirm("是否要推送此消息？")
    	   if (r==true){
    		   window.location.href="${path}/messagePush/sendMessinfo.html?id="+id;
    	   }
       }
</script>
	<div class="column-name">
		<h2>
			 运营维护><b>群发管理</b>
		</h2>
	</div>
<form id="form1" name="form1" action="${path}/messagePush/toShowMess.html" method="post">

	<div class="main-top">
		<div class="fl">
			<a href="${path}/messagePush/toAddMessinfo.html?start=${start}" class="btn btn-p">添加消息</a>
		</div>
		<div class="fr" align="center">
		
			<span class="c-name">推送标题</span>
			<input type="text" name="title" class="input mr10" value="${paramsMap.title}" placeholder="请输入服务名称关键字"> 
			<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
				    <th>序号</th>
				    <th>标题</th>
				    <th>内容</th>
				    <th>发送时间</th>
				    <th>备注</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="data" varStatus="bs">
				<tr>
							<!-- 暂定为不显示入驻类型 -->
							<td>${bs.count}</td>
							<td>${data.title}</td>
							<td>
								<center>
								<div style="width:200px;text-overflow:ellipsis;white-space: nowrap;overflow: hidden;">
									${data.content}
								</div>
								</center>
							</td>
							<td>
								<fmt:formatDate  value="${data.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss" />
							</td>
							<td>
								<center>
									<div style="width:200px;text-overflow:ellipsis;white-space: nowrap;overflow: hidden;">
										${data.remark}
									</div>
								</center>
							</td>
							<td>
								<!-- 修改--> 
								<a class="" href="${path}/messagePush/toUpdateMessinfo.html?id=${data.id}">修改</a>
								<!-- 是否删除判断 -->
								<a href="javascript:;" data-id="${path}/messagePush/deleteMessinfo.html?id=${data.id}" class="remove">删除</a>
								<!-- 判断当前消息是否已经群发过 -->
								<c:choose>
									<c:when test="${data.isSend eq '1'}">
										<%-- <a   href ="${path}/messagePush/sendMessinfo.html?id=${data.id}">未推送</a> --%>
										<a href ="javascript:void(0)" onclick="toSend('${data.id}')">未推送</a>
									</c:when>
									<c:otherwise>
										<a href="javaScript:alert('该消息已经推送')">已推送</a>
									</c:otherwise>
								</c:choose>
								
							</td>
				</tr>	
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a>
					<a href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
