<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>信息管理类</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
</head>
<body>
	
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>群发管理 > <b>添加群发服务</b></h2>
				</div>
				<div class="form">
					<form id="sub" action="${path}/messagePush/addMessinfo.html" method="post">
						<input type="hidden" name="start" value=""> 
						<div class="form-item">
							<div class="form-name">群发标题：</div>
							<div class="form-input" >
								<input type="text" class="input" name="title" style="width:668px;" maxlength="45" placeholder="请输入标题">
								<div class="clearfix"></div>
								<div class="form-error red"></div>
							</div>
						</div>
				
						<div class="form-item">
							<div class="form-name">群发内容：</div>
							<div class="form-input" style="margin-top: 24px">
								<textarea name="content" style="margin: 0px; width: 688px; height: 106px;" placeholder="请输入内容" maxlength="250"></textarea>
								<div class="clearfix"></div>
								<div class="form-error red"></div>
							</div>
						</div>
				
						<div class="form-item">
							<div class="form-name">备&nbsp;&nbsp;注：</div>
							<div class="form-input" style="margin-top: 24px">
								<textarea name="remark" style="margin: 0px; width: 688px; height: 106px;" placeholder="请输入备注" maxlength="250"></textarea>
								<div class="clearfix"></div>
								<div class="form-error red"></div>
							</div>
						</div>
					</form>
					  <button type="submit" onclick="subform()" class="btn w144">保  存</button>
				</div>
			</div>
		</div>
	</div>
</body>
 <script type="text/javascript">
 
	 function subform(){
	   var form1=document.getElementById("sub");
	   //增加参数校验  1.非空 2.长度 
	   
	   
	   
	   
	   form1.submit();
	 }
 </script>
</html>