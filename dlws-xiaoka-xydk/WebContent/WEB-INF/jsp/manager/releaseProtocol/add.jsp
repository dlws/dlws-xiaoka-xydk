<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>添加协议</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
	
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>协议管理 > <b>添加协议</b></h2>
				</div>
				<div class="form">
					<form id="income" name="incomeform" action="${path}/releaseProtocol/addReleaseProtocol.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
						
								<div class="form-item">
									<div class="form-name">协议类型</div>
										<div class="form-input">
											<select class="input" name="classId" id="classId">
												<c:forEach items="${releaseTypes }" var="info">
													<option value="${info.id}">${info.dic_name}</option>
												</c:forEach>							
											</select> 
										</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">标题：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="title" name="title" value="" >
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">发布内容：</div>
									<div class="form-input">
										<!-- <input AutoComplete="off" type="text" class="input" id="content" name="content" value="" > -->
										<textarea name="content" id="content" class="input"></textarea>
									</div>
								</div>
						
						
					<button type="button" class="btn w144" onclick="addBanner()">保  存</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
<script type="text/javascript">

function addBanner(){
	
	var flag = true;
	var title = $.trim($("#title").val());
	var content = $.trim($("#content").val());
	if(title==""){
		alert("标题不为空!");
		flag = false;
	}
	if(content==""){
		alert("发布内容不为空!");
		flag = false;
	}
	
	if(flag){
		$("#income").submit();
	}
}

	KE.show({
				id : 'content',
				height:'500px',
				imageUploadJson : '${path}/tggoods/uploadDescImage.html',
				allowFileManager : true,
				afterCreate : function(id) {
					KE.event.ctrl(document, 13, function() {
						KE.util.setData(id);
					});
				}
			});

</script>
