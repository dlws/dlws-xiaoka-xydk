<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script type="text/javascript">
       function bb(){
       		$("#form1").submit();
       }
</script>
				<div class="column-name">
					<h2>技能管理 ><b>技能列表</b></h2>
				</div>
				<div class="main-top">
					<div class="fl">
						<a href="<%=basePath%>userBasicinfo/toAddUserBasicinfo.html" class="btn btn-p">新建用户</a>
					</div>
				</div>
				<form id = "form1" name="form1" action = "<%=basePath%>/userBasicinfo/userBasicinfoList.html">
				
				<div class="main-top">
			<div class="form-item">
							<div class="check-item">
								<span class="check-tit">名称</span>
								<input type="text"  value="${userName1}" class="input" name="userName"/>
							</div>
						<input type="button" onclick="bb()" class="btn w96" value="查  询" style="float:right">
			</div>
			
	 </div>
	 
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>名称</th>
								<th>学校</th>
								<th>电话</th>
								<th>邮箱</th>
								<th>是否审核</th>
								<!-- <th>审核时间</th> -->
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="info">
							<!-- 遍历数据 -->
							<tr>
								<td>${info.userName }</td>
								<td>${info.schoolName }</td>
								<td>${info.phoneNumber }</td>
								<td>${info.email }</td>
								<c:if test="${info.auditState==1}">
									<td>已审核</td>
								</c:if>
								<c:if test="${info.auditState!=1}">
									<td>未审核</td>
								</c:if>
								<%-- <td>${info.auditDate }</td> --%>
								<td>
									<a href="<%=basePath%>/userBasicinfo/toUpdateUserBasicinfo.html?id=${info.id}&temp='update'" class="">修改</a>
									<a href="<%=basePath%>/userBasicinfo/deleteUserBasicinfo.html?id=${info.id}&isDelete=1" class="">删除</a>
									<a href="<%=basePath%>/skill/list.html?basicId=${info.id}" class="">技能设置</a>
								</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
		<div class="popBox chaLianjie-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="chaLianjie-con">
						<div class="tit" id="lookurl"></div>
						<a href="javascript:;" class="btn w144">确  认</a>
					</div>
				</div>
			</div>
		</div><!-- chaLianjie-pop -->
			</form>
		<script type="text/javascript">
				function confirmDelete(ID){
					if (confirm("是否确认")) {
						location.href='<%=basePath%>v2BannerInfo/deleteV2BannerInfo.html?bannerId='+ID;
					}  else  { 
						return false;
					};
					
				}
				function lookUrl(ID){
					$.ajax({
						type : "post",
						url : "<%=basePath%>v2BannerInfo/getV2BannerInfo.html",
						dataType : "json",
						data : {bannerId:ID},
						success : function(data) {
							var $picurl=data.picUrl;
							var $url = $("#lookurl");
							$url.append($picurl);
						},
					});
					
				}
		</script>

				