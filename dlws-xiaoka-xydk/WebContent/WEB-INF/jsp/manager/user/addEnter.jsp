<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	<style type="text/css">
		.custom{
			position:relative; left:120px; 
			margin: 1% 1% 0% 0%;
		}
	</style>
	<body>
	<div class="column-name">
		<h2>用户入驻管理> <b>场地修改</b></h2>
	</div>
	<div class="form1" name="form1">
		<form action="<%=basePath%>enter/addUserByEnter.html" id="bannerForm" method="post" enctype="multipart/form-data">
		<input type="hidden" value="${paramsMap.enterId }" name="enterId">
		<div class="form-item">
				<div class="form-name">选择省:</div>
					<div class="form-input">
						<select class="input" name="proId" id="proId" onchange="queryCityByPro(this.value);">
							<c:forEach items="${provnceList }" var="provc">
								<c:choose>
									<c:when test="${provc.province == proMap.province}">
										<option value="${provc.province}" selected = "selected">${provc.province}</option>
									</c:when>
									<c:otherwise>
										<option value="${provc.province}">${provc.province}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select> 
					</div>
		 </div>
		 <div class="form-item">
				<div class="form-name">选择城市:</div>
					<div class="form-input">
						<select class="input" name="cityId" id="cityId" onchange="queryScholTypeByCity(this.value);">
							<!-- <option value="">全部</option> -->
							<c:forEach items="${cityList }" var="city">
								<c:choose>
									<c:when test="${city.id == objMap.citycode}">
										<option value="${city.id}" selected = "selected">${city.cityname }</option>
									</c:when>
									<c:otherwise>
										<option value="${city.id}">${city.cityname}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select> 
					</div>
			</div> 
			<div class="form-item">
				<div class="form-name">选择院校类别:</div>
					<div class="form-input">
						<select class="input" name="scholType" id="scholType" onchange="queryScholByType(this.value);">
							<!-- <option value="">全部</option> -->
							<c:forEach items="${scholTypeList }" var="stp">
									<option value="${stp.value}">${stp.text}</option>
							</c:forEach>
						</select> 
				</div>
			</div>
			
			<div class="form-item">
								<div class="form-name">学校:</div>
								<div class="form-input">
									<select name="schoolId" class="input" id="schools">
										 <option value="${objMap.schoolId}">${objMap.schoolName }</option>
									</select>
								</div>
			</div>
			<div class="form-item">
				<div class="form-name">入驻名称:</div>
				<div class="form-input">
					<input name="userName"  id="userName" type="text" class="input" value="${objMap.userName }" maxlength="10">
					<input name="nickname"  id="nickname" type="hidden">
				</div>
			</div>
			<div class="form-item form-item01">
							<div class="form-name">入驻头像:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value=""/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${objMap.headPortrait}"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadhead()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl" width="200" height="200" src="${objMap.headPortrait}">
    							</div>
							</div>
			</div>
			<div class="form-item" style="margin-left: 95px;">
				<div class="form-name">场地类型:
					<select name="fieldType">
						<c:forEach items="${fieldTypeList}" var="fieldType">
							<c:choose>
							 <c:when test="${fieldType.dic_value==echoMap.fieldType}"> 
								<option value="${fieldType.dic_value}" selected = "selected">${fieldType.dic_name}</option>
							 </c:when> 
							<c:otherwise>
								<option value="${fieldType.dic_value}">${fieldType.dic_name}</option>
						    </c:otherwise> 
						    </c:choose>
						</c:forEach>
  					</select>
  				</div>
			</div>
			<div class="form-item" style="margin-left: 95px;">
				<div class="form-name" style="width:242px;">场地规模:
					<select name="filedSize">
						<c:forEach items="${filedSizeList}" var="filedSize">
								<option value="${filedSize.dic_value}">${filedSize.dic_name}</option>
						</c:forEach>
  					</select>
  				</div>
			</div>
			<!-- 此处应循环读取数据库 -->
			<div class="custom">
					<span>基本物资配备(可选)：</span>
					<div class="custom">
						<table >
							<tr>
								<c:forEach items="${materialList}" var="material">
									<td style="border-width: 1px 1px 2px 3px;">
											<input type="checkbox" value="${material.dic_value}" name="material">${material.dic_name}
									</td>
								</c:forEach>
							</tr>
						</table>
					</div>
				
					<br>
					使用时间:
					<div class="custom">
							<span>开始时间:</span>
								<input id="begin" name="begin" value="${echoMap.begin}" class="Wdate" type="text" onFocus="WdatePicker({dateFmt:'H:00'})"/>
							<span>结束时间:</span>
								<input id="end" name="end" value="${echoMap.end}" class="Wdate" type="text" onFocus="WdatePicker({dateFmt:'H:00'})"/>
					</div>
					<br>
					场地照片(每种类型最少上传两张图片):<br>
					<div class="form-item form-up">
							<div class="form-name">近景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picSmall" id="close" multiple size="80" class="btn-blue" onchange="uploadPic(1)"/> 
								</div>
								<ul id="ul1">
									
								</ul>
							</div>
					</div>
					<div class="form-item form-up">
							<div class="form-name">中景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picMiddle" id="middle" multiple size="80" class="btn-blue" onchange="uploadPic(2)"/> 
								</div>
								<ul id="ul2" >
									
								</ul>
							</div>
					</div>
					<div class="form-item form-up">
							<div class="form-name">全景：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="picAll" id="all" multiple size="80" class="btn-blue" onchange="uploadPic(3)"/> 
								</div>
								<ul id="ul3">
									
								</ul>
							</div>
					</div>
					<div>
						是否可提供兼职人员： 是：<input type="radio" name="partimejob" value="1" checked="checked">
										 否：<input type="radio" name="partimejob" value="0"><br/><br>
						<span>场地描述：</span>
							<textarea style="width:400px;height:200px;" id="currentDec" name="aboutMe" placeholder="场地描述将在20字以内200字以下"></textarea>
						<br>
					</div>
		</div>
			<div class="form-item">
						<div class="form-name">场地报价:</div>
						<div class="form-input">
							<input name="planprice" type="number" class="input">
						</div>
			</div>
			<div class="form-item">
				<div class="form-name">联系人:</div>
				<div class="form-input">
					<input name="contactUser" type="text" placeholder="联系人姓名" class="input" id="contactUser" maxlength="10">
					<div class="clearfix"></div>
					<div class="form-msg"></div>
				</div>
			</div>
			<div class="form-item">
				<div class="form-name">联系电话:</div>
				<div class="form-input">
					<input name="phoneNumber" placeholder="请输入电话号码" id="phoneNumber" type="tel" class="input" value="${objMap.phoneNumber}"  placeholder="请输入11位电话号码">
					<div class="clearfix"></div>
					<div class="form-error red"></div>
				</div>
			</div>
			
			<div class="form-item">
				<div class="form-name">邮箱:</div>
				<div class="form-input">
					<input type="email" placeholder="请输入邮箱" name="email" id="email"  class="input" value="${objMap.email }">
				</div>
			</div>
			<div class="form-item">
				<div class="form-name">微信号:</div>
				<div class="form-input">
					<input name="wxNumber" placeholder="请输入您的微信号" id="wxNumber" type="text" class="input" value="${objMap.wxNumber }">
				</div>
			</div>
			<div class="form-item">
				<div class="form-name">访问量:</div>
				<div class="form-input">
					<input name="viewNum" placeholder="请输入初始访问量" id="viewNum" type="number" class="input" value="${objMap.viewNum}">
				</div>
			</div>
			<br/>
			<div style="text-align:center; vertical-align:middel;">
				<input type="button" class="btn w144" value="保    存" onclick="addBanner()">
			</div>
				<input type="hidden" name="cityIds" id="cityIds" value="">
			</form>
			<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
	<script>
		function addBanner(){
			var contactUser = $("#contactUser").val();
			if(contactUser==""){
				alert("请输入联系人");
				return
			}
			var userName = $("#userName").val();
			if(userName==""){
				alert("请输入入驻名称");
				return
			}
			var phoneNumber=$("#phoneNumber").val()
   			var re=/^1[3|5|7|8]\d{9}$/;
	   		if(phoneNumber==""){
	   			alert("请输入手机号");
	   			return;
	   		}else if(!re.test(phoneNumber)){
	   			alert("请输入正确的手机号");
	   			return;
	   		}
	   		var email=$("#email").val()
	   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		
	   		if(email==""||!regEma.test(email)){
	   			alert("邮箱格式有误或不能为空");
	   			return
	   		}
	   		var wxNumber=$("#wxNumber").val()
	   		var regWx=/^[a-zA-Z\d_]{5,}$/;
	   		if(wxNumber==""||!regWx.test(wxNumber)){
		   		alert("微信号格式有误或不能为空");
		   		return
	   		}
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
			var picUrl = $.trim($("#picUrl").val());
			
			var schools = $("#schools").val();
			if(schools==""||schools==0){
				alert("请选择学校！");
				return
			}
			var aboutMe = $("#currentDec").val();
			if(aboutMe==""){
				alert("请输入场地介绍");
				return;				
			}
			if(aboutMe.length<20){
				alert("场地描述不能少于20字");
				return;
			}
			if(aboutMe.length>200){
				alert("场地描述不能大于200字");
				return;
			}
			if(picUrl==""){
				alert("用户头像不能为空！");
				return
			}
			if($("#ul1 li").length<2||$("#ul2 li").length<2||$("#ul3 li").length<2){
				alert("每种 最少上传两张(近景 中景 远景)");
				return
			}
			if($("#ul1 li").length>9||$("#ul2 li").length>9||$("#ul3 li").length>9){
				alert("每种 最多上传九张(近景 中景 远景)");
				return
			}
			if($("#begin").val()==""||$("#end").val()==""){
				alert("开始时间，或者结束时间不能为空");
				return
			}
			if(parseInt($("#begin").val())>parseInt($("#end").val())){
				alert("场地开始使用时间不能大于场地结束使用时间");
				return
			}
			if($("#planprice").val()==""){
				alert("场地报价不能为空");
				return
			}
			$("#bannerForm").submit();
		}
		//上传图片
		function uploadPic(obj){
			var ulId="";
			var url="";
			var name=""
			if(obj==1){
				url="pictureSmall";
				ulId ="ul1";
				name = "closePhoto";
			}
			else if(obj==2){
				url="pictureMiddle";
				ulId ="ul2";
				name = "middlePho";
			}else{
				url="pictureAll";
				ulId ="ul3";
				name = "allPhoto";
			}
			var picListHtml ="";
			var options = {
					url : '<%=basePath %>enter/'+url+'.html',
					dataType : "json",
					type : "post",
					success : function(data){
						
						for(var i=0;i<data.length;i++){
							
							picListHtml += '<li><div class="txt"><span>预览:</span><a href="javascript:void(0);" onclick=deleteDetailImage("'+data[i]+'","'+name+'") class="remove">删除</a></div><div class="img"><img src="'+data[i]+'" width="200" height="200" alt=""></div></li>'+
							'<input type="hidden"  name="'+name+'" value='+data[i]+'>';
						}
						
						$("#"+ulId).append(picListHtml);
					}
			}
			$("#bannerForm").ajaxSubmit(options); 
		}
		//根据省查出市
		function queryCityByPro(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryCityByPro.html",
				dataType : "json",
				data : {proId:value},
				success : function(data) {
					 $("#cityId option").remove();
					 $("#scholType option").remove();
					 $("#schools option").remove();
					var $sel = $("#cityId"); 
					if(data.cityList.length>0){
						 $.each(data.cityList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholTypeByCity(data.cityList[0].value);
					}
				}
			});	
			
		}
		
		//根据市查出院校类别
		 function queryScholTypeByCity(value){
			 $("#scholType option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#scholType").append($option0)
			 $("#schools option").remove();
			  var $option1 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option1)
			$("#cityIds").val(value);
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholTypeByCity.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $sel = $("#scholType");
					if(data.scholTypeList.length>0){
						$.each(data.scholTypeList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholByType(data.scholTypeList[0].value);//根据院校类别查出院校
					}
					
				}
			});	
		}
		//根据院校类别查出院校
		 function queryScholByType(value){
			 $("#schools option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option0)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholByType.html",
				dataType : "json",
				data : {dcdaId:value,cityId:$("#cityIds").val()},
				success : function(data) {
					var $sel = $("#schools");
					if(data.scholList.length>0){
						$.each(data.scholList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		}  
		//用户上传头像
		function uploadhead(){
			//异步上传
			var options = {
					url : "<%=basePath %>bannerInfo/picture.html",
					dataType : "json",
					type : "post",
					success : function(data){
						$("#allUrl").attr("src",data.picUrl);
						$("#keyValue").val(data.key);
						$("#picUrl").val(data.picUrl);
					}
			}
			$("#bannerForm").ajaxSubmit(options);
		}
		
		function deleteDetailImage(imgUrl,name){
			var imgLi = $("img[src='"+imgUrl+"']").parent().parent();
			var imgInput = $("input[name='"+name+"'][value='"+imgUrl+"']");
			console.log(imgInput.prop("outerHTML"));
			//删除图片，父li移除，删除input
			console.log($("input[name=imgUrl]").size());
			imgLi.remove();
			imgInput.remove();
			alert("删除成功！");
			console.log($("input[name=imgUrl]").size());
			return true;
		}
	</script>
</div>
</body>