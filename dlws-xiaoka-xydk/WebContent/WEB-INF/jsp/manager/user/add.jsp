<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>基本信管理> <b>基本信添加</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>userBasicinfo/addUserBasicinfo.html" id="bannerForm" method="post" enctype="multipart/form-data">
						<div class="form-item">
							<div class="form-name">名称:</div>
							<div class="form-input">
								<input name="userName" type="text" class="input" id="userName" value="${user.userName }">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">电话:</div>
							<div class="form-input">
								<input name="phoneNumber" id="phoneNumber" type="text" class="input" value="${user.phoneNumber }"  placeholder="请输入11位电话号码">
								<div class="clearfix"></div>
								<div class="form-error red"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">邮箱:</div>
							<div class="form-input">
								<input name="email" id="email" type="text" class="input" value="${user.email }">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">微信号:</div>
							<div class="form-input">
								<input name="wxNumber" id="wxNumber" type="text" class="input" value="${user.wxNumber }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">入驻类型:</div>
							<div class="form-input">
								<select class="input" name="checkType" id="checkType">
										<option value="0" >个人</option>
										<option value="1">团队</option>
								</select> 
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">访问量:</div>
							<div class="form-input">
								<input name="viewNum" id="viewNum" type="text" class="input">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">选择省:</div>
								<div class="form-input">
									<select class="input" name="proId" id="proId" onchange="queryCityByPro(this.value);">
										<!-- <option value="">全部</option> -->
										<c:forEach items="${provnceList }" var="provc">
												<option value="${provc.province}">${provc.province }</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						 <div class="form-item">
							<div class="form-name">选择城市:</div>
								<div class="form-input">
									<select class="input" name="cityId" id="cityId" onchange="queryScholTypeByCity(this.value);">
										<!-- <option value="">全部</option> -->
										<c:forEach items="${cityList }" var="city">
											<c:if test="${city.id == objMap.citycode }">
												<option value="${city.id}" selected = "selected">${city.cityname }</option>
											</c:if>
											<c:if test="${city.id != objMap.citycode }">
												<option value="${city.id}">${city.cityname }</option>
											</c:if>
										</c:forEach>
									</select> 
								</div>
						</div> 
						
						<div class="form-item">
							<div class="form-name">选择院校类别:</div>
								<div class="form-input">
									<select class="input" name="scholType" id="scholType" onchange="queryScholByType(this.value);">
										<!-- <option value="">全部</option> -->
										<c:forEach items="${scholTypeList }" var="stp">
												<option value="${stp.value}">${stp.text}</option>
										</c:forEach>
									</select> 
								</div>
						</div>
						
						
						<div class="form-item">
								<div class="form-name">学校:</div>
								<div class="form-input">
									<select name="schoolId" class="input" id="schools">
										 <option value="${objMap.schoolId}">${objMap.schoolName }</option>
									</select>
								</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">昵称:</div>
							<div class="form-input">
								<input name="nickname" id="nickname" type="text" class="input" value="${user.nickname }">
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">简介:</div>
							<div class="form-input">
								<input name="aboutMe" id="aboutMe" type="text" class="input" value="${user.aboutMe }">
							</div>
						</div>
						
						<div class="form-item form-item01">
							<div class="form-name">头像:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value="${key }"/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${picUrl }"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl"  src="${picUrl}">
    							</div>
							</div>
						</div>
						
						<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="addBanner()">
						</div>
						<input type="hidden" name="cityIds" id="cityIds" value="">
						</form>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script>
		function addBanner(){
			var userName = $("#userName").val();
			if(userName==""){
				alert("请输入姓名");
				return
			}
			var phoneNumber=$("#phoneNumber").val()
   			var re=/^1[3|5|8]\d{9}$/;
	   		if(phoneNumber==""){
	   			alert("请输入手机号");
	   			return;
	   		}else if(!re.test(phoneNumber)){
	   			alert("请输入正确的手机号");
	   			return;
	   		}
	   		
	   		var email=$("#email").val()
	   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	   		if(email!=""){
		   		if(!regEma.test(email)){
		   			alert("邮箱格式有误");
		   			return
		   		}
	   		}
	   		
	   		var wxNumber=$("#wxNumber").val()
	   		var regWx=/^[a-zA-Z\d_]{5,}$/;
	   		
	   		if(wxNumber!=""){
		   		if(!regWx.test(wxNumber)){
		   			alert("微信号格式有误");
		   			return
		   		}
	   		}
	   		
	   		var viewNum = $("#viewNum").val();
	   		var vieNumReg = /^[0-9]*$/;
	   		if(viewNum==""){
	   			$("#viewNum").val(0);
	   		}else if(!vieNumReg.test(viewNum)){
	   			 alert("访问量必须是数字");
	   			 return;
	   		 }
			var picUrl = $.trim($("#picUrl").val());
		
			var schools = $("#schools").val();
			if(schools==""||schools==0){
				alert("请选择学校！");
				return
			}
			
			var aboutMe = $("#aboutMe").val();
			if(aboutMe==""){
				alert("请输入简介");
				return;				
			}
			
			if(picUrl==""){
				alert("上传图片不能为空！");
				return
			}
			
			$("#bannerForm").submit();
		}
		//上传图片
		function uploadPic(){
			//异步上传
			var options = {
					url : "<%=basePath %>bannerInfo/picture.html",
					dataType : "json",
					type : "post",
					success : function(data){
						$("#allUrl").attr("src",data.picUrl);
						$("#keyValue").val(data.key);
						$("#picUrl").val(data.picUrl);
					}
			}
			$("#bannerForm").ajaxSubmit(options);
		}
		
		//根据省查出市
		function queryCityByPro(value){
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryCityByPro.html",
				dataType : "json",
				data : {proId:value},
				success : function(data) {
					 $("#cityId option").remove();
					 $("#scholType option").remove();
					 $("#schools option").remove();
					var $sel = $("#cityId");
					if(data.cityList.length>0){
						 $.each(data.cityList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholTypeByCity(data.cityList[0].value);
					}
				}
			});	
			
		}
		
		//根据市查出院校类别
		 function queryScholTypeByCity(value){
			 $("#scholType option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#scholType").append($option0)
			 
			  $("#schools option").remove();
			  var $option1 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option1)
			$("#cityIds").val(value);
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholTypeByCity.html",
				dataType : "json",
				data : {cityId:value},
				success : function(data) {
					var $sel = $("#scholType");
					if(data.scholTypeList.length>0){
						$.each(data.scholTypeList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
						queryScholByType(data.scholTypeList[0].value);//根据院校类别查出院校
					}
					
				}
			});	
		}
		//根据院校类别查出院校
		 function queryScholByType(value){
			 $("#schools option").remove();
			 var $option0 = $('<option></option>').val(0).text("请选择");
			 $("#schools").append($option0)
			$.ajax({
				type : "post",
				url : "<%=basePath %>userBasicinfo/queryScholByType.html",
				dataType : "json",
				data : {dcdaId:value,cityId:$("#cityIds").val()},
				success : function(data) {
					var $sel = $("#schools");
					if(data.scholList.length>0){
						$.each(data.scholList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		}  
</script>
</div>
