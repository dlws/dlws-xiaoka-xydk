<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>校咖微平台后台管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>发票管理 > <b>查看详情</b></h2>
				</div>
				<div class="form">
					<form id="editPlatGoods" action="" method="post">
						<div class="form-item">
							<div class="form-name">发票类型：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="typeValue" value = "${po.typeValue }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">发票抬头：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="sign" value = "${po.sign}" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">使用分类：</div>
							<div class="form-input">
							<c:if test="${po.signTypeId==0 }">
								<input type="text" class="input" id="skillName" name="signTypeId" value = "个人" style="border:0px" readonly= "true">							
							</c:if>
							<c:if test="${po.signTypeId==1 }">
								<input type="text" class="input" id="skillName" name="signTypeId" value = "单位" style="border:0px" readonly= "true">							
							</c:if>

								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">单位名称：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="dataName" value = "${po.dataName }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">快递名称：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="expressName" value = "${po.expressName }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">物流公司名称：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="express" value = "${po.express }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">快递单号：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="expressNumber" value = "${po.expressNumber }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">快递价格：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="expressPrice" value = "${po.expressPrice }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">收件人的姓名：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="recipientName" value = "${po.recipientName }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">收件人的电话号码：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="recipientPhone" value = "${po.recipientPhone }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">收件人的地址：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="recipientAdress" value = "${po.recipientAdress }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">开户行全称：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="bankName" value = "${po.bankName }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">支付价格：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="payMoney" value = "${po.payMoney }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">支付状态：</div>
							<div class="form-input">
							<c:if test="${po.payStatus==0 }">
								<input type="text" class="input" id="skillName" name="payStatus" value = "提交未支付" style="border:0px" readonly= "true">
							</c:if>
							<c:if test="${po.payStatus==1 }">
								<input type="text" class="input" id="skillName" name="payStatus" value = "已支付" style="border:0px" readonly= "true">
							</c:if>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">发票类型：</div>
							<div class="form-input">
							<c:if test="${po.invoiceType==0 }">
								<input type="text" class="input" id="skillName" name="invoiceType" value = "普通发票" style="border:0px" readonly= "true">		
							</c:if>
							<c:if test="${po.invoiceType==1 }">
								<input type="text" class="input" id="skillName" name="invoiceType" value = "专用发票" style="border:0px" readonly= "true">		
							</c:if>
								
								<div class="clearfix"></div>
							</div>
						</div>
						
					</form> 
					
				</div>
			</div>
		</div>
	</div>
</body>
</html>