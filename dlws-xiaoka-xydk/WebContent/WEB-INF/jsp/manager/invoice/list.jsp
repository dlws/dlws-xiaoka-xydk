<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/base.css">
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/style.css">
<script src="<%=basePath%>/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="<%=basePath%>/v2/console/js/globle.js"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<div class="column-name">
	<h2>
		发票管理><b>发票管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="<%=basePath%>/managerInvoice/list.html" method="post">
	<div class="main-top">
		<div class="fr" align="center">
			<span class="c-name">名称</span> 
			<input type="text" name="sign" class="input mr10" value="${paramsMap.sign }"placeholder="请输入名称">
			<span class="c-name">收货人姓名</span> 
			<input type="text" name="recipientName" class="input mr10" value="${paramsMap.recipientName }"placeholder="请输入收货人姓名">
			<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>发票抬头</th>
					<th>收货人姓名</th>
					<th>收货人手机号</th>
					<th>收货地址</th>
					<th>创建时间</th>
					<th>审核状态</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="invoice">
					<tr>
						<td>${invoice.sign }</td>
						<td>${invoice.recipientName }</td>
						<td>${invoice.recipientPhone }</td>
						<td>${invoice.recipientAdress }</td>
						<td><fmt:formatDate value="${invoice.createDate }" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td>
							<c:if test="${invoice.auditType == 0 }">未审核</c:if>
							<c:if test="${invoice.auditType == 1 }">审核成功</c:if>
							<c:if test="${invoice.auditType == 2 }">审核失败</c:if>
						</td>
						<td>
							<c:if test="${invoice.auditType == 0 }">
								<a href="javascript:getInvoiceDetail(${invoice.id })">审核</a>
							</c:if>
						<a href="${path}/managerInvoice/details.html?id=${invoice.id }">查看详情</a>	
						</td>
					</tr>
				</c:forEach>
				
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>
<!--  ====================================================  -->
<div class="popBox chaXiangqing-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-title">
			<h3>发票详情</h3>
			<a href="javascript:;" class="close"></a>
		</div>
		<div class="popBox-con">
			<div class="chaXiangqing-con">
					<div class="chaXiangqing-item" id="dicName"></div>
					<div class="chaXiangqing-item" id="sign"></div>
					<div class="chaXiangqing-item" id="expressName"></div>
					<div class="chaXiangqing-item" id="expressPrice"></div>
					<div class="chaXiangqing-item" id="recipientName"></div>
					<div class="chaXiangqing-item" id="recipientPhone"></div>
					<div class="chaXiangqing-item" id="recipientAdress"></div>
					<div class="chaXiangqing-item" id="payStatus"></div>
					<div class="chaXiangqing-item" id="payMoney"></div>
					<div class="chaXiangqing-title">
					<div class="fl">订单明细： </div>
					<!-- <div class="fr" id="payMoney"></div> -->
					</div>
				<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th>订单编号</th>
							<th>用户名称</th>
							<th>联系电话</th>
							<th>金额（元）</th>
						</tr>
					</thead>
					<tbody id="orders">
					</tbody>
				</table>
				<div class="chaXiangqing-item">
						物流名称：<input name="express" id="express"  type="text" class="input" ></input>
				</div>
				<div class="chaXiangqing-item">
						物流单号：<input id="expressNumber" type="text" class="input" >
				</div>
				<div class="chaXiangqing-item">审核状态：
						<select name="auditType" id="auditType" class="input">
							<option value="1">成功</option>
							<option value="2">失败</option>
						</select>
				</div>
				<div class="chaXiangqing-item">
						失败原因：<textarea name="auditReson" id="auditReson" ></textarea>
				</div>
				<input type="hidden" id="invoiceId" name="invoiceId" value="">
				<div class="btns">
					<a href="javascript:audit();" class="btn w144">审核</a> 
					<a href="javascript:;" class="btn-gray w144">返回</a> 
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	function audit(){
		var invoiceId = $("#invoiceId").val();
		var auditType = $('#auditType option:selected').val()
		var auditReson = $("#auditReson").val();
		
		if(auditType == 2){//审核失败
			if(auditReson==undefined || auditReson=="" || auditReson==null){  
				alert("请填写审核原因。");
			}else{
				$.ajax({
					type : "post",
					url : "${path}/managerInvoice/audit.html",
					dataType : "json",
					async : true,//异步
					data : {invoiceId:invoiceId,auditType:auditType,auditReason:auditReson},
					success : function(data) {
						if(data.ajax_status == "ajax_status_success"){
							window.location.reload();
						}
						
						if(data.ajax_status == "ajax_status_failure"){
							alert("审核失败!");
						}
					},
					error : function(){
					  //通常情况下textStatus和errorThrown只有其中一个包含信息
				   } 
				});	
			}
		}else{//审核成功
			var express = $("#express").val();
			var expressNumber = $("#expressNumber").val();
			if(expressNumber==undefined || expressNumber=="" || expressNumber==null){  
				alert("请填写物流单号。");
			}
			if(express==undefined || express=="" || express==null){  
				alert("请填写物流公司名称。");
			}else{
				$.ajax({
					type : "post",
					url : "${path}/managerInvoice/audit.html",
					dataType : "json",
					async : true,//异步
					data : {invoiceId:invoiceId,auditType:auditType,expressNumber:expressNumber,express:express},
					success : function(data) {
						if(data.ajax_status == "ajax_status_success"){
							window.location.reload();
						}
						
						if(data.ajax_status == "ajax_status_failure"){
							alert("审核失败!");
						}
					},
					error : function(){
					  //通常情况下textStatus和errorThrown只有其中一个包含信息
				   } 
				});
			}
		}
		/* */
	}

	function getInvoiceDetail(invoiceId){
		$.ajax({
			type : "post",
			url : "${path}/managerInvoice/getInvoiceDetail.html",
			dataType : "json",
			async : true,//异步
			data : {invoiceId:invoiceId},
			success : function(data) {
				$("#invoiceId").val(data.id);
			
					$("#dicName").html("发票类型："+data.dicName);
					$("#name").html("发票抬头："+data.sign);
					$("#express").html("物流公司："+data.expressName);
					$("#expressName").html("物流名称："+data.expressName);
					$("#expressPrice").html("物流费用："+data.expressPrice);
					$("#expressNumber").html("快递单号："+data.expressName);
					$("#recipientName").html("收件人姓名："+data.recipientName);
					$("#recipientPhone").html("收件人电话："+data.recipientPhone);
					$("#recipientAdress").html("收件人地址："+data.recipientAdress);
					if(data.payStatus == 0){
						$("#payStatus").html("支付状态："+"否");
					}else{
						$("#payStatus").html("支付状态："+"是");
					}
					$("#payMoney").html("支付价格："+data.payMoney);
					var orders = data.orders;
					var orderHtml = "";
					for (var i = 0; i < orders.length; i++) {
						orderHtml += "<tr>";
						orderHtml += "<td>"+orders[i].orderId+"</td>";
						orderHtml += "<td>"+orders[i].linkman+"</td>";
						orderHtml += "<td>"+orders[i].phone+"</td>";
						orderHtml += "<td>"+orders[i].realityMoney+"</td>";
						orderHtml += "</tr>";
					}
					$("#orders").html(orderHtml);
					$(".chaXiangqing-pop").show();
	
				
				if(data.ajax_status == "ajax_status_failure"){
					alert(data.info);
				}
			},
			error : function(){
			  //通常情况下textStatus和errorThrown只有其中一个包含信息
		   }
		});
	}
	
</script>

