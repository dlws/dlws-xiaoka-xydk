<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
				<div class="column-name">
					<h2>资源服务 ><b>社群大V资源</b></h2>
				</div>
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>城市</th>
								<th>学校</th>
								<th>服务名称</th>
								<th>服务性质</th>
								<th>服务价格</th>
								<th>服务介绍</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="association">
							<!-- 遍历数据 -->
							<tr>
								<td>${association.cityName }</td>
								<td>${association.schoolName }</td>
								<td>${association.skillName }</td>
								<c:choose>
									<c:when test="${association.serviceType == 1 }">
										<td>线上服务</td>
									</c:when>
									<c:otherwise>
										<td>线下服务</td>
									</c:otherwise>
								</c:choose>
								<td>${association.skillPrice }</td>
								<td>${association.skillDepict }</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
	</form>