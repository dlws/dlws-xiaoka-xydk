<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>添加income</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
	
</head>
<script type="text/javascript"></script>
<script >
$(function(){
	
	$("#income").submit(function(){
		var isSubmit = true;
		//分两步：
		//第一步做必填字段的校验
		$(this).find("[reg2]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
				isSubmit = false;
				//在jQuery跳出循环不再是break;也部署return;是return false;
				return false;//跳出循环
			}else{
				var inputName = $(this).attr("name");
				if(inputName == "brandName"){
					if(validBrandName(val)){
						$(this).parents(".form-input").find(".form-error").html("<font color='red'>品牌名称已存在</font>");
						isSubmit = false;
						return false;//跳出循环
					}else{
						$(this).parents(".form-input").find(".form-error").html("");
					}
				}else{
					$(this).parents(".form-input").find(".form-error").html("");
				}
			}
		});
		
		$("#income").find("[reg2]").blur(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
			}else{
				$(this).parents(".form-input").find(".form-error").html("");
			}
		});
		
		var imgeUrl = $("#imgeUrl").val();//图片
		if(imgeUrl==""){
			alert("图片不能为空!")
			isSubmit = false;
		}
		return isSubmit;
	}); 
	
})
</script>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>类型管理 > <b>添加类型</b></h2>
				</div>
				<div class="form">
					<form id="income" name="incomeform" action="${path }/entertype/addEntertype.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
						
								<div class="form-item">
									<div class="form-name">类型名称：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="typeName" name="typeName" placeholder="请输入类型名称" reg2="^[a-zA-Z0-9\_\u4e00-\u9fa5]{1,10}$" tip="标题为中英文或数字字符和下划线，长度1-10"/>
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">入驻类型：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="typeValue" name="typeValue" placeholder="请输入入驻类型" reg2="^[a-zA-Z0-9]{1,10}$" tip="入驻类型英文或数字字符，长度1-10" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item form-item01">
									<div class="form-name">类型图片:</div>
									<div class="form-input">
										<button class="btn-blue" style="margin-left: 0;">上传图片</button>
										<input type="hidden" name='keyValue' id='keyValue' class='input' value="${key }" /> 
										<input type="hidden" name='imgeUrl' id='imgeUrl' class='input' value="${imgeUrl }"/> 
										<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()" />
										<div class="clearfix"></div>
										<div class="form-img">
											<img id="allUrl" src="${imgeUrl}">
										</div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">显示顺序：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="ord" name="ord" placeholder="请输入显示顺序" reg2="^[0-9]{1,5}$" tip="显示顺序为数字字符，长度1-5" >
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
					<button type="submit" class="btn w144">保  存</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">

//上传图片
function uploadPic(){
	//异步上传
	var options = {
			url : "${path}/entertype/picture.html",
			dataType : "json",
			type : "post",
			success : function(data){
				//回调 路径  data.path
				$("#allUrl").attr("src",data.imgeUrl);
				$("#keyValue").val(data.key);
				$("#imgeUrl").val(data.imgeUrl);
			}
	}
	$("#income").ajaxSubmit(options);

}

</script>
