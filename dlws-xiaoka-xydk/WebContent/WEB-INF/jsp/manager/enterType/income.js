
/**
 * Entertype
 */
Ext.namespace('com.jzwl.toutiao.income.controller.income');

com.jzwl.toutiao.income.controller.income.list_url = project_path + '/entertype/entertypeList.html';
com.jzwl.toutiao.income.controller.income.add_url = project_path + '/entertype/addEntertype.html';
com.jzwl.toutiao.income.controller.income.edit_url = project_path + '/entertype/updateEntertype.html';
com.jzwl.toutiao.income.controller.income.del_url = project_path + '/entertype/deleteEntertype.html';
com.jzwl.toutiao.income.controller.income.get_url = project_path + '/entertype/getEntertype.html';
com.jzwl.toutiao.income.controller.income.combo_bool_url = project_path + '/dic/getBooleanList.html';

com.jzwl.toutiao.income.controller.income.getMainGrid = function() {

	var isView = true;

	var entertype_grid_st = [ 
	     {name:'id',header:'id',hidden:isView}
		,{name:'id',header:'id',hidden:isView}
		,{
			name:'typeName'
			,header:'类型名称'
		 }
		,{
			name:'typeValue'
			,header:'入驻类型'
		 }
		,{
			name:'imgeUrl'
			,header:'图片路径'
		 }
		,{
			name:'isDelete'
			,header:'是否删除'
		 }
		,{
			name:'ord'
			,header:'顺序'
		 }
		,{
			name:'createDate'
			,header:'createDate'
			,renderer : function(v) {
				return new Date(parseInt(v)).format('Y-m-d H:m:s');
			}	
		}
	];

	var _grid = Ext.getCmp('com.jzwl.toutiao.income.controller.income.mainGrid');
	
	if (_grid == null) {
		_grid = new XGrid({
			id : 'com.jzwl.toutiao.income.controller.income.mainGrid',
			title : 'Entertype管理',
			region : 'center',
			keyField : 'id',
			structure : entertype_grid_st,
			closable : true,
			selectType : 'check',
			url : com.jzwl.toutiao.income.controller.income.list_url,
			createExtBtn : function(_tbar, _grid) {
				var _btn_add = this.createBtn(this, '添加', 'table_add',
						this.addItem);
				var _btn_edit = this.createBtn(this, '修改', 'table_edit',
						this.editItem);
				var _btn_del = this.createBtn(this, '删除', 'table_delete',
						this.myDelete);

				_tbar.add(_btn_add);
				_tbar.add(_btn_edit);
				_tbar.add(_btn_del);
				_tbar.add("->");
				

				_tbar.add(this.createBtn(this, '查询', 'table_query',this.myQuery));

			},
			addItem : function() {
				com.jzwl.toutiao.income.controller.income.addUI(_grid, "add");
			},
			editItem : function() {
				com.jzwl.toutiao.income.controller.income.addUI(_grid, "edit");
			},
			doClick2 : function(_g, _r, _e) {
			},
			myDelete : function(_grid) {
				var url = com.jzwl.toutiao.income.controller.income.del_url;
				_grid.doDelete(_grid, "删除吗", url);
			},
			myQuery : function(_grid) {
				var para = {};

				
				var store = _grid.store;
				store.removeAll(false);
				Ext.apply(store.baseParams,para);
				
				store.load();
				
			}
		});
	}
	
	return _grid;
};

com.jzwl.toutiao.income.controller.income.addUI = function(_grid, action) {

	var my_width = 250;
	var my_height = 50;
	var my_out_width = 440;
	var my_out_hiegth = 480;

	var url = com.jzwl.toutiao.income.controller.income.add_url;
	var record = _grid.getSelectionModel().getSelected();

	if (action == "edit") {
		if (record == null || '' == record) {
			Ext.Msg.alert('提示', '请先选择你要修改的数据');
			return;
		}

		url = com.jzwl.toutiao.income.controller.income.edit_url;
		
		//设置特殊回显
		record.set("createDate",new Date(parseInt(record.get("createDate"))).format('Y-m-d'));


	}

	var _form = new XForm({
		region : 'center',
		layout : "form",
		labelAlign : "right",
		autoScroll : true,
		items : [
		{xtype : 'hidden',fieldLabel : 'id',name : 'id',width : my_width}
		,{xtype:'hidden',fieldLabel:'id',name:'id',width:my_width}
		,{
			xtype:'textfield'
			,name:'typeName'
			,fieldLabel:'类型名称'
			,width:my_width
		 }
		,{
			xtype:'textfield'
			,name:'typeValue'
			,fieldLabel:'入驻类型'
			,width:my_width
		 }
		,{
			xtype:'textfield'
			,name:'imgeUrl'
			,fieldLabel:'图片路径'
			,width:my_width
		 }
		,{
			xtype:'textfield'
			,name:'isDelete'
			,fieldLabel:'是否删除'
			,width:my_width
		 }
		,{
			xtype:'textfield'
			,name:'ord'
			,fieldLabel:'顺序'
			,width:my_width
		 }
		,{
			xtype : 'datefield'
			,name:'createDate'
			,fieldLabel:'createDate'
			,allowBlank : false
			,blankText : '必须填写日期'
			,format : 'Y-m-d'
			,width:my_width
		 }
		],
		doSave : function() {
			
			// --------开始提交------------
			
			// 验证表单
			
			var validFlag = Ext.TAppUtil.checkForm_2(_form, 1);
			
			if (_form.form.isValid() && validFlag > 0) {
				/** 表单提交（推荐） * */
				_form.getForm().submit({
					url : url,
					method : 'POST',
					clientValidation : true,
					success : function(form, action) {
						_form.window.close();
						_grid.getStore().reload();
					},
					failure : function(form, action) {
						Ext.Msg.alert("提示", "操作失败");
					}

				});
				
				
				//ajax提交（推荐） 
//				var obj= new Object();
//				obj = Ext.TAppUtil.getParams4Panel_2(_form,obj);//通过name取值
//					Ext.Ajax.request( {
//						url :  url,
//						method : 'post',
//						success : function(response, opts) {
//							var result = eval('('+response.responseText+')');
//							_form.window.close();
//							Ext.Msg.alert("提示",result.info);
//						},
//						failure : function() {
//							Ext.Msg.alert("提示", "操作失败");
//						},
//						params : {
//							
//							"id" : obj['id'],
//							
//							"typeName" : obj['typeName'],
//							
//							"typeValue" : obj['typeValue'],
//							
//							"imgeUrl" : obj['imgeUrl'],
//							
//							"isDelete" : obj['isDelete'],
//							
//							"ord" : obj['ord'],
//							
//							"createDate" : obj['createDate'],
//							 
//							"x" : 1
//						}
//					});
				 
			}
			
			
			// ------------------
				
		}
	
	
	});
	

	if ("edit" == action) {
		_form.getForm().load({
			waitMsg : '正在加载数据，请稍后...',
			waitTitle : '提示',
			url : com.jzwl.toutiao.income.controller.income.get_url,//此处对应的返回json必须有success和data两个节点
			params : {
				id : record.get("id")//主键为参数
			},
			method : 'POST',
			success :  function(form, action) {
				
				if(null != action.result && null != action.result.data){//服务器返回的data对象
					var bean=action.result.data;
					//在此可以给控件设定自定义值,比如可以将时间戳转为Y-m-d赋给时间控件
					//对于一般的控件，不需要做任何处理
					//_form.getForm().findField('控件name').setValue(bean.id);
				}
				
				
				
			},
			failure : function(form, action) {
				Ext.Msg.alert('提示', '加载数据失败');
			},
		});
	
	}


	var win = new XWindow({
		height : my_out_hiegth,
		width : my_out_width,
		resizable : true,
		layout : 'border',
		items : _form
	});

	Ext.apply(_form, {
		window : win
	});

	if (action == 'edit') {
		win.setTitle("修改");
	} else if (action == 'add') {
		win.setTitle("添加");
	}

	win.show();
};

// include js
/**
<script type="text/javascript" src="自己修改./js/income/income.js"></script>
**/
//menu js
/**

{
id : "com.jzwl.toutiao.income.controller.income.menu",
text : 'Entertype',
handler : function() {
	var panel = com.jzwl.toutiao.income.controller.income.getMainGrid();
	tabpanel.add(panel);
	tabpanel.setActiveTab(panel);
},
icon : Ext.TAppUtil.getImage('table.gif')
}

**/


