
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
       function goSearch(){
       $("#form1").submit();
       
       
       }
</script>
	<div class="column-name">
		<h2>
			类型管理 > <b>类型列表</b>
		</h2>
	</div>
<form id="form1" name="form1" action="${path}/entertype/entertypeList.html" method="post">

	<div class="main-top">
		<div class="fl">
			<a href="${path}/entertype/toAddEntertype.html" class="btn btn-p">新建类型</a>
		</div>
		<div class="fr" align="center">
			<span class="c-name">类型名称</span>
			<input type="text" name="typeName" class="input mr10" value="${paramsMap.typeName }" placeholder="请输入类型名称关键字"> 
			<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
						    <th>类型名称</th>
						    <th>入驻类型</th>
						    <th>图片路径</th>
						    <th>显示顺序</th>
						    <th>创建时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="data">
				<tr>
							<td>${data.typeName}</td>
							<td>${data.typeValue}</td>
							<td><img  height="50" width="100" src="${data.imgeUrl}"></img></td>
							<td>${data.ord}</td>
							<td><fmt:formatDate  value="${data.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				<td>
					<!-- 是否删除判断 -->
					<a href="javascript:;" data-id="${path}/entertype/delEntertype.html?id=${data.id}" class="remove">删除</a> 
					<!-- 修改--> 
					<a class="" href="${path}/entertype/toUpdateEntertype.html?id=${data.id}">修改</a>
				</td>
				</tr>	
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a>
					<a href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
