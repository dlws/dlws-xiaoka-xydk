<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<style>
	    .form1 h2{
	        text-align:center;    
	    }
	    .form-input textarea.input{
	    margin-top:25px;
	    line-height:1.5;
	    }
	    .form-input input{
	      border:0;
	    }
	    form{
	      margin-bottom:30px;
	    }
	</style>
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>订单管理 > <b>订单详情</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>bannerInfo/add.html" id="bannerForm" method="post" enctype="multipart/form-data">
						<h2>买家端</h2>
						<div class="form-item">
							<div class="form-name">订单编号:</div>
							<div class="form-input">
								<input name="name" type="text" class="input" id="name" value="${objMap.orderId }" readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">联系人:</div>
							<div class="form-input">
								<input name="name" type="text" class="input" id="name" value="${objMap.linkman }" readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">联系电话:</div>
							<div class="form-input">
								<input name="name" type="text" class="input" id="name" value="${objMap.phone }" readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">订单金额:</div>
							<div class="form-input">
								<input name="realityMoney" id="realityMoney" type="text" class="input" value="${objMap.realityMoney }" readonly="readonly">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">买家留言:</div>
							<div class="form-input">
								<textarea readonly="readonly" class="input">${objMap.message}</textarea>
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<c:if test="${feedBack}">
							<%-- 当且仅当 买家状态为驳回的时候出现此textarea --%>
							<c:if test="${feedBackBean.buyerReject eq 1}">
							<div class="form-item">
								<div class="form-name">驳回原因:</div>
								<div class="form-input">
									<textarea readonly="readonly" class="input">${feedBackBean.suggestion}</textarea>
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							</c:if>
						</c:if>
						</form>
						<hr>
						<h2 style="margin-top:20px;">卖家端</h2>
							<div class="form-item">
								<div class="form-name">卖家昵称:</div>
								<div class="form-input">
									<input name="name" type="text" class="input" id="name" value="${seller.sellerNickName}" readonly="readonly">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							<div class="form-item">
								<div class="form-name">卖家电话:</div>
								<div class="form-input">
									<input name="name" type="text" class="input" id="name" value="${seller.phone}" readonly="readonly">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							<div class="form-item">
								<div class="form-name">卖家城市:</div>
								<div class="form-input">
									<input name="name" type="text" class="input" id="name" value="${seller.cityName}" readonly="readonly">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							<div class="form-item">
								<div class="form-name">卖家学校:</div>
								<div class="form-input">
									<input name="name" type="text" class="input" id="name" value="${seller.schoolName}" readonly="readonly">
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
						 <div class="form-item">
							<div class="form-name">服务名称:</div>
							<div class="form-input">
								<input name="name" type="text" class="input" id="name" value="${seller.skillName}" readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">服务类型:</div>
							<div class="form-input">
							<c:if test="${seller.serviceType==1}">
								<input name="name" type="text" class="input" id="name" value="线上" readonly="readonly">
							</c:if>
							<c:if test="${seller.serviceType==2}">
								<input name="name" type="text" class="input" id="name" value="线下" readonly="readonly">
							</c:if>
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<!--  
						<div class="form-item">
							<div class="form-name">购买数量:</div>
							<div class="form-input">
								<input name="name" type="text" class="input" id="name" value="${skillInfo.tranNumber }" readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div> 
						<c:if test="${feedBack}">
							<div class="form-item">
								<div class="form-name">反馈标题:</div>
								<div class="form-input">
									<input name="realityMoney" id="realityMoney" type="text" class="input" value="${feedBackBean.title}" readonly="readonly">
								</div>
							</div>
							<div class="form-item">
								<div class="form-name">反馈链接:</div>
								<div class="form-input">
									<input name="realityMoney" id="realityMoney" type="text" class="input" value="${feedBackBean.feedbackurl}" readonly="readonly">
								</div>
							</div>
							<div class="form-item">
								<div class="form-name">反馈描述:</div>
								<div class="form-input">
									<textarea readonly="readonly" class="input">${feedBackBean.describe}</textarea>
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							<%-- 判定当前订单的反馈状态 --%>
							<c:if test="${feedBackBean.sellerRefuse eq 1}">
							<div class="form-item">
								<div class="form-name">拒绝修改:</div>
								<div class="form-input">
									<textarea readonly="readonly" class="input">${feedBackBean.reseon}</textarea>
									<div class="clearfix"></div>
									<div class="form-msg"></div>
								</div>
							</div>
							</c:if>
						</c:if>-->
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
</div>
