<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
				<div class="column-name">
					<h2>订单管理><b>订单列表</b></h2>
				</div>
				<div class="main-top">
				</div>
				<form id = "form1" name="form1" action="<%=basePath%>orderManage/orderMangeList.html" method="post">
				
				<div class="main-top">
					<div class="form-item">
						<div class="check-item">
							<span class="check-tit">订单编号：</span>
							<input type="text" value="${paramsMap.orderId}" class="input" name="orderId" id="orderId" />
							&nbsp;
							<span class="check-tit">下单时间：</span>
							<input type="text" value="${paramsMap.beginDate}" class="input" name="beginDate" id="begindate" onClick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')||\'%y-%M-%d\'}'})" readonly="readonly"/>--
							<input type="text" value="${paramsMap.endDate}" class="input" name="endDate" id="enddate" onClick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\')}',maxDate:'%y-%M-%d'})" readonly="readonly"/>
							&nbsp;&nbsp;&nbsp;
							<span class="check-tit">订单状态：</span>
							<select id="orderStatus" name="orderStatus" class="input">
								     <option selected="selected" value="">请选择</option>
									 <option value="1">下单成功</option>
									 <option value="2">支付成功</option>
									 <option value="3">待确认</option>
									 <option value="4">已确认</option>
									 <option value="5">超时失效</option>
									 <option value="6">申请退款</option>
									 <option value="7">待退款</option>
									 <option value="8">退款成功</option>
									 <option value="9">取消订单</option>
							</select>
							
							
						</div>
							<input type="submit" class="btn w96" value="查  询" style="float:right">
					</div>
				</div>
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>订单编号</th>
								<th>买家姓名</th>
								<th>买家手机号</th>
								<th>卖家昵称</th>
								<th>订单金额</th>
								<th>订单状态</th>
								<th>下单时间</th>
								<th>付款时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource}" var="info">
							<!-- 遍历数据 -->
							<tr>
								<td>${info.orderId }</td>
								<td>${info.linkman }</td>
								<td>${info.phone }</td>
								<td>${info.sellerNickName }</td>
								<td>${info.realityMoney }</td>
								<c:if test="${info.orderStatus==1}">
									<td>待付款</td>
								</c:if>
								<c:if test="${info.orderStatus==2}">
									<td>待服务</td>
								</c:if>
								<c:if test="${info.orderStatus==3}">
									<td>待确认</td>
								</c:if>
								<c:if test="${info.orderStatus==4}">
									<td>已完成</td>
								</c:if>
								<c:if test="${info.orderStatus==5}">
									<td>已关闭</td>
								</c:if>
								<c:if test="${info.orderStatus==6}">
									<td>申请退款</td>
								</c:if>
								<c:if test="${info.orderStatus==7}">
									<td>待退款</td>
								</c:if>
								<c:if test="${info.orderStatus==8}">
									<td>已退款</td>
								</c:if>
								<c:if test="${info.orderStatus==9}">
									<td>已关闭</td>
								</c:if>
								<c:if test="${info.orderStatus=='11'}">
									<td>状态异常</td>
								</c:if>
								<c:if test="${info.orderStatus=='12'}">
									<td>卖家拒绝退款</td>
								</c:if>
								<td>${info.createDate}</td>
								<td>${info.payTime}</td>
								<td>
									<a href="<%=basePath%>/orderManage/getOrderInfo.html?orderId=${info.orderId}&enterType=${info.enterType}" class="">查看详情</a>
								</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
					<%-- <div id="page">
						<%@ include file="/v2/console/common/pageView.jsp"%>
					</div> --%>
					<div id="page">
						<%@ include file="/v2/console/common/paging.jsp"%>
					</div>
				</div>
		<div class="popBox chaLianjie-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="chaLianjie-con">
						<div class="tit" id="lookurl"></div>
						<a href="javascript:;" class="btn w144">确  认</a>
					</div>
				</div>
			</div>
		</div><!-- chaLianjie-pop -->
		
			</form>
		<script type="text/javascript">
				function confirmDelete(ID){
					if (confirm("是否确认")) {
						location.href='<%=basePath%>v2BannerInfo/deleteV2BannerInfo.html?bannerId='+ID;
					}  else  { 
						return false;
					};
					
				}
				function lookUrl(ID){
					$.ajax({
						type : "post",
						url : "<%=basePath%>v2BannerInfo/getV2BannerInfo.html",
						dataType : "json",
						data : {bannerId:ID},
						success : function(data) {
							var $picurl=data.picUrl;
							var $url = $("#lookurl");
							$url.append($picurl);
						},
					});
					
				}
		</script>

				