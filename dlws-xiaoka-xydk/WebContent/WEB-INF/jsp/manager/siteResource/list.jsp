<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript" src="<%=basePath %>/uploadify/jquery.uploadify.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=basePath %>/uploadify/uploadify.css" />
				<div class="column-name">
					<h2>资源服务 ><b>场地资源</b></h2>
				</div>
				<div class="content">
				<div id="fileQueue"></div><!-- 上传进度条 -->
				<input type="file" id="uploadImg" name="uploadImgName" style="float:right"/> <!-- 上传文件 -->
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>城市</th>
								<th>学校</th>
								<th>服务名称</th>
								<th>服务性质</th>
								<th>服务价格</th>
								<th>服务介绍</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="site">
							<!-- 遍历数据 -->
							<tr>
								<td>${site.cityName }</td>
								<td>${site.schoolName }</td>
								<td>${site.skillName }</td>
								<c:choose>
									<c:when test="${site.serviceType == 1 }">
										<td>线上服务</td>
									</c:when>
									<c:otherwise>
										<td>线下服务</td>
									</c:otherwise>
								</c:choose>
								<td>${site.skillPrice }</td>
								<td>${site.skillDepict }</td>
								<td>
								<a href="${pageContext.request.contextPath}/siteResource/？？.html?id=${refunds.id}" class="">显示</a>
								<a href="${pageContext.request.contextPath}/siteResource/update.html?id=${refunds.id}" class="">修改</a>
								<a href="${pageContext.request.contextPath}/siteResource/delete.html?id=${refunds.id}" class="">删除</a>
								</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
	</form>
	
<script>
	//AJAX 异步 上传文件 不保存数据 
	$("#uploadImg").uploadify({
			//插件自带  不可忽略的参数flash插件
			'swf': '<%=basePath %>/uploadify/uploadify.swf',
			//前台请求后台的url 不可忽略的参数
			'uploader': '<%=basePath %>/siteResource/exportToExcel.html',
			//给div的进度条加背景 不可忽略
		  	'queueID': 'fileQueue',
			//上传文件文件名 跟file标签 name值 保持一致
			'fileObjName' : 'uploadImgName',
			//给上传按钮设置文字
			'buttonText': '导入数据',
			//设置文件是否自动上传
			'auto': true,
			//可以同时选择多个文件 默认为true  不可忽略
			'multi': true,
			//上传后队列是否消失
			'removeCompleted': true,
			//队列消失时间
			'removeTimeout' : 1,
			//上传文件的个数，项目中一共可以上传文件的个数
			'uploadLimit':  -1,
			'fileTypeExts': '*.*',
			//成功回调函数 file：文件对象。data后台输出数据
			'onUploadSuccess':function(file,data,response){
					alert(data);
			}
			
	});
</script>