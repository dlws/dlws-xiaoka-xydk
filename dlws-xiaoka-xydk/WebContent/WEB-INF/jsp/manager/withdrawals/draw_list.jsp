<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/base.css">
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/style.css">
<script src="<%=basePath%>/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="<%=basePath%>/v2/console/js/globle.js"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<div class="column-name">
	<h2>
		提现管理><b>提现管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="<%=basePath%>/draw/drawList.html" method="post">
<div class="main-top">
	<div class="fr" align="center">
		<span class="c-name">用户名</span> 
		<input type="text" name="name" class="input mr10" value="${paramsMap.name }"placeholder="请输入用户名">
		<span class="c-name">审核状态</span> 
		<select name="applicationStatus" class="input">
			<option value="">全部</option>
			<c:choose>
				<c:when test="${paramsMap.applicationStatus == '0' }">
					<option value="0" selected="selected">审核中</option>
				</c:when>
				<c:otherwise>
					<option value="0">审核中</option>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${paramsMap.applicationStatus == '1' }">
					<option value="1" selected="selected">审核通过</option>
				</c:when>
				<c:otherwise>
					<option value="1">审核通过</option>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${paramsMap.applicationStatus == '2' }">
					<option value="2" selected="selected">审核不通过</option>
				</c:when>
				<c:otherwise>
					<option value="2">审核不通过</option>
				</c:otherwise>
			</c:choose>
		</select>
		<span class="c-name">银行账户</span> 
		<input type="text" name="bankCardNumber" class="input mr10" value="${paramsMap.bankCardNumber }"placeholder="请输入银行账户"> 
		<span class="c-name">时间：</span> 
				<input type="text" value="${paramsMap.beginDate}" class="input"
				name="beginDate" id="begindate"
				onClick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')||\'%y-%M-%d\'}'})"
				readonly="readonly" />--
				<input type="text" value="${paramsMap.endDate}" class="input"
				name="endDate" id="enddate"
				onClick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\')}',maxDate:'%y-%M-%d'})"
				readonly="readonly" />
		<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
	</div>
</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>提现用户</th>
					<th>联系方式</th>
					<th>申请提现金额</th>
					<th>账户余额</th>
					<th>申请时间</th>
					<th>审核人</th>
					<th>审核状态</th>
					<th>开户银行</th>
					<th>开户行账号</th>
					<th>开户行全称</th>
					<th>审核日期</th>
					<th>创建日期</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="draw">
					<tr>
						<c:choose>
							<c:when test="${!empty draw.name }">
								<td>${draw.name }</td>
							</c:when>
							<c:otherwise>
								<td>匿名</td>
							</c:otherwise>
						</c:choose>
						<td>${draw.phoneNumber }</td>
						<td>${draw.withdrawMoney }</td>
						<td>
							<c:choose>
								<c:when test="${!empty draw.balance }">
									${draw.balance }
								</c:when>
								<c:otherwise>
									0
								</c:otherwise>
							</c:choose>
						</td>
						<td><fmt:formatDate value="${draw.applicationTime }"
								type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td>${draw.checkPeople }</td>
						<c:choose>
							<c:when test="${draw.applicationStatus == 0 }">
								<td>审核中</td>
							</c:when>
							<c:when test="${draw.applicationStatus == 1 }">
								<td>审核通过</td>
							</c:when>
							<c:otherwise>
								<td>审核不通过</td>
							</c:otherwise>
						</c:choose>
						<td>${draw.bank }</td>
						<td>${draw.bankCardNumber }</td>
						<td>${draw.AccountFullName }</td>
						<td><fmt:formatDate value="${draw.checkTime }" type="both"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td><fmt:formatDate value="${draw.createTime }" type="both"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td><c:choose>
								<c:when test="${draw.applicationStatus == 0 }">
									<a href="<%=basePath%>/draw/getDrawById.html?id=${draw.id}"
										class="">审核</a>
								</c:when>
								<c:otherwise>
											审核
										</c:otherwise>
							</c:choose></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
	<div class="popBox chaLianjie-pop">
		<div class="popBox-back"></div>
		<div class="popBox-box">
			<div class="popBox-con">
				<div class="chaLianjie-con">
					<div class="tit" id="lookurl"></div>
					<a href="javascript:;" class="btn w144">确 认</a>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	function goSearch(){
		$("#form1").submit();
	}
</script>

