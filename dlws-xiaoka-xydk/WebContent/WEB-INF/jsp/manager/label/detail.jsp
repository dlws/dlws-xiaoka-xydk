<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>修改income</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>income管理 > <b>查看详情income</b></h2>
				</div>
				<div class="form">
					<form id="updateincome" action="/label/updateLabel.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
						
						
						<div class="form-item">
							<div class="form-name">入住类型的id：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="enterId" name="enterId" value="${obj.enterId} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">标签名称：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="labelName" name="labelName" value="${obj.labelName} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">排列顺序：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="ord" name="ord" value="${obj.ord} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">是否删除：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="isDelete" name="isDelete" value="${obj.isDelete} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						
						<div class="form-item">
							<div class="form-name">创建时间：</div>
							<div class="form-input">
								<input type="text"  class="input" name="createDate" id="createDate"  value="${obj.createDate} " readonly="readonly"/>
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
				</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
//上传图片
function uploadPic(){
	//异步上传
	var options = {
			url : "${path}/label/uploadImg.html",
			dataType : "json",
			type : "post",
			success : function(data){
				//回调 路径  data.path
				$("#allUrl").attr("src",data.url);
				$("#path").val(data.path);
			}
	}
	$("#income").ajaxSubmit(options);
}
</script>