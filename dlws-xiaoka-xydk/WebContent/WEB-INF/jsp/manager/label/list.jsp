
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
       function goSearch(){
       $("#form1").submit();
       
       
       }
</script>
	<div class="column-name">
		<h2>
			标签管理 > <b>标签列表</b>
		</h2>
	</div>
<form id="form1" name="form1" action="${path}/label/labelList.html" method="post">

	<div class="main-top">
		<div class="fl">
			<a href="${path}/label/toAddLabel.html" class="btn btn-p">新建标签</a>
		</div>
		<div class="fr" align="center">
			<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
						    <!-- <th>入住类型的id</th> -->
						    <th>标签名称</th>
						    <th>排列顺序</th>
						    <!-- <th>是否删除</th> -->
						    <th>创建时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="data">
				<tr>
							<%-- <td>${data.enterId}</td> --%>
							<td>${data.labelName}</td>
							<td>${data.ord}</td>
							<%-- <td>${data.isDelete}</td> --%>
							<td><fmt:formatDate  value="${data.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				<td>
					<!-- 是否删除判断 -->
					<a href="javascript:;" data-id="${path}/label/delLabel.html?id=${data.id}" class="remove">删除</a> 
					<!-- 修改--> 
					<a class="" href="${path}/label/toUpdateLabel.html?id=${data.id}">修改</a>
					<!-- 详情 
					<a class="" href="${path}/label/detailLabel.html?id=${data.id}">查看详情</a>-->
				</td>
				</tr>	
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a>
					<a href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
