<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
	<title>修改income</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>标签管理 > <b>修改标签</b></h2>
				</div>
				<div class="form">
					<form id="updateincome" action="${path}/label/updateLabel.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
						
								<div class="form-item">
									<div class="form-name">入驻类型：</div>
									<div class="form-input" style="margin-top: 24px">
											<c:forEach var="map"  items="${enterList}" varStatus="status">
												<c:choose>
													<c:when test="${obj.enterId eq map.id}">
															<input type="radio" checked="checked" name="enterId"  class="lx" value="${map.id}">${map.className }
													</c:when>
													<c:otherwise>
															<input type="radio"  name="enterId"  class="lx" value="${map.id}">${map.className }
													</c:otherwise>
												</c:choose>
											</c:forEach>
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div> 
								<div class="form-item">
									<div class="form-name">标签名称：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" maxlength="250" id="labelName" name="labelName" value="${obj.labelName}" required="required" >
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
								<div class="form-item">
									<div class="form-name">排列顺序：</div>
									<div class="form-input">
										<input AutoComplete="off" type="number" maxlength="4" class="input" id="ord" name="ord" value="${obj.ord}" required="required">
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div>
					<button type="submit" class="btn w144">保  存</button>
				</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
//上传图片
function uploadPic(){
	//异步上传
	var options = {
			url : "${path}/label/uploadImg.html",
			dataType : "json",
			type : "post",
			success : function(data){
				//回调 路径  data.path
				$("#allUrl").attr("src",data.url);
				$("#path").val(data.path);
			}
	}
	$("#income").ajaxSubmit(options);
}
</script>
