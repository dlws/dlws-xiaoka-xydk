<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<%-- <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> --%>
<div class="column-name">
	<h2>
		技能空间 > <b>技能空间列表</b>
	</h2>
</div>
<form id="form1" name="form1" action="${path}/skillUserManager/skillSpaceList.html" method="post">
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
						<th>技能名称</th>
						<th>技能价格</th>
						<th>服务类型</th>
						<th>创建时间</th>
						<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po}" var="map">
					<tr>
							<td>${map.skillName}</td>
							<td>${map.skillPrice}</td>
							<td>
								<c:if test="${map.serviceType==1}">
									线上服务
								</c:if>
								<c:if test="${map.serviceType==2}">
									线下服务
								</c:if>
							</td>
							<td>${map.createDate}</td>
							<td>
								<a href="${path}/skillUserManager/toUpdateSkillInfo.html?id=${map.id}&basicId=${basicId}">修改技能信息</a>
							</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<%-- <div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div> --%>
	</div>
</form>
<script>
	function skillSpaceRecommend(id){
		$.ajax({
			type : "post",
			url : "${path}/skillUserManager/skillSpaceRecommend.html",
			dataType : "json",
			data : {id:id,isRecommend:1},
			success : function(data) {
				if(data.flag==true){
					alert("设置推荐成功!");
					window.location.reload();
				}else{
					alert("设置失败!");
				}
			}
		});
	}
	function canSkillSpaceRecommend(id){
		$.ajax({
			type : "post",
			url : "${path}/skillUserManager/skillSpaceRecommend.html",
			dataType : "json",
			data : {id:id,isRecommend:0},
			success : function(data) {
				if(data.flag==true){
					alert("取消推荐成功!");
					window.location.reload();
				}else{
					alert("设置失败!");
				}
			}
		});
	}
	function skillSpaceCloseOrOpen(id){
		$.ajax({
			type : "post",
			url : "${path}/skillUserManager/skillSpaceCloseOrOpen.html",
			dataType : "json",
			data : {id:id,skillStatus:0},
			success : function(data) {
				if(data.flag==true){
					alert(" 打开成功!");
					window.location.reload();
				}else{
					alert("打开失败!");
				}
			}
		});
	}
	function canSkillSpaceCloseOrOpen(id){
		$.ajax({
			type : "post",
			url : "${path}/skillUserManager/skillSpaceCloseOrOpen.html",
			dataType : "json",
			data : {id:id,skillStatus:1},
			success : function(data) {
				if(data.flag==true){
					alert("关闭成功!");
					window.location.reload();
				}else{
					alert("关闭失败!");
				}
			}
		});
	}
</script>


