<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/js/jquery.form.js"></script>
<script src="http://api.map.baidu.com/api?v=1.3"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
</script>
<div class="column-name">
	<h2>
		技能空间> <b>详情</b>
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/" method="post" enctype="multipart/form-data">
	
	
		<div class="form-item">
			<div class="form-name">空间名称：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="activityName" value="${baseInfo.userName}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">城市：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="activityName" value="${baseInfo.cityName}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">学校：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="activityName" value="${baseInfo.schoolName}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">联系电话：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="activityName" value="${baseInfo.phoneNumber}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">联系邮箱：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="activityName" value="${baseInfo.email}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">微信号：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="activityName" value="${baseInfo.wxNumber}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">关于：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="activityName" value="${baseInfo.aboutMe}" >
			</div>
		</div>
		<c:forEach items="${skillInfo }" var="skill">
			<div class="form-item">
				<div class="form-name">技能名称：</div>
				<div class="form-input">
					<input type="text" class="input" placeholder="" name="activityName" value="${skill.skillName}" >
				</div>
			</div>	
			<div class="form-item">
				<div class="form-name">技能价格：</div>
				<div class="form-input">
					<input type="text" class="input" placeholder="" name="activityName" value="${skill.skillPrice}" >
				</div>
			</div>	
			<div class="form-item">
				<div class="form-name">技能描述：</div>
				<div class="form-input">
					<input type="text" class="input" placeholder="" name="activityName" value="${skill.skillDepict}" >
				</div>
			</div>	
			<div class="form-item">
				<div class="form-name">图文URL：</div>
				<div class="form-input">
					<input type="text" class="input" placeholder="" name="activityName"  value="${skill.textURL}" >
				</div>
			</div>
			<div class="form-item">
				<div class="form-name">视频URL：</div>
				<div class="form-input">
					<input type="text" class="input" placeholder="" name="activityName" value="${skill.videoURL}" >
				</div>
			</div>
			<c:forEach items="${imgList }" var="image">
				<c:if test="${skill.id==image.skillId }">
				<div class="form-item">
					<div class="form-name">技能图片：</div>
						<div class="form-img">
		 						<img id="allUrl" style="width:400px;height:300px;" src="${image.imgURL}">
		 				</div>
					</c:if>	
				</div>
			</c:forEach>
		</c:forEach>
		<c:if test="${baseInfo.auditState==0}">
			<div class="form-item">
				<div class="form-name">审核原因：</div>
				<div class="form-input">
					<input type="text" id="auditNotes" class="input" name="auditNotes" value="" >
				</div>
			</div>
			<input type="button" onclick="checkOk(${baseInfo.id});" class="btn w144" value="审核通过">
			<input type="button" onclick="checkNoOk(${baseInfo.id});" class="btn w144" value="审核不通过">
		</c:if>
	</form>
</div>	
<script>
	
	function checkOk(id){
		window.location.href = "${path}/skillUserManager/skillSpaceCheck.html?auditState=1&id="+id;
	}
	function checkNoOk(id){
		var auditNotes = $("#auditNotes").val();
		if(auditNotes==""){
			alert("审核不通过时,审核原因必须填写！");
			return ;
		}
		window.location.href = "${path}/skillUserManager/skillSpaceCheck.html?auditState=2&id="+id;
	}
	
</script>
		
		
		

	

