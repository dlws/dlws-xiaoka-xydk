<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui.min.js"></script>
<script src="${path}/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/js/jquery.form.js"></script>
<script src="http://api.map.baidu.com/api?v=1.3"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
</script>
<div class="column-name">
	<h2>
		技能空间> <b>修改基本信息</b>
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/skillUserManager/editBaseInfo.html" method="post" enctype="multipart/form-data">
		
				<input type="hidden" name="id" value="${baseInfo.id}" >
		<div class="form-item">
			<div class="form-name">用户名称：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="userName" value="${baseInfo.userName}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">城市：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="cityName" value="${baseInfo.cityName}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">学校：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="schoolName" value="${baseInfo.schoolName}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">联系电话：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="phoneNumber" value="${baseInfo.phoneNumber}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">联系邮箱：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="email" value="${baseInfo.email}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">微信号：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="wxNumber" value="${baseInfo.wxNumber}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">状态：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="wxNumber" value="${baseInfo.auditState}" >
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">关于：</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="" name="aboutMe" value="${baseInfo.aboutMe}" >
			</div>
		</div>
		<input type="submit" class="btn w144" value="保存">
	</form>
</div>	
<script>
	
	/* function checkOk(id){
		window.location.href = "${path}/skillUserManager/skillSpaceCheck.html?auditState=1&id="+id;
	}
	function checkNoOk(id){
		var auditNotes = $("#auditNotes").val();
		if(auditNotes==""){
			alert("审核不通过时,审核原因必须填写！");
			return ;
		}
		window.location.href = "${path}/skillUserManager/skillSpaceCheck.html?auditState=2&id="+id;
	} */
	
</script>
		
		
		

	

