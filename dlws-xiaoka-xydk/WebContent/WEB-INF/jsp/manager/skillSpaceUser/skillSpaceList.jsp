<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/js/jquery.form.js"></script>
<%-- <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> --%>
<div class="column-name">
	<h2>
		技能空间 > <b>技能空间列表</b>
	</h2>
</div>
<form id="form1" name="form1" action="${path}/skillUserManager/skillSpaceList.html" method="post"  >
	<div class="main-top">
			<div class="form-item">
							<div class="check-item">
								<span class="check-tit">技能空间名称</span>
								<input type="text"  value="${userName1}" class="input" name="userName"/>
								<span class="check-tit">是否推荐：</span>
								<select id="isRecommend" name="isRecommend" class="input">
									<option value="">全部</option>
									<c:if test="${isRecommend=='1'}">
										<option value="1" selected="selected">推荐</option>
									</c:if>
									<c:if test="${isRecommend!='1'}">
										<option value="1" >推荐</option>
									</c:if>
									<c:if test="${isRecommend=='0'}">
										<option value="0" selected="selected">不推荐</option>
									</c:if>
									<c:if test="${isRecommend!='0'}">
										<option value="0">不推荐2</option>
									</c:if>
								</select>
								<span class="check-tit">空间状态：</span>
								<select id="" name="skillStatus" class="input">
									<option value="" selected="selected">全部</option>
									<c:if test="${skillStatus=='0'}">
										<option value="0" selected="selected">开启</option>
									</c:if>
									<c:if test="${skillStatus!='0'}">
										<option value="0" >开启</option>
									</c:if>
									<c:if test="${skillStatus=='1'}">
										<option value="1" selected="selected">关闭</option>
									</c:if>
									<c:if test="${skillStatus!='1'}">
										<option value="1">关闭</option>
									</c:if>
									<!-- 
									<option value="0">开启</option>
									<option value="1">关闭</option> -->
								</select>
								<span class="check-tit">审核状态：</span>
								<select id="" name="auditState" class="input">
									<option value="" selected="selected">全部</option>
									<c:if test="${auditState=='0'}">
										<option value="0" selected="selected">审核中</option>
									</c:if>
									<c:if test="${auditState!='0'}">
										<option value="0" >审核中</option>
									</c:if>
									<c:if test="${auditState=='1'}">
										<option value="1" selected="selected">审核成功</option>
									</c:if>
									<c:if test="${auditState!='1'}">
										<option value="1">审核成功</option>
									</c:if>
									<c:if test="${auditState=='2'}">
										<option value="2" selected="selected">审核失败</option>
									</c:if>
									<c:if test="${auditState!='2'}">
										<option value="2">审核失败</option>
									</c:if>
									<!-- 
									<option value="0">审核中</option>
									<option value="1">审核成功</option>
									<option value="2">审核失败</option> -->
								</select>
								<span class="check-tit">城市：</span>
								<select id="cityId" name="cityId" class="input" onchange="queryScholTypeByCity(this.value);">
									<option value="">全部</option>
									<c:forEach items="${cityList}" var="m">
										<c:if test="${m.cityId ==cityId}">
											<option selected="selected" value="${m.cityId}">${m.cityName }</option>
										</c:if>
										<c:if test="${m.cityId != cityId}">
											<option value="${m.cityId}">${m.cityName }</option>
										</c:if>
									</c:forEach>
								</select>
								<span class="check-tit">学校：</span>
								<select name="schoolId" class="input" id="schoolId">
								   <option value="">全部</option>
								   <c:forEach items="${schoolList}" var="school">
										<c:if test="${school.id == schoolId}">
											<option selected="selected" value="${school.id}">${school.schoolName }</option>
										</c:if>
										<c:if test="${school.id != schoolId}">
											<option value="${school.id}">${school.schoolName }</option>
										</c:if>
									</c:forEach>			
								
									<%-- <option value="${objMap.schoolId}">${objMap.schoolName }</option> --%>
								</select>
							</div>
						<input type="button" onclick="bb()" class="btn w96" value="查  询" style="float:right">
						<input type="button" onclick="aa()" class="btn w96" value="导   出" style="float:right">
			</div>
			
	 </div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
						<th>序号</th>
						<th>用户入驻名称</th>
						<th>用户入驻昵称</th>
						<th>状态</th>
						<th>城市</th>
						<th>学校</th>
						<th>联系电话</th>
						<th>联系邮箱</th>
						<th>微信号</th>
						<th>创建时间</th>
						<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="map">
					<tr>
							<td>${map.id }</td>
							<td><a href="${path}/skillUserManager/skillInfoList.html?id=${map.id }">${map.userName}</a></td>
							<%-- <td>${map.userName}</td> --%>
							<td>${map.nickname}</td>
							<td>
								<c:if test="${map.auditState==0}">未审核</c:if>
								<c:if test="${map.auditState==1}">审核成功</c:if>
								<c:if test="${map.auditState==2}">审核失败</c:if>
							</td>
							<td>${map.cityName}</td>
							<td>${map.schoolName}</td>
							<td>${map.phoneNumber}</td>
							<td>${map.email}</td>
							<td>${map.wxNumber}</td>
							<td>${map.createDate}</td>
							<td>
								<c:if test="${map.isRecommend==0}">
									<a href="#" onclick="skillSpaceRecommend(${map.id});">推荐</a><!-- 推荐 -->
								</c:if>
								<c:if test="${map.isRecommend==1}">
									<a href="#" onclick="canSkillSpaceRecommend(${map.id});">取推</a><!-- 取消推荐 -->
								</c:if>
								<c:if test="${map.skillStatus==0}">
									<a href="#" onclick="canSkillSpaceCloseOrOpen(${map.id});">关闭</a>
								</c:if>
								<c:if test="${map.skillStatus==1}">
									<a href="#" onclick="skillSpaceCloseOrOpen(${map.id});">打开</a>
								</c:if>
								<a href="${path}/skillUserManager/skillSpaceDetailV2.html?id=${map.id}">审核</a>
								<%-- <a href="javascript:;"
							data-id="${path}/skillUserManager/delBasInfo.html?id=${map.id}"
							class="remove">删除</a> --%>
								<a href="${path}/skillUserManager/toEditBaseInfo.html?id=${map.id}">修改</a>
								<%-- <a href="${path}/skillUserManager/toEditLabel.html?id=${map.id}">标</a> --%>
							</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
	
</form>
	<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a> <a href="javascript:;"
						class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- remove-pop -->
<script>
	function skillSpaceRecommend(id){
		$.ajax({
			type : "post",
			url : "${path}/skillUserManager/skillSpaceRecommend.html",
			dataType : "json",
			data : {id:id,isRecommend:1},
			success : function(data) {
				if(data.flag==true){
					alert("设置推荐成功!");
					window.location.reload();
				}else{
					alert("设置失败!");
				}
			}
		});
	}
	function canSkillSpaceRecommend(id){
		$.ajax({
			type : "post",
			url : "${path}/skillUserManager/skillSpaceRecommend.html",
			dataType : "json",
			data : {id:id,isRecommend:0},
			success : function(data) {
				if(data.flag==true){
					alert("取消推荐成功!");
					window.location.reload();
				}else{
					alert("设置失败!");
				}
			}
		});
	}
	function skillSpaceCloseOrOpen(id){
		$.ajax({
			type : "post",
			url : "${path}/skillUserManager/skillSpaceCloseOrOpen.html",
			dataType : "json",
			data : {id:id,skillStatus:0},
			success : function(data) {
				if(data.flag==true){
					alert(" 打开成功!");
					window.location.reload();
				}else{
					alert("打开失败!");
				}
			}
		});
	}
	function canSkillSpaceCloseOrOpen(id){
		$.ajax({
			type : "post",
			url : "${path}/skillUserManager/skillSpaceCloseOrOpen.html",
			dataType : "json",
			data : {id:id,skillStatus:1},
			success : function(data) {
				if(data.flag==true){
					alert("关闭成功!");
					window.location.reload();
				}else{
					alert("关闭失败!");
				}
			}
		});
	}
	
	function aa(){
		$("#form1").attr("action","${path}/skillUserManager/exportToExcel.html");
		$("#form1").submit();
	}
	function bb(){
		$("#form1").attr("action","${path}/skillUserManager/skillSpaceList.html");
		$("#form1").submit();
	}
	
	//根据市查出院校类别
	 function queryScholTypeByCity(value){
		 
		 $("#schoolId option").remove();
		 var $option0 = $('<option></option>').val(null).text("请选择");
		 $("#schoolId").append($option0); 
		 $.ajax({
			type : "post",
			url : "${path}/school/getSchoolsByCityId.html",
			dataType : "json",
			data : {cityId:value},
			success : function(data) {
				var $sel = $("#schoolId");
				if(data.list.length>0){
					$.each(data.list, function(i, department){
						 var $option = $('<option></option>').val(department.id).text(department.schoolName);
						 $sel.append($option);
					 });
				}
				
			}
		});	
	}
	
	
</script>


