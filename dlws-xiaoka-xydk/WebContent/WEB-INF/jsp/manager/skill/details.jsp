<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>校咖微平台后台管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>技能设置 > <b>查看详情</b></h2>
				</div>
				<div class="form">
					<form id="editPlatGoods" action="" method="post">
						<input type="hidden" name="id" value="${objMap.id}">
						<input type="hidden" name="skillName" value="${objMap.skillName}">
						<input type="hidden" name="skillFatId" value="${objMap.skillFatId}">
						<input type="hidden" name="skillSonId" value="${objMap.skillSonId}">
						<input type="hidden" name="videoUrl" value="${objMap.videoUrl}">
						<input type="hidden" name="textURL" value="${objMap.textURL}">
						<input type="hidden" name="skillPrice" value="${objMap.skillPrice}">
						<input type="hidden" name="company" value="${objMap.company}">
						<input type="hidden" name="serviceType" value="${objMap.serviceType}">
						<input type="hidden" name="skillDepict" value="${objMap.skillDepict}">
						<input type="hidden" name="ishome" value="${objMap.ishome}">
						
						<div class="form-item">
							<div class="form-name">技能名称：</div>
							<div class="form-input">
								<input type="text" class="input" id="skillName" name="skillName" value = "${objMap.skillName }" style="border:0px" readonly= "true">
								<div class="clearfix"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">技能分类:</div>
								<div class="form-input">
								<input type="text" class="input" id="skillName" name="skillName" value = "${classInfo.className }" style="border:0px" readonly= "true">
								</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">视屏地址：</div>
							<div class="form-input">
								<input type="text" class="input" name="videoUrl" id="videoUrl" value="${objMap.videoUrl }"  style="border:0px" readonly= "true">
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">其他作品：</div>
							<div class="form-input">
								<input type="text" class="input" id="textURL" name="textURL" value="${objMap.textURL }"  style="border:0px" readonly= "true">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">技能价格：</div>
							<div class="form-input">
								<input type="text" class="input" name="skillPrice" id="skillPrice" onpaste="return false;" value="${objMap.skillPrice }"  onkeypress="keyPress()" placeholder="单位：元"  style="border:0px" readonly= "true">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">服务类型：</div>
							<div class="form-input">
								<c:if test="${objMap.serviceType == 1}">
									<input type="text" class="input" id="textURL" name="textURL" value="线下服务"  style="border:0px" readonly= "true">
								</c:if>
								<c:if test="${objMap.serviceType == 2}">
									<input type="text" class="input" id="textURL" name="textURL" value="线上服务"  style="border:0px" readonly= "true">
								</c:if>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">技能描述：</div>
							<div class="form-input">
								<input name="skillDepict" class="input"  id="skillDepict" value="${objMap.skillDepict }" style="border:0px" readonly= "true"></input>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">显示顺序：</div>
							<div class="form-input">
								<input name="ord" class="input"  id="ord" value="${objMap.ord }" style="border:0px" readonly= "true"></input>
							</div>
						</div>
						 <input type="hidden" name="thumbnailUrl" id="thumbnailUrl">
						<input type="hidden" name = "basicId"  id = "basicId" value="${basicId }">
						
						<c:if test="${not empty images}">
							<c:forEach var="img" items="${images}">
								<input type="hidden" name="imgUrl" value="${img.imgURL}">
							</c:forEach>
						</c:if> 
						
					</form>
				
					 <form id="detailPicForm" onsubmit="return false;">
						<div class="form-item form-up">
							<div class="form-name">商品详情页图集：</div>
							<div class="form-input">
								<div class="up-btn">
									<input type="file" name="imgs" id="morePic" multiple size="80" class="btn-blue" /> 
									<!-- <a href="javascript:void(0);" class="btn-blue">选择图片
									</a> -->
								</div>
								<ul id="pics" class="up-list">
									<c:if test="${not empty images}">
										<c:forEach var="imgUrl" items="${images}">
											<li>
												<div class="txt">
													<span>预览:</span>
													<a href="javascript:void(0);" onclick="deleteDetailImage('${imgUrl.imgURL}','${projectPath}${imgUrl.imgURL}')" class="remove">删除</a>
												</div>
												<div class="img">
													<img src="${projectPath}${imgUrl.imgURL}" alt="">
												</div>
											</li>
										</c:forEach>
									</c:if>
								</ul>
								<div class="btns">
									<button class="btn-blue" onclick="uploadDetailImage()">开始上传</button>
									<!-- <a href="" class="btn-lightGray">全部取消</a> -->
								</div>
							</div>
						</div>
					</form> 
					
					<button type="button" class="btn w144" onclick="add()">保  存</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
	<script>
		//提交
		function add(){
			if(flag){
				$("#editPlatGoods").submit();
			}
		}

	</script>
</body>
</html>