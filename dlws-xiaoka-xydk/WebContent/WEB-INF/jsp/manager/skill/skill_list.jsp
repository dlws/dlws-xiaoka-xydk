<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
    <title>校咖微平台后台管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>服务管理 > <b>服务列表</b></h2>
				</div>
				<script type="text/javascript">
				       function goSearch(){
				       $("#form1").submit();
				       }
				</script>
				<form id = "form1" name="form1" action = "${path}/skill/skillList.html">
				<div class="main-top">
					<div class="fr" align="center">
						<span class="check-tit">一级分类：</span>
								<select id="skillFatId" name="skillFatId" class="input" onchange="queryClassSon(this.value);">
									<option value="">全部</option>
									<c:forEach items="${classF}" var="m">
										<c:if test="${m.id ==paramsMap.skillFatId}">
											<option selected="selected" value="${m.id}">${m.className }</option>
										</c:if>
										<c:if test="${m.id != paramsMap.skillFatId}">
											<option value="${m.id}">${m.className }</option>
										</c:if>
									</c:forEach>
								</select>
								
								<span class="check-tit">二级分类：</span>
								<select name="skillSonId" class="input" id="skillSonId">
								   <option value="">全部</option>
								   <c:forEach items="${classS}" var="son">
										<c:if test="${son.value == paramsMap.skillSonId}">
											<option selected="selected" value="${son.value}">${son.text }</option>
										</c:if>
										<c:if test="${son.value != paramsMap.skillSonId}">
											<option value="${son.value}">${son.text }</option>
										</c:if>
									</c:forEach>			
								</select>
								
						<span class="c-name">所属用户</span>
						<input type="text" name="userName" class="input mr10" value="${paramsMap.userName }" placeholder="请输入所属用户关键字"> 
						
						<span class="c-name">服务名称</span>
						<input type="text" name="skillName" class="input mr10" value="${paramsMap.skillName }" placeholder="请输入服务名称关键字"> 
						<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
					</div>
				</div>
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>所属用户</th>
								<th>技能名称</th>
								<th>技能价格</th>
								<th>服务类型</th>
								<th>技能描述</th>
								<th>创建时间</th>
								<th>首页是否显示</th>
								<th>显示顺序</th>
								<th width="250">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="skill" items="${po.datasource}">
								<tr>
									<td>${skill.userName}</td>
									<td>${skill.skillName}</td>
									<td>${skill.skillPrice}</td>
									<td>
										<c:if test="${skill.serviceType == 1}">线上服务</c:if>
										<c:if test="${skill.serviceType == 2}">线下服务</c:if>
									</td>
									<td style="width: 25%">${skill.skillDepict}</td>
									<td style="width: 15%">${skill.createDate}</td>
									<td>
										<c:if test="${skill.ishome == 0}">否</c:if>
										<c:if test="${skill.ishome == 1}">是</c:if></td>
									<td>${skill.ord}</td>
									<td>
										<c:if test="${skill.ishome == 0}">
											<a href="${path}/skill/updateHome.html?id=${skill.id}&ishome=${skill.ishome}">显示</a>
										</c:if>
										<c:if test="${skill.ishome == 1}">
											<a href="${path}/skill/updateHome.html?id=${skill.id}&ishome=${skill.ishome}">不显示</a>
										</c:if>
										<a href="${path}/skill/toUpdateSkill.html?id=${skill.id}">修改</a>
										<a href="${path}/skill/deleteSkill.html?id=${skill.id}">删除</a>
										<a href="${path}/skill/details.html?id=${skill.id}">查看详情</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
				</form>
			</div>
		</div>
		<div class="popBox modifyGoods-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="modifyGoods-con">
					<form id="excelForm" onsubmit="return false;">
						<div class="tit">
							Excel文件:<input type='text' disabled="disabled" style="width: 300px;" id='textExcel' class='input' placeholder="请您选择Excel文件"/> 
							<input type="file" name="excel" class="file" id="fileExcel" onchange="document.getElementById('textExcel').value=this.value" />
						</div>
						<div class="btns">
							<button class="btn w144" onclick="uploadExcel()">确  认</button>
							<a href="javascript:;" class="btn-gray w144">取  消</a>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div><!-- modifyGoods-pop -->
		<div class="popBox chaLianjie-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="chaLianjie-con">
						<div class="tit" id="msg"></div>
						<a href="javascript:;" class="btn w144" id="okButton">确  认</a>
					</div>
				</div>
			</div>
		</div><!-- chaLianjie-pop -->
		<div class="popBox remove-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="remove-con">
						<div class="tit">你确定要删除吗？</div>
						<div class="btns">
							<a href="javascript:;" class="btn w144">删  除</a>
							<a href="javascript:;" class="btn-gray w144">取  消</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			remove();// 删除弹出框
			quxiao();// 弹出框
			selectGoods()
		});
		
		function ss(){
			alert("该功能尚未完善，如要修改，请删除后重新添加！！");
		}
		
		//全选框
		function selectAll(){
			var isChecked = $("#selectAll")[0].checked;
			$(".table").find("tbody").find("input[type='checkbox']").each(function(){{
					this.checked = isChecked;
				/* if(this.checked){
					this.checked = false;
				}else{
					this.checked = true;
				} */
			}});
		}
		
		//选择框
		function selectGoods(){
			$(".table").find("tbody").find("input[type='checkbox']").click(function(){
				if(this.checked){
					if($(".table").find("tbody").find('input:checked').size() == $(".table").find("tbody").find("input[type='checkbox']").size()){
						$("#selectAll")[0].checked = true;
					}
				}else{
					$("#selectAll")[0].checked = false;
				}
			});
		}
		
		//批量删除
		function deleteMore(){
			if($(".table").find("tbody").find("input:checked").size() > 0){
				var goodsIds = '';
				$(".table").find("tbody").find("input:checked").each(function(){
					 goodsIds += this.id+","
				});
				goodsIds = goodsIds.substring(0,goodsIds.length-1);
				window.location.href = "${pageContext.request.contextPath}/platgoods/delete.html?platGoodsId="+goodsIds;
			}else{
				alert("您没有选择商品！");
			}
			
		}
		
		// 弹出框
		function quxiao(){
			var $btn = $('.popBox').find('.btn-gray');
			$btn.click(function() {
				$('.popBox').hide();
			});
		}
		// 删除
		function remove(){
			$('.remove').click(function() {
				$('.remove-pop').show();
				$('.remove-pop .btn').attr({'href': '${pageContext.request.contextPath}/platgoods/delete.html?platGoodsId='+this.id });
			});	
		}
		
		//弹出导入框
		function importPop(){
			$('.modifyGoods-pop').show();
		}
		
		//导入excel数据
		function uploadExcel(){
			var formData = new FormData($("#excelForm")[0]);
			$.ajax({
				type : "post",
				url : "importByExcel.html",
				dataType : "json",
				data : formData,
				async : false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data) {
					if(data.flag){
						$("#msg").html("导入成功！");
						$('.chaLianjie-pop').show();
						$("#okButton").attr("onclick","succ()");
					}else{
						$("#msg").html(data.msg);
						$('.chaLianjie-pop').show();
						$("#okButton").attr("onclick","fail()");
					}
					return false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown){
		          alert("出错了！");
		       }
			});
			return false;;
		}
		
		function succ(){
			$('.chaLianjie-pop').hide();
			$('.modifyGoods-pop').hide();
			window.location.href = "${pageContext.request.contextPath}/platgoods/toPageList.html";
		}
		function fail(){
			$('.chaLianjie-pop').hide();
		}
		
		function loadPlagGoods(){
			$("#pageForm").submit();
		}
		
		
		
		//根据一级分类查二级分类
		 function queryClassSon(value){
			 $("#skillSonId option").remove();
			  var $option0 = $('<option></option>').val(null).text("请选择");
			  $("#skillSonId").append($option0); 
			    $.ajax({
				type : "post",
				url : "${pageContext.request.contextPath}/skill/getClassSonByFatherId.html",
				dataType : "json",
				data : {skillFatId:value},
				success : function(data) {
					var $sel = $("#skillSonId");
					if(data.sonList.length>0){
						$.each(data.sonList, function(i, department){
							 var $option = $('<option></option>').val(department.value).text(department.text);
							 $sel.append($option);
						 });
					}
				}
			});	
		}
	</script>
</body>
</html>
