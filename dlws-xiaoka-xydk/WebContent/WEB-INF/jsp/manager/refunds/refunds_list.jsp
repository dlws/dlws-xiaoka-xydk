<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/base.css">
<link rel="stylesheet" href="<%=basePath%>/v2/console/css/style.css">
<script src="<%=basePath%>/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="<%=basePath%>/v2/console/js/globle.js"></script>
<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
<div class="column-name">
	<h2>
		退款管理><b>退款管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="<%=basePath%>/refunds/refundsList.html" method="post">
	<div class="main-top">
		<div class="fr" align="center">
			<span class="c-name">用户名</span> 
			<input type="text" name="name" class="input mr10" value="${paramsMap.name }"placeholder="请输入用户名">
			<span class="c-name">订单号</span> 
			<input type="text" name="orderId" class="input mr10" value="${paramsMap.orderId }"placeholder="请输入订单号">
			<span class="c-name">审核状态</span> 
			<select name="checkStatus" class="input">
				<option value="">全部</option>
				<c:choose>
					<c:when test="${paramsMap.checkStatus == '0' }">
						<option value="0" selected="selected">待审核</option>
					</c:when>
					<c:otherwise>
						<option value="0">待审核</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${paramsMap.checkStatus == '1' }">
						<option value="1" selected="selected">审核通过</option>
					</c:when>
					<c:otherwise>
						<option value="1">审核通过</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${paramsMap.checkStatus == '2' }">
						<option value="2" selected="selected">审核不通过</option>
					</c:when>
					<c:otherwise>
						<option value="2">审核不通过</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${paramsMap.checkStatus == '3' }">
						<option value="3" selected="selected">商家拒绝</option>
					</c:when>
					<c:otherwise>
						<option value="3">商家拒绝</option>
					</c:otherwise>
				</c:choose>
			</select>
			<span class="c-name">信息来源</span> 
			<select name="orderSourceStatus" class="input">
				<option value="">全部</option>
				<c:choose>
					<c:when test="${paramsMap.orderSourceStatus == '0' }">
						<option value="0" selected="selected">拒绝接单</option>
					</c:when>
					<c:otherwise>
						<option value="0">拒绝接单</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${paramsMap.orderSourceStatus == '1' }">
						<option value="1" selected="selected">商家退款</option>
					</c:when>
					<c:otherwise>
						<option value="1">商家退款</option>
					</c:otherwise>
				</c:choose>
			</select>
			<span class="c-name">银行账户</span> 
			<input type="text" name="bankCardNumber" class="input mr10" value="${paramsMap.bankCardNumber }"placeholder="请输入银行账户"> 
			<span class="c-name">时间：</span> 
				<input type="text" value="${paramsMap.beginDate}" class="input"
				name="beginDate" id="begindate"
				onClick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')||\'%y-%M-%d\'}'})"
				readonly="readonly" />--
				<input type="text" value="${paramsMap.endDate}" class="input"
				name="endDate" id="enddate"
				onClick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\')}',maxDate:'%y-%M-%d'})"
				readonly="readonly" />
			<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>退款用户</th>
					<th>退款订单</th>
					<th>退款金额</th>
					<th>审核人</th>
					<th>审核状态</th>
					<th>卖家响应</th>
					<th>开户银行</th>
					<th>开户银行账号</th>
					<th>开户银行全称</th>
					<th>信息来源</th>
					<th>审核日期</th>
					<th>创建日期</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="refunds">
					<tr>
						<c:choose>
							<c:when test="${refunds.name !=null}">
								<td>${refunds.name }</td>
							</c:when>
							<c:otherwise>
								<td>匿名</td>
							</c:otherwise>
						</c:choose>
						<td><a href="${pageContext.request.contextPath}/refunds/details.html?id=${refunds.id}" class="">${refunds.refundsOrderId }</a></td>
						<td>${refunds.refundsMoney }</td>
						<td>${refunds.checkPeople }</td>
						<td><c:choose>
								<c:when test="${refunds.checkStatus==0}">
											待审核
										</c:when>
								<c:when test="${refunds.checkStatus==1}">
											审核通过
										</c:when>
								<c:when test="${refunds.checkStatus==2}">
											审核不通过
										</c:when>
								<c:when test="${refunds.checkStatus==3}">
											商家拒绝
										</c:when>
								<c:when test="${refunds.checkStatus==4}">
											退款已申请
										</c:when>
							</c:choose></td>
						<td>
							<c:choose>
								<c:when test="${refunds.orderStatus==6}">
									未操作
								</c:when>
								<c:when test="${refunds.orderStatus==7 || refunds.orderStatus == 8}">
									同意退款
								</c:when>
								<c:otherwise>
									拒绝退款
								</c:otherwise>
							</c:choose>
						</td>
						<td>${refunds.bank }</td>
						<td>${refunds.bankCardNumber }</td>
						<td>${refunds.AccountFullName }</td>
						<td><c:choose>
								<c:when test="${refunds.orderSourceStatus==0 }">
											拒绝接单
										</c:when>
								<c:when test="${refunds.orderSourceStatus==1 }">
											商家退款
										</c:when>
							</c:choose></td>
						<td><fmt:formatDate value="${refunds.checkTime }" type="both"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td><fmt:formatDate value="${refunds.createTime }"
								type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td><c:choose>
								<c:when test="${refunds.checkStatus==0}">
									<a
										href="<%=basePath%>/refunds/getRefundsById.html?id=${refunds.id}"
										class="">审核</a>
								</c:when>
								<c:otherwise>
											审核
		   						</c:otherwise>
							</c:choose>
							<a href="${pageContext.request.contextPath}/refunds/details.html?id=${refunds.id}" class="">查看详情</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
	<div class="popBox chaLianjie-pop">
		<div class="popBox-back"></div>
		<div class="popBox-box">
			<div class="popBox-con">
				<div class="chaLianjie-con">
					<div class="tit" id="lookurl"></div>
					<a href="javascript:;" class="btn w144">确 认</a>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	
</script>

