<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
				<div class="column-name">
					<h2>钱包管理 ><b>钱包管理</b></h2>
				</div>
				<form id = "form1" name="form1" action="<%=basePath%>/managerWallet/list.html" method="post">
				<div class="main-top">
					<div class="fr" align="center">
						<span class="c-name">城市</span> 
						<input type="text" name="cityName" class="input mr10" value="${paramsMap.cityName }" placeholder="请输入城市">
						<span class="c-name">学校</span> 
						<input type="text" name="schoolName" class="input mr10" value="${paramsMap.schoolName }" placeholder="请输入学校">
						<span class="c-name">用户名</span> 
						<input type="text" name="name" class="input mr10" value="${paramsMap.name }" placeholder="请输入用户名">
						<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
					</div>
				</div>
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th>用户名</th>
								<th>城市</th>
								<th>学校</th>
								<th>入驻名称</th>
								<th>联系人</th>
								<th>联系电话</th>
								<th>联系邮箱</th>
								<th>微信号</th>
								<th>钱包余额</th>
								<th>创建时间</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="wallet">
							<!-- 遍历数据 -->
							<tr>
								<td>${wallet.name }</td>
								<td>${wallet.cityName }</td>
								<td>${wallet.schoolName }</td>
								<td>${wallet.userName }</td>
								<td>${wallet.contactUser }</td>
								<td>${wallet.phoneNumber }</td>
								<td>${wallet.email }</td>
								<td>${wallet.wxNumber }</td>
								<td>${wallet.balance }</td>
								<td>${wallet.createTime }</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
	</form>