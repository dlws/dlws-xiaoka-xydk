<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>轮播图管理> <b>轮播图修改</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>bannerInfo/updateBannerInfo.html" id="bannerForm" method="post" enctype="multipart/form-data">
						<input id="id" type="hidden" name="id" value="${objMap.id }">
						<div class="form-item">
							<div class="form-name">轮播图名称:</div>
							<div class="form-input">
								<input name="name" type="text" class="input" id="name" value="${objMap.name }">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">排列顺序:</div>
							<div class="form-input">
								<input name="ord" id="ord" type="text" class="input" value="${objMap.ord }" onkeyup="value=value.replace(/[^\-?\d.]/g,'')">
							</div>
						</div>
						<div class="form-item">
						
						<div class="form-name">轮播图类型</div>
							<div class="form-input">
								
								<select class="input" name="bannerType" id="bannerTypeId">
									<c:forEach items="${bannerTypes }" var="info">
										<c:if test="${objMap.bannerType==info.dic_value }">
											<option value="${info.dic_value }" selected="selected">${info.dic_name }</option>
										</c:if>
										<c:if test="${objMap.bannerType!=info.dic_value }">
											<option value="${info.dic_value }">${info.dic_name }</option>
										</c:if>
									</c:forEach>
								</select> 
							</div>
						</div>
						
						<div class="form-item">
							<div class="form-name">轮播图链接:</div>
							<div class="form-input">
								<input name="redirectUrl" id="redirectUrl" type="text" class="input" value="${objMap.redirectUrl }">
							</div>
						</div>
						
						<div class="form-item form-item01">
							<div class="form-name">上传图片:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value="${objMap.keyValue }"/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${objMap.picUrl }"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl" src="${objMap.picUrl}">
    							</div>
							</div>
						</div>
						<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="addBanner()">
						</div>
						</form>
		<div class="popBox zuobiao-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-title">
					<h3>获取坐标</h3>
					<a href="javascript:;" class="close"></a>
				</div>
				<div class="popBox-con">
					<div class="zuobiao-con">
						<form action="" method="get">
						请输入位置：<input id="where" name="where" class="input" type="text" >
						<input type="button" class="btn-blue" value="地图上找" onClick="sear(document.getElementById('where').value);" />
								<div id="container" style="height:400px; width:480px;margin-top:8px;" >
								</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- zuobiao-pop -->
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script>
	$(function(){
		$("#endTime").click(function(){
		
		var mtime=$("#beginTime").val();
			
	 		WdatePicker({minDate:mtime})
		}
		
		)
	
	
	});
		function addBanner(){
			
			var flag = true;
			var name = $.trim($("#name").val());
			var id = $.trim($("#id").val());
			var ord = $.trim($("#ord").val());
			var redirectUrl = $.trim($("#redirectUrl").val());
			var picUrl = $.trim($("#picUrl").val());
			if(name==""){
				alert("轮播图名称不能为空!");
				flag = false;
			}
			if(name.length>20){
				alert("轮播图名称长度最多为20个字!");
				flag = false;
			}
			if(ord==""){
				alert("请设置排列顺序!");
				flag = false;
			}
			if(redirectUrl==""){
				alert("请设置图片的点击跳转地址!");
				flag = false;
			}
			
			if(picUrl==""){
				alert("上传图片不能为空!");
				flag = false;
			}
			
			if(flag){
				$.ajax({
					type : "post",
					url : "<%=basePath%>bannerInfo/judgeRepeat.html",
					dataType : "json",
					data : {name:name,id:id},
					success : function(data) {
						
						if(data.isEmpty != "yes"){
							alert("该轮播图名称已经存在!");
						}else{
							$("#bannerForm").submit();
						}
					}
				});
			}
		}
			//上传图片
			function uploadPic(){
				//异步上传
				var options = {
						url : "<%=basePath %>bannerInfo/picture.html",
						dataType : "json",
						type : "post",
						success : function(data){
							//回调 路径  data.path
							$("#allUrl").attr("src",data.picUrl);
							$("#keyValue").val(data.key);
							$("#picUrl").val(data.picUrl);
						}
				}
				$("#bannerForm").ajaxSubmit(options);
			}
		function querySchool(value){
			
			$.ajax({
				type : "post",
				url : "<%=basePath%>v2busyShopBasicinfo/querySchoolByCity.html",
				dataType : "json",
				data : {code:value},
				success : function(data) {
					var $se = $("#school option").remove();
					var $sel = $("#school");
					for(var i=0;i<=data.school.length;i++){
						var $option = $("<option value = '"+data.school[i].schoolId+"'>"+data.school[i].schoolName +"</option>");
						$sel.append($option);
					}
				},
			});
		}
</script>
</div>
