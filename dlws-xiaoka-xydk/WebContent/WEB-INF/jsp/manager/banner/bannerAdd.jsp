<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
	<script src="<%=basePath %>/v2/console/js/jquery.form.js"></script>
	<script src="<%=basePath %>/v2/console/My97DatePicker/WdatePicker.js"></script>
	
				<div class="column-name">
					<h2>广告管理 > <b>轮播图添加</b></h2>
				</div>
				<div class="form1" name="form1">
					<form action="<%=basePath%>bannerInfo/add.html" id="bannerForm" method="post" enctype="multipart/form-data">
						<div class="form-item">
							<div class="form-name">轮播图名称:</div>
							<div class="form-input">
								<input name="name" type="text" class="input" id="name" value="${bannerinfo.bannerName }">
								<div class="clearfix"></div>
								<div class="form-msg"></div>
							</div>
						</div>
						<div class="form-item">
							<div class="form-name">排列顺序:</div>
							<div class="form-input">
								<input name="ord" id="ord" type="text" class="input" value="${bannerinfo.number }" onkeyup="value=value.replace(/[^\-?\d.]/g,'')">
							</div>
						</div>
						<div class="form-item">
					<div class="form-name">轮播图类型</div>
						<div class="form-input">
							<select class="input" name="bannerType" id="bannerType">
								<c:forEach items="${bannerTypes }" var="info">
									<option value="${info.dic_value }">${info.dic_name }</option>
								</c:forEach>							
									<!-- <option value="1">首页</option>
									<option value="2">发布页</option> -->
							</select> 
						</div>
					</div>
						<div class="form-item">
							<div class="form-name">轮播图链接:</div>
							<div class="form-input">
								<input name="redirectUrl" id="redirectUrl" type="text" class="input" value="${bannerinfo.picUrl }">
							</div>
						</div>
						<div class="form-item form-item01">
							<div class="form-name">上传图片:</div>
							<div class="form-input">
								<button class="btn-blue" style="margin-left:0;">上传图片</button>
								<input type="hidden" name='keyValue' id='keyValue' class='input' value="${key }"/> 
								<input type="hidden" name='picUrl' id='picUrl' class='input' value="${picUrl }"/>
    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
    							<div class="clearfix"></div>
    							<div class="form-img">
    								<img id="allUrl" src="${picUrl}">
    							</div>
							</div>
						</div>
						<br/>
						<div style="text-align:center; vertical-align:middel;">
							<input type="button" class="btn w144" value="保  存" onclick="addBanner()">
						</div>
						</form>
						<!-- 日期 -->
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>/v2/console/js/jquery-ui-timepicker-addon.min.js"></script>
<script>
		function addBanner(){
			
			var flag = true;
			var name = $.trim($("#name").val());
			var ord = $.trim($("#ord").val());
			var redirectUrl = $.trim($("#redirectUrl").val());
			var picUrl = $.trim($("#picUrl").val());
			if(name==""){
				alert("轮播图名称不能为空!");
				flag = false;
			}
			if(name.length>20){
				alert("轮播图名称长度最多为20个字!");
				flag = false;
			}
			if(ord==""){
				alert("请设置排列顺序!");
				flag = false;
			}
			if(redirectUrl==""){
				alert("请设置图片的点击跳转地址!");
				flag = false;
			}
			
			if(picUrl==""){
				alert("上传图片不能为空!");
				flag = false;
			}
			
			if(flag){
				$("#bannerForm").submit();
			}
		}
		//上传图片
		function uploadPic(){
			//异步上传
			var options = {
					url : "<%=basePath %>bannerInfo/picture.html",
					dataType : "json",
					type : "post",
					success : function(data){
						$("#allUrl").attr("src",data.picUrl);
						$("#keyValue").val(data.key);
						$("#picUrl").val(data.picUrl);
					}
			}
			$("#bannerForm").ajaxSubmit(options);
		}		
</script>
</div>
