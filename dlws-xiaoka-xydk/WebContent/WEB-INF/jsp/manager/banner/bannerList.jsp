<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/base.css">
	<link rel="stylesheet" href="<%=basePath %>/v2/console/css/style.css">
	<script src="<%=basePath %>/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath %>/v2/console/js/globle.js"></script>
				<div class="column-name">
					<h2>广告管理 ><b>轮播图管理</b></h2>
				</div>
				<div class="main-top">
					<div class="fl">
						<a href="<%=basePath%>bannerInfo/toAdd.html" class="btn btn-p">新建轮播图</a>
					</div>
				</div>
				<form id = "form1" name="form1">
				<div class="content">
					<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<!-- <th>轮播图名称</th> -->
								<th>排列顺序</th>
								<th>图片链接</th>
								<th>轮播图类型</th>
								<!-- <th>创建人</th> -->
								<th>创建时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${po.datasource }" var="bannerinfo">
							<!-- 遍历数据 -->
							<tr>
								<%-- <td>${bannerinfo.name }</td> --%>
								<td>${bannerinfo.ord }</td>
								<td>${bannerinfo.redirectUrl }</td>
								<td>${bannerinfo.dic_name }
									<%-- <c:if test="${bannerinfo.dic_value}">${bannerinfo.dic_name}</c:if>
									<c:if test="${bannerinfo.bannerType ==2}">发布页</c:if> --%>
								</td>
								<%-- <td>${bannerinfo.createUser }</td> --%>
								<td>${bannerinfo.createDate }</td>
								<td>
								
									<c:if test="${bannerinfo.isUse == 0}">
										<a href="<%=basePath%>/bannerInfo/isUse.html?id=${bannerinfo.id}&isUse=1" class="">上线</a>
									</c:if>
									<c:if test="${bannerinfo.isUse == 1}">
										<a href="<%=basePath%>/bannerInfo/isUse.html?id=${bannerinfo.id}&isUse=0" class="">下线</a>
									</c:if>
									<a href="<%=basePath%>/bannerInfo/toUpdateBannerInfo.html?id=${bannerinfo.id}&temp='update'" class="">修改</a>
									<a href="<%=basePath%>/bannerInfo/deleteBannerInfo.html?id=${bannerinfo.id}&isDelete=1" class="">删除</a>
								</td>
							</tr>						
						</c:forEach>
						</tbody>
					</table>
						<div id="page">
							<%@ include file="/v2/console/common/paging.jsp"%>	
						</div>
				</div>
		<div class="popBox chaLianjie-pop">
			<div class="popBox-back"></div>
			<div class="popBox-box">
				<div class="popBox-con">
					<div class="chaLianjie-con">
						<div class="tit" id="lookurl"></div>
						<a href="javascript:;" class="btn w144">确  认</a>
					</div>
				</div>
			</div>
		</div><!-- chaLianjie-pop -->
			</form>
		<script type="text/javascript">
				function confirmDelete(ID){
					if (confirm("是否确认")) {
						location.href='<%=basePath%>v2BannerInfo/deleteV2BannerInfo.html?bannerId='+ID;
					}  else  { 
						return false;
					};
					
				}
				function lookUrl(ID){
					$.ajax({
						type : "post",
						url : "<%=basePath%>v2BannerInfo/getV2BannerInfo.html",
						dataType : "json",
						data : {bannerId:ID},
						success : function(data) {
							var $picurl=data.picUrl;
							var $url = $("#lookurl");
							$url.append($picurl);
						},
					});
					
				}
		</script>

				