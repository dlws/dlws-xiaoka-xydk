<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>修改income</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
</head>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>优选服务管理 > <b>查看优选服务详情</b></h2>
				</div>
				<div class="form">
					<form id="updateincome" action="/preferredService/updatePreferredService.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
						
						
						<div class="form-item">
							<div class="form-name">入驻类型：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="enterClass" name="enterClass" value="${obj.enterClass} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">类型名字：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="className" name="className" value="${obj.className} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">服务名称：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="serviceName" name="serviceName" value="${obj.serviceName} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">服务介绍：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="introduce" name="introduce" value="${obj.introduce} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">价格：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="price" name="price" value="${obj.price} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">1:线上；2：线下：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="serviceType" name="serviceType" value="${obj.serviceType} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">销售量：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="salesVolume" name="salesVolume" value="${obj.salesVolume} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">图片路径：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="imgUrl" name="imgUrl" value="${obj.imgUrl} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						<div class="form-item">
							<div class="form-name">是否删除：</div>
							<div class="form-input">
								<input AutoComplete="off" type="text" class="input" id="isDelete" name="isDelete" value="${obj.isDelete} " readonly="readonly">
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
						
						
						<div class="form-item">
							<div class="form-name">创建时间：</div>
							<div class="form-input">
								<input type="text"  class="input" name="createDate" id="createDate"  value="${obj.createDate} " readonly="readonly"/>
								<div class="clearfix"></div>
								<div class="form-msg hide" style="color: red;"></div>
							</div>
						</div>
						
				</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
//上传图片
function uploadPic(){
	//异步上传
	var options = {
			url : "${path}/preferredService/uploadImg.html",
			dataType : "json",
			type : "post",
			success : function(data){
				//回调 路径  data.path
				$("#allUrl").attr("src",data.url);
				$("#path").val(data.path);
			}
	}
	$("#income").ajaxSubmit(options);
}
</script>