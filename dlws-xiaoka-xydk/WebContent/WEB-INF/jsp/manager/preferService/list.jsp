
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String contextPath = request.getContextPath();
%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
       function goSearch(){
       $("#form1").submit();
       }
</script>
	<div class="column-name">
		<h2>
			优选服务管理 > <b>优选服务列表</b>
		</h2>
	</div>
<form id="form1" name="form1" action="${path}/preferredService/preferredServiceList.html" method="post">

	<div class="main-top">
		<div class="fl">
			<a href="${path}/preferredService/toAddPreferredService.html" class="btn btn-p">新建优选服务</a>
		</div>
		<div class="fr" align="center">
		
			<span class="c-name">服务名称</span>
			<input type="text" name="className" class="input mr10" value="${paramsMap.className }" placeholder="请输入服务名称关键字"> 
			<input type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>
	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
							<!-- 暂定为不显示入驻类型 -->
						    <th  style="display:none">入驻类型</th>
						    <th>类型名称</th>
						    <th>服务名称</th>
						    <th>服务介绍</th>
						    <th>服务价格</th>
						    <th>服务端分成比例</th>
						    <th>外交官分成比例</th>
						    <th>平台分成比例</th>
						    <th>服务类型</th>
						    <th>销售量</th>
						    <th>创建时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource}" var="data">
				<tr>
							<!-- 暂定为不显示入驻类型 -->
							<td style="display:none">${data.enterClass}</td>
							<td>${data.className}</td>
							<td>${data.serviceName}</td>
							<td width="10%">
								<div style="width:120px;text-overflow:ellipsis;white-space: nowrap;overflow: hidden;">
									${data.introduce}
								</div>
							</td>
							<td>${data.price}</td>
							<td>${data.serviceRatio}</td>
							<td>${data.diplomatRatio}</td>
							<td>${data.platformRatio}</td>
							<%-- <td>${data.classId}</td> --%>
							<c:if test="${empty data.classId}">
								<td>${data.classId}</td>
							</c:if>
							<c:if test="${empty data.classId}">
								<td>${data.dic_name}</td>
							</c:if>
							
							<td>
								<c:if test="${data.serviceType==0}">线上</c:if>
								<c:if test="${data.serviceType==1}">线下</c:if>
							</td>
							<td>${data.salesVolume}</td>
							<td><fmt:formatDate  value="${data.createDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				<td>
					<!-- 修改--> 
					<a class="" href="${path}/preferredService/toUpdatePreferredService.html?id=${data.id}">修改</a>
					<!-- 是否删除判断 -->
					<a href="javascript:;" data-id="${path}/preferredService/delPreferredService.html?id=${data.id}" class="remove">删除</a> 
				</td>
				</tr>	
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a>
					<a href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
