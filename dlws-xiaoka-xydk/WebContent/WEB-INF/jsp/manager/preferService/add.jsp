<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
	<title>添加income</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/style.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/My97DatePicker/WdatePicker.js"></script>
	<script src="${path}/v2/console/js/jquery.form.js"></script>
	<script type="text/javascript"	src="${path}/kindeditor/kindeditor-min.js"></script>
	
</head>
<script >
$(function(){
	
	$("#income").submit(function(){
		var isSubmit = true;
		//分两步：
		//第一步做必填字段的校验
		$(this).find("[reg2]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
				isSubmit = false;
				//在jQuery跳出循环不再是break;也部署return;是return false;
				return false;//跳出循环
			}else{
				var inputName = $(this).attr("name");
				
				if(inputName == "brandName"){
					if(validBrandName(val)){
						$(this).parents(".form-input").find(".form-error").html("<font color='red'>品牌名称已存在</font>");
						isSubmit = false;
						return false;//跳出循环
					}else{
						$(this).parents(".form-input").find(".form-error").html("");
					}
				}else{
					$(this).parents(".form-input").find(".form-error").html("");
				}
			}
		});
		
		$("#income").find("[reg2]").blur(function(){
			
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
			}else{
				$(this).parents(".form-input").find(".form-error").html("");
			}
		});
		$(this).find("[reg1]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg1");
			
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
				isSubmit = false;
				//在jQuery跳出循环不再是break;也部署return;是return false;
				return false;//跳出循环
			}else{
				var inputName = $(this).attr("name");
				
				var pid = $(this).attr("id");
				var p = $("#"+pid).val();
				if(p == null || $.trim(p) == ""){
					$(this).parents(".form-input").find(".form-error").html(tip);
					isSubmit = false;
					return false;//跳出循环
				}
				
			}
		});
	
		
		/* var imgUrl = $("#imgUrl").val();//图片
		if(imgUrl==""){
			alert("图片不能为空!")
			isSubmit = false;
		} */
		return isSubmit;
	}); 
	
})
</script>
<body>
	<div id="wrapper" >
		<div class="main">
			<div class="main-right">
				<div class="column-name">
					<h2>优选服务管理 > <b>添加优选服务</b></h2>
				</div>
				<div class="form">
					<form id="income" name="incomeform" action="${path }/preferredService/addPreferredService.html" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="${obj.id}">
								
								<%-- <div class="form-item">
									<div class="form-name">入驻类型：</div>
									<div class="form-input" style="margin-top: 24px">
											<c:forEach var="map"  items="${enterList}" varStatus="status">
												<input type="checkbox"  name="enterClass" id="enterClass" class="lx"
												 value="${map.id}">${map.typeName }
											</c:forEach>
										<div class="clearfix"></div>
										<div class="form-msg hide" style="color: red;"></div>
									</div>
								</div> --%>
						
								<div class="form-item">
									<div class="form-name">类型名称：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="className" name="className" placeholder="请输入类型名称" reg2="^[a-zA-Z0-9\_\u4e00-\u9fa5]{1,20}$" tip="类型名称为中英文或数字字符和下划线，长度1-20" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">服务名称：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="serviceName" name="serviceName" placeholder="请输入服务名称" reg2="^[^\s]{1,20}$" tip="类型名字为中英文或数字字符，长度1-20" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">服务介绍：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="introduce" name="introduce" placeholder="请输入服务介绍" reg2="^[^\s]{1,1000}$" tip="服务介绍为中英文或数字字符，长度1-100" / >
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">服务价格：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="price" name="price"  placeholder="请输入服务价格" reg1=""  tip="服务价格不能为空" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
								
								<div class="form-item">
									<div class="form-name">服务端分成比例：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="serviceRatio" name="serviceRatio"  placeholder="请输入服务端分成比例" reg1=""  tip="服务端分成比例不能为空" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
								
								<div class="form-item">
									<div class="form-name">外交官分成比例：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="diplomatRatio" name="diplomatRatio"  placeholder="请输入外交官分成比例" reg1=""  tip="外交官分成比例不能为空" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
								
								<div class="form-item">
									<div class="form-name">平台分成比例：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="platformRatio" name="platformRatio"  placeholder="请输入平台分成比例" reg1=""  tip="平台分成比例不能为空" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
								
								<!-- <div class="form-item">
									<div class="form-name">服务分类：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="classId" name="classId"  placeholder="请输入服务分类" reg1=""  tip="服务分类不能为空" />
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div> -->
								
								<div class="form-item">
									<div class="form-name">服务分类：</div>
										<div class="form-input">
											<select class="input" name="classId" id="classId">
												<c:forEach items="${preferServiceClass }" var="info">
													<option value="${info.id}" >${info.dic_name}</option>
												</c:forEach>							
											</select> 
										</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">服务类型：</div>
									<div class="form-input">
										<select class="input" name="serviceType" id="serviceType">
												<option value="1">线上</option>
												<option value="2">线下</option>
										</select>
										<div class="clearfix"></div>
									</div>
								</div>
						
								<div class="form-item">
									<div class="form-name">销售量：</div>
									<div class="form-input">
										<input AutoComplete="off" type="text" class="input" id="salesVolume" name="salesVolume" placeholder="请输入销售量" reg2="^[0-9]{1,5}$" tip="销售量数字，长度1-5" / >
										<div class="clearfix"></div>
										<div class="form-error red"></div>
									</div>
								</div>
						
								<div class="form-item form-item01">
									<div class="form-name">服务图片一:</div>
									<div class="form-input">
										<button class="btn-blue" style="margin-left:0;">上传图片</button>
										<input type="hidden" name='keyValue' id='keyValue' class='input' value="${key }"/> 
										<input type="hidden" name='imgUrl' id='imgUrl' class='input' value="${imgUrl }" reg1=""  tip="服务图片不能为空"/>
		    							<input type="file" name="pic" class="file input" id="fileField" onchange="uploadPic()"/>
		    							<div class="clearfix"></div>
		    							<div class="form-img">
		    								<img id="allUrl" src="${imgUrl}">
		    							</div>
		    							<div class="form-error red"></div>
									</div>
								</div>
								
								<div class="form-item form-item01">
									<div class="form-name">服务图片二:</div>
									<div class="form-input">
										<button class="btn-blue" style="margin-left:0;">上传图片</button>
										<input type="hidden" name='keysValue' id='keysValue' class='input' value="${key }"/> 
										<input type="hidden" name='bannerUrl' id='bannerUrl' class='input' value="${bannerUrl }" reg1=""  tip="服务图片不能为空"/>
		    							<input type="file" name="bic" class="file input" id="fileField" onchange="uploadPicbanner()"/>
		    							<div class="clearfix"></div>
		    							<div class="form-img">
		    								<img id="baUrl" src="${bannerUrl}">
		    							</div>
		    							<div class="form-error red"></div>
									</div>
								</div>
						
					<button type="submit" onclick="SubAdd()" class="btn w144">保  存</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">

function SubAdd(){
	var obj=document.getElementsByName('enterClass');
		var s=''; 
		for(var i=0; i<obj.length; i++){ 
			if(obj[i].checked){
				s+=obj[i].value+','; //如果选中，将value添加到变量s中 
			}
		}
		s.substring(0,s.length-1);
		$("#enterClass").val(s);
		
}


//上传图片
function uploadPic(){
	//异步上传
	var options = {
			url : "${path}/preferredService/picture.html",
			dataType : "json",
			type : "post",
			success : function(data){
				$("#allUrl").attr("src",data.imgUrl);
				$("#keysValue").val(data.key);
				$("#imgUrl").val(data.imgUrl);
			}
	}
	$("#income").ajaxSubmit(options);

}

//上传图片
function uploadPicbanner(){
	//异步上传
	var options = {
			url : "${path}/preferredService/bannerpicture.html",
			dataType : "json",
			type : "post",
			success : function(data){
				$("#baUrl").attr("src",data.bannerUrl);
				$("#keyValue").val(data.key);
				$("#bannerUrl").val(data.bannerUrl);
			}
	}
	$("#income").ajaxSubmit(options);

}

</script>
