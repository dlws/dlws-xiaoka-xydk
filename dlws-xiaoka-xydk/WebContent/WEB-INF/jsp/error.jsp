<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.jzwl.common.utils.Util" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>出错了!</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="${path}/xkh_version_2/css/style.css" />
	<title>提示</title>
  </head>
  	<%  
  		//error 页面跳转，判定当前是手机端展示还是PC端展示
  		boolean flag = false;
		String agent = request.getHeader("User-Agent");
		flag = Util.isMobPlat(agent);
  	%>
  	<body >
  		<c:set var="flag" value="<%=flag%>"></c:set>
  		<c:choose>
  			<c:when test="${flag}">
  				<div class="error_content cue_content">
					<img src="${path}/xkh_version_2/img/error.png" class="search_pic" />
					<div class="error_img"></div>
						<p>出错了！</p>
					<a href="${path}/home/index.html">正在为您跳转...</a>
				</div>
  			</c:when>
  			<c:otherwise>
  				<br/><br/><br/><br/>
				<div align="center">
					<font color="black" size="7">出错了！</font>
				  	<br/><br/>
				    <font color="red" size="5"><c:out value="${ msg }"/></font>
				</div>
  			</c:otherwise>
  		</c:choose>
		
	</body>
	<% if(flag){%>
	<script>
		onload=function(){ //在进入网页的时候加载该方法 
			setTimeout(go, 3000); /*在js中是ms的单位*/ 
		}; 
		function go(){ 
			location.href="${path}/home/index.html"; 
		} 
	</script>
	<%}%>
	
	
</html>


