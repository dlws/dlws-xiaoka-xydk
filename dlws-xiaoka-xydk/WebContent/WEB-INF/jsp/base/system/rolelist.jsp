<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/js/system/disAuthority.js"></script>

<div class="column-name">
	<h2>
		系统管理 > <b>角色管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="${path}/v2sysrole/sysroleList.html" method="post">
	<div class="main-top">
		<div class="fl">
			<a href="${path}/v2sysrole/toAddRole.html" class="btn btn-p">新建角色</a>
		</div>
		<div class="fr">
			<span class="c-name">角色名称</span> <input type="text" name="name"
				class="input mr10" value="${paramsMap.name }"> <input
				type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>

	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>角色名称</th>
					<th>描述信息</th>
					<th>创建时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="map">
					<tr>
						<td>${map.name}</td>
						<td>${map.description}</td>
						<td><fmt:formatDate  value="${map.createdTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td><a
							href="${path}/v2sysrole/toUpdateRole.html?roleId=${map.roleId}">修改</a>
							<a href="javascript:;" data-aid="${map.roleId}"
							class="distribution">权限维护</a> <a href="javascript:;"
							data-id="${path}/v2sysrole/deleteSysrole.html?roleId=${map.roleId}"
							class="remove">删除</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!--  ====================================================  -->

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a> <a href="javascript:;"
						class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- remove-pop -->

<!-- 弹出框 -->
<div class="popBox select-pop" style="display:none">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-title">
			<h3>权限维护</h3>
			<a href="javascript:;" class="close"></a>
		</div>
		<div class="popBox-con">
			<div class="select-con">
				<div class="select-box">
					<div class="fl">
						<div class="select-tit">角色下权限</div>
						<select name="" multiple="multiple" id="selected" size="18">
						</select>
					</div>
					<div class="select-btns">
						<a href="javascript:;" class="select-btn01">&lt;</a> <a
							href="javascript:;" class="select-btn02">&gt;</a>
					</div>
					<div class="fr">
						<div class="select-tit">非角色下权限</div>
						<select name="" multiple="multiple" id="to-be-elected" size="18">
						</select>
					</div>
				</div>
				<div class="btns">
					<a href="javascript:;" class="btn w144">确 认</a> <a
						href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- select-pop -->