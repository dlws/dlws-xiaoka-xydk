<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>

<div class="column-name">
	<h2>
		系统管理 > <b>资源管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="${path}/v2Sysresource/v2sysresourceList.html" method="post">
	<div class="main-top">
		<div class="fl">
			<a href="${path}/v2Sysresource/toAddResource.html" class="btn btn-p">新建资源</a>
		</div>
		<div class="fr">
			<span class="c-name">组件名称</span> <input type="text" name="componentname"
				class="input mr10" value="${paramsMap.componentname }"> <input
				type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>

	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>组件id</th>
					<th>组件名称</th>
					<th>父组件id</th>
					<th>资源路径</th>
					<th>资源类型</th>
					<th>创建时间</th>
					<th>状态</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="map">
					<tr>
						<td>${map.componentid}</td>
						<td>${map.componentname}</td>
						<td>${map.parentid}</td>
						<td>${map.resourceUrl}</td>
						<c:if test="${map.type == 2}">
							<td>按钮</td>
						</c:if>
						<c:if test="${map.type == 1}">
							<td>路径</td>
						</c:if>
						<c:if test="${map.type == 0}">
							<td>菜单</td>
						</c:if>
						<td><fmt:formatDate  value="${map.createdTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<c:if test="${map.status == 0}">
							<td>失效</td>
						</c:if>
						<c:if test="${map.status == 1}">
							<td>正常</td>
						</c:if>
						<td><a
							href="${path}/v2Sysresource/toUpdateResource.html?resourceId=${map.resourceId}">修改</a>
							<a href="javascript:;"
							data-id="${path}/v2Sysresource/v2deleteResource.html?resourceId=${map.resourceId}"
							class="remove">删除</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!--  ====================================================  -->

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a> <a href="javascript:;"
						class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- remove-pop -->