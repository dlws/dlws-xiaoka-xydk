<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>

<div class="column-name">
	<h2>
		系统管理 > <b>部门管理</b>
	</h2>
</div>
<form id="form1" name="form1" action="${path}/v2Department/departmentListChildren.html?parentid=${parentid }" method="post">

	<div class="main-top">
		<div class="fl">
			<a href="${path}/v2Department/departmentAddChildren.html?parentid=${parentid }" class="btn btn-p">新建部门</a>
		</div>
	</div>

	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>部门名称</th>
					<th>部门字典</th>
					<th>父级部门名称</th>
					<th>部门描述</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="list">
					<tr>
						<td>${list.departmentname}</td>
						<td>${list.departmentcode}</td>
						<td>${departmentname }</td>
						<td>${list.description}</td>
						<td>
							<a href="${path}/v2Department/getDepartmentChildren.html?departmentId=${list.departmentId}&parentid=${parentid }">修改</a>
							<a href="javascript:;" data-id="${path}/v2Department/deleteDepartmentChildren.html?departmentId=${list.departmentId}&parentid=${parentid }" class="remove">删除</a>
							<a href="${path}/v2Department/departmentListChildren.html?parentid=${list.departmentId }&departmentname=${list.departmentname }">下级设置</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!--  ====================================================  -->

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a>
					<a href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- remove-pop -->