<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>

<div class="column-name">
	<h2>
		系统管理 > <b>字典管理</b>
	</h2>
</div>
<form id="form1" name="form1" action="${path}/v2DicTypeTwo/v2DicTypeListTwo.html" method="post">
	<div class="main-top">
		<div class="fl">
			<a href="${path}/v2DicTypeTwo/dictypeAdd.html" class="btn btn-p">新建字典</a>
		</div>
		<div class="fr">
			<span class="c-name">字典名称</span> <input type="text" name="name"
				class="input mr10" value="${paramsMap.name }"> <input
				type="button" class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>

	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>字典类型</th>
					<th>字典代码</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="list">
					<tr>
						<td>${list.dicName}</td>
						<td>${list.dicCode}</td>
						<td>
							<a href="${path}/v2DicTypeTwo/getV2DicType.html?dicId=${list.dicId}">修改</a>
							<a href="javascript:;" data-id="${path}/v2DicTypeTwo/deleteV2DicType.html?dicId=${list.dicId}" class="remove">删除</a>
							<a href="${path}/v2DicDataTwo/v2DicDataListTwo.html?dicId=${list.dicId}&typename=${list.dicName}">类型设置</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!--  ====================================================  -->

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a>
					<a href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- remove-pop -->