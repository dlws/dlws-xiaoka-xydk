<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script type="text/javascript">
$(function(){
	$("#form111").submit(function(){
		var isSubmit = true;
		//分两步：
		//第一步做必填字段的校验
		$(this).find("[reg2]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
				isSubmit = false;
				//在jQuery跳出循环不再是break;也部署return;是return false;
				return false;//跳出循环
			}else{
				var inputName = $(this).attr("dicName");
				if(inputName == "brandName"){
					if(validBrandName(val)){
						$(this).parents(".form-input").find(".form-error").html("<font color='red'>该字典名称已存在</font>");
						isSubmit = false;
						return false;//跳出循环
					}else{
						$(this).parents(".form-input").find(".form-error").html("");
					}
				}else{
					$(this).parents(".form-input").find(".form-error").html("");
				}
			}
		});
		
		//第二步做非必填字段的校验
		$(this).find("[reg1]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg1");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(val != null && $.trim(val) != "" && !regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
				isSubmit = false;
				return false;//跳出循环
			}else{
				$(this).parents(".form-input").find(".form-error").html("");
				
			}
		});
		return isSubmit;
	});
	
	$("#form111").find("[reg2]").blur(function(){
		//获得必填的字段的值
		var val = $(this).val();
		//获得正则表达式的字符串
		var reg = $(this).attr("reg2");
		//获得提示信息
		var tip = $(this).attr("tip");
		//创建正则表达式的对象
		var regExp = new RegExp(reg);
		if(!regExp.test(val)){
			$(this).parents(".form-input").find(".form-error").html(tip);
		}else{
			$(this).parents(".form-input").find(".form-error").html("");
		}
	});

});
</script>
<div class="column-name">
	<h2>
		系统管理 > <b>字典管理</b>>
		<c:if test="${type == 'add'}">
			<b>新建</b>
		</c:if>
		<c:if test="${type == 'edit'}">
			<b>修改</b>
		</c:if>
		
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/${url}" method="post">
		<div class="form-item">
			<div class="form-name">部门名称</div>
			<div class="form-input">
				<input type="text" class="input" placeholder="请输入字典名称" name="departmentname" value="${department.departmentname }" reg2="^[a-zA-Z0-9\u4e00-\u9fa5]{1,20}$" tip="必须是中英文或数字，长度1-20">
				<span></span>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		
		
		<div class="form-item">
			<div class="form-name">部门代码</div>
			<div class="form-input">
				<input type="text" name="departmentcode" class="input" value="${department.departmentcode }" reg2="^[a-zA-Z0-9\u4e00-\u9fa5]{1,20}$" tip="必须是中英文或数字，长度1-20">
				<span></span>
				<span></span>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		
		<div class="form-item">
			<div class="form-name">描述</div>
			<div class="form-input">
				<input type="text" name="description" class="input" value="${department.description }" reg2="^[a-zA-Z0-9\u4e00-\u9fa5]{1,20}$" tip="必须是中英文或数字，长度1-20">
				<span></span>
				<span></span>
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		
		<input type="hidden" name="departmentId" value="${department.departmentId }">
		<!-- 添加专用 -->
		<input type="hidden" name="parentid" value="${parentid }">
		<div class="form-item btns">
			<input type="submit" class="btn w144" value="保  存">
			<a href="#" target=main onclick ="javascript:history.go(-1);" class="btn-gray w144">取 消</a>
		</div>
	</form>
</div>
