<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>

<link rel="stylesheet" href="${path}/v2/console/css/ztree/demo.css" type="text/css">
<link rel="stylesheet" href="${path}/v2/console/css/ztree/zTreeStyle/zTreeStyle.css" type="text/css">
	
<script type="text/javascript" src="${path}/v2/console/js/ztree/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="${path}/v2/console/js/ztree/jquery.ztree.core-3.5.js"></script>	
<script type="text/javascript" src="${path}/v2/console/js/ztree/jquery.ztree.excheck-3.5.js"></script>

<script type="text/javascript">
//实现树状菜单
var setting = {
			check: {
				enable: true,
				nocheckInherit: true
			},
			data: {
				simpleData: {
					enable: true,
					idKey: "id",
					pIdKey: "pId",
					rootPId: 0
				}
			},
			//hopuji
		    callback: {
		        beforeClick: beforeClick
		    }
		};
	
	 function beforeClick(treeId, treeNode) {
			  document.getElementById("departmentname").value=treeNode.name;
			  document.getElementById("departmentId").value=treeNode.id;
		      if (treeNode.level == 0 ) {
		        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
		        zTree.expandNode(treeNode);
		        return false;
		      }
		      return true;
		    }
	
		function nocheckNode(e) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nocheck = e.data.nocheck,
			nodes = zTree.getSelectedNodes();
			if (nodes.length == 0) {
				alert("请先选择一个节点");
			}

			for (var i=0, l=nodes.length; i<l; i++) {
				nodes[i].nocheck = nocheck;
				
				zTree.updateNode(nodes[i]);
			}
		}
		
		
		


	function showData(){
		 $.ajax({
			 type: "post",
			 url: "${path}/v2Department/showData.html",
	         data: {},
	         dataType: "json",
	         success: function(data){
	        	 
	        	 $.fn.zTree.init($("#treeDemo"), setting, data);
	         },
	         error: function(XMLHttpRequest, textStatus, errorThrown) {
	             alert(XMLHttpRequest.status);
	             alert(XMLHttpRequest.readyState);
	             alert(textStatus);
	         },
		 });
		
		$("#nocheckTrue").bind("click", {nocheck: true}, nocheckNode);
		$("#nocheckFalse").bind("click", {nocheck: false}, nocheckNode);
	}

   function getSelectedNodes(){  
	        var zTree = $.fn.zTree.getZTreeObj("treeDemo"),  
	        nodes = zTree.getCheckedNodes(true),  
	        v = "";  
	        var ids="";  
	        for (var i=0, l=nodes.length; i<l; i++) {  
	            v += nodes[i].name + ",";  
	            ids+=nodes[i].id+",";  
	        }  
	          
	        if (ids.length > 0 ) ids = ids.substring(0, ids.length-1);  
	        alert(ids);  
	        if (v.length > 0 ) v = v.substring(0, v.length-1);  
	        //给父窗体updateContact.jsp中所属分类赋值  
	         window.opener.document.getElementById("cateSelIds").value=ids;  
	         window.opener.document.getElementById("cateSelName").value=v;  
	   }
   
   
   
   
   
   
   
   
$(function(){
	$("#form111").submit(function(){
		var isSubmit = true;
		//分两步：
		//第一步做必填字段的校验
		$(this).find("[reg2]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
				isSubmit = false;
				//在jQuery跳出循环不再是break;也部署return;是return false;
				return false;//跳出循环
			}else{
				var inputName = $(this).attr("name");
				if(inputName == "brandName"){
					if(validBrandName(val)){
						$(this).parents(".form-input").find(".form-error").html("<font color='red'>品牌名称已存在</font>");
						isSubmit = false;
						return false;//跳出循环
					}else{
						$(this).parents(".form-input").find(".form-error").html("");
					}
				}else{
					$(this).parents(".form-input").find(".form-error").html("");
				}
			}
		});
		
		//第二步做非必填字段的校验
		$(this).find("[reg1]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg1");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(val != null && $.trim(val) != "" && !regExp.test(val)){
				$(this).parents(".form-input").find(".form-error").html(tip);
				isSubmit = false;
				return false;//跳出循环
			}else{
				$(this).parents(".form-input").find(".form-error").html("");
				
			}
		});
		/* 
		if(isSubmit){
			tipShow("#importLoadDiv");
		}
		 */
		return isSubmit;
	});
	
	$("#form111").find("[reg2]").blur(function(){
		//获得必填的字段的值
		var val = $(this).val();
		//获得正则表达式的字符串
		var reg = $(this).attr("reg2");
		//获得提示信息
		var tip = $(this).attr("tip");
		//创建正则表达式的对象
		var regExp = new RegExp(reg);
		if(!regExp.test(val)){
			$(this).parents(".form-input").find(".form-error").html(tip);
		}else{
			$(this).parents(".form-input").find(".form-error").html("");
		}
	});
	
	//显示提示
	$("#form111").find("[reg1]").blur(function(){
		//获得必填的字段的值
		var val = $(this).val();
		//获得正则表达式的字符串
		var reg = $(this).attr("reg1");
		//获得提示信息
		var tip = $(this).attr("tip");
		//创建正则表达式的对象
		var regExp = new RegExp(reg);
		if(val != null && $.trim(val) != "" && !regExp.test(val)){
			$(this).parents(".form-input").find(".form-error").html(tip);
		}else{
			$(this).parents(".form-input").find(".form-error").html("");
			
		}
	});
})
</script>
<div class="column-name">
	<h2>
		商品管理 > <b>商品管理</b>
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/${url}" method="post">
		<div class="form-item">
			<div class="form-name">用户名称</div>
			<div class="form-input">
				<input type="text" name="username" placeholder="请输入长度1-20位的中英文、数字字符和下划线" class="input" value="${objMap.username }" reg2="^[a-zA-Z0-9\_\u4e00-\u9fa5]{1,20}$" tip="必须是中英文或数字字符和下划线，长度1-20">
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		<div class="form-item" ${style }>
			<div class="form-name">密码</div>
			<div class="form-input">
				<input type="password" ${disabled } placeholder="请输入长度6-16位的小写英文和数字" name="password" class="input" maxLength="16" reg2${style }="^[a-z0-9]{6,16}$" tip="必须是小写英文或数字字符，长度6-16">
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		<div class="form-item form-school">
			<div class="form-name">性别</div>
			<div class="form-input">
				<select name="sex" class="input">
					<c:forEach items="${sexList }" var="sex">
						<c:if test="${sex.value == objMap.sex }">
							<option value="${sex.value}" selected="selected">${sex.name }</option>
						</c:if>
						<c:if test="${sex.value != objMap.sex }">
							<option value="${sex.value}">${sex.name }</option>
						</c:if>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">电话</div>
			<div class="form-input">
				<input type="text" name="mobile" class="input" value="${objMap.mobile }" reg1="^[0-9]" tip="必须是格式正确的电话号码">
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">联系地址</div>
			<div class="form-input">
				<input type="text" name="address" class="input" value="${objMap.address }" reg1="^(.|\n){0,300}$" tip="任意字符，长度0-300">
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">状态</div>
			<div class="form-input">
				<select name="status" class="input">
					<option value="1" <c:if test="${objMap.status == 1 }">selected = "selected"</c:if>>已激活</option>
					<option value="0" <c:if test="${objMap.status == 0 }">selected = "selected"</c:if>>未激活</option>
				</select>
			</div>
		</div>
		<div class="form-item form-school">
			<div class="form-name">城市</div>
			<div class="form-input">
				<select name="citycode" class="input">
					<c:forEach items="${cityList }" var="city">
						<c:if test="${city.value == objMap.citycode }">
							<option value="${city.value}" selected = "selected">${city.name }</option>
						</c:if>
						<c:if test="${city.value != objMap.citycode }">
							<option value="${city.value}">${city.name }</option>
						</c:if>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-item form-item01">
			<div class="form-name">部门名称</div>
			<div class="form-input">
				<input  type='text' name='departmentname' id='departmentname' class='input' value="${objMap.departmentname }" reg2="^[a-zA-Z0-9\_\u4e00-\u9fa5]{1,20}$" tip="请选择部门名称"/> 
   				<button type="button" class="btn-blue" onclick="showData()">部门设置</button>
			</div>
		</div>
		<div class="form-item">
			<div class="form-name">&nbsp;</div>
			<div class="form-input">
				<div class="zTreeDemoBackground left">
					<ul id="treeDemo" class="ztree"></ul>
				</div>
			</div>
		</div>
		<input type="hidden" name="id" value="${objMap.id }">
		<input type="hidden" id="departmentId" name="departmentId" value="${objMap.departmentId }">
		<input type="hidden" name="pwhide" value="${objMap.password }">
		<input type="submit" class="btn w144" value="保  存">
	</form>
</div>


