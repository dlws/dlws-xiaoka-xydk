<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>

<script type="text/javascript">
$(function(){
	$("#form111").submit(function(){
		var isSubmit = true;
		//分两步：
		//第一步做必填字段的校验
		$(this).find("[reg2]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(!regExp.test(val)){
				$(this).next("span").html("<font color='red'>"+tip+"</font>");
				isSubmit = false;
				//在jQuery跳出循环不再是break;也部署return;是return false;
				return false;//跳出循环
			}else{
				var inputName = $(this).attr("name");
				if(inputName == "brandName"){
					if(validBrandName(val)){
						$(this).next("span").html("<font color='red'>品牌名称已存在</font>");
						isSubmit = false;
						return false;//跳出循环
					}else{
						$(this).next("span").html("");
					}
				}else{
					$(this).next("span").html("");
				}
			}
		});
		
		//第二步做非必填字段的校验
		$(this).find("[reg1]").each(function(){
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg1");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if(val != null && $.trim(val) != "" && !regExp.test(val)){
				$(this).next("span").html("<font color='red'>"+tip+"</font>");
				isSubmit = false;
				return false;//跳出循环
			}else{
				$(this).next("span").html("");
				
			}
		})
		
		if(isSubmit){
			tipShow("#importLoadDiv");
		}
		return isSubmit;
	});
	
	$("#form111").find("[reg2]").blur(function(){
		//获得必填的字段的值
		var val = $(this).val();
		//获得正则表达式的字符串
		var reg = $(this).attr("reg2");
		//获得提示信息
		var tip = $(this).attr("tip");
		//创建正则表达式的对象
		var regExp = new RegExp(reg);
		if(!regExp.test(val)){
			$(this).next("span").html("<font color='red'>"+tip+"</font>");
		}else{
			$(this).next("span").html("");
		}
	});
	
	//显示提示
	$("#form111").find("[reg1]").blur(function(){
		//获得必填的字段的值
		var val = $(this).val();
		//获得正则表达式的字符串
		var reg = $(this).attr("reg1");
		//获得提示信息
		var tip = $(this).attr("tip");
		//创建正则表达式的对象
		var regExp = new RegExp(reg);
		if(val != null && $.trim(val) != "" && !regExp.test(val)){
			$(this).next("span").html("<font color='red'>"+tip+"</font>");
		}else{
			$(this).next("span").html("");
			
		}
	})
})
</script>

<div class="column-name">
<h2>
		系统管理 > <b>用户管理</b>
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/v2sysuser/updatePwd.html" method="post">
		
		<div class="form-item">
			<div class="form-name">新密码</div>
			<div class="form-input">
				<input type="password" name="password" class="input" maxLength="16" reg2="^[a-z0-9]{6,16}$" tip="必须是小写英文或数字字符，长度6-16">
				<span></span>
			</div>
		</div>
		<input type="hidden" name="id" value="${id }">
		<input type="submit" class="btn w144" value="保  存">
	</form>
</div>
	

