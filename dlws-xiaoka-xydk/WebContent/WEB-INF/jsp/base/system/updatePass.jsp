<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>

<link rel="stylesheet" href="${path}/v2/console/css/ztree/demo.css"
	type="text/css">
<link rel="stylesheet"
	href="${path}/v2/console/css/ztree/zTreeStyle/zTreeStyle.css"
	type="text/css">

<script type="text/javascript"
	src="${path}/v2/console/js/ztree/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="${path}/v2/console/js/ztree/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript"
	src="${path}/v2/console/js/ztree/jquery.ztree.excheck-3.5.js"></script>

<script type="text/javascript">
	$(function() {
		$("#form111")
				.submit(
						function() {
							var isSubmit = true;
							//分两步：
							//第一步做必填字段的校验
							$(this)
									.find("[reg2]")
									.each(
											function() {
												//获得必填的字段的值
												var val = $(this).val();
												//获得正则表达式的字符串
												var reg = $(this).attr("reg2");
												//获得提示信息
												var tip = $(this).attr("tip");
												//创建正则表达式的对象
												var regExp = new RegExp(reg);
												if (!regExp.test(val)) {
													$(this)
															.parents(
																	".form-input")
															.find(".form-error")
															.html(tip);
													isSubmit = false;
													//在jQuery跳出循环不再是break;也部署return;是return false;
													return false;//跳出循环
												} else {
													var inputName = $(this)
															.attr("name");
													if (inputName == "brandName") {
														if (validBrandName(val)) {
															$(this)
																	.parents(
																			".form-input")
																	.find(
																			".form-error")
																	.html(
																			"<font color='red'>品牌名称已存在</font>");
															isSubmit = false;
															return false;//跳出循环
														} else {
															$(this)
																	.parents(
																			".form-input")
																	.find(
																			".form-error")
																	.html("");
														}
													} else {
														$(this)
																.parents(
																		".form-input")
																.find(
																		".form-error")
																.html("");
													}
												}
											});

							//第二步做非必填字段的校验
							$(this).find("[reg1]").each(
									function() {
										//获得必填的字段的值
										var val = $(this).val();
										//获得正则表达式的字符串
										var reg = $(this).attr("reg1");
										//获得提示信息
										var tip = $(this).attr("tip");
										//创建正则表达式的对象
										var regExp = new RegExp(reg);
										if (val != null && $.trim(val) != ""
												&& !regExp.test(val)) {
											$(this).parents(".form-input")
													.find(".form-error").html(
															tip);
											isSubmit = false;
											return false;//跳出循环
										} else {
											$(this).parents(".form-input")
													.find(".form-error").html(
															"");

										}
									});
							/* 
							if(isSubmit){
								tipShow("#importLoadDiv");
							}
							 */
							return isSubmit;
						});

		$("#form111").find("[reg2]").blur(function() {
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg2");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if (!regExp.test(val)) {
				$(this).parents(".form-input").find(".form-error").html(tip);
			} else {
				$(this).parents(".form-input").find(".form-error").html("");
			}
		});

		//显示提示
		$("#form111").find("[reg1]").blur(function() {
			//获得必填的字段的值
			var val = $(this).val();
			//获得正则表达式的字符串
			var reg = $(this).attr("reg1");
			//获得提示信息
			var tip = $(this).attr("tip");
			//创建正则表达式的对象
			var regExp = new RegExp(reg);
			if (val != null && $.trim(val) != "" && !regExp.test(val)) {
				$(this).parents(".form-input").find(".form-error").html(tip);
			} else {
				$(this).parents(".form-input").find(".form-error").html("");

			}

		});

	})

	//判断新密码和重复新密码是否一致 不一致 阻止提交
	function check() {
		if ($("#password1").val() != $("#password2").val()) {
			alert("两次密码不一致");
			return false;
		} else {
			return true;

		}
	}

	//输入原密码判断是否和数据库密码一致
	function input() {
		$.ajax({
			type : "post",
			url : "${path}/v2sysuser/checkPassword.html",
			data : {
				"oldPassword" : $("#password").val(),
				"id" : $("#id").val()

			},
			dataType : "json",
			success : function(data) {
				if (data == "yes") {
					flag = 1;
				} else {
					alert("原密码输入不正确");
					flag = 0;
				}
			}
		});
	}

	//提交时如果输入原密码和数据库密码不一致 阻止提交
	function save() {
		if (flag == 1) {
			$("#form111").submit();

		}

		else {
			alert("原密码输入不正确");
		}

	}
</script>
<div class="column-name">
	<h2>
		修改密码 > <b>修改密码</b>
	</h2>
</div>
<div class="form">
	<form id="form111" action="${path}/v2sysuser/updatePwdByUser.html"
		method="post" onsubmit="return check()">
		<input type="hidden" name="id" value="${paramsMap.id}" id="id" />
		<div class="form-item" ${style }>
			<div class="form-name">原密码</div>
			<div class="form-input">
				<input type="password" ${disabled } placeholder="请输入原密码"
					name="password" id="password" class="input" maxLength="16"
					reg2${style }="^[a-z0-9]{6,16}$" onblur="input()">
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		<div class="form-item" ${style }>
			<div class="form-name">新密码</div>
			<div class="form-input">
				<input type="password" ${disabled } placeholder="请输入长度6-16位的小写英文和数字"
					name="password1" id="password1" class="input" maxLength="16"
					reg2${style }="^[a-z0-9]{6,16}$" tip="必须是小写英文或数字字符，长度6-16">
				<div class="clearfix"></div>
				<div class="form-error red"></div>
			</div>
		</div>
		<div class="form-item" ${style }>
			<div class="form-name">确认新密码</div>
			<div class="form-input">
				<input type="password" ${disabled } placeholder="请输入长度6-16位的小写英文和数字"
					onblur="check()" name="password2" id="password2" class="input"
					maxLength="16" reg2${style }="^[a-z0-9]{6,16}$"
					tip="必须是小写英文或数字字符，长度6-16">
				<div class="clearfix"></div>
				<div class="form-error red" id="password2"></div>
			</div>
		</div>

		<input type="button" class="btn w144" value="保  存" onclick="save()">
	</form>
</div>


