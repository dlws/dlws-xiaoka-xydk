<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>

<div class="column-name">
	<h2>
		系统管理 > <b>参数配置</b>
		<c:if test="${type == 'add'}">
			<b>新建</b>
		</c:if>
		<c:if test="${type == 'edit'}">
			<b>修改</b>
		</c:if>
	</h2>
</div>
<form id="form1" name="form1" action="${path}/v2PlatSystemparam/v2PlatSystemparamList.html" method="post">
	<div class="main-top">
		<div class="fl">
			<a href="${path}/v2PlatSystemparam/platSystemparamAdd.html" class="btn btn-p">新建参数</a>
		</div>
		<div class="fl">
			<a href="${path}/v2PlatSystemparam/dictypeAdd.html" class="btn btn-p">全部推送</a>
		</div>
		<div class="fl">
			<a href="${path}/v2PlatSystemparam/dictypeAdd.html" class="btn btn-p">拉取</a>
		</div>
	</div>

	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>参数中文名称</th>
					<th>参数英文名称</th>
					<th>参数值</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="list">
					<tr>
						<td>${list.paramname}</td>
						<td>${list.paramcode}</td>
						<td>${list.paramvalue}</td>
						<td>
							<a href="${path}/v2PlatSystemparam/getV2PlatSystemparam.html?id=${list.id}">修改</a>
							<a href="javascript:;" data-id="${path}/v2PlatSystemparam/deleteV2PlatSystemparam.html?id=${list.id}" class="remove">删除</a>
							<a href="${path}/v2PlatSystemparam/v2DicDataListTwo.html?dicId=${list.dicId}&typename=${list.dicName}">推送</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!--  ====================================================  -->

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a>
					<a href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- remove-pop -->