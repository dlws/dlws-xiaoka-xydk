<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<link rel="stylesheet" href="${path}/v2/console/css/base.css">
<link rel="stylesheet" href="${path}/v2/console/css/style.css">
<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
<script src="${path}/v2/console/js/globle.js"></script>
<script src="${path}/v2/console/js/system/disResource.js"></script>

<div class="column-name">
	<h2>
		系统管理 > <b>权限管理</b>
	</h2>
</div>
<form id="form1" name="form1"
	action="${path}/v2Sysauthority/v2sysauthorityList.html" method="post">
	<div class="main-top">
		<div class="fl">
			<a href="${path}/v2Sysauthority/toAddAuthority.html"
				class="btn btn-p">新建权限</a>
		</div>
		<div class="fr">
			<span class="c-name">权限名称</span> <input type="text"
				name="authorityName" class="input mr10"
				value="${paramsMap.authorityName }"> <input type="button"
				class="btn w96" value="查  询" onclick="goSearch()">
		</div>
	</div>

	<div class="content">
		<table class="table" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<thead>
				<tr>
					<th>权限名称</th>
					<th>权限代码</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${po.datasource }" var="map">
					<tr>
						<td>${map.authorityName}</td>
						<td>${map.authorityCode}</td>
						<td><a
							href="${path}/v2Sysauthority/toUpdateAuthority.html?authorityId=${map.authorityId}">修改</a>
							<a href="javascript:;" data-aid="${map.authorityId}"
							class="distribution">资源维护</a> <a href="javascript:;"
							data-id="${path}/v2Sysauthority/deleteAuthority.html?authorityId=${map.authorityId}"
							class="remove">删除</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="page">
			<%@ include file="/v2/console/common/paging.jsp"%>
		</div>
	</div>
</form>

<!--  ====================================================  -->

<!-- remove-pop -->
<div class="popBox remove-pop">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-con">
			<div class="remove-con">
				<div class="tit">你确定要删除吗？</div>
				<div class="btns">
					<a href="" class="btn w144">确定</a> <a href="javascript:;"
						class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- remove-pop -->

<!-- 弹出框 -->
<div class="popBox select-pop" style="display:none">
	<div class="popBox-back"></div>
	<div class="popBox-box">
		<div class="popBox-title">
			<h3>资源维护</h3>
			<a href="javascript:;" class="close"></a>
		</div>
		<div class="popBox-con">
			<div class="select-con">
				<div class="select-box">
					<div class="fl">
						<div class="select-tit">已选资源</div>
						<select name="" multiple="multiple" id="selected" size="18">
						</select>
					</div>
					<div class="select-btns">
						<a href="javascript:;" class="select-btn01">&lt;</a> <a
							href="javascript:;" class="select-btn02">&gt;</a>
					</div>
					<div class="fr">
						<div class="select-tit">未选资源</div>
						<select name="" multiple="multiple" id="to-be-elected" size="18">
						</select>
					</div>
				</div>
				<div class="btns">
					<a href="javascript:;" class="btn w144">确 认</a> <a
						href="javascript:;" class="btn-gray w144">取 消</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- select-pop -->