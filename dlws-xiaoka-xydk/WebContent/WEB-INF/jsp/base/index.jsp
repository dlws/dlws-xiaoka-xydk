<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/v2/console/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
  <head>
    <title>校咖微平台后台管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="${path}/v2/console/css/base.css">
	<link rel="stylesheet" href="${path}/v2/console/css/layout.css">
	<link rel="stylesheet" href="${path}/v2/console/css/left.css">
	<script src="${path}/v2/console/js/jquery-1.9.1.min.js"></script>
	<script src="${path}/v2/console/js/globle.js"></script>
  </head>
  <body>
	<div id="wrapper">
		<div class="header">
			 <%@ include file="/v2/console/common/header.jsp"%>
		</div>
		<div class="main">
			<div class="main-arrow"></div>
			<div class="main-left">
				<%@ include file="/v2/console/common/menu.jsp"%>
			</div><!-- main-left -->
			<div class="main-right">
				<iframe id="frame-contect" src="" name="main" class="iframe" width="100%" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="yes" allowtransparency="yes"></iframe>
			</div>
		</div>
	</div>
</body>
</html>
