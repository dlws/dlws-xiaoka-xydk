<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/javascript">
$(function(){
	//获得当前页码和总页数
	var pageNo = parseInt($("#currentPage").val());
	var totalPage = parseInt($("#absolutePage").val());
	var pageSize = parseInt($("#pageSize").val());//每页显示条数
	if (totalPage == 1) {
		$("#previous").hide();
		$("#next").hide();
	} else if (pageNo == 1 && totalPage > 1) {
		$("#previous").hide();
		$("#next").show();
	} else if (pageNo > 1 && pageNo < totalPage) {
		$("#previous").show();
		$("#next").show();
	} else if (pageNo == totalPage && totalPage > 1) {
		$("#previous").show();
		$("#next").hide();
	}else if(totalPage == 0){
		$("#next").hide();
		$("#previous").hide();
		$("#firstPage").hide();
		$("#lastPage").hide();
		$("#selectPage").hide();
		$("#scurpage").hide();
		$("#stotaloage").hide();
	}

	$("#next").click(function() {
		$("#start").val(pageNo*pageSize);
		$("#currentPage").val(++pageNo);
		goSearch();
	});
	$("#previous").click(function() {
		$("#currentPage").val(--pageNo);
		$("#start").val((pageNo-1)*pageSize);
		goSearch();
	});
	$("#firstPage").click(function() {
		$("#start").val(0);
		$("#currentPage").val(1);
		goSearch();
	});
	$("#lastPage").click(function() {
		$("#currentPage").val(totalPage);
		$("#start").val((totalPage-1)*pageSize);
		goSearch();
	});

	$("#selectPage").change(function() {
		var myPage = $(this).val();
		$("#currentPage").val(myPage);
		$("#start").val((myPage-1)*pageSize);
		goSearch();
	});
	//使用js给select赋值即可
	$("#selectPage").val(pageNo);
});

function goSearch(){
	var currentPage = $("#currentPage").val();
	$("#pageForm").append('<input type="hidden" name="currentPage" value="'+currentPage+'">');
	$("#pageForm").submit();
}
	
</script>

<div class="page-con">
	<input type="hidden" value="" id="start" name="start" />
	<input type="hidden" value="${data.page.pageSize}" id="pageSize"/>
	<input type="hidden" value="${data.page.totalCount}" id="totalCount" name="totalCount" />
	<input type="hidden" value="${data.page.currentPage}" id="currentPage"/>
	<input type="hidden" value="${data.page.absolutePage}" id="absolutePage" name="absolutePage" /> 共
	<var id="pagePiece" class="orange">${data.page.totalCount}</var>
	条  
	<var id="pageTotal"><font id="scurpage">${data.page.currentPage }</font><font id="stotaloage">/${data.page.absolutePage }</font></var>
	<a href="javascript:void(0);" id="firstPage">首页</a>
	<a href="javascript:void(0);" id="previous" class="page-item" title="上一页">上一页</a>
	<a href="javascript:void(0);" id="next" class="page-item" title="下一页">下一页</a>
	<select id="selectPage">
		<c:forEach var="myPage" begin="1" end="${data.page.absolutePage }">
			<option value="${myPage }">第${myPage }页</option>
		</c:forEach>
	</select>
	<a href="javascript:void(0);" id="lastPage">尾页</a>





	<!-- 
	<div class="page-num">
	共<span>9/</span>50条内容
    </div>
	<span class="page-item">上一页</span> <a class="page-item" href="">上一页</a>
	<a href="" class="active">1</a> <a href="">2</a> <a href="">3</a> <a
		href="">4</a> <a href="">5</a> <a class="page-item" href="">下一页</a> <span
		class="page-item">下一页</span> -->
</div>