<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<c:if test="${fn:contains(privilege,'systemMenu')}">
	<div class="menu-box">
		<div class="menu-title">系统管理</div>
		<ul class="menu">
			<li>
				<div class="title">
					<i class="icon gao"></i> <span href="" class="menu-name">系统管理</span>
					<span class="arrow">></span>
				</div>
				<ul class="submenu">
					<c:if test="${fn:contains(privilege,'sys_user')}">
						<li><a
							href="${pageContext.request.contextPath}/v2sysuser/sysuserList.html"
							target="main"> <span href="" class="menu-name">用户管理</span> <span
								class="arrow">></span> <i class="icon01"></i>
						</a></li>
					</c:if>
					<c:if test="${fn:contains(privilege,'sys_role')}">
						<span class="arrow"></span>
						<li><a
							href="${pageContext.request.contextPath}/v2sysrole/sysroleList.html?start=0"
							target="main"> <span class="menu-name">角色管理</span> <span
								class="arrow">></span> <i class="icon01"></i>
						</a></li>
					</c:if>
					<c:if test="${fn:contains(privilege,'sys_privilege')}">
						<li><a
							href="${pageContext.request.contextPath}/v2Sysauthority/v2sysauthorityList.html"
							target="main"> <span class="menu-name">权限管理</span> <span
								class="arrow">></span> <i class="icon01"></i>
						</a></li>
					</c:if>
					<c:if test="${fn:contains(privilege,'sys_resource')}">
						<li><a
							href="${pageContext.request.contextPath}/v2Sysresource/v2sysresourceList.html"
							target="main"> <span class="menu-name">资源管理</span> <span
								class="arrow">></span> <i class="icon01"></i>
						</a></li>
					</c:if>
					<c:if test="${fn:contains(privilege,'sys_department')}">
						<li><a
							href="${pageContext.request.contextPath}/v2Department/departmentList.html"
							target="main"> <span class="menu-name">部门管理</span> <span
								class="arrow">></span> <i class="icon01"></i>
						</a></li>
					</c:if>
					<%-- <c:if test="${fn:contains(privilege,'sys_para')}">
						<li><a
							href="${pageContext.request.contextPath}/v2PlatSystemparam/v2PlatSystemparamList.html"
							target="main"> <span class="menu-name">参数配置</span> <span
								class="arrow">></span> <i class="icon01"></i>
						</a></li>
					</c:if> --%>
					<c:if test="${fn:contains(privilege,'sys_dic')}">
						<li><a
							href="${pageContext.request.contextPath}/v2DicTypeTwo/v2DicTypeListTwo.html"
							target="main"> <span class="menu-name">字典管理</span> <span
								class="arrow">></span> <i class="icon01"></i>
						</a></li>
					</c:if>
					<c:if test="${fn:contains(privilege,'sys_dic')}">
						<li><a
							href="${pageContext.request.contextPath}/school/schoolList.html"
							target="main"> <span class="menu-name">学校标签设置</span> <span
								class="arrow">></span> <i class="icon01"></i>
						</a></li>
					</c:if>
				</ul>
			</li>
		</ul>
	</div>
</c:if>
<!-- <div class="menu-box">
		<div class="menu-title">校园大咖</div>
		<ul class="menu">
				
		</ul>
</div> -->
 <div class="menu-box">
		<div class="menu-title">校园大咖</div>
		<ul class="menu">
				<li>
					<div class="title">
						<i class="icon ping"></i> <span href="" class="menu-name">入住用户管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/skillUserManager/skillSpaceList.html" target="main"> <span href="" class="menu-name">入住用户管理</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ping"></i> <span href="" class="menu-name">分类管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/maxCategory/maxCategoryList.html" target="main"> <span href="" class="menu-name">一级分类列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/minCategory/minCategoryList.html" target="main"> <span href="" class="menu-name">二级分类列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<%-- <li>
					<div class="title">
						<i class="icon ping"></i> <span href="" class="menu-name">商家活动</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/businessBasicinfo/businessBasicinfoList.html" target="main"> <span href="" class="menu-name">商家活动列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/order/orderList.html" target="main"> <span href="" class="menu-name">联系大咖</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li> --%>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">基本信息管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						 <%-- <li>
							<a href="${pageContext.request.contextPath}/userBasicinfo/userBasicinfoList.html?start=0" target="main"> <span href="" class="menu-name">基本信息列表</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li> --%>
						
						<%-- <li>
							<a href="${pageContext.request.contextPath}/userBasicinfo/settleList.html?start=0" target="main"> <span href="" class="menu-name">入驻信息5列表</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li> --%>
							<!-- 基本信息列表第三版 -->
						<li>
							<a href="${pageContext.request.contextPath}/basInfoUser/userBasicinfoList.html?start=0" target="main"> <span href="" class="menu-name">入驻信息列表</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<%-- <li>
					<div class="title">

						<i class="icon ji"></i> <span href="" class="menu-name">专题管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/special/specialList.html?start=0" target="main"> <span href="" class="menu-name">专题列表</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li> --%>
				<%-- <li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">学校管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/userBasicinfo/userBasicinfoList.html?start=0" target="main"> <span href="" class="menu-name">学校标签</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li> --%>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">服务管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/skill/skillList.html?start=0" target="main"> <span href="" class="menu-name">服务列表</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">入驻类型管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/entertype/entertypeList.html?start=0" target="main"> <span href="" class="menu-name">入驻类型列表</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				
				<li>
					<div class="title">
						<i class="icon ji"></i> <span class="menu-name">运营维护</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<%-- <li>
							<a href="${pageContext.request.contextPath}/messagePush/toShowMess.html?start=0" target="main"> <span class="menu-name">搜索热词管理管理</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li> --%>
						<li>
							<a href="${pageContext.request.contextPath}/messagePush/toShowMess.html?start=0" target="main"> <span class="menu-name">群发管理</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/label/labelList.html?start=0" target="main"> <span class="menu-name">标签管理</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/orderJztf/orderJztfList.html?start=0" target="main"> <span class="menu-name">精准投放审核</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<%-- <li>
							<a href="${pageContext.request.contextPath}/messagePush/toShowMess.html?start=0" target="main"> <span class="menu-name">需求管理</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li> --%>
						<li>
							<a href="${pageContext.request.contextPath}/preferredService/preferredServiceList.html?start=0" target="main"> <span href="" class="menu-name">优选服务管理</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/hotWordSearch/hotWordSearchList.html?start=0" target="main"> <span href="" class="menu-name">热搜词管理</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/releaseProtocol/releaseProtocolList.html?start=0" target="main"> <span href="" class="menu-name">协议管理</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/publishtype/list.html?start=0" target="main"><span href="" class="menu-name">商家分类管理</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/bannerInfo/bannerInfoList.html?start=0" target="main"> <span href="" class="menu-name">轮播图列表</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/link/list.html?start=0" target="main"> <span href="" class="menu-name">链接管理</span>
									<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">退款管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/refunds/refundsList.html?start=0" target="main"> <span href="" class="menu-name">退款管理</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">提现管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/draw/drawList.html?start=0" target="main"> <span href="" class="menu-name">提现管理</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
						
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">记录管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/dividedraw/divideDrawList.html?start=0" target="main"> <span href="" class="menu-name">记录管理</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">订单管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/orderManage/orderMangeList.html?start=0" target="main"> <span href="" class="menu-name">订单列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">钱包管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/managerWallet/list.html?start=0" target="main"> <span href="" class="menu-name">钱包列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">商家管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/managerPublish/list.html?start=0" target="main"> <span href="" class="menu-name">商家发布列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">发票管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/managerInvoice/list.html?start=0" target="main"> <span href="" class="menu-name">发票列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">资源服务</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/mediaResource/list.html?start=0" target="main"> <span href="" class="menu-name">自媒体资源</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/associationResource/list.html?start=0" target="main"> <span href="" class="menu-name">社群大V资源</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/schoolShopsResource/list.html?start=0" target="main"> <span href="" class="menu-name">校内商家资源</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/siteResource/list.html?start=0" target="main"> <span href="" class="menu-name">场地资源</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">更换管理员管理</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/managechange/list.html?start=0" target="main"> <span href="" class="menu-name">管理员列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="title">
						<i class="icon ji"></i> <span href="" class="menu-name">外交官推广后台</span>
						<span class="arrow">></span>
					</div>
					<ul class="submenu">
						<li>
							<a href="${pageContext.request.contextPath}/diplomatist/list.html?start=0" target="main"> <span href="" class="menu-name">外交官列表</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/diplomatDraw/getApplyWithDraw.html?start=0" target="main"> <span class="menu-name">外交官提现管理</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/diplomatExpand/list.html?start=0" target="main"> <span class="menu-name">外交官推广管理</span>
								<span class="arrow">></span> <i class="icon01"></i>
							</a>
						</li>
					</ul>
				</li>
		</ul>
</div> 
