<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/javascript">
$(function(){
	//获得当前页码和总页数
	var pageNo = parseInt($("#currentPage").val());
	var totalPage = parseInt($("#absolutePage").val());
	var pageSize = parseInt($("#pageSize").val());//每页显示条数
	if (totalPage == 1) {
		$("#next").hide();
		$("#previous").hide();
		$("#firstPage").hide();
		$("#lastPage").hide();
		$("#selectPage").hide();
	} else if (pageNo == 1 && totalPage > 1) {
		$("#previous").hide();
		$("#next").show();
	} else if (pageNo > 1 && pageNo < totalPage) {
		$("#previous").show();
		$("#next").show();
	} else if (pageNo == totalPage && totalPage > 1) {
		$("#previous").show();
		$("#next").hide();
	}else if(totalPage == 0){
		$("#next").hide();
		$("#previous").hide();
		$("#firstPage").hide();
		$("#lastPage").hide();
		$("#selectPage").hide();
		$("#scurpage").hide();
		$("#stotaloage").hide();
	}

	$("#next").click(function() {
		$("#start").val(pageNo*pageSize);
		$("#currentPage").val(++pageNo);
		$("#form1").submit();
	});
	$("#previous").click(function() {
		$("#currentPage").val(--pageNo);
		$("#start").val((pageNo-1)*pageSize);
		$("#form1").submit();
	});
	$("#firstPage").click(function() {
		$("#start").val(0);
		$("#currentPage").val(1);
		$("#form1").submit();
	});
	$("#lastPage").click(function() {
		$("#currentPage").val(totalPage);
		$("#start").val((totalPage-1)*pageSize);
		$("#form1").submit();
	});

	$("#selectPage").change(function() {
		var myPage = $(this).val();
		$("#currentPage").val(myPage);
		$("#start").val((myPage-1)*pageSize);
		$("#form1").submit();
	});
	//使用js给select赋值即可
	$("#selectPage").val(pageNo);
});

function goSearch(){
	$("#form1").submit();
}
	
</script>

<div class="page-con">
	<input type="hidden" value="" id="start" name="start" />
	<input type="hidden" value="${po.pageSize}" id="pageSize"/>
	<input type="hidden" value="${po.totalCount}" id="totalCount" name="totalCount" />
	<input type="hidden" value="${po.currentPage}" id="currentPage" name="currentPage" />
	<input type="hidden" value="${po.absolutePage}" id="absolutePage" name="absolutePage" /> 共
	<var id="pagePiece" class="orange">${po.totalCount}</var>
	条  
	<var id="pageTotal"><font id="scurpage">${po.currentPage }</font><font id="stotaloage">/${po.absolutePage }</font></var>
	<a href="javascript:void(0);" id="firstPage">首页</a>
	<a href="javascript:void(0);" id="previous" class="page-item" title="上一页">上一页</a>
	<a href="javascript:void(0);" id="next" class="page-item" title="下一页">下一页</a>
	<select id="selectPage">
		<c:forEach var="myPage" begin="1" end="${po.absolutePage }">
			<option value="${myPage }">第${myPage }页</option>
		</c:forEach>
	</select>
	<a href="javascript:void(0);" id="lastPage">尾页</a>





	<!-- 
	<div class="page-num">
	共<span>9/</span>50条内容
    </div>
	<span class="page-item">上一页</span> <a class="page-item" href="">上一页</a>
	<a href="" class="active">1</a> <a href="">2</a> <a href="">3</a> <a
		href="">4</a> <a href="">5</a> <a class="page-item" href="">下一页</a> <span
		class="page-item">下一页</span> -->
</div>