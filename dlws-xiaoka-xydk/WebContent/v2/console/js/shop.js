mui.init({
	swipeBack: false
});
(function($) {
	$('.mui-scroll-wrapper').scroll({
		indicators: true //是否显示滚动条
	});	
})(mui);

//损耗管理选项卡
$(function(){
	$('.mui-table .nav-list:first-child').show();
	mui('.nav-loss').on('tap', 'a', function() {
		$('.nav-loss a').removeClass('active');
		$(this).addClass('active');
		$('.mui-table .nav-list').hide();
		$('.mui-table .nav-list').eq($('.nav-loss a').index(this)).show('50');
	});
});
//库存盘点选项卡
$(function(){
	$('.check-table:first-child').show();
	mui('.nav-check').on('tap', 'a', function() {
		$('.nav-check a').removeClass('active');
		$(this).addClass('active');
		$('.check-table').hide();
		$('.check-table').eq($('.nav-check a').index(this)).show('50');
	});
});