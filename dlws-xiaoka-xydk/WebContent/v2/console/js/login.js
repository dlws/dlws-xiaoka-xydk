/**
Copyright @ 校咖微平台后台管理系统
 */
$(function(){
	var rr = false;
	var rr01 = false;
	var $form = $('.form');
	var $username = $('#username');
	var $password = $('#password');
	var usernameTxt = '用户名不能为空';
	var passwordTxt = '密码不能为空';

	$username.focus(function() {
		$('.form .form-error').remove();
	});

	$('.form input[type="button"]').click(function() {

		if ($username.val() === '') {
			$form.append('<div class="form-error">'+usernameTxt+'</div>');
		}else{
			rr = true;
		}

		if ($password.val() === '') {
			$form.append('<div class="form-error">'+passwordTxt+'</div>');
		}else{
			rr01 = true;
		}

		if(rr && rr01){
			return;
		}else{
			return false;
		}
	});
});