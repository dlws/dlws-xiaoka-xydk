/**
Copyright @ 校咖微平台后台管理系统
 */

$(function(){
	nav();
	zuobiao();// 地图弹出框
	city();// 选择城市
	pop();// 弹出框
	promotePop();// 促销弹出框
	promotePop_1();
	modifyGoods();// 修改订单货物弹出框
	weicang();// 微仓查询弹出框
	remove();// 删除弹出框
	confirmTwo();//确认弹出框
	cancel();// 取消弹出框
	kucun();// 库存弹出框
	chaTupian();// 查看图片和链接弹出框
	chaXiangqing();// 查看详情页面弹出框
	FaFang();// 发放弹出框
	purchase();
	time();// 表单中添加配送时段
	windowChanges();
	$('.iframe').height($(window).height() - 82 );
	$('.main-left').height($(window).height() - 82 );

	$(window).resize(function() {
		$('.iframe').height($(window).height() - 82 );
		$('.main-left').height($(window).height() - 82 );
	});
});

function nav(){
	var $menu = $('.menu');
	var $title = $menu.find('.title');
	var $submenu = $menu.find('.submenu');

	$submenu.append('<li class="line-top"></li><li class="line-botton"></li>');
	$menu.find('.active').next('.submenu').slideDown();
	$title.click(function() {
		if (!$(this).hasClass('active')) {
			$title.removeClass('active').next('.submenu').slideUp();
			$(this).addClass('active').next('.submenu').show();
			$title.find('.arrow').css({'top':'0px', 'transform':'rotate(0deg)', '-webkit-transform':'rotate(0deg)', '-moz-transform':'rotate(0deg)', '-o-transform':'rotate(0deg)','transition':'all 0.3s ease', '-webkit-transition':'all 0.3s ease', '-moz-transition':'all 0.3s ease', '-o-transition':'all 0.3s ease'});
			$(this).find('.arrow').css({'top':'0px', 'transform':'rotate(90deg)', '-webkit-transform':'rotate(90deg)', '-moz-transform':'rotate(90deg)', '-o-transform':'rotate(90deg)','transition':'all 0.3s ease', '-webkit-transition':'all 0.3s ease', '-moz-transition':'all 0.3s ease', '-o-transition':'all 0.3s ease'});
		}else{
			$(this).removeClass('active').next('.submenu').slideUp();
			$(this).find('.arrow').css({'top':'0px', 'transform':'rotate(0deg)', '-webkit-transform':'rotate(0deg)', '-moz-transform':'rotate(0deg)', '-o-transform':'rotate(0deg)','transition':'all 0.3s ease', '-webkit-transition':'all 0.3s ease', '-moz-transition':'all 0.3s ease', '-o-transition':'all 0.3s ease'});
		}
	});
	$menu.find("li li a").click(function(){
		$menu.find("li li a").removeClass("activeA");
		$(this).addClass("activeA");
	});
}

// 选择城市
function city(){
	var $city = $('.city');
	var $menu = $city.find('.city-menu');
	$city.mouseover(function() {
		$menu.show();
	});
	$menu.mouseout(function() {
		$menu.hide();
	});
	$menu.find('li').click(function() {
		$('.city-name').html($(this).html());
		$menu.hide();
	});
}

// 弹出框
function pop(){
	var $popBox = $('.popBox');
	var $close = $popBox.find('.close');
	var $btn = $popBox.find('.btn-gray');

	$close.click(function() {
		$popBox.hide();
	});
	$btn.click(function() {
		$popBox.hide();
	});
	$popBox.find('.btn').click(function() {
		$popBox.hide();
	});
}

// 促销弹出框
function promotePop(){
	var $promote = $('.promote');
	var $promotePop = $('.promote-pop');
	var $promoteCon = $promotePop.find('.promote-con');
	var $radio = $promoteCon.find('.radio');
	var $btn = $promoteCon.find('.btn');

	$promote.each(function(index, el) {
		$(this).attr('id','promote-a'+index);
		$(this).click(function() {
			$promotePop.show().attr('id','promote-pop'+index);// 为促销弹出框添加id

			$radio.click(function() { // 选择促销类别
				$radio.removeClass('active').html('').siblings('input').removeAttr('checked');
				$(this).addClass('active').append('√').siblings('input').attr('checked','checked');
			});

			$btn.click(function() {// 点击按钮确认选中促销类别
				$('#promote-a'+$promotePop.attr('id').replace(/[^0-9]/ig,"")).html($radio.siblings('input[checked="checked"]').siblings('span.name').html());
				$promotePop.hide();
			});
		});
	});

	// 促销类别默认状态
	if ($radio.hasClass('active')) {
		$promoteCon.find('.active').append('√');
		$promoteCon.find('.active').siblings('input').attr('checked','checked');
	}
}

function promotePop_1(){
	var $promote = $('.promote1');
	var $promotePop = $('.promote-pop-o');
	var $promoteCon = $promotePop.find('.promote-con');
	var $radio = $promoteCon.find('.radio');
	var $btn = $promoteCon.find('.btn');

	$promote.each(function(index, el) {
		$(this).attr('id','promote-b'+index);
		$(this).click(function() {
			$promotePop.show().attr('id','promote-pop-o'+index);// 为促销弹出框添加id

			$radio.click(function() { // 选择促销类别
				$radio.removeClass('active').html('').siblings('input').removeAttr('checked');
				$(this).addClass('active').append('√').siblings('input').attr('checked','checked');
			});

			$btn.click(function() {// 点击按钮确认选中促销类别
				$('#promote-b'+$promotePop.attr('id').replace(/[^0-9]/ig,"")).html($radio.siblings('input[checked="checked"]').siblings('span.name').html());
				$promotePop.hide();
			});
		});
	});

	// 促销类别默认状态
	if ($radio.hasClass('active')) {
		$promoteCon.find('.active').append('√');
		$promoteCon.find('.active').siblings('input').attr('checked','checked');
	}
}



// 修改订单货物
function modifyGoods(){
	$('.modifyGoods').click(function() {
		$('.modifyGoods-pop').show();
	});
	$('.modifyGoods-pop .btn').click(function() {

	});
}

// 微仓查询
function weicang(){
	$('.weicang').click(function() {
		$('.weicang-pop').show();
	});
}

// 删除
function remove(){
	$('.remove').click(function() {
		var path = "wm-plat";
		var thisd = $(this);
		$('.remove-pop').show();
		$('.remove-pop .btn').attr({'href':thisd.data('id')});
	});	
}

// 确认
function confirmTwo(){
	$('.confirm').click(function() {
		var path = "wm-plat";
		var thisd = $(this);
		$('.confirm-pop').show();
		$('.confirm-pop .btn').attr({'href':thisd.data('id')});
	});	
}

// 取消
function cancel(){
	$('.cancel').click(function() {
		var thisd = $(this);
		$('.cancel-pop').show();
		$('.cancel-pop .btn').click(function() {
			thisd.hide();
			thisd.siblings('.cancelled').show();
		});
	});

	$('.cancelled').on('click',function() {
		$('.cancelled-pop').show();
		$('.cancelled-pop .btn').click(function() {
			
		});
	});
}

// 库存
function kucun(){
	$('.kucun').click(function() {
		$('.kucun-pop').show();
	});
	$('.kucun-pop .btn').click(function() {
		
	});
}

// 查看图片和链接
function chaTupian(){
	$('.chaTupian').click(function() {
		$('.chaTupian-pop').show();
	});
	$('.chaLianjie').click(function() {
		$('.chaLianjie-pop').show();
	});
}

// 查看详情页面
function chaXiangqing(){
	$('.chaXiangqing').click(function() {
		$('.chaXiangqing-pop').show();
	});
	$('.chaXiangqing-pop .btn').click(function() {
		$('.chaXiangqingQuren-pop').show();
	});
}

// 地图
function zuobiao(){
	$('.zuobiao').click(function() {
		$('.zuobiao-pop').show();
	});
}

// 发放
function FaFang(){
	$('.quanFaFang').click(function() {
		$('.quanFaFang-pop').show();
		$(this).attr('href','javascript:;');
	});
	$('.danFaFang').click(function() {
		$('.danFaFang-pop').show();
		$(this).attr('href','javascript:;');
	});
}


// 添加配送时段
function time(){
	var index = 1;
	$('.form .form-item-time .fl .btn-blue').click(function() {
		$('.form .form-item-time .fl').eq(0).prepend('<div class="time-item"><input type="text" class="input" name = "pMoney['+index+'].peopleNum"><div class="dl-num">人</div><span class="dl-line"></span><input type="text" class="input" name = "pMoney['+index+'].money"><div class="dl-num">元</div><a href="javascript:;" class="btn-lightGray">删除</a></div>');
		index = index+1;
		$('.form .form-item-time .fl .time-item .btn-lightGray').show();
	});
	$(document).on('click', '.form .form-item-time .fl .btn-lightGray', function() {
		$(this).parents('.time-item').remove();	
		if ($('.form .form-item-time .fl .time-item').length < 2) {
			$('.form .form-item-time .fl .time-item .btn-lightGray').hide();
			return false;
		}
	});
	if ($('.form .form-item-time .fl .time-item').length < 2) {
		$('.form .form-item-time .fl .time-item .btn-lightGray').hide();
	}
}

//窗口变化
function windowChanges(){
	$(window).resize(function() {
		if ($('.main-left').css('left') == (-$('.main-left').width()+15)+'px') {
			$('.main-right').width($(window).width() - 15);
		}else{
			$('.main-right').width($(window).width() - $('.main-left').width());
		}
	});

	$('.main-right').width($(window).width() - $('.main-left').width());
	$('.main-arrow').on('click',function() {
		if ($(this).hasClass("main-arrow-o")) {
			$(this).removeClass('main-arrow-o');
			$('.main-left').animate({'left':0},200,function(){
				$(this).css({'overflow':'auto'});
			});
			$('.main-right').animate({'left':$('.main-left').width(),'width':$(window).width() - $('.main-left').width()},200);
			$(this).animate({'left':$('.main-left').width() - 20},200);
		} else{
			
			$(this).addClass('main-arrow-o');
			$('.main-left').animate({'left':-$('.main-left').width()+15},200,function(){
				$(this).css({'overflow':'hidden'}); 
			});
			$('.main-right').animate({'left':'15px','width':$(window).width() - 15},200);
			$(this).animate({'left':'5px'},200);
		}
		
	});
}
//新增采购单弹出框
function purchase(){
	$('.purchase').click(function() {
		$('.purchase-pop').show();
	});
}