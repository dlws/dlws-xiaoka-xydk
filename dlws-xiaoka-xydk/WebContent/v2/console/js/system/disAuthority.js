$(function() {
	disAuthority();// 分配权限
});
// 权限ID
var roleId;

/* 分配权限 */
function disAuthority() {
	$('.distribution').click(
			function() {
				var path = "wm-plat";
				var thisd = $(this);
				roleId = thisd.data('aid');
				$('.select-pop').show();

				// ajax异步请求角色已拥有权限列表
				$.ajax({
					url : 'sysroleAuthorityList.html',
					data : {
						roleId : roleId,
						status : 1
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						var list = data.list;
						$("#selected").empty();
						for (var i = 0; i < list.length; i++) {
							$("#selected").append(
									"<option value=" + list[i].authorityId + ">"
											+ list[i].authorityName
											+ "</option>");
						}
					},
					error : function() {
						alert("异常！");
					}
				});

				// ajax异步请求角色未拥有权限列表
				$.ajax({
					url : 'sysroleNoAuthorityList.html',
					data : {
						roleId : roleId,
						status : 1
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						var list = data.list;
						$("#to-be-elected").empty();
						for (var i = 0; i < list.length; i++) {
							$("#to-be-elected").append(
									"<option value=" + list[i].authorityId + ">"
											+ list[i].authorityName
											+ "</option>");
						}
					},
					error : function() {
						alert("异常！");
					}
				});
			});
}

// 左右选项移动
$(function() {
	$(".select-btn01").click(
			function() {
				var authorityId = $('.select-box .fr select option:selected')
						.val();
				if(authorityId == null){
					return;
				}
				// 添加角色权限关系
				$.ajax({
					url : 'addSysroleAuthority.html',
					data : {
						authorityId : authorityId,
						roleId : roleId
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						if (!data.success) {
							alert("分配失败");
						}
					}
				});
				$('.select-box .fr select option:selected').clone().prependTo(
						'.select-box .fl select');
				$('.select-box .fr select option:selected').remove();
			});
	$(".select-btn02").click(
			function() {
				var authorityId = $('.select-box .fl select option:selected')
						.val();
				if(authorityId == null){
					return;
				}
				// 删除角色资源关系
				$.ajax({
					url : 'deleteSysroleAuthority.html',
					data : {
						authorityId : authorityId,
						roleId : roleId
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						if (!data.success) {
							alert("分配失败");
						}
					}
				});
				$('.select-box .fl select option:selected').clone().prependTo(
						'.select-box .fr select');
				$('.select-box .fl select option:selected').remove();
			});
});
