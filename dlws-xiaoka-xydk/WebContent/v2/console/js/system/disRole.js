$(function() {
	disRole();// 分配角色
});
// 用户ID
var userId;

/* 分配角色 */
function disRole() {
	$('.distribution').click(
			function() {
				var path = "wm-plat";
				var thisd = $(this);
				userId = thisd.data('aid');
				$('.select-pop').show();

				// ajax异步请求用户已拥有角色列表
				$.ajax({
					url : 'sysroleUserList.html',
					data : {
						userId : userId,
						status : 1
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						var list = data.list;
						$("#selected").empty();
						for (var i = 0; i < list.length; i++) {
							$("#selected").append(
									"<option value=" + list[i].roleId + ">"
											+ list[i].name + "</option>");
						}
					},
					error : function() {
						alert("异常！");
					}
				});

				// ajax异步请求角色未拥有权限列表
				$.ajax({
					url : 'sysroleNoUserList.html',
					data : {
						userId : userId,
						status : 1
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						var list = data.list;
						$("#to-be-elected").empty();
						for (var i = 0; i < list.length; i++) {
							$("#to-be-elected").append(
									"<option value=" + list[i].roleId + ">"
											+ list[i].name + "</option>");
						}
					},
					error : function() {
						alert("异常！");
					}
				});
			});
}

// 左右选项移动
$(function() {
	$(".select-btn01").click(
			function() {
				var roleId = $('.select-box .fr select option:selected').val();
				if (roleId == null) {
					return;
				}
				// 添加角色权限关系
				$.ajax({
					url : 'addSysroleUser.html',
					data : {
						userId : userId,
						roleId : roleId
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						if (!data.success) {
							alert("分配失败");
						}
					}
				});
				$('.select-box .fr select option:selected').clone().prependTo(
						'.select-box .fl select');
				$('.select-box .fr select option:selected').remove();
			});
	$(".select-btn02").click(
			function() {
				var roleId = $('.select-box .fl select option:selected').val();
				if (roleId == null) {
					return;
				}
				// 删除角色资源关系
				$.ajax({
					url : 'deleteSysroleUser.html',
					data : {
						userId : userId,
						roleId : roleId
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						if (!data.success) {
							alert("分配失败");
						}
					}
				});
				$('.select-box .fl select option:selected').clone().prependTo(
						'.select-box .fr select');
				$('.select-box .fl select option:selected').remove();
			});
});
