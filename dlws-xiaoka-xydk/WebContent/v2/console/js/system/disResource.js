$(function() {
	disResource();// 分配资源
});
// 权限ID
var authorityId;

/* 分配资源 */
function disResource() {
	$('.distribution').click(
			function() {
				var path = "wm-plat";
				var thisd = $(this);
				authorityId = thisd.data('aid');
				$('.select-pop').show();

				// ajax异步请求权限已拥有资源列表
				$.ajax({
					url : 'v2SysresourceAuthList.html',
					data : {
						authorityId : authorityId,
						status : 1
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						var list = data.list;
						$("#selected").empty();
						for (var i = 0; i < list.length; i++) {
							$("#selected").append(
									"<option value=" + list[i].resourceId + ">"
											+ list[i].componentname
											+ "</option>");
						}
					},
					error : function() {
						alert("异常！");
					}
				});

				// ajax异步请求权限未拥有资源列表
				$.ajax({
					url : 'v2SysresourceNoAuthList.html',
					data : {
						authorityId : authorityId,
						status : 1
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						var list = data.list;
						$("#to-be-elected").empty();
						for (var i = 0; i < list.length; i++) {
							$("#to-be-elected").append(
									"<option value=" + list[i].resourceId + ">"
											+ list[i].componentname
											+ "</option>");
						}
					},
					error : function() {
						alert("异常！");
					}
				});
			});
}

// 左右选项移动
$(function() {
	$(".select-btn01").click(
			function() {
				var resourceId = $('.select-box .fr select option:selected')
						.val();
				if(resourceId == null){
					return;
				}
				// 添加资源权限关系
				$.ajax({
					url : 'addSysresourceAuth.html',
					data : {
						authorityId : authorityId,
						resourceId : resourceId
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						if (!data.success) {
							alert("分配失败");
						}
					}
				});
				$('.select-box .fr select option:selected').clone().prependTo(
						'.select-box .fl select');
				$('.select-box .fr select option:selected').remove();
			});
	$(".select-btn02").click(
			function() {
				var resourceId = $('.select-box .fl select option:selected')
						.val();
				if(resourceId == null){
					return;
				}
				// 删除权限资源关系
				$.ajax({
					url : 'deleteSysresourceAuth.html',
					data : {
						authorityId : authorityId,
						resourceId : resourceId
					},
					type : 'post',
					cache : false,
					dataType : 'json',
					success : function(data) {
						if (!data.success) {
							alert("分配失败");
						}
					}
				});
				$('.select-box .fl select option:selected').clone().prependTo(
						'.select-box .fr select');
				$('.select-box .fl select option:selected').remove();
			});
});
