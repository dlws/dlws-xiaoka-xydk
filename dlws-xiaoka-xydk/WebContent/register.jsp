<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>

<html lang="zh-CN">
<head>
<title>校咖网</title>
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<link href="./css/base.css" rel="stylesheet" />
<link href="./css/index.css" rel="stylesheet" />
<script type="text/javascript" src="js/user/jquery-1.7.2.min.js"></script>
<script src="js/user/login.js"></script>
<script type="text/javascript">
	//只有选中同意协议的，才可以对按钮进行提交
	function isaccepted(){  
	    if(document.getElementById("check_id").checked==true){  
	        document.getElementById("submit").disabled = false;  
	    }else{  
	        document.getElementById("submit").disabled = true;  
	    }  
	} 
	
</script>
</head>

<body>
    <div id="warpper">
    	<div id="top">
	    	<div class="w">
	    		<div id="share">
	    			<span class="lightgrey fl">我要分享</span>
	    			<div class="bshare-custom fl"><div class="bsPromo bsPromo2"></div><a title="分享到QQ空间" class="bshare-qzone"></a><a title="分享到新浪微博" class="bshare-sinaminiblog"></a><a title="分享到人人网" class="bshare-renren"></a><a title="分享到腾讯微博" class="bshare-qqmb"></a><a title="分享到网易微博" class="bshare-neteasemb"></a><a title="更多平台" class="bshare-more bshare-more-icon more-style-addthis"></a></div><script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/buttonLite.js#style=-1&amp;uuid=&amp;pophcol=1&amp;lang=zh"></script><script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/bshareC0.js"></script>
	    		</div>
	    		<div class="fr">
	    			<ul>
	    				<li>欢迎来到咖啡网！</li>
	    				<li><span>|</span><a href="./register.jsp">注册</a></li>
	    				<li><span>|</span><a href="./login.jsp">登录</a></li>
	    			</ul>	
	    		</div>
    		</div>
    	</div><!-- 网站页头 -->
    	<div id="header" class="w">
    		<div id="logo2">
    			<a href="">咖啡网</a>
    		</div>
    		<div class="tel">
    			客服热线：  400-890-9850
    		</div>
    	</div><!-- 网站头部 -->
    	<div id="nav">
    		<div class="w">
    			<ul>
    				<li class="current"><a href="">首页</a></li>
    				<li><a href="">合作项目</a></li>
    				<li><a href="">发布项目</a></li>
    				<li><a href="">校园咖啡</a></li>
    				<li><a href="">创业服务</a></li>
    				<li><a href="">关于我们</a></li>
    			</ul>
    		</div>
    	</div><!-- 网站主导航 -->
    	<div id="main">
	    	<div class="w">
		    	<div class="register-layout">
					<p class="register-form-attention">标有 <span class="register-xing">*</span>的为必填选项</p>		
					<form action="/wm-plat/busyShopuser/addBusyShopuser.html">			
					<div class="login-item">
	    				<label><span class="register-xing">*</span>用户名</label>
	    				<p class="input-box">
	    					<input class = "username" id="username" name="username" type="text" placeholder="4-16个字符、英文、数字、汉字和“_”">
	    				</p>
	    			</div>
	    			<div class="login-item">
	    				<label><span class="register-xing">*</span>密码</label>
	    				<p class="input-box">
	    					<input id="password" name="password" type="text" placeholder="密码长度为8-10位，不可为纯数字">
	    				</p>
	    			</div>
	    			<div class="login-item">
	    				<label><span class="register-xing">*</span>确认密码</label>
	    				<p class="input-box">
	    					<input id="password2" name="password2"  type="text" placeholder="请再次输入密码">
	    				</p>
	    			</div>
	    			<div class="login-item">
	    				<label><span class="register-xing">*</span>手机号</label>
	    				<p class="input-box">
	    					<input id="phone" name = "phone" type="text" placeholder="请输入11位手机号码">
	    				</p>
	    			</div>
	    			<div class="login-item">
	    				<label><span class="register-xing">*</span>微信号</label>
	    				<p class="input-box">
	    					<input id="weixin" name = "weixin" type="text" placeholder="请输入微信号">
	    				</p>
	    			</div>
	    			<div class="login-item">
	    				 <label>地址</label>
	    				 
	    				<!--<p class="input-box">
	    					<span class="input-box1">
		    					<select id="register-province" name="Admin[province]" class="register-select register-select-1"><option value="">--请选择省份--</option><option value="北京市">北京市</option><option value="天津市">天津市</option><option value="河北省">河北省</option><option value="山西省">山西省</option><option value="内蒙古自治区">内蒙古自治区</option><option value="辽宁省">辽宁省</option><option value="吉林省">吉林省</option><option value="黑龙江省">黑龙江省</option><option value="上海市">上海市</option><option value="江苏省">江苏省</option><option value="浙江省">浙江省</option><option value="安徽省">安徽省</option><option value="福建省">福建省</option><option value="江西省">江西省</option><option value="山东省">山东省</option><option value="河南省">河南省</option><option value="湖北省">湖北省</option><option value="湖南省">湖南省</option><option value="广东省">广东省</option><option value="广西壮族自治区">广西壮族自治区</option><option value="海南省">海南省</option><option value="重庆市">重庆市</option><option value="四川省">四川省</option><option value="贵州省">贵州省</option><option value="云南省">云南省</option><option value="西藏自治区">西藏自治区</option><option value="陕西省">陕西省</option><option value="甘肃省">甘肃省</option><option value="青海省">青海省</option><option value="宁夏回族自治区">宁夏回族自治区</option><option value="新疆维吾尔自治区">新疆维吾尔自治区</option><option value="香港特别行政区">香港特别行政区</option><option value="澳门特别行政区">澳门特别行政区</option><option value="台湾省">台湾省</option><option value="其它">其它</option></select>
								<select id="register-city" name="Admin[city]" class="register-select register-select-2"><option value="">--请选择城市--</option></select>
								<select id="register-area" name="Admin[area]" class="register-select register-select-3"><option value="">--请选择地区--</option></select>
							</span>
	    				</p> -->
	    				<p class="input-box">
	    					<input id="address" name = "address" type="text" placeholder="请输入商家地址">
	    				</p>
	    			</div>
	    			<!-- 
	    			<div class="login-item fore6">
	    				<label><span class="register-xing">*</span>获取微信激活码</label>
	    				<p class="input-box">
	    					<span class="box-b"><img class="image-weixincode" id="weixincode" src="./images/code.jpg" ></span><input style="margin-top: 10px;" type="text" name="weixincode" class="weixincode" id="weixincode" maxlength="6">
	    				</p>
	    			</div>
	    			 -->
					<div class="login-item fore6">
	    				<label><span class="register-xing">*</span>验证码</label>
	    				<p class="input-box">
	    					<span class="box-b"><img alt="点击更换验证码" title="点击更换验证码" class="image-verifycode" id="yw0" src="VerifyCode.do" onClick="this.src='VerifyCode.do';"></span><input style="margin-top: 10px;" type="text" name="verifycode" class="verifycode" id="verifycode" maxlength="4">
	    				</p>
	    			</div>
	    			<div class="login-item">
	    				<div class="login-remember">
	    					<div class="lr-l fl"><input type="checkbox"  id="check_id" name="check_id" value="checkbox" onclick="isaccepted()">我已阅读并同意<span class="blue">《校咖注册服务协议》</span></div>
	    				</div>
	    			</div>
					<div class="login-btn">
	    				<button  type="submit" id="submit" disabled="disabled">确认注册</button>
	    			</div>
					</form>		
				</div>
		    	<div id="serviceGuarantee" class="mt20">
		    		<ul class="serviceGuarantee-list">
		    			<li>
		    				<p class="list-icons list-icon1"></p>
		    				<p class="list-tit"><b>资源整合</b></p>
		    			</li>
		    			<li class="lineLeft"></li>
		    			<li>
		    				<p class="list-icons list-icon2"></p>
		    				<p class="list-tit"><b>跨界经营</b></p>
		    			</li>
		    			<li class="lineLeft"></li>
		    			<li>
		    				<p class="list-icons list-icon3"></p>
		    				<p class="list-tit"><b>网络营销</b></p>
		    			</li>
		    			<li class="lineLeft"></li>
		    			<li>
		    				<p class="list-icons list-icon4"></p>
		    				<p class="list-tit"><b>融资贷款</b></p>
		    			</li>
		    			<li class="lineLeft"></li>
		    			<li>
		    				<p class="list-icons list-icon5"></p>
		    				<p class="list-tit"><b>创业服务</b></p>
		    			</li>
		    		</ul>
		    	</div>
		    	<div id="friendlyLink" class="clearfix mt20 border">
		    		<div class="fl">
			    		<div class="title">
			    			<h2>友情链接</h2>
			    		</div>
			    		<ul class="friendlyLink-list">
			    			<li><a href="">咖啡师培训</a><em>|</em></li>
			    			<li><a href="">猫窝咖啡</a><em>|</em></li>
			    			<li><a href="">中国饮料网</a><em>|</em></li>
			    			<li><a href="">咖啡</a><em>|</em></li>
			    			<li><a href="">白酒招商</a><em>|</em></li>
			    			<li><a href="">圭江论坛</a><em>|</em></li>
			    			<li><a href="">中国广告网</a><em>|</em></li>
			    			<li><a href="">福来高咖啡</a><em>|</em></li>
			    			<li><a href="">湖南黑</a><em>|</em></li>
			    			<li><a href="">茶叶商城</a><em>|</em></li>
			    			<li><a href="">土特产</a><em>|</em></li>
			    			<li><a href="">空调</a><em>|</em></li>
			    			<li><a href="">iseemini</a><em>|</em></li>
			    			<li><a href="">行业资讯</a><em>|</em></li>
			    		</ul>
		    		</div>
		    		<div class="fr">
		    			<div class="weixin-img">
		    				<img src="images/img.jpg" alt="咖啡网" />
		    			</div>
		    			<div class="weixin-con">
		    				<div class="weixin-tit"><b>官方微信</b></div>
		    				<div class="weixin-p">公众号：<span>kafeiwang</span></div>
		    				<div class="weixin-txt">扫描二维码关注全国最大的咖啡网</div>
		    			</div>
		    		</div>
		    	</div>
	    	</div>
	    </div><!-- 网站主题 -->
	    <div id="footer" class="mt20">
	    	<div class="link">
	    		<a href="">关于我们</a>
	    		<em>|</em>
	    		<a href="">商务合作</a>
	    		<em>|</em>
	    		<a href="">招聘信息</a>
	    		<em>|</em>
	    		<a href="">联系我们</a>
	    	</div>
	    	<div class="copyright">
	    		Copyright&nbsp;©&nbsp;2011&nbsp;coffee.cn All Rights Reserved. &nbsp;&nbsp;咖啡网&nbsp;版权所有&nbsp;&nbsp;京ICP备06065461号-1&nbsp;&nbsp;京公11011502002369
	    	</div>
	    </div>
    </div>
    <script type="text/javascript" src="./js/user/globle.js"></script>
	<script type="text/javascript" src="./js/user/jquery.lazyload.js"></script>
</body>
</html>