
 

	
	
 /** 微信图片上传* */
	var wxSelf;
	var localArr = new Array();
	var localHeadArr = new Array();//头像
	var localCloseArr = new Array();//日常推文的组合  
	var localMiddelArr = new Array();//存放活动的图片

	var serverHeadArr = new Array();//头像
	var serverCloseArr = new Array();//日常推文的组合  
	var serverMiddelArr = new Array();//存放活动的图片

	
	
	
	/*001 选择图片*/
	function uploadImage(flag,num) {
		var clientUrl = window.location.href;
		var reqPath = path+'/skillUser/getJSConfig.html';
		//请求后台，获取jssdk支付所需的参数
		$.ajax({
			type : 'post',
			url : reqPath,
			dataType : 'json',
			data : {
				"clientUrl" : clientUrl
				//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
			},
			cache : false,
			error : function() {
				alert("系统错误，请稍后重试");
				return false;
			},
			success : function(data) {
				//微信支付功能只有微信客户端版本大于等于5.0的才能调用
				var return_date = eval(data);
				/* alert(return_date ); */
				if (parseInt(data[0].agent) < 5) {
					alert("您的微信版本低于5.0无法使用微信支付");
					return;
				}
				//JSSDK支付所需的配置参数，首先会检查signature是否合法。
				wx.config({
					debug : false, //开启debug模式，测试的时候会有alert提示
					appId : return_date[0].appId, //公众平台中-开发者中心-appid
					timestamp : return_date[0].config_timestamp, //时间戳
					nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
					signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
					jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
				});
				//上方的config检测通过后，会执行ready方法
				wx.ready(function() {
					wxSelf=wx;
					wx.chooseImage({
						count: num, // 默认9
						sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						success: function (res) {
							
							if(flag =="1"){//头像
								localHeadArr = res.localIds;
								if(localHeadArr.length > 0){
									
							        for(var j = 0; j < localHeadArr.length; j++){
							        	 var myAvatar=localHeadArr[0];
									        $("#addText").html("");
									        $(".enter_jia").addClass('myInfo_photo');
									        //toggleClass("myInfo_photo");
									        $("#myAvatar").attr("src",myAvatar);
									        $("#myAvatar").css('display','block');
									        headFlag=false; 
							        }
								}
							}
							if(flag =="2"){//近景
								localCloseArr= appandArray(localCloseArr,res.localIds);
								if(localCloseArr.length > 0){
									var result = "";
							        for(var j = 0; j < res.localIds.length; j++){
							        	result += "<div id='close_"+j+"' class='add_picture'>"+
												"<img src='"+res.localIds[j]+"' class='search_pic' />"+
													"<span onclick='removeImg("+j+",this)' class='iconfont icon-shanchu' ></span>"+
													"<span class='cover'></span>"+
												"</div>";
												
							        }
							        var imgNum = $("#closePhoto img").length;
								    var picNum =  parseInt(imgNum) + parseInt(localCloseArr.length);
								    if(picNum>9){
								    	alert("请上传2-9张图片！");
								    }else{
								    	$("#closePhoto").append(result);
								    } 
								}
							}
							if(flag =="3"){//中景
								localMiddelArr= appandArray(localMiddelArr,res.localIds);
						
								if(localMiddelArr.length > 0){
									var result = "";
							        for(var j = 0; j < res.localIds.length; j++){
							        	result += "<div id='middle_"+j+"' class='add_picture'>"+
												"<img src='"+res.localIds[j]+"' class='search_pic' />"+
													"<span onclick='removeImg("+j+",this)' class='iconfont icon-shanchu' ></span>"+
													"<span class='cover'></span>"+
												"</div>";
							        }
							        var imgNum = $("#middlePho img").length;
								    var picNum =  parseInt(imgNum) + parseInt(localMiddelArr.length);
								    if(picNum>9){
								    	alert("请上传2-9张图片！");
								    }else{
								    	$("#middlePho").append(result);
								    }
								}
							}
							if(flag =="4"){//全景
								localAlllArr=appandArray(localAlllArr,res.localIds);
								if(localAlllArr.length > 0){
									var result = "";
							        for(var j = 0; j < res.localIds.length; j++){
							        	result += "<div id='all_"+j+"' class='add_picture'>"+
												"<img src='"+res.localIds[j]+"' class='search_pic' />"+
													"<span onclick='removeImg("+j+",this)' class='iconfont icon-shanchu' ></span>"+
													"<span class='cover'></span>"+
												"</div>";
							        }
							        var imgNum = $("#allPhoto img").length;
								    var picNum =  parseInt(imgNum) + parseInt(localAlllArr.length);
								    if(picNum>9){
								    	alert("请上传2-9张图片！");
								    }else{
								    	$("#allPhoto").append(result);
								    }
							        
								}
							}
						}
					 });
					});
					 wx.error(function(res) {
						alert(res.errMsg);
					});
				}
		});
	}
	/*上传用户头像Self*/
	function uploadHeadImageSelf(){
		//alert("head****function");
		if(0==localHeadArr.length){
			uploadCloseImages();
		 }else{
			 
		 for(var i=0;i<localHeadArr.length;i++){
			 wxSelf.uploadImage({
			        localId: localHeadArr[i], // 需要上传的图片的本地ID，由chooseImage接口获得
			        isShowProgressTips: 1, // 默认为1，显示进度提示
			        success: function (res) {
			            var serverId = res.serverId; // 返回图片的服务器端ID
			            serverHeadArr.push(serverId);
			            if(i == localHeadArr.length){
			            	 //alert("执行上传 count"+serverArr);  
			                var serverStr="";
			                if(serverHeadArr[0].length >0 ){
			    	            serverStr=serverHeadArr[0] ;
			    	           // alert(serverStr);
			    	            $.ajax({
			    		        		type : "post",
			    		        		url : path+"/skillUser/downloadImageSelf.html",
			    		        		dataType : "json",
			    		        		data : {serverId:serverStr},
			    		        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
			    		        			//alert("head***function*****data="+data.path);
			    		        			$("#headPortrait").val(data.path);
			    		        			uploadCloseImages();
			    		        		}
			    		        	});	
			                	
			                }else{
			                	alert("上传头像失败");
			                }
			                            	
			            }
			         }
			    }); 
		 }
		 }
		 //downloadImageSelf(serverArr);
	}
	/*近景图片上传*/
	function uploadCloseImages() {
		//alert("Close*****function****localCloseArr.length："+localCloseArr.length);
        if (localCloseArr.length == 0) {
        	var serverStr="";//头像
        	if(0 == serverCloseArr.length ){
        		uploadMiddelImages();
        	}else{
        		for(var j=0;j<serverCloseArr.length;j++){
                	serverStr+=serverCloseArr[j]+";" ;
                }
        		//alert(serverStr);
                $.ajax({
            		type : "post",
            		url :  path+"/skillUser/downloadImageSelf.html",
            		dataType : "json",
            		data : {serverId:serverStr},
            		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
            			
            			var imageList = data;
            			//alert(imageList);
            			var imgPath=$("#closePhoto_img").val();
    					for(var f=0;f<imageList.length;f++){
    						var imgPathTemp = imageList[f];
    						imgPath += imgPathTemp+",";
    					}
    					//alert(imgPath);
    					$("#closePhoto_img").val(imgPath);
    					//alert("Close*****function****data"+imgPath);
    					uploadMiddelImages();
            		}
            	});	
        	}
        	
        }
        var localId = localCloseArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 1, // 默认为1，显示进度提示
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
                serverCloseArr.push(serverId);
                localCloseArr.shift();
                uploadCloseImages(localCloseArr);
                serverStr+=serverId+";" ;
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
            }
        });
    }
	
	/* 中景图片上传*/
	function uploadMiddelImages() {
		//alert("Middel*****function****localMiddelArr.length："+localMiddelArr.length);
        if (localMiddelArr.length == 0) {
        	var serverStr="";
        	if(0 == serverMiddelArr.length ){
        		uploadAllImages();
        	}else{
        		for(var j=0;j<serverMiddelArr.length;j++){
                	serverStr+=serverMiddelArr[j]+";" ;
                }
    	        $.ajax({
    	    		type : "post",
    	    		url : path+"/skillUser/downloadImageSelf.html",
    	    		dataType : "json",
    	    		data : {serverId:serverStr},
    	    		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
    	    			
    	    			//alert("+++++++++data++++++:"+data);	
    	    			var imageList = data;
    	    			//var pathList = data.
    	    			
    	    			var imgPath=$("#middlePho_img").val();
    	    		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
    					for(var f=0;f<imageList.length;f++){
    				//		alert("+++++++++++++++:"+imageList[f]);
    						var imgPathTemp = imageList[f];
    						imgPath += imgPathTemp+",";
    					}
    					$("#middlePho_img").val(imgPath);
    					//alert("Middel*****function****data"+imgPath);
    					uploadAllImages();
    	    		}
    	    	});	
        	}
        	
        }
        var localId = localMiddelArr[0];
        //tmd 一定要加     解决IOS无法上传的坑 
        if (localId.indexOf("wxlocalresource") != -1) {
            localId = localId.replace("wxlocalresource", "wxLocalResource");
        }
        wxSelf.uploadImage({
            localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
            isShowProgressTips: 0, // 默认为1，显示进度提示
            success: function (res) {
               // serverIds.push(res.serverId); // 返回图片的服务器端ID
               var serverId = res.serverId; // 返回图片的服务器端ID
               serverMiddelArr.push(serverId);
               localMiddelArr.shift();
               uploadMiddelImages(localMiddelArr);
               serverStr+=serverId+";" ;
               // alert("localArr的长度："+localArr.length);
            },
            fail: function (res) {
                alert("上传失败，请重新上传！");
            }
        });
  }
	
	function removeCloseImg(imageId){
		$("#close_"+imageId).remove();
	}
	function removeMiddelImg(imageId){
		$("#middle_"+imageId).remove();
	}
	function removeImg(imageId,obj){
		obj.parentElement.remove();
		
		
		//$("#all_"+imageId).remove();
	}
	// 追加数组的方法 a 被追加的数组 ， b 要追加的数组
	function appandArray(a,b){

		for(var i=0;i<b.length;i++){
			 a.push(b[i]);
		}
		return a;
	}
	