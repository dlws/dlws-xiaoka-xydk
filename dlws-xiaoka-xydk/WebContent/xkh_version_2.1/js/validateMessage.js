/**
 * 校验当前所提交的参数是否合法
 */
	function addEnter(){
		
		var contactUser = $("#contactUser").val();
		if(contactUser==""){
			alert("请输入联系人");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		var fieldSize = $("fieldSize").val();
		if(fieldSize==""){
			alert("场地类型不能为空")
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		var userName = $("#userName").val();
		if(userName==""){
			alert("请输入入驻名称");
			 $(".webservice_mask").addClass("mask_none");
			return
		}else if(userName.length>10){
			alert("入驻名称不得超过10个字");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		var phoneNumber=$("#phoneNumber").val()
			var re=/^1[3|5|7|8]\d{9}$/;//更改手机号验证问题
   		if(phoneNumber==""){
   			alert("请输入手机号");
   		   $(".webservice_mask").addClass("mask_none");
   			return;
   		}else if(!re.test(phoneNumber)){
   			alert("请输入正确的手机号");
   		   $(".webservice_mask").addClass("mask_none");
   			return;
   		}
   		/*var email=$("#email").val()
   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
   		
   		if(email==""||!regEma.test(email)){
   			alert("邮箱格式有误或不能为空");
   		    $(".webservice_mask").addClass("mask_none");
   			return
   		}
   		var wxNumber=$("#wxNumber").val()
   		var regWx=/^[a-zA-Z\d_]{5,}$/;
   		if(wxNumber==""||!regWx.test(wxNumber)){
	   		alert("微信号格式有误或不能为空");
	   	    $(".webservice_mask").addClass("mask_none");
	   		return
   		}*/
   		if($.trim($("#wxNumber").val())!=""){
   			var regWx=/^[a-zA-Z\d_]{5,}$/;
   			if(!regWx.test(wxNumber)){
   		   		alert("微信号格式有误");
   		   		return;
   	   		}
   		}
   		
   		
   		
		var picUrl = $.trim($("#headPortrait").val());
		
		var schools = $("#schoolId").val();
		if(schools==""||schools==null){
			alert("请选择学校！");
			$(".webservice_mask").addClass("mask_none");
			return
		}
		var aboutMe = $("#currentDec").val();
		if(aboutMe==""){
			alert("请输入场地介绍");
			$(".webservice_mask").addClass("mask_none");
			return;				
		}
		if(aboutMe.length<20||aboutMe.length>200){
			alert("场地描述不能少于20字或不能大于200字");
			$(".webservice_mask").addClass("mask_none");
			return;
		}
		if($("#begin").val()==""||$("#end").val()==""){
			alert("开始时间，或者结束时间不能为空");
			$(".webservice_mask").addClass("mask_none");
			return
		}
		if(parseInt($("#begin").val())>parseInt($("#end").val())){
			alert("场地开始使用时间不能大于场地结束使用时间");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		if($("#planprice").val()==""){
			alert("场地报价不能为空");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		var close = $("#closePhoto").children();
		var moddle = $("#middlePho").children();
		var all = $("#allPhoto").children();
		//图片上传限制最少上传2张每个种类
		var url = $("#myAvatar")[0].src;

		if(url==""){
			alert("上传头像不能为空");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		if( close.length<2||close.length>9){
			alert("近景图片必须为2~9张");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		if( moddle.length<2||moddle.length>9){
			alert("中景图片必须为2~9张");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		if( all.length<2||all.length>9){
			alert("全景图片必须为2~9张")
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		//增加遮罩
		/*var h=$(document).height();
		$(".webservice_mask").css("height",h);*/
		$(".webservice_mask").removeClass("mask_none"); 
		//上传图片并提交表单
		uploadHeadImageSelf();
		//暂时屏蔽关于图片的验证
		//$("#enterForm").submit();
		
	}
	
	/**
	 * 校验校园入驻参数是否合法
	 */
	function addSchoolBusi(){
		
		var h=$(".webservice_container").height();
		$(".webservice_mask").css("height",h);
		$(".webservice_mask").removeClass("mask_none"); 
		var contactUser = $("#contactUser").val();
		if(contactUser==""){
			alert("请输入联系人");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		var userName = $("#userName").val();
		if(userName==""){
			alert("请输入入驻名称");
			 $(".webservice_mask").addClass("mask_none");
			return
		}else if(userName.length>10){
			alert("入驻名称不得超过10个字");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		var phoneNumber=$("#phoneNumber").val()
			var re=/^1[3|5|7|8]\d{9}$/;//更改手机号验证问题
   		if(phoneNumber==""){
   			alert("请输入手机号");
   		    $(".webservice_mask").addClass("mask_none");
   			return;
   		}else if(!re.test(phoneNumber)){
   			alert("请输入正确的手机号");
   		   $(".webservice_mask").addClass("mask_none");
   			return;
   		}
   		/*var email=$("#email").val()
   		var regEma=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/;
   		
   		if(email==""||!regEma.test(email)){
   			alert("邮箱格式有误或不能为空");
   			$(".webservice_mask").addClass("mask_none");
   			return
   		}
   		var wxNumber=$("#wxNumber").val()
   		var regWx=/^[a-zA-Z\d_]{5,}$/;
   		if(wxNumber==""||!regWx.test(wxNumber)){
	   		alert("微信号格式有误或不能为空");
	   		$(".webservice_mask").addClass("mask_none");
	   		return
   		}*/
   		if($.trim($("#wxNumber").val())!=""){
   			var regWx=/^[a-zA-Z\d_]{5,}$/;
   			if(!regWx.test(wxNumber)){
   		   		alert("微信号格式有误");
   		   		return;
   	   		}
   		}
   		
		var picUrl = $.trim($("#headPortrait").val());
		
		var schools = $("#schoolId").val();
		if(schools==""||schools==null){
			alert("请选择学校！");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		var aboutMe = $("#storeDec").val();
		if(aboutMe.length<20||aboutMe.length>200){
			alert("门店描述不能少于20字或不能大于200字");
			 $(".webservice_mask").addClass("mask_none");
			return;
		} 
		if(aboutMe==""){
			alert("请输入门店介绍");
			 $(".webservice_mask").addClass("mask_none");
			return;				
		}
		var close = $("#closePhoto").children();
		var moddle = $("#middlePho").children();
		var all = $("#allPhoto").children();
		//图片上传限制最少上传2张每个种类
		var url = $("#myAvatar")[0].src;

		if(url==""){
			alert("上传头像不能为空");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		if( close.length<2||close.length>9){
			alert("近景图片必须为2~9张");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		if( moddle.length<2||moddle.length>9){
			alert("中景图片必须为2~9张");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		if( all.length<2||all.length>9){
			alert("全景图片必须为2~9张");
			 $(".webservice_mask").addClass("mask_none");
			return
		}
		//增加遮罩
		//var h=$(".webservice_container").height();
		//$(".webservice_mask").css("height",h);
		$(".webservice_mask").removeClass("mask_none"); 
		uploadHeadImageSelf();
		//$("#addBusi").submit();
}
