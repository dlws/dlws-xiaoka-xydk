<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<title>校咖网</title>
		<meta name="description" content=""/>
		<meta name="keywords" content=""/>
		<link href="./css/base.css" rel="stylesheet" />
		<link href="./css/index.css" rel="stylesheet" />
		<script type="text/javascript" src="./js/user/jquery-1.7.2.min.js"></script>
		<script src="./js/user/login.js"></script>
	</head>
	<body>
    <div id="warpper">
    	<div id="top">
	    	<div class="w">
	    		<div id="share">
	    			<span class="lightgrey fl">我要分享</span>
	    			<div class="bshare-custom fl"><div class="bsPromo bsPromo2"></div><a title="分享到QQ空间" class="bshare-qzone"></a><a title="分享到新浪微博" class="bshare-sinaminiblog"></a><a title="分享到人人网" class="bshare-renren"></a><a title="分享到腾讯微博" class="bshare-qqmb"></a><a title="分享到网易微博" class="bshare-neteasemb"></a><a title="更多平台" class="bshare-more bshare-more-icon more-style-addthis"></a></div><script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/buttonLite.js#style=-1&amp;uuid=&amp;pophcol=1&amp;lang=zh"></script><script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/bshareC0.js"></script>
	    		</div>
	    		<div class="fr">
	    			<ul>
	    				<li>欢迎来到咖啡网！</li>
	    				<!-- <li><span>|</span><a href="./register.jsp">注册</a></li> -->
	    				<li><span>|</span><a href="./login.jsp">登录</a></li>
	    			</ul>	
	    		</div>
    		</div>
    	</div><!-- 网站页头 -->
    	<div id="header" class="w">
    		<div id="logo2">
    			<a href="">咖啡网</a>
    		</div>
    		<div class="tel">
    			客服热线：  400-890-9850
    		</div>
    	</div><!-- 网站头部 -->
    	<div id="nav">
    		<div class="w">
    			<ul>
    				<li class="current"><a href="">首页</a></li>
    				<li><a href="">合作项目</a></li>
    				<li><a href="">发布项目</a></li>
    				<li><a href="">校园咖啡</a></li>
    				<li><a href="">创业服务</a></li>
    				<li><a href="">关于我们</a></li>
    			</ul>
    		</div>
    	</div><!-- 网站主导航 -->
    	<div id="main">
	    	<div class="w">
		    	<div class="login">
		    		<form id = "formId" name = "myform" action="${pageContext.request.contextPath}/j_spring_security_check" method="post" accept-charset="utf-8" >
		    			<div class="login-item">
		    				<label>用户名</label>
		    				<p class="input-box">
		    					<input id="username" type="text" name="j_username" placeholder="输入您的用户名"/>
		    				</p>
		    			</div>
		    			<div class="login-item">
		    				<label>密码</label>
		    				<p class="input-box">
		    					<input id="password" type="password" name = "j_password" placeholder="输入您的密码">
		    				</p>
		    			</div>
		    			<c:if test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message == 'Bad credentials'}">
	                     	<div class="login-item">
	                     		<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
		                     	<p class="input-box">
		                     		<font color="red">用户名或密码错误，请重新登陆</font>
		                     	</p>
	                     	</div>
                         </c:if>
		    			<div class="login-item">
		    				<div class="login-remember">
		    					<div class="lr-l fl"><input type="checkbox" name="" id="">记住我的登录状态</div>
		    					<!-- <a href="./retrievePassword.jsp" class="fr">忘记密码？</a> -->
		    				</div>
		    			</div>
		    			
		    			<div class="login-btn">
		    				<button id="submit" type="submit">登录</button>
		    			</div>
		    		</form>
		    		<div class="loginLine"></div>
		    		<!-- <div class="loginTxt">
		    			还没有校咖帐号？<a href="./register.jsp">立即注册</a>
		    		</div> -->
		    	</div>
		    	<div id="serviceGuarantee" class="mt20">
		    		<ul class="serviceGuarantee-list">
		    			<li>
		    				<p class="list-icons list-icon1"></p>
		    				<p class="list-tit"><b>资源整合</b></p>
		    			</li>
		    			<li class="lineLeft"></li>
		    			<li>
		    				<p class="list-icons list-icon2"></p>
		    				<p class="list-tit"><b>跨界经营</b></p>
		    			</li>
		    			<li class="lineLeft"></li>
		    			<li>
		    				<p class="list-icons list-icon3"></p>
		    				<p class="list-tit"><b>网络营销</b></p>
		    			</li>
		    			<li class="lineLeft"></li>
		    			<li>
		    				<p class="list-icons list-icon4"></p>
		    				<p class="list-tit"><b>融资贷款</b></p>
		    			</li>
		    			<li class="lineLeft"></li>
		    			<li>
		    				<p class="list-icons list-icon5"></p>
		    				<p class="list-tit"><b>创业服务</b></p>
		    			</li>
		    		</ul>
		    	</div>
		    	<div id="friendlyLink" class="clearfix mt20 border">
		    		<div class="fl">
			    		<div class="title">
			    			<h2>友情链接</h2>
			    		</div>
			    		<ul class="friendlyLink-list">
			    			<li><a href="">咖啡师培训</a><em>|</em></li>
			    			<li><a href="">猫窝咖啡</a><em>|</em></li>
			    			<li><a href="">中国饮料网</a><em>|</em></li>
			    			<li><a href="">咖啡</a><em>|</em></li>
			    			<li><a href="">白酒招商</a><em>|</em></li>
			    			<li><a href="">圭江论坛</a><em>|</em></li>
			    			<li><a href="">中国广告网</a><em>|</em></li>
			    			<li><a href="">福来高咖啡</a><em>|</em></li>
			    			<li><a href="">湖南黑</a><em>|</em></li>
			    			<li><a href="">茶叶商城</a><em>|</em></li>
			    			<li><a href="">土特产</a><em>|</em></li>
			    			<li><a href="">空调</a><em>|</em></li>
			    			<li><a href="">iseemini</a><em>|</em></li>
			    			<li><a href="">行业资讯</a><em>|</em></li>
			    		</ul>
		    		</div>
		    		<div class="fr">
		    			<div class="weixin-img">
		    				<img src="./images/img.jpg" alt="咖啡网" />
		    			</div>
		    			<div class="weixin-con">
		    				<div class="weixin-tit"><b>官方微信</b></div>
		    				<div class="weixin-p">公众号：<span>kafeiwang</span></div>
		    				<div class="weixin-txt">扫描二维码关注全国最大的咖啡网</div>
		    			</div>
		    		</div>
		    	</div>
	    	</div>
	    </div><!-- 网站主题 -->
	    <div id="footer" class="mt20">
	    	<div class="link">
	    		<a href="">关于我们</a>
	    		<em>|</em>
	    		<a href="">商务合作</a>
	    		<em>|</em>
	    		<a href="">招聘信息</a>
	    		<em>|</em>
	    		<a href="">联系我们</a>
	    	</div>
	    	<div class="copyright">
	    		Copyright&nbsp;©&nbsp;2011&nbsp;coffee.cn All Rights Reserved. &nbsp;&nbsp;咖啡网&nbsp;版权所有&nbsp;&nbsp;京ICP备06065461号-1&nbsp;&nbsp;京公11011502002369
	    	</div>
	    </div>
    </div>
</body>
</html>