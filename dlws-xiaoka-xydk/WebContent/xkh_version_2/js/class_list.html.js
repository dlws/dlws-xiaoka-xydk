$('.hot_inp').bind('input propertychange', function() {  
   if($(this).val().length!=0){
	   $(".icon_remove").show();
   }else{
	   $(".icon_remove").hide();
   }
});
var timer="";
//点击取消清空搜索框
$(".ctt_cancel,.icon_remove").click(function(){
	$(".hot_inp").val("");
});

var pageNum = 10;

//获取技能详情
function getSkillInfo(id,fathValue){
	$.cookie('xkh_lock','open'); 
	//window.location.href="../home/getSkillInfo.html?id="+id;
	//alert(id+"*************************"+fathValue);
	window.location.href="../skill_detail/getskill_detail.html?skillId="+id+"&&classValue="+fathValue;
}
function joinSelected(skillId,isPreferred){
	var enterType=$("#openId_"+skillId).val();
	var openId=$("#type_"+skillId).val();
	window.location.href="../skillOrder/joinSelected.html?skillId="+skillId+"&openId_sel="+openId+"&isPreferred="+isPreferred+"&enterType="+enterType;
}
//
function goUserInfo(id){
	window.location.href="../personInfoDetail/detail.html?id="+id;
}

$(function() {
	var itemIndex = 0;
	var dataIndex = 0;
	var sid = 0;
	var page = 1;
    var tab1LoadEnd = false;
    var tab2LoadEnd = false;
    var tab3LoadEnd = false;
    var tab4LoadEnd = false;
    var tab5LoadEnd = false;
    var val1=""
    var citys = proList;
    var dataname ="";
    //定义公共的引用不到，在这个js中进行数据赋值
    var classSon_image = "x-oss-process=image/resize,m_fill,w_380,h_194,limit_0/auto-orient,0/quality,q_100";
    var classSon_headImage = "x-oss-process=image/resize,m_fill,w_52,h_52,limit_0/auto-orient,0/quality,q_70"
    //点击搜索图标
    $(".ctt_cancel").click(function() {
		$(".hot_inp").val("")
	})
//		//设置全部的样式
//		var ht=$(".class_container").height();
//		$(".class_left").css({"height":ht,"line-height":ht+'px'});
//		$(".denote").click(function(){
//	    var ht1=$(".class_list_head").outerHeight()+$(".class_title").outerHeight()+$(".class_container").outerHeight();
//	         $(this).toggleClass("class_up").toggleClass("class_down");
//
//	         $(".class_container").toggleClass("none");
//	         $(".mask").toggle();
//	         $(".mask").css("top",ht1);
//	         if($(this).hasClass("class_down")){
//	        	 $(".class-title1 li").not($(".class-title1 li:first-child")).hide()
//	         }else{
//	        	 $(".class-title1 li").show()
//	         }
//		});
		/*$(".class_right li").click(function(){
			$(".class_container").addClass("none");
			$(".mask").hide();
			$(".denote").removeClass("class_down").addClass("class_up")
		})*/
	//标题左右滑动
	$(".secondIndex_title li").click(function(){
		itemIndex=$(this).index();
		$(this).addClass("title_active").siblings().removeClass("title_active");
		$(">span",this).addClass("title_color");
		$(this).siblings().find("span").removeClass("title_color")
		
		jQuery("#maxPrice").val("");
		jQuery("#minPrice").val("");
		jQuery("#cityName").val("");
		jQuery("#schoolLabel").val(""); 
		jQuery("#serviceType").val("");			 
		var $this = $(this);
	    sid = this.id;
	    $this.addClass('cur').siblings('.item').removeClass('cur');
        $('.lists').eq(itemIndex).show().siblings('.lists').hide();
//	    // 如果数据没有加载完
        dropload.unlock();
        dropload.noData(false);
//        // 重置
        dropload.resetload();   
		
	})
	//点击默认排序相关
	$(".class_sort li").click(function() {
    	$(this).addClass("class_active").siblings().removeClass("class_active");
    	var index = $(this).index();
    	$(".class_sort span").removeClass("icon-gou").eq(index).addClass("icon-gou");
    	
    	dataname = this.dataset.name
    	$("#orderPrice").val(dataname);
    	$('.lists').eq(itemIndex).html("");
    	 page = 1;//当前页
    	 $("#page").val(1);
    	 dropload.unlock();
         dropload.noData(false);
         dropload.resetload();
    	 setTimeout(function() {
    		$(".class_sortContent").hide();
    		$('.iconSort').removeClass("icon_up").addClass("icon_down")
    	}, 1000)
    })
    //销量排序相关
	$('.sales').click(function() {
		$("#ordersellNo").val("desc");
		$('.lists').eq(itemIndex).html("");
		page = 1;// 当前页
		$("#page").val(1);
		dropload.unlock();
		dropload.noData(false);
		dropload.resetload();
		setTimeout(function() {
			$(".class_sortContent").hide();
			$('.iconSort').removeClass("icon_up").addClass("icon_down")
		}, 1000)
	});
    //筛选   点击确定
	$(".enure_box").click(function() {
		$(".class_filterContent").hide();
		$(".mask").hide();
		$('.lists').eq(itemIndex).html("");
		 page = 1;//当前页
		 $("#page").val(1);
		 dropload.unlock();
	     dropload.noData(false);
	     dropload.resetload();
	})
	// ajax请求这一部分
	$(".class_title li").click(function() {
		//alert("this.id class_title"+this.id);
		jQuery("#maxPrice").val("");
 		jQuery("#minPrice").val("");
 		jQuery("#cityName").val("");
 		jQuery("#schoolLabel").val(""); 
 		jQuery("#serviceType").val(""); 
 		jQuery("#ordersellNo").val("");
		
		var $this = $(this);
        itemIndex = $this.index();
        dataIndex = this.dataset.name;
      //  page = this.value;
        sid = this.id;
        $this.addClass('cur').siblings('.item').removeClass('cur');
        $('.lists').eq(itemIndex).show().siblings('.lists').hide();
         // 如果选中菜单一
        var li_length = $('.lists').eq(itemIndex).find("li").length;
        //alert("********************li_length:"+li_length );
        if(li_length>0){//说明已经加载过了
        	if(li_length-pageNum == 0){
        		page = 2;
        		sid = this.id;
                $this.addClass('cur').siblings('.item').removeClass('cur');
                $('.lists').eq(itemIndex).show().siblings('.lists').hide();
                // 如果数据没有加载完
                dropload.unlock();
                dropload.noData(false);
                // 重置
                dropload.resetload();
        	}
        }else{
        	 sid = this.id;
             $this.addClass('cur').siblings('.item').removeClass('cur');
             $('.lists').eq(itemIndex).show().siblings('.lists').hide();
             // 如果数据没有加载完
             dropload.unlock();
             dropload.noData(false);
             // 重置
             dropload.resetload();
        }
       /* if(itemIndex == dataIndex){
//        	var $this = $(this);
//          itemIndex = $this.index();
//          dataIndex = this.dataset.name;
          //  page = this.value;
            sid = this.id;
            $this.addClass('cur').siblings('.item').removeClass('cur');
            $('.lists').eq(itemIndex).show().siblings('.lists').hide();
            // 如果数据没有加载完
            dropload.unlock();
            dropload.noData(false);
            // 重置
            dropload.resetload();
            //alert("bbbbbbbbbbbbb");
        }*/
    });
	/*$(".class_left").click(function() {
		if($(this).hasClass("class_down")){
        	$(".class-title1 li").not(".class-title1 li:first-child").hide()
         }else{
        	 $(".class-title1 li").show();
         }
		$(".class_container").addClass("none");
		$(".mask").hide();
		$(".denote").removeClass("class_down").addClass("class_up")
		//alert("this.id class_left"+this.id);
			
			jQuery("#maxPrice").val("");
	 		jQuery("#minPrice").val("");
	 		jQuery("#cityName").val("");
	 		jQuery("#schoolLabel").val(""); 
	 		jQuery("#serviceType").val(""); 
			
	        itemIndex = 1;
	        dataIndex = 1;
	      //  page = this.value;
	        sid = 0;
	         // 如果选中菜单一
	        if(itemIndex == dataIndex){
	            itemIndex = 1;
	            dataIndex = 1;
	          //  page = this.value;
	            sid = 0;
	            // 如果数据没有加载完
	            dropload.unlock();
	            dropload.noData(false);
	            // 重置
	            dropload.resetload();
	        }
	    });*/
/*$(".class_right li").click(function() {
	if($(this).hasClass("class_down")){
    	$(".class-title1 li").not(".class-title1 li:first-child").hide()
     }else{
    	 $(".class-title1 li").show();
     }
	
	$(".class_container").addClass("none");
	$(".mask").hide();
	$(".denote").removeClass("class_down").addClass("class_up")
	//alert("this.id class_right"+this.id);
		
		jQuery("#maxPrice").val("");
 		jQuery("#minPrice").val("");
 		jQuery("#cityName").val("");
 		jQuery("#schoolLabel").val(""); 
 		jQuery("#serviceType").val(""); 
		var $this = $(this);
        itemIndex = $this.index();
        $('.lists li').remove();
        dataIndex = this.dataset.name;
      //  page = this.value;
        sid = this.id;
        $this.addClass('cur').siblings('.item').removeClass('cur');
        $('.lists').eq(itemIndex).show().siblings('.lists').hide();
         // 如果选中菜单一
        if(itemIndex == dataIndex){
        	
        	var $this = $(this);
            itemIndex = $this.index();
            dataIndex = this.dataset.name;
          //  page = this.value;
            sid = this.id;
            $this.addClass('cur').siblings('.item').removeClass('cur');
            $('.lists').eq(itemIndex).show().siblings('.lists').hide();
            // 如果数据没有加载完
            dropload.unlock();
            dropload.noData(false);
            // 重置
            dropload.resetload();
        }
    });*/
    
//    设置弹出框始终在屏幕中央
    var width=$(".menu").width();
    var height=$(".menu").height();
    var left=(document.documentElement.clientWidth-width)/2;
    var top=(document.documentElement.clientHeight-height)/2;
    $(".menu").css({"left":left,"top":top});
    
    /*$(".class_sort li").click(function(){
		$(this).addClass("class_active").siblings().removeClass("class_active");
		var index=$(this).index();
		dataname = this.dataset.name
		$(".class_sort span").removeClass("icon-gou").eq(index).addClass("icon-gou");
		var data_n = $("#orderp"+sid).val();
		var gao=$("body").height();
		console.log(gao);
		if(dataname == data_n){
			//$(".menu").css("display","block")
//			$(".sort_mask").css({"height":gao+"px","position":'absolute',"width":"100vw","background":"#000","z-index": 100,"opacity": 0.2})
//		    $(".menu_child2").click(function(){
//		    	$(".sort_mask").css("height",0);
//		    	$(".menu").css("display","none");
//		    });
			
			$(".class_sortContent").addClass("show");
			
		}else{
			jQuery("#orderp"+sid).val(dataname);
			$("#"+sid).val(2);
			 dropload.unlock();
             dropload.noData(false);
            var fid2 =  $("#fid2").val();
			serchList(sid,dataname,fid2)
			$(".class_sortContent").addClass("show");
		}
//		延迟一秒以后关闭默认排序 ----不能这样设置这样设置dropload不起作用
//		setTimeout(function(){
//			$(".class_sortContent").addClass("show");
//		},1000)

	});*/
    
    // dropload
    var dropload = $('.content').dropload({
        scrollArea : window,
        loadDownFn : function(me){
            // 加载菜单一的数据
        	var orderp = jQuery("#orderp"+sid).val();
        	var sorting = dataname;
        	var fid2 =  $("#fid2").val();
        	page=parseInt(document.getElementById(sid).getAttribute("value")) ;
        	pageNum = 10;
        	var max = jQuery("#maxPrice").val();
    		var min = jQuery("#minPrice").val();
    		var cname = jQuery("#cityName").val();
    		var schoolLabel = jQuery("#schoolLabel").val(); 
    		var ordersellNo = jQuery("#ordersellNo").val(); 
    		var serviceType = jQuery("#serviceType").val();
    		var lock = $.cookie('xkh_lock');
    		var xkh_page_num = jQuery("#xkh_page_num").val();
    		var xkh_class_id = jQuery("#xkh_class_id").val();
    		if(lock == 'open'){
    			$("#"+xkh_class_id).addClass("title_active cur").siblings().removeClass();
    			$("#"+xkh_class_id).children("span").addClass("title_color").parent().siblings().children().removeClass();
    			page = 1;
    			pageNum = (xkh_page_num)*pageNum;
    			jQuery("#sid").val(xkh_page_num);
    			sid= xkh_class_id;
    		}
    		
    		$.ajax({
            	type : "post",
				url : "getUserInfoByClassIdV2.html",
				dataType : "json",
				async : false,
				data : {currentPage:page,pageNum:pageNum,sid:sid,fid:fid2,orderPrice:sorting,maxPrice:max,minPrice:min,cityName:cname,schoolLabel:schoolLabel,serviceType:serviceType,sellNo:ordersellNo},
                success: function(data){
                    var result = '';
//                    if(pageStart <= data.skills.length){
                        for(var j = 0; j < data.skills.length; j++){
                        	 var result1 = '';
                        	 var result2 = '';
                        	 var result3 = '';
                        	 var result4 = '';
                        	 result += '<li>' +
								'<div class="search_bg"></div>' +
								'<div class="searchList_content">' +
								'<div class="searchList_title">' +
								'<a href="javascript:goUserInfo(\''+data.skills[j].basicId+'\')">' +
	 								'<span class="searchList_pic">' +
	 									'<img src="'+data.skills[j].headPortrait+'" class="search_pic" />' +
	 								'</span>' +
 								'</a>' +
								'<span class="searchList_name">'+data.skills[j].userName+'</span>' +
								'<div class="searchList_city">' +
								'<span class="iconfont icon-zuobiao"></span>' +
								'<span>'+data.skills[j].cityName+'</span>' +
								'</div>' +
								'</div>' +
								'<div class="ctt_Piccontent">' +
								'<a href="javascript:getSkillInfo('+data.skills[j].id+',\''+data.skills[j].classValueF+'\')">' +
								'<div class="ctt_pic">' +
								'<img src=' + data.skills[j].image + ' class="search_pic">' +
								'<span class="ctt_consult"><span>' + data.skills[j].schoolName + '</span></span>' +
								'</div>' +
								'</a>' +
								'<span class="ctt_serviceTitle"><span>服务内容&nbsp;:&nbsp;'+data.skills[j].skillName+'</span></span>' +
								'</div>' +
								'</div>' +
								'<div class="ctt_Pricecontent">' +
								'<div class="Pricecontent_left">' +
								'<span>￥' + data.skills[j].skillPrice+ '元/次</span>' +
								'<span>[已售'+data.skills[j].sellNo+']</span>' +
								'</div>' +
								'<span class="Pricecontent_right" onclick="joinse(this)">加入已选</span>' +
								'<input type="hidden" value="'+data.skills[j].id+'" name="skillId">'+
								'<input type="hidden" value="'+data.skills[j].openId+'" name="openId_sel">'+
								'<input type="hidden" value="0" name="isPreferred">'+
								'<input type="hidden" value="'+data.skills[j].classValueF+'" name="enterType">'+
								'</div>' +
								'</li>';
                        }
                        if(pageNum > data.skills.length){
                            // 数据加载完
                            tab1LoadEnd = true;
                            // 锁定  ----不用锁定
                            me.lock();
                            // 无数据
                            me.noData();
//                        /    break;
                        }
                        
                      //  back();
                      //  var b= parseInt(pageNum)-parseInt(data.skills.length)
                       // alert(b==0)
                        if(data.skills.length == pageNum){
                        	$("#"+sid).val(page+1);
                        	$("#xkh_page_num").val(page);
                        	//$.cookie('xkh_page_num', page); 
                        }
                        // 为了测试，延迟1秒加载
                        setTimeout(function(){
                            $('.lists').eq(itemIndex).append(result);
                            // 每次数据加载完，必须重置
                            me.resetload();
                           // $.cookie('xkh_lock','off'); 
                        },1000);
            
                        if(lock == 'open'){
                        	backTwo();
                		}
//                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown){
  		          //通常情况下textStatus和errorThrown只有其中一个包含信息
  		          alert("失败！");
  		        me.resetload();
  		       }
            });
        }
    });
    
  /*  function goSkillInfo(id){
    	window.location.href="${path}/home/getSkillInfo.html?id="+id;
    	//window.location.href="../skill_detail/getskill_detail.html?skillId="+id+"&&classValue="+fathValue;
    }*/
    /**
     * 按照销售量进行排序，
     * 为了区分方便，将排序方式进行分开了。
     */
   /* function orderBySellNo(sid,fid){
    	var ordersellNo = jQuery("#ordersellNo").val();
             $.ajax({
             	type : "post",
 				url : "getUserInfoByClassIdV2.html",
 				dataType : "json",
 				async : false,
 				data : {currentPage:1,pageNum:pageNum,sid:sid,sellNo:ordersellNo,fid:fid},
                 success: function(data){
                     var result = '';
                         for(var j = 0; j < data.skills.length; j++){
                             result +='<li class="class_show">' +
									'<div class="class_bg"></div>' +
									'<a href="javascript:getSkillInfo('+data.skills[j].id+',\''+data.skills[j].classValueF+'\')">'+
									'<div class="pic_show">' +
									'<div class="class_pic">' +
									'<img src="'+data.skills[j].image+'?'+classSon_image+'" class="search_pic"/>' +
									'<div class="school_name">' +
									'  <span>' + data.skills[j].schoolName + '</span>' +
									'  </div>' +
									'<div class="busniess_name">' +
									'  <span>' + data.skills[j].skillName + '</span>' +
									'  </div>' +
									'</div>' +
									'<div class="person_pic"><img style="border-radius: 100%;" src="' + data.skills[j].headPortrait + '" class="search_pic"  /></div>' +
									'<p class="author">' + data.skills[j].userName + '</p>' +
									'<div class="diamond_content">'+
									' <span class="iconfont icon-zuobiao"></span>' +
									'<span class="class_city">' +  data.skills[j].cityName + '</span>' +
									'</div>' +
									'</a>'+
									'<div class="service_content">' +
									'<span class="class_service">擅长服务：</span>' +
									'<div class="class_service"><span>' + data.skills[j].className + '</span></div>' +
									
									'<a href="javascript:void(0)">'+
									'<div class="already" onclick="joinse(this)">加入已选</div>'+
									'<input type="hidden" value="'+data.skills[j].id+'" name="skillId">'+
									'<input type="hidden" value="'+data.skills[j].openId+'" name="openId_sel">'+
									'<input type="hidden" value="0" name="isPreferred">'+
									'<input type="hidden" value="'+data.skills[j].classValueF+'" name="enterType">'+
									'</a>'+
									'</div>' +
									'</div>' +
									'</div>' +
									'<div class="sell_content">' +
									'<div class="sell_pic"><img src="../xkh_version_2/img/class_icon.png" class="search_pic" /></div>' +
									'<div class="sell_number">' +
									'<span>[已售</span>' +
									'<span>' + data.skills[j].sellNo + ']</span>' +
									'</div>' +
									'<div class="sell_star">' +
									'<span class="iconfont icon-xingxing"></span>' +
									'<span class="iconfont icon-xingxing"></span>' +
									'<span class="iconfont icon-xingxing"></span>' +
									'<span class="iconfont icon-xingxing"></span>' +
									'<span class="iconfont icon-xingxing"></span>' +
									'</div>' +
									'<div class="diamond_content">'+
									'<div class="diamond">' +
									'<img src="../xkh_version_2/img/icon_diamond.png" class="search_pic"  />' +
									'</div>' +
									'<div class="num"><span>' + data.skills[j].priceStr + '</span>元/'+data.skills[j].companyStr+'</div>' +
									'</div>' +
									'</div>' +
									'</div>'+
									'</li>';
                             
                         }
                         
                         // 为了测试，延迟1秒加载
                         setTimeout(function(){
                            jQuery("#maxPrice").val("");
                     		jQuery("#minPrice").val("");
                     		jQuery("#cityName").val("");
                     		jQuery("#schoolLabel").val(""); 
                     		jQuery("#serviceType").val(""); 
                        	$('.lists').eq(itemIndex).html(result);
                             // 每次数据加载完，必须重置
//                        	  dropload.unlock();
//                              dropload.noData(false);
                             dropload.resetload();
                         },1000);
                      
                 },
                 error : function(XMLHttpRequest, textStatus, errorThrown){
   		          //通常情况下textStatus和errorThrown只有其中一个包含信息
   		          alert("失败！");
   		       }
             });
         // 加载菜单二的数据
    }*/
    
    function serchList(sid,orderp,fid){
             $.ajax({
             	type : "post",
 				url : "getUserInfoByClassIdV2.html",
 				dataType : "json",
 				async : false,
 				data : {currentPage:1,pageNum:pageNum,sid:sid,orderPrice:orderp,fid:fid},
                 success: function(data){
                     var result = '';
                         for(var j = 0; j < data.skills.length; j++){
                             result +='<li class="class_show">' +
									'<div class="class_bg"></div>' +
									'<a href="javascript:getSkillInfo('+data.skills[j].id+',\''+data.skills[j].classValueF+'\')">'+
									'<div class="pic_show">' +
									'<div class="class_pic">' +
									'<img src="'+data.skills[j].image+'?'+classSon_image+'" class="search_pic"/>' +
									'<div class="school_name">' +
									'  <span>' + data.skills[j].schoolName + '</span>' +
									'  </div>' +
									'<div class="busniess_name">' +
									'  <span>' + data.skills[j].skillName + '</span>' +
									'  </div>' +
									'</div>' +
									'<div class="person_pic"><img style="border-radius: 100%;" src="' + data.skills[j].headPortrait + '" class="search_pic"  /></div>' +
									'<p class="author">' + data.skills[j].userName + '</p>' +
									'<div class="diamond_content">'+
									' <span class="iconfont icon-zuobiao"></span>' +
									'<span class="class_city">' +  data.skills[j].cityName + '</span>' +
									'</div>' +
									'</a>'+
									'<div class="service_content">' +
									/*'<span class="class_service">擅长服务：</span>' +*/
									'<div class="class_service"><span>' + data.skills[j].className + '</span></div>' +
									'<a href="javascript:void(0)">'+
									'<div class="already" onclick="joinse(this)">加入已选</div>'+
									'<input type="hidden" value="'+data.skills[j].id+'" name="skillId">'+
									'<input type="hidden" value="'+data.skills[j].openId+'" name="openId_sel">'+
									'<input type="hidden" value="0" name="isPreferred">'+
									'<input type="hidden" value="'+data.skills[j].classValueF+'" name="enterType">'+
									'</a>'+
									'</div>' +
									'</div>' +
									'</div>' +
									'<div class="sell_content">' +
									'<div class="sell_pic"><img src="../xkh_version_2/img/class_icon.png" class="search_pic" /></div>' +
									'<div class="sell_number">' +
									'<span>[已售</span>' +
									'<span>' + data.skills[j].sellNo + ']</span>' +
									'</div>' +
									'<div class="sell_star">' +
									'<span class="iconfont icon-xingxing"></span>' +
									'<span class="iconfont icon-xingxing"></span>' +
									'<span class="iconfont icon-xingxing"></span>' +
									'<span class="iconfont icon-xingxing"></span>' +
									'<span class="iconfont icon-xingxing"></span>' +
									'</div>' +
									'<div class="diamond_content">'+
									'<div class="diamond">' +
									'<img src="../xkh_version_2/img/icon_diamond.png" class="search_pic"  />' +
									'</div>' +
									'<div class="num"><span>' + data.skills[j].priceStr + '</span>元/'+data.skills[j].companyStr+'</div>' +
									'</div>' +
									'</div>' +
									'</div>'+
									'</li>';
                             
                         }
                         
                         // 为了测试，延迟1秒加载
                         setTimeout(function(){
                        	jQuery("#maxPrice").val("");
                     		jQuery("#minPrice").val("");
                     		jQuery("#cityName").val("");
                     		jQuery("#schoolLabel").val(""); 
                     		jQuery("#serviceType").val(""); 
                        	$('.lists').eq(itemIndex).html(result);
                             // 每次数据加载完，必须重置
//                        	  dropload.unlock();
//                              dropload.noData(false);
                              dropload.resetload();
                         },1000);
                      
                 },
                 error : function(XMLHttpRequest, textStatus, errorThrown){
   		          //通常情况下textStatus和errorThrown只有其中一个包含信息
   		          alert("失败！");
   		       }
             });
         // 加载菜单二的数据
    }
    
    

	function backTwo() {
		
		$.cookie('xkh_lock', 'off');
		var str = window.location.href;
		str = str.substring(str.lastIndexOf("/") + 1);
		if ($.cookie(str)) {
			$("html,body").animate({
				scrollTop : $.cookie(str)
			}, 1000);

		} else {
		}
		$(window).scroll(function() {
			var str = window.location.href;
			str = str.substring(str.lastIndexOf("/") + 1);
			var top = $(document).scrollTop();
			$.cookie(str, top, {
				path : '/'
			});
			return $.cookie(str);

		})

	}
//	2.4版本新增
    $(".class_choose li").click(function() {
	 
		var index = $(this).index(".class_choose li");
		
		if($(this).index()!=0&&$(this).index()!=1){
			$(this).find("span:first-child").toggleClass("class_active");
		}else{
			$(this).find("span").eq(0).addClass("class_active");
			
		}
		$(this).siblings().find("span:first-child").removeClass("class_active");
	   if(index!=2){
		   $(".choose2 span:nth-of-type(2)").removeClass("filter_green").addClass("filter");
		   $(".class_filterContent").hide();
		   $(".mask").hide();
	   }
	   if($(".choose_sort span:first-child").hasClass("class_active")){
		   $(".choose_sort span:last-child").removeClass("icon_black").toggleClass("icon_down").toggleClass("icon_up")
	   }else{
		   $(".choose_sort span:last-child").removeClass("icon_up").removeClass("icon_down").addClass("icon_black") 
		   $(".class_sortContent").hide();
	   }
		
	})
   
  //点击排序出来的那一部分
    $(".choose_sort").click(function() {

    	$(">span:nth-of-type(2)",this).toggleClass("icon_down").toggleClass("icon_up");
    	if($(".choose_sort span:nth-of-type(2)").hasClass("icon_down")){
    		 $(".choose_sort span:nth-of-type(2)").removeClass("icon_down").addClass("icon_up");
    		$(".class_sortContent").show();
    	}else{
    		$(".choose_sort span:nth-of-type(2)").removeClass("icon_up").addClass("icon_down");
    		$(".class_sortContent").hide();
    	}
    	
    })
    
	
	//点击筛选出来的
	/*$('.choose2').on('click', function(e){
		var wh = $('.wraper').height();
		$('.mask').css('height', wh).show();
		
		$('.class_filterContent').addClass('moved');
	});*/

    
    
//	$(".choose1").click(function() {
//		if($(this).children("span").hasClass("class_active")){
//			$("#ordersellNo").val(""); 
//		}else{
//			$("#ordersellNo").val("desc"); 
//			$("#orderPrice").val(""); 
//		}
//		$('.lists').eq(itemIndex).html("");
//		$(this).find("span:first-child").toggleClass("class_active").parent().siblings().find("span").removeClass("class_active");
//		 $("#page").val(1);
//		 dropload.unlock();
//	     dropload.noData(false);
//	     dropload.resetload();
//})
    
    
    
	//2.1版本新换样式
	$(".choose2").on("click",function(e){
		$(">span:nth-of-type(2)",this).toggleClass('filter').toggleClass("filter_green")
		var wh = $(document).height();
		var h = $(".ctt_headerSearch").outerHeight(true) + $(".secondIndex_title").outerHeight(true) + $(".class_choose").outerHeight(true) + 9;
		$('.mask').css({ 'height': wh, "top": h + "px" }).toggle();
		$('.class_filterContent').css("top", h + "px").toggle();
	     	
	 })
	 
	
	
	 $('.mask').on('click', function(){
		$('.mask').hide();
		$('.class_filterContent').toggle();
	});
	
		
/*//		设置标题超宽隐藏问题
		var w=0;
       $(".class_title li").each(function(i,ele){
    	   w=w+$(ele).outerWidth(true)
       })
       var w1=w-$(".class_title li:nth-of-type(4)").outerWidth(true);
	       
        if(w1>$(".class_title").width()){
    		$(".class_title li:nth-of-type(3)").hide();
    	}
    	if(w>$(".class_title").width()){
    		$(".class_title li:nth-of-type(4)").hide();
    	}*/
	
	//点击右侧标签部分
	$(".label_content a").click(function(){
		$(this).css({"border":"1px solid #1d9243","border-radius":"5px"});
		var index=$(this).index();
		$(".green_cha").eq(index).css("display","block")
	});
	
	$(".serviceLabel div").click(function(){
		
		$(this).toggleClass("line_style").siblings().removeClass("line_style")
		var serviceStyle = this.dataset.name;
		$("#serviceType").val(serviceStyle);
	})
	
	$(".schoolStyle div").click(function(){
		$(this).toggleClass("line_style").siblings().removeClass("line_style")
		$("#schoolLabel").val(this.id);
	})
	
	function getBy(){
		var max = jQuery("#maxPrice").val();
		var min = jQuery("#minPrice").val();
		var cname = jQuery("#cityName").val();
		var schoolLabel = jQuery("#schoolLabel").val(); 
		var serviceType = jQuery("#serviceType").val(); 
	}
	
//	點擊重置
	$(".reset_box").click(function(){
		
		
		jQuery("#maxPrice").val("");
 		jQuery("#minPrice").val("");
 		jQuery("#cityName").val("");
 		jQuery("#schoolLabel").val(""); 
 		jQuery("#serviceType").val(""); 
		
	
		$("#minPrice").val("");
		$("#maxPrice").val("");
		$(".show-result").css("display","none");
		$(".result-tips").css("display","block");
		$(".service_style div").removeClass("line_style");
		$(".ctt_cha").hide();
		$(".ctt_jiantou").show();
	})
	
	function screen(sid,orderp,fid,max,min,cname,schoolLabel,serviceType){
//		alert("筛选进入");
		$.ajax({
         	type : "post",
				url : "getUserInfoByClassIdV2.html",
				dataType : "json",
				async : false,
				data : {currentPage:1,pageNum:pageNum,sid:sid,orderPrice:orderp,fid:fid,maxPrice:max,minPrice:min,cityName:cname,schoolLabel:schoolLabel,serviceType:serviceType},
				success: function(data){
					var result = '';
                     for(var j = 0; j < data.skills.length; j++){

                    	  result +='<li class="class_show">' +
							'<div class="class_bg"></div>' +
							'<a href="../skill_detail/getskill_detail.html?skillId='+data.skills[j].id+'&classValue ='+data.skills[j].classValueF+'">'+
							'<div class="pic_show">' +
							'<div class="class_pic">' +
							'<img src="'+data.skills[j].image+'?'+classSon_image+'" class="search_pic"/>' +
							'<div class="school_name">' +
							'  <span>' + data.skills[j].schoolName + '</span>' +
							'  </div>' +
							'<div class="busniess_name">' +
							'  <span>' + data.skills[j].skillName + '</span>' +
							'  </div>' +
							'</div>' +
							'<div class="person_pic"><img style="border-radius: 100%;" src="' + data.skills[j].headPortrait + '" class="search_pic"  /></div>' +
							'<p class="author">' + data.skills[j].userName + '</p>' +
							'<div class="diamond_content">'+
							' <span class="iconfont icon-zuobiao"></span>' +
							'<span class="class_city">' +  data.skills[j].cityName + '</span>' +
							'</div>'+
							'</a>'+
							'<div class="service_content">' +
							/*'<span class="class_service">擅长服务：</span>' +*/
							'<div class="class_service"><span>' + data.skills[j].className + '</span></div>' +
							'<a href="javascript:void(0)">'+
							'<div class="already" onclick="joinse(this)">加入已选</div>'+
							'<input type="hidden" value="'+data.skills[j].id+'" name="skillId">'+
							'<input type="hidden" value="'+data.skills[j].openId+'" name="openId_sel">'+
							'<input type="hidden" value="0" name="isPreferred">'+
							'<input type="hidden" value="'+data.skills[j].classValueF+'" name="enterType">'+
							'</a>'+
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="sell_content">' +
							'<div class="sell_pic"><img src="../xkh_version_2/img/class_icon.png" class="search_pic" /></div>' +
							'<div class="sell_number">' +
							'<span>[已售</span>' +
							'<span>' + data.skills[j].sellNo + ']</span>' +
							'</div>' +
							'<div class="sell_star">' +
							'<span class="iconfont icon-xingxing"></span>' +
							'<span class="iconfont icon-xingxing"></span>' +
							'<span class="iconfont icon-xingxing"></span>' +
							'<span class="iconfont icon-xingxing"></span>' +
							'<span class="iconfont icon-xingxing"></span>' +
							'</div>' +
							'<div class="diamond_content">'+
							'<div class="diamond">' +
							'<img src="../xkh_version_2/img/icon_diamond.png" class="search_pic"  />' +
							'</div>' +
							'<div class="num"><span>' + data.skills[j].priceStr + '</span>元/'+data.skills[j].companyStr+'</div>' +
							'</div>' +
							'</div>' +
							'</div>'+
							'</li>';

                         
                     }
                     // 为了测试，延迟1秒加载
                     setTimeout(function(){
                    	  $('.lists').eq(itemIndex).html(result);
                         // 每次数据加载完，必须重置
//                    	  dropload.unlock();
//                          dropload.noData(false);
//                          dropload.resetload();
                    	  dropload.resetload();
                     },1000);
                    
             },
             error : function(XMLHttpRequest, textStatus, errorThrown){
		          //通常情况下textStatus和errorThrown只有其中一个包含信息
		          alert("失败！");
		       }
         });
	}
//	点击确定
  /*$(".enure_box").click(function(){
	  $(".class_filterContent").toggleClass("moved");
  })*/
	 //选择所在地
	mui.init();
	var cityPicker = new mui.PopPicker({
		layer: 2
	});
	cityPicker.setData(citys);
	var showCityPickerButton = document.getElementById('chooseCity');
	showCityPickerButton.addEventListener('tap', function(event) {
		if($(event.target).hasClass('ctt_cha')){
			$("#chooseCity .result-tips").show();
			$("#chooseCity .show-result").hide();
			$(".ctt_jiantou").show();
			$(".ctt_cha").hide();
			return false;
		}
		
		cityPicker.show(function(items) {
			document.querySelector('#chooseCity .result-tips').style.display = "none";
			document.querySelector('#chooseCity .show-result').style.display = "block";
			document.querySelector('#chooseCity .show-result').innerText = items[1].text;
			$(".ctt_jiantou").hide();
			$(".ctt_cha").show();
			$("#cityName").val(items[1].text);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});
	}, false);
	mui.init();
	/*//选择学校类型
	var schoolpicker = new mui.PopPicker({
						});
	schoolpicker.setData(las);  
	var showCityPickerButton = document.getElementById('chooseSchool');
	showCityPickerButton.addEventListener('tap', function(event) {
		if($("div").hasClass("mui-active")){
		}else{
			schoolpicker.show(function(selectItems) {
			document.querySelector('#chooseSchool .result-tips').style.display = "none";
			document.querySelector('#chooseSchool .show-result').style.display = "block";
			document.querySelector('#chooseSchool .show-result').innerText = selectItems[0].text;		
			$("#schoolLabel").val(selectItems[0].value);
			});
		}
	}, false);*/
})


/**
 * add to choice
 * @param obj
 */
function joinse(obj){
	
		var gaoH=$(".class_list_head").outerHeight(true)+$(".class_title").outerHeight(true)+$(".class_choose").outerHeight(true)+$(".lists").outerHeight(true)+$(".dropload-down").outerHeight(true)+$(".foot-bar-tab").outerHeight(true);//获取当前页面高度	
		var skillId = $(obj).siblings("input[name='skillId']").val();
		var openId_sel = $(obj).siblings("input[name='openId_sel']").val();
		var isPreferred = $(obj).siblings("input[name='isPreferred']").val();
		var enterType = $(obj).siblings("input[name='enterType']").val();
		var information ={"skillId":skillId,"openId_sel":openId_sel,"isPreferred":isPreferred,"enterType":enterType};

		$.ajax({
			  url: baseP+"/skillOrder/joinSelectedSecond.html",
			  type: 'post',
			  dataType: 'json',
			  data:information,
			  success: function (data) {
				 if(data.ajax_status==true){
					 //增加加入已选
					
					 var html_str = "";
					  $.each(data.skill_list,function(i,v){  
						  html_str += '<li>'+
										'<span class="queen">'+
						  					'<img src="../xkh_version_2.3/xkh_version_2.3_1/img/queen.png" class="search_pic" />'+
						  				'</span>'+
										'<div class="popupWindow_message">'+
											'<span>'+v.skillName+'</span> <span>'+v.skillDepict+'</span>'+
										'</div>'+
										'<div class="popupWindow_number">'+
											'<span class="reduce" onclick="reduce(this)">-</span>'+
											'<span class="value">'+v.buyNum+'</span> '+
											'<span class="Add" onclick="add(this)" >+</span>'+
										'</div>'+
										'<input type="hidden" value='+v.seledId+' name="seledId">'+
									'</li>';
                     })  
                     $('#selectSkill').html(html_str);
					 $(".popupWindow_content").removeClass("add_Move").addClass("add_move");
					 $("body,html").css({
							"overflow": "hidden"
						});
					//5秒弹窗自动关闭
						timer=setTimeout(function(){
							$(".popupWindow_content").removeClass("add_move").addClass("add_Move");
							$("body,html").css({
								"overflow": "scroll"
							});
						},5000)
					 $(".poopupWindow_box li:gt(0)").hide();
					if($(".poopupWindow_box li").length<=1){
							$(".popupWindow_more").removeClass("popupWindow_up");
						}
					//超过16个字省略号显示
					 var arr=[];
					 for(var i=0;i<$(".popupWindow_content li").length;i++){
					 	console.log($(".popupWindow_content li").length)
					 	
					     var message=$(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html()
					     arr.push(message);
					     var length=arr[i].length;
					 	if(length>16){
					 		$(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html($(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html().substring(0,16)+"...")
					 	}

					 }
				 }else{
					 alert("加入已选失败");
				 } 
			  },
			  fail: function (err, status) {
				  alert("加入已选失败");
			  }
			})
		
	}

//06-19加入已选出来的
var popupWindow_height = $(".popupWindow_content").height();
$(".popupWindow_content").css({
	"WebkitTransform": "translateY(" + popupWindow_height + "px)",
	"position": "fixed",
	"left": 0,
	"bottom": 0,
	"zIndex": 1000,
	"background": "#fff"
})

/*function joinse(){
	alert("*************************");
	
	$(".popupWindow_content").removeClass("add_Move").addClass("add_move")
}*/

$(".popupWindow_title .popupWindow_close").click(function() {
	$(".popupWindow_content").removeClass("add_move").addClass("add_Move")
})
$(".popupWindow_more").click(function() {
	clearInterval(timer)
	$(this).toggleClass("popupWindow_up").toggleClass("popupWindow_down");
	if($(this).hasClass("popupWindow_down")) {
      $(".poopupWindow_box li").show();
		

	} else {
		$(".poopupWindow_box li:gt(0)").hide();
	}
})
/*$('.reduce').on('click', function() {
	alert("点击数量增加");
	var html = $(this).next('.value')
	var value = html.text()
	value = parseInt(value)
	value -= 1
	html.text(value)
	if(value<1){
		value=1;
		html.text(1)
	}
})*/
function reduce(obj){
	
	var seledId = $(obj).parent().siblings('input').val();
	var html = $(obj).siblings('.value');
	var value = html.text()
	value = parseInt(value)
	value -= 1
	
	if(value<1){
		value=1;
		html.text(1)
	}
	$.ajax({
		  url: baseP+"/skillOrder/modifyBuyNum.html",
		  type: 'post',
		  dataType: 'json',
		  data:{seledId:seledId,buyNum:value},
		  success: function (data) {
			 if(data==true){
				 html.text(value)
			 }else{
				 alert("修改失败");
			 } 
		  },
		  fail: function (err, status) {
			  alert("修改失败");
		  }
		})
	
	
	
}

function add(obj){
	var seledId = $(obj).parent().siblings('input').val();;
	var html = $(obj).siblings('.value');
	var value = html.text()
	value = parseInt(value)
	value += 1
	$.ajax({
		  url: baseP+"/skillOrder/modifyBuyNum.html",
		  type: 'post',
		  dataType: 'json',
		  data:{seledId:seledId,buyNum:value},
		  success: function (data) {
			 if(data==true){
				 html.text(value)
			 }else{
				 alert("修改失败");
			 } 
		  },
		  fail: function (err, status) {
			  alert("修改失败");
		  }
		})
}


/**
 * 关于已选问题中未读信息的标注
 */
$(function () {
    (function isReadSelect() {
        $.ajax({
            url: path+"/skillOrder/isReadSelect.html",
            data: {"timed": new Date().getTime()},
            dataType: "json",
            timeout: 5000,
            error: function (XMLHttpRequest, errorThrown) {
            	$(".redCar_pic").hide();
            	isReadSelect(); // 递归调用
            },
            success: function (data) {
                if (data == true) {// 请求成功
                	$(".redCar_pic").show();
                }
                if(data == false){
                	$(".redCar_pic").hide();
                }
                isReadSelect();
            }
        });
    })();
    
});


