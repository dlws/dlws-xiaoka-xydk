function load() {
	var length = $(".Introduce").val().length;
	var html = $(".Introduce").val();

	if(length > 499) {
		$(".Introduce").val(html.substring(0, 500))

		$("#span Span").html(500);

	} else {
		$("#span Span").html(length)
	}

}

function load2() {
	var length = $(".Introduce2").val().length;
	var html = $(".Introduce2").val();

	if(length > 499) {
		$(".Introduce2").val(html.substring(0, 500))
		$("#span2 Span").html(500);
	} else {
		$("#span2 Span").html(length)
	}

}
//验证手机号码
//当文本框失去焦点的时候
$(".phone").blur(function() {
		var val1 = $(".phone").val()
		var re = /^1[3|4|5|7|8]\d{9}$/;
		if(!re.test(val1)) {
			$(".warn_tel").html("输入手机格式有误，请重新输入").css("display", "block")

		}

		if(val1 == "") {
			$(".warn_tel").html("请输入手机号").css("display", "block");
		}

	})
	//当文本框重新获得焦点的时候
$(".phone").focus(function() {
	var val1 = $(".phone").val()
	var re = /^1[3|4|5|7|8]\d{9}$/;
	if(!re.test(val1)) {
		$(".warn_tel").html("").css("display", "none")
	}

	if(val1 == "") {
		$(".warn_tel").html("").css("display", "none");
	}
})

////时间选择
var tfTime="";
	(function($) {
				$.init();
			     var now=new Date();
			    var obj1 = opt()
			    var time = document.getElementById("time");
			    time.innerText = obj1.beginYear + "-" +(obj1.beginMonth >=10 ? obj1.beginMonth : "0" + obj1.beginMonth ) + "-" + obj1.beginDay +"  "+obj1.beginHour+":"+obj1.beginMinutes;             
			    var obj = getTime([obj1.beginYear,obj1.beginMonth,obj1.beginDay])
                var later48 = opt( (obj1.beginYear + "-" +(obj.month >=10 ? obj.month : "0" + obj.month ) + "-" + obj.day +"  "+obj.hour+":"+obj.second),obj1.beginYear,obj.month,obj.day,obj.hour,obj.second)
		//console.log(later48)
        time.setAttribute('data-options',JSON.stringify(later48));
			    var result = $('#time')[0];
				var btns = $('.Btn');
			btns.each(function(i, btn) {
					btn.addEventListener('tap', function() {
						var optionsJson = this.getAttribute('data-options') || '{}';
						var options = JSON.parse(optionsJson);
						var id = this.getAttribute('id');
						
						 var today = new Date();
						 var picker = new $.DtPicker(options);
						picker.show(function(rs) {
							result.innerText = rs.text;
							tfTime=rs.text;
					changeTime(tfTime)
							picker.dispose();
						});
						
					}, false);
				})
			})(mui);
	
	
		 function changeTime(tfTime){
			$("#time").val(tfTime);
		 }
		 function getTime(arr) {
				//arr 的格式是["2017","06","29"]

				// 是不是闰年
				var year = 0;
				// 记录加48小时后的日期
				var day = 0;
				// 记录当前的月份
				var month = 0;
				// 一月是 31 天的数组
				var day31 = [1,3,5,7,8,10,12];
				// 一月30天的数组
				var day30 = [4,6,9,11];
				var hour=new Date().getHours();
				var second=new Date().getMinutes();
				// 判断是不是闰年
			    function isLeapYear(year) {
			    	return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
			    }
			    // 闰年二月要加1
			    year = isLeapYear(parseInt(arr[0])) ? 1 : 0;

				if($.inArray(parseInt(arr[1]),day31) != -1){
					day = parseInt(arr[2]) + 2 > 31 ? (parseInt(arr[2])+2 - 31) : parseInt(arr[2]) + 2;
			        month = parseInt(arr[2]) + 2 > 31 ? parseInt(arr[1]) +1 : parseInt(arr[1])
			    }else if($.inArray(parseInt(arr[1]),day30) != -1){
					day = parseInt(arr[2]) + 2 > 30 ? (parseInt(arr[2])+2 - 30) : parseInt(arr[2]) + 2;
			        month = parseInt(arr[2]) + 2 > 30 ? parseInt(arr[1]) +1 : parseInt(arr[1])
				}else{
			        if(year){
			        	day = parseInt(arr[2]) + 2 + year > 29 ? parseInt(arr[2]) + 2 + year - 29 : parseInt(arr[2]) +2
			            month = parseInt(arr[2]) + 2 + year > 29 ? parseInt(arr[1]) +1 : parseInt(arr[1])
					}else{
			            day = parseInt(arr[2]) + 2 + year > 28 ? parseInt(arr[2]) + 2 + year - 28 : parseInt(arr[2]) +2
			            month = parseInt(arr[2]) + 2 > 28 ? parseInt(arr[1]) +1 : parseInt(arr[1])
					}
				}

				// 将日给处理成需要的格式
			    day = day >= 10 ? day : "0" + day
			    		return {month:month,day:day,hour:hour,second:second};
			}

			// 格式化时间选择器的配置
			function opt(value,year,mouth,day,hour,second) {
				
			    var obj = {
			        type:"datetime"
			    }
			    
			    if(arguments[1]){
			    	
			        obj.value = value;
					obj.beginYear=year;
					obj.beginMonth=parseInt(mouth);
			        obj.beginDay=parseInt(day);
			        obj.beginHours=parseInt(hour);
					obj.beginMinutes=parseInt(second);
			       
				}else{
			    	var now = new Date();
			    	
			        obj.beginYear=now.getFullYear();
			        obj.beginMonth=now.getMonth()+1;;
			        obj.beginDay=now.getDate() >= 10 ? now.getDate() : "0" + now.getDate();
			        obj.beginHour=now.getHours() >= 10 ? now.getHours() : "0" + now.getHours();
				       obj.beginMinutes=now.getMinutes() >= 10 ? now.getMinutes() : "0" + now.getMinutes();
				        obj.value = obj.beginYear + "-" + obj.beginMonth + "-" + obj.beginDay+"  "+ obj.beginHour+":"+ obj.beginMinutes
			        
				}


			    return obj;
			}


//点击提交订单
var h = $("body").height();
var popupHeight = $(".consult_popup").height() / 2;
$(".consult_popup").css("margin-top", -popupHeight);

$(".popup_mask").click(function() {
	$(".popup_mask").hide();
	$(".consult_popup").hide();
})
$(".close").click(function() {
	$(this).parent().hide();
	$(".popup_mask").hide();
})

