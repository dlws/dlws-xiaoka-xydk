$(".choose_webservice").click(function() {
		$(".webservice_box1").hide();
		$(".webservice_box2").show();
	})
$(".webservice_styleContent li").click(function() {
	var index = $(this).index();
	$("span", this).addClass("webservice_color");
	$(this).siblings().find("span").removeClass("webservice_color");
	var id = $(this).find("input").val();
	$.ajax({
		url : 'getSecondCategory.html',
		data : {
			"id" : id
		},
		type : 'post',
		cache : false,
		dataType : 'json',
		success : function(data) {
			$("#secondCategory").html("");
			if(data != null && data.length > 0 ){
				$(".webservice_styleContent1").css("display", "block");
				var html = "<p>二级服务</p>";
				for(var i = 0; i < data.length;i++){
					html += "<li><span>"+data[i].className+"</span></li>";
				}
				$("#secondCategory").html(html);
			}
		},
		error : function() {
			alert("异常！");
		}
	});
});
//	$(document).on("click",".webservice_styleContent1 li",function(){
//		var index = $(this).index();
//		$("span", this).addClass("webservice_color");
//		$(this).siblings().find("span").removeClass("webservice_color");
//		style = $(this).text();
//		$(".webservice_form").click(function() {
//			$(this).css("background", "#1D9243");
//			$("button", this).css("background", "#1D9243");
//			setTimeout(function() {
//				$(".webservice_box2").hide();
//				$(".webservice_box1").show();
//				$(".publish_reverse").html(style);
//				//					$(".popup_mask").hide()
//			}, 1000)
//		})
//	});
//选择时间
(function($) {
	$.init();
	var btns = $('.btn');
	btns.each(function(i, btn) {
		btn.addEventListener('tap', function() {
			var optionsJson = this.getAttribute('data-options') || '{}';
			var options = JSON.parse(optionsJson);
			var id = this.getAttribute('id');
			var c = document.getElementById(id)
			var picker = new $.DtPicker(options);
			picker.show(function(rs) {
				c.innerText = rs.text;
				picker.dispose();
			});
		}, false);
	});
})(mui);
//投入量级
var putStyle = new mui.PopPicker({
	layer: 1
});
putStyle.setData([{
	value: 0,
	text: "1000"
}, {
	value: 1,
	text: "2000"
}, {
	value: 2,
	text: "3000"
}]);
var showplaceStyleButton = document.getElementById('publishDemand_price');
showplaceStyleButton.addEventListener('tap', function(event) {
	putStyle.show(function(items) {
		document.querySelector('#publishDemand_price .result-tips').style.display = "none";
		document.querySelector('#publishDemand_price .show-result').style.display = "block";
		document.querySelector('#publishDemand_price .show-result').innerText = items[0].text;
		//返回 false 可以阻止选择框的关闭
		//return false;
		putStyle.hide(function(items) {
			if(document.querySelector('#publishDemand_price .show-result').innerText = "") {
				$(".mui-backdrop").css("display", "none")
			}

		})
	});

}, false);

function load() {
	var length = $(".Introduce").val().length;
	var html = $(".Introduce").val();

	if(length > 199) {
		$(".Introduce").val(html.substring(0, 200))

		$("#span Span").html(200);

	} else {
		$("#span Span").html(length)
	}

}

function load2() {
	var length = $(".Introduce2").val().length;
	var html = $(".Introduce2").val();

	if(length > 499) {
		$(".Introduce2").val(html.substring(0, 500))
		$("#span2 Span").html(500);
	} else {
		$("#span2 Span").html(length)
	}

}
//验证手机号码
//当文本框失去焦点的时候
$(".phone").blur(function() {
		var val1 = $(".phone").val()
		var re = /^1[3|4|5|7|8]\d{9}$/;
		if(!re.test(val1)) {
			$(".warn_tel").html("输入手机格式有误，请重新输入").css("display", "block")

		}

		if(val1 == "") {
			$(".warn_tel").html("请输入手机号").css("display", "block");
		}

	})
	//当文本框重新获得焦点的时候
$(".phone").focus(function() {
		var val1 = $(".phone").val()
		var re = /^1[3|4|5|7|8]\d{9}$/;
		if(!re.test(val1)) {
			$(".warn_tel").html("").css("display", "none")
		}

		if(val1 == "") {
			$(".warn_tel").html("").css("display", "none");
		}
	})

//选择全选时候左边
$(".left_choose").click(function() {
		if($(this).hasClass("publishConsult_border")) {
			$(this).removeClass("publishConsult_border").addClass("publishConsult_check");
			$('.box_left ul .publishConsult_Left').removeClass("publishConsult_border").addClass("publishConsult_check");
		    $(".box_left .school_name").removeClass("color").addClass("choose_color");
		    //当点击左边全选以后，右边全选出现
		   $(".school_box ul").show();
		   $(".right_choose").removeClass("publishConsult_border").addClass("publishConsult_check");
		   $(".school_box .publishConsult_left").addClass("publishConsult_check").removeClass("publishConsult_border");
		   $(".box_right .school_name").removeClass("color").addClass("choose_color");
		} else {
			$(this).removeClass("publishConsult_check").addClass("publishConsult_border")
			$('.box_left ul .publishConsult_Left').addClass("publishConsult_border").removeClass("publishConsult_check");
            $(".box_left .school_name").addClass("color").removeClass("choose_color");
            //取消左边全选，右边出现默认的985/211院校
           $(".school_box ul:gt(0)").hide();
           $(".right_choose").removeClass("publishConsult_check").addClass("publishConsult_border");
           $(".school_box .publishConsult_left").removeClass("publishConsult_check").addClass("publishConsult_border");
		   $(".box_right .school_name").removeClass("choose_color").addClass("color")
		}

	})
//选择全选时候右边的
$(".right_choose").click(function(){
	if($(this).hasClass("publishConsult_border")) {
			$(this).removeClass("publishConsult_border").addClass("publishConsult_check");
			$('.school_box .publishConsult_left').removeClass("publishConsult_border").addClass("publishConsult_check");
		    $(".school_box .school_name").removeClass("color").addClass("choose_color");
		    $(".school_box ul").removeClass("none")
		} else {
			$(this).removeClass("publishConsult_check").addClass("publishConsult_border")
			$('.school_box .publishConsult_left').addClass("publishConsult_border").removeClass("publishConsult_check");
            $(".school_box .school_name").addClass("color").removeClass("choose_color");
            $(".school_box ul:gt(0)").addClass("none")
		}
})
//a为左边全选以下的图标，b为左边这些除了全选以外的图标长度，c为除了全选以外的图标，d为全选所在的图标，f为除全选以外的图标，e为左边每个类别对应的内容
function touch(a,b,c,d,f,e){
	var str = d.split(" ")[1];
	$(a).click(function() {
	var index=$(this).parent().index();
	//console.log($($(this).parent().siblings('li')[0]).find('.publishConsult_left'));
	if($(this).hasClass("publishConsult_border")) {
		$(this).removeClass("publishConsult_border").addClass("publishConsult_check");
		$(this).siblings(".school_name").removeClass("color").addClass("choose_color");
		$(e).eq(index).show();
		$(e).eq(index).find("li span:first-child").removeClass("publishConsult_border").addClass("publishConsult_check")

	    $(e).eq(index).find("li span:last-child").removeClass("color").addClass("choose_color")
	} else {
		$(this).addClass("publishConsult_border").removeClass("publishConsult_check");
		$(this).siblings(".school_name").removeClass("choose_color").addClass("color");
		$(e).eq(index).hide();
		$(e).eq(index).find("li span:first-child").addClass("publishConsult_border").removeClass("publishConsult_check")
        $(e).eq(index).find("li span:last-child").addClass("color").removeClass("choose_color")
	}
	if(str === '.right_choose'){
		b = $(this).parent().parent().find('li').length;
		c = $(this).parent().parent().find('li');
	}
	for(var i = 0; i < b; i++) {
		if(str === '.left_choose'){
			if(!c.eq(i).find(f).hasClass('publishConsult_check')) {
				$(d).removeClass("publishConsult_check").addClass('publishConsult_border');
				break;
			}else {
				$(d).addClass("publishConsult_check").removeClass('publishConsult_border');
			}
		}else{
			if(!$(c[i]).find(f).hasClass('publishConsult_check')) {
				$(d).removeClass("publishConsult_check").addClass('publishConsult_border');
				break;
			}else {
				$(d).addClass("publishConsult_check").removeClass('publishConsult_border');
			}
		}
	}
})
}
////左选
//touch(".publishConsult_Left",$("#city li").length,$(".school_Box .box_left ul li"),".left_choose",".publishConsult_Left",".school_box ul");
////右选
//touch(".publishConsult_left",$(".school_box ul li").length,$(".school_Box .box_right ul li"),".right_choose",".publishConsult_left")



//点击选择活动院校
$(".choose_school").click(function(){
	var mask_height=$(document).height();
	$(".popup_mask").css({"height":mask_height,"display":"block"})
	$(".school_Content").addClass("add_move");
	////左选
    touch(".school_Content .publishConsult_Left",$(".school_Content .box_left ul li").length,$(".school_Content .school_Box .box_left ul li"),".school_Content .left_choose",".publishConsult_Left",".school_Content .school_box ul");
////右选
    touch(".school_Content .publishConsult_left",$(".school_Content .school_box ul li").length,$(".school_Content .school_Box .box_right ul li"),".school_Content .right_choose",".publishConsult_left")


})
$(".school_header span:first-child").click(function(){
	$(".popup_mask").hide();
	$(".school_Content").removeClass("add_move").addClass("add_Move");
	$(".city_content").removeClass("add_move").addClass("add_Move");
})


var schoolName=""
var cityName=""
//城市选择
sure(".ensure_city",".city_content .school_box li .publishConsult_left",cityName,".city_content",".school_city");
//学校选择
sure(".ensure_school",".school_Content .school_box li .publishConsult_left",schoolName,".school_Content",".choose_school")
//j为点击确定按钮，k为二级选项下的所有左边图标，l为schoolName或者cityName，m为整个弹窗类名，n为选择活动城市或者活动院校
function sure(j,k,l,m,n){
	$(j).click(function(){
	$(k).each(function(){
		if($(this).hasClass("publishConsult_check")){
			l+=$(this).siblings(".school_name").html()+"&nbsp;";
		}
	})
	$(".popup_mask").hide();
	$(m).removeClass("add_move").addClass("add_Move")
	$(n).html(l);

})
}
//选择活动城市
	$(".school_city").click(function(){
	var mask_height=$(document).height();
	$(".popup_mask").css({"height":mask_height,"display":"block"})
	$(".city_content").addClass("add_move");
    //左选
touch(".city_content .publishConsult_Left",$(".city_content .box_left ul li").length,$(".city_content .school_Box .box_left ul li"),".city_content .left_choose",".publishConsult_Left",".city_content .school_box ul");

//右选
touch(".city_content .publishConsult_left",$(".city_content .school_box ul li").length,$(".city_content .school_Box .box_right ul li"),".city_content .right_choose",".publishConsult_left")
})

//选择资源类型
var resourcelD = new mui.PopPicker({
	layer: 2
});
 resourcelD.setData(dateSource);
var showresourceStyleButton = document.getElementById('resourceStyle');
showresourceStyleButton.addEventListener('tap', function(event) {
	resourcelD.show(function(items) {
		document.querySelector('#resourceStyle .result-tips').style.display = "none";
		document.querySelector('#resourceStyle .show-result').style.display = "block";
		document.querySelector('#resourceStyle .show-result').innerText = items[1].text;
		$("#resourceType").val(items[1].text);
		//返回 false 可以阻止选择框的关闭
		//return false;
		resourcelD.hide(function(items) {
			if(document.querySelector('#resourceStyle .show-result').innerText = "") {
				$(".mui-backdrop").css("display", "none")
			}

		})
	});

}, false);



