//服务介绍
function load() {
	var length = $(".introduce").val().length;
	var html = $(".introduce").val();

	if(length > 199) {
		$(".introduce").val(html.substring(0, 200))

		$("#span Span").html(200);

	} else {
		$("#span Span").html(length)
	}

}


//微信上传 全局变量
var wxSelf;
var localArr = new Array();
var localHeadArr = new Array();//头像
var localDailyArr = new Array();//日常推文的组合  
var localActivityArr = new Array();//存放活动的图片
var serverArr = new Array();

var serverHeadArr = new Array();//头像
var serverDailyArr = new Array();//日常推文的组合  
var serverActivityArr = new Array();//存放活动的图片

/*001 选择图片   日常推文选择图片*/
function uploadImage(flag,num) {
	var clientUrl = window.location.href;
	var reqPath = '<%=contextPath%>/skillUser/getJSConfig.html';
	//请求后台，获取jssdk支付所需的参数
	$.ajax({
		type : 'post',
		url : reqPath,
		dataType : 'json',
		data : {
			"clientUrl" : clientUrl
			//当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
		},
		cache : false,
		error : function() {
			alert("系统错误，请稍后重试");
			return false;
		},
		success : function(data) {
			//微信支付功能只有微信客户端版本大于等于5.0的才能调用
			var return_date = eval(data);
			/* alert(return_date ); */
			if (parseInt(data[0].agent) < 5) {
				alert("您的微信版本低于5.0无法使用微信支付");
				return;
			}
			//JSSDK支付所需的配置参数，首先会检查signature是否合法。
			wx.config({
				debug : false, //开启debug模式，测试的时候会有alert提示
				appId : return_date[0].appId, //公众平台中-开发者中心-appid
				timestamp : return_date[0].config_timestamp, //时间戳
				nonceStr : return_date[0].config_nonceStr, //随机字符串,不长于32位
				signature : return_date[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
				jsApiList : [ 'chooseImage','uploadImage','downloadImage' ]
			});
			//上方的config检测通过后，会执行ready方法
			wx.ready(function() {
				wxSelf=wx;
				wx.chooseImage({
					count: num, // 默认9
					sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
					sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
					success: function (res) {
							localActivityArr=appandArray(localActivityArr,res.localIds);
							if(localActivityArr.length > 0){
								var result = "";
						        for(var j = 0; j < res.localIds.length; j++){
						        	result += "<div id='xkh_activity_"+j+"' class='add_picture'>"+
											"<img src='"+res.localIds[j]+"' class='search_pic' />"+
												"<span onclick='removeImgActiv("+j+","+res.localIds[j]+")' class='iconfont icon-shanchu' ></span>"+
												"<span class='cover'></span>"+
											"</div>";
						        }
						        var imgNum = $("#picture_box_activity img").length;
						        var picNum =  parseInt(imgNum) + parseInt(localDailyArr.length);
						        if(picNum>9){
						        	alert("请上传1-9张图片！");
						        }else{
						        	$("#picture_box_activity").append(result);
						        } 
							}
					}
				 });

				});
				 wx.error(function(res) {
					alert(res.errMsg);
				});
			}
	});
}		


/*活动图片 点击提交后上传到微信*/
/*使用递归的方式进行上传图片*/
function uploadActivityImageSelf() {
    if (localActivityArr.length == 0) {
    	var serverStr="";
    	if(0 == serverActivityArr.length ){
    		$("#skillPub").submit();
    	}else{
            for(var j=0;j<serverActivityArr.length;j++){
            	serverStr+=serverActivityArr[j]+";" ;
            }
            $.ajax({
        		type : "post",
        		url : "<%=contextPath%>/skillUser/downloadImageSelf.html",
        		dataType : "json",
        		data : {serverId:serverStr},
        		success : function(data) {//success后添加heidden的input用于保存上传服务器返回地址(删除图片,添加img,添加保存地址,保存时获取值)
        			
        			//alert("+++++++++data++++++:"+data);	
        			var imageList = data;
        			//var pathList = data.
        			
        			var imgPath="";
        		//	alert("+++++++++返回集合的长度++++++:"+imageList.length);
					for(var f=0;f<imageList.length;f++){
				//		alert("+++++++++++++++:"+imageList[f]);
						var imgPathTemp = imageList[f];
						imgPath += imgPathTemp+",";
					}
					
					var u = $("#activtyUrl").val()+imgPath;
					$("#activtyUrl").val(u)
					
					var imgNum = $("#picture_box_activity img").length;
					//alert("imgNum:"+imgNum);
					if(imgNum<1||imgNum>9){
						alert("图片个数为1-9");
						$(".webservice_mask").addClass("mask_none");
						return;
					}
					
        			$("#skillPub").submit();
					$(".webservice_mask").removeClass("mask_none");
        		}
        	});	
    	}
    }
    var localId = localActivityArr[0];
    //tmd 一定要加     解决IOS无法上传的坑 
    if (localId.indexOf("wxlocalresource") != -1) {
        localId = localId.replace("wxlocalresource", "wxLocalResource");
    }
    wxSelf.uploadImage({
        localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
        isShowProgressTips: 0, // 默认为1，显示进度提示    0不显示进度条
        success: function (res) {
           // serverIds.push(res.serverId); // 返回图片的服务器端ID
           var serverId = res.serverId; // 返回图片的服务器端ID
           serverActivityArr.push(serverId);
            localActivityArr.shift();
            uploadActivityImageSelf();
            serverStr+=serverId+";" ;
        },
        fail: function (res) {
            alert("上传失败，请重新上传！");
        }
    });
}

function appandArray(a,b){

	for(var i=0;i<b.length;i++){
		 a.push(b[i]);
	}
	return a;
}



function removeImgActiv(imageId,val){
	$("#xkh_activity_"+imageId).remove();
	localActivityArr.remove(val);
}

function removeImgDairy(imageId,val){
	$("#xkh_daily_"+imageId).remove();
	localDailyArr.remove(val);
}

//点击删除推文图片
function delImgDairyTUrl(imgId,val,obj){
	if(confirm("确定删除?")){
			//表示修改需要数据库删除
			$.ajax({
				type : "post",
				url : "<%=basePath%>highSchoolSkill/delImage.html",
				dataType : "json",
				data : {imgeId:imgId,imageUrl:val},
				success : function(data) {
					if(data.flag){
						$("#dairyTweetUrl").val(data.typeValue);
						obj.parentElement.remove();
					}
				}
			});
  }
}

//点击删除活动图片
function delImgActivtyUrl(imgId,val,obj){
	if(confirm("确定删除?")){
			//表示修改需要数据库删除
			$.ajax({
				type : "post",
				url : "<%=basePath%>highSchoolSkill/delImage.html",
				dataType : "json",
				data : {imgeId:imgId,imageUrl:val},
				success : function(data) {
					if(data.flag){
						$("#activtyUrl").val(data.typeValue);
						obj.parentElement.remove();
					}
				}
			});
  }
}