function orderStatus(status){
	var resultHtml = "";
	if(status=='1'){
		resultHtml +=" 等待买家付款";
	}else if(status=='2'){
		resultHtml +=" 等待卖家接单";
	}else if(status=='3'){
		resultHtml +=" 卖家已服务";
	}else if(status=='0'){
		resultHtml +=" 卖家已接单";
	}else if(status=='6'){
		resultHtml +=" 退款申请已提交";
	}else if(status=='11'){
		resultHtml +=" 卖家拒绝了您的订单，退款将在一个工作日内退回您的账户";
	}else if(status=='9'){
		resultHtml +=" 您已取消订单，退款将在一个工作日内退回您的账户";
	}else if(status=='7'){
		resultHtml +=" 您已取消订单，退款将在一个工作日内退回您的账户";
	}else if(status=='4'){
		resultHtml +=" 服务已完成";
	}else if(status=='10'){
		resultHtml +=" 您已取消订单，服务已关闭";
	}else if(status=='12'){
		resultHtml +=" 卖家拒绝退款，请重新处理";
	}else if(status=='8'){
		resultHtml +=" 已退款";
	}else if(status=='5'){
		resultHtml +=" 已超时";
	}else if(status=='12'){
		resultHtml +=" 卖家拒绝退款，请重新处理";
	}
}