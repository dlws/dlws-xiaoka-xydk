	//门店描述
	function load() {
	var length = $(".introduce").val().length;
	var html = $(".introduce").val();

	if(length > 199) {
		$(".introduce").val(html.substring(0, 200))

		$("#span Span").html(200);

	} else {
		$("#span Span").html(length)
	}

}
//	ios系统下字数检测
	$(".introduce").on("input propertychange", function() {    
		var length = $(".introduce").val().length;
		var html = $(".introduce").val();

		if(length > 199) {
			$(".introduce").val(html.substring(0, 200))

			$("#span Span").html(200);

		} else {
			$("#span Span").html(length)
		}
	    });  
	
	//选择时间
	(function($) {
		
		$.init();
		var btns = $('.btn');
		btns.each(function(i, btn) {
			btn.addEventListener('tap', function() {
				var optionsJson = this.getAttribute('data-options') || '{}';
				var options = JSON.parse(optionsJson);
				var id = this.getAttribute('id');
				var c = document.getElementById(id)
				var picker = new $.DtPicker(options);
				picker.show(function(rs) {
					c.innerText = rs.text
					if(i==0){
						 document.getElementById("begin").value = rs.text;
						//$("#begin").val(rs.text);
					}else{
						 document.getElementById("end").value = rs.text;
						//$("#end").val(rs.text);
					}
					picker.dispose();
				});
			}, false);
		});

	})(mui);
	//选择定金额度
	var depositLimitlD = new mui.PopPicker({
		layer: 1
	});
	depositLimitlD.setData([{
		value: 0.1,
		text: "10%"
	}, {
		value: 0.2,
		text: "20%"
	}, {
		value: 0.3,
		text: "30%"
	}, {
		value: 0.4,
		text: "40%"
	}, {
		value: 0.5,
		text: "50%"
	}, {
		value: 0.6,
		text: "60%"
	}, {
		value: 0.7,
		text: "70%"
	}, {
		value: 0.8,
		text: "80%"
	}, {
		value: 0.9,
		text: "90%"
	}, {
		value: 1,
		text: "100%"
	}
	]);
	var showdepositLimitButton = document.getElementById('depositLimit');
	showdepositLimitButton.addEventListener('tap', function(event) {
		depositLimitlD.show(function(items) {
			document.querySelector('#depositLimit .result-tips').style.display = "none";
			document.querySelector('#depositLimit .show-result').style.display = "block";
			document.querySelector('#depositLimit .show-result').innerText = items[0].text;
			//将当前定金比例返回给 input 
			$("#subscribeScale").val(items[0].value);
			//返回 false 可以阻止选择框的关闭
			//return false;
		});

	}, false);
	//定金说明
	$(".depositLimit_pic").click(function(){
		$(".depositmessage_box,.depositLimit_bg").show();
		$(".depositLimit_introduce").css("margin","10px 3px 10px 0")
	})
	$(".depositlLimit_up").click(function(){
		$(".depositmessage_box,.depositLimit_bg").hide();
		$(".depositLimit_introduce").css("margin","10px 3px 20px 0")
	})