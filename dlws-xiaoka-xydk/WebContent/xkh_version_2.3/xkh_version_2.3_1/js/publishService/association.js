

//介绍
function load() {
	var length = $(".introduce").val().length;
	var html = $(".introduce").val();

	if(length > 199) {
		$(".introduce").val(html.substring(0, 200))

		$("#span span").html(200);

	} else {
		$("#span span").html(length)
	}

}
//ios系统下字数检测
$(".introduce").on("input propertychange", function() {    
	var length = $(".introduce").val().length;
	var html = $(".introduce").val();

	if(length > 199) {
		$(".introduce").val(html.substring(0, 200))

		$("#span span").html(200);

	} else {
		$("#span span").html(length)
	}
    });  
function load2() {
	var length = $(".Introduce").val().length;
	var html = $(".Introduce").val();

	if(length > 199) {
		$(".Introduce").val(html.substring(0, 200))

		$("#span2 span").html(200);

	} else {
		$("#span2 span").html(length)
	}

}
//ios系统下字数检测
$(".Introduce").on("input propertychange", function() {    
	var length = $(".Introduce").val().length;
	var html = $(".Introduce").val();

	if(length > 199) {
		$(".Introduce").val(html.substring(0, 200))

		$("#span2 span").html(200);

	} else {
		$("#span2 span").html(length)
	}
    });  

		//验证微信号
         //当文本框失去焦点的时候
		 $(".wechat").blur(function(){
		 	var val1=$(".wechat").val()
    		var re=/^[a-zA-Z]{1}[-_a-zA-Z0-9]{5,19}$/;
    		if(!re.test(val1)){
    			$(".warn_wechat").html("输入微信号格式有误，请重新输入").css("display","block")
    			
    		}
    		
  		if(val1==""){
  			$(".warn_wechat").html("请输入微信号").css("display","block");
  		} 
    		
		 })
        //当文本框重新获得焦点的时候
        $(".wechat").focus(function(){
        	var val1=$(".wechat").val()
    		var re=/^[a-zA-Z]{1}[-_a-zA-Z0-9]{5,19}$/;
    		if(!re.test(val1)){
    			$(".warn_wechat").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_tel").html("").css("display","none");
    		}
        })
        
         //当文本框失去焦点的时候 群人数
		 $("#groupNum").blur(function(){
		 	var val1=$("#groupNum").val()
    		var re=/^[0-9]*$/;
    		if(!re.test(val1)){
    			$(".warn_groupNum").html("群人数必须是数字").css("display","block")
    		}
	  		if(val1==""){
	  			$(".warn_groupNum").html("请输入群人数").css("display","block");
	  		} 
		 })
        //当文本框重新获得焦点的时候
        $("#groupNum").focus(function(){
        	var val1=$("#groupNum").val()
    		var re=/^[0-9]*$/;
    		if(!re.test(val1)){
    			$(".warn_groupNum").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_groupNum").html("").css("display","none");
    		}
        })
        
        
         //当文本框失去焦点的时候  投放价格
		 $("#skillPrice").blur(function(){
		 	var val1=$("#skillPrice").val()
    		var re=/^\d+(\.\d+)?$/;
    		if(!re.test(val1)){
    			$(".warn_touPrice").html("投放价格必须是数字").css("display","block")
    		}
	  		if(val1==""){
	  			$(".warn_touPrice").html("请输入投放价格").css("display","block");
	  		} 
		 })
        //当文本框重新获得焦点的时候
        $("#skillPrice").focus(function(){
        	var val1=$("#skillPrice").val()
    		var re=/^\d+(\.\d+)?$/;
    		if(!re.test(val1)){
    			$(".warn_touPrice").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_touPrice").html("").css("display","none");
    		}
        })
        
        
        //粉丝人数
        $("#fansNum").blur(function(){
        	var val1=$("#fansNum").val()
        	var re=/^[0-9]*$/;
        	if(!re.test(val1)){
        		$(".warn_funNum").html("粉丝人数必须是数字").css("display","block")
        	}
        	if(val1==""){
        		$(".warn_funNum").html("请输入粉丝人数").css("display","block");
        	} 
        })
        
        //当文本框重新获得焦点的时候 粉丝人数
        $("#fansNum").focus(function(){
        	var val1= $("#fansNum").val()
    		var re=/^[0-9]*$/;
    		if(!re.test(val1)){
    			$(".warn_funNum").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_funNum").html("").css("display","none");
    		}
        })
        
        
         //平均阅读量
        $("#readNum").blur(function(){
        	var val1=$("#readNum").val()
        	var re=/^[0-9]*$/;
        	if(!re.test(val1)){
        		$(".warn_readNum").html("平均阅读量必须是数字").css("display","block")
        	}
        	if(val1==""){
        		$(".warn_readNum").html("请输入平均阅读量").css("display","block");
        	} 
        })
        
        //平均阅读量 当文本框重新获得焦点的时候
        $("#readNum").focus(function(){
        	var val1= $("#readNum").val()
    		var re=/^[0-9]*$/;
    		if(!re.test(val1)){
    			$(".warn_readNum").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_readNum").html("").css("display","none");
    		}
        })
        
        //头条报价
        $("#topLinePrice").blur(function(){
        	var val1=$("#topLinePrice").val()
        	var re=/^\d+(\.\d+)?$/;
        	if(!re.test(val1)){
        		$(".warn_topPrice").html("头条报价必须是数字").css("display","block")
        	}
        	if(val1==""){
        		$(".warn_topPrice").html("请输入头条报价`1").css("display","block");
        	} 
        })
        
        //当文本框重新获得焦点的时候 头条报价
        $("#topLinePrice").focus(function(){
        	var val1=$("#topLinePrice").val()
        	var re=/^\d+(\.\d+)?$/;
    		if(!re.test(val1)){
    			$(".warn_topPrice").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_topPrice").html("").css("display","none");
    		}
        })
        
         
        //次条报价
        $("#lessLinePrice").blur(function(){
        	var val1= $("#lessLinePrice").val()
        	var re=/^\d+(\.\d+)?$/;
        	if(!re.test(val1)){
        		$(".warn_lesPrice").html("次条报价必须是数字").css("display","block")
        	}
        	if(val1==""){
        		$(".warn_lesPrice").html("请输入次条报价").css("display","block");
        	} 
        })
        
        //当文本框重新获得焦点的时候 头条报价
        $("#lessLinePrice").focus(function(){
        	var val1= $("#lessLinePrice").val()
        	var re=/^\d+(\.\d+)?$/;
    		if(!re.test(val1)){
    			$(".warn_lesPrice").html("").css("display","none")
    		}
    		
    		if(val1==""){
    			$(".warn_lesPrice").html("").css("display","none");
    		}
        })
        
        function checkRate(input) {
			 var re = /^[1-9]+[0-9]*]*$/; //判断字符串是否为数字 //判断正整数 /^[1-9]+[0-9]*]*$/ 
			if (!re.test(input)) {
				return false;
			}
			return true;
		}
        
        