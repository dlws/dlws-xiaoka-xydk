var itemIndex = 0;
var tab1LoadEnd = false;
var tab2LoadEnd = false;

var las = labelList;
var citys = proList;
var timer="";
$('.hot_inp').bind('input propertychange', function() {  
   if($(this).val().length!=0){
	   $(".icon_remove").show();
   }else{
	   $(".icon_remove").hide();
   }
});  
  
//点击取消清空搜索框
$(".ctt_cancel,.icon_remove").click(function(){
	$(".hot_inp").val("");
});

//输入框输入内容搜索
function search(){
/*$(".ctt_hot a").click(function(){*/
	
	
	if($(".searchSource_content span:first-child").hasClass("searchSource_border")){
		var searchValue = $("#searchValue").val(); 
		 page = 1;//当前页
		 $("#page").val(1);
		 $("#maxPrice").val("");
		 $("#minPrice").val("");
		 $("#cityName").val("");
		 $("#schoolLabel").val("");
		 $("#serviceType").val("");
		if(!searchValue){
			searchValue="";
		}
		$(".searchResource_content").html("");
		
		$(".ctt_searchBox").eq(0).show().siblings(".ctt_searchBox").hide();
		 dropload.unlock();
         dropload.noData(false);
         dropload.resetload();
	}else{
		/*$(".search_content").show();*/
		 pageKa=1;
		 $(".ctt_searchBox").eq(1).show().siblings(".ctt_searchBox").hide();
		 $('.search_content .cttRecommend_content').html("");
		 dropload.unlock();
         dropload.noData(false);
         dropload.resetload();
	}
/*})*/
}
//切换搜资源和搜索大咖
$(".searchSource_content .searchSource_left").click(function() {
    var $this = $(this);
	itemIndex = $this.index();
	$(this).addClass("searchSource_border");
	$(this).find("span").addClass("searchSource_green");
	$(this).siblings().removeClass("searchSource_border");
	$(this).siblings().find("span").removeClass("searchSource_green");
	
        $(".ctt_searchBox").eq(itemIndex).show().siblings(".ctt_searchBox").hide();
		// 如果选中校园资源
		if(itemIndex == '0') {
			
			$(".class_choose").show();
			// 如果数据没有加载完
			if(!tab1LoadEnd) {
				// 解锁
				dropload.unlock();
				dropload.noData(false);
			} else {
				// 锁
				dropload.lock('down');
				dropload.noData();
			}
			// 如果选中校园大咖
		} else if(itemIndex == '1') {
			$(".class_choose").hide();
			if(!tab2LoadEnd) {
				// 解锁
				dropload.unlock();
				dropload.noData(false);
			} else {
				// 锁定
				dropload.lock('down');
				dropload.noData();
			}
		}
		// 重置
		dropload.resetload();
	
});

//点击重置
$(".reset_box").click(function() {
	$(".price_box input").val("");
	$(".result-tips").show();
	$(".show-result").hide();
	$(".service_style div").removeClass("line_style");
	$(".ctt_cha").hide();
	$(".ctt_jiantou").show();
	
})
//点击确定
$(".enure_box").click(function() {
	$(".class_filterContent").hide();
	$(".mask").hide();
	
	/*var max = jQuery("#maxPrice").val();
	var min = jQuery("#minPrice").val();
	var cname = jQuery("#cityName").val();
	var schoolLabel = jQuery("#schoolLabel").val(); 
	var serviceType = jQuery("#serviceType").val(); 
	var searchValue = $("#searchValue").val();*/
	$(".searchResource_content").html("");
	 page = 1;//当前页
	 $("#page").val(1);
	 dropload.unlock();
     dropload.noData(false);
     dropload.resetload();
	/*screen(max,min,cname,schoolLabel,serviceType,searchValue);*/
})

     
var page = 1;//当前页
var pageNum = 4;//每页显示条数

var pageKa = 1;//当前页
var pageNumKa = 10;//每页显示条数

var dropload = $('.search_content').dropload({
	scrollArea: window,
	domDown : {
         domClass   : 'dropload-down',
         domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
         domNoData  : '<div class="dropload-noData">没有更多内容</div>'
      },
	loadDownFn: function(me) {
		//这里需要判断来回切换的是搜索热点还是搜索结果页，只有当搜索热点消失，搜索结果页出现的时候才走dropload事件
	if($(".list_content").hasClass("ctt_none")) {
		
		// 加载搜资源的数据
		if(itemIndex == '0'){
        	page=parseInt($("#page").val());
        	var max = $("#maxPrice").val();
    		var min = $("#minPrice").val();
    		var cname = $("#cityName").val();
    		var schoolLabel = $("#schoolLabel").val(); //学校类别
    		var serviceType = $("#serviceType").val(); //线上线下
    		var searchValue = $("#searchValue").val(); //搜索词
    		var orderPrice = $("#orderPrice").val(); //价格由高到底 ,由低到高
    		var ordersellNo = $("#ordersellNo").val(); //销量
    		var fid = $("#fid").val(); //一级分类
			$.ajax({
				type: 'post',
				url: 'searchSkill.html',
				dataType: 'json',
				data : {currentPage:page,pageNum:pageNum,fid:fid,maxPrice:max,minPrice:min,cityName:cname,schoolLabel:schoolLabel,serviceType:serviceType,orderPrice:orderPrice,searchValue:searchValue,sellNo:ordersellNo},
				success: function(data) {
					
					var result = '';
					if(0 <= data.skills.length) {
						for(var j = 0; j <data.skills.length; j++) {
							result += '<li>' +
								'<div class="search_bg"></div>' +
								'<div class="searchList_content">' +
								'<div class="searchList_title">' +
								'<span class="searchList_pic"><img src="'+data.skills[j].headPortrait+'" class="search_pic" /></span>' +
								'<span class="searchList_name">'+data.skills[j].userName+'</span>' +
								'<div class="searchList_city">' +
								'<span class="iconfont icon-zuobiao"></span>' +
								'<span>'+data.skills[j].cityName+'</span>' +
								'</div>' +
								'</div>' +
								'<div class="ctt_Piccontent">' +
								'<div class="ctt_pic">' +
								'<img src=' + data.skills[j].image + ' class="search_pic">' +
								'<span class="ctt_consult"><span>' + data.skills[j].schoolName + '</span></span>' +
								'</div>' +
								'<span class="ctt_serviceTitle"><span>服务内容&nbsp;:&nbsp;'+data.skills[j].skillName+'</span></span>' +
								'</div>' +
								'</div>' +
								'<div class="ctt_Pricecontent">' +
								'<div class="Pricecontent_left">' +
								'<span>￥' + data.skills[j].skillPrice+ '元/次</span>' +
								'<span>[已售'+data.skills[j].sellNo+']</span>' +
								'</div>' +
								'<span class="Pricecontent_right" onclick="joinse(this)">加入已选</span>' +
								'<input type="hidden" value="'+data.skills[j].id+'" name="skillId">'+
								'<input type="hidden" value="'+data.skills[j].openId+'" name="openId_sel">'+
								'<input type="hidden" value="0" name="isPreferred">'+
								'<input type="hidden" value="'+data.skills[j].classValueF+'" name="enterType">'+
								'</div>' +
								'</li>';
						}
						
						if(pageNum > data.skills.length) {
							// 数据加载完
							tab1LoadEnd = true;
							// 锁定
							me.lock();
							// 无数据
							me.noData();
						}
						
						// 为了测试，延迟2秒加载
						setTimeout(function() {
							//判断是否进行页数加1
							 if(pageNum == data.skills.length){
	                            $("#page").val(page+1);
	                          }
							$('.searchResource_content').append(result);
							$(".dropload-down").css("margin-bottom",0)
							/* var oBox= document.getElementById("searchResource_content");  
						     var oCont=oBox.innerHTML;
						    
						     var fen=oCont.split(searchValue);
				             oBox.innerHTML = fen.join('<span style="background:#cfc;">' + searchValue + '</span> ');*/
							// 每次数据加载完，必须重置
							me.resetload();
						}, 1000);
					}
					if(data.skills.length==0){
                   	 me.lock();
                     // 无数据
                     me.noData();
                     me.resetload();
	               }
				},
				error: function(xhr, type) {
					alert('Ajax error!');
					// 即使加载出错，也得重置
					me.resetload();
				}
			});

			// 加载搜大咖的数据
		} else if(itemIndex == '1') {
			var searchValue = $("#searchValue").val();
			$.ajax({
				type: 'GET',
				url: 'searchBasicUser.html',
				dataType: 'json',
				data : {currentPage:pageKa,pageNum:pageNumKa,searchValue:searchValue},
				success: function(data) {
					var result = '';
					if(0 < data.users.length) {
						for(var j = 0; j < data.users.length; j++) {
							var oCont = data.users[j].cityName;
							var oContSchoolName = data.users[j].schoolName;
							var oContNickname = data.users[j].nickname;
							var oContaboutMe = data.users[j].aboutMe;
							
							var fen=oCont.split(searchValue);
							var fenSchoolName=oContSchoolName.split(searchValue);
							var fenNickname=oContNickname.split(searchValue);
							var fenAboutMe=oContaboutMe.split(searchValue);
							
							result +="<a href='"+path+"/personInfoDetail/detail.html?id="+data.users[j].id+"' class='ctt_new'>"+
							    '<li class="recommend">' +
								'<div class="recommend_left ctt_recommendLeft">' +
								'<img src= "'+data.users[j].headPortrait+'" class="search_pic">' +
								'</div>' +
								'<div class="ctt_recommendRight">' +
								'<div class="recommend_title1">' ;
								if(searchValue!=""){
									result+='<span class="recommend_name2">'+fenNickname.join('<strong class="searchSource_green">' + searchValue + '</strong> ')+'</span>';
								}else{
									result+='<span class="recommend_name2">'+data.users[j].schoolName+'-'+data.users[j].userName+'</span>';
								}
								
								if(searchValue!=""){
									result+='</div><p class="recommend_p">'+fenAboutMe.join('<strong class="searchSource_green">' + searchValue + '</strong> ')+'</p>';
								}else{
									result+='</div><p class="recommend_p">'+data.users[j].aboutMe+'</p>';
								}
								result+='</div>' +
								'</li>'+
								'</a>'
								
						}
						if(pageNumKa>data.users.length) {
							// 数据加载完
							tab2LoadEnd = true;
							// 锁定
							me.lock();
							// 无数据
							me.noData();
						}
						
						// 为了测试，延迟2秒加载
						setTimeout(function() {
							//判断是否进行页数加1
							 if(pageNumKa == data.users.length){
								 pageKa++;
	                          }
							$('.cttRecommend_content').append(result);
							$(".dropload-down").css("margin-bottom",0)
							// 每次数据加载完，必须重置
							me.resetload();
						}, 1000);
					}
					
					if(data.users.length==0){
                   	 me.lock();
                     // 无数据
                     me.noData();
                     me.resetload();
                   }
				},
				error: function(xhr, type) {
					alert('Ajax error!');
					// 即使加载出错，也得重置
					me.resetload();
				}
			});
		}
		}
	}
})

//}
$(".ctt_hot a").click(function(){
	$(".list_content").addClass("ctt_none");
	$(".search_content").show();
})
//点击搜索热词
$(".search_list a").click(function() {
	$(".list_content").addClass("ctt_none");
	$(".search_content").show();
	if($(".searchSource_content span:first-child").hasClass("searchSource_border")){
		$(".ctt_searchBox").eq(0).show().siblings(".ctt_searchBox").hide();
	}else{
		$(".ctt_searchBox").eq(1).show().siblings(".ctt_searchBox").hide();
	}
	var searchValue = $(this).children("span").html();
	$("#searchValue").val(searchValue);
	dropload.resetload(); //这个重载
})

$(".class_choose li").click(function() {
	var index = $(this).index(".class_choose li");
	if(index==1){
		if($(this).children("span").hasClass("class_active")){
			$("#ordersellNo").val(""); 
		}else{
			$("#ordersellNo").val("desc"); 
			$("#orderPrice").val(""); 
		}
		$(".searchResource_content").html("");
		 $("#page").val(1);
		 dropload.unlock();
	     dropload.noData(false);
	     dropload.resetload();
	}else{
		$("#ordersellNo").val(""); 
	}
	console.log($(this).index());
	if($(this).index()!=0&&$(this).index()!=1){
		$(this).find("span:first-child").toggleClass("class_active");
	}else{
		$(this).find("span:first-child").addClass("class_active");
		 
	}
	$(this).siblings().find("span:first-child").removeClass("class_active");
   if(index!=2){
	   $(".choose2 span:nth-of-type(2)").removeClass("filter_green").addClass("filter");
	   $(".class_filterContent").hide();
	   $(".mask").hide();
   }
   if($(".choose_sort span:first-child").hasClass("class_active")){
	   $(".choose_sort span:last-child").removeClass("icon_black").toggleClass("icon_down").toggleClass("icon_up")
   }else{
	   $(".choose_sort span:last-child").removeClass("icon_up").removeClass("icon_down").addClass("icon_black") 
	   $(".class_sortContent").hide();
   }
})
//点击排序出来的那一部分
$(".choose_sort").click(function() {

	$(">span:nth-of-type(2)",this).toggleClass("icon_down").toggleClass("icon_up");
	if($(".choose_sort span:nth-of-type(2)").hasClass("icon_down")){
		 $(".choose_sort span:nth-of-type(2)").removeClass("icon_down").addClass("icon_up");
		$(".class_sortContent").show();
	}else{
		$(".choose_sort span:nth-of-type(2)").removeClass("icon_up").addClass("icon_down");
		$(".class_sortContent").hide();
	}
	
})
$(".class_sort li").click(function() {
	$(this).addClass("class_active").siblings().removeClass("class_active");
	var index = $(this).index();
	$(".class_sort span").removeClass("icon-gou").eq(index).addClass("icon-gou");
	
	dataname = this.dataset.name
	$("#orderPrice").val(dataname);
	$(".searchResource_content").html("");
	 page = 1;//当前页
	 $("#page").val(1);
	 dropload.unlock();
     dropload.noData(false);
     dropload.resetload();
	 setTimeout(function() {
		$(".class_sortContent").hide();
		$('.iconSort').removeClass("icon_up").addClass("icon_down")
	}, 1000)
})
//点击筛选出来的
$('.choose2').on('click', function(e) {
	$(">span:nth-of-type(2)",this).toggleClass('filter').toggleClass("filter_green")
	var wh = $(document).height();
	var h = $(".ctt_headerSearch").outerHeight(true) + $(".searchSource_content").outerHeight(true) + $(".class_choose").outerHeight(true) + 9;
	$('.mask').css({ 'height': wh, "top": h + "px" }).toggle();
	$('.class_filterContent').css("top", h + "px").toggle();
});
$('.mask').on('click', function() {
	$('.mask').hide();
	$('.class_filterContent').toggle();
});

//点击右侧标签部分
$(".label_content a").click(function() {
	$(this).css({ "border": "1px solid #1d9243", "border-radius": "5px" });
	var index = $(this).index();
	$(".green_cha").eq(index).css("display", "block")
});
$(".service_style div").click(function() {
	var index = $(this).index();
	$("#serviceType").val(index+1);
	$(this).toggleClass("line_style").siblings().removeClass("line_style")
})

//选择所在地
mui.init();
var cityPicker = new mui.PopPicker({
	layer: 2
});
cityPicker.setData(citys);
var showCityPickerButton = document.getElementById('chooseCity');
showCityPickerButton.addEventListener('tap', function(event) {
	if($(event.target).hasClass('ctt_cha')){
		$("#chooseCity .result-tips").show();
		$("#chooseCity .show-result").hide();
		$(".ctt_jiantou").show();
		$(".ctt_cha").hide();
		return false;
	}
	cityPicker.show(function(items) {
		document.querySelector('#chooseCity .result-tips').style.display = "none";
		document.querySelector('#chooseCity .show-result').style.display = "block";
		document.querySelector('#chooseCity .show-result').innerText = items[1].text;
		$(".ctt_jiantou").hide();
		$(".ctt_cha").show();
		$("#cityName").val(items[1].text);
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}, false);
/*//选择学校类型
mui.init();
var picker = new mui.PopPicker({

	layer: 1
});
picker.setData(las);
var showCityPickerButton = document.getElementById('chooseSchool');
showCityPickerButton.addEventListener('tap', function(event) {
	picker.show(function(selectItems) {
		document.querySelector('#chooseSchool .result-tips').style.display = "none";
		document.querySelector('#chooseSchool .show-result').style.display = "block";
		document.querySelector('#chooseSchool .show-result').innerText = selectItems[0].text;
		$("#schoolLabel").val(selectItems[0].value);
	});
}, false);*/

/**
 * add to choice 
 * @param obj
 */

function joinse(obj){
	var gaoH=$(".ctt_headerSearch").outerHeight(true)+$(".searchSource_content").outerHeight(true)+$(".search_content").outerHeight(true);//获取当前页面高度	
	
	var skillId = $(obj).siblings("input[name='skillId']").val();
	var openId_sel = $(obj).siblings("input[name='openId_sel']").val();
	var isPreferred = $(obj).siblings("input[name='isPreferred']").val();
	var enterType = $(obj).siblings("input[name='enterType']").val();
	var information ={"skillId":skillId,"openId_sel":openId_sel,"isPreferred":isPreferred,"enterType":enterType};

	$.ajax({
		  url: baseP+"/skillOrder/joinSelectedSecond.html",
		  type: 'post',
		  dataType: 'json',
		  data:information,
		  success: function (data) {
			  if(data.ajax_status==true){
					 //增加加入已选
					
					 var html_str = "";
					  $.each(data.skill_list,function(i,v){  
						  html_str += '<li>'+
										'<span class="queen">'+
						  					'<img src="../xkh_version_2.3/xkh_version_2.3_1/img/queen.png" class="search_pic" />'+
						  				'</span>'+
										'<div class="popupWindow_message">'+
											'<span>'+v.skillName+'</span> <span>'+v.skillDepict+'</span>'+
										'</div>'+
										'<div class="popupWindow_number">'+
											'<span class="reduce" onclick="reduce(this)">-</span>'+
											'<span class="value">'+v.buyNum+'</span> '+
											'<span class="Add" onclick="add(this)" >+</span>'+
										'</div>'+
										'<input type="hidden" value='+v.seledId+' name="seledId">'+
									'</li>';
                  })  
                  $('#selectSkill').html(html_str);
					  
					  $(".popupWindow_content").removeClass("add_Move").addClass("add_move");
						 $("body,html").css({
								"overflow": "hidden"
							});
						//5秒弹窗自动关闭
							timer=setTimeout(function(){
								$(".popupWindow_content").removeClass("add_move").addClass("add_Move");
								$("body,html").css({
									"overflow": "scroll"
								});
							},5000)
						 $(".poopupWindow_box li:gt(0)").hide();
						if($(".poopupWindow_box li").length<=1){
								$(".popupWindow_more").removeClass("popupWindow_up");
							}
						//超过16个字省略号显示
						 var arr=[];
						 for(var i=0;i<$(".popupWindow_content li").length;i++){
						 	console.log($(".popupWindow_content li").length)
						 	
						     var message=$(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html()
						     arr.push(message);
						     var length=arr[i].length;
						 	if(length>16){
						 		$(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html($(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html().substring(0,16)+"...")
						 	}

						 }
				 }else{
					 alert("加入已选失败");
				 } 
		  },
		  fail: function (err, status) {
			  alert("加入已选失败");
		  }
		})
	
}
/*06-19加入已选*/
var popupWindow_height = $(".popupWindow_content").height();
$(".popupWindow_content").css({
	"WebkitTransform": "translateY(" + popupWindow_height + "px)",
	"position": "fixed",
	"left": 0,
	"bottom": 0,
	"zIndex": 1000,
	"background": "#fff"
})
/*$(document).on("click", ".Pricecontent_right", function() {
	$(".popupWindow_content").removeClass("add_Move").addClass("add_move")
})*/
$(".popupWindow_title .popupWindow_close").click(function() {
	$(".popupWindow_content").removeClass("add_move").addClass("add_Move");
	 $("body,html").css({
			"overflow": "scroll"
		});
})
$(".popupWindow_more").click(function() {
	clearInterval(timer)
	$(this).toggleClass("popupWindow_up").toggleClass("popupWindow_down");
	if($(this).hasClass("popupWindow_down")) {

		$(".poopupWindow_box li:nth-of-type(2)").show();

	} else {
		$(".poopupWindow_box li:nth-of-type(2)").hide()
	}
})
function reduce(obj){
	
	var seledId = $(obj).parent().siblings('input').val();
	var html = $(obj).siblings('.value');
	var value = html.text()
	value = parseInt(value)
	value -= 1
	
	if(value<1){
		value=1;
		html.text(1)
	}
	$.ajax({
		  url: baseP+"/skillOrder/modifyBuyNum.html",
		  type: 'post',
		  dataType: 'json',
		  data:{seledId:seledId,buyNum:value},
		  success: function (data) {
			 if(data==true){
				 html.text(value)
			 }else{
				 alert("修改失败");
			 } 
		  },
		  fail: function (err, status) {
			  alert("修改失败");
		  }
		})
	
	
	
}


function add(obj){
	var seledId = $(obj).parent().siblings('input').val();;
	var html = $(obj).siblings('.value');
	var value = html.text()
	value = parseInt(value)
	value += 1
	$.ajax({
		  url: baseP+"/skillOrder/modifyBuyNum.html",
		  type: 'post',
		  dataType: 'json',
		  data:{seledId:seledId,buyNum:value},
		  success: function (data) {
			 if(data==true){
				 html.text(value)
			 }else{
				 alert("修改失败");
			 } 
		  },
		  fail: function (err, status) {
			  alert("修改失败");
		  }
		})
}







