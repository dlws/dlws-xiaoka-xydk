var las = labelList;
var citys = proList;
//点击取消清空搜索框
$(".ctt_cancel").click(function() {
	$(".hot_inp").val("");
});

//输入框输入内容搜索
$(".ctt_hot a").click(function() {
	$(".list_Content").hide();
	$(".search_content").show();
	$(".searchResource_content").html("");
	$(".dropload-down").remove();
	 $("#page").val(1);
	drop();
})
//点击推荐热词
$(".search_list a").click(function() {
	$(".list_Content").hide();
	$(".search_content").show();
	var searchValue = $(this).children("span").html();
	$("#searchValue").val(searchValue);
	$(".searchResource_content").html("");
	$(".dropload-down").remove();
	page = 1;//当前页
	drop();
})
$(".class_choose li").click(function() {
	var index = $(this).index(".class_choose li");
	if(index==1){
		if($(this).children("span").hasClass("class_active")){
			$("#ordersellNo").val(""); 
		}else{
			$("#ordersellNo").val("desc"); 
		}
		$(".searchResource_content").html("");
		$(".dropload-down").remove();
		$("#page").val(1);
		drop();
	}else{
		$("#ordersellNo").val("");//是否按销量排序 
	}
	$(this).find("span:first-child").toggleClass("class_active");
	$(this).siblings().find("span:first-child").removeClass("class_active");

})
//点击排序出来的那一部分
$(".choose_sort").click(function() {
	$(".class_sortContent").toggle();
	if($('.icon_sort').hasClass('icon-triangle')) {
		$('.icon_sort').removeClass("icon-triangle").addClass("icon-triangle-copy")
	} else {
		$('.icon_sort').removeClass("icon-triangle-copy").addClass("icon-triangle")
	}

})
$(".class_sort li").click(function() {
	$(this).addClass("class_active").siblings().removeClass("class_active");
	var index = $(this).index();
	$(".class_sort span").removeClass("icon-gou").eq(index).addClass("icon-gou");
	
	dataname = this.dataset.name
	$("#orderPrice").val(dataname);
	$(".searchResource_content").html("");
	$(".dropload-down").remove();
	 page = 1;//当前页
	 $("#page").val(1);
	 drop();
	 setTimeout(function() {
		$(".class_sortContent").hide();
		$('.icon_sort').removeClass("icon-triangle-copy").addClass("icon-triangle")
	}, 1000)
})
//点击筛选出来的
$('.choose2').on('click', function(e) {
	var wh = $(document).height();
	var h = $(".ctt_headerSearch").outerHeight(true) + $(".searchSource_content").outerHeight(true) + $(".class_choose").outerHeight(true) + 9;
	console.log(h)
	$('.mask').css({ 'height': wh, "top": h + "px" }).toggle();
	$('.class_filterContent').css("top", h + "px").toggle();
});
$('.mask').on('click', function() {
	$('.mask').hide();
	$('.class_filterContent').toggle();
});

//点击右侧标签部分
$(".label_content a").click(function() {
	$(this).css({ "border": "1px solid #1d9243", "border-radius": "5px" });
	var index = $(this).index();
	$(".green_cha").eq(index).css("display", "block")
});
$(".service_style div").click(function() {
	$(this).addClass("line_style").siblings().removeClass("line_style")
})
//点击重置
$(".reset_box").click(function() {
	$(".price_box input").val("");
	$(".result-tips").show();
	$(".show-result").hide();
	$(".service_style div").removeClass("line_style");
})
//点击确定
$(".enure_box").click(function() {
	$(".class_filterContent").hide();
	$(".mask").hide();
	
	var max = jQuery("#maxPrice").val();
	var min = jQuery("#minPrice").val();
	var cname = jQuery("#cityName").val();
	var schoolLabel = jQuery("#schoolLabel").val(); 
	var serviceType = jQuery("#serviceType").val(); 
	var searchValue = $("#searchValue").val();
	$(".searchResource_content").html("");
	$(".dropload-down").remove();
	 page = 1;//当前页
	 $("#page").val(1);
	 drop();
})
//选择所在地
mui.init();
var cityPicker = new mui.PopPicker({
	layer: 2
});
cityPicker.setData(citys);
var showCityPickerButton = document.getElementById('chooseCity');
showCityPickerButton.addEventListener('tap', function(event) {
	cityPicker.show(function(items) {
		document.querySelector('#chooseCity .result-tips').style.display = "none";
		document.querySelector('#chooseCity .show-result').style.display = "block";
		document.querySelector('#chooseCity .show-result').innerText = items[1].text;
		$("#cityName").val(items[1].text);
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}, false);

//选择学校类型
mui.init();
var picker = new mui.PopPicker({

	layer: 1
});
picker.setData(las);
var showCityPickerButton = document.getElementById('chooseSchool');
showCityPickerButton.addEventListener('tap', function(event) {
	picker.show(function(selectItems) {
		document.querySelector('#chooseSchool .result-tips').style.display = "none";
		document.querySelector('#chooseSchool .show-result').style.display = "block";
		document.querySelector('#chooseSchool .show-result').innerText = selectItems[0].text;
		$("#schoolLabel").val(selectItems[0].value);
	});
}, false);

var page = 1;//当前页
var pageNum = 4;//每页显示条数
function drop(){
   	//下拉刷新
	page=parseInt($("#page").val());
	var max = $("#maxPrice").val();
	var min = $("#minPrice").val();
	var cname = $("#cityName").val();
	var schoolLabel = $("#schoolLabel").val(); //学校类别
	var serviceType = $("#serviceType").val(); //线上线下
	var searchValue = $("#searchValue").val(); //搜索词
	var orderPrice = $("#orderPrice").val(); //价格由高到底 ,由低到高
	var ordersellNo = $("#ordersellNo").val(); //销量
	var fid = $("#fatherId").val(); //一级分类
    // dropload
    $('.search_content').dropload({
        scrollArea : window,
          domDown : {
         domClass   : 'dropload-down',
         domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
          domNoData  : '<div class="dropload-noData">没有更多内容</div>'
      },
        loadDownFn : function(me){
            $.ajax({
                type: 'GET',
                url: 'getUserInfoByClassIdV2.html',
                dataType: 'json',
                data : {fid:fid,currentPage:page,pageNum:pageNum,maxPrice:max,minPrice:min,cityName:cname,schoolLabel:schoolLabel,serviceType:serviceType,orderPrice:orderPrice,searchValue:searchValue,sellNo:ordersellNo},
                success: function(data){
                    var result = '';
                    for(var j = 0; j < data.skills.length; j++){
	                        result +=  '<li>'+
										'<div class="search_bg"></div>'+
										'<div class="searchList_content">'+
											'<div class="searchList_title">'+
												'<span class="searchList_pic"><img src="'+data.skills[j].headPortrait+'" class="search_pic" /></span>'+
												'<span class="searchList_name">'+data.skills[j].userName+'</span>'+
												'<div class="searchList_city">'+
													'<span class="iconfont icon-zuobiao"></span>'+
													'<span>'+data.skills[j].cityName+'</span>'+
												'</div>'+
											'</div>'+
											'<div class="ctt_Piccontent">'+
												'<div class="ctt_pic">'+
													'<img src="'+data.skills[j].image+'" class="search_pic">'+
													'<span class="ctt_consult"><span>'+data.skills[j].schoolName+'</span></span>'+
												'</div>'+
												'<span class="ctt_serviceTitle"><span>服务标题&nbsp;:&nbsp;'+data.skills[j].skillName+'</span></span>'+
											'</div>'+
										'</div>'+
										'<div class="ctt_Pricecontent">'+
											'<div class="Pricecontent_left">'+
												'<span>￥' + data.skills[j].skillPrice+ '元/次</span>'+
												'<span>[已售'+data.skills[j].sellNo+']</span>'+
											'</div>'+
											'<span class="Pricecontent_right">加入已选</span>'+
										'</div>'+
									'</li>';
                        if((j + 1) >= data.skills.length){
                        /*	$(".dropload-noData").html("没有更多内容")*/
                            // 锁定
                            me.lock();
                            // 无数据
                            me.noData();
                            break;
                        }
                    }
                    
                    if(pageNum>data.skills.length) {
						// 数据加载完
						tab2LoadEnd = true;
						// 锁定
						me.lock();
						// 无数据
						me.noData();
					}
                    
                    if(data.skills.length==0){
                    	 me.lock();
                         // 无数据
                         me.noData();
                    }
                      
                    // 为了测试，延迟1秒加载
                    setTimeout(function(){
                        $('.searchResource_content').append(result);
                        // 每次数据加载完，必须重置
                        me.resetload();
                    },1000);
                },
                error: function(xhr, type){
                    alert('Ajax error!');
                    // 即使加载出错，也得重置
                    me.resetload();
                }
            });
        }
});
   }
