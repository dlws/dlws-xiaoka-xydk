/**
 * 公用js引用（加入已选更改为调用ajax方法） 此js需要后引入
 */
//06-19加入已选出来的
var timer="";
var popupWindow_height = $(".popupWindow_content").height();
$(".popupWindow_content").css({
	"WebkitTransform": "translateY(" + popupWindow_height + "px)",
	"position": "fixed",
	"left": 0,
	"bottom": 0,
	"zIndex": 1000,
	"background": "#fff"
})
$(".popupWindow_close").click(function() {
	$(".popupWindow_content").removeClass("add_move").addClass("add_Move");
	$("body,html").css({
		"overflow": "scroll"
	});
})

//跳转至已选
$(".popupWindow_car").click(function(){
	window.location.href = "../home/getmyInform.html"
})
$(".popupWindow_more").click(function() {
	clearInterval(timer)
	$(this).toggleClass("popupWindow_up").toggleClass("popupWindow_down");
	if($(this).hasClass("popupWindow_down")) {

		$(".poopupWindow_box li").show();

	} else {
		
		$(".poopupWindow_box li:gt(0)").hide()
	}
})
function reduce(obj){
	
	var html = $(obj).next('.value')
	var value = html.text()
	value = parseInt(value)
	value -= 1
	var seledId = obj.parentNode.nextElementSibling.value;
	if(value<1){
		value=1;
		html.text(1)
	}
	$.ajax({
		  url: baseP+"/skillOrder/modifyBuyNum.html",
		  type: 'post',
		  dataType: 'json',
		  data:{seledId:seledId,buyNum:value},
		  success: function (data) {
			 if(data==true){
				 html.text(value)
			 }else{
				 alert("修改失败");
			 } 
		  },
		  fail: function (err, status) {
			  alert("修改失败");
		  }
		})
}

function add(obj){

	var html = $(obj).prev('.value')
	var value = html.text()
	var seledId = obj.parentNode.nextElementSibling.value;
	value = parseInt(value)
	value += 1
	//购买数量+1
	$.ajax({
		  url: baseP+"/skillOrder/modifyBuyNum.html",
		  type: 'post',
		  dataType: 'json',
		  data:{seledId:seledId,buyNum:value},
		  success: function (data) {
			 if(data==true){
				 html.text(value)
			 }else{
				 alert("修改失败");
			 } 
		  },
		  fail: function (err, status) {
			  alert("修改失败");
		  }
		})

}

	var extendsList = new Array();
	extendsList[1]="jzzx";
	extendsList[2]="jztf";
	
	var h=$(document).height();//获取当前页面高度
	
	function joinse(obj){
		
		var skillId = $(obj).siblings("input[name='skillId']").val();
		var openId_sel = $(obj).siblings("input[name='openId_sel']").val();
		var isPreferred = $(obj).siblings("input[name='isPreferred']").val();
		var enterType = $(obj).siblings("input[name='enterType']").val();
		var information ={"skillId":skillId,"openId_sel":openId_sel,"isPreferred":isPreferred,"enterType":enterType};
		 var topHeight=$(obj).offset().top;
			 console.log(topHeight)
		$.ajax({
			  url: baseP+"/skillOrder/joinSelectedSecond.html",
			  type: 'post',
			  dataType: 'json',
			  data:information,
			  timeout: 10000,
			  success: function (data, status) {
				 if(data.ajax_status==true){
					 //清空当前box中的样式
					 $(".poopupWindow_box").empty();
					 var maneyLi = "";
					 for(var i=0;i<data.skill_list.length;i++){

						 var sub="";
						 if(extendsList.toString().indexOf(data.skill_list[i].enterType) > -1 ){
		       					sub ="<span class='reduce'>-</span>"
		       					+"<span class='value'>"+data.skill_list[i].buyNum+"</span>"
		       					+"<span class='Add'>+</span>";
		       				}else{
		       					sub="<span class='reduce' onclick='reduce(this)'>-</span>"
		       					+"<span class='value'>"+data.skill_list[i].buyNum+"</span>"
		       					+"<span class='Add' onclick='add(this)'>+</span>";
		       				}
						 maneyLi += "<li>"+
						 "<span class='queen'><img src='/xkh_version_2.3/xkh_version_2.3_1/img/queen.png' class='search_pic'></span>"+
			       			"<div class='popupWindow_message'>"+
			       				"<span>"+data.skill_list[i].skillName+"</span>"+
			       				"<span>"+data.skill_list[i].skillDepict+"</span>"+
			       			"</div>"+
			       			"<div class='popupWindow_number'>"
			       				+sub
			       			+"</div>"+
			       			"<input type='hidden' value='"+data.skill_list[i].seledId+"' name='seledId'>"+
			       		"</li>"
					 }
					//将当前信息追加到
					 $(".poopupWindow_box").append(maneyLi);
					 $(".popupWindow_content").removeClass("add_Move").addClass("add_move");
					
					 $("body,html").css({
							"overflow": "hidden"
							
						});
					//5秒弹窗自动关闭
						timer=setTimeout(function(){
							$(".popupWindow_content").removeClass("add_move").addClass("add_Move");
							$("body,html").css({
								"overflow": "scroll"
								
							});
						},5000)
					 $(".poopupWindow_box li:gt(0)").hide();
					if($(".poopupWindow_box li").length<=1){
							$(".popupWindow_more").removeClass("popupWindow_up");
						}
					//超过16个字省略号显示
					 var arr=[];
					 for(var i=0;i<$(".popupWindow_content li").length;i++){
					 	console.log($(".popupWindow_content li").length)
					 	
					     var message=$(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html()
					     arr.push(message);
					     var length=arr[i].length;
					 	if(length>16){
					 		$(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html($(".popupWindow_content li").eq(i).find(".popupWindow_message span:nth-of-type(2)").html().substring(0,16)+"...")
					 	}

					 }
					 
				 }else{
					 alert("加入已选失败");
				 } 
			  },
			  fail: function (err, status) {
				  alert("加入已选失败");
			  }
			})
		
	}
	/**
	 * （count down）
	 * 订单倒计时(当前毫秒数，当前对象)
	 * 调用时时间应由span标签包起来 obj为this对象
	 * 
	 */
	var interval = 1000; 
	var classArray = ['head_content','head-content','orderDetail_wait'];//符合条件的样式放到这个数组里
	function ShowCountDown(timmer,obj,flage) 
	{ 
		var now = new Date(); 
		//var endDate = new Date(year, month-1, day,hour,minute,sec_num);生成一个 
		var leftTime=timmer-now.getTime(); 
		var leftsecond = parseInt(leftTime/1000); 
		//var day1=parseInt(leftsecond/(24*60*60*6)); 
		var day1=Math.floor(leftsecond/(60*60*24)); 
		var hour=Math.floor((leftsecond-day1*24*60*60)/3600); 
		var minute=Math.floor((leftsecond-day1*24*60*60-hour*3600)/60); 
		var second=Math.floor(leftsecond-day1*24*60*60-hour*3600-minute*60); 
		var cc = obj; 
		if(flage){
			cc.innerHTML = hour+"小时"+minute+"分"+second+"秒";//当前样式无天
		}else{
			cc.innerHTML = day1+"天"+hour+"小时"+minute+"分"+second+"秒";
		}
		 

	} 
	window.setInterval(function(){
		
		//获取当前所有的span标签中的关于时间信息的参数
		var array = $(".timmer");
		
		for(var i=0;i<array.length;i++){
			
			//alert($(array[i]).siblings());
			var timmer = $(array[i]).parent().siblings("input[name='timmer']").val();
			//判定当前样式名称是否为head_content 为头样式 若有则去掉‘0天’
			var className = $(array[i]).parent().parent().attr("class");
			
			if(classArray.indexOf(className) != -1){
				ShowCountDown(timmer,array[i],true);
				continue;
			}
			
			ShowCountDown(timmer,array[i],false);
		}
		//当前为数组对象 for循环调用当前获取到的所有时间类型，以及传递当前对象
		//ShowCountDown(2010,4,20,'divdown1');
		}, interval
		
	); 
	
	/**
	 * 判定当前页面跳转根据不同的状态跳转
	 * 做不同的处理
	 */
	/*点击判断跳转到那个页面  */
	$(".call_content").click(function(e){
		
		//dianji点击拨打电话
		if($(e.target).hasClass('call_tel')){
			var h=$("body").height();
			$(".seller_picture img:first-child").click(function(){
				
				var sellPhone = $(this).siblings("input[name='sellPhone']").val();
				var tel = 'tel:'+sellPhone;
				$(".popup_mask").css({"height":h,"display":"block"});
				$(".telephone_content").attr('href',tel);
				$(".telephone_content").text("点此拨打："+sellPhone);
				$(".telephone_content").css("display","block")
			})
			$(".popup_mask").click(function(){
				$(this).hide();
				$(".telephone_content").hide();
			})
			return;
		}
		if($(e.target).hasClass('call_communicate')){
			var otherOpenId = $(this).children("input[name='otherOpenId']").val();
			window.location.href=path+"/chatMesseage/createChatRelation.html?otherOpenId="+otherOpenId;
			return;
		}
		
		var orderId = $(this).children("input[name='orderId']").val();
		window.location.href=path+"/chatMesseage/queryOrderDetail.html?orderId="+orderId+"&channel=buyer"; 
			/* 	console.log("我要跳页面了") */
	})
	
/**
 * 关于已选问题中未读信息的标注
 */
$(function () {
    (function isReadSelect() {
        $.ajax({
            url: path+"/skillOrder/isReadSelect.html",
            data: {"timed": new Date().getTime()},
            dataType: "json",
            timeout: 5000,
            error: function (XMLHttpRequest, errorThrown) {
            	$(".redCar_pic").hide();
            	isReadSelect(); // 递归调用
            },
            success: function (data) {
                if (data == true) {// 请求成功
                	$(".redCar_pic").show();
                }
                if(data == false){
                	$(".redCar_pic").hide();
                }
                isReadSelect();
            }
        });
    })();
    
});

