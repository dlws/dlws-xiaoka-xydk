/**
 * 卖家拒绝退款和同意退款
 */

	//1.拒绝退款 
	function  reject(obj){
		var value = $(obj).siblings("input[name='orderId']").val();
		var json = {"orderId":value};
		$.ajax({
			  url: path+"/orderseaSell/updateOrderSellReject.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data);
			    location.reload();//刷新当前请求  
			  },
			  fail: function (err, status) {
			    console.log(err)
			  }
			})
		
	}
	//2.同意退款 
	function  agree(obj){
		$(obj).parents('li').hide();
		var value = $(obj).siblings("input[name='orderId']").val();
		var json = {"orderId":value};
		$.ajax({
			  url: path+"/orderseaSell/updateOrderSellAgree.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data);
			    location.reload();//刷新当前请求  
			  },
			  fail: function (err, status) {
			    console.log(err);
			  }
			})
		
	}