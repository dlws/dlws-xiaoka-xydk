/**
 * 用于卖家更改订单状态 公用js
 */
	//1.拒绝接单
	function  refuseSell(obj){
		$(obj).parents('li').hide();
		var value = $(obj).siblings("input[name='orderId']").val();
		var json = {"orderId":value};
		$.ajax({
			  url: path+"/orderseaSell/updateOrderSellDfuRefuse.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data);
			    //location.reload();//刷新当前请求  
			    window.location.href=path+"/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
			  },
			  fail: function (err, status) {
			    console.log(err);
			  }
			})
		$(obj).parent().removeAttr("href");
	}
	//2. 接单 
	function  accept(obj){
		$(obj).parents('li').hide();
		var value = $(obj).siblings("input[name='orderId']").val();
		var json = {"orderId":value};
		$.ajax({
			  url: path+"/orderseaSell/updateOrderSellAccept.html",
			  type: 'post',
			  dataType: 'json',
			  data:json,
			  timeout: 10000,
			  success: function (data, status) {
			    console.log(data);
			    //location.reload();//刷新当前请求  
			    window.location.href= path+"/orderseaSell/searchOrderSell.html?tabNum="+sessionStorage.tabNum;
			  },
			  fail: function (err, status) {
			    console.log(err);
			  }
			})
		$(obj).parent().removeAttr("href");
	}

