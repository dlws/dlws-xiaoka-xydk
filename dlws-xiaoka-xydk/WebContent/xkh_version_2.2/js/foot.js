
/**
 * 加载当前可配置参数
 * 控制foot样式 可以使当前页面配置默认选中列
 */
$(document).ready(function(){
	$(".foot-tab-item").eq(0).removeClass("foot-active");
	//获取当前默认选中的列  如：首页 - 消息 - 发布 - 已选 - 我的 012345
	$(".foot-tab-item").eq(index).addClass("foot-active");
});


/**
 * 关于已选问题中未读信息的标注
 */
$(function () {
    (function isReadSelect() {
        $.ajax({
            url: path+"/skillOrder/isReadSelect.html",
            data: {"timed": new Date().getTime()},
            dataType: "json",
            timeout: 5000,
            error: function (XMLHttpRequest, errorThrown) {
            	$("#select").hide();
            	isReadSelect(); // 递归调用
            },
            success: function (data) {
            	
            	//控制当前样式  isSelect 为已选功能的mark
            	var isSelect = jQuery.inArray("isSelect", data);  //0
            	if (isSelect!=-1) {// 请求成功
                	$("#select").show();
                }else{
                	$("#select").hide();
                }
            	//控制当前消息列表中的mark标记
            	var haveNewMessage = jQuery.inArray("haveNewMessage", data);  //0
            	if (haveNewMessage != -1) {// 请求成功
                	$("#message").show();
                }else{
                	$("#message").hide();
                }
            	//标记当前用户 我的按钮
            	var markMyself = jQuery.inArray("markMyself", data);  //0
            	if (markMyself!=-1) {// 请求成功
                	$(".circle").show();
                }else{
                	$(".circle").hide();
                }
            	//当已选功能 index 为4的时候表示为我的页面
            	if(index==4){
            		
            		detailMarkbuyer(data);
            	}
            	isReadSelect(); // 递归调用	
            }
        });
    })();
    
});


/**
 * 初始化时请求到已选
 */
$(document).ready(function(){
	
	var json = {"json":0};
	$.ajax({
        url: path+"/skillOrder/isReadSelect.html",
        data: json,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
        	
        	//控制当前样式  isSelect 为已选功能的mark
        	var isSelect = jQuery.inArray("isSelect", data);  //0
        	if (isSelect!=-1) {// 请求成功
            	$("#select").show();
            }else{
            	$("#select").hide();
            }
        	//控制当前消息列表中的mark标记
        	var haveNewMessage = jQuery.inArray("haveNewMessage", data);  //0
        	if (haveNewMessage != -1) {// 请求成功
            	$("#message").show();
            }else{
            	$("#message").hide();
            }
        	//标记当前用户 我的按钮
        	var markMyself = jQuery.inArray("markMyself", data);  //0
        	if (markMyself!=-1) {// 请求成功
            	$(".circle").show();
            }else{
            	$(".circle").hide();
            }
        	//当已选功能 index 为4的时候表示为我的页面
        	if(index==4){
        		
        		detailMarkbuyer(data);
        	}
            	
        }
    });
	
});


/**
 * 处理当前buyer中的标记
 * 
 */
function detailMarkbuyer(data){
	//待付款
	var obligation = jQuery.inArray("obligation", data);  //0
	//待服务
	var pendingService = jQuery.inArray("pendingService", data);  //0
	//待确认
	var noConfirmed = jQuery.inArray("noConfirmed", data);  //0
	//待退款
	var refundable = jQuery.inArray("refundable", data);  //0
	//当前卖出技能的总数	
	var sellerCount = jQuery.inArray("sellerCount", data);  //0
	//控制当前样式  isSelect 为已选功能的mark
	if (obligation!=-1) {// 请求成功
    	$(".user_content .user_picContent:first-child .ctt_circle").show();
    }else{
    	$(".user_content .user_picContent:first-child .ctt_circle").hide();
    }
	if (pendingService!=-1) {// 请求成功
    	$(".user_content .user_picContent:nth-of-type(2) .ctt_circle").show();
    }else{
    	$(".user_content .user_picContent:nth-of-type(2) .ctt_circle").hide();
    }
	if (noConfirmed!=-1) {// 请求成功
    	$(".user_content .user_picContent:nth-of-type(3) .ctt_circle").show();
    }else{
    	$(".user_content .user_picContent:nth-of-type(3) .ctt_circle").hide();
    }
	if (refundable!=-1) {// 请求成功
    	$(".user_content .user_picContent:nth-of-type(4) .ctt_circle").show();
    }else{
    	$(".user_content .user_picContent:nth-of-type(4) .ctt_circle").hide();
    }
	if (sellerCount!=-1) {// 请求成功
    	$(".ctt_circle1").show();
    }else{
    	$(".ctt_circle1").hide();
    }
	
}
