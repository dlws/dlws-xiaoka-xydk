/*
Navicat MySQL Data Transfer

Source Server         : 111.206.116.90
Source Server Version : 50173
Source Host           : 111.206.116.90:3306
Source Database       : xiaoka

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2015-10-26 16:00:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for busy_cart
-- ----------------------------
DROP TABLE IF EXISTS `busy_cart`;
CREATE TABLE `busy_cart` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `wxCustomerId` varchar(200) NOT NULL COMMENT '微信顾客ID',
  `createdTime` datetime NOT NULL COMMENT '加入购物车时间时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车表';

-- ----------------------------
-- Records of busy_cart
-- ----------------------------

-- ----------------------------
-- Table structure for busy_cart_goods
-- ----------------------------
DROP TABLE IF EXISTS `busy_cart_goods`;
CREATE TABLE `busy_cart_goods` (
  `cartId` bigint(20) NOT NULL COMMENT '购物车ID',
  `goodsId` bigint(20) NOT NULL COMMENT '商品ID',
  `num` int(5) NOT NULL DEFAULT '0' COMMENT '商品数量'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车商品表';

-- ----------------------------
-- Records of busy_cart_goods
-- ----------------------------

-- ----------------------------
-- Table structure for busy_coupon
-- ----------------------------
DROP TABLE IF EXISTS `busy_coupon`;
CREATE TABLE `busy_coupon` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `couponId` int(20) NOT NULL COMMENT '优惠卷编号',
  `couponname` varchar(64) NOT NULL COMMENT '优惠卷名字',
  `coupontype` int(3) NOT NULL COMMENT ' 优惠卷类型',
  `shopId` bigint(20) NOT NULL COMMENT '店铺ID',
  `facevalue` decimal(10,2) NOT NULL COMMENT ' 优惠卷面值',
  `consumlimit` decimal(10,2) DEFAULT NULL COMMENT '最低消费限制',
  `deadlinetype` int(3) DEFAULT NULL COMMENT ' 截止时间类型',
  `begindate` datetime NOT NULL COMMENT ' 有效天数',
  `enddate` datetime NOT NULL COMMENT ' 截止日期',
  `description` mediumtext COMMENT '优惠卷描述信息',
  `ownerId` bigint(20) NOT NULL COMMENT '店铺所有者',
  `isDelete` int(2) NOT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠卷列表设置';

-- ----------------------------
-- Records of busy_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for busy_coupon_control
-- ----------------------------
DROP TABLE IF EXISTS `busy_coupon_control`;
CREATE TABLE `busy_coupon_control` (
  `id` bigint(20) NOT NULL,
  `switchStatus` int(3) NOT NULL COMMENT '首次进入店铺时自动发放优惠券 x-s',
  `couponId` bigint(20) NOT NULL COMMENT '选择首次进入店铺时自动发放优的惠券 x-s',
  `couponNum` int(10) NOT NULL COMMENT '发放张数 x-s',
  `ownerId` bigint(20) NOT NULL COMMENT '店铺所有者 ',
  `isDelete` int(2) NOT NULL COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='优惠卷的设置';

-- ----------------------------
-- Records of busy_coupon_control
-- ----------------------------

-- ----------------------------
-- Table structure for busy_coupon_log
-- ----------------------------
DROP TABLE IF EXISTS `busy_coupon_log`;
CREATE TABLE `busy_coupon_log` (
  `giveoutId` bigint(20) NOT NULL,
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `openId` varchar(64) DEFAULT NULL COMMENT '顾客微信号 x-like',
  `orderId` bigint(20) DEFAULT NULL COMMENT '订单ID',
  `couponId` bigint(20) DEFAULT NULL COMMENT '优惠券ID x-s',
  `status` int(3) DEFAULT NULL COMMENT '使用状态 x-s',
  PRIMARY KEY (`giveoutId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠卷发放';

-- ----------------------------
-- Records of busy_coupon_log
-- ----------------------------

-- ----------------------------
-- Table structure for busy_goods
-- ----------------------------
DROP TABLE IF EXISTS `busy_goods`;
CREATE TABLE `busy_goods` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `shop_id` bigint(20) NOT NULL COMMENT '商品所在店铺ID',
  `goods_num` int(3) DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
  `goods_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  `goods_category` bigint(20) NOT NULL COMMENT '商品分类ID',
  `lable` varchar(255) DEFAULT NULL COMMENT '标签',
  `integral` int(10) NOT NULL DEFAULT '0' COMMENT '积分',
  `detail_id` bigint(20) NOT NULL COMMENT '描述ID',
  `status` int(4) NOT NULL DEFAULT '1' COMMENT '商品状态（0：停售，1：正常）',
  `inventory_status` int(3) NOT NULL DEFAULT '0' COMMENT '启用库存（0：关闭，1：启用）',
  `inventory` int(11) DEFAULT '0' COMMENT '库存量（总的）',
  `inventory_sync` int(3) NOT NULL DEFAULT '0' COMMENT '是否开启每天自动库存功能（0：关闭，1：启用）',
  `original_price_status` int(3) NOT NULL DEFAULT '0' COMMENT '是否开启原价（0：关闭，1：启用）',
  `original_price` decimal(20,2) DEFAULT '0.00' COMMENT '原价',
  `attribute_status` int(3) NOT NULL DEFAULT '0' COMMENT '是否开启商品属性（0：关闭，1：启用）',
  `limit_status` int(3) DEFAULT '0' COMMENT '是否开启限购活动（0：不开启，1：开启）',
  `limit_num` int(5) DEFAULT '1' COMMENT '每人限购数量',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `modifiedTime` datetime NOT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
-- Records of busy_goods
-- ----------------------------

-- ----------------------------
-- Table structure for busy_goods_attribute
-- ----------------------------
DROP TABLE IF EXISTS `busy_goods_attribute`;
CREATE TABLE `busy_goods_attribute` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `attribute_name` varchar(50) NOT NULL COMMENT '属性名',
  `attribute_value` varchar(50) NOT NULL COMMENT '属性值',
  `attribute_price` decimal(20,2) NOT NULL COMMENT '属性价格',
  `goods_id` bigint(20) NOT NULL COMMENT '商品ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品属性表';

-- ----------------------------
-- Records of busy_goods_attribute
-- ----------------------------

-- ----------------------------
-- Table structure for busy_goods_category
-- ----------------------------
DROP TABLE IF EXISTS `busy_goods_category`;
CREATE TABLE `busy_goods_category` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `category_name` varchar(10) NOT NULL COMMENT '分类名称',
  `category_no` int(3) DEFAULT NULL COMMENT '分类编号(编号决定展示顺序，编号越小越靠前)',
  `week_show` int(3) NOT NULL DEFAULT '0' COMMENT '是否开启只星期几显示(0：关闭，1：开启)',
  `monday_show` int(3) DEFAULT '0' COMMENT '星期一显示(0：关闭，1：开启)',
  `tuesday_show` int(3) DEFAULT '0' COMMENT '星期二显示(0：关闭，1：开启)',
  `wednesday_show` int(3) DEFAULT '0' COMMENT '星期三显示(0：关闭，1：开启)',
  `thursday_show` int(3) DEFAULT '0' COMMENT '星期四显示(0：关闭，1：开启)',
  `friday_show` int(3) DEFAULT '0' COMMENT '星期五显示(0：关闭，1：开启)',
  `saturday_show` int(3) DEFAULT '0' COMMENT '星期六显示(0：关闭，1：开启)',
  `sunday_show` int(3) DEFAULT '0' COMMENT '星期日显示(0：关闭，1：开启)',
  `parent_id` bigint(20) NOT NULL COMMENT '父分类ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of busy_goods_category
-- ----------------------------

-- ----------------------------
-- Table structure for busy_goods_detail
-- ----------------------------
DROP TABLE IF EXISTS `busy_goods_detail`;
CREATE TABLE `busy_goods_detail` (
  `id` bigint(20) NOT NULL,
  `goods_id` bigint(20) NOT NULL COMMENT '商品ID',
  `detail` text COMMENT '描述内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品的描述信息表';

-- ----------------------------
-- Records of busy_goods_detail
-- ----------------------------

-- ----------------------------
-- Table structure for busy_goods_image
-- ----------------------------
DROP TABLE IF EXISTS `busy_goods_image`;
CREATE TABLE `busy_goods_image` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `goods_id` bigint(20) NOT NULL COMMENT '商品ID',
  `sort` int(6) NOT NULL DEFAULT '0' COMMENT '图片排序，是否主图等',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品图片表';

-- ----------------------------
-- Records of busy_goods_image
-- ----------------------------

-- ----------------------------
-- Table structure for busy_goods_limit
-- ----------------------------
DROP TABLE IF EXISTS `busy_goods_limit`;
CREATE TABLE `busy_goods_limit` (
  `id` bigint(20) NOT NULL,
  `limit_begin_time` datetime DEFAULT NULL,
  `limit_end_time` varchar(0) DEFAULT NULL COMMENT '（多个就加表）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品限购';

-- ----------------------------
-- Records of busy_goods_limit
-- ----------------------------

-- ----------------------------
-- Table structure for busy_goods2
-- ----------------------------
DROP TABLE IF EXISTS `busy_goods2`;
CREATE TABLE `busy_goods2` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `goods_num` int(3) DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
  `goods_spec` varchar(255) NOT NULL COMMENT '商品规格x-s',
  `categoryId` bigint(20) NOT NULL COMMENT '商品分类(外键)x-s',
  `goods_place` varchar(255) NOT NULL COMMENT '商品产地',
  `market_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '商品市场价格',
  `factory_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '商品供货价格',
  `retail_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '商品零售价格',
  `lable` varchar(255) DEFAULT NULL COMMENT '标签',
  `detail_id` bigint(20) DEFAULT NULL COMMENT '描述ID',
  `status` int(4) NOT NULL DEFAULT '1' COMMENT '商品状态（0：停售，1：正常）x-like',
  `salewayId` bigint(20) NOT NULL DEFAULT '0' COMMENT '促销方式（0：无促销）x-s',
  `limit_status` int(3) DEFAULT '0' COMMENT '是否开启限购活动（0：不开启，1：开启）x-s',
  `limit_num` int(5) DEFAULT '1' COMMENT '每人限购数量',
  `is_delete` int(3) NOT NULL DEFAULT '0' COMMENT '是否删除（0：未删除，1：已删除）',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `modifiedTime` datetime DEFAULT NULL COMMENT '最后修改时间',
  `citycode` varchar(10) CHARACTER SET armscii8 NOT NULL COMMENT '城市代码（eg:1000[北京]）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
-- Records of busy_goods2
-- ----------------------------

-- ----------------------------
-- Table structure for busy_order
-- ----------------------------
DROP TABLE IF EXISTS `busy_order`;
CREATE TABLE `busy_order` (
  `orderId` bigint(20) NOT NULL COMMENT '订单编号',
  `createdTime` datetime NOT NULL COMMENT '下单时间',
  `payMoney` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `payway` int(3) NOT NULL COMMENT '支付方式',
  `shopId` bigint(20) NOT NULL COMMENT '店铺ID',
  `openId` varchar(64) NOT NULL COMMENT '顾客ID',
  `payflag` int(3) NOT NULL DEFAULT '0' COMMENT '是否支付',
  `makeflag` int(3) NOT NULL DEFAULT '0' COMMENT '是否制作完成',
  `distribflag` int(3) NOT NULL DEFAULT '0' COMMENT '是否配送',
  `distribId` bigint(20) DEFAULT NULL COMMENT '配送员Id',
  `finishflag` int(3) NOT NULL DEFAULT '0' COMMENT '订单是否结束',
  `timeoutflag` int(3) NOT NULL DEFAULT '0' COMMENT '是否超时',
  `cancelflag` int(3) NOT NULL DEFAULT '0' COMMENT '是否取消订单',
  `orderDetails` varchar(2000) DEFAULT NULL COMMENT '订单详情',
  `orderPreset` varchar(2000) DEFAULT NULL COMMENT '用户下单填写的预设字段值',
  `couponenabled` int(3) NOT NULL DEFAULT '0' COMMENT '是否允许使用优惠卷',
  `ownerId` bigint(20) NOT NULL COMMENT '注册商户ID',
  `isDelete` int(3) NOT NULL DEFAULT '0' COMMENT '是否删除（1：删除；0:未删除）',
  `remark` mediumtext COMMENT '备注信息',
  `cancleTime` datetime DEFAULT NULL COMMENT '取消订单时间',
  `payTime` datetime DEFAULT NULL COMMENT '支付时间',
  `takeTime` datetime DEFAULT NULL COMMENT '取货时间',
  `tel` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `takeCode` varchar(20) DEFAULT NULL COMMENT '取货码（目前为6为数字）支付成功后生成',
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

-- ----------------------------
-- Records of busy_order
-- ----------------------------

-- ----------------------------
-- Table structure for busy_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `busy_order_goods`;
CREATE TABLE `busy_order_goods` (
  `orderId` bigint(20) NOT NULL COMMENT '订单ID',
  `goodsId` bigint(20) NOT NULL COMMENT '商品ID',
  `num` int(5) NOT NULL DEFAULT '0' COMMENT '数量',
  `goods_spec` varchar(255) NOT NULL COMMENT '商品规格',
  `market_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '商品市场价格',
  `retail_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '商品零售价格',
  `giveNum` int(5) DEFAULT '0' COMMENT '赠送数量',
  `saleway_name` varchar(255) DEFAULT NULL COMMENT '促销方式名称 x-like'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单商品表\r\n该表没有主键，orderId唯一性';

-- ----------------------------
-- Records of busy_order_goods
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_activity
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_activity`;
CREATE TABLE `busy_shop_activity` (
  `shopId` bigint(20) NOT NULL COMMENT '店铺ID',
  `couponcontrol` int(3) NOT NULL DEFAULT '0' COMMENT '店铺优惠卷控制开关',
  `salecontrol` int(3) NOT NULL DEFAULT '0' COMMENT '店铺打折控制开关',
  `couponvalue` decimal(10,2) DEFAULT NULL COMMENT '优惠卷最大面值',
  `salevalue` double(10,2) DEFAULT NULL COMMENT '店铺折扣值',
  PRIMARY KEY (`shopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺活动表';

-- ----------------------------
-- Records of busy_shop_activity
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_basicinfo
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_basicinfo`;
CREATE TABLE `busy_shop_basicinfo` (
  `shopId` bigint(20) NOT NULL COMMENT '店铺Id',
  `shopname` varchar(255) NOT NULL COMMENT '店铺名称x-like',
  `shopclassId` bigint(20) NOT NULL COMMENT '店铺分类（目前分为：1：校内和2：校外）',
  `industrytype` int(3) DEFAULT NULL COMMENT '行业分类',
  `phone` varchar(20) NOT NULL COMMENT '联系电话x-like',
  `address` varchar(255) DEFAULT NULL COMMENT '店铺地址x-like',
  `longitude` double(10,6) NOT NULL COMMENT '店铺经度',
  `latitude` double(10,6) NOT NULL COMMENT '店铺纬度',
  `ownerId` bigint(20) NOT NULL COMMENT '店铺所有着（个人、公司）',
  `sortId` int(6) DEFAULT NULL COMMENT '编号字段用于排序',
  `isDelete` int(3) NOT NULL DEFAULT '0' COMMENT '是否删除(1:删除;0:未删除)',
  `description` mediumtext COMMENT '店铺描述',
  `citycode` varchar(10) NOT NULL COMMENT '城市代码（如：1000[北京]）',
  `orderPrefix` varchar(20) DEFAULT NULL COMMENT '订单的前缀',
  PRIMARY KEY (`shopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺基本信息表';

-- ----------------------------
-- Records of busy_shop_basicinfo
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_business
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_business`;
CREATE TABLE `busy_shop_business` (
  `shopId` bigint(20) NOT NULL COMMENT '店铺Id',
  `shopstatus` int(3) NOT NULL DEFAULT '0' COMMENT '店铺状态(0:关闭;1:开启)',
  `statusinfo` varchar(255) DEFAULT NULL COMMENT '店铺关闭提示信息',
  `begintime` time DEFAULT NULL COMMENT '营业开始时间',
  `endtime` time DEFAULT NULL COMMENT '营业结束时间',
  `serviceDistance` double(10,2) NOT NULL COMMENT '服务距离',
  `dispatchArea` varchar(2000) DEFAULT NULL COMMENT '配送区域',
  PRIMARY KEY (`shopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺营业表';

-- ----------------------------
-- Records of busy_shop_business
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_class
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_class`;
CREATE TABLE `busy_shop_class` (
  `classId` bigint(20) NOT NULL COMMENT '店铺分类ID',
  `className` varchar(255) NOT NULL COMMENT '店铺分类名称',
  `ownerId` bigint(20) DEFAULT NULL COMMENT '店铺分类所有着',
  PRIMARY KEY (`classId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺分类表';

-- ----------------------------
-- Records of busy_shop_class
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_display
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_display`;
CREATE TABLE `busy_shop_display` (
  `shopId` bigint(20) NOT NULL COMMENT '店铺ID',
  `shopnotice` mediumtext COMMENT '店铺公告',
  `chNoticestatus` int(3) NOT NULL DEFAULT '0' COMMENT '选购页面公开状态(0:关闭；1:开启)',
  `choosenotice` mediumtext COMMENT '选购页面公告',
  `connecttype` int(3) DEFAULT NULL COMMENT '进入店铺连接类型（1：首页；2：选购）',
  `commentstatus` int(3) NOT NULL DEFAULT '0' COMMENT '评论功能(0:关闭；1：开启)',
  `showtype` int(3) NOT NULL DEFAULT '1' COMMENT '展示风格（1：图片展示；2：文字展示）',
  `salestatus` int(3) NOT NULL DEFAULT '0' COMMENT '是否显示商品的销量',
  PRIMARY KEY (`shopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺显示表';

-- ----------------------------
-- Records of busy_shop_display
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_industry
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_industry`;
CREATE TABLE `busy_shop_industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '行业分类ID',
  `industryName` varchar(255) NOT NULL COMMENT '行业分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='行业分类字典表';

-- ----------------------------
-- Records of busy_shop_industry
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_pic
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_pic`;
CREATE TABLE `busy_shop_pic` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `shopId` bigint(20) NOT NULL COMMENT '店铺ID',
  `shopPicUrl` varchar(255) NOT NULL COMMENT '店铺图片路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺图片表';

-- ----------------------------
-- Records of busy_shop_pic
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_remind
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_remind`;
CREATE TABLE `busy_shop_remind` (
  `shopId` bigint(20) NOT NULL COMMENT '店铺ID',
  `messageflag` int(3) NOT NULL DEFAULT '0' COMMENT '短信订单提醒状态x-combo',
  `mobile` varchar(15) NOT NULL COMMENT '接收短信电话号码',
  `emailflag` int(3) NOT NULL DEFAULT '0' COMMENT '邮箱订单提醒状态x-combo',
  `email` varchar(64) NOT NULL COMMENT '接收订单email地址',
  `wxflag` int(3) NOT NULL DEFAULT '0' COMMENT '微信订单提醒状态x-combo',
  `wxId` varchar(128) NOT NULL COMMENT '商家微信号',
  `openId` varchar(64) NOT NULL COMMENT '商家微信号(加密后)',
  `ownerId` bigint(20) NOT NULL COMMENT '注册商户ID',
  PRIMARY KEY (`shopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺订单提醒表';

-- ----------------------------
-- Records of busy_shop_remind
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shop_user_basicinfo
-- ----------------------------
DROP TABLE IF EXISTS `busy_shop_user_basicinfo`;
CREATE TABLE `busy_shop_user_basicinfo` (
  `shopId` bigint(20) NOT NULL COMMENT '店铺的Id',
  `shopuserId` bigint(20) DEFAULT NULL COMMENT '店铺用户的Id',
  `isMaster` int(11) DEFAULT NULL COMMENT '  是否是商家(1：商家 0：员工)',
  PRIMARY KEY (`shopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺员工基本信息表';

-- ----------------------------
-- Records of busy_shop_user_basicinfo
-- ----------------------------

-- ----------------------------
-- Table structure for busy_shopuser
-- ----------------------------
DROP TABLE IF EXISTS `busy_shopuser`;
CREATE TABLE `busy_shopuser` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `username` varchar(64) DEFAULT NULL COMMENT '商家用户名',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `sex` int(3) DEFAULT '1' COMMENT '性别(1:男、0:女)',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
  `wxId` varchar(64) DEFAULT NULL COMMENT '商家微信号',
  `openId` varchar(64) DEFAULT NULL COMMENT '商家微信号（加密）',
  `address` varchar(255) DEFAULT NULL COMMENT '商家联系地址',
  `citycode` varchar(6) DEFAULT '0' COMMENT '所在城市代码',
  `status` int(3) DEFAULT NULL COMMENT '状态（1，是已激活、0未激活）',
  `isDelete` int(3) DEFAULT '0' COMMENT '是否删除(1:是、0:否)',
  `createdTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商家用户表';

-- ----------------------------
-- Records of busy_shopuser
-- ----------------------------
INSERT INTO `busy_shopuser` VALUES ('1', 'shopuser', '670b14728ad9902aecba32e22fa4f6bd', '1', '18637048966', null, null, null, '110100', '1', '0', '2015-08-29 13:59:34');
INSERT INTO `busy_shopuser` VALUES ('1596001001', 'bj_yunying', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '北京市', '110100', '1', '0', '2015-10-26 15:07:24');
INSERT INTO `busy_shopuser` VALUES ('1596002001', 'bj_yunyingjingli', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '北京市', '110100', '1', '0', '2015-10-26 15:09:16');
INSERT INTO `busy_shopuser` VALUES ('1596003001', 'bj_dianzhang', '6590f73ecdf351c38de00befd2ecf17b', '1', '123456789', null, null, '北京市', '110100', '1', '0', '2015-10-26 15:10:57');
INSERT INTO `busy_shopuser` VALUES ('1596004001', 'bj_dianyuan', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '北京市', '110100', '1', '0', '2015-10-26 15:11:54');
INSERT INTO `busy_shopuser` VALUES ('1596005001', 'cd_yunying', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '成都', '510100', '1', '0', '2015-10-26 15:14:22');
INSERT INTO `busy_shopuser` VALUES ('1596006001', 'cd_yunyingjingli', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '成都', '510100', '1', '0', '2015-10-26 15:15:54');
INSERT INTO `busy_shopuser` VALUES ('1596007001', 'cd_dianzhang', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '成都', '510100', '1', '0', '2015-10-26 15:16:53');
INSERT INTO `busy_shopuser` VALUES ('1596008001', 'cd_dianyuan', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '成都', '510100', '1', '0', '2015-10-26 15:18:19');
INSERT INTO `busy_shopuser` VALUES ('1596009001', 'xa_yunying', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '西安', '610100', '1', '0', '2015-10-26 15:19:34');
INSERT INTO `busy_shopuser` VALUES ('1596010001', 'xa_yunyingjingli', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '西安', '610100', '1', '0', '2015-10-26 15:20:37');
INSERT INTO `busy_shopuser` VALUES ('1596011001', 'xa_dianzhang', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '西安', '610100', '1', '0', '2015-10-26 15:21:31');
INSERT INTO `busy_shopuser` VALUES ('1596012001', 'xa_dianyuan', 'e10adc3949ba59abbe56e057f20f883e', '1', '123456789', null, null, '西安', '610100', '1', '0', '2015-10-26 15:22:14');

-- ----------------------------
-- Table structure for dic_industryClassification
-- ----------------------------
DROP TABLE IF EXISTS `dic_industryClassification`;
CREATE TABLE `dic_industryClassification` (
  `id` int(10) NOT NULL DEFAULT '0' COMMENT 'id主键',
  `industryName` varchar(15) DEFAULT NULL COMMENT '行业名称',
  `industryCode` varchar(15) DEFAULT NULL COMMENT '行业代码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='行业分类字典表';

-- ----------------------------
-- Records of dic_industryClassification
-- ----------------------------

-- ----------------------------
-- Table structure for plat_authority
-- ----------------------------
DROP TABLE IF EXISTS `plat_authority`;
CREATE TABLE `plat_authority` (
  `authorityId` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `authorityName` varchar(255) NOT NULL DEFAULT '' COMMENT '权限名称x-like',
  `authorityCode` varchar(255) DEFAULT NULL COMMENT '权限简称',
  PRIMARY KEY (`authorityId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='系统权限表';

-- ----------------------------
-- Records of plat_authority
-- ----------------------------
INSERT INTO `plat_authority` VALUES ('3', '权限管理', 'AUTH_AUTH');
INSERT INTO `plat_authority` VALUES ('5', '统计', 'AUTH_COUNT');
INSERT INTO `plat_authority` VALUES ('6', '查看订单--ALL', 'AUTH_ORDER_LOOK_ALL');
INSERT INTO `plat_authority` VALUES ('7', '分拣汇总订单', 'AUTH_ORDER_SORT');
INSERT INTO `plat_authority` VALUES ('8', '采购汇总订单', 'AUTH_ORDER_PURCH');
INSERT INTO `plat_authority` VALUES ('9', '店铺管理--店长', 'AUTH_SHOP');
INSERT INTO `plat_authority` VALUES ('10', '商品管理', 'AUTH_GOODS');
INSERT INTO `plat_authority` VALUES ('11', '营销活动促销', 'AUTH_ACTIVITY_PROMOT');
INSERT INTO `plat_authority` VALUES ('12', '商品管理--允许查看供货价', 'AUTH_GOODS_LOOK_ALLOW');
INSERT INTO `plat_authority` VALUES ('13', '店铺管理--店员', ' AUTH_SHOP_UPDATE');
INSERT INTO `plat_authority` VALUES ('14', '商品管理--不允许查看供货价', 'AUTH_GOODS_LOOK_NOALLOW');
INSERT INTO `plat_authority` VALUES ('15', '营销活动优惠卷', 'AUTH_ACTIVITY_COUPON');
INSERT INTO `plat_authority` VALUES ('16', '查看订单--SELF', 'AUTH_ORDER_LOOK_SELF');
INSERT INTO `plat_authority` VALUES ('17', '客户管理', null);
INSERT INTO `plat_authority` VALUES ('19', '店铺管理--运营', 'AUTH_SHOP_MULTI');

-- ----------------------------
-- Table structure for plat_authresource
-- ----------------------------
DROP TABLE IF EXISTS `plat_authresource`;
CREATE TABLE `plat_authresource` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `authorityId` int(20) NOT NULL COMMENT '权限Id',
  `resourceId` int(20) NOT NULL COMMENT '资源Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8 COMMENT='权限资源关联表';

-- ----------------------------
-- Records of plat_authresource
-- ----------------------------
INSERT INTO `plat_authresource` VALUES ('1', '1', '1');
INSERT INTO `plat_authresource` VALUES ('2', '1', '2');
INSERT INTO `plat_authresource` VALUES ('4', '1', '3');
INSERT INTO `plat_authresource` VALUES ('8', '1', '5');
INSERT INTO `plat_authresource` VALUES ('10', '5', '921004001');
INSERT INTO `plat_authresource` VALUES ('11', '5', '919002001');
INSERT INTO `plat_authresource` VALUES ('12', '5', '921001001');
INSERT INTO `plat_authresource` VALUES ('13', '5', '921002001');
INSERT INTO `plat_authresource` VALUES ('14', '5', '921003001');
INSERT INTO `plat_authresource` VALUES ('15', '6', '947001001');
INSERT INTO `plat_authresource` VALUES ('16', '6', '947002001');
INSERT INTO `plat_authresource` VALUES ('17', '9', '992002001');
INSERT INTO `plat_authresource` VALUES ('18', '9', '992003001');
INSERT INTO `plat_authresource` VALUES ('19', '9', '992004001');
INSERT INTO `plat_authresource` VALUES ('20', '9', '992005001');
INSERT INTO `plat_authresource` VALUES ('21', '9', '992006001');
INSERT INTO `plat_authresource` VALUES ('22', '10', '992007001');
INSERT INTO `plat_authresource` VALUES ('23', '10', '992008001');
INSERT INTO `plat_authresource` VALUES ('24', '10', '992009001');
INSERT INTO `plat_authresource` VALUES ('25', '10', '992010001');
INSERT INTO `plat_authresource` VALUES ('26', '10', '992011001');
INSERT INTO `plat_authresource` VALUES ('27', '11', '992012001');
INSERT INTO `plat_authresource` VALUES ('28', '3', '1068007001');
INSERT INTO `plat_authresource` VALUES ('29', '3', '1068008001');
INSERT INTO `plat_authresource` VALUES ('30', '3', '1068009001');
INSERT INTO `plat_authresource` VALUES ('31', '3', '1068010001');
INSERT INTO `plat_authresource` VALUES ('32', '3', '1068011001');
INSERT INTO `plat_authresource` VALUES ('33', '12', '992007001');
INSERT INTO `plat_authresource` VALUES ('34', '12', '992008001');
INSERT INTO `plat_authresource` VALUES ('35', '11', '1068001001');
INSERT INTO `plat_authresource` VALUES ('36', '13', '992002001');
INSERT INTO `plat_authresource` VALUES ('37', '13', '992003001');
INSERT INTO `plat_authresource` VALUES ('38', '13', '992005001');
INSERT INTO `plat_authresource` VALUES ('39', '14', '992007001');
INSERT INTO `plat_authresource` VALUES ('40', '14', '992008001');
INSERT INTO `plat_authresource` VALUES ('45', '15', '992012001');
INSERT INTO `plat_authresource` VALUES ('46', '15', '1068005001');
INSERT INTO `plat_authresource` VALUES ('47', '15', '1068006001');
INSERT INTO `plat_authresource` VALUES ('48', '7', '921004001');
INSERT INTO `plat_authresource` VALUES ('49', '7', '921003001');
INSERT INTO `plat_authresource` VALUES ('50', '8', '921004001');
INSERT INTO `plat_authresource` VALUES ('51', '8', '921003001');
INSERT INTO `plat_authresource` VALUES ('52', '16', '947001001');
INSERT INTO `plat_authresource` VALUES ('53', '16', '947002001');
INSERT INTO `plat_authresource` VALUES ('56', '10', '1314003001');
INSERT INTO `plat_authresource` VALUES ('57', '17', '1316001001');
INSERT INTO `plat_authresource` VALUES ('58', '17', '1316002001');
INSERT INTO `plat_authresource` VALUES ('59', '17', '1316003001');
INSERT INTO `plat_authresource` VALUES ('61', '10', '1496002001');
INSERT INTO `plat_authresource` VALUES ('62', '10', '1496003001');
INSERT INTO `plat_authresource` VALUES ('63', '10', '1496004001');
INSERT INTO `plat_authresource` VALUES ('64', '6', '1496005001');
INSERT INTO `plat_authresource` VALUES ('65', '6', '1496006001');
INSERT INTO `plat_authresource` VALUES ('66', '6', '1496007001');
INSERT INTO `plat_authresource` VALUES ('67', '6', '1496008001');
INSERT INTO `plat_authresource` VALUES ('68', '6', '1496009001');
INSERT INTO `plat_authresource` VALUES ('69', '15', '1496010001');
INSERT INTO `plat_authresource` VALUES ('70', '15', '1496011001');
INSERT INTO `plat_authresource` VALUES ('71', '15', '1496012001');
INSERT INTO `plat_authresource` VALUES ('72', '15', '1496013001');
INSERT INTO `plat_authresource` VALUES ('73', '15', '1496014001');
INSERT INTO `plat_authresource` VALUES ('74', '15', '1496015001');
INSERT INTO `plat_authresource` VALUES ('75', '11', '1068002001');
INSERT INTO `plat_authresource` VALUES ('76', '11', '1068003001');
INSERT INTO `plat_authresource` VALUES ('77', '11', '1068004001');
INSERT INTO `plat_authresource` VALUES ('78', '17', '1496016001');
INSERT INTO `plat_authresource` VALUES ('79', '17', '1496017001');
INSERT INTO `plat_authresource` VALUES ('80', '17', '1496018001');
INSERT INTO `plat_authresource` VALUES ('81', '17', '1496019001');
INSERT INTO `plat_authresource` VALUES ('82', '17', '1496020001');
INSERT INTO `plat_authresource` VALUES ('83', '17', '1496021001');
INSERT INTO `plat_authresource` VALUES ('84', '17', '1496022001');
INSERT INTO `plat_authresource` VALUES ('85', '17', '1496023001');
INSERT INTO `plat_authresource` VALUES ('86', '17', '1496024001');
INSERT INTO `plat_authresource` VALUES ('87', '19', '992002001');
INSERT INTO `plat_authresource` VALUES ('88', '19', '992003001');
INSERT INTO `plat_authresource` VALUES ('89', '19', '992004001');
INSERT INTO `plat_authresource` VALUES ('90', '19', '992005001');
INSERT INTO `plat_authresource` VALUES ('91', '19', '992006001');
INSERT INTO `plat_authresource` VALUES ('92', '9', '1573001001');
INSERT INTO `plat_authresource` VALUES ('93', '19', '1573001001');

-- ----------------------------
-- Table structure for plat_category
-- ----------------------------
DROP TABLE IF EXISTS `plat_category`;
CREATE TABLE `plat_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL COMMENT '分类名称',
  `category_no` int(3) DEFAULT NULL COMMENT '分类编号(编号决定展示顺序，编号越小越靠前)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1441880139609043 DEFAULT CHARSET=utf8 COMMENT='商品分类表';

-- ----------------------------
-- Records of plat_category
-- ----------------------------

-- ----------------------------
-- Table structure for plat_city
-- ----------------------------
DROP TABLE IF EXISTS `plat_city`;
CREATE TABLE `plat_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) NOT NULL,
  `name` varchar(20) NOT NULL,
  `provincecode` varchar(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=345 DEFAULT CHARSET=utf8 COMMENT='级联表城市';

-- ----------------------------
-- Records of plat_city
-- ----------------------------
INSERT INTO `plat_city` VALUES ('1', '110100', '北京市', '110000');
INSERT INTO `plat_city` VALUES ('2', '130100', '石家庄市', '130000');
INSERT INTO `plat_city` VALUES ('3', '130200', '唐山市', '130000');
INSERT INTO `plat_city` VALUES ('4', '130300', '秦皇岛市', '130000');
INSERT INTO `plat_city` VALUES ('5', '130400', '邯郸市', '130000');
INSERT INTO `plat_city` VALUES ('6', '130500', '邢台市', '130000');
INSERT INTO `plat_city` VALUES ('7', '130600', '保定市', '130000');
INSERT INTO `plat_city` VALUES ('8', '130700', '张家口市', '130000');
INSERT INTO `plat_city` VALUES ('9', '130800', '承德市', '130000');
INSERT INTO `plat_city` VALUES ('10', '130900', '沧州市', '130000');
INSERT INTO `plat_city` VALUES ('11', '131000', '廊坊市', '130000');
INSERT INTO `plat_city` VALUES ('12', '131100', '衡水市', '130000');
INSERT INTO `plat_city` VALUES ('13', '140100', '太原市', '140000');
INSERT INTO `plat_city` VALUES ('14', '140200', '大同市', '140000');
INSERT INTO `plat_city` VALUES ('15', '140300', '阳泉市', '140000');
INSERT INTO `plat_city` VALUES ('16', '140400', '长治市', '140000');
INSERT INTO `plat_city` VALUES ('17', '140500', '晋城市', '140000');
INSERT INTO `plat_city` VALUES ('18', '140600', '朔州市', '140000');
INSERT INTO `plat_city` VALUES ('19', '140700', '晋中市', '140000');
INSERT INTO `plat_city` VALUES ('20', '140800', '运城市', '140000');
INSERT INTO `plat_city` VALUES ('21', '140900', '忻州市', '140000');
INSERT INTO `plat_city` VALUES ('22', '141000', '临汾市', '140000');
INSERT INTO `plat_city` VALUES ('23', '141100', '吕梁市', '140000');
INSERT INTO `plat_city` VALUES ('24', '150100', '呼和浩特市', '150000');
INSERT INTO `plat_city` VALUES ('25', '150200', '包头市', '150000');
INSERT INTO `plat_city` VALUES ('26', '150300', '乌海市', '150000');
INSERT INTO `plat_city` VALUES ('27', '150400', '赤峰市', '150000');
INSERT INTO `plat_city` VALUES ('28', '150500', '通辽市', '150000');
INSERT INTO `plat_city` VALUES ('29', '150600', '鄂尔多斯市', '150000');
INSERT INTO `plat_city` VALUES ('30', '150700', '呼伦贝尔市', '150000');
INSERT INTO `plat_city` VALUES ('31', '150800', '巴彦淖尔市', '150000');
INSERT INTO `plat_city` VALUES ('32', '150900', '乌兰察布市', '150000');
INSERT INTO `plat_city` VALUES ('33', '152200', '兴安盟', '150000');
INSERT INTO `plat_city` VALUES ('34', '152500', '锡林郭勒盟', '150000');
INSERT INTO `plat_city` VALUES ('35', '152900', '阿拉善盟', '150000');
INSERT INTO `plat_city` VALUES ('36', '210100', '沈阳市', '210000');
INSERT INTO `plat_city` VALUES ('37', '210200', '大连市', '210000');
INSERT INTO `plat_city` VALUES ('38', '210300', '鞍山市', '210000');
INSERT INTO `plat_city` VALUES ('39', '210400', '抚顺市', '210000');
INSERT INTO `plat_city` VALUES ('40', '210500', '本溪市', '210000');
INSERT INTO `plat_city` VALUES ('41', '210600', '丹东市', '210000');
INSERT INTO `plat_city` VALUES ('42', '210700', '锦州市', '210000');
INSERT INTO `plat_city` VALUES ('43', '210800', '营口市', '210000');
INSERT INTO `plat_city` VALUES ('44', '210900', '阜新市', '210000');
INSERT INTO `plat_city` VALUES ('45', '211000', '辽阳市', '210000');
INSERT INTO `plat_city` VALUES ('46', '211100', '盘锦市', '210000');
INSERT INTO `plat_city` VALUES ('47', '211200', '铁岭市', '210000');
INSERT INTO `plat_city` VALUES ('48', '211300', '朝阳市', '210000');
INSERT INTO `plat_city` VALUES ('49', '211400', '葫芦岛市', '210000');
INSERT INTO `plat_city` VALUES ('50', '220100', '长春市', '220000');
INSERT INTO `plat_city` VALUES ('51', '220200', '吉林市', '220000');
INSERT INTO `plat_city` VALUES ('52', '220300', '四平市', '220000');
INSERT INTO `plat_city` VALUES ('53', '220400', '辽源市', '220000');
INSERT INTO `plat_city` VALUES ('54', '220500', '通化市', '220000');
INSERT INTO `plat_city` VALUES ('55', '220600', '白山市', '220000');
INSERT INTO `plat_city` VALUES ('56', '220700', '松原市', '220000');
INSERT INTO `plat_city` VALUES ('57', '220800', '白城市', '220000');
INSERT INTO `plat_city` VALUES ('58', '222400', '延边朝鲜族自治州', '220000');
INSERT INTO `plat_city` VALUES ('59', '230100', '哈尔滨市', '230000');
INSERT INTO `plat_city` VALUES ('60', '230200', '齐齐哈尔市', '230000');
INSERT INTO `plat_city` VALUES ('61', '230300', '鸡西市', '230000');
INSERT INTO `plat_city` VALUES ('62', '230400', '鹤岗市', '230000');
INSERT INTO `plat_city` VALUES ('63', '230500', '双鸭山市', '230000');
INSERT INTO `plat_city` VALUES ('64', '230600', '大庆市', '230000');
INSERT INTO `plat_city` VALUES ('65', '230700', '伊春市', '230000');
INSERT INTO `plat_city` VALUES ('66', '230800', '佳木斯市', '230000');
INSERT INTO `plat_city` VALUES ('67', '230900', '七台河市', '230000');
INSERT INTO `plat_city` VALUES ('68', '231000', '牡丹江市', '230000');
INSERT INTO `plat_city` VALUES ('69', '231100', '黑河市', '230000');
INSERT INTO `plat_city` VALUES ('70', '231200', '绥化市', '230000');
INSERT INTO `plat_city` VALUES ('71', '232700', '大兴安岭地区', '230000');
INSERT INTO `plat_city` VALUES ('72', '310100', '上海市', '310000');
INSERT INTO `plat_city` VALUES ('74', '320100', '南京市', '320000');
INSERT INTO `plat_city` VALUES ('75', '320200', '无锡市', '320000');
INSERT INTO `plat_city` VALUES ('76', '320300', '徐州市', '320000');
INSERT INTO `plat_city` VALUES ('77', '320400', '常州市', '320000');
INSERT INTO `plat_city` VALUES ('78', '320500', '苏州市', '320000');
INSERT INTO `plat_city` VALUES ('79', '320600', '南通市', '320000');
INSERT INTO `plat_city` VALUES ('80', '320700', '连云港市', '320000');
INSERT INTO `plat_city` VALUES ('81', '320800', '淮安市', '320000');
INSERT INTO `plat_city` VALUES ('82', '320900', '盐城市', '320000');
INSERT INTO `plat_city` VALUES ('83', '321000', '扬州市', '320000');
INSERT INTO `plat_city` VALUES ('84', '321100', '镇江市', '320000');
INSERT INTO `plat_city` VALUES ('85', '321200', '泰州市', '320000');
INSERT INTO `plat_city` VALUES ('86', '321300', '宿迁市', '320000');
INSERT INTO `plat_city` VALUES ('87', '330100', '杭州市', '330000');
INSERT INTO `plat_city` VALUES ('88', '330200', '宁波市', '330000');
INSERT INTO `plat_city` VALUES ('89', '330300', '温州市', '330000');
INSERT INTO `plat_city` VALUES ('90', '330400', '嘉兴市', '330000');
INSERT INTO `plat_city` VALUES ('91', '330500', '湖州市', '330000');
INSERT INTO `plat_city` VALUES ('92', '330600', '绍兴市', '330000');
INSERT INTO `plat_city` VALUES ('93', '330700', '金华市', '330000');
INSERT INTO `plat_city` VALUES ('94', '330800', '衢州市', '330000');
INSERT INTO `plat_city` VALUES ('95', '330900', '舟山市', '330000');
INSERT INTO `plat_city` VALUES ('96', '331000', '台州市', '330000');
INSERT INTO `plat_city` VALUES ('97', '331100', '丽水市', '330000');
INSERT INTO `plat_city` VALUES ('98', '340100', '合肥市', '340000');
INSERT INTO `plat_city` VALUES ('99', '340200', '芜湖市', '340000');
INSERT INTO `plat_city` VALUES ('100', '340300', '蚌埠市', '340000');
INSERT INTO `plat_city` VALUES ('101', '340400', '淮南市', '340000');
INSERT INTO `plat_city` VALUES ('102', '340500', '马鞍山市', '340000');
INSERT INTO `plat_city` VALUES ('103', '340600', '淮北市', '340000');
INSERT INTO `plat_city` VALUES ('104', '340700', '铜陵市', '340000');
INSERT INTO `plat_city` VALUES ('105', '340800', '安庆市', '340000');
INSERT INTO `plat_city` VALUES ('106', '341000', '黄山市', '340000');
INSERT INTO `plat_city` VALUES ('107', '341100', '滁州市', '340000');
INSERT INTO `plat_city` VALUES ('108', '341200', '阜阳市', '340000');
INSERT INTO `plat_city` VALUES ('109', '341300', '宿州市', '340000');
INSERT INTO `plat_city` VALUES ('110', '341400', '巢湖市', '340000');
INSERT INTO `plat_city` VALUES ('111', '341500', '六安市', '340000');
INSERT INTO `plat_city` VALUES ('112', '341600', '亳州市', '340000');
INSERT INTO `plat_city` VALUES ('113', '341700', '池州市', '340000');
INSERT INTO `plat_city` VALUES ('114', '341800', '宣城市', '340000');
INSERT INTO `plat_city` VALUES ('115', '350100', '福州市', '350000');
INSERT INTO `plat_city` VALUES ('116', '350200', '厦门市', '350000');
INSERT INTO `plat_city` VALUES ('117', '350300', '莆田市', '350000');
INSERT INTO `plat_city` VALUES ('118', '350400', '三明市', '350000');
INSERT INTO `plat_city` VALUES ('119', '350500', '泉州市', '350000');
INSERT INTO `plat_city` VALUES ('120', '350600', '漳州市', '350000');
INSERT INTO `plat_city` VALUES ('121', '350700', '南平市', '350000');
INSERT INTO `plat_city` VALUES ('122', '350800', '龙岩市', '350000');
INSERT INTO `plat_city` VALUES ('123', '350900', '宁德市', '350000');
INSERT INTO `plat_city` VALUES ('124', '360100', '南昌市', '360000');
INSERT INTO `plat_city` VALUES ('125', '360200', '景德镇市', '360000');
INSERT INTO `plat_city` VALUES ('126', '360300', '萍乡市', '360000');
INSERT INTO `plat_city` VALUES ('127', '360400', '九江市', '360000');
INSERT INTO `plat_city` VALUES ('128', '360500', '新余市', '360000');
INSERT INTO `plat_city` VALUES ('129', '360600', '鹰潭市', '360000');
INSERT INTO `plat_city` VALUES ('130', '360700', '赣州市', '360000');
INSERT INTO `plat_city` VALUES ('131', '360800', '吉安市', '360000');
INSERT INTO `plat_city` VALUES ('132', '360900', '宜春市', '360000');
INSERT INTO `plat_city` VALUES ('133', '361000', '抚州市', '360000');
INSERT INTO `plat_city` VALUES ('134', '361100', '上饶市', '360000');
INSERT INTO `plat_city` VALUES ('135', '370100', '济南市', '370000');
INSERT INTO `plat_city` VALUES ('136', '370200', '青岛市', '370000');
INSERT INTO `plat_city` VALUES ('137', '370300', '淄博市', '370000');
INSERT INTO `plat_city` VALUES ('138', '370400', '枣庄市', '370000');
INSERT INTO `plat_city` VALUES ('139', '370500', '东营市', '370000');
INSERT INTO `plat_city` VALUES ('140', '370600', '烟台市', '370000');
INSERT INTO `plat_city` VALUES ('141', '370700', '潍坊市', '370000');
INSERT INTO `plat_city` VALUES ('142', '370800', '济宁市', '370000');
INSERT INTO `plat_city` VALUES ('143', '370900', '泰安市', '370000');
INSERT INTO `plat_city` VALUES ('144', '371000', '威海市', '370000');
INSERT INTO `plat_city` VALUES ('145', '371100', '日照市', '370000');
INSERT INTO `plat_city` VALUES ('146', '371200', '莱芜市', '370000');
INSERT INTO `plat_city` VALUES ('147', '371300', '临沂市', '370000');
INSERT INTO `plat_city` VALUES ('148', '371400', '德州市', '370000');
INSERT INTO `plat_city` VALUES ('149', '371500', '聊城市', '370000');
INSERT INTO `plat_city` VALUES ('150', '371600', '滨州市', '370000');
INSERT INTO `plat_city` VALUES ('151', '371700', '荷泽市', '370000');
INSERT INTO `plat_city` VALUES ('152', '410100', '郑州市', '410000');
INSERT INTO `plat_city` VALUES ('153', '410200', '开封市', '410000');
INSERT INTO `plat_city` VALUES ('154', '410300', '洛阳市', '410000');
INSERT INTO `plat_city` VALUES ('155', '410400', '平顶山市', '410000');
INSERT INTO `plat_city` VALUES ('156', '410500', '安阳市', '410000');
INSERT INTO `plat_city` VALUES ('157', '410600', '鹤壁市', '410000');
INSERT INTO `plat_city` VALUES ('158', '410700', '新乡市', '410000');
INSERT INTO `plat_city` VALUES ('159', '410800', '焦作市', '410000');
INSERT INTO `plat_city` VALUES ('160', '410900', '濮阳市', '410000');
INSERT INTO `plat_city` VALUES ('161', '411000', '许昌市', '410000');
INSERT INTO `plat_city` VALUES ('162', '411100', '漯河市', '410000');
INSERT INTO `plat_city` VALUES ('163', '411200', '三门峡市', '410000');
INSERT INTO `plat_city` VALUES ('164', '411300', '南阳市', '410000');
INSERT INTO `plat_city` VALUES ('165', '411400', '商丘市', '410000');
INSERT INTO `plat_city` VALUES ('166', '411500', '信阳市', '410000');
INSERT INTO `plat_city` VALUES ('167', '411600', '周口市', '410000');
INSERT INTO `plat_city` VALUES ('168', '411700', '驻马店市', '410000');
INSERT INTO `plat_city` VALUES ('169', '420100', '武汉市', '420000');
INSERT INTO `plat_city` VALUES ('170', '420200', '黄石市', '420000');
INSERT INTO `plat_city` VALUES ('171', '420300', '十堰市', '420000');
INSERT INTO `plat_city` VALUES ('172', '420500', '宜昌市', '420000');
INSERT INTO `plat_city` VALUES ('173', '420600', '襄樊市', '420000');
INSERT INTO `plat_city` VALUES ('174', '420700', '鄂州市', '420000');
INSERT INTO `plat_city` VALUES ('175', '420800', '荆门市', '420000');
INSERT INTO `plat_city` VALUES ('176', '420900', '孝感市', '420000');
INSERT INTO `plat_city` VALUES ('177', '421000', '荆州市', '420000');
INSERT INTO `plat_city` VALUES ('178', '421100', '黄冈市', '420000');
INSERT INTO `plat_city` VALUES ('179', '421200', '咸宁市', '420000');
INSERT INTO `plat_city` VALUES ('180', '421300', '随州市', '420000');
INSERT INTO `plat_city` VALUES ('181', '422800', '恩施土家族苗族自治州', '420000');
INSERT INTO `plat_city` VALUES ('182', '429000', '省直辖行政单位', '420000');
INSERT INTO `plat_city` VALUES ('183', '430100', '长沙市', '430000');
INSERT INTO `plat_city` VALUES ('184', '430200', '株洲市', '430000');
INSERT INTO `plat_city` VALUES ('185', '430300', '湘潭市', '430000');
INSERT INTO `plat_city` VALUES ('186', '430400', '衡阳市', '430000');
INSERT INTO `plat_city` VALUES ('187', '430500', '邵阳市', '430000');
INSERT INTO `plat_city` VALUES ('188', '430600', '岳阳市', '430000');
INSERT INTO `plat_city` VALUES ('189', '430700', '常德市', '430000');
INSERT INTO `plat_city` VALUES ('190', '430800', '张家界市', '430000');
INSERT INTO `plat_city` VALUES ('191', '430900', '益阳市', '430000');
INSERT INTO `plat_city` VALUES ('192', '431000', '郴州市', '430000');
INSERT INTO `plat_city` VALUES ('193', '431100', '永州市', '430000');
INSERT INTO `plat_city` VALUES ('194', '431200', '怀化市', '430000');
INSERT INTO `plat_city` VALUES ('195', '431300', '娄底市', '430000');
INSERT INTO `plat_city` VALUES ('196', '433100', '湘西土家族苗族自治州', '430000');
INSERT INTO `plat_city` VALUES ('197', '440100', '广州市', '440000');
INSERT INTO `plat_city` VALUES ('198', '440200', '韶关市', '440000');
INSERT INTO `plat_city` VALUES ('199', '440300', '深圳市', '440000');
INSERT INTO `plat_city` VALUES ('200', '440400', '珠海市', '440000');
INSERT INTO `plat_city` VALUES ('201', '440500', '汕头市', '440000');
INSERT INTO `plat_city` VALUES ('202', '440600', '佛山市', '440000');
INSERT INTO `plat_city` VALUES ('203', '440700', '江门市', '440000');
INSERT INTO `plat_city` VALUES ('204', '440800', '湛江市', '440000');
INSERT INTO `plat_city` VALUES ('205', '440900', '茂名市', '440000');
INSERT INTO `plat_city` VALUES ('206', '441200', '肇庆市', '440000');
INSERT INTO `plat_city` VALUES ('207', '441300', '惠州市', '440000');
INSERT INTO `plat_city` VALUES ('208', '441400', '梅州市', '440000');
INSERT INTO `plat_city` VALUES ('209', '441500', '汕尾市', '440000');
INSERT INTO `plat_city` VALUES ('210', '441600', '河源市', '440000');
INSERT INTO `plat_city` VALUES ('211', '441700', '阳江市', '440000');
INSERT INTO `plat_city` VALUES ('212', '441800', '清远市', '440000');
INSERT INTO `plat_city` VALUES ('213', '441900', '东莞市', '440000');
INSERT INTO `plat_city` VALUES ('214', '442000', '中山市', '440000');
INSERT INTO `plat_city` VALUES ('215', '445100', '潮州市', '440000');
INSERT INTO `plat_city` VALUES ('216', '445200', '揭阳市', '440000');
INSERT INTO `plat_city` VALUES ('217', '445300', '云浮市', '440000');
INSERT INTO `plat_city` VALUES ('218', '450100', '南宁市', '450000');
INSERT INTO `plat_city` VALUES ('219', '450200', '柳州市', '450000');
INSERT INTO `plat_city` VALUES ('220', '450300', '桂林市', '450000');
INSERT INTO `plat_city` VALUES ('221', '450400', '梧州市', '450000');
INSERT INTO `plat_city` VALUES ('222', '450500', '北海市', '450000');
INSERT INTO `plat_city` VALUES ('223', '450600', '防城港市', '450000');
INSERT INTO `plat_city` VALUES ('224', '450700', '钦州市', '450000');
INSERT INTO `plat_city` VALUES ('225', '450800', '贵港市', '450000');
INSERT INTO `plat_city` VALUES ('226', '450900', '玉林市', '450000');
INSERT INTO `plat_city` VALUES ('227', '451000', '百色市', '450000');
INSERT INTO `plat_city` VALUES ('228', '451100', '贺州市', '450000');
INSERT INTO `plat_city` VALUES ('229', '451200', '河池市', '450000');
INSERT INTO `plat_city` VALUES ('230', '451300', '来宾市', '450000');
INSERT INTO `plat_city` VALUES ('231', '451400', '崇左市', '450000');
INSERT INTO `plat_city` VALUES ('232', '460100', '海口市', '460000');
INSERT INTO `plat_city` VALUES ('233', '460200', '三亚市', '460000');
INSERT INTO `plat_city` VALUES ('235', '500100', '重庆市', '500000');
INSERT INTO `plat_city` VALUES ('238', '510100', '成都市', '510000');
INSERT INTO `plat_city` VALUES ('239', '510300', '自贡市', '510000');
INSERT INTO `plat_city` VALUES ('240', '510400', '攀枝花市', '510000');
INSERT INTO `plat_city` VALUES ('241', '510500', '泸州市', '510000');
INSERT INTO `plat_city` VALUES ('242', '510600', '德阳市', '510000');
INSERT INTO `plat_city` VALUES ('243', '510700', '绵阳市', '510000');
INSERT INTO `plat_city` VALUES ('244', '510800', '广元市', '510000');
INSERT INTO `plat_city` VALUES ('245', '510900', '遂宁市', '510000');
INSERT INTO `plat_city` VALUES ('246', '511000', '内江市', '510000');
INSERT INTO `plat_city` VALUES ('247', '511100', '乐山市', '510000');
INSERT INTO `plat_city` VALUES ('248', '511300', '南充市', '510000');
INSERT INTO `plat_city` VALUES ('249', '511400', '眉山市', '510000');
INSERT INTO `plat_city` VALUES ('250', '511500', '宜宾市', '510000');
INSERT INTO `plat_city` VALUES ('251', '511600', '广安市', '510000');
INSERT INTO `plat_city` VALUES ('252', '511700', '达州市', '510000');
INSERT INTO `plat_city` VALUES ('253', '511800', '雅安市', '510000');
INSERT INTO `plat_city` VALUES ('254', '511900', '巴中市', '510000');
INSERT INTO `plat_city` VALUES ('255', '512000', '资阳市', '510000');
INSERT INTO `plat_city` VALUES ('256', '513200', '阿坝藏族羌族自治州', '510000');
INSERT INTO `plat_city` VALUES ('257', '513300', '甘孜藏族自治州', '510000');
INSERT INTO `plat_city` VALUES ('258', '513400', '凉山彝族自治州', '510000');
INSERT INTO `plat_city` VALUES ('259', '520100', '贵阳市', '520000');
INSERT INTO `plat_city` VALUES ('260', '520200', '六盘水市', '520000');
INSERT INTO `plat_city` VALUES ('261', '520300', '遵义市', '520000');
INSERT INTO `plat_city` VALUES ('262', '520400', '安顺市', '520000');
INSERT INTO `plat_city` VALUES ('263', '522200', '铜仁地区', '520000');
INSERT INTO `plat_city` VALUES ('264', '522300', '黔西南布依族苗族自治州', '520000');
INSERT INTO `plat_city` VALUES ('265', '522400', '毕节地区', '520000');
INSERT INTO `plat_city` VALUES ('266', '522600', '黔东南苗族侗族自治州', '520000');
INSERT INTO `plat_city` VALUES ('267', '522700', '黔南布依族苗族自治州', '520000');
INSERT INTO `plat_city` VALUES ('268', '530100', '昆明市', '530000');
INSERT INTO `plat_city` VALUES ('269', '530300', '曲靖市', '530000');
INSERT INTO `plat_city` VALUES ('270', '530400', '玉溪市', '530000');
INSERT INTO `plat_city` VALUES ('271', '530500', '保山市', '530000');
INSERT INTO `plat_city` VALUES ('272', '530600', '昭通市', '530000');
INSERT INTO `plat_city` VALUES ('273', '530700', '丽江市', '530000');
INSERT INTO `plat_city` VALUES ('274', '530800', '思茅市', '530000');
INSERT INTO `plat_city` VALUES ('275', '530900', '临沧市', '530000');
INSERT INTO `plat_city` VALUES ('276', '532300', '楚雄彝族自治州', '530000');
INSERT INTO `plat_city` VALUES ('277', '532500', '红河哈尼族彝族自治州', '530000');
INSERT INTO `plat_city` VALUES ('278', '532600', '文山壮族苗族自治州', '530000');
INSERT INTO `plat_city` VALUES ('279', '532800', '西双版纳傣族自治州', '530000');
INSERT INTO `plat_city` VALUES ('280', '532900', '大理白族自治州', '530000');
INSERT INTO `plat_city` VALUES ('281', '533100', '德宏傣族景颇族自治州', '530000');
INSERT INTO `plat_city` VALUES ('282', '533300', '怒江傈僳族自治州', '530000');
INSERT INTO `plat_city` VALUES ('283', '533400', '迪庆藏族自治州', '530000');
INSERT INTO `plat_city` VALUES ('284', '540100', '拉萨市', '540000');
INSERT INTO `plat_city` VALUES ('285', '542100', '昌都地区', '540000');
INSERT INTO `plat_city` VALUES ('286', '542200', '山南地区', '540000');
INSERT INTO `plat_city` VALUES ('287', '542300', '日喀则地区', '540000');
INSERT INTO `plat_city` VALUES ('288', '542400', '那曲地区', '540000');
INSERT INTO `plat_city` VALUES ('289', '542500', '阿里地区', '540000');
INSERT INTO `plat_city` VALUES ('290', '542600', '林芝地区', '540000');
INSERT INTO `plat_city` VALUES ('291', '610100', '西安市', '610000');
INSERT INTO `plat_city` VALUES ('292', '610200', '铜川市', '610000');
INSERT INTO `plat_city` VALUES ('293', '610300', '宝鸡市', '610000');
INSERT INTO `plat_city` VALUES ('294', '610400', '咸阳市', '610000');
INSERT INTO `plat_city` VALUES ('295', '610500', '渭南市', '610000');
INSERT INTO `plat_city` VALUES ('296', '610600', '延安市', '610000');
INSERT INTO `plat_city` VALUES ('297', '610700', '汉中市', '610000');
INSERT INTO `plat_city` VALUES ('298', '610800', '榆林市', '610000');
INSERT INTO `plat_city` VALUES ('299', '610900', '安康市', '610000');
INSERT INTO `plat_city` VALUES ('300', '611000', '商洛市', '610000');
INSERT INTO `plat_city` VALUES ('301', '620100', '兰州市', '620000');
INSERT INTO `plat_city` VALUES ('302', '620200', '嘉峪关市', '620000');
INSERT INTO `plat_city` VALUES ('303', '620300', '金昌市', '620000');
INSERT INTO `plat_city` VALUES ('304', '620400', '白银市', '620000');
INSERT INTO `plat_city` VALUES ('305', '620500', '天水市', '620000');
INSERT INTO `plat_city` VALUES ('306', '620600', '武威市', '620000');
INSERT INTO `plat_city` VALUES ('307', '620700', '张掖市', '620000');
INSERT INTO `plat_city` VALUES ('308', '620800', '平凉市', '620000');
INSERT INTO `plat_city` VALUES ('309', '620900', '酒泉市', '620000');
INSERT INTO `plat_city` VALUES ('310', '621000', '庆阳市', '620000');
INSERT INTO `plat_city` VALUES ('311', '621100', '定西市', '620000');
INSERT INTO `plat_city` VALUES ('312', '621200', '陇南市', '620000');
INSERT INTO `plat_city` VALUES ('313', '622900', '临夏回族自治州', '620000');
INSERT INTO `plat_city` VALUES ('314', '623000', '甘南藏族自治州', '620000');
INSERT INTO `plat_city` VALUES ('315', '630100', '西宁市', '630000');
INSERT INTO `plat_city` VALUES ('316', '632100', '海东地区', '630000');
INSERT INTO `plat_city` VALUES ('317', '632200', '海北藏族自治州', '630000');
INSERT INTO `plat_city` VALUES ('318', '632300', '黄南藏族自治州', '630000');
INSERT INTO `plat_city` VALUES ('319', '632500', '海南藏族自治州', '630000');
INSERT INTO `plat_city` VALUES ('320', '632600', '果洛藏族自治州', '630000');
INSERT INTO `plat_city` VALUES ('321', '632700', '玉树藏族自治州', '630000');
INSERT INTO `plat_city` VALUES ('322', '632800', '海西蒙古族藏族自治州', '630000');
INSERT INTO `plat_city` VALUES ('323', '640100', '银川市', '640000');
INSERT INTO `plat_city` VALUES ('324', '640200', '石嘴山市', '640000');
INSERT INTO `plat_city` VALUES ('325', '640300', '吴忠市', '640000');
INSERT INTO `plat_city` VALUES ('326', '640400', '固原市', '640000');
INSERT INTO `plat_city` VALUES ('327', '640500', '中卫市', '640000');
INSERT INTO `plat_city` VALUES ('328', '650100', '乌鲁木齐市', '650000');
INSERT INTO `plat_city` VALUES ('329', '650200', '克拉玛依市', '650000');
INSERT INTO `plat_city` VALUES ('330', '652100', '吐鲁番地区', '650000');
INSERT INTO `plat_city` VALUES ('331', '652200', '哈密地区', '650000');
INSERT INTO `plat_city` VALUES ('332', '652300', '昌吉回族自治州', '650000');
INSERT INTO `plat_city` VALUES ('333', '652700', '博尔塔拉蒙古自治州', '650000');
INSERT INTO `plat_city` VALUES ('334', '652800', '巴音郭楞蒙古自治州', '650000');
INSERT INTO `plat_city` VALUES ('335', '652900', '阿克苏地区', '650000');
INSERT INTO `plat_city` VALUES ('336', '653000', '克孜勒苏柯尔克孜自治州', '650000');
INSERT INTO `plat_city` VALUES ('337', '653100', '喀什地区', '650000');
INSERT INTO `plat_city` VALUES ('338', '653200', '和田地区', '650000');
INSERT INTO `plat_city` VALUES ('339', '654000', '伊犁哈萨克自治州', '650000');
INSERT INTO `plat_city` VALUES ('340', '654200', '塔城地区', '650000');
INSERT INTO `plat_city` VALUES ('341', '654300', '阿勒泰地区', '650000');
INSERT INTO `plat_city` VALUES ('342', '659000', '省直辖行政单位', '650000');
INSERT INTO `plat_city` VALUES ('343', '120100', '天津市', '120000');

-- ----------------------------
-- Table structure for plat_province
-- ----------------------------
DROP TABLE IF EXISTS `plat_province`;
CREATE TABLE `plat_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='级联表省份';

-- ----------------------------
-- Records of plat_province
-- ----------------------------
INSERT INTO `plat_province` VALUES ('1', '110000', '北京市');
INSERT INTO `plat_province` VALUES ('2', '120000', '天津市');
INSERT INTO `plat_province` VALUES ('3', '130000', '河北省');
INSERT INTO `plat_province` VALUES ('4', '140000', '山西省');
INSERT INTO `plat_province` VALUES ('5', '150000', '内蒙古');
INSERT INTO `plat_province` VALUES ('6', '210000', '辽宁省');
INSERT INTO `plat_province` VALUES ('7', '220000', '吉林省');
INSERT INTO `plat_province` VALUES ('8', '230000', '黑龙江');
INSERT INTO `plat_province` VALUES ('9', '310000', '上海市');
INSERT INTO `plat_province` VALUES ('10', '320000', '江苏省');
INSERT INTO `plat_province` VALUES ('11', '330000', '浙江省');
INSERT INTO `plat_province` VALUES ('12', '340000', '安徽省');
INSERT INTO `plat_province` VALUES ('13', '350000', '福建省');
INSERT INTO `plat_province` VALUES ('14', '360000', '江西省');
INSERT INTO `plat_province` VALUES ('15', '370000', '山东省');
INSERT INTO `plat_province` VALUES ('16', '410000', '河南省');
INSERT INTO `plat_province` VALUES ('17', '420000', '湖北省');
INSERT INTO `plat_province` VALUES ('18', '430000', '湖南省');
INSERT INTO `plat_province` VALUES ('19', '440000', '广东省');
INSERT INTO `plat_province` VALUES ('20', '450000', '广  西');
INSERT INTO `plat_province` VALUES ('21', '460000', '海南省');
INSERT INTO `plat_province` VALUES ('22', '500000', '重庆市');
INSERT INTO `plat_province` VALUES ('23', '510000', '四川省');
INSERT INTO `plat_province` VALUES ('24', '520000', '贵州省');
INSERT INTO `plat_province` VALUES ('25', '530000', '云南省');
INSERT INTO `plat_province` VALUES ('26', '540000', '西  藏');
INSERT INTO `plat_province` VALUES ('27', '610000', '陕西省');
INSERT INTO `plat_province` VALUES ('28', '620000', '甘肃省');
INSERT INTO `plat_province` VALUES ('29', '630000', '青海省');
INSERT INTO `plat_province` VALUES ('30', '640000', '宁  夏');
INSERT INTO `plat_province` VALUES ('31', '650000', '新  疆');
INSERT INTO `plat_province` VALUES ('32', '710000', '台湾省');
INSERT INTO `plat_province` VALUES ('33', '810000', '香  港');
INSERT INTO `plat_province` VALUES ('34', '820000', '澳  门');

-- ----------------------------
-- Table structure for plat_resource
-- ----------------------------
DROP TABLE IF EXISTS `plat_resource`;
CREATE TABLE `plat_resource` (
  `resourceId` int(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `componentid` varchar(255) NOT NULL DEFAULT '' COMMENT '组件id',
  `componentname` varchar(255) NOT NULL DEFAULT '' COMMENT '组件名称',
  `parentid` varchar(255) NOT NULL DEFAULT '' COMMENT '父组件id',
  `resourceUrl` varchar(255) NOT NULL DEFAULT '' COMMENT '资源路径',
  `type` int(2) NOT NULL DEFAULT '1' COMMENT '资源类型(0:菜单;1:路径;2:按钮)',
  `createdTime` datetime NOT NULL COMMENT '创建时间',
  `status` int(3) NOT NULL DEFAULT '1' COMMENT '状态(1:有效；0：无效)',
  PRIMARY KEY (`resourceId`)
) ENGINE=MyISAM AUTO_INCREMENT=1573001002 DEFAULT CHARSET=utf8 COMMENT='角色资源信息表';

-- ----------------------------
-- Records of plat_resource
-- ----------------------------
INSERT INTO `plat_resource` VALUES ('1068009001', 'jueseguanli', '角色管理', 'xitongguanli', '', '0', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068008001', 'yonghuguanli', '用户管理', 'xitongguanli', '', '0', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068007001', 'xitongguanli', '系统管理', '', '', '0', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('921001001', 'yingyeetongji', '营业额统计', 'tongjiguanli', '', '0', '2015-09-22 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('919002001', 'dingdantongji', '订单统计', 'tongjiguanli', '', '0', '2015-09-21 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('921002001', 'guketongji', '顾客统计', 'tongjiguanli', '', '0', '2015-09-22 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('921003001', 'xiaoshouliangtongji', '销售量统计', 'tongjiguanli', '', '0', '2015-09-22 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('921004001', 'tongjiguanli', '统计管理', '', '', '0', '2015-09-22 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('947001001', 'dingdanguanli', '订单管理', '', '', '0', '2015-09-22 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('947002001', 'chakandingdan', '查看订单', 'dingdanguanli', '', '0', '2015-09-22 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('947003001', 'xiaoliangtongji', '销量统计-订单', 'dingdanguanli', '', '0', '2015-09-22 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('947004001', 'caigouhuizong', '采购汇总', 'dingdanguanli', '', '0', '2015-09-22 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992002001', 'dianpuguanli', '店铺管理', '', '', '0', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992003001', 'dianpuguanli-zi', '店铺管理', 'dianpuguanli', '', '0', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992004001', 'xinjiandianpu', '新建店铺', 'dianpuguanli-zi', '', '2', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992005001', 'xiugaidianpu', '修改店铺', 'dianpuguanli-zi', '', '2', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992006001', 'shanchudianpu', '删除店铺', 'dianpuguanli-zi', '', '2', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992007001', 'shangpinguanli', '商品管理', '', '', '0', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992008001', 'shangpinguanli-zi', '商品管理', 'shangpinguanli', '', '0', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992009001', 'xinjianshangpin', '新建商品', 'shangpinguanli-zi', '', '2', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992010001', 'xiugaishangpin', '修改商品', 'shangpinguanli-zi', '', '2', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992011001', 'shanchushangpin', '删除商品', 'shangpinguanli-zi', '', '2', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('992012001', 'huodongguanli', '活动管理', '', '', '0', '2015-09-23 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068001001', 'chakancuxiao', '查看促销', 'huodongguanli', '', '0', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068002001', 'xinjiancuxiao', '新建促销', 'chakancuxiao', '', '2', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068003001', 'xiugaicuxiao', '修改促销', 'chakancuxiao', '', '2', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068004001', 'shanchucuxiao', '删除促销', 'chakancuxiao', '', '2', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068005001', 'shengchengcoupon', '生成优惠卷', 'huodongguanli', '', '0', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068006001', 'fafangcoupon', '发放优惠卷', 'huodongguanli', '', '0', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068010001', 'quanxianguanli', '权限管理', 'xitongguanli', '', '0', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1068011001', 'ziyuanguanli', '资源管理', 'xitongguanli', '', '0', '2015-09-24 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1314003001', 'shangpinfenlei-zi', '商品分类', 'shangpinguanli', '', '0', '2015-10-09 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1316001001', 'gukeguanli', '顾客管理', '', '', '0', '2015-10-09 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1316002001', 'gukeguanli-zi', '顾客管理', 'gukeguanli', '', '0', '2015-10-09 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1316003001', 'biaoqianguanli', '标签管理', 'gukeguanli', '', '0', '2015-10-09 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496001001', 'fuzhidianpu', '复制店铺', 'dianpuguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496002001', 'cuxiaohuodong', '促销活动', 'shangpinguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496003001', 'daochushangpin', '导出商品', 'shangpinguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496004001', 'daorushangpin', '导入商品', 'shangpinguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496005001', 'zhifuchenggong', '支付成功', 'chakandingdan', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496006001', 'yiziti', '已自提', 'chakandingdan', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496007001', 'chaoshishixiao', '超时失效', 'chakandingdan', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496008001', 'yiquxiao', '已取消', 'chakandingdan', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496009001', 'daochudingdan', '导出订单', 'chakandingdan', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496010001', 'xinjiancoupon', '新建优惠卷', 'shengchengcoupon', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496011001', 'xiugaicoupon', '修改优惠卷', 'shengchengcoupon', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496012001', 'shanchucoupon', '删除优惠卷', 'shengchengcoupon', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496013001', 'fafangcouponself', '发放优惠卷', 'fafangcoupon', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496014001', 'fafangcouponall', '全顾客发放优惠卷', 'fafangcoupon', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496015001', 'shanchucoupon', '批量删除优惠卷', 'fafangcoupon', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496016001', 'xiugaiguke', '修改顾客', 'gukeguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496017001', 'shanchuguke', '删除顾客', 'gukeguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496018001', 'addbacklist', '加入黑名单', 'gukeguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496019001', 'quxiaobacklist', '取消黑名单', 'gukeguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496020001', 'xuanzebiaoqian', '选择标签', 'gukeguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496021001', 'daochugukelist', '导出顾客列表', 'gukeguanli-zi', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496022001', 'xinjianbiaoqian', '新建标签', 'biaoqianguanli', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496023001', 'xiugaibiaoqian', '修改标签', 'biaoqianguanli', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1496024001', 'shanchubiaoqian', '删除标签', 'biaoqianguanli', '', '2', '2015-10-19 00:00:00', '1');
INSERT INTO `plat_resource` VALUES ('1573001001', 'dianyuanchakan', '店员查看', 'dianpuguanli-zi', '', '2', '2015-10-22 00:00:00', '1');

-- ----------------------------
-- Table structure for plat_role
-- ----------------------------
DROP TABLE IF EXISTS `plat_role`;
CREATE TABLE `plat_role` (
  `roleId` int(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT '角色名称',
  `description` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '描述信息',
  `createdTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifiedTime` datetime DEFAULT NULL COMMENT '修改时间',
  `modifiedCount` int(11) DEFAULT '0' COMMENT '修改次数',
  `status` int(2) DEFAULT '1' COMMENT '状态信息',
  PRIMARY KEY (`roleId`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=gbk ROW_FORMAT=FIXED COMMENT='系统角色表';

-- ----------------------------
-- Records of plat_role
-- ----------------------------
INSERT INTO `plat_role` VALUES ('1', '超级管理员', '超级管理员', '2007-01-17 10:49:22', '2007-01-17 12:01:05', '1', '1');
INSERT INTO `plat_role` VALUES ('67', '公司运营', '公司运营', '2015-09-25 14:51:38', null, '0', '1');
INSERT INTO `plat_role` VALUES ('66', '农业公司采购员', '农业公司采购员', '2015-09-25 14:48:09', null, '0', '1');
INSERT INTO `plat_role` VALUES ('65', '仓库分拣员', '仓库分拣员', '2015-09-25 14:44:29', null, '0', '1');
INSERT INTO `plat_role` VALUES ('64', '店员', '店员', '2015-09-25 11:18:49', null, '0', '1');
INSERT INTO `plat_role` VALUES ('63', '店长', '自提点或店铺负责人', '2015-09-23 09:52:35', null, '0', '1');
INSERT INTO `plat_role` VALUES ('62', '城市运营经理', '城市运营经理', '2015-09-18 17:23:18', null, '0', '1');

-- ----------------------------
-- Table structure for plat_roleauthority
-- ----------------------------
DROP TABLE IF EXISTS `plat_roleauthority`;
CREATE TABLE `plat_roleauthority` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `roleId` int(20) NOT NULL COMMENT '角色id',
  `authorityId` int(20) NOT NULL COMMENT '权限id',
  `createdTime` datetime NOT NULL COMMENT '创建时间',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='角色资源关联表';

-- ----------------------------
-- Records of plat_roleauthority
-- ----------------------------
INSERT INTO `plat_roleauthority` VALUES ('1', '1', '1', '2015-08-13 15:43:09', '1');
INSERT INTO `plat_roleauthority` VALUES ('2', '1', '2', '2015-08-20 10:36:07', '1');
INSERT INTO `plat_roleauthority` VALUES ('6', '62', '5', '2015-09-21 16:19:17', '1');
INSERT INTO `plat_roleauthority` VALUES ('7', '62', '6', '2015-09-22 17:45:23', '1');
INSERT INTO `plat_roleauthority` VALUES ('8', '63', '9', '2015-09-23 14:43:33', '1');
INSERT INTO `plat_roleauthority` VALUES ('12', '63', '12', '2015-09-24 17:49:02', '1');
INSERT INTO `plat_roleauthority` VALUES ('10', '63', '11', '2015-09-23 14:49:03', '1');
INSERT INTO `plat_roleauthority` VALUES ('11', '1', '3', '2015-09-24 17:17:44', '1');
INSERT INTO `plat_roleauthority` VALUES ('23', '63', '16', '2015-09-28 17:52:16', '1');
INSERT INTO `plat_roleauthority` VALUES ('14', '64', '13', '2015-09-25 14:03:19', '1');
INSERT INTO `plat_roleauthority` VALUES ('15', '64', '14', '2015-09-25 14:16:33', '1');
INSERT INTO `plat_roleauthority` VALUES ('22', '64', '16', '2015-09-28 17:52:08', '1');
INSERT INTO `plat_roleauthority` VALUES ('17', '65', '7', '2015-09-25 14:47:03', '1');
INSERT INTO `plat_roleauthority` VALUES ('18', '66', '8', '2015-09-25 14:50:11', '1');
INSERT INTO `plat_roleauthority` VALUES ('19', '67', '10', '2015-09-25 14:53:24', '1');
INSERT INTO `plat_roleauthority` VALUES ('20', '67', '11', '2015-09-25 14:54:52', '1');
INSERT INTO `plat_roleauthority` VALUES ('21', '67', '15', '2015-09-25 14:59:33', '1');
INSERT INTO `plat_roleauthority` VALUES ('26', '67', '5', '2015-10-19 11:26:07', '1');
INSERT INTO `plat_roleauthority` VALUES ('25', '67', '17', '2015-10-09 17:44:29', '1');
INSERT INTO `plat_roleauthority` VALUES ('27', '67', '6', '2015-10-19 11:26:12', '1');
INSERT INTO `plat_roleauthority` VALUES ('29', '67', '19', '2015-10-21 16:21:17', '1');

-- ----------------------------
-- Table structure for plat_saleway
-- ----------------------------
DROP TABLE IF EXISTS `plat_saleway`;
CREATE TABLE `plat_saleway` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `saleway_name` varchar(255) NOT NULL COMMENT '促销方式名称 x-like',
  `begindate` date DEFAULT NULL COMMENT '开始日期',
  `enddate` date DEFAULT NULL COMMENT '结束日期',
  `begintime` time DEFAULT NULL COMMENT '开始时间',
  `endtime` time DEFAULT NULL COMMENT '结束时间',
  `details` varchar(1000) DEFAULT NULL COMMENT '促销详情',
  `giveNum` int(5) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='促销方式信息表';

-- ----------------------------
-- Records of plat_saleway
-- ----------------------------

-- ----------------------------
-- Table structure for plat_user
-- ----------------------------
DROP TABLE IF EXISTS `plat_user`;
CREATE TABLE `plat_user` (
  `id` bigint(20) NOT NULL COMMENT '自增主键',
  `username` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '用户名|x-like',
  `password` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '密码',
  `createdTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifiedTime` datetime DEFAULT NULL COMMENT '修改时间',
  `status` int(3) DEFAULT '1' COMMENT '当前状态',
  `description` varchar(200) CHARACTER SET utf8 DEFAULT '' COMMENT '描述信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=gbk ROW_FORMAT=FIXED COMMENT='系统用户表';

-- ----------------------------
-- Records of plat_user
-- ----------------------------

-- ----------------------------
-- Table structure for plat_userrole
-- ----------------------------
DROP TABLE IF EXISTS `plat_userrole`;
CREATE TABLE `plat_userrole` (
  `userRoleId` int(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `userId` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `roleId` int(20) DEFAULT NULL COMMENT '角色Id',
  `createdTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`userRoleId`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=gbk ROW_FORMAT=FIXED COMMENT='用户角色关系表';

-- ----------------------------
-- Records of plat_userrole
-- ----------------------------
INSERT INTO `plat_userrole` VALUES ('117', '1', '1', '2015-10-26 15:51:47');
INSERT INTO `plat_userrole` VALUES ('118', '1596001001', '67', '2015-10-26 15:52:03');

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` bigint(20) NOT NULL,
  `name` varchar(20) DEFAULT NULL COMMENT '学生姓名 x-like',
  `age` int(20) DEFAULT NULL COMMENT '年龄',
  `bir` datetime DEFAULT NULL COMMENT '生日',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别 x-s',
  `addrss` varchar(50) DEFAULT NULL COMMENT '家庭住址 x-like',
  `mi` tinyint(2) DEFAULT NULL COMMENT '是否已婚 x-s',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='教师管理';

-- ----------------------------
-- Records of teacher
-- ----------------------------

-- ----------------------------
-- Table structure for wx_customer
-- ----------------------------
DROP TABLE IF EXISTS `wx_customer`;
CREATE TABLE `wx_customer` (
  `openId` varchar(100) NOT NULL COMMENT '顾客的微信号',
  `wxname` varchar(64) DEFAULT NULL COMMENT '微信名称',
  `name` varchar(64) DEFAULT NULL COMMENT '顾客姓名 x-like',
  `phone` varchar(20) DEFAULT NULL COMMENT '顾客手机号 x-like',
  `labelId` varchar(255) DEFAULT NULL COMMENT '标签',
  `sex` varchar(4) DEFAULT NULL COMMENT '性别 x-s',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `country` varchar(10) DEFAULT NULL COMMENT '国家代码',
  `province` varchar(20) DEFAULT NULL COMMENT '省份代码',
  `city` varchar(20) DEFAULT NULL COMMENT '城市代码',
  `longitude` double(10,7) DEFAULT NULL COMMENT '客户纬度',
  `latitude` double(10,7) DEFAULT NULL COMMENT '客户纬度',
  `isBlack` int(3) DEFAULT '0' COMMENT '是否为黑名单 x-s',
  `status` int(3) DEFAULT '1' COMMENT '状态 x-s',
  `ownerId` bigint(20) DEFAULT NULL COMMENT '注册商户的Id',
  `isDelete` int(3) DEFAULT NULL COMMENT '是否删除 x-s',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifiedTime` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` mediumtext COMMENT '备注信息',
  PRIMARY KEY (`openId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='顾客表';

-- ----------------------------
-- Records of wx_customer
-- ----------------------------
INSERT INTO `wx_customer` VALUES ('144-2225522639000', '65656', '6565656', '5656565', 'normal', '0', '656565', '656565', '65656', '56565', '999.9999999', '656.0000000', '0', '1', '2', '0', '2015-09-14 18:12:02', '2015-09-14 18:12:02', '565');
INSERT INTO `wx_customer` VALUES ('1441789943979000', '1', '1', '1', 'vip;svip', '1', '1', '1', '1', '1', '1.0000000', '1.0000000', '0', '1', '1', '1', '2015-09-24 00:00:00', '2015-09-04 00:00:00', '1');
INSERT INTO `wx_customer` VALUES ('1442196899633000', '123', '123', '123', 'xxx', '0', '123', '123', '123', '123', '123.0000000', '123.0000000', '0', '1', '1', '1', '2015-09-14 10:14:59', '2015-09-14 14:32:22', '123');
INSERT INTO `wx_customer` VALUES ('1442210935044000', '9000', '0000', '000', 'svip;ppoo', '1', '0000', '0', '0', '0', '0.0000000', '0.0000000', '0', '1', '1', '1', '2015-09-14 14:08:55', '2015-09-14 14:38:57', '000');
INSERT INTO `wx_customer` VALUES ('1442210935044001', '111', '111', '111', 'xxx', '1', '111', '111', '111', '111', '111.0000000', '111.0000000', '0', '1', '1', '1', '2015-09-14 14:09:24', '2015-09-14 14:38:22', '111');
INSERT INTO `wx_customer` VALUES ('1442211755289000', '222', '22', '22', 'normal', '男', '22', '22', '22', '22', '22.0000000', '22.0000000', '0', '1', '1', '0', '2015-09-10 14:22:35', '2015-09-16 18:37:47', '22');
INSERT INTO `wx_customer` VALUES ('1442212840544000', '444', '44', '44', 'svip;normal', '女', '44', '44', '44', '44', '44.0000000', '44.0000000', '1', '1', '1', '0', '2015-09-14 14:40:40', '2015-09-17 11:20:01', '44');
INSERT INTO `wx_customer` VALUES ('1442224537451000', '747474', '74747', '474747', 'normal', '1', '47474', '47474', '4747', '47747', '999.9999999', '999.9999999', '0', '1', '1', '1', '2015-09-14 17:55:37', '2015-09-14 17:55:37', '4747');
INSERT INTO `wx_customer` VALUES ('oHxmUjg1X-G5SPAhY-RA8Lzg68-w', '张东涛', '张东涛', '', null, '男', null, '中国', '北京', '昌平', '116.3835370', '39.9706880', '0', '1', null, '0', '2015-10-14 12:38:47', '2015-10-23 12:38:52', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjgGmjdL6EWfMWceKBRZ-ifU', '王杰', '王杰', null, null, '女', null, '中国', '', '', '116.3837050', '39.9707030', '0', '1', null, '0', '2015-10-07 12:38:55', '2015-10-23 12:38:59', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjh5q1WPrBFgyOj2POYLsivs', 'wfs', 'wfs', null, null, '男', null, '中国', '北京', '海淀', null, null, '0', '1', null, '0', '2015-10-20 12:39:03', '2015-10-26 12:39:06', null);
INSERT INTO `wx_customer` VALUES ('oHxmUji9MHS5srl-oItImOQyCNmg', '赵鹏飞', '赵鹏飞', '135～：*%^&*', null, '男', null, '中国', '北京', '海淀', '116.3835830', '39.9705920', '0', '1', null, '0', '2015-10-13 12:39:10', '2015-10-23 12:39:16', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjmsQj8Uz3EJNdqMk_V9R7EI', '阿泰', '阿泰', '13720041255', null, '男', null, '中国', '山东', '潍坊', '116.3835070', '39.9706840', '0', '1', null, '0', '2015-10-06 12:39:20', '2015-10-13 12:39:24', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjniWdzxXOgCLNL8ze0rMOro', '雷_Jin', '雷_Jin', null, null, '女', null, '中国', '北京', '朝阳', '116.3835750', '39.9706950', '0', '1', null, '0', '2015-10-07 12:39:28', '2015-10-22 12:39:31', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjnk121zB2qg6PA3fbvnO_O8', '雅', '雅', null, null, '女', null, '中国', '', '', '116.3089600', '39.9835970', '0', '1', null, '0', '2015-10-20 15:36:40', '2015-10-21 12:39:45', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjnNzMEArNmPy8tZOh8S-XC0', '雷δJin', '雷δJin', null, null, '女', null, '中国', '', '', '116.3090820', '39.9836920', '0', '1', null, '0', '2015-10-20 11:51:51', '2015-10-22 12:39:51', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjnusafswL-b32qKWiApYoUo', '冰影御风', '冰影御风', null, null, '男', null, '中国', '北京', '东城', '116.2916560', '40.1421780', '0', '1', null, '0', '2015-10-19 12:39:55', '2015-10-22 12:40:21', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjocGzEUcKhQ37vN3MDWi1Mg', '李伟', '李伟', null, null, '男', null, '中国', '北京', '海淀', '116.3101200', '39.9843030', '0', '1', null, '0', '2015-10-20 12:40:02', '2015-10-22 12:40:27', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjoQ75MuhAhpwNJH9b1v3EZ0', '叶丹', '叶丹', null, null, '女', null, '中国', '北京', '海淀', '116.3094940', '39.9838330', '0', '1', null, '0', '2015-10-20 16:34:41', '2015-10-21 12:40:31', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjoxTqAuZzBwZIEMkEq2TaUY', '非我所愿水天一色沧海一粟海绵宝宝', '非我所愿水天一色沧海一粟海绵宝宝', '15210849289', null, '男', null, '中国', '北京', '海淀', '116.3836590', '39.9707570', '0', '1', null, '0', '2015-10-06 12:40:07', '2015-10-22 12:40:38', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjpMj9Yu1AbgR-sV9Mw_vkPk', 'Lee春雨', 'Lee春雨', null, null, '女', null, '中国', '北京', '朝阳', '116.3090590', '39.9836430', '0', '1', null, '0', '2015-10-20 12:40:12', '2015-10-20 12:40:41', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjsLN27LoZEc8PLSIZIZO39E', '么么李宝贝', '么么李宝贝', null, null, '男', null, '中国', '河南', '商丘', '116.3835680', '39.9704860', '0', '1', null, '0', '2015-10-14 12:40:16', '2015-10-21 12:40:44', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjt0HDbh08gkqYH4hylzNNww', '皮擦擦', '皮擦擦', null, null, '男', null, '中国', '', '', null, null, '0', '1', null, '0', '2015-10-20 14:02:21', '2015-10-23 12:40:54', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjtl58_o48UZQg9v9Br_GCA4', '    Derek', '    Derek', null, null, '男', null, '中国', '北京', '海淀', '116.3091510', '39.9836770', '0', '1', null, '0', '2015-10-21 11:16:39', '2015-10-23 12:40:57', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjtV60Kz4URNB3sVqDc20T-s', 'Li', 'Li', null, null, '男', null, '中国', '陕西', '渭南', '116.3880840', '39.9745370', '0', '1', null, '0', '2015-10-23 12:44:44', '2015-10-23 13:32:28', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjuajtxanJYK-2IH_s1Ed4Tw', '言午金三 郎', '言午金三 郎', null, null, '男', null, '中国', '北京', '海淀', '116.3836670', '39.9708100', '0', '1', null, '0', '2015-10-20 19:19:49', '2015-10-23 12:41:01', null);
INSERT INTO `wx_customer` VALUES ('oHxmUjvKkaLrY5doi4aZXEoUH4ZQ', '言午金三郎', '言午金三郎', null, null, '女', null, '中国', '', '', '116.3337480', '39.9994770', '0', '1', null, '0', '2015-10-14 12:41:03', '2015-10-23 12:41:07', null);

-- ----------------------------
-- Table structure for wx_customer_label
-- ----------------------------
DROP TABLE IF EXISTS `wx_customer_label`;
CREATE TABLE `wx_customer_label` (
  `labelId` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '标签的Id',
  `labelname` varchar(64) DEFAULT NULL COMMENT '标签名称',
  `status` int(3) DEFAULT NULL COMMENT '状态 x-s',
  `isDelete` int(3) DEFAULT NULL COMMENT '是否删除 x-s',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifiedTime` datetime DEFAULT NULL COMMENT '修改时间',
  `ownerId` bigint(20) NOT NULL COMMENT '注册商户的ID',
  PRIMARY KEY (`labelId`)
) ENGINE=InnoDB AUTO_INCREMENT=1444642348287002 DEFAULT CHARSET=utf8 COMMENT='顾客的标签表';

-- ----------------------------
-- Records of wx_customer_label
-- ----------------------------

-- ----------------------------
-- Table structure for wx_customer_property
-- ----------------------------
DROP TABLE IF EXISTS `wx_customer_property`;
CREATE TABLE `wx_customer_property` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '表主键',
  `openId` varchar(64) NOT NULL COMMENT '顾客的Id',
  `propertyName` varchar(100) DEFAULT NULL COMMENT '属性名',
  `propertyValue` varchar(100) DEFAULT NULL COMMENT '属性值',
  `status` int(3) DEFAULT NULL COMMENT '状态(1:有效、0:无效)',
  `isDelete` int(3) DEFAULT NULL COMMENT 'd是否删除(1:是、0:否)',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `ownerId` bigint(20) DEFAULT NULL COMMENT '注册商户的ID',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='顾客自定义表';

-- ----------------------------
-- Records of wx_customer_property
-- ----------------------------
