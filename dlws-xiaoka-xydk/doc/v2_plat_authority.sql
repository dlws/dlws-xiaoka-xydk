/*
Navicat MySQL Data Transfer

Source Server         : 111.206.116.90
Source Server Version : 50173
Source Host           : 111.206.116.90:3306
Source Database       : xiaoka

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2016-01-22 16:12:30
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `v2_plat_authority`
-- ----------------------------
DROP TABLE IF EXISTS `v2_plat_authority`;
CREATE TABLE `v2_plat_authority` (
  `authorityId` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `authorityName` varchar(255) NOT NULL DEFAULT '' COMMENT '权限名称x-like',
  `authorityCode` varchar(255) DEFAULT NULL COMMENT '权限简称',
  PRIMARY KEY (`authorityId`)
) ENGINE=InnoDB AUTO_INCREMENT=101700191 DEFAULT CHARSET=utf8 COMMENT='系统权限表';

-- ----------------------------
-- Records of v2_plat_authority
-- ----------------------------
INSERT INTO v2_plat_authority VALUES ('1', '产品中心所有权限', 'procenter');
INSERT INTO v2_plat_authority VALUES ('2', '产品中心商品管理权限', 'platgoods');
INSERT INTO v2_plat_authority VALUES ('3', '产品中心采购订单管理权限', 'platpurorder');
INSERT INTO v2_plat_authority VALUES ('4', '产品中心统计管理权限', 'platpurcount');
INSERT INTO v2_plat_authority VALUES ('5', '产品中心第三方供应商管理权限', 'platsupplier');
