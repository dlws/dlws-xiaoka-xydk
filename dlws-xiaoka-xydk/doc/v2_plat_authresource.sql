/*
Navicat MySQL Data Transfer

Source Server         : 111.206.116.90
Source Server Version : 50173
Source Host           : 111.206.116.90:3306
Source Database       : xiaoka

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2016-01-22 16:12:38
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `v2_plat_authresource`
-- ----------------------------
DROP TABLE IF EXISTS `v2_plat_authresource`;
CREATE TABLE `v2_plat_authresource` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `authorityId` int(20) NOT NULL COMMENT '权限Id',
  `resourceId` int(20) NOT NULL COMMENT '资源Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='权限资源关联表';

-- ----------------------------
-- Records of v2_plat_authresource
-- ----------------------------

-- '1', '产品中心所有权限', 'procenter'
INSERT INTO v2_plat_authresource VALUES ('null', '1', '1');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '2');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '3');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '4');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '5');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '6');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '7');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '8');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '9');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '10');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '11');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '12');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '13');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '14');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '15');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '16');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '17');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '18');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '19');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '20');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '21');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '22');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '23');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '24');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '25');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '26');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '27');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '28');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '29');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '30');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '31');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '32');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '33');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '34');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '35');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '36');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '37');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '38');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '39');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '40');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '41');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '42');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '43');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '44');
INSERT INTO v2_plat_authresource VALUES ('null', '1', '45');

-- '2', '产品中心商品管理权限', 'platgoods'
INSERT INTO v2_plat_authresource VALUES ('null', '2', '1');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '2');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '3');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '4');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '5');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '6');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '7');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '8');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '9');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '10');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '11');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '12');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '13');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '14');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '15');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '16');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '17');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '18');
INSERT INTO v2_plat_authresource VALUES ('null', '2', '19');

-- '3', '产品中心采购订单管理权限', 'platpurorder'
INSERT INTO v2_plat_authresource VALUES ('null', '3', '20');
INSERT INTO v2_plat_authresource VALUES ('null', '3', '21');
INSERT INTO v2_plat_authresource VALUES ('null', '3', '22');
INSERT INTO v2_plat_authresource VALUES ('null', '3', '23');
INSERT INTO v2_plat_authresource VALUES ('null', '3', '24');
INSERT INTO v2_plat_authresource VALUES ('null', '3', '25');
INSERT INTO v2_plat_authresource VALUES ('null', '3', '26');

-- '4', '产品中心统计管理权限', 'platpurcount'
INSERT INTO v2_plat_authresource VALUES ('null', '4', '27');
INSERT INTO v2_plat_authresource VALUES ('null', '4', '28');
INSERT INTO v2_plat_authresource VALUES ('null', '4', '29');
INSERT INTO v2_plat_authresource VALUES ('null', '4', '30');
INSERT INTO v2_plat_authresource VALUES ('null', '4', '31');
INSERT INTO v2_plat_authresource VALUES ('null', '4', '32');

-- '5', '产品中心第三方供应商管理权限', 'platsupplier'
INSERT INTO v2_plat_authresource VALUES ('null', '5', '33');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '34');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '35');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '36');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '37');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '38');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '39');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '40');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '41');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '42');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '43');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '44');
INSERT INTO v2_plat_authresource VALUES ('null', '5', '45');

