/*
Navicat MySQL Data Transfer

Source Server         : 111.206.116.90
Source Server Version : 50173
Source Host           : 111.206.116.90:3306
Source Database       : xiaoka

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2016-01-22 13:48:29
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `v2_plat_resource`
-- ----------------------------
DROP TABLE IF EXISTS `v2_plat_resource`;
CREATE TABLE `v2_plat_resource` (
  `resourceId` int(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `componentid` varchar(255) NOT NULL DEFAULT '' COMMENT '组件id',
  `componentname` varchar(255) NOT NULL DEFAULT '' COMMENT '组件名称',
  `parentid` varchar(255) NOT NULL DEFAULT '' COMMENT '父组件id',
  `resourceUrl` varchar(255) NOT NULL DEFAULT '' COMMENT '资源路径',
  `type` int(2) NOT NULL DEFAULT '1' COMMENT '资源类型(0:菜单;1:路径;2:按钮)',
  `createdTime` datetime NOT NULL COMMENT '创建时间',
  `status` int(3) NOT NULL DEFAULT '1' COMMENT '状态(1:有效；0：无效)',
  PRIMARY KEY (`resourceId`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='角色资源信息表';

-- ----------------------------
-- Records of v2_plat_resource
-- type 资源类型(0:菜单;1:路径;2:按钮)
-- status 状态(1:有效；0：无效)
-- ----------------------------
-- 集采中心商品分类
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心分类列表', '', '/platcategory/toPageList.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心跳转到添加分类', '', '/platcategory/toAdd.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心添加分类校验名称', '', '/platcategory/addExistName.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心添加分类', '', '/platcategory/add.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心跳转到编辑分类', '', '/platcategory/toEdit.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心编辑分类校验名称', '', '/platcategory/editExistName.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心编辑分类', '', '/platcategory/edit.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心删除分类', '', '/platcategory/delete.html*', '1', '2016-01-22 13:00:00', '1');

-- 集采中心商品
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心商品列表', '', '/platgoods/toPageList.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心跳转到添加商品', '', '/platgoods/toAdd.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心添加商品校验名称', '', '/platgoods/addExistName.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心上传单张图片', '', '/platgoods/uploadFirstImage.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心上传多张图片', '', '/platgoods/uploadDetailImage.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心删除图片', '', '/platgoods/deleteDetailImage.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心添加商品', '', '/platgoods/add.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心跳转到编辑商品', '', '/platgoods/toEdit.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心编辑商品校验名称', '', '/platgoods/editExistName.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心编辑商品', '', '/platgoods/edit.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心删除商品', '', '/platgoods/delete.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心导出商品', '', '/platgoods/exportToExcel.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心导入商品', '', '/platgoods/importByExcel.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心商品描述富文本上传图片', '', '/platgoods/uploadDescImage.html*', '1', '2016-01-22 13:00:00', '1');

-- 集采中心采购订单
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心采购订单管理', '', '/purorder/toList.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心采购订单查询', '', '/purorder/toPageList.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心根据学校ID查询店铺列表', '', '/purorder/loadShops.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心采购订单详情', '', '/purorder/orderDetail.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心确认采购订单', '', '/purorder/orderConfirm.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心取消采购订单', '', '/purorder/orderCancle.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心导出采购订单', '', '/purorder/exportToExcel.html*', '1', '2016-01-22 13:00:00', '1');

-- 集采中心统计
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心采购订单统计', '', '/purCount/order.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心导出采购订单统计', '', '/purCount/exportOrderCount.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心营业额统计', '', '/purCount/money.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心导出营业额统计', '', '/purCount/exportMoneyCount.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心销售量统计', '', '/purCount/sale.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心导出销售量统计', '', '/purCount/exportSaleCount.html*', '1', '2016-01-22 13:00:00', '1');

-- 集采中心供应商
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心供应商列表', '', '/supplier/toPageList.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心跳转到添加供应商', '', '/supplier/toAdd.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心添加供应商校验名称', '', '/supplier/addExistName.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心添加供应商', '', '/supplier/add.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心跳转到编辑供应商', '', '/supplier/toEdit.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心编辑供应商校验名称', '', '/supplier/editExistName.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心编辑供应商', '', '/supplier/edit.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心删除', '', '/supplier/delete.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心终止供应商', '', '/supplier/end.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心恢复供应商', '', '/supplier/recover.html*', '1', '2016-01-22 13:00:00', '1');

-- 供应商结算记录
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心供应商结算记录列表页', '', '/supcount/toPageList.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心跳转到添加供应商结算记录页面', '', '/supcount/toAdd.html*', '1', '2016-01-22 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '集采中心添加供应商结算记录', '', '/supcount/add.html*', '1', '2016-01-22 13:00:00', '1');


-- 校园运营端店铺
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端微店列表页', '', '/v2busyShopBasicinfo/busyShopBasicinfoList.html+', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端添加微店', '', '/v2busyShopBasicinfo/addBusyShopBasicinfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端去添加微店', '', '/v2busyShopBasicinfo/toAddBusyShopBasicinfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端修改微店', '', '/v2busyShopBasicinfo/updateBusyShopBasicinfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端删除微店', '', '/v2busyShopBasicinfo/deleteBusyShopBasicinfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端上传微店图片', '', '/v2busyShopBasicinfo/uplodPic.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端根据区域code获取区域下学校', '', '/v2busyShopBasicinfo/querySchoolByCity.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端获取学校下店铺', '', '/v2busyShopBasicinfo/queryShopBySchool.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端我的账户信息', '', '/v2busyShopBasicinfo/myAccount.html*', '1', '2016-02-23 13:00:00', '1');
-- 校园运营端商品
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端商品列表', '', '/schoolGoods/schoolGoodsList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端商品上架', '', '/schoolGoods/upframe.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端商品下架', '', '/schoolGoods/downframe.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端设置商品分类', '', '/schoolGoods/updateCategory.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端设置商品市场价', '', '/schoolGoods/setMarketPrice.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端设置商品零售价', '', '/schoolGoods/setRetailPrice.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端设置配送时间段', '', '/schoolGoods/setPromotion.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端设置促销方式', '', '/schoolGoods/setProm.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端设置限购', '', '/schoolGoods/setPurchase.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端判断商品是否已经上架', '', '/schoolGoods/isGoodsUpFrame.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营端商品分类
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端去添加商品分类', '', '/v2SchoolGoodsCategory/toAddSchoolGoodsCategory.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端去修改商品分类', '', '/v2SchoolGoodsCategory/toUpdateGoodsCategory.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端添加商品分类', '', '/v2SchoolGoodsCategory/addV2SchoolGoodsCategory.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端商品分类列表', '', '/v2SchoolGoodsCategory/v2SchoolGoodsCategoryList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端修改学校商品分类', '', '/v2SchoolGoodsCategory/updateV2SchoolGoodsCategory.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端删除学校商品分类', '', '/v2SchoolGoodsCategory/deleteV2SchoolGoodsCategory.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端判断分类名称是否唯一', '', '/v2SchoolGoodsCategory/addSchoolGoodsExistName.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端修改是判断名称是否唯一排除当前修改', '', '/v2SchoolGoodsCategory/editSchoolGoodsExistName.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营端商品采购

INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端采购订单列表', '', '/purchase/toPurchaseOrder.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端跳转添加采购订单', '', '/purchase/toAddPurchaseOrder.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端新增采购订单', '', '/purchase/addPurchaseCard.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端查询订单详情', '', '/purchase/queryOrderDetailByOrderId.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端根据ID查找订单', '', '/purchase/getOrderByOrderId.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端根据订单ID取消订单', '', '/purchase/updateOrderById.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营端库存
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端库存列表', '', '/goodsStock/toStock.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端修改库存', '', '/goodsStock/updateStock.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端去库存修改记录', '', '/goodsStock/toModifyRecord.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端去库存损耗明细', '', '/goodsStock/toLossRecord.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营端活动
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端活动列表', '', '/sendActivity/toSendActivityList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端去添加或修改活动', '', '/sendActivity/toAddActivity.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端根据店铺ID获取店铺商品', '', '/sendActivity/queryGoodsByShopId.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端添加预售活动', '', '/sendActivity/addActivity.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端修改预售活动', '', '/sendActivity/updateActivity.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营端删除预售活动', '', '/sendActivity/deleteActivity.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营端广告管理（轮播图）
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营去添加轮播图', '', '/v2BannerInfo/toAddBanner.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营添加轮播图', '', '/v2BannerInfo/addV2BannerInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营轮播图列表', '', '/v2BannerInfo/v2BannerInfoList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营获取轮播图链接', '', '/v2BannerInfo/getV2BannerInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营获取修改轮播图', '', '/v2BannerInfo/updateV2BannerInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营删除轮播图', '', '/v2BannerInfo/deleteV2BannerInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营上传轮播图', '', '/v2BannerInfo/uplodBannerPic.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营端统计
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营订单统计', '', '/schoolCount/order.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营导出订单统计', '', '/schoolCount/exportOrderCount.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营营业额统计', '', '/schoolCount/money.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营导出营业额统计', '', '/schoolCount/exportMoneyCount.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营爆品营业额统计', '', '/schoolCount/burstGoodsmoney.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营导出爆品营业额统计', '', '/schoolCount/exportBurstGoodsMoneyCount.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营销售量统计', '', '/schoolCount/sale.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营销爆品售量统计', '', '/schoolCount/burstGoodsSale.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营导出销售量统计', '', '/schoolCount/exportSaleCount.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营导出爆品销售量统计', '', '/schoolCount/exportBurstGoodsSaleCount.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营-红包规则管理
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营红包规则列表', '', '/v2busyRedenvelopeRule/activityRedenvelopeRuleList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营保存活动红包', '', '/v2busyRedenvelopeRule/addActivityRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营跳转到新建活动红包', '', '/v2busyRedenvelopeRule/toAddActivityRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营跳转到修改活动红包', '', '/v2busyRedenvelopeRule/toUpdateActivityRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营修改活动红包', '', '/v2busyRedenvelopeRule/updateActivityRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营-红包信息管理    activityRedenvelopeInfoList
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营红包信息列表', '', '/v2busyRedenvelopeRule/activityRedenvelopeRuleList.html*', '1', '2016-02-23 13:00:00', '1');

-- 校园运营-红包管理    activityRedenvelopeInfoList
INSERT INTO v2_plat_resource VALUES (null, '', '校园运营红包列表', '', '/v2busyRedenvelopeRule/activityRedenvelopeList.html*', '1', '2016-02-23 13:00:00', '1');



-- 校园运营-红包规则信息管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营修改红包', '', '/v2busyRedenvelopeRule/updateBusyRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');


-- 城市运营—校园管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营校园列表', '', '/v2schoolInfo/schoolInfoList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到新建校园', '', '/v2schoolInfo/toAddSchoolInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到修改校园', '', '/v2schoolInfo/toUpdateSchoolInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营删除校园', '', '/v2schoolInfo/deleteSchoolInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营保存校园', '', '/v2schoolInfo/addSchoolInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营修改校园', '', '/v2schoolInfo/updateSchoolInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营验证校园名称', '', '/v2schoolInfo/validSchoolName.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—促销活动
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营促销活动列表', '', '/v2salePromotion/salePromotionList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到新建促销活动', '', '/v2salePromotion/toAddSalePromotion.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到修改促销活动', '', '/v2salePromotion/toUpdateSalePromotion.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营保存促销活动', '', '/v2salePromotion/addBusyCoupon.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营修改促销活动', '', '/v2salePromotion/updateBusyCoupon.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营删除促销活动', '', '/v2salePromotion/deleteBusyCoupon.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营验证促销活动名称', '', '/v2salePromotion/validPromotionName.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—优惠券管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营优惠券列表', '', '/v2busyCoupon/busyCouponList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到新建优惠券', '', '/v2busyCoupon/toAddBusyCoupon.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到修改优惠券', '', '/v2busyCoupon/toUpdateBusyCoupon.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营删除优惠券', '', '/v2busyCoupon/toUpdateBusyCoupon.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营保存优惠券', '', '/v2busyCoupon/addBusyCoupon.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营修改优惠券', '', '/v2busyCoupon/updateBusyCoupon.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营验证优惠券名称', '', '/v2busyCoupon/validCouponName.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到添加优惠券控制', '', '/v2busyCouponControl/toCouponControl.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到保存优惠券控制', '', '/v2busyCouponControl/couponControl.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—优惠券管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营优惠券发放列表', '', '/v2busyCouponLog/busyCouponLogList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营优惠券全顾客发放', '', '/v2busyCouponLog/addCouponLogAll.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营优惠券单顾客发放', '', '/v2busyCouponLog/addCouponLogOne.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营删除优惠券', '', '/v2busyCouponLog/deleteBusyCouponLog.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—红包规则管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营保存平台红包', '', '/v2busyRedenvelopeRule/addFlatRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营红包规则列表', '', '/v2busyRedenvelopeRule/busyRedenvelopeRuleList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市/校园删除红包规则', '', '/v2busyRedenvelopeRule/deleteBusyRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到新建平台红包', '', '/v2busyRedenvelopeRule/toAddFlatRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到修改平台红包', '', '/v2busyRedenvelopeRule/toUpdateFlatRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营修改红包', '', '/v2busyRedenvelopeRule/updateBusyRedenvelopeRule.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市/校园运营上传红包图片', '', '/v2busyRedenvelopeRule/uplodPic.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营校验红包名称', '', '/v2busyRedenvelopeRule/validRedenvelopeName.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—红包信息管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营保存红包信息', '', '/v2busyRedenvelopeInfo/addActivityRedenvelopeInfo.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营红包信息列表', '', '/v2busyRedenvelopeInfo/busyRedenvelopeInfoList.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—红包管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营红包列表', '', '/v2busyRedenvelope/busyRedenvelopeList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营删除红包', '', '/v2busyRedenvelope/deleteBusyRedenvelope.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—顾客订单管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营顾客订单列表', '', '/v2busyOrder/busyOrderList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营顾客订单详情', '', '/v2busyOrder/getOrderDetail.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营导出顾客订单', '', '/v2busyOrder/writeCustomerStat.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营获得学校的店铺', '', '/v2busyOrder/getAllShopSchool.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—统计管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营订单统计', '', '/v2busyOrder/statisticsOrderList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营导出订单统计', '', '/v2busyOrder/writeOrderStat.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营营业额统计', '', '/v2busyOrder/statisticsTurnoverList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营导出营业额统计', '', '/v2busyOrder/writeTurnoverStat.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营销量统计', '', '/v2busyOrderDetail/statisticsGoodsList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营导出销量统计', '', '/v2busyOrderDetail/writeSalesStat.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营顾客统计', '', '/v2busyOrder/statisticsCustomerList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营导出顾客统计', '', '/v2busyOrder/writeCustomerStat.html*', '1', '2016-02-23 13:00:00', '1');

-- 城市运营—财务管理
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营提成比例列表', '', '/v2divideRatio/divideRatioList.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到新建提成比例', '', '/v2divideRatio/toAddDivideRatio.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营跳转到修改提成比例', '', '/v2divideRatio/toUpdateDivideRatio.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营保存提成比例', '', '/v2divideRatio/addDivideRatio.html*', '1', '2016-02-23 13:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '城市运营修改提成比例', '', '/v2divideRatio/updateDivideRatio.html*', '1', '2016-02-23 13:00:00', '1');

-- 系统管理—用户管理
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理用户管理主页面', '', '/v2sysuser/sysuserList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转添加用户页面', '', '/v2sysuser/toAddUser.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理添加用户', '', '/v2sysuser/addSysuser.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转修改用户页面', '', '/v2sysuser/toUpdateUser.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理修改用户信息', '', '/v2sysuser/updateSysuser.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转修改密码页面', '', '/v2sysuser/toUpdatePwd.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理修改用户密码', '', '/v2sysuser/updatePwd.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理删除用户', '', '/v2sysuser/deleteSysuser.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理根据ID获取用户信息', '', '/v2sysuser/getSysuser.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理查询用户已拥有角色', '', '/v2sysuser/sysroleUserList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理查询用户未拥有角色', '', '/v2sysuser/sysroleNoUserList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理删除用户角色关系', '', '/v2sysuser/deleteSysroleUser.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理添加用户角色关系', '', '/v2sysuser/addSysroleUser.html*', '1', '2016-02-26 18:00:00', '1');

-- 系统管理—角色管理
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理角色管理主页面', '', '/v2sysrole/sysroleList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转角色添加页面', '', '/v2sysrole/toAddRole.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理添加角色', '', '/v2sysrole/addSysrole.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转角色修改页面', '', '/v2sysrole/toUpdateRole.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理修改角色信息', '', '/v2sysrole/updateSysrole.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理根据ID获取角色信息', '', '/v2sysrole/getSysrole.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理删除角色', '', '/v2sysrole/deleteSysrole.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理获取角色已拥有权限', '', '/v2sysrole/sysroleAuthorityList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理获取角色未拥有权限', '', '/v2sysrole/sysroleNoAuthorityList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理删除角色权限关系', '', '/v2sysrole/deleteSysroleAuthorityhtml*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理添加角色权限关系', '', '/v2sysrole/addSysroleAuthority*', '1', '2016-02-26 18:00:00', '1');

-- 系统管理—权限管理
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理权限管理主页面', '', '/v2Sysauthority/v2sysauthorityList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转添加权限页面', '', '/v2Sysauthority/toAddAuthority.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理添加权限', '', '/v2Sysauthority/v2addSysauthority.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转修改权限页面', '', '/v2Sysauthority/toUpdateAuthority.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理修改权限', '', '/v2Sysauthority/v2updateSysauthority.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理删除权限', '', '/v2Sysauthority/deleteAuthority.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理获取权限已拥有资源', '', '/v2Sysauthority/v2SysresourceAuthList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理获取权限未拥有资源', '', '/v2Sysauthority/v2SysresourceNoAuthList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理添加权限资源关系', '', '/v2Sysauthority/addSysresourceAuth.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理删除权限资源关系', '', '/v2Sysauthority/deleteSysresourceAuth.html*', '1', '2016-02-26 18:00:00', '1');

-- 系统管理—资源管理
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理资源管理主页面', '', '/v2Sysresource/v2sysresourceList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转添加资源页面', '', '/v2Sysresource/toAddResource.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理添加资源', '', '/v2Sysresource/v2addSysresource.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理跳转修改资源页面', '', '/v2Sysresource/toUpdateResource.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理修改资源', '', '/v2Sysresource/v2updateSysresource.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统管理删除资源', '', '/v2Sysresource/v2deleteResource.html*', '1', '2016-02-26 18:00:00', '1');

-- 系统部门管理
INSERT INTO v2_plat_resource VALUES (null, '', '系统部门管理部门管理的添加', '', '/v2Department/addDepartment.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统部门管理得到部门管理的页面', '', '/v2Department/departmentList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统部门管理得到部门', '', '/v2Department/getDepartment.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统部门管理跟新部门', '', '/v2Department/updateDepartment.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '系统部门管理删除部门', '', '/v2Department/deleteDepartment.html*', '1', '2016-02-26 18:00:00', '1');

-- 第三方供应平台

INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台得到订单的列表页面', '', '/purorder/queryPurchaseOrderList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台确认订单的页面', '', '/purorder/getPurchaseOrderList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台订单统计', '', '/purorder/orderCountList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台营业额统计', '', '/purorder/turnoverCountList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台销售量统计', '', '/purorder/salesCountList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台订单统计查询', '', '/purorder/orderStatisticsInfo.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台营业额统计查询', '', '/purorder/turnoverStatisticsInfo.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台销售量统计查询', '', '/purorder/salesStatisticsInfo.html*', '1', '2016-02-26 18:00:00', '1');

INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台我的财务', '', '/supcount/financialManagement.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '第三方供应平台结算记录查询', '', '/supcount/getAccountBySupplierId.html*', '1', '2016-02-26 18:00:00', '1');



-- 团购总部-城市管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部城市管理城市列表', '', '/city/cityList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部城市管理去添加城市信息', '', '/city/toAddCity.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部城市管理添加城市信息', '', '/city/addCity.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部城市管理修改城市信息', '', '/city/updateCity.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部城市管理删除城市信息', '', '/city/deleteCityInfo.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部城市管理根据城市ID获取城市信息', '', '/city/querySchoolByCityId.html*', '1', '2016-02-26 18:00:00', '1');

-- 团购总部-拼团信息管理（阶梯团）
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团拼团列表', '', '/groupPurchase/groupPurchaseList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团去添加拼团信息', '', '/groupPurchase/toAddGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团添加拼团信息', '', '/groupPurchase/addGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团修改拼团信息', '', '/groupPurchase/updaeteGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团获取商品大分类下小分类', '', '/groupPurchase/queryCategory.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团获取分类下商品', '', '/groupPurchase/queryGoods.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团根据cityId获取学校', '', '/groupPurchase/querySchoolByCityId.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团根据上架拼团', '', '/groupPurchase/upGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团根据下架拼团', '', '/groupPurchase/downGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部阶梯团根据置顶拼团', '', '/groupPurchase/topGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部成团列表', '', '/groupPurchase/groupList.html*', '1', '2016-02-26 18:00:00', '1');


-- 团购总部-拼团信息管理（分享团）
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团拼团列表', '', '/shareGroupPurchase/groupPurchaseList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团去添加拼团信息', '', '/shareGroupPurchase/toAddGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团添加拼团信息', '', '/shareGroupPurchase/addGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团修改拼团信息', '', '/shareGroupPurchase/updaeteGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团获取商品大分类下小分类', '', '/shareGroupPurchase/queryCategory.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团获取分类下商品', '', '/shareGroupPurchase/queryGoods.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团根据cityId获取学校', '', '/shareGroupPurchase/querySchoolByCityId.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团根据上架拼团', '', '/shareGroupPurchase/upGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团根据下架拼团', '', '/shareGroupPurchase/downGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团根据置顶拼团', '', '/shareGroupPurchase/topGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');

-- 团购总部-订单管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部订单管理订单列表', '', '/order/orderList.html*', '1', '2016-02-26 18:00:00', '1');


-- 团购总部-提现管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部提现记录列表', '', '/withdraw/withdrawList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团去录入提现记录', '', '/withdraw/toAddWithdraw.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团添加提现记录', '', '/withdraw/addWithdraw.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部分享团根据cityId获取学校', '', '/withdraw/querySchoolByCityId.html*', '1', '2016-02-26 18:00:00', '1');


-- 团购总部-供应商管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部供应商列表', '', '/supplierInfo/supplierList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部去添加供应商', '', '/supplierInfo/toAddSupplier.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部添加供应商', '', '/supplierInfo/addSupplier.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部修改供应商', '', '/supplierInfo/updateSupplier.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部删除供应商', '', '/supplierInfo/deleteSupplierInfo.html*', '1', '2016-02-26 18:00:00', '1');


-- 团购总部-供应商结算记录
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部供应商结算记录列表', '', '/supplierSettlement/supplierSettlementList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部去添加供应商结算记录', '', '/supplierSettlement/toAddSupplierSettlementRecord.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部添加供应商结算记录', '', '/supplierSettlement/addSupplierSettlement.html*', '1', '2016-02-26 18:00:00', '1');

-- 团购总部-统计管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部订单统计', '', '/hqCount/order.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部导出订单统计', '', '/hqCount/exportOrderCount.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部营业额统计', '', '/hqCount/money.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部导出营业额统计', '', '/hqCount/exportMoneyCount.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部销售量统计', '', '/hqCount/sale.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部导出销售量统计', '', '/hqCount/exportSaleCount.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部粉丝统计', '', '/hqCount/coustomer.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部导出粉丝统计', '', '/hqCount/exportCoustomerCount.html*', '1', '2016-02-26 18:00:00', '1');



-- 城市运营端-校园管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市端学校列表信息', '', '/school/schoolList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市端去添加学校信息', '', '/school/toAddSchool.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市端添加学校', '', '/school/addSchool.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市端修改学校', '', '/school/updateSchool.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市端删除学校', '', '/school/deleteSchoolInfo.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市端上传学校logo', '', '/school/uplodSchoolPic.html*', '1', '2016-02-26 18:00:00', '1');


-- 团购城市运营端-拼团信息管理（阶梯团）
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团拼团列表', '', '/cityGroupPurchase/groupPurchaseList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团去添加拼团信息', '', '/cityGroupPurchase/toAddGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团添加拼团信息', '', '/cityGroupPurchase/addGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团修改拼团信息', '', '/cityGroupPurchase/updaeteGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团获取商品大分类下小分类', '', '/cityGroupPurchase/queryCategory.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团获取分类下商品', '', '/cityGroupPurchase/queryGoods.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团根据cityId获取学校', '', '/cityGroupPurchase/querySchoolByCityId.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团根据上架拼团', '', '/cityGroupPurchase/upGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团根据下架拼团', '', '/cityGroupPurchase/downGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端阶梯团根据置顶拼团', '', '/cityGroupPurchase/topGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端成团列表', '', '/cityGroupPurchase/groupList.html*', '1', '2016-02-26 18:00:00', '1');


-- 团购城市运营端-拼团信息管理（分享团）
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团拼团列表', '', '/cityShareGroupPurchase/groupPurchaseList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团去添加拼团信息', '', '/cityShareGroupPurchase/toAddGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团添加拼团信息', '', '/cityShareGroupPurchase/addGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团修改拼团信息', '', '/cityShareGroupPurchase/updaeteGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团获取商品大分类下小分类', '', '/cityShareGroupPurchase/queryCategory.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团获取分类下商品', '', '/cityShareGroupPurchase/queryGoods.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团根据cityId获取学校', '', '/cityShareGroupPurchase/querySchoolByCityId.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团根据上架拼团', '', '/cityShareGroupPurchase/upGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团根据下架拼团', '', '/cityShareGroupPurchase/downGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端分享团根据置顶拼团', '', '/cityShareGroupPurchase/topGroupPurchase.html*', '1', '2016-02-26 18:00:00', '1');


-- 团购城市运营端-供应商管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端供应商列表', '', '/citySupplierInfo/supplierList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端去添加供应商', '', '/citySupplierInfo/toAddSupplier.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端添加供应商', '', '/citySupplierInfo/addSupplier.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端修改供应商', '', '/citySupplierInfo/updateSupplier.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端删除供应商', '', '/citySupplierInfo/deleteSupplierInfo.html*', '1', '2016-02-26 18:00:00', '1');




-- 团购城市运营端-供应商结算记录
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端供应商结算记录列表', '', '/citySupplierSettlement/supplierSettlementList.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端去添加供应商结算记录', '', '/citySupplierSettlement/toAddSupplierSettlementRecord.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端添加供应商结算记录', '', '/citySupplierSettlement/addSupplierSettlement.html*', '1', '2016-02-26 18:00:00', '1');




-- 团购城市运营端-统计管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端订单统计', '', '/cityHQCount/order.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端导出订单统计', '', '/cityHQCount/exportOrderCount.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端营业额统计', '', '/cityHQCount/money.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端导出营业额统计', '', '/cityHQCount/exportMoneyCount.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端销售量统计', '', '/cityHQCount/sale.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端导出销售量统计', '', '/cityHQCount/exportSaleCount.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端粉丝统计', '', '/cityHQCount/coustomer.html*', '1', '2016-02-26 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购城市运营端导出粉丝统计', '', '/cityHQCount/exportCoustomerCount.html*', '1', '2016-02-26 18:00:00', '1');

-- 团购商品管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理列表', '', '/tggoods/toPageList.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理跳转到添加页面', '', '/tggoods/toAdd.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理添加时校验名称', '', '/tggoods/addExistName.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理添加时加载分类', '', '/tggoods/loadCategory.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理上传商品首页图片', '', '/tggoods/uploadFirstImage.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理上传商品详情图集', '', '/tggoods/uploadDetailImage.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理删除商品详情图集', '', '/tggoods/deleteDetailImage.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理添加', '', '/tggoods/add.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理跳转到编辑页面', '', '/tggoods/toEdit.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理编辑时校验名称', '', '/tggoods/editExistName.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理编辑', '', '/tggoods/edit.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理删除', '', '/tggoods/delete.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购商品管理上传商品详细描述图片', '', '/tggoods/uploadDescImage.html*', '1', '2016-03-25 18:00:00', '1');

-- 团购总部商品分类
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品分类列表', '', '/tgcategory/toPageList.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品分类跳转到添加页面', '', '/tgcategory/toAdd.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品分类添加时校验名称', '', '/tgcategory/addExistName.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品分类添加', '', '/tgcategory/add.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品分类跳转到编辑页面', '', '/tgcategory/toEdit.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品分类编辑时校验名称', '', '/tgcategory/editExistName.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品分类编辑', '', '/tgcategory/edit.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品分类删除', '', '/tgcategory/delete.html*', '1', '2016-03-25 18:00:00', '1');

-- 团购总部商品显示分类
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品显示分类列表', '', '/tgshowcategory/toPageList.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品显示分类跳转到添加页面', '', '/tgshowcategory/toAdd.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品显示分类添加时校验名称', '', '/tgshowcategory/addExistName.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品显示分类添加', '', '/tgshowcategory/add.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品显示分类跳转到编辑页面', '', '/tgshowcategory/toEdit.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品显示分类编辑时校验名称', '', '/tgshowcategory/editExistName.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品显示分类编辑', '', '/tgshowcategory/edit.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部商品显示分类删除', '', '/tgshowcategory/delete.html*', '1', '2016-03-25 18:00:00', '1');

-- 团购总部备货管理
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部备货管理列表', '', '/prepareorder/toPageList.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部备货管理根据学校ID查询店铺列表', '', '/prepareorder/loadSchools.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部备货管理备货单详情', '', '/prepareorder/orderDetail.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部备货管理取消备货单', '', '/prepareorder/orderCancle.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部备货管理跳转到添加备货单页面', '', '/prepareorder/toAdd.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部备货管理添加备货单时加载商品列表', '', '/prepareorder/loadGoods.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购总部备货管理新增采购单', '', '/prepareorder/add.html*', '1', '2016-03-25 18:00:00', '1');

-- 微信退款申请、微信退款状态查询、微信企业付款申请、微信企业付款状态查询
INSERT INTO v2_plat_resource VALUES (null, '', '团购微信退款申请', '', '/refund/applyRefund.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购微信退款状态查询', '', '/refund/queryRefund.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购微信企业付款申请', '', '/transfers/applyTransfers.html*', '1', '2016-03-25 18:00:00', '1');
INSERT INTO v2_plat_resource VALUES (null, '', '团购微信企业付款状态查询', '', '/transfers/queryTransfers.html*', '1', '2016-03-25 18:00:00', '1');










