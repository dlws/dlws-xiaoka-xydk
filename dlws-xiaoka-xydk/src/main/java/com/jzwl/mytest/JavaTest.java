/**
 * 测试使用类
 */
package com.jzwl.mytest;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.jzwl.common.upload.ImageUtil;
import com.jzwl.common.utils.Util;

/**
 * @author Administrator
 *
 */
public class JavaTest {

	/**
	 * 
	 */
	public JavaTest() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * get方法提交
	 * 
	 * @param url
	 *            String 访问的URL
	 * @param param
	 *            String 提交的内容
	 * @param repType
	 *            返回类型
	 * @return String
	 * */
	public static byte[] getRequest(String url, String repType) {
		String result = "";
		byte[] resByt = null;
		try {
			URL urlObj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) urlObj
					.openConnection();

			// 连接超时
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setConnectTimeout(25000);

			// 读取超时 --服务器响应比较慢,增大时间
			conn.setReadTimeout(25000);
			conn.setRequestMethod("GET");

			conn.addRequestProperty("Accept-Language", "zh-cn");
			conn.addRequestProperty("Content-type", repType);
			conn.addRequestProperty(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727)");
			conn.connect();

			PrintWriter out = new PrintWriter(new OutputStreamWriter(
					conn.getOutputStream(), "UTF-8"), true);

			if ("image/jpeg".equals(repType)) {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				BufferedImage bufImg = ImageIO.read(conn.getInputStream());
				ImageIO.write(bufImg, "jpg", outputStream);
				resByt = outputStream.toByteArray();
				outputStream.close();

			} else {
				// 取得输入流，并使用Reader读取
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(conn.getInputStream()));

				System.out.println("=============================");
				System.out.println("Contents of get request");
				System.out.println("=============================");
				String lines = null;
				while ((lines = reader.readLine()) != null) {
					System.out.println(lines);
					result += lines;
					result += "\r";
				}
				resByt = result.getBytes();
				reader.close();
			}
			out.print(resByt);
			out.flush();
			out.close();
			// 断开连接
			conn.disconnect();
			System.out.println("=============================");
			System.out.println("Contents of get request ends");
			System.out.println("=============================");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resByt;
	}
	
	
	 public static void main(String[] args) throws ClientProtocolException, IOException    
     {    
         CloseableHttpClient httpclient = HttpClients.createDefault();   
         try {  
             HttpGet httpGet = new HttpGet("http://wx.qlogo.cn/mmopen/NvQjgUz0qZW6TicLKibX3mV0Fia51uYXbgSrhQMvfgVcB8vTWyiagGQia2y1arKKTf8HHibvia8iaicQqMIy2ngjFlOMiaRgK0gG6dL1M1/0");  
             CloseableHttpResponse response = httpclient.execute(httpGet);  
             try {  
                 HttpEntity entity = response.getEntity();  
                 InputStream inStream = entity.getContent();
                 
                 BufferedImage buffer = ImageUtil.imageThumbnail(inStream, 200, 200);
                 
                 byte [] imagebyte =  Util.imageToBytes(buffer);
                 
                 EntityUtils.consume(entity);  
             } finally {  
                 response.close();  
             }  
         }finally {  
             httpclient.close();  
         }  
     }   

}
