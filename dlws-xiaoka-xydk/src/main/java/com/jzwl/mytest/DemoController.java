package com.jzwl.mytest;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.chanjar.weixin.mp.bean.result.WxMpUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.aspectj.token.Token;
import com.jzwl.common.cache.RedisService;
import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.jms.MessageSender;
import com.jzwl.common.log.SystemLog;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.importdata.service.ImportDataService;
import com.jzwl.xydk.sendmessage.SendMessageService;

/**
 * 范例Controller  dev_xkh2.2版本提交
 * 
 * @ClassName: DemoController
 * @description: 通过看这个例子，让大家更快掌握这个框架
 * @author: zhang guo yu
 * @date: 2015-1-21 下午2:54:43
 * @version: 1.0.0
 */
@Controller
@RequestMapping("/demo")
public class DemoController extends BaseController {

	/********************一眼能看明白意思的引用，不需要注释。否则请一定添加注释*********************/

	//	@Autowired
	private MongoTemplate mongoTemplate;//mongodb操作模板

	@Autowired
	private RedisService cacheUtil;//缓存工具类

	@Autowired
	private SystemLog systemLog;//日志工具类

	@Autowired
	private ImportDataService importDataService;

	//@Autowired
	private MessageSender messageSender;//消息队列发出类

	@Autowired
	private SendMessageService sendMessage;//发送模板消息

	/**
	 * post方式交互，提交URL和参数，返回一个页面 参数从request中取,中文统一UTF-8格式 ，免乱码
	 * */
	@RequestMapping(value = "/myrequestpost", method = RequestMethod.POST)
	public ModelAndView myrequestpost(HttpServletRequest request, HttpServletResponse response) {
		//		Map map = (Map) request.getSession().getAttribute(GlobalConstant.Global_SESSION_USER);
		//		System.out.println("测试session：" + map.get("username"));//测试使用session中的信息
		//		System.out.println(request.getSession().getId());//测试抓取jsessionid
		//		System.out.println(idFactory.nextId());//测试ID生成
		ModelAndView mov = new ModelAndView();
		mov.setViewName("/demo/mynice");//  /demo/myrequestpost
		//		mov.addObject("msg", "id为" + request.getParameter("name")); 
		return mov;
	}

	/**
	 * get + json方式交互，提交URL和参数，返回一个map，系统自动转成一个json字符串返回
	 * 参数从request中取，但是中文要记得处理一下，避免乱码
	 * */
	@Token(remove = true)
	@RequestMapping(value = "/mydempjson", method = RequestMethod.GET)
	public @ResponseBody Map mydempjson(HttpServletRequest request, HttpServletResponse response) {
		//		System.out.println(request.getAttribute(GlobalConstant.Global_SUBMIT).toString());
		Map map = new HashMap();
		map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);//json串内，必须携带ajax_status字段，用来判定本次ajax操作是否成功
		map.put("keyStr1", "one");
		map.put("keyStr2", "two");
		String n = "";
		try {
			n = new String(request.getParameter("name").getBytes("ISO-8859-1"), "UTF-8");//或者在JS里进行UTF-8的转码更好
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		map.put("name", n);
		return map;
	}

	/**
	 * 提交条件，生成并下载excel文件
	 * 参数从request中取，但是中文要记得处理一下，避免乱码
	 * */
	@RequestMapping(value = "/mydownloadexcel", method = RequestMethod.GET)
	public void mydownloadexcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String filename = "test.xls";// 设置下载时客户端Excel的名称
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition", "attachment;filename=" + filename);
		OutputStream ouputStream = response.getOutputStream();
		ouputStream.flush();
		ouputStream.close();
	}

	/**
	 * 文件上传
	 * 也可以同时传进来其他参数，参数从request中取
	 * */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String upload(@RequestParam MultipartFile[] myfiles, HttpServletRequest request, HttpServletResponse response) throws IOException {
		//如果只是上传一个文件，则只需要MultipartFile类型接收文件即可，而且无需显式指定@RequestParam注解  
		//如果想上传多个文件，那么这里就要用MultipartFile[]类型来接收文件，并且还要指定@RequestParam注解  
		//并且上传多个文件时，前台表单中的所有<input type="file"/>的name都应该是myfiles，否则参数里的myfiles无法获取到所有上传的文件  
		for (MultipartFile myfile : myfiles) {
			if (myfile.isEmpty()) {
				System.out.println("文件未上传");
			} else {
				System.out.println("文件长度: " + myfile.getSize());
				System.out.println("文件类型: " + myfile.getContentType());
				System.out.println("文件名称: " + myfile.getName());
				System.out.println("文件原名: " + myfile.getOriginalFilename());
				System.out.println("========================================");
			}
		}
		request.setAttribute("sendkey", "sendvalue");
		return "forward:/demo/redirectlist.html"; //注意，这里forward的方法，只能是post的，get会报错
	}

	/**
	 * 跟上边方法之间直接redirect
	 * */
	@RequestMapping(value = "/redirectlist", method = RequestMethod.POST)
	public ModelAndView redirectlist(HttpServletRequest request, HttpServletResponse response) {
		System.out.println(request.getAttribute("sendkey"));
		ModelAndView mov = new ModelAndView();
		mov.setViewName("/demo/mynice");
		mov.addObject("msg", "id为redirectlist");
		return mov;
	}

	/**
	 * 测试shiro登录前的跳转
	 * */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mov = new ModelAndView();
		mov.setViewName("/index");
		return mov;
	}

	/**
	 * 测试shiro登录前的跳转
	 * */
	@RequestMapping(value = "/localWxLoginTest")
	public String localWxLoginTest(HttpServletRequest request, HttpServletResponse response) {
		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("oHxmUjmsQj8Uz3EJNdqMk_V9R7EI");
		wxMpUser.setNickname("阿泰");

		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		return "redirect:/weixinCustomer/nearbyShops.html";
	}

	/**
	 * 郭云鹏测试
	 * */
	@RequestMapping(value = "/localTest")
	public String localTestEshop(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
		wxMpUser.setNickname("阿泰");
		request.getSession().setAttribute("dkOpenId", "o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
		request.getSession().setAttribute("schoolId", "1459490853077000");
		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		return "redirect:/home/index.html";
		//return "redirect:/skillUser/toSkillInfoV2.html";

		//	return "redirect:/home/getmyInform.html";

	}

	@RequestMapping(value = "/test")
	public void test(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("openId", "o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
		map.put("recordId", "1495709963608000");
		sendMessage.numberChange(map);
	}

	/**
	 * 郭云鹏测试
	 * */
	@RequestMapping(value = "/XNSJ")
	public String localTestEshop2(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("oHxmUjgSIW36wQbYa6rI3T9SlsW8");
		wxMpUser.setNickname("阿泰");
		request.getSession().setAttribute("dkOpenId", "oHxmUjgSIW36wQbYa6rI3T9SlsW8");
		request.getSession().setAttribute("schoolId", "1459490853077000");
		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		return "redirect:/home/index.html";
		//return "redirect:/skillUser/toSkillInfoV2.html";

		//	return "redirect:/home/getmyInform.html";

	}

	/**
	 * 郭云鹏测试
	 * */
	@RequestMapping(value = "/CDZY")
	public String localTestEshop0(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("o3P2SwSfpJww70Dv5k--cQeRDoEA");//oHxmUjgTv3PnsjNmNH5HTQIgO10U
		wxMpUser.setNickname("阿泰");
		request.getSession().setAttribute("dkOpenId", "o3P2SwSfpJww70Dv5k--cQeRDoEA");//oHxmUjgTv3PnsjNmNH5HTQIgO10U
		request.getSession().setAttribute("schoolId", "1459490853077000");
		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		return "redirect:/home/index.html";
	}

	/**
	 * 郭云鹏测试
	 * */
	@RequestMapping(value = "/SQDV")
	public String wxl(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();
		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("o3P2SwZCegQ-M6AG4-Nukz5R8gN9");
		wxMpUser.setNickname("阿泰");
		request.getSession().setAttribute("dkOpenId", "oHxmUjguno5skzSBi6VfufsghkmA");
		request.getSession().setAttribute("schoolId", "1459490853077000");
		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		return "redirect:/home/index.html";
		//return "redirect:/skillUser/toSkillInfoV2.html";

		//	return "redirect:/home/getmyInform.html";

	}

	/**
	 * 设置学校
	 * */
	@RequestMapping(value = "/setDateInfo")
	public String setDateInfo(HttpServletRequest request, HttpServletResponse response) {

		importDataService.getData();
		return "redirect:/home/index.html";

		//	return "redirect:/home/getmyInform.html";

	}

	@RequestMapping(value = "/setBaseInfo")
	public void setBaseInfo(HttpServletRequest request, HttpServletResponse response) {
		try {
			importDataService.setBaseInfo1();
			importDataService.setBaseInfo2();
			importDataService.setBaseInfo3();
			importDataService.setBaseInfo4();
			importDataService.setBaseInfo0();
			importDataService.setBaseInfoTwo();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("出现异常");
		}

		//	return "redirect:/home/getmyInform.html";

	}

	/**
	 * dxf测试
	 * */
	@RequestMapping(value = "/localTestdxf")
	public String localTestdxf(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
		wxMpUser.setNickname("阿泰");
		request.getSession().setAttribute("dkOpenId", "o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
		request.getSession().setAttribute("enterId", "5");
		request.getSession().setAttribute("schoolId", "1459490853077000");
		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		return "redirect:/home/index.html";

	}

	/**
	 * 郭云鹏测试eshop,
	 * */
	@RequestMapping(value = "/addSkillTest")
	public String addSkillTest(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("onONowGA527KItDPM3sQIuUJcQmo");
		wxMpUser.setNickname("阿泰");
		request.getSession().setAttribute("openId", "onONowGA527KItDPM3sQIuUJcQmo");
		request.getSession().setAttribute("schoolId", "1459490853077000");
		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		return "redirect:/skillUser/toAddSkillUser.html";

	}

	/**
	 * 郭云鹏测试eshop,
	 * */
	@RequestMapping(value = "/addSkillUserBaseInfoTest")
	public String addbasciTest(HttpServletRequest request, HttpServletResponse response) {

		request.getSession().setAttribute("dkOpenId", "oWGZPs3L5b9OtPD1dW12KOF8-oJE");
		request.getSession().setAttribute("schoolId", "1459490853077000");
		return "redirect:/skillUser/addSkillUserBaseInfo.html";

	}

	public static void main(String[] args) {
		URL url;
		try {
			url = new URL(
					"http://v.youku.com/v_show/id_XMTc4MTQ0NzU0NA==.html?spm=a2hww.20023042.m_223465.5~5~5~5~5~5~A&from=y1.3-idx-beta-1519-23042.223465.1-1");
			Object o = url.getContent();
			InputStream ip = (InputStream) o;
			byte[] b = new byte[20];
			FileOutputStream fout = new FileOutputStream("d://b.png");
			while (ip.read(b) != -1) {
				fout.write(b);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 王小雷测试我的信息
	 * */
	@RequestMapping(value = "/wxlSkillTest")
	public String wxlSkillTest(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
		wxMpUser.setNickname("阿泰");
		request.getSession().setAttribute("openId", "o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
		request.getSession().setAttribute("schoolId", "1459490853077000");
		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		//return "redirect:/skillUser/toSkillInfoV2.html";
		return "redirect:/home/index.html";
	}

	/**
	 * 王小雷测试我的信息
	 * */
	@RequestMapping(value = "/wxlToSkill")
	public String wxlToSkill(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		WxMpUser wxMpUser = new WxMpUser();
		wxMpUser.setOpenId("onONowGA527KItDPM3sQIuUJcQmo");
		wxMpUser.setNickname("阿泰");
		request.getSession().setAttribute("openId", "onONowGA527KItDPM3sQIuUJcQmo");
		request.getSession().setAttribute("schoolId", "1459490853077000");
		String location = "39.91357,116.5544";
		request.getSession().setAttribute("location", location);
		request.getSession().setAttribute("wxuser", wxMpUser);
		return "redirect:/skillUser/toSkillInfo.html";

	}
}
