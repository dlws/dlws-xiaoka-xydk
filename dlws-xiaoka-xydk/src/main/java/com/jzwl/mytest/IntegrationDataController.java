package com.jzwl.mytest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.importdata.service.SetDataService;

/**
 * 
 * 描述:整合2.2的数据专用
 * @author    gyp
 * @Date	 2017年5月11日
 */
@Controller
@RequestMapping("/setData")
public class IntegrationDataController extends BaseController {
	
	@Autowired
	private SetDataService setDataService;


	@RequestMapping(value = "/setGXSTdata")
	public void setGXSTdata(HttpServletRequest request, HttpServletResponse response) {
		try {
			setDataService.setBaseInfoVZMT();
			setDataService.setBaseInfoGXST();
			setDataService.setBaseInfoJNDR();
			setDataService.setBaseInfoCDZY();
			setDataService.setBaseInfoXNSJ();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("出现异常");
		}
	}
	
	
	/**
	 * 执行场地资源插入数据方法
	 * @param request
	 * @param response
	 */
	
	@RequestMapping(value = "/setCDZYdata")
	public void setCDZYdata(HttpServletRequest request, HttpServletResponse response){
		setDataService.setSubscribeScaleForUserDteail();
	}
	
	
	

	

	
}
