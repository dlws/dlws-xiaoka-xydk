package com.jzwl.common.extjs;

/**
 * ext 复选框
 * @author Administrator
 *
 */
public class CheckBox {

	private String name;

	private String boxLabel;

	private boolean checked = false;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBoxLabel() {
		return boxLabel;
	}

	public void setBoxLabel(String boxLabel) {
		this.boxLabel = boxLabel;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}
