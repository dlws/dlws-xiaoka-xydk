package com.jzwl.common.constant;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 全局常量类，全局常量定义在此,前缀Global_
 * @author zhang guo yu
 * @version 1.0.0
 * @since 2015-02-01
 * */
public class GlobalConstant {
	
	/**
	 * 当前session中的用户User标识
	 */
	public static final String Global_SESSION_USER = "global_session_user";
	
	/**
	 * 初始化缓存的字典表信息标识前缀
	 */
	public static final String Global_INFO_CACHE_INIT = "global_info_cache_init";
	
	/**
	 * 分页显示的时候，每页显示多少条记录
	 * */
	public static final int Global_PAGESIZE = 20;
	
	/**
	 * 分页显示的时候，每页显示多少条记录
	 * */
	public static final int BUSINESS_PAGESIZE = 10;
	/**
	 * 分页显示的时候，默认显示第几页
	 * */
	public static final int Global_PAGENUM = 1;
	
	/**
	 * 缓存超时时间（单位：秒）
	 * */
	public static final int Global_CACHETIMEOUT = 1800;
	
	/**
	 * 用户重复提交判定标识
	 */
	public static final String Global_SUBMIT = "global_submit";
	
	/**
	 * 用户操作正常提交
	 */
	public static final String Global_SUBMIT_SUCCESS = "1";
	
	/**
	 * 用户操作重复提交
	 */
	public static final String Global_SUBMIT_FAILURE = "0";
	
	/**
	 * app访问合法
	 */
	public static final String Global_APP_SUCCESS = "1";
	
	/**
	 * app访问非法
	 */
	public static final String Global_APP_FAILURE = "0";
	
	/**
	 * app访问代码端密钥
	 */
	public static final String Global_APP_SECRET = "fjkl?cxv@fds0-=f34";
	
	/** redis控制开关*/
	public static final int REDIS_CACHE_CONTROL = 1;//0:关闭、1：开启
	
	/** key前缀*/
	public static final String CACHE_PREFIX = "xk_";
	
	/** key前缀--系统参数 */
	public static final String CACHE_PREFIX_SYS = "xiaoka_weixin_";
	public static final String CACHE_TG_PREFIX_LOCATION ="xk_location_";
	public static final String CACHE_TG_PREFIX_SCHOOLID ="xk_schoolId_";
	public static final String CACHE_TG_PREFIX_WXMPUSER = "xk_wxMpUser_";
	public static final String CACHE_XKH_HOME_COUNT = "xkh_home_count";//首页人数统计
	public static final String CACHE_XKH_HOME_BANNER = "xkh_home_banner";//首页轮播图
	public static final String CACHE_XKH_HOME_CLASS = "xkh_home_class";//首页分类
	public static final String CACHE_XKH_HOME_PROMOTE = "xkh_home_promote";//首页推荐资源
	
	/** 资源库项目默认字符编码 */
	public static final String CHARACTER_ENCODING_PROJECT = "UTF-8";
	public static final String CONTENTTYPE_PROJECT_HTML = "text/html;charset=UTF-8";
	public static final String CONTENTTYPE_PROJECT_JSON = "text/json; charset=utf-8";
	
	/** 定义用户的openId*/
	public static final String openId_gyp = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
	
	/** 圆周率 */
	public final static double PI = 3.14159265358979323;
	/** 地球的半径 */
	public final static double R = 6371229;
	/**	中间距离 单位（米）*/
//	public final static double MID_INSTANCE=9000d;//9千米
	/**	最大距离 单位（米） */
//	public final static double MAX_INSTANCE=900000d;//900千米
	/**	商品配送方式 */
	public final static String SEND_GOODS_TYPE_ZITI="到店自提";
	public final static String SEND_GOODS_TYPE_JISHI="即时配送";
	public final static String SEND_GOODS_TYPE_GETIAN="隔天配送";
	public final static String SEND_GOODS_TYPE_YUDING="预定商品";
	
	/**团购的状态*/
	public static final int TG_SHART_GROUPTYPE = 1;//1：分享团  2：阶梯团
	public static final int TG_LADDER_GROUPTYPE = 2;//1：分享团  2：阶梯团
	
	public static final int TG_GROUPSTATUS_OPEN = 1;//1：开团
	public static final int TG_GROUPSTATUS_ATTEND = 0;//1：参团
	public static final int TG_PURCHASE_OPEN = 4;//单独支付--开团
	public static final int TG_PURCHASE_ATTEND = 5;//单独支付--参团
	
	public static final int TG_PEOPLE_COST = 6;//校园社团人员单独购买时状态设置
	public static final int TG_PEOPLE_ATTENDCOST = 7;//校园社团人员参团设置
	
	public static final int TG_LARGECLASS_REAL = 1;//1：实物
	public static final int TG_LARGECLASS_DUMMY = 2;//2：虚拟
	
	public static final int TG_OPENGROUP_SUCCESS = 2;//开团成功
	public static final int TG_OPENGROUP_FAIL = 3;//失败
	public static final int TG_OPENGROUP_ONGOING = 1;//正在拼团
	
	public static final int TG_WEIKUAN =   4;//补交尾款的标识 
	
	public static final String WEIXIN_SAVE_MAPUSER_FRONT = "saveFront";//微信保存用户信息之前执行业务的标识 
	public static final String WEIXIN_AUTHORIZE_BACK_URL = "backUrl";//微信只授权的回调地址
	/**
	 * 微信回调sate参数的key值，根据key值执行相关业务
	 */
	public static final String WEIXIN_V2_STATE_OF_REDID = "redId";//校咖分享红包
	public static final String WEIXIN_TG_STATE_OF_SCHOOLID = "schoolId";//团购所属学校Id
	public static final String WEIXIN_TG_STATE_OF_OPENID = "openId";//团购校咖大使openId
	public static final String WEIXIN_TG_STATE_OF_GROUPID = "groupId";//校咖团购，分享团Id
	public static final String WEIXIN_TG_STATE_OF_GROUP_TYPE = "type";//校咖团购，加入社团的社团ID
	public static final String WEIXIN_TG_STATE_OF_ASSNID = "assnId";//校咖团购，加入社团的社团ID
	
	
	public static final String WEIXIN_TG_STATE_OF_WEIKUAN = "weikuan";//校咖团购,WEIXIN_TG_STATE_OF_WEIKUAN
	public static final String WEIXIN_TG_STATE_GROUPMONEY_OPEN = "open";//开团支付标记
	public static final String WEIXIN_TG_STATE_GROUPMONEY_SHAREOPEN = "shareopen";//分享团开团支付标记
	public static final String WEIXIN_TG_STATE_GROUPMONEY_ATTEND = "attend";//参团支付标记
	public static final String WEIXIN_TG_STATE_OF_ORDERID = "orderId";//校咖团购,订单的idWEIXIN_TG_STATE_OF_WEIKUAN
	public static final String WEIXIN_TG_STATE_OF_OPENGROUPID = "openGroupId";//校咖团购,开团的id
	public static final String WEIXIN_TG_STATE_OF_GPPRICE = "gpPrice";//校咖团购,尾款金额
	public static final String WEIXIN_TG_STATE_OF_STATUSFLAG = "statusflag";//校咖团购,开团的标志    3 表示补缴尾款
	
	public static final String WEIXIN_TG_STATE_OF_REFERRALID = "referralId";//推荐人的openId
	public static final String WEIXIN_TG_STATE_OF_FANSOPENID = "fansOpenId";//粉丝的openId
	public static final String BUSINTYPE = "businessType";//校园商家类型
	public static final String COMPARE_TYPE = "cooperaType";//合作类型
	
	
	/**
	 * 团购分享跳转url  weixinLocation/getTgWeixinLocation.html
	 */
	public static final String WEIXIN_TG_SHARE_URL = "tuangou/initShareInfo.html";
	/**
	 * dic 中的类型
	 */
	public static final String DIC_TYPE_CODE = "HotWord";
	public static final String DIC_TYPE_CODE_COMPANY = "companyType";
	
	public static final String DIC_TYPE_CODE_NEWNUM = "NewNum";
	public static final String DIC_TYPE_CODE_SCHOOLLABEL = "SchoolLabel";
	
	
	public static final int PAGESIZE = 2;
	
	
	/**
	 * 测试时间段的
	 */
	public static long TG_TIME_START = 0l;//开始时间
	public static long TG_TIME_END = 0l;//结束时间
	
	/**
	 * 入住类型   enter  type
	 */
	public static final String ENTER_TYPE_SQDV = "sqdv";
	public static final String ENTER_TYPE_GXST = "gxst";
	public static final String ENTER_TYPE_JNDR = "jndr";
	public static final String ENTER_TYPE_XNSJ = "xnsj";
	public static final String ENTER_TYPE_CDZY = "cdzy";
	public static final String ENTER_TYPE_JZZX = "jzzx";
	public static final String ENTER_TYPE_JZTF = "jztf";
	
	public static final String TYPE_YES = "1";
	public static final String TYPE_NO = "0";
	
	/**
	 * 订单的订单状态
	 */
	//1：下单成功，2：待付款、 3：30分钟未付款取消，4：支付成功（也就是待服务）、 5：待确认（已服务）， 6：已确认， 7：超时失效， 8：申请退、9：退款成功'
	public static final String  ORDER_SUC = "1";
	public static final String  ORDER_DFK = "2";
	public static final String  ORDER_QX = "3";
	public static final String  ORDER_ZFCG = "4";
	public static final String  ORDER_DQR = "5";
	public static final String  ORDER_YQR = "6";
	public static final String  ORDER_SXCS = "7";
	public static final String  ORDER_SQTK = "8";
	public static final String  ORDER_TKCG = "9";
	public static final String  ORDER_JJTK = "12";
	
	
	/**
	 * 订单超时时间配置
	 */
	public static final Integer HOUR = 1;//单位小时 下单之后的订单超时时间
	
	public static final Integer DAY = 1;//单位天  支付之后至卖家接单到订单的超时时间
	
	public static final Integer WEEK = 7;//单位天 卖家已服务 确认时间为一周，申请退款确认时间为一周 共用  
	
	
	/**
	 * 轮播图的状态
	 */
	public static final String  BANNER_HOME = "1";//首页
	public static final String  BANNER_RELEASE = "2";//发布
	public static final String  BANNER_PERSON = "3";//个人
	public static final String  BANNER_SQDV = "4";//社群大V
	public static final String  BANNER_JNDR = "5";//技能达人
	public static final String  BANNER_GXST = "6";//高校社团
	public static final String  BANNER_CDZY = "7";//场地入驻
	public static final String  BANNER_XNSJ = "8";//校内商家
	public static final String  BANNER_SJFB= "9";//商家发布
	
	
	/**
	 * 获取当前登录用户名
	 * @return
	 */
	public static String getCurrentUserName() {
		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		String username = null;
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		} else {
			username = principal.toString();
		}
		return username;
	}
	
	/**	店铺信息放在redis中的时间 */
//	public final static long SHOP_LIVETIME=60L;//1小时=60*60s 3600L
	/**	商品放在redis中的时间 */
//	public final static long GOODS_LIVETIME=60L;//1天=60*60*24s 86400L
}
