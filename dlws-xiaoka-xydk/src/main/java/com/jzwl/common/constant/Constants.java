package com.jzwl.common.constant;

import java.net.URLDecoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.cache.RedisService;
import com.jzwl.common.init.infoholder.SystemInfoInitAndHolder;
import com.jzwl.common.utils.StringUtil;

/**
 * 可控制的常量类
 */
@Service("constants")
public class Constants {
	@Autowired
	private RedisService redisService;
	// weixin
	private String xiaoka_weixin_appId;
	private String xiaoka_weixin_secret;
	private String xiaoka_weixin_token;
	private String xiaoka_weixin_aesKey;
	private String xiaoka_weixin_oauth2redirectUri;

	private String xiaoka_weixin_api_key;
	private String xiaoka_weixin_mch_id;
	private Long xiaoka_weixin_expiresTime;

	// pay redirect
	private String notify_url;
	private String tell_url;

	// tg 团购
	private String tg_notify_url;
	private String tg_tell_url;
	private String tg_templateMessage_set_url;// 模板消息跳转链接
	private String tg_group_success;// 模板消息拼团成功的消息通知
	private String tg_group_fail;// 模板消息拼团失败的退款消息通知
	private String tg_group_weikuan;// 模板消息阶梯团补缴尾款的消息通知
	private String tg_bujiaoweikuan_set_url;// 模板消息跳转链接

	// xkh
	private String xkh_audit_success_template;// 校咖汇审核成功模板通知

	private String templateMessage_to_home_url;// 点击跳转地址
	private String templateMessage_to_my_url;// 点击跳转地址
	private String templateMessage_to_chatList_url;// 点击群发信息的跳转
	private String templateMessage_to_myOrder_url;// 点击模板消息进入我的订单列表
	private String textMessage_to_sendText;//发送模板信息
	private String templateMessage_to_OrderInfo_url;//点击进入订单的详情页
	private String templateMessage_to_numberChange_url;//管理员更换的url
	private String templateMessage_to_diplomatChange_url;//外交官更换的url
	
	private String textMessage_jztf_putContent_url;//点击该文本信息，跳转到投放内容
	
	private String xiaoka_weixin_wm_sendTemplateByOpenId;// 访问地址

	private Double xiaoka_weixin_tg_league_scale;// 校咖团购——校园社团提成比例，eg：3就是300%
	private Double xiaoka_weixin_tg_envoy_scale;// 校咖团购——校园大使提成比例，eg：2就是200%
	private String xiaoka_weixin_tg_share_desc;// 校咖团购——分享描述

	private String xiaoka_weixin_tg_day;// 校咖结束天数的设置

	private String xiaoka_weixin_baoming;// 输入报名的回复链接

	private String xiaoka_people_cost;// 校园社团人员购买时的价格设置

	private String xiaoka_page_myorder;// 我的订单显示条数设置

	// bbs--begin
	private String xiaoka_weixin_bbs_toAuto;// 去授权地址
	private String xiaoka_weixin_bbs_toGetLocation;// 去获取坐标地址
	private String xiaoka_weixin_bbs_projectUrl;// bbs项目前端访问地址
	// APP to TgIndex
	private String xiaoka_weixin_toTg_toTgIndex;// app访问到团购对应Index

	// 商家绑定url
	private String wx_business_toBind_url;
	// 校咖团购——社团
	private String wx_tuangou_league_toBind_url;

	// GlobalConstant
	private Double xiaoka_weixin_mid_instance;
	private Double xiaoka_weixin_max_instance;
	private Long xiaoka_weixin_shop_livetime;
	private Long xiaoka_weixin_goods_livetime;
	// UploadConstants
	private String xiaoka_weixin_staticresource_path;
	private String xiaoka_weixin_targetdirectory_goods;
	private String xiaoka_weixin_targetdirectory_banner;
	private String xiaoka_weixin_targetdirectory_goods_detail;
	private String xiaoka_weixin_targetdirectory_user_avatar;
	private String xiaoka_weixin_targetdirectory_shop_logo;
	private String xiaoka_weixin_targetdirectory_zhongbao;

	private String xiaoka_weixin_systemversioncode;
	private String xiaoka_weixin_versionDescription;

	private String xiaoka_weixin_targetdirectory_bbs;
	private String xiaoka_weixin_bbs_toGetJsConfig;// 校咖获取js签名的url地址

	private String xiaoka_weixin_bbs_toDownloadImage;

	// 微信消息通知模板ID
	private String order_submit;// 下单
	private String order_pay;// 支付
	private String order_take;// 提货
	private String order_cancle;// 取消
	private String templateMessage_set_url;// 模板消息跳转链接
	private String templateMessage_set_url_business;// 商家模板消息跳转链接
	private String templateMessage_v2_set_url;// v2模板消息跳转链接
	private String xiaoka_hf;
	private String xiaoka_tg_hf;
	private String xiaoka_zb_hf;
	private String xiaoka_train_hf;

	// 首次关注通知消息
	private String xiaoka_weixin_firstsendtext;

	private String xiaoka_logo;
	private String xiaoka_news_share_url;

	private String xiaoka_news_filterText;
	private String xiaoka_news_index_share_desc;
	private String xiaoka_news_index_share_title;

	// 微信二维码生成连接
	private String xiaoka_two_login_Url;

	// 打款system_tg_zb_remote_token
	private String system_tx_bbs_remote_token;
	// 去打款url
	private String system_tx_bbs_playMoneyUrl;
	// 获取tokenUrl
	private String system_tx_bbs_applyMoney_getToken;
	// 产品的url
	private String xiaoka_bbs_to_eshop_url;

	private String xiaoka_bbs_oosUpload_endpoint;
	private String xiaoka_bbs_oosUpload_accessKeyId;
	private String xiaoka_bbs_oosUpload_accessKeySecret;
	private String xiaoka_bbs_oosUpload_bucketName;
	private String xiaoka_bbs_oosGetImg_beforeUrl;
	private String xiaoka_weixin_toTg_toSendText;

	private String xiaoka_weixin_xydk_toGetJsConfig;
	private String xiaoka_weixin_xydk_toDownloadImage;
	private String xiaoka_weixin_xydk_tuikuan;// 退款
	private String wx_signKey;// 生成签名用的key

	public String getWx_signKey() {
		wx_signKey = (String) SystemInfoInitAndHolder
				.getSystemInfo("wx_signKey");
		return wx_signKey;
	}

	public String getXiaoka_weixin_xydk_tuikuan() {
		xiaoka_weixin_xydk_tuikuan = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_xydk_tuikuan");
		return xiaoka_weixin_xydk_tuikuan;
	}

	// 头条用户注册申请url
	private String toutiao_user_regis_url;

	public String getXiaoka_weixin_toTg_toSendText() {
		xiaoka_weixin_toTg_toSendText = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_toTg_toSendText");
		return xiaoka_weixin_toTg_toSendText;
	}

	public void setXiaoka_weixin_toTg_toSendText(
			String xiaoka_weixin_toTg_toSendText) {
		this.xiaoka_weixin_toTg_toSendText = xiaoka_weixin_toTg_toSendText;
	}

	public String getXiaoka_weixin_xydk_toDownloadImage() {
		xiaoka_weixin_xydk_toDownloadImage = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_xydk_toDownloadImage");
		return xiaoka_weixin_xydk_toDownloadImage;
	}

	public void setXiaoka_weixin_xydk_toDownloadImage(
			String xiaoka_weixin_xydk_toDownloadImage) {
		this.xiaoka_weixin_xydk_toDownloadImage = xiaoka_weixin_xydk_toDownloadImage;
	}

	public String getXiaoka_weixin_xydk_toGetJsConfig() {
		xiaoka_weixin_xydk_toGetJsConfig = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_xydk_toGetJsConfig");
		return xiaoka_weixin_xydk_toGetJsConfig;
	}

	public void setXiaoka_weixin_xydk_toGetJsConfig(
			String xiaoka_weixin_xydk_toGetJsConfig) {
		this.xiaoka_weixin_xydk_toGetJsConfig = xiaoka_weixin_xydk_toGetJsConfig;
	}

	public String getToutiao_user_regis_url() {
		toutiao_user_regis_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("toutiao_user_regis_url");
		return toutiao_user_regis_url;
	}

	public void setToutiao_user_regis_url(String toutiao_user_regis_url) {
		this.toutiao_user_regis_url = toutiao_user_regis_url;
	}

	public String getXiaoka_bbs_oosGetImg_beforeUrl() {
		xiaoka_bbs_oosGetImg_beforeUrl = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_bbs_oosGetImg_beforeUrl");
		return xiaoka_bbs_oosGetImg_beforeUrl;
	}

	public void setXiaoka_bbs_oosGetImg_beforeUrl(
			String xiaoka_bbs_oosGetImg_beforeUrl) {
		this.xiaoka_bbs_oosGetImg_beforeUrl = xiaoka_bbs_oosGetImg_beforeUrl;
	}

	public String getXiaoka_bbs_oosUpload_bucketName() {
		xiaoka_bbs_oosUpload_bucketName = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_bbs_oosUpload_bucketName");
		return xiaoka_bbs_oosUpload_bucketName;
	}

	public void setXiaoka_bbs_oosUpload_bucketName(
			String xiaoka_bbs_oosUpload_bucketName) {
		this.xiaoka_bbs_oosUpload_bucketName = xiaoka_bbs_oosUpload_bucketName;
	}

	public String getXiaoka_bbs_oosUpload_accessKeySecret() {
		xiaoka_bbs_oosUpload_accessKeySecret = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_bbs_oosUpload_accessKeySecret");
		return xiaoka_bbs_oosUpload_accessKeySecret;
	}

	public void setXiaoka_bbs_oosUpload_accessKeySecret(
			String xiaoka_bbs_oosUpload_accessKeySecret) {
		this.xiaoka_bbs_oosUpload_accessKeySecret = xiaoka_bbs_oosUpload_accessKeySecret;
	}

	public String getXiaoka_bbs_oosUpload_accessKeyId() {
		xiaoka_bbs_oosUpload_accessKeyId = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_bbs_oosUpload_accessKeyId");
		return xiaoka_bbs_oosUpload_accessKeyId;
	}

	public void setXiaoka_bbs_oosUpload_accessKeyId(
			String xiaoka_bbs_oosUpload_accessKeyId) {
		this.xiaoka_bbs_oosUpload_accessKeyId = xiaoka_bbs_oosUpload_accessKeyId;
	}

	public String getXiaoka_bbs_oosUpload_endpoint() {
		xiaoka_bbs_oosUpload_endpoint = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_bbs_oosUpload_endpoint");
		return xiaoka_bbs_oosUpload_endpoint;
	}

	public void setXiaoka_bbs_oosUpload_endpoint(
			String xiaoka_bbs_oosUpload_endpoint) {
		this.xiaoka_bbs_oosUpload_endpoint = xiaoka_bbs_oosUpload_endpoint;
	}

	public String getXiaoka_two_login_Url() {
		xiaoka_two_login_Url = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_two_login_Url");
		return xiaoka_two_login_Url;
	}

	public void setXiaoka_two_login_Url(String xiaoka_two_login_Url) {
		this.xiaoka_two_login_Url = xiaoka_two_login_Url;
	}

	public String getXiaoka_news_index_share_title() {
		xiaoka_news_index_share_title = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_news_index_share_title");
		return xiaoka_news_index_share_title;
	}

	public void setXiaoka_news_index_share_title(
			String xiaoka_news_index_share_title) {
		this.xiaoka_news_index_share_title = xiaoka_news_index_share_title;
	}

	public String getXiaoka_news_index_share_desc() {
		xiaoka_news_index_share_desc = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_news_index_share_desc");
		return xiaoka_news_index_share_desc;
	}

	public void setXiaoka_news_index_share_desc(
			String xiaoka_news_index_share_desc) {
		this.xiaoka_news_index_share_desc = xiaoka_news_index_share_desc;
	}

	public String getXiaoka_news_filterText() {
		xiaoka_news_filterText = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_news_filterText");
		return xiaoka_news_filterText;
	}

	public void setXiaoka_news_filterText(String xiaoka_news_filterText) {
		this.xiaoka_news_filterText = xiaoka_news_filterText;
	}

	public String getXiaoka_news_share_url() {
		xiaoka_news_share_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_news_share_url");
		return xiaoka_news_share_url;
	}

	public void setXiaoka_news_share_url(String xiaoka_news_share_url) {
		this.xiaoka_news_share_url = xiaoka_news_share_url;
	}

	public String getXiaoka_logo() {
		xiaoka_logo = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_logo");
		return xiaoka_logo;
	}

	public void setXiaoka_logo(String xiaoka_logo) {
		this.xiaoka_logo = xiaoka_logo;
	}

	public String getXiaoka_weixin_firstsendtext() {
		xiaoka_weixin_firstsendtext = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_firstsendtext");
		return xiaoka_weixin_firstsendtext;
	}

	public void setXiaoka_weixin_firstsendtext(
			String xiaoka_weixin_firstsendtext) {
		this.xiaoka_weixin_firstsendtext = xiaoka_weixin_firstsendtext;
	}

	/**
	 * 
	 * @return
	 */
	public String getRedisParamValue(String key) {
		String value = null;
		try {
			value = redisService.get(key);
			if (null != value && !"".equals(value)) {
				value = URLDecoder.decode(value, "utf-8");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public String getXiaoka_weixin_bbs_toGetJsConfig() {
		xiaoka_weixin_bbs_toGetJsConfig = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_bbs_toGetJsConfig");
		return xiaoka_weixin_bbs_toGetJsConfig;
	}

	public String getXiaoka_weixin_bbs_toDownloadImage() {
		xiaoka_weixin_bbs_toDownloadImage = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_bbs_toDownloadImage");
		return xiaoka_weixin_bbs_toDownloadImage;
	}

	public String getXiaoka_weixin_toTg_toTgIndex() {
		xiaoka_weixin_toTg_toTgIndex = getRedisParamValue("xiaoka_weixin_toTg_toTgIndex");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_toTg_toTgIndex)) {
			xiaoka_weixin_toTg_toTgIndex = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_toTg_toTgIndex");
		}
		return xiaoka_weixin_toTg_toTgIndex;
	}

	public void setXiaoka_weixin_toTg_toTgIndex(
			String xiaoka_weixin_toTg_toTgIndex) {
		this.xiaoka_weixin_toTg_toTgIndex = xiaoka_weixin_toTg_toTgIndex;
	}

	public String getXiaoka_weixin_bbs_toAuto() {
		xiaoka_weixin_bbs_toAuto = getRedisParamValue("xiaoka_weixin_bbs_toAuto");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_bbs_toAuto)) {
			xiaoka_weixin_bbs_toAuto = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_bbs_toAuto");
		}
		return xiaoka_weixin_bbs_toAuto;
	}

	public void setXiaoka_weixin_bbs_toAuto(String xiaoka_weixin_bbs_toAuto) {
		this.xiaoka_weixin_bbs_toAuto = xiaoka_weixin_bbs_toAuto;
	}

	public String getXiaoka_weixin_bbs_toGetLocation() {
		xiaoka_weixin_bbs_toGetLocation = getRedisParamValue("xiaoka_weixin_bbs_toGetLocation");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_bbs_toGetLocation)) {
			xiaoka_weixin_bbs_toGetLocation = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_bbs_toGetLocation");
		}
		return xiaoka_weixin_bbs_toGetLocation;
	}

	public void setXiaoka_weixin_bbs_toGetLocation(
			String xiaoka_weixin_bbs_toGetLocation) {
		this.xiaoka_weixin_bbs_toGetLocation = xiaoka_weixin_bbs_toGetLocation;
	}

	public String getXiaoka_weixin_bbs_projectUrl() {
		xiaoka_weixin_bbs_projectUrl = getRedisParamValue("xiaoka_weixin_bbs_projectUrl");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_bbs_projectUrl)) {
			xiaoka_weixin_bbs_projectUrl = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_bbs_projectUrl");
		}
		return xiaoka_weixin_bbs_projectUrl;
	}

	public void setXiaoka_weixin_bbs_projectUrl(
			String xiaoka_weixin_bbs_projectUrl) {
		this.xiaoka_weixin_bbs_projectUrl = xiaoka_weixin_bbs_projectUrl;
	}

	public Double getXiaoka_weixin_mid_instance() {
		String mid_instance = getRedisParamValue("xiaoka_weixin_mid_instance");
		if (StringUtil.isEmptyOrBlank(mid_instance)) {
			mid_instance = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_mid_instance");
		}
		xiaoka_weixin_mid_instance = Double.valueOf(mid_instance);
		return xiaoka_weixin_mid_instance;
	}

	public void setXiaoka_weixin_mid_instance(Double xiaoka_weixin_mid_instance) {
		this.xiaoka_weixin_mid_instance = xiaoka_weixin_mid_instance;
	}

	// 版本描述信息
	public String getXiaoka_weixin_versionDescription() {

		xiaoka_weixin_versionDescription = getRedisParamValue("xiaoka_weixin_versionDescription");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_versionDescription)) {
			xiaoka_weixin_versionDescription = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_versionDescription");
		}
		return xiaoka_weixin_versionDescription;
	}

	public void setXiaoka_weixin_versionDescription(
			String xiaoka_weixin_versionDescription) {
		this.xiaoka_weixin_versionDescription = xiaoka_weixin_versionDescription;
	}

	public Double getXiaoka_weixin_max_instance() {
		String max_instance = getRedisParamValue("xiaoka_weixin_max_instance");
		if (StringUtil.isEmptyOrBlank(max_instance)) {
			max_instance = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_max_instance");
		}
		xiaoka_weixin_max_instance = Double.valueOf(max_instance);
		return xiaoka_weixin_max_instance;
	}

	public void setXiaoka_weixin_max_instance(Double xiaoka_weixin_max_instance) {
		this.xiaoka_weixin_max_instance = xiaoka_weixin_max_instance;
	}

	public Long getXiaoka_weixin_shop_livetime() {
		String shop_livetime = getRedisParamValue("xiaoka_weixin_shop_livetime");
		if (StringUtil.isEmptyOrBlank(shop_livetime)) {
			shop_livetime = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_shop_livetime");
		}
		xiaoka_weixin_shop_livetime = Long.valueOf(shop_livetime);
		return xiaoka_weixin_shop_livetime;
	}

	public void setXiaoka_weixin_shop_livetime(Long xiaoka_weixin_shop_livetime) {
		this.xiaoka_weixin_shop_livetime = xiaoka_weixin_shop_livetime;
	}

	public Long getXiaoka_weixin_goods_livetime() {
		String goods_livetime = getRedisParamValue("xiaoka_weixin_goods_livetime");
		if (StringUtil.isEmptyOrBlank(goods_livetime)) {
			goods_livetime = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_goods_livetime");
		}
		xiaoka_weixin_goods_livetime = Long.valueOf(goods_livetime);
		return xiaoka_weixin_goods_livetime;
	}

	public void setXiaoka_weixin_goods_livetime(
			Long xiaoka_weixin_goods_livetime) {
		this.xiaoka_weixin_goods_livetime = xiaoka_weixin_goods_livetime;
	}

	public String getXiaoka_weixin_staticresource_path() {
		// xiaoka_weixin_staticresource_path =
		// getRedisParamValue("xiaoka_weixin_staticresource_path");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_staticresource_path)) {
			xiaoka_weixin_staticresource_path = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_staticresource_path");
		}
		return xiaoka_weixin_staticresource_path;
	}

	public void setXiaoka_weixin_staticresource_path(
			String xiaoka_weixin_staticresource_path) {
		this.xiaoka_weixin_staticresource_path = xiaoka_weixin_staticresource_path;
	}

	public String getXiaoka_weixin_targetdirectory_goods() {
		xiaoka_weixin_targetdirectory_goods = getRedisParamValue("xiaoka_weixin_targetdirectory_goods");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_targetdirectory_goods)) {
			xiaoka_weixin_targetdirectory_goods = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_targetdirectory_goods");
		}
		return xiaoka_weixin_targetdirectory_goods;
	}

	public void setXiaoka_weixin_targetdirectory_goods(
			String xiaoka_weixin_targetdirectory_goods) {
		this.xiaoka_weixin_targetdirectory_goods = xiaoka_weixin_targetdirectory_goods;
	}

	public String getXiaoka_weixin_targetdirectory_banner() {
		xiaoka_weixin_targetdirectory_banner = getRedisParamValue("xiaoka_weixin_targetdirectory_banner");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_targetdirectory_banner)) {
			xiaoka_weixin_targetdirectory_banner = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_targetdirectory_banner");
		}
		return xiaoka_weixin_targetdirectory_banner;
	}

	public void setXiaoka_weixin_targetdirectory_banner(
			String xiaoka_weixin_targetdirectory_banner) {
		this.xiaoka_weixin_targetdirectory_banner = xiaoka_weixin_targetdirectory_banner;
	}

	public String getXiaoka_weixin_targetdirectory_goods_detail() {
		xiaoka_weixin_targetdirectory_goods_detail = getRedisParamValue("xiaoka_weixin_targetdirectory_goods_detail");
		if (StringUtil
				.isEmptyOrBlank(xiaoka_weixin_targetdirectory_goods_detail)) {
			xiaoka_weixin_targetdirectory_goods_detail = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_targetdirectory_goods_detail");
		}
		return xiaoka_weixin_targetdirectory_goods_detail;
	}

	public String getXiaoka_weixin_targetdirectory_bbs() {
		xiaoka_weixin_targetdirectory_bbs = getRedisParamValue("xiaoka_weixin_targetdirectory_bbs");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_targetdirectory_bbs)) {
			xiaoka_weixin_targetdirectory_bbs = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_targetdirectory_bbs");
		}
		return xiaoka_weixin_targetdirectory_bbs;
	}

	public void setXiaoka_weixin_targetdirectory_goods_detail(
			String xiaoka_weixin_targetdirectory_goods_detail) {
		this.xiaoka_weixin_targetdirectory_goods_detail = xiaoka_weixin_targetdirectory_goods_detail;
	}

	public String getXiaoka_weixin_targetdirectory_user_avatar() {
		xiaoka_weixin_targetdirectory_user_avatar = getRedisParamValue("xiaoka_weixin_targetdirectory_user_avatar");
		if (StringUtil
				.isEmptyOrBlank(xiaoka_weixin_targetdirectory_user_avatar)) {
			xiaoka_weixin_targetdirectory_user_avatar = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_targetdirectory_user_avatar");
		}
		return xiaoka_weixin_targetdirectory_user_avatar;
	}

	public void setXiaoka_weixin_targetdirectory_user_avatar(
			String xiaoka_weixin_targetdirectory_user_avatar) {
		this.xiaoka_weixin_targetdirectory_user_avatar = xiaoka_weixin_targetdirectory_user_avatar;
	}

	public String getXiaoka_weixin_targetdirectory_shop_logo() {
		xiaoka_weixin_targetdirectory_shop_logo = getRedisParamValue("xiaoka_weixin_targetdirectory_shop_logo");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_targetdirectory_shop_logo)) {
			xiaoka_weixin_targetdirectory_shop_logo = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_targetdirectory_shop_logo");
		}
		return xiaoka_weixin_targetdirectory_shop_logo;
	}

	public void setXiaoka_weixin_targetdirectory_shop_logo(
			String xiaoka_weixin_targetdirectory_shop_logo) {
		this.xiaoka_weixin_targetdirectory_shop_logo = xiaoka_weixin_targetdirectory_shop_logo;
	}

	public String getXiaoka_weixin_systemversioncode() {
		xiaoka_weixin_systemversioncode = getRedisParamValue("xiaoka_weixin_systemversioncode");
		if (StringUtil.isEmptyOrBlank(xiaoka_weixin_systemversioncode)) {
			xiaoka_weixin_systemversioncode = (String) SystemInfoInitAndHolder
					.getSystemInfo("xiaoka_weixin_systemversioncode");
		}
		return xiaoka_weixin_systemversioncode;
	}

	public void setXiaoka_weixin_systemversioncode(
			String xiaoka_weixin_systemversioncode) {
		this.xiaoka_weixin_systemversioncode = xiaoka_weixin_systemversioncode;
	}

	public String getXiaoka_weixin_appId() {
		// xiaoka_weixin_appId = getRedisParamValue("xiaoka_weixin_appId");
		// if (StringUtil.isEmptyOrBlank(xiaoka_weixin_appId)) {
		xiaoka_weixin_appId = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_appId");
		// }
		return xiaoka_weixin_appId;
	}

	public void setXiaoka_weixin_appId(String xiaoka_weixin_appId) {
		this.xiaoka_weixin_appId = xiaoka_weixin_appId;
	}

	public String getXiaoka_weixin_secret() {
		// xiaoka_weixin_secret = getRedisParamValue("xiaoka_weixin_secret");
		// if (StringUtil.isEmptyOrBlank(xiaoka_weixin_secret)) {
		xiaoka_weixin_secret = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_secret");
		// }
		return xiaoka_weixin_secret;
	}

	public void setXiaoka_weixin_secret(String xiaoka_weixin_secret) {
		this.xiaoka_weixin_secret = xiaoka_weixin_secret;
	}

	public String getXiaoka_weixin_token() {
		// xiaoka_weixin_token = getRedisParamValue("xiaoka_weixin_token");
		// if (StringUtil.isEmptyOrBlank(xiaoka_weixin_token)) {
		xiaoka_weixin_token = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_token");
		// }
		return xiaoka_weixin_token;
	}

	public void setXiaoka_weixin_token(String xiaoka_weixin_token) {
		this.xiaoka_weixin_token = xiaoka_weixin_token;
	}

	public String getXiaoka_weixin_aesKey() {
		// xiaoka_weixin_aesKey = getRedisParamValue("xiaoka_weixin_aesKey");
		// if (StringUtil.isEmptyOrBlank(xiaoka_weixin_aesKey)) {
		xiaoka_weixin_aesKey = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_aesKey");
		// }
		return xiaoka_weixin_aesKey;
	}

	public void setXiaoka_weixin_aesKey(String xiaoka_weixin_aesKey) {
		this.xiaoka_weixin_aesKey = xiaoka_weixin_aesKey;
	}

	public String getXiaoka_weixin_oauth2redirectUri() {
		// xiaoka_weixin_oauth2redirectUri =
		// getRedisParamValue("xiaoka_weixin_oauth2redirectUri");
		// if (StringUtil.isEmptyOrBlank(xiaoka_weixin_oauth2redirectUri)) {
		xiaoka_weixin_oauth2redirectUri = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_oauth2redirectUri");
		// }
		return xiaoka_weixin_oauth2redirectUri;
	}

	public void setXiaoka_weixin_oauth2redirectUri(
			String xiaoka_weixin_oauth2redirectUri) {
		this.xiaoka_weixin_oauth2redirectUri = xiaoka_weixin_oauth2redirectUri;
	}

	public String getXiaoka_weixin_api_key() {
		// xiaoka_weixin_api_key = getRedisParamValue("xiaoka_weixin_api_key");
		// if (StringUtil.isEmptyOrBlank(xiaoka_weixin_api_key)) {
		xiaoka_weixin_api_key = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_api_key");
		// }
		return xiaoka_weixin_api_key;
	}

	public void setXiaoka_weixin_api_key(String xiaoka_weixin_api_key) {
		this.xiaoka_weixin_api_key = xiaoka_weixin_api_key;
	}

	public String getXiaoka_weixin_mch_id() {
		// xiaoka_weixin_mch_id = getRedisParamValue("xiaoka_weixin_mch_id");
		// if (StringUtil.isEmptyOrBlank(xiaoka_weixin_mch_id)) {
		xiaoka_weixin_mch_id = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_mch_id");
		// }
		return xiaoka_weixin_mch_id;
	}

	public void setXiaoka_weixin_mch_id(String xiaoka_weixin_mch_id) {
		this.xiaoka_weixin_mch_id = xiaoka_weixin_mch_id;
	}

	public Long getXiaoka_weixin_expiresTime() {
		// String expiresTime = getRedisParamValue("xiaoka_weixin_expiresTime");
		// if (StringUtil.isEmptyOrBlank(expiresTime)) {
		// expiresTime =
		// (String)SystemInfoInitAndHolder.getSystemInfo("xiaoka_weixin_expiresTime");
		// }
		// xiaoka_weixin_expiresTime = Long.valueOf(expiresTime);
		xiaoka_weixin_expiresTime = Long
				.valueOf((String) SystemInfoInitAndHolder
						.getSystemInfo("xiaoka_weixin_expiresTime"));
		return xiaoka_weixin_expiresTime;
	}

	public void setXiaoka_weixin_expiresTime(Long xiaoka_weixin_expiresTime) {
		this.xiaoka_weixin_expiresTime = xiaoka_weixin_expiresTime;
	}

	public String getTell_url() {
		// tell_url = getRedisParamValue("tell_url");
		// if (StringUtil.isEmptyOrBlank(tell_url)) {
		tell_url = (String) SystemInfoInitAndHolder.getSystemInfo("tell_url");
		// }
		return tell_url;
	}

	public void setTell_url(String tell_url) {
		this.tell_url = tell_url;
	}

	public String getNotify_url() {
		// notify_url = getRedisParamValue("notify_url");
		// if (StringUtil.isEmptyOrBlank(notify_url)) {
		notify_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("notify_url");
		// }
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getOrder_submit() {
		order_submit = (String) SystemInfoInitAndHolder
				.getSystemInfo("order_submit");
		return order_submit;
	}

	public void setOrder_submit(String order_submit) {
		this.order_submit = order_submit;
	}

	public String getOrder_pay() {
		order_pay = (String) SystemInfoInitAndHolder.getSystemInfo("order_pay");
		return order_pay;
	}

	public void setOrder_pay(String order_pay) {
		this.order_pay = order_pay;
	}

	public String getOrder_take() {
		order_take = (String) SystemInfoInitAndHolder
				.getSystemInfo("order_take");
		return order_take;
	}

	public void setOrder_take(String order_take) {
		this.order_take = order_take;
	}

	public String getOrder_cancle() {
		order_cancle = (String) SystemInfoInitAndHolder
				.getSystemInfo("order_cancle");
		return order_cancle;
	}

	public void setOrder_cancle(String order_cancle) {
		this.order_cancle = order_cancle;
	}

	public String getWx_business_toBind_url() {
		wx_business_toBind_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("wx_business_toBind_url");
		return wx_business_toBind_url;
	}

	public void setWx_business_toBind_url(String wx_business_toBind_url) {
		this.wx_business_toBind_url = wx_business_toBind_url;
	}

	public String getTemplateMessage_set_url() {
		templateMessage_set_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_set_url");
		return templateMessage_set_url;
	}

	public void setTemplateMessage_set_url(String templateMessage_set_url) {
		this.templateMessage_set_url = templateMessage_set_url;
	}

	public String getTg_templateMessage_set_url() {
		tg_templateMessage_set_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("tg_templateMessage_set_url");
		return tg_templateMessage_set_url;
	}

	public void setTg_templateMessage_set_url(String tg_templateMessage_set_url) {
		this.tg_templateMessage_set_url = tg_templateMessage_set_url;
	}

	public String getTemplateMessage_set_url_business() {
		templateMessage_set_url_business = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_set_url_business");
		return templateMessage_set_url_business;
	}

	public void setTemplateMessage_set_url_business(
			String templateMessage_set_url_business) {
		this.templateMessage_set_url_business = templateMessage_set_url_business;
	}

	public String getTemplateMessage_v2_set_url() {
		templateMessage_v2_set_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_v2_set_url");
		return templateMessage_v2_set_url;
	}

	public void setTemplateMessage_v2_set_url(String templateMessage_v2_set_url) {
		this.templateMessage_v2_set_url = templateMessage_v2_set_url;
	}

	public String getTg_notify_url() {
		tg_notify_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("tg_notify_url");
		return tg_notify_url;
	}

	public void setTg_notify_url(String tg_notify_url) {
		this.tg_notify_url = tg_notify_url;
	}

	public String getTg_tell_url() {
		tg_tell_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("tg_tell_url");
		return tg_tell_url;
	}

	public void setTg_tell_url(String tg_tell_url) {
		this.tg_tell_url = tg_tell_url;
	}

	public String getTg_group_success() {
		tg_group_success = (String) SystemInfoInitAndHolder
				.getSystemInfo("tg_group_success");
		return tg_group_success;
	}

	public void setTg_group_success(String tg_group_success) {
		this.tg_group_success = tg_group_success;
	}

	public String getTg_group_fail() {
		tg_group_fail = (String) SystemInfoInitAndHolder
				.getSystemInfo("tg_group_fail");
		return tg_group_fail;
	}

	public void setTg_group_fail(String tg_group_fail) {
		this.tg_group_fail = tg_group_fail;
	}

	public String getTg_group_weikuan() {
		tg_group_weikuan = (String) SystemInfoInitAndHolder
				.getSystemInfo("tg_group_weikuan");
		return tg_group_weikuan;
	}

	public void setTg_group_weikuan(String tg_group_weikuan) {
		this.tg_group_weikuan = tg_group_weikuan;
	}

	public String getTg_bujiaoweikuan_set_url() {
		tg_bujiaoweikuan_set_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("tg_bujiaoweikuan_set_url");
		return tg_bujiaoweikuan_set_url;
	}

	public void setTg_bujiaoweikuan_set_url(String tg_bujiaoweikuan_set_url) {
		this.tg_bujiaoweikuan_set_url = tg_bujiaoweikuan_set_url;
	}

	public Double getXiaoka_weixin_tg_league_scale() {
		String league_scale = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_tg_league_scale");
		if (!StringUtil.isEmptyOrBlank(league_scale)) {
			this.xiaoka_weixin_tg_league_scale = Double.valueOf(league_scale);
		} else {
			this.xiaoka_weixin_tg_league_scale = 0d;
		}
		return xiaoka_weixin_tg_league_scale;
	}

	public Double getXiaoka_weixin_tg_envoy_scale() {
		String league_scale = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_tg_envoy_scale");
		if (!StringUtil.isEmptyOrBlank(league_scale)) {
			this.xiaoka_weixin_tg_envoy_scale = Double.valueOf(league_scale);
		} else {
			this.xiaoka_weixin_tg_envoy_scale = 0d;
		}
		return xiaoka_weixin_tg_envoy_scale;
	}

	public String getXiaoka_weixin_tg_share_desc() {
		this.xiaoka_weixin_tg_share_desc = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_tg_share_desc");
		return xiaoka_weixin_tg_share_desc;
	}

	public String getXiaoka_weixin_tg_day() {
		xiaoka_weixin_tg_day = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_tg_day");
		return xiaoka_weixin_tg_day;
	}

	public void setXiaoka_weixin_tg_day(String xiaoka_weixin_tg_day) {
		this.xiaoka_weixin_tg_day = xiaoka_weixin_tg_day;
	}

	public String getWx_tuangou_league_toBind_url() {
		wx_tuangou_league_toBind_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("wx_tuangou_league_toBind_url");
		return wx_tuangou_league_toBind_url;
	}

	public String getXiaoka_weixin_baoming() {
		xiaoka_weixin_baoming = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_baoming");
		return xiaoka_weixin_baoming;
	}

	public void setXiaoka_weixin_baoming(String xiaoka_weixin_baoming) {
		this.xiaoka_weixin_baoming = xiaoka_weixin_baoming;
	}

	public String getXiaoka_people_cost() {
		xiaoka_people_cost = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_people_cost");
		return xiaoka_people_cost;
	}

	public void setXiaoka_people_cost(String xiaoka_people_cost) {
		this.xiaoka_people_cost = xiaoka_people_cost;
	}

	public String getXiaoka_page_myorder() {
		xiaoka_page_myorder = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_page_myorder");
		return xiaoka_page_myorder;
	}

	public void setXiaoka_page_myorder(String xiaoka_page_myorder) {
		this.xiaoka_page_myorder = xiaoka_page_myorder;
	}

	public String getXiaoka_weixin_targetdirectory_zhongbao() {
		xiaoka_weixin_targetdirectory_zhongbao = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_targetdirectory_zhongbao");
		return xiaoka_weixin_targetdirectory_zhongbao;
	}

	public void setXiaoka_weixin_targetdirectory_zhongbao(
			String xiaoka_weixin_targetdirectory_zhongbao) {
		this.xiaoka_weixin_targetdirectory_zhongbao = xiaoka_weixin_targetdirectory_zhongbao;
	}

	public String getXiaoka_hf() {
		xiaoka_hf = (String) SystemInfoInitAndHolder.getSystemInfo("xiaoka_hf");
		// System.out.println(xiaoka_hf);
		return xiaoka_hf;
	}

	public void setXiaoka_hf(String xiaoka_hf) {
		this.xiaoka_hf = xiaoka_hf;
	}

	public String getXiaoka_tg_hf() {
		xiaoka_tg_hf = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_tg_hf");
		return xiaoka_tg_hf;
	}

	public void setXiaoka_tg_hf(String xiaoka_tg_hf) {
		this.xiaoka_tg_hf = xiaoka_tg_hf;
	}

	public String getXiaoka_zb_hf() {
		xiaoka_zb_hf = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_zb_hf");
		return xiaoka_zb_hf;
	}

	public void setXiaoka_zb_hf(String xiaoka_zb_hf) {
		this.xiaoka_zb_hf = xiaoka_zb_hf;
	}

	public String getXiaoka_train_hf() {
		xiaoka_train_hf = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_train_hf");
		return xiaoka_train_hf;
	}

	public void setXiaoka_train_hf(String xiaoka_train_hf) {
		this.xiaoka_train_hf = xiaoka_train_hf;
	}

	public String getSystem_tx_bbs_remote_token() {
		system_tx_bbs_remote_token = (String) SystemInfoInitAndHolder
				.getSystemInfo("system_tx_bbs_remote_token");
		return system_tx_bbs_remote_token;
	}

	public void setSystem_tx_bbs_remote_token(String system_tg_zb_remote_token) {
		this.system_tx_bbs_remote_token = system_tg_zb_remote_token;
	}

	public String getSystem_tx_bbs_playMoneyUrl() {
		system_tx_bbs_playMoneyUrl = (String) SystemInfoInitAndHolder
				.getSystemInfo("system_tx_bbs_playMoneyUrl");
		return system_tx_bbs_playMoneyUrl;
	}

	public void setSystem_tx_bbs_applyMoneyUrl(String system_tg_zb_playMoneyUrl) {
		this.system_tx_bbs_playMoneyUrl = system_tg_zb_playMoneyUrl;
	}

	public String getSystem_tx_bbs_applyMoney_getToken() {
		system_tx_bbs_applyMoney_getToken = (String) SystemInfoInitAndHolder
				.getSystemInfo("system_tx_bbs_applyMoney_getToken");
		return system_tx_bbs_applyMoney_getToken;
	}

	public void setSystem_tx_bbs_applyMoney_getToken(
			String system_tg_zb_applyMoney_getToken) {
		this.system_tx_bbs_applyMoney_getToken = system_tg_zb_applyMoney_getToken;
	}

	public String getXiaoka_bbs_to_eshop_url() {
		xiaoka_bbs_to_eshop_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_bbs_to_eshop_url");
		System.out.println(xiaoka_bbs_to_eshop_url);
		return xiaoka_bbs_to_eshop_url;
	}

	public void setXiaoka_bbs_to_eshop_url(String xiaoka_bbs_to_eshop_url) {
		this.xiaoka_bbs_to_eshop_url = xiaoka_bbs_to_eshop_url;
	}

	public String getXkh_audit_success_template() {
		xkh_audit_success_template = (String) SystemInfoInitAndHolder
				.getSystemInfo("xkh_audit_success_template");
		return xkh_audit_success_template;
	}

	public void setXkh_audit_success_template(String xkh_audit_success_template) {
		this.xkh_audit_success_template = xkh_audit_success_template;
	}

	public String getTemplateMessage_to_home_url() {
		templateMessage_to_home_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_to_home_url");
		return templateMessage_to_home_url;
	}

	public void setTemplateMessage_to_home_url(
			String templateMessage_to_home_url) {
		this.templateMessage_to_home_url = templateMessage_to_home_url;
	}

	public String getXiaoka_weixin_wm_sendTemplateByOpenId() {
		xiaoka_weixin_wm_sendTemplateByOpenId = (String) SystemInfoInitAndHolder
				.getSystemInfo("xiaoka_weixin_wm_sendTemplateByOpenId");
		return xiaoka_weixin_wm_sendTemplateByOpenId;
	}

	public void setXiaoka_weixin_wm_sendTemplateByOpenId(
			String xiaoka_weixin_wm_sendTemplateByOpenId) {
		this.xiaoka_weixin_wm_sendTemplateByOpenId = xiaoka_weixin_wm_sendTemplateByOpenId;
	}

	public String getTemplateMessage_to_my_url() {
		templateMessage_to_my_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_to_my_url");
		return templateMessage_to_my_url;
	}

	public void setTemplateMessage_to_my_url(String templateMessage_to_my_url) {
		this.templateMessage_to_my_url = templateMessage_to_my_url;
	}

	public String getTemplateMessage_to_chatList_url() {
		templateMessage_to_chatList_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_to_chatList_url");
		return templateMessage_to_chatList_url;
	}

	public void setTemplateMessage_to_chatList_url(
			String templateMessage_to_chatList_url) {
		this.templateMessage_to_chatList_url = templateMessage_to_chatList_url;
	}

	public String getTemplateMessage_to_myOrder_url() {
		templateMessage_to_myOrder_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_to_myOrder_url");
		return templateMessage_to_myOrder_url;
	}

	public void setTemplateMessage_to_myOrder_url(
			String templateMessage_to_myOrder_url) {
		this.templateMessage_to_myOrder_url = templateMessage_to_myOrder_url;
	}

	public String getTextMessage_jztf_putContent_url() {
		textMessage_jztf_putContent_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("textMessage_jztf_putContent_url");
		return textMessage_jztf_putContent_url;
	}

	public void setTextMessage_jztf_putContent_url(
			String textMessage_jztf_putContent_url) {
		this.textMessage_jztf_putContent_url = textMessage_jztf_putContent_url;
	}

	public String getTextMessage_to_sendText() {
		textMessage_to_sendText = (String) SystemInfoInitAndHolder
				.getSystemInfo("textMessage_to_sendText");
		return textMessage_to_sendText;
	}

	public void setTextMessage_to_sendText(String textMessage_to_sendText) {
		this.textMessage_to_sendText = textMessage_to_sendText;
	}

	public String getTemplateMessage_to_OrderInfo_url() {
		templateMessage_to_OrderInfo_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_to_OrderInfo_url");
		return templateMessage_to_OrderInfo_url;
	}

	public void setTemplateMessage_to_OrderInfo_url(
			String templateMessage_to_OrderInfo_url) {
		this.templateMessage_to_OrderInfo_url = templateMessage_to_OrderInfo_url;
	}

	public String getTemplateMessage_to_numberChange_url() {
		templateMessage_to_numberChange_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_to_numberChange_url");
		return templateMessage_to_numberChange_url;
	}

	public void setTemplateMessage_to_numberChange_url(
			String templateMessage_to_numberChange_url) {
		this.templateMessage_to_numberChange_url = templateMessage_to_numberChange_url;
	}

	public String getTemplateMessage_to_diplomatChange_url() {
		templateMessage_to_diplomatChange_url = (String) SystemInfoInitAndHolder
				.getSystemInfo("templateMessage_to_diplomatChange_url");
		return templateMessage_to_diplomatChange_url;
	}

	public void setTemplateMessage_to_diplomatChange_url(
			String templateMessage_to_diplomatChange_url) {
		this.templateMessage_to_diplomatChange_url = templateMessage_to_diplomatChange_url;
	}
	

}
