package com.jzwl.common.constant;

import java.io.Serializable;
import java.util.Map;

/**
 * 接口返回的处理结果类
 * @author cct
 */
public class Result implements Serializable {
	/** 状态。true:成功/false：失败 */
	private boolean flag;
	/** 相关信息 */
	private String msg;
	/** 数据 */
	private Map<String, Object> data;
	
	/**
	 * 正常
	 * @param flag 状态
	 * @param msg 信息
	 * @param data 数据
	 */
	public Result(boolean flag, String msg, Map<String, Object> data) {
		super();
		this.flag = flag;
		this.msg = msg;
		this.data = data;
	}
	
	/**
	 * 失败
	 * @param msg
	 */
	public Result(String msg) {
		super();
		this.flag = false;
		this.msg = msg;
		this.data = null;
	}
	
	/**
	 * 成功
	 * @param data
	 */
	public Result(Map<String, Object> data) {
		super();
		this.flag = true;
		this.msg = "";
		this.data = data;
	}
	/**
	 * 成功
	 * @param data
	 */
	public Result() {
		super();
		this.flag = true;
		this.msg = "";
		this.data = null;
	}
	
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Result [flag=" + flag + ", msg=" + msg + ", data=" + data + "]";
	}
	
}
