package com.jzwl.common.cache;

import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import com.alibaba.druid.support.logging.Log;

/**
 * 封装redis 缓存服务器服务接口
 * 
 * @author hk
 * 
 *         2012-12-16 上午3:09:18
 */
@Component
@SuppressWarnings("unchecked")
public class RedisServiceImpl implements RedisService {

	private static String redisCode = "utf-8";

	// private static String redisCode = "gbk";
	// private static String redisCode = "iso8859-1";
	
	private RedisServiceImpl() {

	}

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	/**
	 * @param key
	 */
	public long del(final String... keys) {
		return (Long) redisTemplate.execute(new RedisCallback() {
			public Long doInRedis(RedisConnection connection)
					throws DataAccessException {
				long result = 0;
				for (int i = 0; i < keys.length; i++) {
					result = connection.del(keys[i].getBytes());
				}
				return result;
			}
		});
	}

	/**
	 * @param key
	 * @param value
	 * @param liveTime
	 */
	public void set(final byte[] key, final byte[] value, final long liveTime) {
		try {
			redisTemplate.execute(new RedisCallback() {
				public Long doInRedis(RedisConnection connection) {
					connection.set(key, value);
					if (liveTime > 0) {
						connection.expire(key, liveTime);
					}
					return 1L;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	/**
	 * @param key
	 * @param liveTime
	 */
	public void expire(final byte[] key, final long liveTime) {
		try {
			redisTemplate.execute(new RedisCallback() {
				public Long doInRedis(RedisConnection connection)
						throws DataAccessException {
					if (liveTime > 0) {
						connection.expire(key, liveTime);
					}
					return 1L;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @param key
	 * @param liveTime
	 */
	public void expire(String key, long liveTime) {
		this.expire(key.getBytes(), liveTime);
	}

	/**
	 * @param key
	 * @param value
	 * @param liveTime
	 */
	public void set(String key, String value, long liveTime) {
		this.set(key.getBytes(), value.getBytes(), liveTime);
	}

	/**
	 * @param key
	 * @param value
	 */
	public void set(String key, String value) {
		this.set(key, value, 0L);
	}

	/**
	 * @param key
	 * @param value
	 */
	public void set(String key, Object obj) {
		ValueOperations<String, Object> valueops = redisTemplate.opsForValue();
		valueops.set(key, obj);
	}

	/**
	 * @param key
	 * @param value
	 */
	public void set(byte[] key, byte[] value) {
		this.set(key, value, 0L);
	}

	/**
	 * @param key
	 * @return
	 */
	public String get(final String key) {
		return (String) redisTemplate.execute(new RedisCallback() {
			public String doInRedis(RedisConnection connection) {

				try {
					if (com.jzwl.common.constant.GlobalConstant.REDIS_CACHE_CONTROL == 1) {
						byte[] result = connection.get(key.getBytes());
						if (null != result) {
							return new String(result, redisCode);
						} else {
							return null;
						}
					} else {
						return null;
					}

				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}

			}
		});
	}

	/**
	 * 
	 */
	public Object getObj(String key) {
		ValueOperations<String, Object> valueops = redisTemplate.opsForValue();
		Object obj = valueops.get(key);
		return obj;
	}

	/**
	 * @param pattern
	 * @return
	 * @return
	 */
	public Set<String> Setkeys(String pattern) {
		return redisTemplate.keys(pattern);

	}

	/**
	 * @param key
	 * @return
	 */
	public boolean exists(final String key) {
		return (Boolean) redisTemplate.execute(new RedisCallback() {
			public Boolean doInRedis(RedisConnection connection)
					throws DataAccessException {
				return connection.exists(key.getBytes());
			}
		});
	}

	/**
	 * @return
	 */
	public String flushDB() {
		return (String) redisTemplate.execute(new RedisCallback() {
			public String doInRedis(RedisConnection connection)
					throws DataAccessException {
				connection.flushDb();
				return "ok";
			}
		});
	}

	/**
	 * @return
	 */
	public long dbSize() {
		return (Long) redisTemplate.execute(new RedisCallback() {
			public Long doInRedis(RedisConnection connection)
					throws DataAccessException {
				return connection.dbSize();
			}
		});
	}

	/**
	 * @return
	 */
	public String ping() {
		return (String) redisTemplate.execute(new RedisCallback() {
			public String doInRedis(RedisConnection connection)
					throws DataAccessException {

				return connection.ping();
			}
		});
	}

	@Override
	public Set<String> keys(String pattern) {
		String str = "";
		return redisTemplate.keys(pattern);
	}
}