package com.jzwl.common.cache;

import java.util.Set;
import java.util.regex.Pattern;

/**
 * redis 的操作开放接口
 * 
 * @author hk
 * 
 *         2013-3-31 下午7:25:42
 */
public interface RedisService {
	
	
	// redis key pre
	//激活账号验证码前缀
	public static final String weixin_pre_active="weixin.active.";
	//激活账号验证码前缀
	public static final String weixin_pre_pwd="weixin.pwd.";

	
	/**
	 * 通过key删除
	 * 
	 * @param key
	 */
	public abstract long del(String... keys);

	/**
	 * 添加key value 并且设置存活时间(byte)
	 * 
	 * @param key
	 * @param value
	 * @param liveTime
	 */
	public abstract void set(byte[] key, byte[] value, long liveTime);

	/**
	 * 添加key value 并且设置存活时间
	 * 
	 * @param key
	 * @param value
	 * @param liveTime
	 *            单位秒
	 */
	public abstract void set(String key, String value, long liveTime);

	/**
	 * 添加key value
	 * 
	 * @param key
	 * @param value
	 */
	public abstract void set(String key, String value);

	/**
	 * 
	 * @param key
	 * @param obj
	 */
	public void set(String key, Object obj);
	
	/**
	 * 添加key value (字节)(序列化)
	 * 
	 * @param key
	 * @param value
	 */
	public abstract void set(byte[] key, byte[] value);

	/**
	 * @param key
	 * @param liveTime
	 */
	public void expire(String key, long liveTime);

	/**
	 * 获取redis value (String)
	 * 
	 * @param key
	 * @return
	 */
	public abstract String get(String key);
	
	/**
	 * 获取redis value (Object)
	 * 
	 * @param key
	 * @return
	 */
	public abstract Object getObj(String key);

	/**
	 * 通过正则匹配keys
	 * 
	 * @param pattern
	 * @return
	 * @return
	 */
	public abstract Set Setkeys(String pattern);

	/**
	 * 检查key是否已经存在
	 * 
	 * @param key
	 * @return
	 */
	public abstract boolean exists(String key);

	/**
	 * 清空redis 所有数据
	 * 
	 * @return
	 */
	public abstract String flushDB();

	/**
	 * 查看redis里有多少数据
	 */
	public abstract long dbSize();

	/**
	 * 检查是否连接成功
	 * 
	 * @return
	 */
	public abstract String ping();
	
	/**
	 * 根据正则表达式，查询对应的value值集合
	 * @param pattern
	 * @return
	 */
	public abstract Set<String> keys(String pattern);

}