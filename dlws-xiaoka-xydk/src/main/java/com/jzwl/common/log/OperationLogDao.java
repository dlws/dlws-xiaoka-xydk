package com.jzwl.common.log;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
@Repository("OperationLog")
public class OperationLogDao{
	/**
	 * 操作记录
	 * @param map 操作参数
	 * @param operation 执行的操作
	 * @param tabName	操作表名
	 * @return 
	 */
	public void operationLog(Map<String, Object> map,String operation,String tabName,String id){
		
		String currentUserName = GlobalConstant.getCurrentUserName();//获取用户名
		Date operationTime = new Date();							//获取操作时间
		Map<String,Object> operationMap = new HashMap<String, Object>();//存放操作所执行的参数，所执行的操作，执行的时间。

		operationMap.put("sys_uid", currentUserName);
		operationMap.put("sys_action", operation);//所执行的操作
		operationMap.put("sys_tableName", tabName);//操作的表名
		operationMap.put("sys_operationTime", operationTime);//操作时间
		operationMap.put("sys_idName", id);
		operationMap.put("sys_idValue",map.get(id));
		operationMap.putAll(map);
	}
}
