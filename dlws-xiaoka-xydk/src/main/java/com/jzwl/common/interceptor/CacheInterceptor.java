package com.jzwl.common.interceptor;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.json.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.cache.RedisService;
import com.jzwl.common.constant.Constants;
//import com.jzwl.common.utils.HttpClientUtil;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;

public class CacheInterceptor implements HandlerInterceptor{
	
	private static Logger log = LoggerFactory.getLogger(CacheInterceptor.class);
	@Autowired
	private Constants constants;
	@Autowired
	private HomeService homeService;
	@Autowired
	private RedisService redisService;
	
	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		log.info("++++++++进入拦截器++++++++");
		
		//统计人数的缓存设置
		String homeCount = redisService.get(GlobalConstant.CACHE_XKH_HOME_COUNT);
		if (null != homeCount && "".equals(homeCount)) {// 如果为空，则从数据库中进行查询
			log.info("+++++++++++++++++++++++++++++++++++++++++查询数据库+++++++++++++++++++++++");
			Map<String, Object> map = homeService.getUserAllNum();//获取用户总数
			Map<String, Object> map_skill_num = homeService.getUserSkillAllNum();//获取所有技能的总数
			DecimalFormat df = new DecimalFormat("0000");
			String str2 = df.format(Integer.parseInt(map.get("allNum").toString())+Integer.parseInt(map_skill_num.get("allNum").toString()));
			redisService.set(GlobalConstant.CACHE_XKH_HOME_COUNT,str2);
		}
		//轮播图的缓存设置
		String bannerJson = redisService.get(GlobalConstant.CACHE_XKH_HOME_COUNT);
		if (null != bannerJson && "".equals(bannerJson)) {// 如果为空，则从数据库中进行查询
			log.info("+++++++++++++++++++++++++++++++++++++++++轮播图的集合查询数据库+++++++++++++++++++++++");
			Map<String, Object> bannerMap = new HashMap<String, Object>();
			//获取轮播图的集合
			List<BannerInfo>  bannerInfos = homeService.getBannerInfoList();
			if (bannerInfos.size() > 0) {
				bannerMap.put("firstPic", bannerInfos.get(0));
				bannerMap.put("lastPic", bannerInfos.get(bannerInfos.size() - 1));
			}
			bannerMap.put("list", bannerInfos);
			String jBanner = Json.toJson(bannerMap);
			redisService.set(GlobalConstant.CACHE_XKH_HOME_BANNER,jBanner);
		}
		//分类的缓存设置
		String cla = redisService.get(GlobalConstant.CACHE_XKH_HOME_CLASS);
		if (null != cla && "".equals(cla)) {// 如果为空，则从数据库中进行查询
			log.info("+++++++++++++++++++++++++++++++++++++++++分类集合查询数据库+++++++++++++++++++++++");
			Map<String, Object> bannerMap = new HashMap<String, Object>();
			//获取轮播图的集合
			List<BannerInfo>  bannerInfos = homeService.getBannerInfoList();
			if (bannerInfos.size() > 0) {
				bannerMap.put("firstPic", bannerInfos.get(0));
				bannerMap.put("lastPic", bannerInfos.get(bannerInfos.size() - 1));
			}
			bannerMap.put("list", bannerInfos);
			String jBanner = Json.toJson(bannerMap);
			redisService.set(GlobalConstant.CACHE_XKH_HOME_BANNER,jBanner);
		}
		
		return true;

	}
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
