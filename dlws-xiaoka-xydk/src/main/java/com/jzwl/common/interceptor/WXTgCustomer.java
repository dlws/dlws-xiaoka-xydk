package com.jzwl.common.interceptor;

import java.util.Date;

/**
 * 微信顾客
 * @author Administrator
 *
 */
public class WXTgCustomer implements java.io.Serializable{
	/** 顾客的微信号 */
	private String openId;
	/** 微信名称 */
	private String wxname;
	/** 顾客姓名 */
	private String name;
	/** 顾客手机号 */
	private String phone;
	/** 标签 */
	private String labelId;
	/** 性别 */
	private String sex;
	/** 地址 */
	private String address;
	/** 国家代码 */
	private String country;
	/** 省份代码 */
	private String province;
	/** 城市代码 */
	private String city;
	/** 客户纬度 */
	private Double longitude;
	/** 客户纬度 */
	private Double latitude;
	/** 是否为黑名单 */
	private Integer isBlack;
	/** 状态 */
	private Integer status;
	/** 是否删除 */
	private Integer isDelete;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date modifiedTime;
	/** 成为粉丝时间 */
	private Date toFansTime;
	/** 备注信息 */
	private String remark;
	/**
	 * 收货人姓名
	 */
	private String reciveName;
	/**
	 * 送货地址
	 */
	private String deliveryAddr;
	/**
	 * 收货人电话
	 */
	private String recivePhone;
	
	private Long tgSchoolId;
	
	private Long cityId;
	
	private String cityName;
	
	private String schoolName;
	
	private String headImgUrl;
	
	
	
	public Date getToFansTime() {
		return toFansTime;
	}
	public void setToFansTime(Date toFansTime) {
		this.toFansTime = toFansTime;
	}
	public String getHeadImgUrl() {
		return headImgUrl;
	}
	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}
	public String getReciveName() {
		return reciveName;
	}
	public void setReciveName(String reciveName) {
		this.reciveName = reciveName;
	}
	public String getDeliveryAddr() {
		return deliveryAddr;
	}
	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}
	public String getRecivePhone() {
		return recivePhone;
	}
	public void setRecivePhone(String recivePhone) {
		this.recivePhone = recivePhone;
	}
	/** 注册商户的Id(有用吗？) */
	private Long ownerId;
	
	/** 店铺的id标注最后一次进入的店铺id*/
	
	private Long shopId;
	
	/* getter/setter */
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getWxname() {
		return wxname;
	}
	public void setWxname(String wxname) {
		this.wxname = wxname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLabelId() {
		return labelId;
	}
	public void setLabelId(String labelId) {
		this.labelId = labelId;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Integer getIsBlack() {
		return isBlack;
	}
	public void setIsBlack(Integer isBlack) {
		this.isBlack = isBlack;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getTgSchoolId() {
		return tgSchoolId;
	}
	public void setTgSchoolId(Long tgSchoolId) {
		this.tgSchoolId = tgSchoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	
	
}

