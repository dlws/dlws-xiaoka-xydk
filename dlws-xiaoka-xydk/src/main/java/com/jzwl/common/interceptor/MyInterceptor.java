package com.jzwl.common.interceptor;

import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.cache.RedisService;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.StringUtil;
//import com.jzwl.common.utils.HttpClientUtil;

public class MyInterceptor implements HandlerInterceptor{
	private static Logger log = LoggerFactory.getLogger(MyInterceptor.class);
	@Autowired
	private Constants constants;
	@Autowired
	private RedisService redisService;
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		log.info("++++++++MyInterceptor进入拦截器++++++++");
		WXTgCustomer customer = null;
//		request.getSession().setAttribute("dkOpenId", "oWGZPs8z0PqrJcj5EZc75BllVnPM");
		if(null!=request.getParameter("openId")){//判断是否是授权完成后回调过来的地址
			log.info("++++++++++++++++++++++++111111");
			log.info("++++++++++++++++openId="+request.getParameter("openId"));
			String openId = request.getParameter("openId");
			request.getSession().setAttribute("dkOpenId", openId);
			//去掉链接中的openId
			String url = request.getRequestURL().toString();
			String params = "";
			Enumeration<String> paramKeys = request.getParameterNames();
			while (paramKeys.hasMoreElements()) {
				String key = paramKeys.nextElement();
				log.info("+++++++++++++++++++++++++++++key="+key);
				if(!"null".equals(key)&&!"openId".equals(key)){
					params = params + key + "=" + (request.getParameter(key)).toString() + "&" ;
					log.info("++++++++++++++++++++++++params="+params);
				}
			}
			if(StringUtil.isNotEmptyOrBlank(params)){
				params = params.substring(0, params.length()-1);
				url = url + "?" + params;
				log.info("++++++++++++++++++++++++++++++++++++url="+url);
			}
			log.info("++++++++++++++++++++++++1222222url"+url);
			response.sendRedirect(url);
			return true;
		}
		// >/dlws-xiaoka-zb/center/toCenterList.html<
		if(null!=request.getSession().getAttribute("dkOpenId")){
			log.info("+++++++openId"+request.getSession().getAttribute("dkOpenId"));
			String openId = (String) request.getSession().getAttribute("dkOpenId");
			
			return true;
		}else{
			log.info("++++++++++++++++++++++++直接去授权");
			String reqUrl = request.getRequestURL().toString();
			
			String queryString = request.getQueryString();
			if(queryString != null&&StringUtil.isNotEmptyOrBlank(queryString)){
				reqUrl = reqUrl + "?" + queryString;
			}
			log.info("++++++++回调地址++++++++"+reqUrl);
			log.info("++++++++++++++user_agent+==="+request.getHeader("user-agent"));
			String reqUrlEcod = URLEncoder.encode(reqUrl);
			String authUrl = constants.getXiaoka_weixin_bbs_toAuto()+"?systemFlag=xydkSystemAuth&backUrl="+reqUrlEcod;//去授权
			log.info("++++++++去授权++++++++"+authUrl);
			//HttpClientUtil.toAuth(authUrl);
			log.info("++++++++++++++++++++++++33333"+authUrl);
			response.sendRedirect(authUrl);
			return false;
				
		}
	//	return true;
	}
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
