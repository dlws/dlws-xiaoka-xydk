package com.jzwl.common.init.photodownload;

/**
 * 此为优化图片上传类
 * @author Administrator
 *
 */
public class ImageChangeBean {

	/**
	 * 微信服务器中的 weChatImageUrl
	 */
	private String weChatImageUrl;
	
	private String localServerImageUrl;
	
	
	public String getWeChatImageUrl() {
		return weChatImageUrl;
	}


	public void setWeChatImageUrl(String weChatImageUrl) {
		this.weChatImageUrl = weChatImageUrl;
	}


	public String getLocalServerImageUrl() {
		return localServerImageUrl;
	}


	public void setLocalServerImageUrl(String localServerImageUrl) {
		this.localServerImageUrl = localServerImageUrl;
	}


	public ImageChangeBean() {
		// TODO Auto-generated constructor stub
	}

}
