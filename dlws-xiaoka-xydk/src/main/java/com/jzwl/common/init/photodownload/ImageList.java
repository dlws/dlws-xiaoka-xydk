package com.jzwl.common.init.photodownload;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImageList {

	private static  List<ImageChangeBean> list;
	
	static{
		
		 list = Collections.synchronizedList(new ArrayList<ImageChangeBean>()); 
	}

	public static List<ImageChangeBean> getList() {
		return list;
	}

	public static void setList(List<ImageChangeBean> list) {
		ImageList.list = list;
	}
	

}
