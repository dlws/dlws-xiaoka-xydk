package com.jzwl.common.utils;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

/**
 *
 */
public class DateUtil {
	public static final SimpleDateFormat FORMAT_FULL =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	/**一天代表的毫秒数*/
	private static final int MILLIS_OF_DAY = 1000*60*60*24 ;
	/**4位年*/
	public static final String FORMAT_YYYY = "yyyy" ; 
	
	/**年月，4位年，2位月*/
	public static final String FORMAT_YYYYMM = "yyyyMM" ; 
	
	/**年月日格式*/
	public static final String FORMAT_YYYYMMDD = "yyyyMMdd" ;
	
	/**两位的天*/
	public static final String FORMAT_DD = "dd" ;
	/**年月日时分秒格式*/
	public static final String FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss" ;
	
	public static String getDate() {
		Calendar c = Calendar.getInstance();

		String year = String.valueOf(c.get(Calendar.YEAR));
		String month = String.valueOf(c.get(Calendar.MONTH) + 1);
		String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
		String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		String mins = String.valueOf(c.get(Calendar.MINUTE));
		String sec = String.valueOf(c.get(Calendar.SECOND));

		StringBuffer sbBuffer = new StringBuffer();
		sbBuffer.append(year + "-" + month + "-" + day + " " + hour + ":"
				+ mins + ":" + sec);

		return sbBuffer.toString();
	}
	
	public static String formatDate(Date time){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(time);
	}
	
	public static String formatDay(Date time){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(time);
	}
	
	/**
	 * 获取当前日期，只要年月日
	 * @return
	 */
	public static String getDay(){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(new Date());
	}
	
	//获取当前时间的下一天
	public static String getDateNext() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DAY_OF_MONTH, 1);
		String year = String.valueOf(c.get(Calendar.YEAR));
		String month = String.valueOf(c.get(Calendar.MONTH) + 1);
		String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
		String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		String mins = String.valueOf(c.get(Calendar.MINUTE));
		String sec = String.valueOf(c.get(Calendar.SECOND));

		StringBuffer sbBuffer = new StringBuffer();
		sbBuffer.append(year + "-" + month + "-" + day + " " + hour + ":"
				+ mins + ":" + sec);

		return sbBuffer.toString();
	}
	
	//获取当前时间的下一天(rerurn yyyy-MM-dd)
	public static String getDayNext() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DAY_OF_MONTH, 1);
		String year = String.valueOf(c.get(Calendar.YEAR));
		String month = String.valueOf(c.get(Calendar.MONTH) + 1);
		String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
		String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		String mins = String.valueOf(c.get(Calendar.MINUTE));
		String sec = String.valueOf(c.get(Calendar.SECOND));

		StringBuffer sbBuffer = new StringBuffer();
		sbBuffer.append(year + "-" + month + "-" + day );

		return sbBuffer.toString();
	}	
	/**
	 * 获取当前时间，如：12:08:05
	 * @return
	 */
	public static Time getCurrTime(){
		Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int mins = c.get(Calendar.MINUTE);
		int sec = c.get(Calendar.SECOND);
		return new Time(hour,mins,sec);
	}
	
	/**
	 * 比较时间，如：10:06:11和 12:08:05返回1
	 * @param t1
	 * @param t2
	 * @return 如果t1和t2相等，则返回0; 如果t1比t2早，则返回1; 如果t1比t2晚，则返回2
	 */
	public static int compareTime(Time t1,Time t2){
		if ( t1.getHours() < t2.getHours() ) {
			return 1;
		} else if( t1.getHours() == t2.getHours() ){
			if ( t1.getMinutes() < t2.getMinutes() ) {
				return 1;
			} else if( t1.getMinutes() == t2.getMinutes() ){
				if ( t1.getSeconds() < t2.getSeconds() ) {
					return 1;
				} else if( t1.getSeconds() == t2.getSeconds() ){
					return 0;
				} else {// if ( t1.getSeconds() > t2.getSeconds() ) 
					return 2;
				}
			} else {// if ( t1.getMinutes() > t2.getMinutes() ) 
				return 2;
			}
		} else {// if ( t1.getHours() > t2.getHours() ) 
			return 2;
		}
	}
	
	 /**
	  * 返回指定的日期加上n天后的日期
	  * @param date
	  * @param days 
	  * @return
	  */
    public static Date addDay(Date date,Integer days) {
	    Calendar c = Calendar.getInstance();
	    c.setTime(date) ;
	    c.add(Calendar.DAY_OF_MONTH, days);
	    return c.getTime();  
    }
    
    /**
     * 返回指定的日期加上n分钟后的日期
     * @param date
     * @param minute
     * @return
     */
    public static Date addMinute(Date date,Integer minute) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date) ;
    	c.add(Calendar.MINUTE, minute);
    	return c.getTime();  
    }
    
    /**
     * 返回指定的日期加上n小时后的日期
     * @param date
     * @param hour
     * @return
     */
    public static Date addHour(Date date,Integer hour) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date) ;
    	c.add(Calendar.HOUR, hour);
    	return c.getTime();  
    }
    
    /**根据日期字符串中的数字个数判断使用对应的日期格式化方式*/
	private static String getFormatPattern(String dateString){
		int len = dateString.length() ;
		String formatPattern = null ;
		
		switch(len){
		case 2:
			formatPattern =  FORMAT_DD ;
			break ;
		case 4:
			formatPattern = FORMAT_YYYY ;
			break ;
		case 6:
			formatPattern = FORMAT_YYYYMM ;
			break ;
		case 8:
			formatPattern = FORMAT_YYYYMMDD ;
			break ;
		case 14:
			formatPattern = FORMAT_YYYYMMDDHHMMSS ;
			break ;
		default:
			throw new IllegalArgumentException("日期格式不正确") ;
		}
		return formatPattern ;
	}
    
	/**
	 * 返回两个日期之间相差的天数，如果第1个日期小于第二个日期，将返回正数，否则返回负数。
	 * 使用yyyyMMdd格式化日期
	 */
	public static int daysBetween(String datePre,String dateLatter){
		datePre = StringUtil.drawNumber(datePre) ;
		dateLatter = StringUtil.drawNumber(dateLatter) ;
		
		if(datePre.length() != dateLatter.length()){
			throw new IllegalArgumentException("两个日期的格式要相同") ;
		}
		
		String formatPattern = getFormatPattern(datePre) ;
		return daysBetween(datePre,dateLatter,formatPattern) ; 
	}
	
	/**
	 * 返回两个日期之间相差的天数，如果第1个日期小于第二个日期，将返回正数，否则返回负数。
	 * 使用指定的格式formatPattern，格式化日期
	 */
	public static int daysBetween(String datePre,String dateLatter,String formatPattern){
		Calendar pre = parse2Calendar(datePre,formatPattern) ;
		Calendar latter = parse2Calendar(dateLatter,formatPattern) ;
		return daysBetween(pre,latter) ; 
	}
	
	/**
	 * 将字符串表示的日期转化为Calendar
	 */
	public static Calendar parse2Calendar(String dateString,String formatPattern){
		Date date = parse(dateString,formatPattern) ;
		Calendar calendar = Calendar.getInstance() ;
		calendar.setTime(date) ;
		return calendar ;
	}
	
	public static Date parse(String dateStr,String formatPattern){
		if(StringUtil.isEmpty(dateStr)){
			throw new IllegalArgumentException("日期字符串不能为空") ;
		}
		Date date = null ;
		DateFormat dateFormat = new SimpleDateFormat(formatPattern) ;
		
		try {
			date = dateFormat.parse(dateStr) ;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
        return date;
    }
	
	/**
	 * 返回两个Calendar之间相差的天数,使用数学的方式计算
	 */
	public static int daysBetween(Calendar pre,Calendar latter){
		long millis = millisBetween(pre,latter) ;
		return (int) (millis/MILLIS_OF_DAY) ;
	}
	
	public static long millisBetween(Calendar pre,Calendar latter){
		long preMillis = pre.getTimeInMillis() ;
		long latterMillis = latter.getTimeInMillis() ;
		long millis = latterMillis - preMillis ;
		return millis ;
	}
	
	/**
	 * 返回两个日期之间相差的天数，如果第1个日期小于第二个日期，将返回正数，否则返回负数。
	 * 使用yyyyMMdd格式化日期
	 */
	public static int daysBetween(Date datePre,Date dateLatter){
		String pre = FORMAT_FULL.format(datePre) ;
		String latter = FORMAT_FULL.format(dateLatter) ;
		return daysBetween(pre,latter) ; 
	}
	
	public static Long millisTime(Date endTime){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long between = 0;
		  
		try {
			java.util.Date  begin = sdf.parse(sdf.format(endTime));
			java.util.Date end = sdf.parse(sdf.format(new Date()));
            between = (end.getTime() - begin.getTime());// 得到两者的毫秒数
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return between;
	}
	
	
	
    public static String numToUpper(int num) {    
        // String u[] = {"零","壹","贰","叁","肆","伍","陆","柒","捌","玖"};    
        String u[] = { "〇", "一", "二", "三", "四", "五", "六", "七", "八", "九" };    
        char[] str = String.valueOf(num).toCharArray();    
        String rstr = "";    
        for (int i = 0; i < str.length; i++) {    
            rstr = rstr + u[Integer.parseInt(str[i] + "")];    
        }    
        return rstr;    
    }  
	
	
	//auto-code-end
		public static void main(String[] args) {
			
			Map<String,Object> map1 = new HashMap<String,Object>();
			map1.put("allPhoto", "12");
			map1.put("allPhoto1", "13");
			map1.put("allPhoto2", "14");
			String str=JSON.toJSON(map1).toString();
			
			Map<String,Object> maps = (Map<String,Object>)JSON.parse(str);  
			System.out.println(getMinValue(maps));
			
			
	            
		}
		
		 public static Object getMinValue(Map<String,Object> map) {
			 if (map == null) return null;
			 Collection<Object> c = map.values();
			 Object[] obj = c.toArray();
			 Arrays.sort(obj);
			 return obj[obj.length-1];
		 }
		
		/**
		 * @param num1 被减数
		 * @param num2 减数
		 * @return 返回Long 类型
		 */
		public static long subtracDateL(Date num1,Date num2){
			
			long count = 0;
			
			long meiosis = num1.getTime();
			long minuend = num2.getTime();
			
			count = meiosis - minuend;
			
			return count;
		}
		
		/**
		 * @param num1 被减数 结束时间
		 * @param num2 减数 当前时间
		 * @return 返回String 类型
		 */
		public static String subtracDateS(Date d1,Date d2){
			
			String timer = "";
			long date = d1.getTime() - d2.getTime();   
		    long day = date / (1000 * 60 * 60 * 24);    
		    long hour = (date / (1000 * 60 * 60) - day * 24);    
		    long min = ((date / (60 * 1000)) - day * 24 * 60 - hour * 60);  
		    long s = (date/1000 - day*24*60*60 - hour*60*60 - min*60);  
		    if(day<=0){
		    	timer=""+hour+"小时"+min+"分"+s+"秒";
		    }else{
		    	timer=""+day+"天"+hour+"小时"+min+"分"+s+"秒";
		    }
		    if(date<=0){
		    	timer="0";
		    }
		    
		    return timer;
		}
		/**
		 * 
		 * 项目名称：managerorder
		 * 描述：日期转换成字符串
		 * 创建人： sx
		 * 创建时间： 2017年4月6日
		 * 标记：
		 * @param date
		 * @param dateformat
		 * @return
		 * @version
		 */
		public static String dateToStr(Date date, String dateformat) {
			if(null==date){
				return null;
			}
			SimpleDateFormat format = new SimpleDateFormat(dateformat);
			String str = format.format(date);
			return str;
		}
		
		public static Date strToDate(String str, String dateFormat) {
			SimpleDateFormat format = new SimpleDateFormat(dateFormat);
			Date date = null;
			try {
				date = format.parse(str);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return date;
		}
}
