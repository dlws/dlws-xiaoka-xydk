package com.jzwl.common.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/*import com.jzwl.goods.goodsmanager.pojo.PlatCategory;
import com.jzwl.goods.goodsmanager.service.PlatCategoryService;*/

@Component
public class ReadWriteExcelUtilImport {
	
	
/*	@Autowired
	private PlatCategoryService platCategoryService;
	*/
	
	/**
	 * 验证Excel的格式
	 */
	public  boolean validateExcel(String fileName,String rightHead) throws FileNotFoundException, IOException {  
    
    	 HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(fileName));
    	 HSSFSheet   sheet   =   workbook.getSheetAt(0);
    	 String headStr = "";
    	 boolean flag = false;
    	 HSSFRow   row   =   sheet.getRow(0);
		 for(int j=0;j<row.getPhysicalNumberOfCells();j++){
			 headStr += row.getCell(j);
		 }
		 
		 if(headStr.equals(rightHead)){
			 flag = true;
		 }
		 return flag;
    }
	
	
	
    /** 
     * 從excel文件中讀取所有的內容 
     *  
     * @param file 
     *            excel文件  
     * @return excel文件的內容 
     * @throws IOException 
     * @throws FileNotFoundException 
     */  
    public  List<Map<String,Object>> readExcel(String fileName,String citycode) throws FileNotFoundException, IOException {  
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
         
        //try {  
            // 构造Workbook（工作薄）对象  
        	HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(fileName));
        	 HSSFSheet   sheet   =   workbook.getSheetAt(0);
        	 for(int i=1;i<sheet.getPhysicalNumberOfRows();i++){
        		 Map<String,Object> map = new HashMap<String,Object>();
        		 HSSFRow   row   =   sheet.getRow(i);
        		 for(int j=0;j<row.getPhysicalNumberOfCells();j++){
                	 HSSFCell   cell   =   row.getCell(j);
                	 if(j==0){
//                		 Integer num = Integer.parseInt(cell.getNumericCellValue());
                		 map.put("goodsNum", cell.getNumericCellValue());//商品编号
                	 }else if(j==1){
                		 map.put("goodsName",cell.getStringCellValue());//商品名字
                	 }else if(j==2){
                		 map.put("goodsSpec", cell.getStringCellValue());//商品规格
                	 }else if(j==3){
                		 //map.put("categoryId", categoryNameUtil(cell.getStringCellValue(),citycode));//商品分类
                	 }else if(j==4){
                		 map.put("goodsPlace", cell.getStringCellValue());//商品产地
                	 }else if(j==5){
                		 map.put("marketPrice", cell.getNumericCellValue());//商品市场价格
                	 }else if(j==6){
                		 map.put("factoryPrice", cell.getNumericCellValue());//商品供货价格
                	 }else if(j==7){
                		 map.put("retailPrice", cell.getNumericCellValue());//商品零售价格
                	 }else if(j==8){
                		 map.put("limitStatus", limitStatusUtil(cell.getStringCellValue()));//是否开启限购活动（0：不开启，1：开启）
                	 }else if(j==9){
                		 map.put("limitNum", cell.getNumericCellValue());//每人限购数量
                	 }else if(j==10){
                		 map.put("detail", cell.getStringCellValue());//商品描述
                	 }
        		 }
        		 list.add(map);
        	 }
        
        return list;  
    }
  
    private  Object limitStatusUtil(String str) {
    	int result = 0;
    	if("开启".equals(str.trim())){
    		result = 1;
    	}else if("关闭".equals(str.trim())){
    		result = 0;
    	}
    	return result;
	}

    /*private  Long categoryNameUtil(String str,String citycode){
    	
    	PlatCategory platCategory = platCategoryService.getByClassName(str,citycode);
    	
    	return platCategory.getId();
    }*/
    
    
    
    
}
