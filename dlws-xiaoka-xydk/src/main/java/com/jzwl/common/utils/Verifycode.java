/**
 * 
 */
package com.jzwl.common.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 
 * 
 * web.xml 
 * 
 * <servlet> 
 * 		<servlet-name>VerifyCode</servlet-name>
 * 		<servlet-class>com.gezhi.platform.util.VerifyCode</servlet-class>
 * 		<load-on-startup>5</load-on-startup>
 * </servlet>
 * <servlet-mapping>
 * 		<servlet-name>VerifyCode</servlet-name>
 * 		<url-pattern>/VerifyCode.do</url-pattern>
 * </servlet-mapping>
 */

public class Verifycode extends HttpServlet {

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	public void destroy() {
		super.destroy();
	}

	public Verifycode() {
	}

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int width = 60, height = 20;
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);

		// ��ȡͼ��������
		Graphics g = image.getGraphics();

		// �趨����ɫ
		g.setColor(getRandColor(200, 250));
		g.fillRect(0, 0, width, height);

		// ���߿�
		g.setColor(new Color(0, 0, 255));
		g.drawRect(0, 0, width - 1, height - 1);

		// �趨����
		g.setFont(new Font("Times New Roman", Font.PLAIN, 18));

		// ��������
		Random random = new Random();

		// ������155�������ߣ�ʹͼ���е���֤�벻�ױ��������̽�⵽
		g.setColor(getRandColor(160, 200));
		for (int i = 0; i < 155; i++) {
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int xl = random.nextInt(12);
			int yl = random.nextInt(12);
			g.drawLine(x, y, x + xl, y + yl);
		}

		// ȡ���������֤��(4λ����)
		String sRand = "";
		for (int i = 0; i < 4; i++) {
			String rand = String.valueOf(random.nextInt(10));
			sRand += rand;
			// ����֤����ʾ��ͼ����
			g.setColor(new Color(20 + random.nextInt(110), 20 + random
					.nextInt(110), 20 + random.nextInt(110)));
			// ���ú����������ɫ��ͬ����������Ϊ����̫�ӽ�����ֻ��ֱ�����
			g.drawString(rand, 13 * i + 6, 16);
		}

		// ͼ����Ч
		g.dispose();

		// ����֤�����SESSION
		HttpSession session = request.getSession(true);
		session.setAttribute("Verifycode", sRand);

		response.setContentType("image/jpeg; charset=GBK");
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		// ���ͼ��ҳ��
		ImageIO.write(image, "JPEG", response.getOutputStream());
	}

	public Color getRandColor(int fc, int bc) {
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;

		Random random = new Random();
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);

		return new Color(r, g, b);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
