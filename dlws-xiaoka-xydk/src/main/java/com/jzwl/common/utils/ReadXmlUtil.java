package com.jzwl.common.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class ReadXmlUtil {
	
	
	public static String classPath = ReadXmlUtil.class.getClassLoader().getResource("/").getPath();  
	
	
	public static Map<String, Object> readXmlOfJdom(String fileName)
			throws Exception, IOException {

		Map<String, Object> map = new HashMap<String, Object>();
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(classPath+fileName);
		Element root = doc.getRootElement();// 获得根元素：fields

		Element excelName = (Element) root.getChildren("filename").get(0);// 文件名
		map.put("excelName", excelName.getText());

		List<String> excelColName = new ArrayList<String>();// 列名的集合
		List<Map<String, String>> colList = new ArrayList<Map<String, String>>();
		List<Element> fieldList = root.getChildren("field");
		for (Element ele : fieldList) {
			String name = ele.getChild("name").getText();
			excelColName.add(name);

			String column = StringUtils.capitalize(ele.getChild("key").getText());//首字母大写
			String type = ele.getChild("type").getText();
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("column",column);
			dataMap.put("type", type);
			colList.add(dataMap);
		}

		map.put("excelColName", excelColName);
		map.put("colList", colList);
		return map;
	}
	
	//获得组装数据
	public static<T> List<List<String>> getExcelData(List<T> objList,List<Map<String, String>> colList) throws Exception, NoSuchMethodException{
		
		ArrayList<List<String>> dList = new ArrayList<List<String>>();
		if (objList != null && objList.size() > 0) {
			for (T t : objList) {
				ArrayList<String> data = new ArrayList<String>();
				 Class clazz = t.getClass(); 
				//组装数据
				for (Map<String, String> map : colList) {
					String column = map.get("column");
					String type = map.get("type").toLowerCase();
					
					if("datetime".equals(type)){
						String mehtodName = "get" + column + "View";
						Method m = clazz.getDeclaredMethod(mehtodName);
						data.add(StringUtil.isEmptyOrBlank((String) m.invoke(t, null))?"无":(String) m.invoke(t, null));
					}else if("double".equals(type)){
						String mehtodName = "get" + column + "View";
						Method m = clazz.getDeclaredMethod(mehtodName);
						data.add(StringUtil.isEmptyOrBlank((String) m.invoke(t, null))?"0.00":(String) m.invoke(t, null));
					}else if("int".equals(type)){
						String mehtodName = "get" + column;
						Method m = clazz.getDeclaredMethod(mehtodName);
						data.add(String.valueOf(m.invoke(t, null)));
					}else{
						String mehtodName = "get" + column;
						Method m = clazz.getDeclaredMethod(mehtodName);
						data.add(StringUtil.isEmptyOrBlank((String) m.invoke(t, null))?"无":(String) m.invoke(t, null));
					}
				}
				dList.add(data);
			}
		}
		return dList;
	}
	
	/*
	 * 设置reponse头，返回文件输出流
	 */
	public static OutputStream setResp(HttpServletResponse response,
			String filename) throws Exception {
		//ReadWriteExcelUtil exportToExcel = busyOrderService.exportCustomerOrder(paramsMap);
		//重置输出流，该代码可以加，如果不加，要保证resposne缓冲区中没有其他的数据，但是建议加上
		response.reset();
		OutputStream os = response.getOutputStream();
		//文件下载（添加的配置）
		response.setContentType("application/vnd.ms-excel");
		filename = new String(filename.getBytes("gbk"),"iso-8859-1");
		response.setHeader("Content-disposition", "attachment;filename="+filename+".xls");
		response.setBufferSize(1024);
		return os;
	}
	
	
	/*public static String getRootPath() {  
        String rootPath = "";  
        //windows下  
        if("\\".equals(File.separator)){  
            System.out.println("windows");  
        rootPath = classPath.substring(1,classPath.indexOf("/WEB-INF/classes"));  
        rootPath = rootPath.replace("/", "\\");  
        }  
        //linux下  
        if("/".equals(File.separator)){  
            System.out.println("linux");  
        rootPath = classPath.substring(0,classPath.indexOf("/WEB-INF/classes"));  
        rootPath = rootPath.replace("\\", "/");  
        }  
        return rootPath;  
        }  
	
	*/
	
	
	public static void main(String[] args) throws Exception {
		//String path = ReadXmlUtil.class.getClassLoader().getResource("").getPath();
		//System.out.println(path);
		//Map<String,Object> m = readXmlOfJdom(path+"/com/jzwl/v2/order/busyOrder/pojo/busyorder.xml");
	
	}
}
