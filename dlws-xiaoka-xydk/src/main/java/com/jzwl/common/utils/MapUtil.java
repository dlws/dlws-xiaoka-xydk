package com.jzwl.common.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;


/**
 * 
 * Map处理工具
 *
 */
public class MapUtil {
	
	/**
	 * 
	 * 描述:得到最小的价格
	 * 作者:gyp
	 * @Date	 2017年5月17日
	 */
	 public static Object getMinValue(Map<String,Object> map) {
		 if (map == null) return null;
		 Collection<Object> c = map.values();
		 Object[] obj = c.toArray();
		 Arrays.sort(obj);
		 return obj[0];
	 }
	 /**
	  * 
	  * 描述:得到最大的价格
	  * 作者:gyp
	  * @Date	 2017年5月17日
	  */
	 public static Object getMaxValue(Map<String,Object> map) {
		 if (map == null) return null;
		 Collection<Object> c = map.values();
		 Object[] obj = c.toArray();
		 Arrays.sort(obj);
		 return obj[obj.length-1];
	 }
	 /**
	  * 
	  * 描述:根据map集合转成键值并返回
	  * 作者:gyp
	  * @Date	 2017年5月22日
	  */
	 public static Object getValueStr(Map<String,Object> map) {
		 if (map == null) return null;
		 Collection<Object> c = map.values();
	
		 Object[] obj = c.toArray();
		 Arrays.sort(obj);
		 return obj[0];
	 }
	

}
