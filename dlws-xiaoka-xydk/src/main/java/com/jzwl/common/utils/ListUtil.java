package com.jzwl.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nutz.json.Json;

import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassF;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassS;

/**
 * 批量处理list的集合
 * @author gyp
 *
 */
public class ListUtil {
	/**匹配数字的数字正则表达式*/
	public static final String REGEX_NUMBER = "\\d+"; 
	// 防止直接实例化

	private ListUtil() {};
	
	/**
	 * 首页  ---或取用户手机端的分类
	 * @param sourList
	 * @param batchCount
	 * @return
	 */
	public static String dealBySubList(List<UserSkillclassF> sourList, int batchCount){
		 int sourListSize = sourList.size();
	        int subCount = sourListSize%batchCount==0 ? sourListSize/batchCount : sourListSize/batchCount+1;
	        int startIndext = 0;
	        int stopIndext = 0;
	        String skills = "";
	        for(int i=0;i<subCount;i++){
	            stopIndext = (i==subCount-1) ? stopIndext + sourListSize%batchCount : stopIndext + batchCount;
	            if(stopIndext==0){
	            	stopIndext =batchCount;
	            }
	            List<Object> tempList = new ArrayList<Object>(sourList.subList(startIndext, stopIndext)); 
	            String jsonSkillClass = Json.toJson(tempList);
				String skillClass = "{\"classItem\":"+jsonSkillClass+"}";
				
				skills = skills+","+skillClass;
	            startIndext = stopIndext;
	        }
	        String ss = skills.substring(1,skills.length());
	        String skill = "{\"sort1\":["+ss+"]}";
	        
	        return skill;
	}
	
	/**
	 * 分类的二级页面 二级分类的处理
	 * @param sourList
	 * @param batchCount
	 * @return
	 */
	public static String dealSonList(List<UserSkillclassS> sourList, int batchCount){
		 int sourListSize = sourList.size();
	        int subCount = sourListSize%batchCount==0 ? sourListSize/batchCount : sourListSize/batchCount+1;
	        int startIndext = 0;
	        int stopIndext = 0;
	        String skills = "";
	        for(int i=0;i<subCount;i++){
	            stopIndext = (i==subCount-1) ? stopIndext + sourListSize%batchCount : stopIndext + batchCount;
	            if(stopIndext==0){
	            	stopIndext =batchCount;
	            }
	            List<Object> tempList = new ArrayList<Object>(sourList.subList(startIndext, stopIndext)); 
	            String jsonSkillClass = Json.toJson(tempList);
				String skillClass = "{\"navItem\":"+jsonSkillClass+"}";
				
				skills = skills+","+skillClass;
	            startIndext = stopIndext;
	        }
	        String ss = skills.substring(1,skills.length());
	        String skill = "{\"nav\":["+ss+"]}";
	        
	        return skill;
	}

	
}
