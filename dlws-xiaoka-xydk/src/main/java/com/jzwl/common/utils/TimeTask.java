package com.jzwl.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;


@Service("TimeTask")
public class TimeTask implements ApplicationListener<ContextRefreshedEvent>{
	/*@Autowired
	private V2BusyShopBasicinfoService busyShopBasicinfoService;*/
	
	private Timer timmer = new Timer();
	
	public static void main(String[] args) {
	        Runnable runnable = new Runnable() {  
	            public void run() {  
	                // task to run goes here  
	                System.out.println("Hello !!");  
	            }  
	        };  
	        ScheduledExecutorService service = Executors  
	                .newSingleThreadScheduledExecutor();  
	        long oneDay = 24 * 60 * 60 * 1000;  
	        long initDelay  = getTimeMillis("02:00:00") - System.currentTimeMillis();  
	        initDelay = initDelay > 0 ? initDelay : oneDay + initDelay; 
	        // 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间  
	        service.scheduleAtFixedRate(runnable, 0, 2, TimeUnit.SECONDS);  
	}
	public void v2SynchMoney(){//v2同步店铺账户金额
		Runnable runnable = new Runnable() {  
            public void run() {  
                // task to run goes here  
            	try {
            	/*	busyShopBasicinfoService.synchronousMoney();  */
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }  
        };  
        ScheduledExecutorService service = Executors  
                .newSingleThreadScheduledExecutor();  
        long oneDay = 24 * 60 * 60 * 1000;  
        long initDelay  = getTimeMillis("01:30:00") - System.currentTimeMillis();
        initDelay = initDelay > 0 ? initDelay : oneDay + initDelay; 
        // 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间  
        service.scheduleAtFixedRate(runnable, initDelay, oneDay, TimeUnit.MILLISECONDS);  
	}
	/** 
	 * 获取指定时间对应的毫秒数 
	 * @param time "HH:mm:ss" 
	 * @return 
	 */  
	private static long getTimeMillis(String time) {  
	    try {  
	        DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");  
	        DateFormat dayFormat = new SimpleDateFormat("yy-MM-dd");  
	        Date curDate = dateFormat.parse(dayFormat.format(new Date()) + " " + time);  
	        return curDate.getTime();  
	    } catch (ParseException e) {  
	        e.printStackTrace();  
	    }  
	    return 0;  
	}
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		
		//创建巡回机制轮训订单表
		//timmer.schedule(task, firstTime, 2000);
		
	}
}
