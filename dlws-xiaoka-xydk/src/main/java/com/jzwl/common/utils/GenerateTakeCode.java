package com.jzwl.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.cache.RedisService;
/**
 * 取货码
 */
@Service("generateTakeCode")
public class GenerateTakeCode {
	@Autowired
	private RedisService redisService;
	
	private static final int LENGTH = 8;
//	private String num = null;
	
	public String getCode(){
		String num = RandomUtil.getRandomIntNum(LENGTH);
		num = isExist(num);
		return num;
	}
	
	public String isExist(String num){
		if (redisService.exists( "lwm_take_code_" + num )) {
			num = RandomUtil.getRandomIntNum(LENGTH);
			num = isExist(num);
			return num;
		}else{
			redisService.set( "lwm_take_code_" + num, num, 345600l);//4天=345600s
			return num;
		}
	}
}
