package com.jzwl.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**

 * 字符串工具类，提供常见的字符串操作

 */
public class StringUtil {
	/**匹配数字的数字正则表达式*/
	public static final String REGEX_NUMBER = "\\d+"; 
	// 防止直接实例化

	private StringUtil() {};

	/**

	 *  为空或者空白 

	 * @param str

	 * @return 如果为空就返回true

	 */
	public static boolean isEmptyOrBlank(String str) {
		return str == null || str.trim().length() <= 0;
	}
	/**

	 *  不为空或者空白 

	 * @param str

	 * @return 如果不为空就返回true

	 */
	public static boolean isNotEmptyOrBlank(String str) {
		
		return str != null && str.trim().length() > 0;
		
	}
	
	/**
	 * 检测某个字符变量是否为空；<br>
	 * 为空的情况，包括：null，空串或只包含可以被 trim() 的字符；
	 */
	public final static boolean isEmpty(String value) {
		if (value == null || value.trim().length() == 0)
			return true;
		else
			return false;
	}
	
	/**
	 * 抽取出字符串中的所有数字,如果没有数字字符，则返回空字符串
	 */
	public static String drawNumber(String source){ 
		return drawMatches(source,REGEX_NUMBER) ;
	}
	
	/**
	 * 从字符串中取出满足正则表达式条件的字符串
	 */
	public static String drawMatches(String source,String regex){
		Matcher matcher = getMatcher(source,regex) ;
		StringBuffer buff = new StringBuffer() ;
		while(matcher.find()){
			buff.append((matcher.group()));
		}
		return buff.toString() ;
	}
	
	/**返回字符串source匹配正则表达式regex的matcher实例(regex对大小写敏感)*/
	public static Matcher getMatcher(String source,String regex){
		Pattern pattern = Pattern.compile(regex) ; 
		return pattern.matcher(source) ;
	}

	/**
	 * 验证是否为坐标的格式
	 */
	public static boolean validateLocation(String location){
		if(StringUtil.isEmpty(location)){
			return false;
		}else if(!location.contains(",")){
			return false;
		}
		return true;
	}
	
	/**
	 * 组装sql where语句
	 * @param buildsql
	 * @return
	 */
	public static StringBuffer addWhere(StringBuffer buildsql){
		if (buildsql.toString().contains("where") || buildsql.toString().contains("WHERE")) {
			//第一次拼接 where子句不需要and
			buildsql.append(" AND ");
		}else{
			//不是第一次拼接where，则需要加'and'
			buildsql.append(" WHERE ");
		}
		return buildsql;
	}
	/**
	 * 将小写的数字转换为大写 --gyp
	 * eg:输入11  return 一一
	 * @param num
	 * @return
	 */
	public static String numToUpper(int num) {    
	        String u[] = { "〇", "一", "二", "三", "四", "五", "六", "七", "八", "九" };    
	        char[] str = String.valueOf(num).toCharArray();    
	        String rstr = "";    
	        for (int i = 0; i < str.length; i++) {    
	            rstr = rstr + u[Integer.parseInt(str[i] + "")];    
	        }    
	        return rstr;    
	    }
	
	/**
	 * 处理参数用，号隔开的方式进行返回String类型
	 */
	public static String dealArray(String[] str){
		StringBuffer all = new StringBuffer();
		for (int i = 0; i < str.length; i++) {
			
			if(i==str.length-1){
				all.append(str[i]);
			}else{
				all.append(str[i]+",");
			}
			
		}
		return all.toString();
	}
	/**
	 * 处理参数用，将两个数组中的值一一对应并以 key：value
	 * 号的形式存入数据库中 {1,2,3,4,5}
	 */
	
	public static String dealArray(String[] keys,String[] values){
		
		Map<String,String> map = new HashMap<String, String>();
		if(keys.length==values.length){
			for (int i = 0; i < values.length; i++) {
				if(StringUtils.isNotBlank(values[i])){
					map.put(keys[i], values[i]);
				}
			}
		}
		String str=JSON.toJSON(map).toString();
		return str;
	}
	/**
	 * 将List<Map<String,String>>的形式进行处理成为 
	 * text 和 value 的形式 其中key 为 text value 为value
	 * (仅仅用于处理字典表)
	 */
	public static String dealList(List<Map<String, Object>> fieldTypeList){
		
		List<Map<String,Object>> resultList = new ArrayList<Map<String,Object>>();
		
		try {
			for (Map<String, Object> map : fieldTypeList) {
				HashMap<String, Object> result = new HashMap<String, Object>();
				for (String key : map.keySet()) {
					result.put("text", map.get("dic_name"));
					result.put("value", map.get("dic_value"));
				}
				resultList.add(result);
			}
		} catch (Exception e) {
			System.out.println("转换List出现异常。。。。");
			e.printStackTrace();
		}
		return JSONObject.toJSONString(resultList);
	}
	
	/**
	 * 将以，号分割的STring 串以list的形式进行返回
	 * @param str
	 * @return
	 */
	public static List<String> dealString(String str) {

		List<String> list = new ArrayList<String>();
		String[] arre = str.split(",");
		for (String string : arre) {
			list.add(string);
		}
		return list;
	}
	
	/**
	 * 将String数组转换为int数组
	 */
	public static int[] toGetIntArray(String[] array){
		
		List<Integer> list = new ArrayList<Integer>();
		//将数组中的空值进行处理掉
		for (int i = 0; i < array.length; i++){
			
			if(StringUtils.isNotBlank(array[i])){
				
				list.add(Integer.parseInt(array[i]));
			}
			
		}
		
		int[] emplv=new int[list.size()];
		
		for (int i = 0; i < emplv.length; i++) {
				
				emplv[i]=list.get(i);
		}
		return emplv;
		
	}
	
	/* public static void main(String[] args) {  
	        String words = "中国是世界四大文明古国之一，有着悠久的历史，距今约5000年前，以中原地区为中心开始出现聚落组织进而成国家和朝代，后历经多次演变和朝代更迭，持续时间较长的朝代有夏、商、周、汉、晋、唐、宋、元、明、清等。中原王朝历史上不断与北方游牧民族交往、征战，众多民族融合成为中华民族。20世纪初辛亥革命后，中国的君主政体退出历史舞台，取而代之的是共和政体。1949年中华人民共和国成立后，在中国大陆建立了人民代表大会制度的政体。中国有着多彩的民俗文化，传统艺术形式有诗词、戏曲、书法和国画等，春节、元宵、清明、端午、中秋、重阳等是中国重要的传统节日。";  
	        System.out.println(ToAnalysis.parse(words));  
	 }  */


	
	/**
	 * 用来处理商家发布信息的订单状态的
	 * @param list
	 * @return
	 */
	public static String judgeOrderStatu(List<String> list){
		
		String result = list.get(0);
		//判定当前订单状态为待付款的订单状态
		if(list.size()==1){
			//当前状态是几就写几
			result = list.get(0);
		}else{
			//订单超时，待退款
			if(list.contains("5")){
				result = "5";
			}
			//已退款 说明有退款成功的单子
			if(list.contains("8")){
				result = "8";
			}
		}
		return result;
	}
	
	/*public static void main(String[] args) { 
		Integer ob[]={11,22,33};  
		List<Integer> objList=Arrays.asList(ob);  //数组里的每一个元素都是作为list中的一个元素  
		objList.add(45);
		System.out.println(objList.size());
	}*/
	
	
}
