package com.jzwl.common.utils;

import java.util.Date;

/**
 * 
 * 时间工具类 处理常用对于时间的操作
 *
 */
public class DataUtil {
	
	
	/**
	 * @param num1 被减数
	 * @param num2 减数
	 * @return 返回Long 类型
	 */
	public static long subtracDateL(Date num1,Date num2){
		
		long count = 0;
		
		long meiosis = num1.getTime();
		long minuend = num2.getTime();
		
		count = meiosis - minuend;
		
		return count;
	}
	
	/**
	 * @param num1 被减数 结束时间
	 * @param num2 减数 当前时间
	 * @return 返回String 类型
	 */
	public static String subtracDateS(Date d1,Date d2){
		
		String timer = "";
		long date = d1.getTime() - d2.getTime();   
	    long day = date / (1000 * 60 * 60 * 24);    
	    long hour = (date / (1000 * 60 * 60) - day * 24);    
	    long min = ((date / (60 * 1000)) - day * 24 * 60 - hour * 60);  
	    long s = (date/1000 - day*24*60*60 - hour*60*60 - min*60);  
	    System.out.println(""+day+"天"+hour+"小时"+min+"分"+s+"秒");
	    timer=""+day+"天"+hour+"小时"+min+"分"+s+"秒";
	    
	    return timer;
	}
	

}
