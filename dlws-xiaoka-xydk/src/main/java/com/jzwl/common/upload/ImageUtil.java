package com.jzwl.common.upload;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.nutz.img.Images;
import org.springframework.web.multipart.MultipartFile;

import com.jzwl.common.utils.Util;

/**
 * 图片裁剪
 */
public class ImageUtil {

	/**
	 * 生成缩略图
	 * @param img 原始图片（CMYK模式或者正常的jpg）
	 * @param width 要生成的缩略图的宽
	 * @param height 要生成的缩略图的高
	 * @throws IOException
	 */
	public static File imageThumbnail(MultipartFile img, int width, int height, File targetFile) throws IOException {
		//用org.nutz.img.Images进行处理
		BufferedImage bufferedImage = Images.read(img.getInputStream());
		int imageWidth = bufferedImage.getWidth();
		int imageHeitht = bufferedImage.getHeight();
		if (width > 0 && height > 0) {
			if (imageWidth > width && imageHeitht > height) {
				//裁剪
				Thumbnails.of(bufferedImage).size(width, height).keepAspectRatio(true).toFile(targetFile);
			} else if (imageWidth <= width && imageHeitht <= height) {
				//原图
				Thumbnails.of(bufferedImage).size(imageWidth, imageHeitht).keepAspectRatio(true).toFile(targetFile);
			} else if (imageWidth > width && imageHeitht <= height) {
				//只裁剪宽
				Thumbnails.of(bufferedImage).width(width).keepAspectRatio(true).toFile(targetFile);
			} else if (imageWidth <= width && imageHeitht > height) {
				//只裁剪高
				Thumbnails.of(bufferedImage).height(height).keepAspectRatio(true).toFile(targetFile);
			}
		}
		bufferedImage.flush();
		return targetFile;
	}
	
	
	
	/**
	 * 生成缩略图
	 * @param img 原始图片（CMYK模式或者正常的jpg）
	 * @param width 要生成的缩略图的宽
	 * @param height 要生成的缩略图的高
	 * @throws IOException
	 */
	public static BufferedImage imageThumbnail(InputStream img, int width, int height ) throws IOException {
		//用org.nutz.img.Images进行处理
		BufferedImage bufferedImage = Images.read(img);
		int imageWidth = bufferedImage.getWidth();
		int imageHeitht = bufferedImage.getHeight();
		if (width > 0 && height > 0) {
			if (imageWidth > width && imageHeitht > height) {
				//裁剪
				BufferedImage crrentBuffer = Thumbnails.of(bufferedImage).size(width, height).keepAspectRatio(true).asBufferedImage();
				bufferedImage.flush();
				return crrentBuffer;
			} else if (imageWidth <= width && imageHeitht <= height) {
				//原图
				BufferedImage crrentBuffer = Thumbnails.of(bufferedImage).size(imageWidth, imageHeitht).keepAspectRatio(true).asBufferedImage();
				bufferedImage.flush();
				return crrentBuffer;
			} else if (imageWidth > width && imageHeitht <= height) {
				//只裁剪宽
				BufferedImage crrentBuffer = Thumbnails.of(bufferedImage).width(width).keepAspectRatio(true).asBufferedImage();
				bufferedImage.flush();
				return crrentBuffer;
			} else if (imageWidth <= width && imageHeitht > height) {
				//只裁剪高
				BufferedImage crrentBuffer = Thumbnails.of(bufferedImage).height(height).keepAspectRatio(true).asBufferedImage();
				bufferedImage.flush();
				return crrentBuffer;
			}
		}
		return bufferedImage;
		
	}
	
	@SuppressWarnings("finally")
	public static byte[] getImageByInternet (String imageUrl) throws IOException    
    {    
		
		byte [] imagebyte = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();   
        try {  
            HttpGet httpGet = new HttpGet(imageUrl);  
            CloseableHttpResponse response = httpclient.execute(httpGet);  
            try {  
                HttpEntity entity = response.getEntity();  
                InputStream inStream = entity.getContent();
                
                BufferedImage buffer = ImageUtil.imageThumbnail(inStream, 200, 200);
                
                imagebyte =  Util.imageToBytes(buffer);
                
                EntityUtils.consume(entity);  
            } finally {  
                response.close();  
            }  
        }finally {  
            httpclient.close();  
            return imagebyte;
        }  
        
        
    }  
	
	
	
	
}
