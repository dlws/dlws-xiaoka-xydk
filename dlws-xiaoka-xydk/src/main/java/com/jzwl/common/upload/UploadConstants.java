package com.jzwl.common.upload;
/**
 * 存放上传文件的相关常量
 * @author cct
 */
public class UploadConstants {
	
	/**
	 * 图片允许格式
	 */
	public static String[] IMG_TYPE = { ".JPEG", ".JPG", ".jpg", ".jpeg" , ".PNG" , ".png" , ".gif" , ".GIF" , ".BMP" , ".bmp" , ".eps" , ".EPS"};

	/**
	 * 静态资源服务器的主机目录
	 * 172.16.0.19
	 * 
	 * 111.206.116.90:8087
	 */
//	public static final String STATICRESOURCE_PATH = "http://111.206.116.90:8087/uniformStaticResourceService";
	/**
	 * 商品主图地址
	 * upload下指定的目录，该目录为已存在的目录
	 * /upload/image/goods/goods/
	 */
//	public static final String TARGETDIRECTORY_GOODS = "/upload/image/goods/goods/";
	
	/**
	 * 商品详细介绍图片地址
	 * upload下指定的目录，该目录为已存在的目录
	 * /upload/image/goods/detail/
	 */
//	public static final String TARGETDIRECTORY_GOODS_DETAIL = "/upload/image/goods/detail/";
	
	/**
	 * 用户头像地址
	 * upload下指定的目录，该目录为已存在的目录
	 * /upload/image/user/avatar/
	 */
//	public static final String TARGETDIRECTORY_USER_AVATAR = "/upload/image/user/avatar/";
	
	/**
	 * 店铺图片地址
	 * upload下指定的目录，该目录为已存在的目录
	 * /upload/image/shop/logo/
	 */
//	public static final String TARGETDIRECTORY_SHOP_LOGO = "/upload/image/shop/logo/";
	
}
