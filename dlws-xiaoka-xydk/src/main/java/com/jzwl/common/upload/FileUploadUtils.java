package com.jzwl.common.upload;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

/**
 * 文件上传工具类
 * @author cct
 */
public class FileUploadUtils {
	
	public static boolean isFileType(String fileName, String[] typeArray) {
		for (String type : typeArray) {
			if (fileName.toLowerCase().endsWith(type)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 上传到静态资源服务器....
	 * @param multipartFile 要上传的文件
	 * @param targetPath 要上传到绝对路径  http://localhost:8080/uniformStaticResourceService/upload/image/goods/goods/123.jpg
	 * @throws IOException 抛出异常，意味着上传失败或者图片服务器未开启。调用方法的时候进行捕获异常
	 */
	public static void uploadToStaticResourceServerByJersey(MultipartFile multipartFile, String targetPath) throws IOException,Exception {
		if (isFileType(targetPath, UploadConstants.IMG_TYPE)) {
			//创建jersey客户端
			Client client = Client.create();
			//指定要操作的文件的绝对路径
			WebResource wr = client.resource(targetPath.replaceAll("\\\\", "/"));
			byte[] bytes = multipartFile.getBytes();
			//同名文件的时候，删除原文件，进行覆盖
			try {
				wr.delete();
			} catch (Exception e) {
			}
			//上传文件
			wr.put(String.class, bytes);
		} else{
			throw new Exception("该文件格式不允许上传！");
		}
	}
	/**
	 * 上传到静态资源服务器
	 * @param multipartFile 要上传的文件
	 * @param targetPath 要上传到绝对路径  http://localhost:8080/uniformStaticResourceService/upload/image/goods/goods/123.jpg
	 * @throws IOException 抛出异常，意味着上传失败或者图片服务器未开启。调用方法的时候进行捕获异常
	 */
	public static void uploadToStaticResourceServerByJersey(File multipartFile, String targetPath) throws IOException,Exception {
		if (isFileType(targetPath, UploadConstants.IMG_TYPE)) {
			//创建jersey客户端
			Client client = Client.create();
			//指定要操作的文件的绝对路径
			WebResource wr = client.resource(targetPath.replaceAll("\\\\", "/"));
			//同名文件的时候，删除原文件，进行覆盖
			try {
				wr.delete();
			} catch (Exception e) {
			}
			//上传文件
			wr.put(File.class, multipartFile);
		} else{
			throw new Exception("该文件格式不允许上传！");
		}
	}
	
	/**
	 * 删除静态资源服务器上的文件
	 * UploadConstants.STATICRESOURCE_PATH 静态资源服务器的主机目录
	 * @param targetPath 文件的绝对路径  http://localhost:8080/uniformStaticResourceService/upload/image/goods/goods/123.jpg
	 * @throws Exception 抛出异常，意味着删除失败或者文件不存在。调用方法的时候进行捕获异常
	 */
	public static void deleteFromStaticResourceServerByJersey(String targetPath) throws Exception {
		//创建jersey客户端
		Client client = Client.create();
	    //指定要操作的文件的绝对路径
		WebResource wr = client.resource(targetPath.replaceAll("\\\\", "/"));
		//如果文件不存在，会报删除不了。所有用try/catch包含以下。
		wr.delete();
	}
	
	/**
	 * 上传到本地
	 * @param multipartFile 要上传的文件
	 * @param targetPath 要上传到绝对路径  E://apache_tomcat_6.0.1/webapps/wm-plat/upload/image/goods/123.jpg
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	public static void uploadToLocal(MultipartFile multipartFile, String targetPath) throws IllegalStateException, IOException{
		if (isFileType(targetPath, UploadConstants.IMG_TYPE)) {
			File targetFile = new File(targetPath.replaceAll("\\\\", "/"));
			multipartFile.transferTo(targetFile);
		}
	}
}
