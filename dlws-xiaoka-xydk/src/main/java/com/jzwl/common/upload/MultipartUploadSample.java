package com.jzwl.common.upload;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.PartETag;
import com.aliyun.oss.model.PutObjectResult;
/**
 * This sample demonstrates how to upload multiparts to Aliyun OSS 
 * using the OSS SDK for Java.
 */
public class MultipartUploadSample {
	//http://xiaoka2016
    private static  String endpoint = "oss-cn-qingdao.aliyuncs.com";//"*** Provide OSS endpoint ***";
    private static String accessKeyId = "prC1rq1b2DocCcMn";//"*** Provide your AccessKeyId ***";
    private static String accessKeySecret =  "Cu36qaAFFI1nlU1eINM7p0HjSZaIIf";// "*** Provide your AccessKeySecret ***";

    private static OSSClient client = null;
    
    private static String bucketName = "xiaoka2016";//"*** Provide bucket name ***";
    private String key = "xiaokaImageKey";//"*** Provide key ***";
    private String localFilePath = "";//"*** Provide local file path ***";
    
    
    private ExecutorService executorService = Executors.newFixedThreadPool(5);
    private List<PartETag> partETags = Collections.synchronizedList(new ArrayList<PartETag>());
    
    
    /**
     * 上传图片的到oss
     * 项目名称：dlws-xiaoka-bbs
     * 描述：图片上传，上传成功返回true,失败返回false;
     * 创建人： ln
     * 创建时间： 2016年7月28日
     * 标记：工具类
     * @param key 传入key为文件唯一表示，保证不能重复。
     * @param file springmvc上传文件获取到的multipartFile 类型文件
     * @return boolean
     * @throws IOException
     * @version
     */
    public static boolean uploadOssImg(String key,MultipartFile file) throws IOException{
        InputStream in = file.getInputStream();
        try {
        	PutObjectResult result = upload(key,in);
        	if(null!=result){
        		if(null!=result.getETag()){
        			return true;
        		}
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return false;
    }
    
    public static  PutObjectResult upload(String key,InputStream file) throws IOException {
        /*
         * Constructs a client instance with your account for accessing OSS
         */
        client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        
        try {
            System.out.println("Begin to upload multiparts to OSS from a file\n");

            PutObjectResult result = client.putObject(bucketName, key, file);
            
            return result;

        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message: " + oe.getErrorCode());
            System.out.println("Error Code:       " + oe.getErrorCode());
            System.out.println("Request ID:      " + oe.getRequestId());
            System.out.println("Host ID:           " + oe.getHostId());
            return null;
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ce.getMessage());
            return null;
        } finally {
            /*
             * Do not forget to shut down the client finally to release all allocated resources.
             */
            if (client != null) {
                client.shutdown();
            }
        }
    }
    
   /* public static  PutObjectResult upload(String key,InputStream file) throws IOException {
        
         * Constructs a client instance with your account for accessing OSS
         
        client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        
        try {
            System.out.println("Begin to upload multiparts to OSS from a file\n");

            PutObjectResult result = client.putObject(bucketName, key, file);
            
            return result;

        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message: " + oe.getErrorCode());
            System.out.println("Error Code:       " + oe.getErrorCode());
            System.out.println("Request ID:      " + oe.getRequestId());
            System.out.println("Host ID:           " + oe.getHostId());
            return null;
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ce.getMessage());
            return null;
        } finally {
            
             * Do not forget to shut down the client finally to release all allocated resources.
             
            if (client != null) {
                client.shutdown();
            }
        }
    }*/
    
    
    
    /**
     * 
     * 项目名称：dlws-xiaoka-bbs
     * 描述：根据上传时保存的key值进行删除oos图片
     * 创建人： ln
     * 创建时间： 2016年7月28日
     * 标记：
     * @param key 传入上传时保存的key
     * @throws IOException
     * @version
     */
	public static void delete(String key) throws IOException {
        /*
         * Constructs a client instance with your account for accessing OSS
         */
        client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        
        try {
            System.out.println("Begin to upload multiparts to OSS from a file\n");

            client.deleteObject(bucketName, key);
            
            /*return result;*/

        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message: " + oe.getErrorCode());
            System.out.println("Error Code:       " + oe.getErrorCode());
            System.out.println("Request ID:      " + oe.getRequestId());
            System.out.println("Host ID:           " + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ce.getMessage());
        } finally {
            /*
             * Do not forget to shut down the client finally to release all allocated resources.
             */
            if (client != null) {
                client.shutdown();
            }
        }
    }
    
	
	/**
	 * 
	 * @param url 处理图片字符串
	 * @return
	 */
	public static String dealUrl(String url){
		
			String string = "";
			String [] arrayUrl = url.split("/");
			
			for (int i = 0; i < arrayUrl.length; i++) {
				if(i==arrayUrl.length-1){
					string = arrayUrl[i];
					System.out.println(string);
				}
				
			}
		return string;
	}
   
    
  /*  public static void main(String[] args) {
    	try {
			upload("bbs/222");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
    
}
