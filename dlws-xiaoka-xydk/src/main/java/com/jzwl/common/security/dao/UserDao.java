package com.jzwl.common.security.dao;

import java.util.List;
import java.util.Map;

import com.jzwl.common.security.pojo.UserBean;

public interface UserDao {
	/**
	 * 登陆时，根据用户名查找用户
	 * @param username
	 * @return
	 */
	public List<UserBean> getUser(String username);
	
	/**
	 * 根据角色查找权限
	 * @param roleid 角色ID
	 * @return
	 */
	public List<Map<String,Object>> findPrivilege(String roleid);
	
	/**
	 * 
	 * @param username 登录的用户名
	 * @return
	 */
	public List<Map<String,Object>> getBusyShopUser(String username);
}
