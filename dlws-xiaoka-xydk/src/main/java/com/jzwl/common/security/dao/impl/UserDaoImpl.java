package com.jzwl.common.security.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.security.dao.UserDao;
import com.jzwl.common.security.pojo.UserBean;
import com.jzwl.system.base.dao.BaseDAO;

@Repository
public class UserDaoImpl implements UserDao{
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	
	//登陆时，根据用户名查找用户
	public List<UserBean> getUser(String username) {
		StringBuffer sql = new StringBuffer();
//		sql = "select u.id,u.username,u.password,u.status,ru.roleId from plat_userrole ru left join busy_shopuser u on ru.userId=u.id left join plat_role r on ru.roleId=r.roleId where u.username='"+username+"'";
//		List<Map<String,Object>> list = baseDAO.queryForList(sql.toString());
		sql.append("SELECT u.id ,u.username, u.`password`,u.`sex`,u.`mobile`,u.`wxId`,u.`citycode`,d.`cityName` as cityname,u.`departmentId`, u.`status` ,ru.roleId ");
		sql.append(" FROM v2_plat_userrole ru");
		sql.append(" LEFT JOIN v2_busy_shopuser u ON u.id = ru.userId ");
		sql.append(" LEFT JOIN  v2_plat_role r ON r.roleId = ru.roleId ");
		sql.append(" LEFT JOIN  v2_district_info d ON u.citycode = d.cityCode ");
		sql.append(" WHERE r.`status` = 1 ");
		sql.append(" AND u.`status` = 1 AND u.isDelete = 0 ");
		sql.append(" AND u.username = '" + username+"'");
	
		List<UserBean> list = baseDAO.getJdbcTemplate().query(sql.toString(),ParameterizedBeanPropertyRowMapper.newInstance(UserBean.class));
		return list;
	}
	
	//根据角色查找权限
	public List<Map<String,Object>> findPrivilege(String roleid) {
		String sql = "SELECT ra.authorityId id FROM v2_plat_roleauthority ra LEFT JOIN v2_plat_authority a ON ra.authorityId = a.authorityId WHERE ra.roleId = "+roleid;
		List<Map<String,Object>> list = baseDAO.queryForList(sql);
		return list;
	}

	
	@Override
	public List<Map<String, Object>> getBusyShopUser(String username) {
		String sql = "SELECT us.id,us.address,us.mobile,us.wxId,us.username,us.password FROM busy_shopuser us where us.username='"+username+"'";
		List<Map<String,Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	
	

}

