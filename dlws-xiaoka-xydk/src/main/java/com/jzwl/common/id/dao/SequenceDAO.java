package com.jzwl.common.id.dao;

public interface SequenceDAO {
	
	public String nextId();
}
