package com.jzwl.system.base.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.jzwl.common.constant.GlobalConstant;

public class BaseWeixinController extends BaseController {
	
	
	
	public static final String currentOpenIdKey = "dkOpenId";
	public static final String currentLocationKey = "location";
	public static final String currentSchoolIdKey = "schoolId";


	protected HttpSession getBaseSession(HttpServletRequest request) {
		if (null != request) {
			return request.getSession();

		}
		return null;
	}

	
	/**
	 * 获取openid
	 * 
	 * @param request
	 * @return
	 */
	protected String getCurrentOpenId(HttpServletRequest request) {
		
		String currrenOpenId =  String.valueOf(getBaseSession(request).getAttribute(currentOpenIdKey));
		if (null != currrenOpenId&&currrenOpenId.trim().length() > 0&&!("null".equals(currrenOpenId))) {
			return currrenOpenId;
		}
		return null;

	}
	
	/**
	 * 获取location
	 * 
	 * @param request
	 * @return
	 */
	protected String getCurrentLocation(HttpServletRequest request) {
		String currentLocation =  String.valueOf(getBaseSession(request).getAttribute(currentLocationKey));
		if (null != currentLocation&&currentLocation.trim().length() > 0) {

			return currentLocation;

		}

		return "39.958367,116.349794";

	}
	
	/**
	 * 获取当前用户的schoolId
	 * 
	 * @param request
	 * @return
	 */
	protected String getCurrentSchoolId(HttpServletRequest request) {
		String currentSchoolId = String.valueOf(getBaseSession(request).getAttribute(currentSchoolIdKey));
		if (null != currentSchoolId && !"null".equals(currentSchoolId) &&currentSchoolId.trim().length() > 0) {
			return currentSchoolId;
		}
		return null;
		
	}

}
