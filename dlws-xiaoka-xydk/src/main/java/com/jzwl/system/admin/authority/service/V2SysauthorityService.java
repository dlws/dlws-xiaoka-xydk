package com.jzwl.system.admin.authority.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.system.admin.authority.dao.V2SysauthorityDao;
import com.jzwl.system.admin.role.pojo.Sysauthority;
import com.jzwl.common.page.PageObject;

/**
 * V2权限管理service层
 * 
 * @author H.Jock
 * @Time 2016-1-7 11:14:08
 * @version 1.0
 * 
 */
@Service("V2SysauthorityService")
public class V2SysauthorityService {

	@Autowired
	private V2SysauthorityDao sysauthorityDao;

	/**
	 * 条件查询权限列表
	 * 
	 * @param paramsMap
	 * @return
	 */
	public PageObject queryAuthorityList(Map<String, Object> paramsMap) {
		return sysauthorityDao.queryAuthorityList(paramsMap);
	}

	/**
	 * 添加权限
	 * 
	 * @param map
	 * @return
	 */
	public boolean addSysauthority(Map<String, Object> map) {
		return sysauthorityDao.addSysauthority(map);
	}

	/**
	 * 获取权限byID
	 * 
	 * @param map
	 * @return
	 */
	public Sysauthority getByAuthorityId(Map<String, Object> map) {
		return sysauthorityDao.getByAuthorityId(map);
	}

	/**
	 * 更新权限
	 * 
	 * @param map
	 * @return
	 */
	public boolean updateSysauthority(Map<String, Object> map) {
		return sysauthorityDao.updateSysauthority(map);
	}

	/**
	 * 删除权限
	 * 
	 * @param map
	 * @return
	 */
	public boolean deleteAuthority(Map<String, Object> map) {
		return sysauthorityDao.deleteAuthority(map);
	}

	/**
	 * 获取权限
	 * 
	 * @param paramsMap
	 * @return
	 */
	public Map<String, Object> getAuthority(Map<String, Object> paramsMap) {
		return sysauthorityDao.getAuthority(paramsMap);
	}

	/**
	 * 获取权限已拥有资源列表
	 * 
	 * @param paramsMap
	 * @return
	 */
	public List querySysresourceAuthList(Map<String, Object> paramsMap) {
		return sysauthorityDao.querySysresourceAuthList(paramsMap);
	}

	/**
	 * 获取权限未拥有资源列表
	 * 
	 * @param paramsMap
	 * @return
	 */
	public List querySysresourceNoAuthList(Map<String, Object> paramsMap) {
		return sysauthorityDao.querySysresourceNoAuthList(paramsMap);
	}

	/**
	 * 添加资源权限关系
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean addSysresourceAuth(Map<String, Object> paramsMap) {
		return sysauthorityDao.addSysresourceAuth(paramsMap);
	}

	/**
	 * 删除资源权限关系
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean deleteSysresourceAuth(Map<String, Object> paramsMap) {
		return sysauthorityDao.deleteSysresourceAuth(paramsMap);
	}
}
