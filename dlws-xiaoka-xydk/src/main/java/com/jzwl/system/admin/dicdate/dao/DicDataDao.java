/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.dicdate.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.admin.dicdate.pojo.*;
import com.jzwl.system.admin.dicdate.service.*;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.common.page.PageObject;



@Repository("v2DicDataDao")
public class DicDataDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	
	public boolean addDicData(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id",  Sequence.nextId());
		
		String sql = "insert into v2_dic_data " 
				 + " (id,dic_id,dic_name,parentId,dic_value,isDelete) " 
				 + " values "
				 + " (:id,:dicId,:dicName,:parentId,:dicValue,:isDelete)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	public String getColumns() {
		return ""
				+" t.id as id,"
				+" t.dic_id as dicId,"
				+" t.dic_name as dicName,"
				+" t.parentId as parentId,"
				+" t.dic_value as dicValue,"
				+" t.isDelete as isDelete"
				;
	}
	

	public PageObject queryDicDataList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select " + getColumns() + " from v2_dic_data t where 1=1 ";
		
		  	if(null !=map.get("dicId") && StringUtils.isNotEmpty(map.get("dicId").toString())){
			  		sql=sql+ " and t.dic_id  = " + map.get("dicId") +"";
		  	}
		  	if(null !=map.get("dicName") && StringUtils.isNotEmpty(map.get("dicName").toString())){
			  		sql=sql+ " and t.dic_name  = " + map.get("dicName") +"";
		  	}
		  	if(null !=map.get("parentId") && StringUtils.isNotEmpty(map.get("parentId").toString())){
			  		sql=sql+ " and t.parentId  = " + map.get("parentId") +"";
		  	}
		  	if(null !=map.get("dicValue") && StringUtils.isNotEmpty(map.get("dicValue").toString())){
			  		sql=sql+ " and t.dic_value  = " + map.get("dicValue") +"";
		  	}
		  	if(null !=map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())){
			  		sql=sql+ " and t.isDelete  = " + map.get("isDelete") +"";
		  	}
					
					
		sql=sql+ " order by id ";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}

	public boolean updateDicData(Map<String, Object> map) {
		
		String sql ="update v2_dic_data set "
				+ " dic_name=:dicName,parentId=0,dic_value=:dicValue,isDelete=0 "
				+ " where id=:id";
		
		return  baseDAO.executeNamedCommand(sql, map);
	}
	/**
	 * 更改完字典表更新tg_city城市信息
	 * @param map
	 * @return
	 */
	public boolean updateTgCity(Map<String, Object> map) {
		String sql = " update tg_city set "
				+ "  cityName=:dicName "
				+ " where id=:dicValue ";
			
		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteDicData(Map<String, Object> map) {
		
		String sql = "delete from v2_dic_data where id in ("+ map.get("id")+ " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	@SuppressWarnings("unchecked")
	public DicData getById(Map<String, Object> map) {
		
		String sql = "select " + getColumns() + " from v2_dic_data as t where id = "+ map.get("id") + "";
		
		DicData v2DicData=new DicData();
		
		Map m=baseDAO.queryForMap(sql);
		
		try {
			org.apache.commons.beanutils.BeanUtils.copyProperties(v2DicData, m);
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return null;
		}
		
		return v2DicData;
		
	
	}


	public List<Map<String, Object>> getByDataTypeCode(String dicCode
			) {
		String sql = "select "+getColumns()+" from v2_dic_data t"
				+ " inner join v2_dic_type d on t.dic_id=d.dic_id where t.isDelete=0 and d.dic_code='"+dicCode+"'";
		
		return baseDAO.queryForList(sql);
	}


	
	

}
