/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class Sysresource extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "Sysresource";
	public static final String ALIAS_RESOURCE_ID = "自增主键";
	public static final String ALIAS_FORMID = "组件id";
	public static final String ALIAS_XT = "组件名称或id";
	public static final String ALIAS_PARENTCONTAINERID = "父组件id";
	public static final String ALIAS_POSTIONTYPE = "定位类型(id或者为空串'')";
	public static final String ALIAS_RESOURCE_URL = "资源路径";
	public static final String ALIAS_TYPE = "资源类型(0:菜单;1:路径;2:按钮)";
	public static final String ALIAS_CREATED_TIME = "创建时间";
	public static final String ALIAS_STATUS = "状态";
	
	//date formats
	public static final String FORMAT_CREATED_TIME = DATE_FORMAT;
	

	//columns START
    /**
     * 自增主键       db_column: resourceId 
     */ 	
	private java.lang.Integer resourceId;
    /**
     * 组件id       db_column: formid 
     */ 	
	private java.lang.String formid;
    /**
     * 组件名称或id       db_column: xt 
     */ 	
	private java.lang.String xt;
    /**
     * 父组件id       db_column: parentcontainerid 
     */ 	
	private java.lang.String parentcontainerid;
    /**
     * 定位类型(id或者为空串'')       db_column: postiontype 
     */ 	
	private java.lang.String postiontype;
    /**
     * 资源路径       db_column: resourceUrl 
     */ 	
	private java.lang.String resourceUrl;
    /**
     * 资源类型(0:菜单;1:路径;2:按钮)       db_column: type 
     */ 	
	private java.lang.Integer type;
    /**
     * 创建时间       db_column: createdTime 
     */ 	
	private java.util.Date createdTime;
    /**
     * 状态       db_column: status 
     */ 	
	private java.lang.Integer status;
	//columns END

	
	
	public void setResourceId(java.lang.Integer value) {
		this.resourceId = value;
	}
	
	public java.lang.Integer getResourceId() {
		return this.resourceId;
	}
	
	
	
	
	
	
	
	


	public java.lang.String getFormid() {
		return this.formid;
	}
	
	public void setFormid(java.lang.String value) {
		this.formid = value;
	}
	

	public java.lang.String getXt() {
		return this.xt;
	}
	
	public void setXt(java.lang.String value) {
		this.xt = value;
	}
	

	public java.lang.String getParentcontainerid() {
		return this.parentcontainerid;
	}
	
	public void setParentcontainerid(java.lang.String value) {
		this.parentcontainerid = value;
	}
	

	public java.lang.String getPostiontype() {
		return this.postiontype;
	}
	
	public void setPostiontype(java.lang.String value) {
		this.postiontype = value;
	}
	

	public java.lang.String getResourceUrl() {
		return this.resourceUrl;
	}
	
	public void setResourceUrl(java.lang.String value) {
		this.resourceUrl = value;
	}
	

	public java.lang.Integer getType() {
		return this.type;
	}
	
	public void setType(java.lang.Integer value) {
		this.type = value;
	}
	

	public String getCreatedTimeString() {
		return dateToStr(getCreatedTime(), FORMAT_CREATED_TIME);
	}
	public void setCreatedTimeString(String value) {
		setCreatedTime(strToDate(value, java.util.Date.class,FORMAT_CREATED_TIME));
	}

	public java.util.Date getCreatedTime() {
		return this.createdTime;
	}
	
	public void setCreatedTime(java.util.Date value) {
		this.createdTime = value;
	}
	

	public java.lang.Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(java.lang.Integer value) {
		this.status = value;
	}
	

}

