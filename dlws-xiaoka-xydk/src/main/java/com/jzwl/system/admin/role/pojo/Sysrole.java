/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class Sysrole extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "Sysrole";
	public static final String ALIAS_ROLE_ID = "自增主键";
	public static final String ALIAS_NAME = "角色名称";
	public static final String ALIAS_DESCRIPTION = "描述信息";
	public static final String ALIAS_CREATED_TIME = "创建时间";
	public static final String ALIAS_MODIFIED_TIME = "修改时间";
	public static final String ALIAS_MODIFIED_COUNT = "修改次数";
	public static final String ALIAS_STATUS = "状态信息";
	
	//date formats
	public static final String FORMAT_CREATED_TIME = DATE_FORMAT;
	public static final String FORMAT_MODIFIED_TIME = DATE_FORMAT;
	

	//columns START
    /**
     * 自增主键       db_column: roleId 
     */ 	
	private java.lang.Integer roleId;
    /**
     * 角色名称       db_column: name 
     */ 	
	private java.lang.String name;
    /**
     * 描述信息       db_column: description 
     */ 	
	private java.lang.String description;
    /**
     * 创建时间       db_column: createdTime 
     */ 	
	private java.util.Date createdTime;
    /**
     * 修改时间       db_column: modifiedTime 
     */ 	
	private java.util.Date modifiedTime;
    /**
     * 修改次数       db_column: modifiedCount 
     */ 	
	private java.lang.Integer modifiedCount;
    /**
     * 状态信息       db_column: status 
     */ 	
	private java.lang.Integer status;
	//columns END

	
	
	public void setRoleId(java.lang.Integer value) {
		this.roleId = value;
	}
	
	public java.lang.Integer getRoleId() {
		return this.roleId;
	}
	
	
	
	
	
	


	public java.lang.String getName() {
		return this.name;
	}
	
	public void setName(java.lang.String value) {
		this.name = value;
	}
	

	public java.lang.String getDescription() {
		return this.description;
	}
	
	public void setDescription(java.lang.String value) {
		this.description = value;
	}
	

	public String getCreatedTimeString() {
		return dateToStr(getCreatedTime(), FORMAT_CREATED_TIME);
	}
	public void setCreatedTimeString(String value) {
		setCreatedTime(strToDate(value, java.util.Date.class,FORMAT_CREATED_TIME));
	}

	public java.util.Date getCreatedTime() {
		return this.createdTime;
	}
	
	public void setCreatedTime(java.util.Date value) {
		this.createdTime = value;
	}
	

	public String getModifiedTimeString() {
		return dateToStr(getModifiedTime(), FORMAT_MODIFIED_TIME);
	}
	public void setModifiedTimeString(String value) {
		setModifiedTime(strToDate(value, java.util.Date.class,FORMAT_MODIFIED_TIME));
	}

	public java.util.Date getModifiedTime() {
		return this.modifiedTime;
	}
	
	public void setModifiedTime(java.util.Date value) {
		this.modifiedTime = value;
	}
	

	public java.lang.Integer getModifiedCount() {
		return this.modifiedCount;
	}
	
	public void setModifiedCount(java.lang.Integer value) {
		this.modifiedCount = value;
	}
	

	public java.lang.Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(java.lang.Integer value) {
		this.status = value;
	}
	

}

