/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.dicdate.service;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.dicdate.dao.DicDataDao;
import com.jzwl.system.admin.dicdate.pojo.*;



@Service("v2DicDataService")
public class DicDataService {
	
	
	@Autowired
	private DicDataDao dicDataDao;
	
	public boolean addDicData(Map<String, Object> map) {
		
		return dicDataDao.addDicData(map);
		
	}

	public PageObject queryDicDataList(Map<String, Object> map) {
		
		return dicDataDao.queryDicDataList(map);
	
	}

	public boolean updateDicData(Map<String, Object> map) {
	
		 boolean flag = dicDataDao.updateDicData(map);
		 if(flag){
			flag =  dicDataDao.updateTgCity(map);
		 }else{
			 flag = false;
		 }
		 return flag; 
	
	}

	public boolean deleteDicData(Map<String, Object> map) {
		
		return dicDataDao.deleteDicData(map);
	
	}
	
	
	@SuppressWarnings("unchecked")
	public DicData getById(Map<String, Object> map) {
		
		return dicDataDao.getById(map);
		
	}

	public List<Map<String, Object>> getByDataTypeCode(String dicCode) {

		return dicDataDao.getByDataTypeCode(dicCode);
	}
	
	
}
