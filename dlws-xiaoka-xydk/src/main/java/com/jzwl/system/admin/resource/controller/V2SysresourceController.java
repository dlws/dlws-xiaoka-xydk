package com.jzwl.system.admin.resource.controller;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.resource.service.V2SysresourceService;
import com.jzwl.system.base.controller.BaseController;

/**
 * V2资源管理控制层
 * 
 * @author H.Jock
 * @Time 2016-1-6 14:01:22
 * @version 1.0
 * 
 */
@Controller("V2SysresourceController")
@RequestMapping("/v2Sysresource")
public class V2SysresourceController extends BaseController {

	@Autowired
	private V2SysresourceService sysresourceService;

	/**
	 * 跳转资源管理主页面
	 * 
	 * @author H.Jock
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/v2sysresourceList")
	public String sysresourceList(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		PageObject po = new PageObject();

		try {
			po = sysresourceService.queryResourceList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap", paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}
		return "/base/system/resourcelist";
	}

	/**
	 * 跳转到添加页面
	 * 
	 * @author H.Jock
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/toAddResource")
	public String toAddRole(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		model.addAttribute("url", "v2Sysresource/v2addSysresource.html");
		return "/base/system/resourceform";
	}

	/**
	 * 添加Sysresource
	 * 
	 * @author H.Jock
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/v2addSysresource", method = RequestMethod.POST)
	public String v2addSysresource(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);// 封装请求参数
		paramsMap.put("createdTime", new Date());// 写入资源创建时间
		try {
			boolean flag = sysresourceService.addSysresource(paramsMap);
			if (flag) {
				return "redirect:/v2Sysresource/v2sysresourceList.html?start=0";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}
	}

	/**
	 * 跳转修改资源页面
	 * 
	 * @author H.Jock
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/toUpdateResource")
	public String toUpdateResource(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			Map<String, Object> objMap = sysresourceService
					.getResource(paramsMap);
			model.addAttribute("objMap", objMap);
			model.addAttribute("style", "style='display:none'");
			model.addAttribute("url", "v2Sysresource/v2updateSysresource.html");
			return "/base/system/resourceform";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}
	}

	/**
	 * 修改Sysresource
	 * 
	 * @author H.Jock
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/v2updateSysresource", method = RequestMethod.POST)
	public String v2updateSysresource(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);// 封装请求参数
		try {
			boolean flag = sysresourceService.updateSysresource(paramsMap);
			if (flag) {
				return "redirect:/v2Sysresource/v2sysresourceList.html?start=0";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}
	}

	/**
	 * 删除资源
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/v2deleteResource")
	public String deleteResource(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);// 封装请求参数
		try {
			boolean flag = sysresourceService.deleteSysresource(paramsMap);
			if (flag) {
				return "redirect:/v2Sysresource/v2sysresourceList.html?start=0";
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}
	}

}
