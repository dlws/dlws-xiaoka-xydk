package com.jzwl.system.admin.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.admin.user.service.BusyShopuserService;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {

	@Autowired
	private BusyShopuserService busyShopuserService;
	
	@RequestMapping(value = "/existUserName")//, method = RequestMethod.POST
	public @ResponseBody Map existUserName(HttpServletRequest request, HttpServletResponse response,String username){
		Map map = new HashMap();

		if (StringUtil.isEmptyOrBlank(username)) {
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);// json串内，必须携带ajax_status字段，用来判定本次ajax操作是否成功
			map.put("flag", -1);
			return map;
		}
		// 查询教验用户登录的名称
		List<Map<String, Object>> dbUserlist = busyShopuserService
				.getBusyShopUser(username);
		// 判断
		if (dbUserlist.size() > 0) {
			// 当前输入的登录名已经存在
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			map.put("flag", 1);
		} else {
			// 当前输入的登录名不存在
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("flag", 0);
		}
		return map;
	}

	@RequestMapping(value = "/existVerifycode")
	// , method = RequestMethod.POST
	public @ResponseBody
	Map existVerifycode(HttpServletRequest request,
			HttpServletResponse response, String verifycode) {

		Map map = new HashMap();

		String confirmVerifycode = (String) request.getSession().getAttribute(
				"Verifycode");

		// 判断
		if (!verifycode.equals(confirmVerifycode)) {
			
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			map.put("flag", 1);
			
		} else {
			
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("flag", 0);
		}
		return map;
	}
	
	@RequestMapping(value = "/existWeixinCode")//, method = RequestMethod.POST
	public @ResponseBody Map existWeixinCode(HttpServletRequest request, HttpServletResponse response,String weixincode){
		
		Map map = new HashMap();
		
		//此处应该获取微信扫码激活后生成的验证码
		
//		String weixincodeTwo = (String)request.getSession().getAttribute("weixincode");
		
		
		//判断
		if (!weixincode.equals("4444")) {
			
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			map.put("flag", 1);
			
		} else {
			
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("flag", 0);
		}
		return map;
	}

	/**
	 * 找回密码时验证用户名是否存在
	 * 
	 * @param request
	 * @param response
	 * @param username
	 * @return
	 */
	@RequestMapping(value = "/verificationUserName")
	// , method = RequestMethod.POST
	public @ResponseBody
	Map verificationUserName(HttpServletRequest request,
			HttpServletResponse response, String xiaokaNumber) {
		Map map = new HashMap();

		if (StringUtil.isEmptyOrBlank(xiaokaNumber)) {
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);// json串内，必须携带ajax_status字段，用来判定本次ajax操作是否成功
			map.put("flag", -1);
			return map;
		}
		// 查询教验用户登录的名称
		List<Map<String, Object>> dbUserlist = busyShopuserService
				.getBusyShopUser(xiaokaNumber);

		// 判断
		if (dbUserlist.size() > 0) {

			// 当前输入的登录名已经存在
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("flag", 0);
		} else {
			// 当前输入的登录名不存在
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			map.put("flag", 1);
		}
		return map;
	}

	/**
	 * 检验用户输入的验证码是否正确
	 * @return
	 */
	private boolean isCheckCode(String code) {

		try {

			if(StringUtils.isNotBlank(code)){
				
				
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();

			return false;

		}
		return false;

	}

}
