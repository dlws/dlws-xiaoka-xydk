package com.jzwl.system.admin.sysuser.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.sysuser.dao.*;
import com.jzwl.system.admin.user.pojo.BusyShopuser;

@Service("v2sysuserService")
public class SysuserService {

	@Autowired
	private SysuserDao sysuserDao;

	public boolean addSysuser(Map<String, Object> map) {

		return sysuserDao.addSysuser(map);

	}

	public PageObject querySysuserList(Map<String, Object> map) {

		return sysuserDao.querySysuserList(map);

	}

	public boolean updateSysuser(Map<String, Object> map) {

		boolean flag_user = sysuserDao.updateSysuser(map);
		return flag_user;
	}

	public boolean updatePasswordSysuser(Map<String, Object> map) {

		return sysuserDao.updatePasswordSysuser(map);

	}

	//根据id获取单个用户的信息
	public Map<String, Object> getSysuser(Map<String, Object> map) {

		return sysuserDao.getSysuserById(map);

	}

	public boolean deleteSysuser(Map<String, Object> map) {

		return sysuserDao.deleteSysuser(map);

	}

	public Map<String, Object> queryUser(Map<String, Object> map) {

		return sysuserDao.queryUser(map);
	}

	public List<Map<String, Object>> queryRoleResourceList(String roleId) {

		return sysuserDao.queryRoleResourceList(roleId);
	}

	public List<Map<String, Object>> queryUserByName(String username) {

		return sysuserDao.queryUserByName(username);
	}

	public List<Map<String, Object>> findAuthorityByUserId(String userId) {

		return sysuserDao.findAuthorityByUserId(userId);
	}

	public List<Map<String, Object>> queryUserResourceList(String userId) {

		return sysuserDao.queryUserResourceList(userId);
	}

	public List<Map<String, Object>> queryConsultList(
			Map<String, Object> paramsMap) {
		return sysuserDao.queryConsultList(paramsMap);
	}

	public PageObject queryCityInShopList(Map<String, Object> map) {

		return sysuserDao.queryCityInShopList(map);

	}

	public PageObject queryNoCityShopList(Map<String, Object> map) {

		return sysuserDao.queryNoCityShopList(map);

	}

	public boolean addShopToUser(Map<String, Object> map) {

		return sysuserDao.addShopToUser(map);

	}

	public boolean deleteShopToUser(Map<String, Object> map) {

		return sysuserDao.deleteShopToUser(map);

	}

	public List<Map<String, Object>> queryUserShop(Map<String, Object> paramsMap) {
		return sysuserDao.queryUserShop(paramsMap);
	}

	public Map<String, Object> queryUserRole(Map<String, Object> map) {

		return sysuserDao.queryUserRole(map);
	}

	public boolean addShopToUserDZ(Map<String, Object> map) {

		return sysuserDao.addShopToUserDZ(map);

	}

	/**
	 * 查询用户已拥有角色
	 * 
	 * @param paramsMap
	 * @return
	 */
	public PageObject querySysroleUserList(Map<String, Object> paramsMap) {
		return sysuserDao.querySysroleUserList(paramsMap);
	}

	/**
	 * 查询用户未拥有角色
	 * 
	 * @param paramsMap
	 * @return
	 */
	public PageObject querySysroleNoUserList(Map<String, Object> paramsMap) {
		return sysuserDao.querySysroleNoUserList(paramsMap);
	}

	/**
	 * 删除用户角色关系
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean deleteSysroleUser(Map<String, Object> paramsMap) {
		return sysuserDao.deleteSysroleUser(paramsMap);
	}

	/**
	 * 添加用户角色关系
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean addSysroleUser(Map<String, Object> paramsMap) {
		return sysuserDao.addSysroleUser(paramsMap);
	}

	/**
	 * 根据用户名查询用户
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findUserByName(String name) throws Exception {
		return sysuserDao.findUserByName(name);
	}

	/**
	 * 根据用户ID重新用户的权限
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> findPrivilegesByUserId(String userId)
			throws Exception {
		return sysuserDao.findPrivilegesByUserId(userId);
	}

	/**
	 * 根据用户名判断用户是否已存在
	 * 
	 * @param string
	 * @return
	 */
	public boolean isUserLive(String username) {
		return sysuserDao.isUserLive(username);
	}

	/**
	 * 根据userId获取用户角色
	 * 
	 * @param valueOf
	 * @return
	 */
	public Map findRolerByUserId(String userId) {
		return sysuserDao.findRolerByUserId(userId);
	}
//getuser
	

}
