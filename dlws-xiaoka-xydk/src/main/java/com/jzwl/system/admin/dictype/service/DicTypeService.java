/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.dictype.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.dictype.dao.DicTypeDao;
import com.jzwl.system.admin.dictype.pojo.DicType;




@Service("v2DicTypeService")
public class DicTypeService {
	
	
	@Autowired
	private DicTypeDao dicTypeDao;
	
	public boolean addDicType(Map<String, Object> map) {
		
		return dicTypeDao.addDicType(map);
		
	}

	public PageObject queryDicTypeList(Map<String, Object> map) {
		
		return dicTypeDao.queryDicTypeList(map);
	
	}

	public boolean updateDicType(Map<String, Object> map) {
	
		return dicTypeDao.updateDicType(map);
	
	}

	public boolean deleteDicType(Map<String, Object> map) {
		
		return dicTypeDao.deleteDicType(map);
	
	}
	
	
	@SuppressWarnings("unchecked")
	public DicType getByDicId(Map<String, Object> map) {
		
		return dicTypeDao.getByDicId(map);
		
	}
	
	
}
