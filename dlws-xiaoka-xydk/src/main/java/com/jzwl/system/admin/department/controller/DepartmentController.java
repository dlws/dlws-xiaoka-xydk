/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.department.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.system.admin.department.pojo.*;
import com.jzwl.system.admin.department.service.*;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.common.page.PageObject;

/**
 * V2Department
 * V2Department
 * v2Department
 * <p>Title:V2DepartmentController </p>
 * 	<p>Description:V2Department </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("v2DepartmentController")
@RequestMapping("/v2Department")
public class DepartmentController  extends BaseController {
	
	@Autowired
	private DepartmentService departmentService;
	
	
	/**
	 * 跳转到添加页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/departmentAdd")
	public String departmentAdd(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		model.addAttribute("url", "v2Department/addDepartment.html");
		model.addAttribute("type", "add");
		return "/base/system/department_form";
	}
	/**
	 * 添加V2Department
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addDepartment")
	public String addDepartment(HttpServletRequest request, HttpServletResponse response) {
		
		createParameterMap(request);
		
		if(paramsMap.get("departmentId") == null||paramsMap.get("departmentId") ==""){
			paramsMap.put("parentid", 0);
		}
		paramsMap.put("isdelete", 0);
		try{
		
			boolean flag = departmentService.addDepartment(paramsMap);
		
			if(flag){
			
				return "redirect:/v2Department/departmentList.html";
			}else{
			
				return "redirect:/error";
			}
		
		}catch(Exception e){
			
			e.printStackTrace();
			
			return "redirect:/error";
		}
		
	}
	
	/**
	 * v2Departmentlist
	 */
	@RequestMapping(value = "/departmentList")
	public ModelAndView departmentList(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new ModelAndView();
	
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		paramsMap.put("isdelete", 0);
		paramsMap.put("parentid", 0);
		try{
		
			po = departmentService.queryDepartmentList(paramsMap);
			
			mov.addObject("po", po);
			
			mov.setViewName("/base/system/department");
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			mov.addObject("msg", "数据加载失败:"+e.getMessage());
			
			mov.setViewName("/error");
		
		}
		
		return mov;
	}
	
	
	
	/**
	 * getV2Department
	 */
	@RequestMapping(value = "/getDepartment")
	public ModelAndView getDepartment(HttpServletRequest request, HttpServletResponse response) {
	
		ModelAndView mov = new ModelAndView();
		
		createParameterMap(request);
		
		try{
			
			Department department=departmentService.getByDepartmentId(paramsMap);
			
			mov.addObject("department", department);
			mov.addObject("url", "v2Department/updateDepartment.html");
			mov.addObject("type", "edit");
			mov.setViewName("/base/system/department_form");
			
		
		}catch(Exception e){
		
			e.printStackTrace();
			mov.addObject("msg","获取数据失败："+e.getMessage());
			mov.setViewName("/error");
			
		}
		
		return mov;
	}
	
	
	/**
	 * 修改v2Department
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateDepartment")
	public String updateDepartment(HttpServletRequest request, HttpServletResponse response) {
		
		createParameterMap(request);
		
		try{
		
			boolean flag = departmentService.updateDepartment(paramsMap);
			
			if(flag){
			
				return "redirect:/v2Department/departmentList.html";
			
			}else{
			
				return"/error";
			
			}
		}catch(Exception e){
		
			return"/error";
		}
		
	}
	
	
	/**
	 * 删除v2Department
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteDepartment")
	public String deleteDepartment(HttpServletRequest request, HttpServletResponse response) {
		
		createParameterMap(request);
		
		try{
		
			boolean flag = departmentService.deleteDepartment(paramsMap);
			
			if(flag){
			
				return "redirect:/v2Department/departmentList.html";
			
			}else{
			
				return"/error";
			
			}
		}catch(Exception e){
			
			return"/error";
		
		}
	}
	
	//===================下级设置========================
	/**
	 * v2Departmentlist
	 */
	@RequestMapping(value = "/departmentListChildren")
	public ModelAndView departmentListChildren(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new ModelAndView();
	
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		paramsMap.put("isdelete", 0);
		
		try{
		
			po = departmentService.queryDepartmentListChildren(paramsMap);
			
			mov.addObject("po", po);
			mov.addObject("parentid", paramsMap.get("parentid"));
			mov.addObject("departmentname", paramsMap.get("departmentname"));
			
			mov.setViewName("/base/system/department_children");
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			mov.addObject("msg", "数据加载失败:"+e.getMessage());
			
			mov.setViewName("/error");
		
		}
		
		return mov;
	}
	
	/**
	 * 跳转到添加页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/departmentAddChildren")
	public String departmentAddChildren(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		createParameterMap(request);
		
		model.addAttribute("url", "v2Department/addDepartmentChildren.html");
		model.addAttribute("type", "add");
		model.addAttribute("parentid",paramsMap.get("parentid"));
		return "/base/system/department_form";
	}
	
	/**
	 * 添加V2Department
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addDepartmentChildren")
	public ModelAndView addDepartmentChildren(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new  ModelAndView();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		paramsMap.put("isdelete", 0);
		paramsMap.put("parentid", paramsMap.get("parentid"));
		
		boolean flag = false;
		try{
		
			flag = departmentService.addDepartment(paramsMap);
			
			if(flag){
			
				po = departmentService.queryDepartmentListChildren(paramsMap);
				
				mov.addObject("po", po);
				mov.addObject("departmentId", paramsMap.get("departmentId"));
				mov.addObject("departmentname", paramsMap.get("departmentname"));
				mov.addObject("parentid", paramsMap.get("parentid"));
				mov.setViewName("/base/system/department_children");
			}else{
			
				mov.addObject("flag", flag);
				mov.setViewName("/error");
			}
		
		}catch(Exception e){
			
			mov.addObject("flag", flag);
			mov.setViewName("/error");
		}
		 return mov;
		
	}
	
	/**
	 * getV2Department
	 */
	@RequestMapping(value = "/getDepartmentChildren")
	public ModelAndView getDepartmentChildren(HttpServletRequest request, HttpServletResponse response) {
	
		ModelAndView mov = new ModelAndView();
		
		createParameterMap(request);
		
		paramsMap.put("isdelete", 0);
		
		paramsMap.put("parentid", paramsMap.get("parentid"));
		
		try{
			
			Department department=departmentService.getByDepartmentId(paramsMap);
			
			mov.addObject("department", department);
			mov.addObject("url", "v2Department/updateDepartmentChildren.html");
			mov.addObject("type", "edit");
			mov.addObject("parentid", paramsMap.get("parentid"));
			mov.setViewName("/base/system/department_form");
			
		
		}catch(Exception e){
		
			e.printStackTrace();
			mov.addObject("msg","获取数据失败："+e.getMessage());
			mov.setViewName("/error");
			
		}
		
		return mov;
	}
	
	/**
	 * 修改v2Department
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateDepartmentChildren")
	public ModelAndView updateDepartmentChildren(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new  ModelAndView();
		
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		paramsMap.put("isdelete", 0);
		
		paramsMap.put("parentid", paramsMap.get("parentid"));
		
		try{
		
			boolean flag = departmentService.updateDepartment(paramsMap);
			
			if(flag){
			
				po = departmentService.queryDepartmentListChildren(paramsMap);
				
				mov.addObject("po", po);
				mov.addObject("departmentId", paramsMap.get("departmentId"));
				mov.addObject("departmentname", paramsMap.get("departmentname"));
				mov.addObject("parentid", paramsMap.get("parentid"));
				mov.setViewName("/base/system/department_children");
			
			}else{
			
				mov.setViewName("/error");
			
			}
		}catch(Exception e){
		
			e.printStackTrace();
			mov.addObject("msg","获取数据失败："+e.getMessage());
			mov.setViewName("/error");
			
		}
		return mov;
		
	}
	
	/**
	 * 删除v2Department
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteDepartmentChildren")
	public ModelAndView deleteDepartmentChildren(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new  ModelAndView();
		
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		paramsMap.put("isdelete", 0);
		
		paramsMap.put("parentid", paramsMap.get("parentid"));
		
		try{
		
			boolean flag = departmentService.deleteDepartment(paramsMap);
			
			if(flag){
			
				po = departmentService.queryDepartmentListChildren(paramsMap);
				
				mov.addObject("po", po);
				mov.addObject("departmentId", paramsMap.get("departmentId"));
				mov.addObject("departmentname", paramsMap.get("departmentname"));
				mov.addObject("parentid", paramsMap.get("parentid"));
				mov.setViewName("/base/system/department_children");
			
			}else{
			
				mov.setViewName("/error");
			
			}
		}catch(Exception e){
			
			e.printStackTrace();
			mov.addObject("msg","获取数据失败："+e.getMessage());
			mov.setViewName("/error");
		
		}
		
		return mov;
	}
	/**
	 * 获取前台页面树状菜单的值
	 */
	@RequestMapping(value="/showData") 
	@ResponseBody
	public List<TreeNode> showData()throws Exception{
		
		return getList();
	}
	/**
	 * 因为ztree需要的字节名称为name,再此进行转换
	 * @return
	 */
	public List<TreeNode> getList() throws Exception{
		
		List<Department> getList =departmentService.getDepartmentList();
		List<TreeNode> list = new ArrayList<TreeNode>();
		
		for(int i = 0;i<getList.size();i++){
			list.add(new TreeNode(getList.get(i).getDepartmentId(),getList.get(i).getParentid(),getList.get(i).getDepartmentname(),false,false,false));
		}
		return list;
	}
	
	
}

