package com.jzwl.system.admin.resource.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.resource.dao.V2SysresourceDao;

/**
 * V2资源管理service层
 * 
 * @author H.Jock
 * @Time 2016-1-6 14:03:45
 * @version 1.0
 * 
 */
@Service("V2SysresourceService")
public class V2SysresourceService {

	@Autowired
	private V2SysresourceDao sysresourceDao;

	/**
	 * 条件查询资源列表
	 * 
	 * @param map
	 * @return
	 */
	public PageObject queryResourceList(Map<String, Object> map) {
		return sysresourceDao.queryResourceList(map);
	}

	/**
	 * 新建资源
	 * 
	 * @param map
	 * @return
	 */
	public boolean addSysresource(Map<String, Object> map) {
		return sysresourceDao.addSysresource(map);
	}

	/**
	 * 通过参数获取资源对象
	 * 
	 * @param paramsMap
	 * @return
	 */
	public Map<String, Object> getResource(Map<String, Object> paramsMap) {
		return sysresourceDao.getResource(paramsMap);
	}

	/**
	 * 修改资源
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean updateSysresource(Map<String, Object> paramsMap) {
		return sysresourceDao.updateSysresource(paramsMap);
	}

	/**
	 * 删除资源
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean deleteSysresource(Map<String, Object> paramsMap) {
		return sysresourceDao.deleteSysresource(paramsMap);
	}

}
