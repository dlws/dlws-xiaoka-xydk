package com.jzwl.system.admin.authority.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.system.admin.authority.service.V2SysauthorityService;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.common.page.PageObject;

/**
 * V2权限管理控制层
 * 
 * @author H.Jock
 * @Time 2016-1-7 11:12:34
 * @version 1.0
 * 
 */
@Controller("V2SysauthorityController")
@RequestMapping("/v2Sysauthority")
public class V2SysauthorityController extends BaseController {

	@Autowired
	private V2SysauthorityService sysauthorityService;

	/**
	 * 跳转权限页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/v2sysauthorityList")
	public String sysauthorityList(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = sysauthorityService.queryAuthorityList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap", paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}
		return "/base/system/authoritylist";
	}

	/**
	 * 跳转到添加页面
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/toAddAuthority")
	public String toAddAuthority(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		model.addAttribute("url", "v2Sysauthority/v2addSysauthority.html");
		return "/base/system/authorityform";
	}

	/**
	 * 添加权限
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/v2addSysauthority", method = RequestMethod.POST)
	public String v2addSysauthority(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);// 封装请求参数
		try {
			boolean flag = sysauthorityService.addSysauthority(paramsMap);
			if (flag) {
				return "redirect:/v2Sysauthority/v2sysauthorityList.html?start=0";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}
	}

	/**
	 * 跳转修改页面
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/toUpdateAuthority")
	public String toUpdateAuthority(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			Map<String, Object> objMap = sysauthorityService
					.getAuthority(paramsMap);
			model.addAttribute("objMap", objMap);
			model.addAttribute("style", "style='display:none'");
			model.addAttribute("url",
					"v2Sysauthority/v2updateSysauthority.html");
			return "/base/system/authorityform";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}
	}

	/**
	 * 更新权限
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/v2updateSysauthority", method = RequestMethod.POST)
	public String v2updateSysauthority(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);// 封装请求参数
		try {
			boolean flag = sysauthorityService.updateSysauthority(paramsMap);
			if (flag) {
				return "redirect:/v2Sysauthority/v2sysauthorityList.html?start=0";
			} else {
				model.addAttribute("msg", "修改失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "修改失败");
			return "/error";
		}
	}

	/**
	 * 删除权限
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/deleteAuthority")
	public String v2deleteAuthority(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			boolean flag = sysauthorityService.deleteAuthority(paramsMap);
			if (flag) {
				return "redirect:/v2Sysauthority/v2sysauthorityList.html?start=0";
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}
	}

	/**
	 * 获取权限已拥有资源列表
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/v2SysresourceAuthList", method = RequestMethod.POST)
	public Map<String, Object> sysresourceAuthList(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		createParameterMap(request);
		//paramsMap.put("type", 1);
		//PageObject po = new PageObject();
		try {
			List querySysresourceAuthList = sysauthorityService.querySysresourceAuthList(paramsMap);
			resultMap.put("list", querySysresourceAuthList);
			//resultMap.put("totalProperty", po.getTotalCount());
		} catch (Exception e) {
			e.printStackTrace();
			/*resultMap.put("list", po.getDatasource());
			resultMap.put("totalProperty", po.getTotalCount());*/
		}
		return resultMap;
	}

	/**
	 * 获取权限未拥有资源列表
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/v2SysresourceNoAuthList", method = RequestMethod.POST)
	public Map<String, Object> sysresourceNoAuthList(
			HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		createParameterMap(request);
		//paramsMap.put("type", 1);
		//PageObject po = new PageObject();
		try {
			List authList = sysauthorityService.querySysresourceNoAuthList(paramsMap);
			resultMap.put("list", authList);
			//resultMap.put("totalProperty", po.getTotalCount());
		} catch (Exception e) {
			e.printStackTrace();
			/*resultMap.put("list", po.getDatasource());
			resultMap.put("totalProperty", po.getTotalCount());*/
		}
		return resultMap;
	}

	/**
	 * 添加资源权限关系
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addSysresourceAuth", method = RequestMethod.POST)
	public Map<String, Object> sysresourceAuth(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		createParameterMap(request);
		try {
			boolean flag = sysauthorityService.addSysresourceAuth(paramsMap);
			if (flag) {
				resultMap.put("success", true);
				resultMap.put("info", "添加成功！");
			} else {
				resultMap.put("info", "添加失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("info", "添加失败!");
		}
		return resultMap;
	}

	/**
	 * 删除资源权限关系
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSysresourceAuth", method = RequestMethod.POST)
	public Map<String, Object> deleteSysresourceAuth(
			HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		createParameterMap(request);
		try {
			boolean flag = sysauthorityService.deleteSysresourceAuth(paramsMap);
			if (flag) {
				resultMap.put("success", true);
				resultMap.put("info", "删除成功！");
			} else {
				resultMap.put("info", "删除失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("info", "删除失败！");
		}
		return resultMap;
	}
}
