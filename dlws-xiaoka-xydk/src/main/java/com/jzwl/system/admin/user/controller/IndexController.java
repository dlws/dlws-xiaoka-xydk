package com.jzwl.system.admin.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.system.base.controller.BaseController;

/**
 * <p>Title:IndexController </p>
 * 	<p>Description: 系统首页对应的controller</p>
 * 	<p>Company: </p> 
 * 	@author Administrator 
 * 	@date 2015年8月20日 上午10:52:51
 */
@Controller
@RequestMapping("/index")
public class IndexController extends BaseController {
	
	/**
	 * 首页跳转controller
	 */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("userinfo",request.getSession().getAttribute("userinfo"));
		mv.setViewName("/index");
		return mv;
	}
}
