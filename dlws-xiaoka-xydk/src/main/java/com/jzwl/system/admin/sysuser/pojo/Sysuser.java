/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.sysuser.pojo;

import com.jzwl.system.base.pojo.BasePojo;

public class Sysuser extends BasePojo implements java.io.Serializable {

	private static final long serialVersionUID = 5454155825314635342L;

	// alias
	public static final String TABLE_ALIAS = "Sysuser";
	public static final String ALIAS_USER_ID = "userId";
	public static final String ALIAS_EMP_ID = "员工编号";
	public static final String ALIAS_USERNAME = "username";
	public static final String ALIAS_PASSWORD = "password";
	public static final String ALIAS_EMAIL = "email";
	public static final String ALIAS_REALNAME = "realname";
	public static final String ALIAS_DESCRIPTION = "description";
	public static final String ALIAS_CREATED_TIME = "createdTime";
	public static final String ALIAS_MODIFIED_TIME = "modifiedTime";
	public static final String ALIAS_MODIFIED_COUNT = "modifiedCount";
	public static final String ALIAS_STATUS = "status";
	public static final String ALIAS_LAST_LOGIN_TIME = "lastLoginTime";
	public static final String ALIAS_LOGIN_TIME = "loginTime";
	public static final String ALIAS_LOGIN_COUNT = "loginCount";
	public static final String ALIAS_SCID = "scid";
	public static final String ALIAS_IS_SCHOOOL_MASTER = "isSchooolMaster";

	// columns START
	/**
	 * userId db_column: userId
	 */
	private java.lang.Integer userId;
	/**
	 * 员工编号 db_column: empId
	 */
	private java.lang.String empId;
	/**
	 * username db_column: username
	 */
	private java.lang.String username;
	/**
	 * password db_column: password
	 */
	private java.lang.String password;
	/**
	 * email db_column: email
	 */
	private java.lang.String email;
	/**
	 * realname db_column: realname
	 */
	private java.lang.String realname;
	/**
	 * description db_column: description
	 */
	private java.lang.String description;
	/**
	 * createdTime db_column: createdTime
	 */
	private java.util.Date createdTime;
	/**
	 * modifiedTime db_column: modifiedTime
	 */
	private java.util.Date modifiedTime;
	/**
	 * modifiedCount db_column: modifiedCount
	 */
	private java.lang.Integer modifiedCount;
	/**
	 * status db_column: status
	 */
	private java.lang.Integer status;
	/**
	 * lastLoginTime db_column: lastLoginTime
	 */
	private java.util.Date lastLoginTime;
	/**
	 * loginTime db_column: loginTime
	 */
	private java.util.Date loginTime;
	/**
	 * loginCount db_column: loginCount
	 */
	private java.lang.Integer loginCount;
	/**
	 * scid db_column: scid
	 */
	private java.lang.Integer scid;
	/**
	 * isSchooolMaster db_column: isSchooolMaster
	 */
	private java.lang.Integer isSchooolMaster;

	// columns END

	public void setUserId(java.lang.Integer value) {
		this.userId = value;
	}

	public java.lang.Integer getUserId() {
		return this.userId;
	}

	public java.lang.String getEmpId() {
		return this.empId;
	}

	public void setEmpId(java.lang.String value) {
		this.empId = value;
	}

	public java.lang.String getUsername() {
		return this.username;
	}

	public void setUsername(java.lang.String value) {
		this.username = value;
	}

	public java.lang.String getPassword() {
		return this.password;
	}

	public void setPassword(java.lang.String value) {
		this.password = value;
	}

	public java.lang.String getEmail() {
		return this.email;
	}

	public void setEmail(java.lang.String value) {
		this.email = value;
	}

	public java.lang.String getRealname() {
		return this.realname;
	}

	public void setRealname(java.lang.String value) {
		this.realname = value;
	}

	public java.lang.String getDescription() {
		return this.description;
	}

	public void setDescription(java.lang.String value) {
		this.description = value;
	}

	public java.util.Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(java.util.Date value) {
		this.createdTime = value;
	}

	public java.util.Date getModifiedTime() {
		return this.modifiedTime;
	}

	public void setModifiedTime(java.util.Date value) {
		this.modifiedTime = value;
	}

	public java.lang.Integer getModifiedCount() {
		return this.modifiedCount;
	}

	public void setModifiedCount(java.lang.Integer value) {
		this.modifiedCount = value;
	}

	public java.lang.Integer getStatus() {
		return this.status;
	}

	public void setStatus(java.lang.Integer value) {
		this.status = value;
	}

	public java.util.Date getLastLoginTime() {
		return this.lastLoginTime;
	}

	public void setLastLoginTime(java.util.Date value) {
		this.lastLoginTime = value;
	}

	public java.util.Date getLoginTime() {
		return this.loginTime;
	}

	public void setLoginTime(java.util.Date value) {
		this.loginTime = value;
	}

	public java.lang.Integer getLoginCount() {
		return this.loginCount;
	}

	public void setLoginCount(java.lang.Integer value) {
		this.loginCount = value;
	}

	public java.lang.Integer getScid() {
		return this.scid;
	}

	public void setScid(java.lang.Integer value) {
		this.scid = value;
	}

	public java.lang.Integer getIsSchooolMaster() {
		return this.isSchooolMaster;
	}

	public void setIsSchooolMaster(java.lang.Integer value) {
		this.isSchooolMaster = value;
	}

}
