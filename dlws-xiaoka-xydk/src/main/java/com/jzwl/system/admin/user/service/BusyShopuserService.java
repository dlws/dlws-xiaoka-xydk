package com.jzwl.system.admin.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.user.dao.BusyShopuserDao;
import com.jzwl.system.admin.user.pojo.BusyShopuser;


@Service("busyShopuserService")
public class BusyShopuserService {
	
	
	@Autowired
	private BusyShopuserDao busyShopuserDao;
	
	public boolean addBusyShopuser(Map<String, Object> map) {
		
		return busyShopuserDao.addBusyShopuser(map);
		
	}

	public PageObject queryBusyShopuserList(Map<String, Object> map) {
		
		return busyShopuserDao.queryBusyShopuserList(map);
	
	}

	public boolean updateBusyShopuser(Map<String, Object> map) {
	
		return busyShopuserDao.updateBusyShopuser(map);
	
	}
	
	public boolean updatePassWord(Map<String, Object> map) {
		
		return busyShopuserDao.updatePassWord(map);
	
	}

	public boolean deleteBusyShopuser(Map<String, Object> map) {
		
		return busyShopuserDao.deleteBusyShopuser(map);
	
	}
	
	
	@SuppressWarnings("unchecked")
	public BusyShopuser getById(Map<String, Object> map) {
		
		return busyShopuserDao.getById(map);
		
	}
	
	public List<Map<String, Object>> getBusyShopUser(String username) {
		
		return busyShopuserDao.getBusyShopUser(username);
		
	}
	
	public List<Map<String, Object>> getPlatUser(String username) {
		
		return busyShopuserDao.getPlatUser(username);
		
	}
	
}