/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;






import com.jzwl.system.admin.role.dao.*;
import com.jzwl.system.admin.role.pojo.*;
import com.jzwl.system.admin.role.service.*;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.common.page.PageObject;

/**
 * Sysresource
 * Sysresource
 * sysresource
 * <p>Title:SysresourceController </p>
 * 	<p>Description:Sysresource </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("v2sysresourceController")
@RequestMapping("/v2sysresource")
public class SysresourceController  extends BaseController {
	
	@Autowired
	private SysresourceService sysresourceService;
	
	/**
	 * 添加Sysresource
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addSysresource", method = RequestMethod.POST)
	public Map<String,Object> sysresource(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = sysresourceService.addSysresource(paramsMap);
		
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "添加成功！");
			}else{
			
				resultMap.put("info", "添加失败！");
			}
		
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "添加失败!");
		}
		
		return resultMap;
		
	}
	
	/**
	 * sysresourcelist
	 */
	@ResponseBody
	@RequestMapping(value = "/sysresourceList", method = RequestMethod.POST)
	public Map<String,Object> sysresourceList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			po = sysresourceService.querySysresourceList(paramsMap);
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}
		
		return resultMap;
	}
	
	
	
	/**
	 * getSysresource
	 */
	@ResponseBody
	@RequestMapping(value = "/getSysresource", method = RequestMethod.POST)
	public Map<String,Object> getSysresource(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
			
			Sysresource sysresource=sysresourceService.getByResourceId(paramsMap);
			
			resultMap.put("success", true);
			resultMap.put("obj", sysresource);
			
		
		}catch(Exception e){
		
			resultMap.put("info", "获取失败");
			
			e.printStackTrace();
			
		}
		
		return resultMap;
	}
	
	
	/**
	 * 修改sysresource
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateSysresource", method = RequestMethod.POST)
	public Map<String,Object> updateSysresource(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = sysresourceService.updateSysresource(paramsMap);
			
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "修改成功！");
			
			}else{
			
				resultMap.put("info", "修改失败！");
			
			}
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("info", "修改失败!");
		}
		
		return resultMap;
	}
	
	
	/**
	 * 删除sysresource
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSysresource", method = RequestMethod.POST)
	public Map<String,Object> deleteSysresource(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = sysresourceService.deleteSysresource(paramsMap);
			
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "删除成功！");
			
			}else{
			
				resultMap.put("info", "删除失败！");
			
			}
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "删除失败！");
		
		}
		
		return resultMap;
	}
	
	//auto-code-end
	
	/**
	 * 查询权限拥有的资源信息
	 */
	@ResponseBody
	@RequestMapping(value = "/sysresourceAuthList", method = RequestMethod.POST)
	public Map<String,Object> sysresourceAuthList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		paramsMap.put("type", 1);
		PageObject po = new PageObject();
		
		try{
		
			po = sysresourceService.querySysresourceAuthList(paramsMap);
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}
		
		return resultMap;
	}
	
	/**
	 * 查询权限拥有的资源信息
	 */
	@ResponseBody
	@RequestMapping(value = "/sysresourceNoAuthList", method = RequestMethod.POST)
	public Map<String,Object> sysresourceNoAuthList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			po = sysresourceService.querySysresourceNoAuthList(paramsMap);
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}
		
		return resultMap;
	}
	
	/**
	 * 添加资源权限关系
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addSysresourceAuth", method = RequestMethod.POST)
	public Map<String,Object> sysresourceAuth(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = sysresourceService.addSysresourceAuth(paramsMap);
		
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "添加成功！");
			}else{
			
				resultMap.put("info", "添加失败！");
			}
		
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "添加失败!");
		}
		
		return resultMap;
		
	}
	
	/**
	 * 删除资源权限关系
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSysresourceAuth", method = RequestMethod.POST)
	public Map<String,Object> deleteSysresourceAuth(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = sysresourceService.deleteSysresourceAuth(paramsMap);
			
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "删除成功！");
			
			}else{
			
				resultMap.put("info", "删除失败！");
			
			}
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "删除失败！");
		
		}
		
		return resultMap;
	}
}

