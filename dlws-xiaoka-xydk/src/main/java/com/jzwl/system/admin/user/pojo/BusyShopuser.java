package com.jzwl.system.admin.user.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class BusyShopuser extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "BusyShopuser";
	public static final String ALIAS_ID = "主键";
	public static final String ALIAS_USERNAME = "商家用户名";
	public static final String ALIAS_PASSWORD = "密码";
	public static final String ALIAS_SEX = "性别(1:男、0:女)";
	public static final String ALIAS_MOBILE = "手机号";
	public static final String ALIAS_WX_ID = "商家微信号";
	public static final String ALIAS_OPEN_ID = "商家微信号（加密）";
	public static final String ALIAS_ADDRESS = "商家联系地址";
	public static final String ALIAS_OWNER_ID = "商家注册用户";
	public static final String ALIAS_STATUS = "状态（1，是已激活、0未激活）";
	public static final String ALIAS_IS_DELETE = "是否删除(1:是、0:否)";
	public static final String ALIAS_CREATE_TIME = "创建时间";
	
	//date formats
	public static final String FORMAT_CREATE_TIME = DATE_FORMAT;
	

	//columns START
    /**
     * 主键       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 商家用户名       db_column: username 
     */ 	
	private java.lang.String username;
    /**
     * 密码       db_column: password 
     */ 	
	private java.lang.String password;
    /**
     * 性别(1:男、0:女)       db_column: sex 
     */ 	
	private java.lang.Integer sex;
    /**
     * 手机号       db_column: mobile 
     */ 	
	private java.lang.String mobile;
    /**
     * 商家微信号       db_column: wxId 
     */ 	
	private java.lang.String wxId;
    /**
     * 商家微信号（加密）       db_column: openId 
     */ 	
	private java.lang.String openId;
    /**
     * 商家联系地址       db_column: address 
     */ 	
	private java.lang.String address;
    /**
     * 商家注册用户       db_column: ownerId 
     */ 	
	private java.lang.Long ownerId;
    /**
     * 状态（1，是已激活、0未激活）       db_column: status 
     */ 	
	private java.lang.Integer status;
    /**
     * 是否删除(1:是、0:否)       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
    /**
     * 创建时间       db_column: createTime 
     */ 	
	private java.util.Date createTime;
	//columns END

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	
	
	
	
	
	
	
	
	
	


	public java.lang.String getUsername() {
		return this.username;
	}
	
	public void setUsername(java.lang.String value) {
		this.username = value;
	}
	

	public java.lang.String getPassword() {
		return this.password;
	}
	
	public void setPassword(java.lang.String value) {
		this.password = value;
	}
	

	public java.lang.Integer getSex() {
		return this.sex;
	}
	
	public void setSex(java.lang.Integer value) {
		this.sex = value;
	}
	

	public java.lang.String getMobile() {
		return this.mobile;
	}
	
	public void setMobile(java.lang.String value) {
		this.mobile = value;
	}
	

	public java.lang.String getWxId() {
		return this.wxId;
	}
	
	public void setWxId(java.lang.String value) {
		this.wxId = value;
	}
	

	public java.lang.String getOpenId() {
		return this.openId;
	}
	
	public void setOpenId(java.lang.String value) {
		this.openId = value;
	}
	

	public java.lang.String getAddress() {
		return this.address;
	}
	
	public void setAddress(java.lang.String value) {
		this.address = value;
	}
	

	public java.lang.Long getOwnerId() {
		return this.ownerId;
	}
	
	public void setOwnerId(java.lang.Long value) {
		this.ownerId = value;
	}
	

	public java.lang.Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(java.lang.Integer value) {
		this.status = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

	public String getCreateTimeString() {
		return dateToStr(getCreateTime(), FORMAT_CREATE_TIME);
	}
	public void setCreateTimeString(String value) {
		setCreateTime(strToDate(value, java.util.Date.class,FORMAT_CREATE_TIME));
	}

	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	public void setCreateTime(java.util.Date value) {
		this.createTime = value;
	}
	

}

