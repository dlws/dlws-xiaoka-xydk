
/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.department.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.department.dao.*;
import com.jzwl.system.admin.department.pojo.*;
import com.jzwl.system.admin.department.service.*;


@Service("v2DepartmentService")
public class DepartmentService {
	
	
	@Autowired
	private DepartmentDao departmentDao;
	
	public boolean addDepartment(Map<String, Object> map) {
		
		return departmentDao.addDepartment(map);
		
	}

	public PageObject queryDepartmentList(Map<String, Object> map) {
		
		return departmentDao.queryDepartmentList(map);
	
	}

	public boolean updateDepartment(Map<String, Object> map) {
	
		return departmentDao.updateDepartment(map);
	
	}

	public boolean deleteDepartment(Map<String, Object> map) {
		
		return departmentDao.deleteDepartment(map);
	
	}
	
	
	@SuppressWarnings("unchecked")
	public Department getByDepartmentId(Map<String, Object> map) {
		
		return departmentDao.getByDepartmentId(map);
		
	}
	
	//=======================获取下级的查询方法===============================
	
	public PageObject queryDepartmentListChildren(Map<String, Object> map) {
		
		return departmentDao.queryDepartmentListChildren(map);
	
	}
	
	public List<Department> getDepartmentList() throws Exception{
		
		return departmentDao.getDepartmentList();
	
	}
}

