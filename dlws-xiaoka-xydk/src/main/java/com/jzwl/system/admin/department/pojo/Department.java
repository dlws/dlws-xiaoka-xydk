/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.department.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class Department extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "V2Department";
	public static final String ALIAS_DEPARTMENT_ID = "主键";
	public static final String ALIAS_DEPARTMENTNAME = "部门名称";
	public static final String ALIAS_DEPARTMENTCODE = "部门代码值";
	public static final String ALIAS_PARENTID = "父级的id（如果不存在父级的话则为null）";
	public static final String ALIAS_DESCRIPTION = "描述";
	public static final String ALIAS_ISDELETE = "是否删除";
	
	//date formats
	

	//columns START
    /**
     * 主键       db_column: departmentId 
     */ 	
	private java.lang.Long departmentId;
    /**
     * 部门名称       db_column: departmentname 
     */ 	
	private java.lang.String departmentname;
    /**
     * 部门代码值       db_column: departmentcode 
     */ 	
	private java.lang.String departmentcode;
    /**
     * 父级的id（如果不存在父级的话则为null）       db_column: parentid 
     */ 	
	private java.lang.Long parentid;
	/**
     * 父级的name
     */
	private java.lang.String parentname;
	
	/**
     * 用来标注是否含有下级
     */
	private java.lang.String isHave;
    /**
     * 描述       db_column: description 
     */ 	
	private java.lang.String description;
    /**
     * 是否删除       db_column: isdelete 
     */ 	
	private java.lang.Integer isdelete;
	
	
	//columns END
	
	
	public java.lang.String getIsHave() {
		return isHave;
	}

	public void setIsHave(java.lang.String isHave) {
		this.isHave = isHave;
	}
	
	public java.lang.String getParentname() {
		return parentname;
	}

	public void setParentname(java.lang.String parentname) {
		this.parentname = parentname;
	}
	
	public void setDepartmentId(java.lang.Long value) {
		this.departmentId = value;
	}
	

	public java.lang.Long getDepartmentId() {
		return this.departmentId;
	}
	
	
	
	
	


	public java.lang.String getDepartmentname() {
		return this.departmentname;
	}
	
	public void setDepartmentname(java.lang.String value) {
		this.departmentname = value;
	}
	

	public java.lang.String getDepartmentcode() {
		return this.departmentcode;
	}
	
	public void setDepartmentcode(java.lang.String value) {
		this.departmentcode = value;
	}
	

	public java.lang.Long getParentid() {
		return this.parentid;
	}
	
	public void setParentid(java.lang.Long value) {
		this.parentid = value;
	}
	

	public java.lang.String getDescription() {
		return this.description;
	}
	
	public void setDescription(java.lang.String value) {
		this.description = value;
	}
	

	public java.lang.Integer getIsdelete() {
		return this.isdelete;
	}
	
	public void setIsdelete(java.lang.Integer value) {
		this.isdelete = value;
	}
	

}

