
/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.dictype.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.system.admin.dictype.pojo.DicType;
import com.jzwl.system.admin.dictype.service.DicTypeService;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.common.page.PageObject;

/**
 * V2DicType
 * V2DicType
 * v2DicType
 * <p>Title:V2DicTypeController </p>
 * 	<p>Description:V2DicType </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("v2DicTypeController")
@RequestMapping("/v2DicTypeTwo")
public class DicTypeController  extends BaseController {
	
	@Autowired
	private DicTypeService dicTypeService;
	
	/**
	 * 添加V2DicType
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addV2DicType")
	public String dicType(HttpServletRequest request, HttpServletResponse response) {
		
		createParameterMap(request);
		paramsMap.put("isDelete", 0);
		try{
		
			boolean flag = dicTypeService.addDicType(paramsMap);
		
			if(flag){
			
				return "redirect:/v2DicTypeTwo/v2DicTypeListTwo.html";
			}else{
			
				return"/error";
			}
		
		}catch(Exception e){
			
			return "/error";
		}
	}
	/**
	 * v2DicTypelist
	 */
	@RequestMapping(value = "/v2DicTypeListTwo")
	public ModelAndView v2DicTypeList(HttpServletRequest request, HttpServletResponse response) {
	
		ModelAndView mov = new ModelAndView();
		createParameterMap(request);
		paramsMap.put("isDelete", 0);
		PageObject po = new PageObject();
		
		try{
		
			po = dicTypeService.queryDicTypeList(paramsMap);
			
			mov.addObject("po", po);
			mov.setViewName("/base/system/dictype");
		
		}catch(Exception e){
		
			e.printStackTrace();
			mov.addObject("msg", "数据加载失败:"+e.getMessage());
			mov.setViewName("/error");
		
		}
		
		return mov;
	}
	
	/**
	 * 跳转到添加页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/dictypeAdd")
	public String dictypeAdd(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		model.addAttribute("url", "v2DicTypeTwo/addV2DicType.html");
		return "/base/system/dictype_add";
	}
	
	
	
	/**
	 * getV2DicType
	 */
	@RequestMapping(value = "/getV2DicType")
	public ModelAndView getDicType(HttpServletRequest request, HttpServletResponse response) {
	
		ModelAndView mov = new ModelAndView();
		
		createParameterMap(request);
		
		try{
			
			DicType dicType=dicTypeService.getByDicId(paramsMap);
			
			mov.addObject("dicType", dicType);
			mov.addObject("url", "v2DicTypeTwo/updateV2DicType.html");
			mov.setViewName("/base/system/dictype_add");
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			mov.addObject("msg","获取数据失败："+e.getMessage());
			mov.setViewName("/error");
			
		}
		
		return mov;
	}
	
//	@RequestMapping(value = "/toUpdateRole")
//	public String toUpdateRole(HttpServletRequest request, HttpServletResponse response,Model model) {
//		try{
//			createParameterMap(request);
//			Map<String,Object> objMap = sysroleService.getByRoleId(paramsMap);
//			model.addAttribute("objMap", objMap);
//			model.addAttribute("url", "v2sysrole/updateSysrole.html");
//			return "/v2/jsp/role/roleform";
//		}catch(Exception e){
//			e.printStackTrace();
//			model.addAttribute("msg","获取数据失败");
//			return "/error";
//		}
//	}
	
	
	/**
	 * 修改v2DicType
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateV2DicType")
	public String updateV2DicType(HttpServletRequest request, HttpServletResponse response) {
		
		createParameterMap(request);
		
		try{
		
			boolean flag = dicTypeService.updateDicType(paramsMap);
			
			if(flag){
				
				return "redirect:/v2DicTypeTwo/v2DicTypeListTwo.html";
			}else{
			
				return"/error";
			}
		
		}catch(Exception e){
		
			return"/error";
		}
		
	}
	
	
	/**
	 * 删除v2DicType
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteV2DicType")
	public String deleteDicType(HttpServletRequest request, HttpServletResponse response) {
		
		createParameterMap(request);
		
		try{
		
			boolean flag = dicTypeService.deleteDicType(paramsMap);
			
			if(flag){
				return "redirect:/v2DicTypeTwo/v2DicTypeListTwo.html";
			}else{
				return"/error";
			}
		}catch(Exception e){
			
			return"/error";
		}
	}
	
	//auto-code-end
	
	
}

