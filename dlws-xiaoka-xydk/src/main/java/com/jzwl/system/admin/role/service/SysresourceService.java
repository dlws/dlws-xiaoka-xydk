/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.role.dao.*;
import com.jzwl.system.admin.role.pojo.*;
import com.jzwl.system.admin.role.service.*;


@Service("v2sysresourceService")
public class SysresourceService {
	
	
	@Autowired
	private SysresourceDao sysresourceDao;
	
	public boolean addSysresource(Map<String, Object> map) {
		
		return sysresourceDao.addSysresource(map);
		
	}

	public PageObject querySysresourceList(Map<String, Object> map) {
		
		return sysresourceDao.querySysresourceList(map);
	
	}

	public boolean updateSysresource(Map<String, Object> map) {
	
		return sysresourceDao.updateSysresource(map);
	
	}

	public boolean deleteSysresource(Map<String, Object> map) {
		
		return sysresourceDao.deleteSysresource(map);
	
	}
	
	
	@SuppressWarnings("unchecked")
	public Sysresource getByResourceId(Map<String, Object> map) {
		
		return sysresourceDao.getByResourceId(map);
		
	}
	
	public PageObject querySysresourceAuthList(Map<String, Object> map) {
		
		return sysresourceDao.querySysresourceAuthList(map);
	
	}
	
	public PageObject querySysresourceNoAuthList(Map<String, Object> map) {
		
		return sysresourceDao.querySysresourceNoAuthList(map);
	
	}
	
	public boolean addSysresourceAuth(Map<String, Object> map) {
		
		return sysresourceDao.addSysresourceAuth(map);
		
	}
	
	public boolean deleteSysresourceAuth(Map<String, Object> map) {
		
		return sysresourceDao.deleteSysresourceAuth(map);
	
	}
}
