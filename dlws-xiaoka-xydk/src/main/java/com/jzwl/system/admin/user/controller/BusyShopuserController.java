package com.jzwl.system.admin.user.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.system.admin.user.pojo.BusyShopuser;
import com.jzwl.system.admin.user.service.BusyShopuserService;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.common.md5.AuthUtils;
import com.jzwl.common.page.PageObject;

/**
 * BusyShopuser
 * BusyShopuser
 * busyShopuser
 * <p>Title:BusyShopuserController </p>
 * 	<p>Description:BusyShopuser </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller
@RequestMapping("/busyShopuser")
public class BusyShopuserController  extends BaseController {
	
	@Autowired
	private BusyShopuserService busyShopuserService;
	
	/**
	 * 添加BusyShopuser
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addBusyShopuser")
	public String busyShopuser(HttpServletRequest request, HttpServletResponse response) {
		
		
		String username = request.getParameter("username");
		
		String confirmVerifycode = (String)request.getSession().getAttribute("Verifycode");
		
		String verifycode = request.getParameter("Verifycode");
		
//		PrintWriter   out  = null;
//		if(verifycode == null || verifycode.length() <= 3 ){
//			
//			try{
//				out   =   response.getWriter();
//			}catch(IOException e){
//				e.printStackTrace();
//			}
//			out.close();
//		}
//		if(!verifycode.equals(confirmVerifycode)){
//			try{
//				out   =   response.getWriter();
//			}catch(IOException e){
//				e.printStackTrace();
//			}
//			out.print("<script   language= 'javascript '> alert('验证码错误'); </script>");
//			out.close();
//		}
		
//		if(!StringUtil.isEmpty(username)){
//			String Verifycode = request.getParameter("Verifycode");
//			String confirmVerifycode = (String)session.getAttribute("Verifycode");
//
//			if(StringUtil.isEmpty(confirmVerifycode)){
//				out.print(StringUtil.alert("验证码已过期请重新输入！", "back"));
//				return;
//			}
//
//			if(!StringUtil.isSame(Verifycode, confirmVerifycode)){
//				out.print(StringUtil.alert("验证码输入错误！", "back"));
//				return;
//			}
//
//			SaleChannelAction action = new SaleChannelAction();
//			String message = action.execute(request, response);
//			if(!StringUtil.isEmpty(message)){
//				out.print(StringUtil.alert(message, "back"));
//				return;
//			}else{
//				response.sendRedirect("index.jsp");
//				return;
//			}
//		}
		
		Date date = new Date();
		
		createParameterMap(request);
		
		paramsMap.put("username", request.getParameter("username"));
		paramsMap.put("password", AuthUtils.getPassword(request.getParameter("password")));
		paramsMap.put("mobile", request.getParameter("phone"));
		paramsMap.put("wxId", request.getParameter("weixin"));
		paramsMap.put("address", request.getParameter("address"));
		paramsMap.put("status", 0);
		paramsMap.put("isDelete", 0);
		paramsMap.put("createTime", date);
		
		try{
		
			boolean flag = busyShopuserService.addBusyShopuser(paramsMap);
		
			if(flag){
	
				request.setAttribute("msg", "登录成功");
	    		return "weixinjihuo";
			}else{
			
				request.setAttribute("msg", "登录失败");
	  	    	return "error";
			}
		
		}catch(Exception e){
			
			e.printStackTrace();
			
			request.setAttribute("msg", "登录失败");
  	    	return "error";
		}
		
		
	}
	
	/**
	 * busyShopuserlist
	 */
	@ResponseBody
	@RequestMapping(value = "/busyShopuserList", method = RequestMethod.POST)
	public Map<String,Object> busyShopuserList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			po = busyShopuserService.queryBusyShopuserList(paramsMap);
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}
		
		return resultMap;
	}
	
	
	
	/**
	 * getBusyShopuser
	 */
	@ResponseBody
	@RequestMapping(value = "/getBusyShopuser", method = RequestMethod.POST)
	public Map<String,Object> getBusyShopuser(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
			
			BusyShopuser busyShopuser=busyShopuserService.getById(paramsMap);
			
			resultMap.put("success", true);
			resultMap.put("obj", busyShopuser);
			
		
		}catch(Exception e){
		
			resultMap.put("info", "获取失败");
			
			e.printStackTrace();
			
		}
		
		return resultMap;
	}
	
	
	/**
	 * 修改busyShopuser
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateBusyShopuser", method = RequestMethod.POST)
	public Map<String,Object> updateBusyShopuser(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = busyShopuserService.updateBusyShopuser(paramsMap);
			
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "修改成功！");
			
			}else{
			
				resultMap.put("info", "修改失败！");
			
			}
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("info", "修改失败!");
		}
		
		return resultMap;
	}
	
	
	/**
	 * 删除busyShopuser
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteBusyShopuser", method = RequestMethod.POST)
	public Map<String,Object> deleteBusyShopuser(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = busyShopuserService.deleteBusyShopuser(paramsMap);
			
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "删除成功！");
			
			}else{
			
				resultMap.put("info", "删除失败！");
			
			}
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "删除失败！");
		
		}
		
		
		
		return resultMap;
	}
	
	//auto-code-end
	
	/**
	 * getBusyShopuser
	 */
	@RequestMapping(value = "/backPassWord")
	public  ModelAndView backPassWord(HttpServletRequest request, HttpServletResponse response,String xiaokaNumber) {
		
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		//查询教验用户登录的名称
	    List<Map<String,Object>> dbUserlist = busyShopuserService.getBusyShopUser(xiaokaNumber);
		
		
		if(dbUserlist.size()==0){
			return null;
		}else{
			
			//这是登陆成功的页面
			
			//将获取的值放到session里面
			request.getSession().setAttribute("xiaokaNumber",xiaokaNumber);
			
			ModelAndView mv = new ModelAndView();
//			request.setAttribute("msg", "登录成功");
//			mv.addObject("msg", "登录成功");
			mv.setViewName("/retrievePasswordTwo");
  	    	return mv;
		}
		
	}
	
	@RequestMapping(value = "/backPassWordTwo")
	public  ModelAndView backPassWordTwo(HttpServletRequest request, HttpServletResponse response,String xiaokaNumber) {
		
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		String str = (String) request.getSession().getAttribute("xiaokaNumber");
		
		//在这里获取验证码
		
		resultMap.put("username", str);
		resultMap.put("password", AuthUtils.getPassword(request.getParameter("password")));
		
		//查询教验用户登录的名称
		boolean flag = busyShopuserService.updatePassWord(resultMap);
		
		if(flag == true){
			//这是登陆成功的页面
			
			
			ModelAndView mv = new ModelAndView();
			request.setAttribute("msg", "设置成功");
			mv.addObject("msg", "设置成功");
			mv.setViewName("/success");
  	    	return mv;
		}else{
			
			ModelAndView mv = new ModelAndView();
			request.setAttribute("msg", "登陆失败！请重新登陆！");
			mv.addObject("msg", "登陆失败！请重新登陆！");
			mv.setViewName("/error");
  	    	return mv;
			
		}
	    
//	    String username = request.getParameter("username");
//		
//		String confirmVerifycode = (String)request.getSession().getAttribute("Verifycode");
//		
//		String verifycode = request.getParameter("Verifycode");
		
		
//		if(dbUserlist.size()==0){
//			return null;
//		}else{
//			
//			//这是登陆成功的页面
//			
//			//将获取的值放到session里面
//			request.getSession().setAttribute("xiaokaNumber",xiaokaNumber);
//			
//			ModelAndView mv = new ModelAndView();
//			request.setAttribute("msg", "登陆失败！请重新登陆！");
//			mv.addObject("msg", "登陆失败！请重新登陆！");
//			mv.setViewName("/retrievePasswordTwo");
//  	    	return mv;
//		}
//		
	}

}

