package com.jzwl.system.admin.department.pojo;

import java.io.Serializable;

public class TreeNode implements Serializable{
	
	private static final long serialVersionUID = -766759489880382042L;
	
	private Long id;
	private Long pId;
	private String name;
	private boolean open;  //是否默认打开
	private boolean nocheck; //是否可以选
	private boolean checked; //是否默认选上
	
	public TreeNode() {
		super();
	}

	public TreeNode(Long id, Long pId, String name, boolean open, boolean nocheck, boolean checked) {
		super();
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.open = open;
		this.nocheck = nocheck;
		this.checked = checked;
	}

	

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isNocheck() {
		return nocheck;
	}

	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}
	
}
