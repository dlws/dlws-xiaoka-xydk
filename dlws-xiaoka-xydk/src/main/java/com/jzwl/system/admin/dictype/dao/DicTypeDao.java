/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.dictype.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.admin.dictype.pojo.DicType;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.common.page.PageObject;



@Repository("v2DicTypeDao")
public class DicTypeDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	
	public boolean addDicType(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("dicId",  Sequence.nextId());
		
		String sql = "insert into v2_dic_type " 
				 + " (dic_id,dic_name,dic_code,isDelete) " 
				 + " values "
				 + " (:dicId,:dicName,:dicCode,:isDelete)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	public String getColumns() {
		return ""
				+" dic_id as dicId,"
				+" dic_name as dicName,"
				+" dic_code as dicCode,"
				+" isDelete as isDelete"
				;
	}
	

	public PageObject queryDicTypeList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select " + getColumns() + " from v2_dic_type t where 1=1 ";
		
		  	if(null !=map.get("dicName") && StringUtils.isNotEmpty(map.get("dicName").toString())){
			  		sql=sql+ " and t.dic_name  = " + map.get("dicName") +"";
		  	}
		  	if(null !=map.get("dicCode") && StringUtils.isNotEmpty(map.get("dicCode").toString())){
			  		sql=sql+ " and t.dic_code  = " + map.get("dicCode") +"";
		  	}
		  	if(null !=map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())){
			  		sql=sql+ " and t.isDelete  = " + map.get("isDelete") +"";
		  	}
					
					
		sql=sql+ " order by dic_id ";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}

	public boolean updateDicType(Map<String, Object> map) {
		
		String sql ="update v2_dic_type set "
				+ " dic_id=:dicId,dic_name=:dicName,dic_code=:dicCode "
				+ " where dic_id=:dicId";
		
		return  baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteDicType(Map<String, Object> map) {
		
//		String sql = "delete from v2_dic_type where dic_id in ("+ map.get("dicId")+ " ) ";
//
//		return baseDAO.executeNamedCommand(sql, map);
		
		String sql ="update v2_dic_type set "
				+ " isDelete = 1 "
				+ " where dic_id=:dicId";
		
		return  baseDAO.executeNamedCommand(sql, map);
	}
	
	
	@SuppressWarnings("unchecked")
	public DicType getByDicId(Map<String, Object> map) {
		
		String sql = "select " + getColumns() + " from v2_dic_type where dic_id = "+ map.get("dicId") + "";
		
		DicType dicType=new DicType();
		
		Map m=baseDAO.queryForMap(sql);
		
		try {
			org.apache.commons.beanutils.BeanUtils.copyProperties(dicType, m);
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return null;
		}
		
		return dicType;
		
	
	}
	

}




