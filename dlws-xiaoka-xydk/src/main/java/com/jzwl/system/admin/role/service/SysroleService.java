/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.role.dao.*;
import com.jzwl.system.admin.role.pojo.*;


@Service("v2sysroleService")
public class SysroleService {
	
	
	@Autowired
	private SysroleDao sysroleDao;
	
	public boolean addSysrole(Map<String, Object> map) {
		
		return sysroleDao.addSysrole(map);
		
	}

	public PageObject querySysroleList(Map<String, Object> map) {
		
		return sysroleDao.querySysroleList(map);
	
	}

	public boolean updateSysrole(Map<String, Object> map) {
	
		return sysroleDao.updateSysrole(map);
	
	}

	public boolean deleteSysrole(Map<String, Object> map) {
		
		return sysroleDao.deleteSysrole(map);
	
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getByRoleId(Map<String, Object> map) {
		
		return sysroleDao.getByRoleId(map);
		
	}
	
	public PageObject querySysroleUserList(Map<String, Object> map) {
		
		return sysroleDao.querySysroleUserList(map);
	
	}
	
	public PageObject querySysroleNoUserList(Map<String, Object> map) {
		
		return sysroleDao.querySysroleNoUserList(map);
	
	}
	
	public boolean deleteSysroleUser(Map<String, Object> map) {
		
		return sysroleDao.deleteSysroleUser(map);
	
	}
	
	public boolean addSysroleUser(Map<String, Object> map) {
		
		return sysroleDao.addSysroleUser(map);
		
	}
	
	public PageObject querySysroleAuthorityList(Map<String, Object> map) {
		
		return sysroleDao.querySysroleAuthorityList(map);
	
	}
	
	public List querySysroleNoAuthorityList(Map<String, Object> map) {
		
		return sysroleDao.querySysroleNoAuthorityList(map);
	
	}
	
	public boolean deleteSysroleAuthotity(Map<String, Object> map) {
		
		return sysroleDao.deleteSysroleAuthotity(map);
	
	}
	
	public boolean addSysroleAuthority(Map<String, Object> map) {
		
		return sysroleDao.addSysroleAuthority(map);
		
	}
	/**
	 * 获取用户对应的角色
	 * @param object
	 * @return
	 */
	public List<Map<String, Object>> getRoleListByUserId(Map<String, Object> map) {
		
		return sysroleDao.getRoleListByUserId(map);
	}
}
