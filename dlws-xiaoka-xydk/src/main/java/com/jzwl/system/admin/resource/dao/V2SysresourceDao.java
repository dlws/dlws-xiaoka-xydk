package com.jzwl.system.admin.resource.dao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.IdFactory;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("v2SysresourceDao")
public class V2SysresourceDao {

	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	@Autowired
	private IdFactory idFactory;

	/**
	 * 条件查询资源列表
	 * 
	 * @param map
	 * @return
	 */
	public PageObject queryResourceList(Map<String, Object> map) {
		String sql = "select " + getColumns()
				+ " from v2_plat_resource t where 1=1 ";

		if (null != map.get("componentid")
				&& StringUtils.isNotEmpty(map.get("componentid").toString())) {
			sql = sql + " and t.componentid  = " + map.get("componentid") + "";
		}
		if (null != map.get("componentname")
				&& StringUtils.isNotEmpty(map.get("componentname").toString())) {
			sql = sql + " and t.componentname like '%"
					+ map.get("componentname") + "%' ";
		}
		if (null != map.get("parentid")
				&& StringUtils.isNotEmpty(map.get("parentid").toString())) {
			sql = sql + " and t.parentid  = " + map.get("parentid") + "";
		}
		if (null != map.get("resourceUrl")
				&& StringUtils.isNotEmpty(map.get("resourceUrl").toString())) {
			sql = sql + " and t.resourceUrl  = " + map.get("resourceUrl") + "";
		}
		if (null != map.get("type")
				&& StringUtils.isNotEmpty(map.get("type").toString())) {
			sql = sql + " and t.type  = " + map.get("type") + "";
		}
		if (null != map.get("createdTime")
				&& StringUtils.isNotEmpty(map.get("createdTime").toString())) {
			sql = sql + " and t.createdTime >= " + map.get("createdTimeBegin")
					+ "";
			sql = sql + " and t.createdTime <= " + map.get("createdTimeEnd")
					+ "";
		}
		if (null != map.get("status")
				&& StringUtils.isNotEmpty(map.get("status").toString())) {
			sql = sql + " and t.status  = " + map.get("status") + "";
		}

		sql = sql + " order by status desc ";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	/**
	 * 新建资源
	 * 
	 * @param map
	 * @return
	 */
	public boolean addSysresource(Map<String, Object> map) {
		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		map.put("resourceId", idFactory.nextId());
		String sql = "insert into v2_plat_resource "
				+ " (resourceId,componentid,componentname,parentid,resourceUrl,type,createdTime,status) "
				+ " values "
				+ " (:resourceId,:componentid,:componentname,:parentid,:resourceUrl,:type,:createdTime,:status)";
		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 通过参数获取资源对象
	 * 
	 * @param paramsMap
	 * @return
	 */
	public Map<String, Object> getResource(Map<String, Object> paramsMap) {
		String sql = "select resourceId,componentid,componentname,parentid,resourceUrl,type,createdTime,status from "
				+ "v2_plat_resource t where t.resourceId = '"
				+ paramsMap.get("resourceId") + "'";
		Map m = baseDAO.queryForMap(sql);
		return m;
	}

	/**
	 * 修改资源
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean updateSysresource(Map<String, Object> map) {
		String sql = "update v2_plat_resource set "
				+ " resourceId=:resourceId,componentid=:componentid,componentname=:componentname,parentid=:parentid,resourceUrl=:resourceUrl,type=:type,createdTime=:createdTime,status=:status "
				+ " where resourceId=:resourceId";
		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 删除资源
	 * 
	 * @param paramsMap
	 * @return
	 */
	public boolean deleteSysresource(Map<String, Object> paramsMap) {
		String sql = "update v2_plat_resource set status = 0"
				+ " where resourceId=:resourceId";
		return baseDAO.executeNamedCommand(sql, paramsMap);
	}

	public String getColumns() {
		return "" + " t.resourceId as resourceId,"
				+ " t.componentid as componentid,"
				+ " t.componentname as componentname,"
				+ " t.parentid as parentid," + " t.resourceUrl as resourceUrl,"
				+ " t.type as type," + " t.createdTime as createdTime,"
				+ " t.status as status";
	}

}
