package com.jzwl.system.admin.authority.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.system.admin.role.pojo.Sysauthority;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.common.id.IdFactory;
import com.jzwl.common.page.PageObject;

@Repository("V2SysauthorityDao")
public class V2SysauthorityDao {

	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	@Autowired
	private IdFactory idFactory;

	/**
	 * 条件查询权限列表
	 * 
	 * @param paramsMap
	 * @return
	 */
	public PageObject queryAuthorityList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接
		// [column] 为PageRequest的属性
		String sql = "select " + getColumns()
				+ " from v2_plat_authority t where 1=1 ";

		if (null != map.get("authorityName")
				&& StringUtils.isNotEmpty(map.get("authorityName").toString())) {
			sql = sql + " and t.authorityName  like '%"
					+ map.get("authorityName") + "%' ";
		}
		if (null != map.get("authorityCode")
				&& StringUtils.isNotEmpty(map.get("authorityCode").toString())) {
			sql = sql + " and t.authorityCode  like '%"
					+ map.get("authorityCode") + "%' ";
		}
		sql = sql + " order by authorityId ";
		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);
		return po;
	}

	/**
	 * 添加权限
	 * 
	 * @param map
	 * @return
	 */
	public boolean addSysauthority(Map<String, Object> map) {
		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		map.put("authorityId", idFactory.nextId());
		String sql = "insert into v2_plat_authority "
				+ " (authorityId,authorityName,authorityCode) " + " values "
				+ " (:authorityId,:authorityName,:authorityCode)";
		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 通过ID查询权限
	 * 
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Sysauthority getByAuthorityId(Map<String, Object> map) {
		String sql = "select " + getColumns()
				+ " from v2_plat_authority where authorityId = "
				+ map.get("authorityId") + "";
		Sysauthority sysauthority = new Sysauthority();
		Map m = baseDAO.queryForMap(sql);
		try {
			org.apache.commons.beanutils.BeanUtils.copyProperties(sysauthority,
					m);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return sysauthority;
	}

	/**
	 * 修改权限
	 * 
	 * @param map
	 * @return
	 */
	public boolean updateSysauthority(Map<String, Object> map) {
		String sql = "update v2_plat_authority set "
				+ " authorityName=:authorityName,authorityCode=:authorityCode "
				+ " where authorityId=:authorityId";
		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 删除权限
	 * 
	 * @param map
	 * @return
	 */
	public boolean deleteAuthority(Map<String, Object> map) {
		String sql = "delete from v2_plat_authority where authorityId in ("
				+ map.get("authorityId") + " ) ";
		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 获取权限
	 * 
	 * @param paramsMap
	 * @return
	 */
	public Map<String, Object> getAuthority(Map<String, Object> paramsMap) {
		String sql = "select authorityId,authorityName,authorityCode"
				+ " from v2_plat_authority t where t.authorityId = '"
				+ paramsMap.get("authorityId") + "'";
		Map m = baseDAO.queryForMap(sql);
		return m;
	}

	/**
	 * 获取权限已拥有资源列表
	 * 
	 * @param paramsMap
	 * @return
	 */
	public List querySysresourceAuthList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接
		// [column] 为PageRequest的属性

		String sql = "select " + getResourceColumns()
				+ " from v2_plat_resource t ";

		// 关联权限资源表查询
		sql = sql
				+ " left join v2_plat_authresource ar on t.resourceId = ar.resourceId where 1=1 ";

		if (null != map.get("componentid")
				&& StringUtils.isNotEmpty(map.get("componentid").toString())) {
			sql = sql + " and t.componentid  = " + map.get("componentid") + "";
		}
		if (null != map.get("componentname")
				&& StringUtils.isNotEmpty(map.get("componentname").toString())) {
			sql = sql + " and t.componentname  = " + map.get("componentname")
					+ "";
		}
		if (null != map.get("parentid")
				&& StringUtils.isNotEmpty(map.get("parentid").toString())) {
			sql = sql + " and t.parentid  = " + map.get("parentid") + "";
		}
		if (null != map.get("resourceUrl")
				&& StringUtils.isNotEmpty(map.get("resourceUrl").toString())) {
			sql = sql + " and t.resourceUrl  = " + map.get("resourceUrl") + "";
		}
		if (null != map.get("type")
				&& StringUtils.isNotEmpty(map.get("type").toString())) {
			sql = sql + " and t.type  != " + map.get("type") + "";
		}
		if (null != map.get("createdTime")
				&& StringUtils.isNotEmpty(map.get("createdTime").toString())) {
			sql = sql + " and t.createdTime >= " + map.get("createdTimeBegin")
					+ "";
			sql = sql + " and t.createdTime <= " + map.get("createdTimeEnd")
					+ "";
		}
		if (null != map.get("status")
				&& StringUtils.isNotEmpty(map.get("status").toString())) {
			sql = sql + " and t.status  = " + map.get("status") + "";
		}
		sql = sql + " and ar.authorityId=" + map.get("authorityId") + "";
		sql = sql + " order by t.resourceId ";
		
		//PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);
		return baseDAO.queryForList(sql);
	}

	/**
	 * 获取权限未拥有资源列表
	 * 
	 * @param paramsMap
	 * @return
	 */
	public List querySysresourceNoAuthList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接
		// [column] 为PageRequest的属性

		String sql = "select * from v2_plat_resource where resourceId not in";
		sql = sql
				+ "(select t.resourceId as resourceId from v2_plat_resource t ";
		// 关联权限资源表查询
		sql = sql
				+ " left join v2_plat_authresource ar on t.resourceId = ar.resourceId where 1=1 ";

		// sql = sql + " and t.type  != 1";

		sql = sql + " and ar.authorityId=" + map.get("authorityId") + "";
		sql = sql + " order by t.resourceId) ";
		sql = sql + " order by resourceId";
		
		//PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);
		List  queryForList = baseDAO.queryForList(sql);
		return queryForList;
	}

	public String getColumns() {
		return "" + " authorityId as authorityId,"
				+ " authorityName as authorityName,"
				+ " authorityCode as authorityCode";
	}

	public String getResourceColumns() {
		return "" + " t.resourceId as resourceId,"
				+ " t.componentid as componentid,"
				+ " t.componentname as componentname,"
				+ " t.parentid as parentid," + " t.resourceUrl as resourceUrl,"
				+ " t.type as type," + " t.createdTime as createdTime,"
				+ " t.status as status";
	}

	/**
	 * 添加权限资源关系
	 * 
	 * @param map
	 * @return
	 */
	public boolean addSysresourceAuth(Map<String, Object> map) {
		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		String sql = "insert into v2_plat_authresource "
				+ " (resourceId,authorityId) " + " values "
				+ " (:resourceId,:authorityId)";
		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 删除权限资源关系
	 * 
	 * @param map
	 * @return
	 */
	public boolean deleteSysresourceAuth(Map<String, Object> map) {
		String sql = "delete from v2_plat_authresource where resourceId=:resourceId and authorityId=:authorityId ";
		return baseDAO.executeNamedCommand(sql, map);
	}
}
