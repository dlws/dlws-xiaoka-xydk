/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.dicdate.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.system.admin.dicdate.pojo.*;
import com.jzwl.system.admin.dicdate.service.*;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.common.page.PageObject;

/**
 * V2DicData
 * V2DicData
 * v2DicData
 * <p>Title:V2DicDataController </p>
 * 	<p>Description:V2DicData </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("v2DicDataController")
@RequestMapping("/v2DicDataTwo")
public class DicDataController  extends BaseController {
	
	@Autowired
	private DicDataService dicDataService;
	
	
	/**
	 * 跳转到添加页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/dicdataAdd")
	public String dicdataAdd(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		createParameterMap(request);
		
		model.addAttribute("url", "v2DicDataTwo/addV2DicData.html");
		model.addAttribute("dicId",paramsMap.get("dicId") );
		model.addAttribute("typename",paramsMap.get("typename"));
		return "/base/system/dicdata_form";
	}
	
	/**
	 * 添加V2DicData
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addV2DicData")
	public String v2DicData(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
			
			paramsMap.put("isDelete", 0);
			paramsMap.put("parentId", 0);
			boolean flag = dicDataService.addDicData(paramsMap);
			
			if(flag){
			
				return "redirect:/v2DicDataTwo/v2DicDataListTwo.html?dicId="+paramsMap.get("dicId")+"&typename="+paramsMap.get("typename");
			}else{
			
				return "redirect:/error";
			}
		
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "添加失败!");
		}
		
		return "/error";
		
	}
	
	/**
	 * v2DicDatalist
	 */
	@RequestMapping(value = "/v2DicDataListTwo")
	public ModelAndView dicDataList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		ModelAndView modelAndView = new ModelAndView();
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			po = dicDataService.queryDicDataList(paramsMap);
			
			modelAndView.addObject("po", po);
			modelAndView.addObject("typename", paramsMap.get("typename"));
			modelAndView.addObject("dicId", paramsMap.get("dicId"));
			modelAndView.setViewName("/base/system/dicdata");
		
		}catch(Exception e){
		
			e.printStackTrace();
			modelAndView.addObject("msg", "数据加载失败:"+e.getMessage());
			modelAndView.setViewName("/error");
		
		}
		
		return modelAndView;
	}
	
	
	
	/**
	 * getV2DicData
	 */
	@RequestMapping(value = "/getV2DicData")
	public ModelAndView getDicData(HttpServletRequest request, HttpServletResponse response) {
	
		ModelAndView mov = new ModelAndView();
		
		createParameterMap(request);
		
		try{
			
			DicData dicData=dicDataService.getById(paramsMap);
			
			mov.addObject("dicData", dicData);
			mov.addObject("typename", paramsMap.get("typename"));
			mov.addObject("url", "v2DicDataTwo/updateV2DicData.html?id="+paramsMap.get("id")+"&typename="+paramsMap.get("typename"));
			mov.setViewName("/base/system/dicdata_form");
			
		}catch(Exception e){
		
			e.printStackTrace();

			mov.addObject("msg","获取数据失败："+e.getMessage());
			mov.setViewName("/error");
			
		}
		
		return mov;
	}
	
	
	/**
	 * 修改v2DicData
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateV2DicData")
	public String updateDicData(HttpServletRequest request, HttpServletResponse response) {
		
		createParameterMap(request);
		
		try{
		
			boolean flag = dicDataService.updateDicData(paramsMap);
			
			if(flag){
			
				return "redirect:/v2DicDataTwo/v2DicDataListTwo.html?dicId="+paramsMap.get("dicId")+"&typename="+paramsMap.get("typename");
			
			}else{
			
				return"/error";
			
			}
		}catch(Exception e){
		
			return"/error";
		}
		
	}
	
	
	/**
	 * 删除v2DicData
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteV2DicData")
	public String deleteDicData(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = dicDataService.deleteDicData(paramsMap);
			
			if(flag){
			
				return "redirect:/v2DicDataTwo/v2DicDataListTwo.html?dicId="+paramsMap.get("dicId")+"&typename="+paramsMap.get("typename");
			
			}else{
			
				return"/error";
			
			}
		}catch(Exception e){
			
			e.printStackTrace();
			
			return"/error";
		
		}
	}
	
	//auto-code-end
	
	
}

