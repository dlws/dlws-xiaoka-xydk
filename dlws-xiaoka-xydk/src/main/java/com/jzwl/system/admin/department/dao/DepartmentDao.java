/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.department.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.admin.department.pojo.*;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.common.page.PageObject;

@Repository("v2DepartmentDao")
public class DepartmentDao {

	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	public boolean addDepartment(Map<String, Object> map) {

		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		map.put("departmentId", Sequence.nextId());

		String sql = "insert into v2_department "
				+ " (departmentId,departmentname,departmentcode,parentid,description,isdelete) "
				+ " values "
				+ " (:departmentId,:departmentname,:departmentcode,:parentid,:description,:isdelete)";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public String getColumns() {
		return "" + " departmentId as departmentId,"
				+ " departmentname as departmentname,"
				+ " departmentcode as departmentcode,"
				+ " parentid as parentid," + " description as description,"
				+ " isdelete as isdelete";
	}

	public PageObject queryDepartmentList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接
		// [column] 为PageRequest的属性

		String sql = "select " + getColumns()
				+ " from v2_department t where 1=1 ";

		if (null != map.get("departmentname")
				&& StringUtils.isNotEmpty(map.get("departmentname").toString())) {
			sql = sql + " and t.departmentname  = " + map.get("departmentname")
					+ "";
		}
		if (null != map.get("departmentcode")
				&& StringUtils.isNotEmpty(map.get("departmentcode").toString())) {
			sql = sql + " and t.departmentcode  = " + map.get("departmentcode")
					+ "";
		}
		if (null != map.get("parentid")
				&& StringUtils.isNotEmpty(map.get("parentid").toString())) {
			sql = sql + " and t.parentid  = " + map.get("parentid") + "";
		}
		if (null != map.get("description")
				&& StringUtils.isNotEmpty(map.get("description").toString())) {
			sql = sql + " and t.description  = " + map.get("description") + "";
		}
		if (null != map.get("isdelete")
				&& StringUtils.isNotEmpty(map.get("isdelete").toString())) {
			sql = sql + " and t.isdelete  = " + map.get("isdelete") + "";
		}

		sql = sql + " order by departmentId ";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	public boolean updateDepartment(Map<String, Object> map) {

		String sql = "update v2_department set "
				+ " departmentname=:departmentname,departmentcode=:departmentcode,description=:description "
				+ " where departmentId=:departmentId";

		return baseDAO.executeNamedCommand(sql, map);
	}
	
	public boolean updateDepartmentname(Map<String, Object> map) {

		String sql = "update v2_department set "
				+ " departmentname=:departmentname "
				+ " where departmentId=:departmentId";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteDepartment(Map<String, Object> map) {

		String sql = "update v2_department set isdelete=1 where departmentId=:departmentId";

		return baseDAO.executeNamedCommand(sql, map);
	}

	@SuppressWarnings("unchecked")
	public  Department getByDepartmentId(Map<String, Object> map) {
		
		String sql = "select " + getColumns() + " from v2_department where departmentId = "+ map.get("departmentId") + "";
				
		Department  department=new  Department();
		
		Map m=baseDAO.queryForMap(sql);
		
		try {
			org.apache.commons.beanutils.BeanUtils.copyProperties(department, m);
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return null;
		}
		
		return department;
		
	
	}

	// ========================获取下级的查询方式===========================
	
	public String getColumnsChildren() {
		return "" + " t.departmentId as departmentId,"
				+ " t.departmentname as departmentname,"
				+ " t.departmentcode as departmentcode,"
				+ " t.parentid as parentid," 
				+ " description as description,"
//				+ "(select a.departmentId from v2_department a where a.parentid = t.departmentId) as isHave,"
				+ " t.isdelete as isdelete";
	}

	public PageObject queryDepartmentListChildren(Map<String, Object> map) {

		String sql = "select " + getColumnsChildren()
				+ " from v2_department t where t.isdelete = 0 ";

		if (null != map.get("parentid") && StringUtils.isNotEmpty(map.get("parentid").toString())) {
			sql = sql + " and t.parentid  = " + map.get("parentid") + "";
		}

		sql = sql + " order by departmentId ";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}
	
	/**
	 * 用户管理获取部门的信息
	 * @return
	 */
	public List<Department> getDepartmentList() throws Exception{
		
		String sql = "select "+getColumns()+" from v2_department t where t.isdelete = 0";
		List<Department> list = baseDAO.getJdbcTemplate().query(sql,ParameterizedBeanPropertyRowMapper.newInstance(Department.class));
		return list;
	}

}
