package com.jzwl.system.admin.user.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.admin.user.pojo.BusyShopuser;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.common.page.PageObject;



@Repository("busyShopuserDao")
public class BusyShopuserDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	
	public boolean addBusyShopuser(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id",  Sequence.nextId());
		
		String sql = "insert into busy_shopuser " 
				 + " (id,username,password,mobile,wxId,address,status,isDelete,createTime) " 
				 + " values "
				 + " (:id,:username,:password,:mobile,:wxId,:address,:status,:isDelete,:createTime)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	public String getColumns() {
		return ""
				+" id as id,"
				+" username as username,"
				+" password as password,"
				+" sex as sex,"
				+" mobile as mobile,"
				+" wxId as wxId,"
				+" openId as openId,"
				+" address as address,"
				+" ownerId as ownerId,"
				+" status as status,"
				+" isDelete as isDelete,"
				+" createTime as createTime"
				;
	}
	

	public PageObject queryBusyShopuserList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select " + getColumns() + " from busy_shopuser t where 1=1 ";
		
		  	if(null !=map.get("username") && StringUtils.isNotEmpty(map.get("username").toString())){
			  		sql=sql+ " and t.username  = " + map.get("username") +"";
		  	}
		  	if(null !=map.get("password") && StringUtils.isNotEmpty(map.get("password").toString())){
			  		sql=sql+ " and t.password  = " + map.get("password") +"";
		  	}
		  	if(null !=map.get("sex") && StringUtils.isNotEmpty(map.get("sex").toString())){
			  		sql=sql+ " and t.sex  = " + map.get("sex") +"";
		  	}
		  	if(null !=map.get("mobile") && StringUtils.isNotEmpty(map.get("mobile").toString())){
			  		sql=sql+ " and t.mobile  = " + map.get("mobile") +"";
		  	}
		  	if(null !=map.get("wxId") && StringUtils.isNotEmpty(map.get("wxId").toString())){
			  		sql=sql+ " and t.wxId  = " + map.get("wxId") +"";
		  	}
		  	if(null !=map.get("openId") && StringUtils.isNotEmpty(map.get("openId").toString())){
			  		sql=sql+ " and t.openId  = " + map.get("openId") +"";
		  	}
		  	if(null !=map.get("address") && StringUtils.isNotEmpty(map.get("address").toString())){
			  		sql=sql+ " and t.address  = " + map.get("address") +"";
		  	}
		  	if(null !=map.get("ownerId") && StringUtils.isNotEmpty(map.get("ownerId").toString())){
			  		sql=sql+ " and t.ownerId  = " + map.get("ownerId") +"";
		  	}
		  	if(null !=map.get("status") && StringUtils.isNotEmpty(map.get("status").toString())){
			  		sql=sql+ " and t.status  = " + map.get("status") +"";
		  	}
		  	if(null !=map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())){
			  		sql=sql+ " and t.isDelete  = " + map.get("isDelete") +"";
		  	}
		  	if(null !=map.get("createTime") && StringUtils.isNotEmpty(map.get("createTime").toString())){
			  		sql=sql+ " and t.createTime >= " + map.get("createTimeBegin") +"";
			  		sql=sql+ " and t.createTime <= " + map.get("createTimeEnd"  ) +"";
		  	}
					
					
		sql=sql+ " order by id ";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}

	public boolean updateBusyShopuser(Map<String, Object> map) {
		
		String sql ="update busy_shopuser set "
				+ " id=:id,username=:username,password=:password,sex=:sex,mobile=:mobile,wxId=:wxId,openId=:openId,address=:address,ownerId=:ownerId,status=:status,isDelete=:isDelete,createTime=:createTime "
				+ " where id=:id";
		
		return  baseDAO.executeNamedCommand(sql, map);
	}
	
	public boolean updatePassWord(Map<String, Object> map) {
		
		String sql ="update busy_shopuser set "
				+ " password=:password "
				+ " where username=:username";
		
		return  baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteBusyShopuser(Map<String, Object> map) {
		
		String sql = "delete from busy_shopuser where id in ("+ map.get("id")+ " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	@SuppressWarnings("unchecked")
	public BusyShopuser getById(Map<String, Object> map) {
		
		String sql = "select " + getColumns() + " from busy_shopuser where id = "+ map.get("id") + "";
		
		BusyShopuser busyShopuser=new BusyShopuser();
		
		Map m=baseDAO.queryForMap(sql);
		
		try {
			org.apache.commons.beanutils.BeanUtils.copyProperties(busyShopuser, m);
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return null;
		}
		
		return busyShopuser;
		
	
	}
	
	//返回集合，也可以返回一个对象
	public List<Map<String, Object>> getBusyShopUser(String username) {
		
		String sql = "SELECT "
				+ " us.id,us.address,us.mobile,us.wxId,us.username,us.password,us.citycode,pc.name as cityname "
				+ " FROM busy_shopuser us left join plat_city pc on us.citycode = pc.code "
				+ " where us.username='"+username+"'";
		List<Map<String,Object>> list = baseDAO.queryForList(sql);
		return list;
	}

	//返回集合，也可以返回一个对象
	public List<Map<String, Object>> getPlatUser(String username) {

		String sql = "SELECT us.id,us.username,us.password,us.status,us.citycode FROM plat_user where us.username='"
				+ username + "'";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	
}
