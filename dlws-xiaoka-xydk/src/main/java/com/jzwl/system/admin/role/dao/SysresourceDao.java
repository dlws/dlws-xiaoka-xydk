/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.dao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.IdFactory;
import com.jzwl.system.admin.role.pojo.*;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.common.page.PageObject;



@Repository("v2sysresourceDao")
public class SysresourceDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	@Autowired
	private IdFactory idFactory;
	
	public boolean addSysresource(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("resourceId", idFactory.nextId());
		
		String sql = "insert into v2_plat_resource " 
				 + " (resourceId,componentid,componentname,parentid,resourceUrl,type,createdTime,status) " 
				 + " values "
				 + " (:resourceId,:componentid,:componentname,:parentid,:resourceUrl,:type,:createdTime,:status)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	public String getColumns() {
		return ""
				+" t.resourceId as resourceId,"
				+" t.componentid as componentid,"
				+" t.componentname as componentname,"
				+" t.parentid as parentid,"
				+" t.resourceUrl as resourceUrl,"
				+" t.type as type,"
				+" t.createdTime as createdTime,"
				+" t.status as status"
				;
	}
	

	public PageObject querySysresourceList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select " + getColumns() + " from v2_plat_resource t where 1=1 ";
		
		  	if(null !=map.get("componentid") && StringUtils.isNotEmpty(map.get("componentid").toString())){
			  		sql=sql+ " and t.componentid  = " + map.get("componentid") +"";
		  	}
		  	if(null !=map.get("componentname") && StringUtils.isNotEmpty(map.get("componentname").toString())){
			  		sql=sql+ " and t.componentname  = " + map.get("componentname") +"";
		  	}
		  	if(null !=map.get("parentid") && StringUtils.isNotEmpty(map.get("parentid").toString())){
			  		sql=sql+ " and t.parentid  = " + map.get("parentid") +"";
		  	}
		  	if(null !=map.get("resourceUrl") && StringUtils.isNotEmpty(map.get("resourceUrl").toString())){
			  		sql=sql+ " and t.resourceUrl  = " + map.get("resourceUrl") +"";
		  	}
		  	if(null !=map.get("type") && StringUtils.isNotEmpty(map.get("type").toString())){
			  		sql=sql+ " and t.type  = " + map.get("type") +"";
		  	}
		  	if(null !=map.get("createdTime") && StringUtils.isNotEmpty(map.get("createdTime").toString())){
			  		sql=sql+ " and t.createdTime >= " + map.get("createdTimeBegin") +"";
			  		sql=sql+ " and t.createdTime <= " + map.get("createdTimeEnd"  ) +"";
		  	}
		  	if(null !=map.get("status") && StringUtils.isNotEmpty(map.get("status").toString())){
			  		sql=sql+ " and t.status  = " + map.get("status") +"";
		  	}
					
					
		sql=sql+ " order by resourceId ";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}

	public boolean updateSysresource(Map<String, Object> map) {
		
		String sql ="update v2_plat_resource set "
				+ " resourceId=:resourceId,componentid=:componentid,componentname=:componentname,parentid=:parentid,resourceUrl=:resourceUrl,type=:type,createdTime=:createdTime,status=:status "
				+ " where resourceId=:resourceId";
		
		return  baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteSysresource(Map<String, Object> map) {
		
		String sql = "delete from v2_plat_resource where resourceId in ("+ map.get("resourceId")+ " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	@SuppressWarnings("unchecked")
	public Sysresource getByResourceId(Map<String, Object> map) {
		
		String sql = "select " + getColumns() + " from v2_plat_resource where resourceId = "+ map.get("resourceId") + "";
		
		Sysresource sysresource=new Sysresource();
		
		Map m=baseDAO.queryForMap(sql);
		
		try {
			org.apache.commons.beanutils.BeanUtils.copyProperties(sysresource, m);
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return null;
		}
		
		return sysresource;
		
	
	}
	
	public PageObject querySysresourceAuthList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select " + getColumns() + " from v2_plat_resource t ";
		
		//关联权限资源表查询			
		sql = sql+" left join v2_plat_authresource ar on t.resourceId = ar.resourceId where 1=1 ";
		
		  	if(null !=map.get("componentid") && StringUtils.isNotEmpty(map.get("componentid").toString())){
			  		sql=sql+ " and t.componentid  = " + map.get("componentid") +"";
		  	}
		  	if(null !=map.get("componentname") && StringUtils.isNotEmpty(map.get("componentname").toString())){
			  		sql=sql+ " and t.componentname  = " + map.get("componentname") +"";
		  	}
		  	if(null !=map.get("parentid") && StringUtils.isNotEmpty(map.get("parentid").toString())){
			  		sql=sql+ " and t.parentid  = " + map.get("parentid") +"";
		  	}
		  	if(null !=map.get("resourceUrl") && StringUtils.isNotEmpty(map.get("resourceUrl").toString())){
			  		sql=sql+ " and t.resourceUrl  = " + map.get("resourceUrl") +"";
		  	}
		  	if(null !=map.get("type") && StringUtils.isNotEmpty(map.get("type").toString())){
			  		sql=sql+ " and t.type  != 1";
		  	}
		  	if(null !=map.get("createdTime") && StringUtils.isNotEmpty(map.get("createdTime").toString())){
			  		sql=sql+ " and t.createdTime >= " + map.get("createdTimeBegin") +"";
			  		sql=sql+ " and t.createdTime <= " + map.get("createdTimeEnd"  ) +"";
		  	}
		  	if(null !=map.get("status") && StringUtils.isNotEmpty(map.get("status").toString())){
			  		sql=sql+ " and t.status  = " + map.get("status") +"";
		  	}
		sql = sql +" and ar.authorityId="+map.get("authorityId")+"";			
		sql=sql+ " order by t.resourceId ";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}
	
	public PageObject querySysresourceNoAuthList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select * from v2_plat_resource where resourceId not in";
		sql = sql+"(select t.resourceId as resourceId from v2_plat_resource t ";
		//关联权限资源表查询			
		sql = sql+" left join v2_plat_authresource ar on t.resourceId = ar.resourceId where 1=1 ";
		
		  
		sql=sql+ " and t.type  != 1";
		  	
		sql = sql +" and ar.authorityId="+map.get("authorityId")+"";			
		sql=sql+ " order by t.resourceId) ";
		sql = sql +" and type !=1 order by resourceId";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}
	
	public boolean addSysresourceAuth(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
//		map.put("resourceId",  Sequence.nextId());
		
		String sql = "insert into v2_plat_authresource " 
				 + " (resourceId,authorityId) " 
				 + " values "
				 + " (:resourceId,:authorityId)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	public boolean deleteSysresourceAuth(Map<String, Object> map) {
		
		String sql = "delete from v2_plat_authresource where resourceId=:resourceId and authorityId=:authorityId ";

		return baseDAO.executeNamedCommand(sql, map);
	}
}
