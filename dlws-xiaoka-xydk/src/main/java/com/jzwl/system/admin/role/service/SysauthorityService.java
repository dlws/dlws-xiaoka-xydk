/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.admin.role.dao.*;
import com.jzwl.system.admin.role.pojo.*;


@Service("v2sysauthorityService")
public class SysauthorityService {
	
	
	@Autowired
	private SysauthorityDao sysauthorityDao;
	
	public boolean addSysauthority(Map<String, Object> map) {
		
		return sysauthorityDao.addSysauthority(map);
		
	}

	public PageObject querySysauthorityList(Map<String, Object> map) {
		
		return sysauthorityDao.querySysauthorityList(map);
	
	}

	public boolean updateSysauthority(Map<String, Object> map) {
	
		return sysauthorityDao.updateSysauthority(map);
	
	}

	public boolean deleteSysauthority(Map<String, Object> map) {
		
		return sysauthorityDao.deleteSysauthority(map);
	
	}
	
	
	@SuppressWarnings("unchecked")
	public Sysauthority getByAuthorityId(Map<String, Object> map) {
		
		return sysauthorityDao.getByAuthorityId(map);
		
	}
	
	
}
