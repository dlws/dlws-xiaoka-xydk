/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.dao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.system.admin.role.pojo.*;
import com.jzwl.system.base.dal.strategy.IDynamicDataSource;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.common.page.PageObject;



@Repository("v2sysauthorityDao")
public class SysauthorityDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	@Autowired
	private IDynamicDataSource dynamicDataSource;
	
	
	public boolean addSysauthority(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
//		map.put("authorityId",  Sequence.nextId());
		
		String sql = "insert into v2_plat_authority " 
				 + " (authorityName,authorityCode) " 
				 + " values "
				 + " (:authorityName,authorityCode)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	public String getColumns() {
		return ""
				+" authorityId as authorityId,"
				+" authorityName as authorityName,"
				+" authorityCode as authorityCode"
				;
	}
	

	public PageObject querySysauthorityList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select " + getColumns() + " from v2_plat_authority t where 1=1 ";
		
		  	if(null !=map.get("authorityName") && StringUtils.isNotEmpty(map.get("authorityName").toString())){
			  		sql=sql+ " and t.authorityName  like '%" + map.get("authorityName") +"%' ";	
		  	}
			
		  	if(null !=map.get("authorityCode") && StringUtils.isNotEmpty(map.get("authorityCode").toString())){
		  		sql=sql+ " and t.authorityCode  like '%" + map.get("authorityCode") +"%' ";	
		  	}
					
		sql=sql+ " order by authorityId ";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}

	public boolean updateSysauthority(Map<String, Object> map) {
		
		String sql ="update v2_plat_authority set "
				+ " authorityName=:authorityName,authorityCode=:authorityCode "
				+ " where authorityId=:authorityId";
		
		return  baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteSysauthority(Map<String, Object> map) {
		
		String sql = "delete from v2_plat_authority where authorityId in ("+ map.get("authorityId")+ " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	@SuppressWarnings("unchecked")
	public Sysauthority getByAuthorityId(Map<String, Object> map) {
		
		String sql = "select " + getColumns() + " from v2_plat_authority where authorityId = "+ map.get("authorityId") + "";
		
		Sysauthority sysauthority=new Sysauthority();
		
		Map m=baseDAO.queryForMap(sql);
		
		try {
			org.apache.commons.beanutils.BeanUtils.copyProperties(sysauthority, m);
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return null;
		}
		
		return sysauthority;
		
	
	}
	

}
