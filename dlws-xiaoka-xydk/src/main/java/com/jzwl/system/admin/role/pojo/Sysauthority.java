/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class Sysauthority extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "Sysauthority";
	public static final String ALIAS_AUTHORITY_ID = "自增主键";
	public static final String ALIAS_AUTHORITY_NAME = "权限名称x-like";
	
	//date formats
	

	//columns START
    /**
     * 自增主键       db_column: authorityId 
     */ 	
	private java.lang.Integer authorityId;
    /**
     * 权限名称x-like       db_column: authorityName 
     */ 	
	private java.lang.String authorityName;
	//columns END

	
	
	public void setAuthorityId(java.lang.Integer value) {
		this.authorityId = value;
	}
	
	public java.lang.Integer getAuthorityId() {
		return this.authorityId;
	}
	


	public java.lang.String getAuthorityName() {
		return this.authorityName;
	}
	
	public void setAuthorityName(java.lang.String value) {
		this.authorityName = value;
	}
	

}

