/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2015
 */

package com.jzwl.system.admin.role.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.system.admin.role.pojo.*;
import com.jzwl.system.admin.role.service.*;
import com.jzwl.system.admin.sysuser.service.SysuserService;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.common.page.PageObject;

/**
 * Sysrole
 * Sysrole
 * sysrole
 * <p>Title:SysroleController </p>
 * 	<p>Description:Sysrole </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("v2sysroleController")
@RequestMapping("/v2sysrole")
public class SysroleController  extends BaseController {
	
	@Autowired
	private SysroleService sysroleService;
	
	@Autowired
	private SysuserService sysuserService;
	
	@Autowired
	private SysauthorityService sysauthorityService;
	
	
	/**
	 * 跳转到添加页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toAddRole")
	public String toAddRole(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		model.addAttribute("url", "v2sysrole/addSysrole.html");
		return "/base/system/roleform";
	}
	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdateRole")
	public String toUpdateRole(HttpServletRequest request, HttpServletResponse response,Model model) {
		try{
			createParameterMap(request);
			Map<String,Object> objMap = sysroleService.getByRoleId(paramsMap);
			model.addAttribute("objMap", objMap);
			model.addAttribute("url", "v2sysrole/updateSysrole.html");
			return "/base/system/roleform";
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg","获取数据失败");
			return "/error";
		}
	}
	
	
	/**
	 * 添加Sysrole
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addSysrole", method = RequestMethod.POST)
	public String sysrole(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		createParameterMap(request);
		paramsMap.put("createdTime", new Date());
		
		try{
		
			boolean flag = sysroleService.addSysrole(paramsMap);
		
			if(flag){
				return "redirect:/v2sysrole/sysroleList.html?start=0";
			}else{
				model.addAttribute("msg","添加失败");
				return "/error";
			}
		
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg","添加失败");
			return "/error";
		}
	}
	
	/**
	 * sysrolelist
	 */
	@RequestMapping(value = "/sysroleList")
	public String sysroleList(HttpServletRequest request, HttpServletResponse response,Model model) {
	
		createParameterMap(request);
		PageObject po = new PageObject();
		try{
			po = sysroleService.querySysroleList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap",paramsMap);
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg","获取数据失败");
			return "/error";
		}
		return "/base/system/rolelist";
	}
	
	
	
	/**
	 * getSysrole
	 */
	@ResponseBody
	@RequestMapping(value = "/getSysrole", method = RequestMethod.POST)
	public Map<String,Object> getSysrole(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
			
			Map<String,Object> objMap = sysroleService.getByRoleId(paramsMap);
			
			resultMap.put("success", true);
			resultMap.put("obj", objMap);
			
		
		}catch(Exception e){
		
			resultMap.put("info", "获取失败");
			
			e.printStackTrace();
			
		}
		
		return resultMap;
	}
	
	
	/**
	 * 修改sysrole
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateSysrole", method = RequestMethod.POST)
	public String updateSysrole(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		createParameterMap(request);
		paramsMap.put("modifiedTime", new Date());
		
		try{
		
			boolean flag = sysroleService.updateSysrole(paramsMap);
			
			if(flag){
				return "redirect:/v2sysrole/sysroleList.html?start=0";
			}else{
				model.addAttribute("msg","保存失败");
				return "/error";
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg","保存失败");
			return "/error";
		}
	}
	
	
	/**
	 * 删除sysrole
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteSysrole")
	public String deleteSysrole(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		createParameterMap(request);
		
		try{
		
			boolean flag = sysroleService.deleteSysrole(paramsMap);
			
			if(flag){
				return "redirect:/v2sysrole/sysroleList.html?start=0";
			}else{
				model.addAttribute("msg","删除失败");
				return "/error";
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg","删除失败");
			return "/error";
		}
	}
	
	//auto-code-end
	
	/**
	 * sysrolelist
	 */
	@ResponseBody
	@RequestMapping(value = "/sysroleUserList", method = RequestMethod.POST)
	public Map<String,Object> sysroleUserList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			po = sysroleService.querySysroleUserList(paramsMap);
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}
		
		return resultMap;
	}
	
	/**
	 * sysrolelist
	 */
	@ResponseBody
	@RequestMapping(value = "/sysroleNoUserList", method = RequestMethod.POST)
	public Map<String,Object> sysroleNoUserList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			po = sysroleService.querySysroleNoUserList(paramsMap);
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}
		
		return resultMap;
	}
	
	/**
	 * 删除sysrole
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSysroleUser", method = RequestMethod.POST)
	public Map<String,Object> deleteSysroleUser(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = sysroleService.deleteSysroleUser(paramsMap);
			
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "删除成功！");
			
			}else{
			
				resultMap.put("info", "删除失败！");
			
			}
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "删除失败！");
		
		}
		
		return resultMap;
	}
	
	/**
	 * 添加角色下的用户
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addSysroleUser", method = RequestMethod.POST)
	public Map<String,Object> addSysroleUser(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		paramsMap.put("createdTime", new Date());
		
		try{
		
			boolean flag = sysroleService.addSysroleUser(paramsMap);
		
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "添加成功！");
			}else{
			
				resultMap.put("info", "添加失败！");
			}
		
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "添加失败!");
		}
		
		return resultMap;
		
	}
	
	/**
	 * sysrolelist
	 */
	@ResponseBody
	@RequestMapping(value = "/sysroleAuthorityList", method = RequestMethod.POST)
	public Map<String,Object> sysroleAuthorityList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			po = sysroleService.querySysroleAuthorityList(paramsMap);
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}
		
		return resultMap;
	}
	
	/**
	 * sysrolelist
	 */
	@ResponseBody
	@RequestMapping(value = "/sysroleNoAuthorityList", method = RequestMethod.POST)
	public Map<String,Object> sysroleNoAuthorityList(HttpServletRequest request, HttpServletResponse response) {
	
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			List  list = sysroleService.querySysroleNoAuthorityList(paramsMap);
			
			resultMap.put("list", list );
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}catch(Exception e){
		
			e.printStackTrace();
			
			resultMap.put("list", po.getDatasource());
			
			resultMap.put("totalProperty", po.getTotalCount());
		
		}
		
		return resultMap;
	}
	
	/**
	 * 删除sysrole
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSysroleAuthority", method = RequestMethod.POST)
	public Map<String,Object> deleteSysroleAuthority(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		
		try{
		
			boolean flag = sysroleService.deleteSysroleAuthotity(paramsMap);
			
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "删除成功！");
			
			}else{
			
				resultMap.put("info", "删除失败！");
			
			}
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "删除失败！");
		
		}
		
		return resultMap;
	}
	
	/**
	 * 添加角色下的权限
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addSysroleAuthority", method = RequestMethod.POST)
	public Map<String,Object> addSysroleAuthority(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		createParameterMap(request);
		paramsMap.put("createdTime", new Date());
		
		try{
		
			boolean flag = sysroleService.addSysroleAuthority(paramsMap);
		
			if(flag){
			
				resultMap.put("success", true);
				resultMap.put("info", "添加成功！");
			}else{
			
				resultMap.put("info", "添加失败！");
			}
		
		}catch(Exception e){
			
			e.printStackTrace();
			
			resultMap.put("info", "添加失败!");
		}
		
		return resultMap;
		
	}
}

