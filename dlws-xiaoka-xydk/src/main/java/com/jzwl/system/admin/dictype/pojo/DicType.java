/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.system.admin.dictype.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class DicType extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "V2DicType";
	public static final String ALIAS_DIC_ID = "主键";
	public static final String ALIAS_DIC_NAME = "字典类型名";
	public static final String ALIAS_DIC_CODE = "字典类型标示代码 ";
	public static final String ALIAS_IS_DELETE = "是否删除";
	
	//date formats
	

	//columns START
    /**
     * 主键       db_column: dic_id 
     */ 	
	private java.lang.Long dicId;
    /**
     * 字典类型名       db_column: dic_name 
     */ 	
	private java.lang.String dicName;
    /**
     * 字典类型标示代码        db_column: dic_code 
     */ 	
	private java.lang.String dicCode;
    /**
     * 是否删除       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
	//columns END

	
	
	public void setDicId(java.lang.Long value) {
		this.dicId = value;
	}
	
	public java.lang.Long getDicId() {
		return this.dicId;
	}
	
	
	


	public java.lang.String getDicName() {
		return this.dicName;
	}
	
	public void setDicName(java.lang.String value) {
		this.dicName = value;
	}
	

	public java.lang.String getDicCode() {
		return this.dicCode;
	}
	
	public void setDicCode(java.lang.String value) {
		this.dicCode = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

}

