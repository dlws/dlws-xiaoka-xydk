/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.special.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("specialDao")
public class SpecialDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	public boolean addSpecial(Map<String, Object> map) {

		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id", Sequence.nextId());
		map.put("isDelete", 0);

		String sql = "insert into `xiaoka-xydk`.special "
				+ " (id,title,content,imgerUrl,ord,isDisplay,createDate,isDelete,jumpLink) " + " values "
				+ " (:id,:title,:content,:imgerUrl,:ord,:isDisplay,:createDate,:isDelete,:jumpLink)";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public String getColumns() {
		return "" + " id as id," + " title as title," + " content as content," + " imgerUrl as imgerUrl,"
				+ " ord as ord," + " isDisplay as isDisplay," + " createDate as createDate," + " isDelete as isDelete,"
				+ "jumpLink as jumpLink";
	}

	public PageObject querySpecialList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.special t where 1=1 ";

		if (null != map.get("title") && StringUtils.isNotEmpty(map.get("title").toString())) {
			sql = sql + " and t.title  like '%" + map.get("title") + "%' ";
		}

		if (null != map.get("jumpLink") && StringUtils.isNotEmpty(map.get("jumpLink").toString())) {
			sql = sql + " and t.jumpLink  = " + map.get("jumpLink") + "";
		}
		if (null != map.get("content") && StringUtils.isNotEmpty(map.get("content").toString())) {
			sql = sql + " and t.content  = " + map.get("content") + "";
		}

		if (null != map.get("imgerUrl") && StringUtils.isNotEmpty(map.get("imgerUrl").toString())) {
			sql = sql + " and t.imgerUrl  = " + map.get("imgerUrl") + "";
		}

		if (null != map.get("ord") && StringUtils.isNotEmpty(map.get("ord").toString())) {
			sql = sql + " and t.ord  = " + map.get("ord") + "";
		}

		if (null != map.get("isDisplay") && StringUtils.isNotEmpty(map.get("isDisplay").toString())) {
			sql = sql + " and t.isDisplay  = " + map.get("isDisplay") + "";
		}

		if (null != map.get("createDateBegin") && StringUtils.isNotEmpty(map.get("createDateBegin").toString())) {
			sql = sql + " and t.createDate >= '" + map.get("createDateBegin") + "'";
		}
		if (null != map.get("createDateEnd") && StringUtils.isNotEmpty(map.get("createDateEnd").toString())) {
			sql = sql + " and t.createDate <= '" + map.get("createDateEnd") + "'";
		}

		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + " and t.isDelete  = " + map.get("isDelete") + "";
		}

		sql = sql + " order by isDisplay desc,createDate ";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	public boolean updateSpecial(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.special set ";
		if (null != map.get("title") && StringUtils.isNotEmpty(map.get("title").toString())) {
			sql = sql + "			title=:title ,";
		}
		if (null != map.get("content") && StringUtils.isNotEmpty(map.get("content").toString())) {
			sql = sql + "			content=:content ,";
		}
		if (null != map.get("imgerUrl") && StringUtils.isNotEmpty(map.get("imgerUrl").toString())) {
			sql = sql + "			imgerUrl=:imgerUrl ,";
		}
		if (null != map.get("jumpLink") && StringUtils.isNotEmpty(map.get("jumpLink").toString())) {
			sql = sql + "			jumpLink=:jumpLink ,";
		}
		if (null != map.get("ord") && StringUtils.isNotEmpty(map.get("ord").toString())) {
			sql = sql + "			ord=:ord ,";
		}
		if (null != map.get("isDisplay") && StringUtils.isNotEmpty(map.get("isDisplay").toString())) {
			sql = sql + "			isDisplay=:isDisplay ,";
		}
		if (null != map.get("createDate") && StringUtils.isNotEmpty(map.get("createDate").toString())) {
			sql = sql + "			createDate=:createDate ,";
		}
		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + "			isDelete=:isDelete ,";
		}
		sql = sql.substring(0, sql.length() - 1);
		sql = sql + " where id=:id";
		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteSpecial(Map<String, Object> map) {

		String sql = "delete from `xiaoka-xydk`.special where id in (" + map.get("id") + " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean delSpecial(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.special set " + " isDelete =:" + map.get("isDelete") + " " + " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.special where id = " + map.get("id") + "";

		return baseDAO.queryForMap(sql);
	}

	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		String sql = " SELECT dd.id,dd.dic_name " + "    from dic_type  dt "
				+ "    LEFT JOIN  dic_data dd on dt.dic_id = dd.dic_id " + "    where dt.dic_code = '"
				+ map.get("code") + "'  and dd.isDelete = 0 ";
		return baseDAO.queryForList(sql);
	}

}
