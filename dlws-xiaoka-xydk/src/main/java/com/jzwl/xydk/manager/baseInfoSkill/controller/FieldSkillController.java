package com.jzwl.xydk.manager.baseInfoSkill.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.baseInfoSkill.service.BaseInfoSkillService;
import com.jzwl.xydk.manager.baseInfoSkill.service.FieldSkillService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserEnterInfoService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;
import com.jzwl.xydk.wap.xkh2_3.publish.controller.PublishXkhController;


/**
 * 关于场地基本信息入驻的（增删改）
 * @author Administrator
 */

@Controller
@RequestMapping(value="/fieldSkill")
public class FieldSkillController extends BaseWeixinController {
	

	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private UserEnterInfoService userenterInfoService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private FieldSkillService fieldSkillService;
	@Autowired
	private Constants constants;
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	@Autowired
	private BaseInfoSkillService baseInfoSkillService;
	
	private final String FILED_SIZE = "filedsize";//场地规模
	
	private final String BASIC_MATERIAL = "basicmaterial";//基本物资配备
	
	Logger log = Logger.getLogger(FieldSkillController.class);
	
	/**
	 * 加载当前页面基本信息
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/toAddField")
	public String toAddField(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		//1.获取当前用户的用户Id
		if(paramsMap.containsKey("id")&&StringUtils.isNotBlank(paramsMap.get("id").toString())){
			
			model = toLoadCommon(model);
	
			return "manager/baseUser/FieldSkill/add_field";
		}
		
		return "";
	}
	
	/**
	 * 场地技能添加的方法
	 */
	@RequestMapping(value="/addField")
	public String addField(HttpServletRequest request,Model model){
		
		createParameterMap(request);//获取页面传递过来的参数
		String basicId = paramsMap.get("basicId").toString();
		Map<String, Object> map = createParameterMap(request);
		map.put("serviceType", 2);//场地默认线下服务
		if (request.getParameterValues("material") != null) {//设置当前场地的属性
			map.put("material", dealArray(request.getParameterValues("material")));
		}
		//覆盖当前数组类参数
		if (request.getParameterValues("closePhoto") != null && request.getParameterValues("middlePho") != null
				&& request.getParameterValues("allPhoto") != null) {
			map.put("closePhoto", dealArray(request.getParameterValues("closePhoto")));//近景
			map.put("middlePho", dealArray(request.getParameterValues("middlePho")));//中景
			map.put("allPhoto", dealArray(request.getParameterValues("allPhoto")));//远景
		} else {
			log.info("近景，中景，远景照片不能为空  且每个种类照片不能小于两张");
		}
		if(StringUtils.isNotBlank(baseInfoSkillService.addUserSkillinfo(map))){
			model.addAttribute("msg", "添加成功");
			return "redirect:/baseInfoSkill/list.html?basicId=" + basicId;
		}
		model.addAttribute("msg", "添加失败");
		return "/error";
		
	}
	
	/**
	 * 关于场地技能的修改
	 */
	@RequestMapping(value="/toUpFieldSkill")
	public String toUpFieldSkill(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		//加载需要展示的公共参数
		model = toLoadCommon(model);
		//根据当前用户的技能ID获取技能信息，以及详情表的信息
		Map<String, Object> skillInfo = baseInfoSkillService.querySkillById(paramsMap);
		//查询当前技能的技能详情
		paramsMap.put("pid", paramsMap.get("skillId"));
		
		Map<String,Object> detailMap = personInfoDetSer.getuserSkillinfoDetail(paramsMap);
		
		//获取图片信息处理成List
		List<String> listClose = StringUtil.dealString(detailMap.get("closePhoto").toString());
		List<String> listMiddle =StringUtil.dealString(detailMap.get("allPhoto").toString()) ;
		List<String> listAll = StringUtil.dealString(detailMap.get("middlePho").toString());
		
		//组装返回前台参数
		model.addAttribute("skillInfo", skillInfo);
		model.addAttribute("detailMap", detailMap);
		model.addAttribute("listClose", listClose);
		model.addAttribute("listMiddle", listMiddle);
		model.addAttribute("listAll", listAll);
		
		return "manager/baseUser/FieldSkill/edit_field";
	}
	
	/**
	 * 更新表中的场地技能
	 */
	@RequestMapping(value="/upFieldSkill")
	public String upFieldSkill(HttpServletRequest request,Model model){
		
		createParameterMap(request);//获取页面传递过来的参数
		String basicId = paramsMap.get("basicId").toString();
		Map<String, Object> map = createParameterMap(request);
		map.put("serviceType", 2);//场地修改默认为线下
		map.remove("basicId");
		if (request.getParameterValues("material") != null) {//设置当前场地的属性
			map.put("material", dealArray(request.getParameterValues("material")));
		}
		//覆盖当前数组类参数
		if (request.getParameterValues("closePhoto") != null && request.getParameterValues("middlePho") != null
				&& request.getParameterValues("allPhoto") != null) {
			map.put("closePhoto", dealArray(request.getParameterValues("closePhoto")));//近景
			map.put("middlePho", dealArray(request.getParameterValues("middlePho")));//中景
			map.put("allPhoto", dealArray(request.getParameterValues("allPhoto")));//远景
		} else {
			log.info("近景，中景，远景照片不能为空  且每个种类照片不能小于两张");
		}
		//执行更新表的操作
		System.out.println(map.toString());
		boolean flage = baseInfoSkillService.updateUserSkillinfo(map);
		
		if(flage){
			
			return "redirect:/baseInfoSkill/list.html?basicId=" + basicId;
		}
		
		model.addAttribute("msg", "修改失败");
		return "/error";
	}
	
	
	
	
	
	
	/**
	 * 加载当前的公共信息
	 * @param model
	 * @return
	 */
	public Model toLoadCommon(Model model){
		
		//2.获取当前技能要在页面中加载的参数
		List<Map<String,Object>> list = fieldSkillService.getSkillType(GlobalConstant.ENTER_TYPE_CDZY);
		//加载当前的场地规模（字典表） filedsize
		List<Map<String, Object>> filedSizeList = userBasicinfoService.getFieldtype(FILED_SIZE);
		//加载当前的基本物资配备（字典表）basicmaterial 
		List<Map<String, Object>> materialList = userBasicinfoService.getFieldtype(BASIC_MATERIAL);
		//加载当前页面需要的所有单位
		List<Map<String,Object>> allUnit = baseInfoSkillService.getCanpanyMap();
		model.addAttribute("paramsMap", paramsMap);
		
		model.addAttribute("filedSizeList", filedSizeList);//场地规模
		model.addAttribute("materialList", materialList);//基本物资配备
		model.addAttribute("list", list);
		model.addAttribute("id", paramsMap.get("id").toString());//当前用户的ID传递到前台
		model.addAttribute("allUnit", allUnit);
		
		return model;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 处理参数用，号隔开的方式进行返回String类型
	 */
	public String dealArray(String[] str) {
		StringBuffer all = new StringBuffer();
		for (int i = 0; i < str.length; i++) {

			if (i == str.length - 1) {
				all.append(str[i]);
			} else {
				all.append(str[i] + ",");
			}

		}
		return all.toString();
	}
	
	

}
