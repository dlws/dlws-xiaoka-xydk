package com.jzwl.xydk.manager.wallet.dao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("managerWalletDao")
public class ManagerWalletDao {

	@Autowired
	private BaseDAO baseDAO;

	public PageObject queryWalletforPage(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append(" select c.name,t.id,t.openId,t.balance,t.createTime,u.userName,u.email,u.phoneNumber,"
				+ "u.wxNumber,u.contactUser,d.cityName,d.schoolName from `xiaoka-xydk`.wallet t "
				+ " left join v2_wx_customer c on t.openId = c.openId "
				+ " left join `xiaoka-xydk`.user_basicinfo u on t.openId=u.openId "
				+ " left join `xiaoka-xydk`.diplomatinfo d on u.schoolId=d.schoolId " + " where 1=1 ");
		if (null != map.get("name") && StringUtils.isNotEmpty(map.get("name").toString())) {
			sb.append(" and c.name like '%" + map.get("name") + "%' ");
		}
		if (null != map.get("cityName") && StringUtils.isNotEmpty(map.get("cityName").toString())) {
			sb.append(" and d.cityName like '%" + map.get("cityName") + "%' ");
		}
		if (null != map.get("schoolName") && StringUtils.isNotEmpty(map.get("schoolName").toString())) {
			sb.append(" and d.schoolName like '%" + map.get("schoolName") + "%' ");
		}
		sb.append(" order by t.createTime desc ");
		PageObject po = baseDAO.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;
	}
}
