package com.jzwl.xydk.manager.refunds.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jzwl.common.constant.Constants;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.refunds.service.RefundsService;

/**
 * 退款管理controller
 * @author sx
 */
@Controller
@RequestMapping("/refunds")
public class RefundsController extends BaseWeixinController {
	@Autowired
	private RefundsService refundsService;
	@Autowired
	private Constants constants;

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：退款管理列表
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/refundsList")
	public String refundsList(HttpServletRequest request, HttpServletResponse response, Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = refundsService.queryRefundsforPage(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/refunds/refunds_list";
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据id获取要审核的条目
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/getRefundsById")
	public String getRefundsById(HttpServletRequest request, HttpServletResponse response, Model mov) {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		createParameterMap(request);
		if (paramsMap.get("id") != null && StringUtils.isNotBlank(paramsMap.get("id").toString())) {
			list = refundsService.queryRefundsforList(paramsMap);
			if (list.size() == 0) {
				mov.addAttribute("msg", "未查询到当前退款条目");
				return "/error";
			}
		} else {
			mov.addAttribute("msg", "未查询到当前退款条目");
			return "/error";
		}
		mov.addAttribute("refunds", list.get(0));
		mov.addAttribute("start", paramsMap.get("start"));
		// 暂时无需要加载的基础参数  只作为跳转
		return "/manager/refunds/refunds_review";
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：审核
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/audit")
	public String audit(HttpServletRequest request, HttpServletResponse response) {
		createParameterMap(request);
		paramsMap.put("userName", request.getSession().getAttribute("userName"));
		String radio = (String) paramsMap.get("radio");
		//订单id
		String orderId = (String) paramsMap.get("refundsOrderId");
		//用户id
		String openId = (String) paramsMap.get("openId");
		//数据来源,如果是商家拒绝(0)订单全额退款,否则(1)按比例退款
		String orderSourceStatus = (String) paramsMap.get("orderSourceStatus");
		Double money = 0d;
		Double amount = 0d;
		String dic_value = "";
		try {
			if ("1".equals(radio)) {
				//计算退款金额
				String refundsMoney = (String) paramsMap.get("refundsMoney");
				//根据字典查询服务费用
				List<Map<String, Object>> dic_daList = refundsService.getSourceTypeByCode();
				Map<String, Object> map = new HashMap<String, Object>();
				if (dic_daList != null && dic_daList.size() > 0) {
					map = dic_daList.get(0);
					dic_value = (String) map.get("dic_value");
				}
				if (dic_value != null && !"".equals(dic_value)) {
					money = Double.valueOf(dic_value);
				}
				Double reMoney = 0d;
				if (refundsMoney != null && !"".equals(refundsMoney)) {
					reMoney = Double.valueOf(refundsMoney);
				}
				//买家实际退款金额  
				if (orderSourceStatus.equals("0")) {
					amount = reMoney;
				} else {
					amount = reMoney - money;
				}
				if (amount < 0) {
					amount = 0d;
				}
				//计算卖家该订单收入,根据比例算出金额.
				//根据订单id查订单
				Map<String, Object> order = refundsService.findOrderById(Long.valueOf(orderId));
				//商家分成比例
				BigDecimal serviceRatio = (BigDecimal) order.get("serviceRatio");
				//订单金额
				BigDecimal realityMoney = (BigDecimal) order.get("realityMoney");
				//商家分的金额
				BigDecimal saleMoney = realityMoney.multiply(serviceRatio);
				//商家openId
				String sellerOpenId = (String) order.get("sellerOpenId");
				//查询商家当前账户余额
				Map<String, Object> mapp = refundsService.findWalletBalanceByOpenId(sellerOpenId);
				//查询买家账户余额
				Map<String, Object> bmap = refundsService.findWalletBalanceByOpenId(openId);
				//买家钱包数据
				Map<String, Object> bwmap = new HashMap<String, Object>();
				bwmap.put("openId", openId);
				bwmap.put("balanceMoney", amount);
				Double balance = 0d;
				if (mapp != null && mapp.size() > 0) {
					balance = (Double) mapp.get("balance");
				}
				//计算新的账户余额
				BigDecimal ban = BigDecimal.valueOf(balance);
				BigDecimal totalMoney = ban.add(saleMoney);
				//插入到分成表中start
				Map<String, Object> mapMoney = new HashMap<String, Object>();
				mapMoney.put("openId", sellerOpenId);
				mapMoney.put("divideOrWithdraw", saleMoney);
				mapMoney.put("balanceMoney", totalMoney);
				mapMoney.put("state", 0);
				mapMoney.put("divideOrWithdrawTime", new Date());
				mapMoney.put("orderId", orderId);
				refundsService.divideOrWithDrawRecord(mapMoney);
				//插入数据到分成表中end
				//更新商家钱包start
				refundsService.updateWallet(mapp, mapMoney);
				//更新商家钱包end
				//更新买家钱包start
				refundsService.updateWallet(bmap, bwmap);
				//更新买家钱包end
				//更改订单状态start
				Map<String, Object> orderMap = new HashMap<String, Object>();
				orderMap.put("orderId", orderId);
				if (orderSourceStatus.equals("0")) {
					//商家拒绝接单 将订单状态改为10 已关闭
					orderMap.put("state", 10);
				} else {
					//商家同意退款 将订单状态改为8 退款成功
					orderMap.put("state", 8);
				}
				refundsService.updateOrderState(orderMap);
				//更改订单状态end
				//更新本条记录start
				refundsService.updateRefunds(paramsMap);
				//更新本条记录end
			} else {
				//审核不通过
				refundsService.auditNotPass(paramsMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		//mov.setViewName("redirect:/refunds/refundsList.html");
		return "redirect:/refunds/refundsList.html";
	}

	/*退款--->调用微信(暂时不用)
	 * @RequestMapping(value = "/audit")
	public String audit(HttpServletRequest request, HttpServletResponse response){
		createParameterMap(request);
		paramsMap.put("userName", request.getSession().getAttribute("userName"));
		Map<String,Object> signMap = new HashMap<String,Object>();
		//ModelAndView mov = new ModelAndView();
		String radio = (String)paramsMap.get("radio");
		//订单id
		Long orderId = (Long)paramsMap.get("refundsOrderId");
		//用户id
		String openId = (String)paramsMap.get("openId");
		//数据来源,如果是商家拒绝(0)订单全额退款,否则(1)按比例退款
		String orderSourceStatus = (String)paramsMap.get("orderSourceStatus");
		Double money = 0d;
		Double amount = 0d;
		String desc = "";
		try {
			if("1".equals(radio)){
				//计算退款金额
				Double refundsMoney = (Double)paramsMap.get("refundsMoney");
				//根据字典查询服务费用
				List<Map<String, Object>> dic_daList = refundsService.getSourceTypeByCode();
				Map<String, Object> map = new HashMap<String, Object>();
				if(dic_daList != null && dic_daList.size() > 0){
					map = dic_daList.get(0);
					money = (Double) map.get("dic_value");
				}
				//买家实际退款金额  
				if(orderSourceStatus.equals("0")){
					amount = refundsMoney;
				}else{
					amount = refundsMoney - money;
				}
				if(amount < 0){
					amount = 0d;
				}
				//获取订单描述
				desc = refundsService.findOrderDetailNikeNameByOrderId(orderId)+"退款";
				Double mount = amount * 100;
				String str = mount+"";
				String[] split = str.split("\\.");
				String samount = split[0];
				signMap.put("openid", openId);
				signMap.put("amount", samount);
				signMap.put("desc", desc);
				String sign = AccessCore.sign(signMap, constants.getWx_signKey());//拿到签名数据
				//根据退款接口传入相应参数,调用退款
				String path = constants.getXiaoka_weixin_xydk_tuikuan()+"?openid="+openId +"&amount="+samount+"&desc="+desc+"&sign="+sign;
				Map<String, Object> urlMap = HttpClientUtil.requestByGetMethod(path);
				boolean flag = (boolean)urlMap.get("flag");
				//--返回--返回判断
				if(flag){
					//计算卖家该订单收入,根据比例算出金额.
					//根据订单id查订单
					Map<String, Object> order = refundsService.findOrderById(orderId);
					//商家分成比例
					BigDecimal serviceRatio = (BigDecimal)order.get("serviceRatio");
					//订单金额
					BigDecimal realityMoney = (BigDecimal)order.get("realityMoney");
					//商家分的金额
					BigDecimal saleMoney = realityMoney.multiply(serviceRatio);
					//商家openId
					String sellerOpenId = (String)order.get("sellerOpenId");
					//查询当前账户余额
					Map<String, Object> mapp = refundsService.findWalletBalanceByOpenId(sellerOpenId);
					Double balance = 0d;
					if(mapp != null && mapp.size() > 0){
						balance = (Double)mapp.get("balance");
					}
					//计算新的账户余额
					BigDecimal ban = BigDecimal.valueOf(balance);
					BigDecimal totalMoney = ban.add(saleMoney);
					//插入到分成表中start
					Map<String, Object> mapMoney = new HashMap<String, Object>();
					mapMoney.put("openId", sellerOpenId);
					mapMoney.put("saleMoney", saleMoney);
					mapMoney.put("totalMoney", totalMoney);
					mapMoney.put("state", 0);
					mapMoney.put("divideOrWithdrawTime", new Date());
					refundsService.divideOrWithDrawRecord(mapMoney);
					//插入数据到分成表中end
					//更新钱包start
					refundsService.updateWallet(mapp,mapMoney);
					//更新钱包end
					//更改订单状态start
					Map<String, Object> orderMap = new HashMap<String, Object>();
					orderMap.put("orderId", orderId);
					if(orderSourceStatus.equals("0")){
						//商家拒绝接单 将订单状态改为10 已关闭
						orderMap.put("state", 10);
					}else{
						//商家同意退款 将订单状态改为8 退款成功
						orderMap.put("state", 8);
					}
					refundsService.updateOrderState(orderMap);
					//更改订单状态end
					//更新本条记录start
					refundsService.updateRefunds(paramsMap);
					//更新本条记录end
				}
			}else{
				//审核不通过
				refundsService.auditNotPass(paramsMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}	
		//mov.setViewName("redirect:/refunds/refundsList.html");
		return "redirect:/refunds/refundsList.html";
	}*/

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家拒绝订单
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/refuseOrder")
	public Map<String, Object> refuseOrder(HttpServletRequest request, HttpServletResponse response) {
		// 用户id:openId  订单id:refundsOrderId  价格:price  
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			boolean flag = refundsService.refuseOrder(paramsMap);
			map.put("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家拒绝退款
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/refundDenied")
	public Map<String, Object> refundDenied(HttpServletRequest request, HttpServletResponse response) {
		//商家拒绝退款 直接在退款表里插入数据 checkStatus = 2(审核失败)
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			boolean flag = refundsService.refundDenied(paramsMap);
			map.put("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家同意退款
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/agreeRefund")
	public Map<String, Object> agreeRefund(HttpServletRequest request, HttpServletResponse response) {
		// 用户id:openId  订单id:refundsOrderId  价格:price
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			boolean flag = refundsService.agreeRefund(paramsMap);
			map.put("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：确认订单
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/confirmReceipt")
	public Map<String, Object> confirmReceipt(HttpServletRequest request, HttpServletResponse response) {
		//修改订单状态   记录表  余额表
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			//获取订单ID
			Long orderId = (Long) paramsMap.get("orderId");
			Map<String, Object> order = refundsService.findOrderById(orderId);
			//商家分成比例
			BigDecimal serviceRatio = (BigDecimal) order.get("serviceRatio");
			//订单金额
			BigDecimal realityMoney = (BigDecimal) order.get("realityMoney");
			//商家分成金额
			BigDecimal saleMoney = realityMoney.multiply(serviceRatio);
			//商家用户openId
			String sellerOpenId = (String) order.get("sellerOpenId");
			//根据用户openId获取用户钱包信息
			Map<String, Object> mapp = refundsService.findWalletBalanceByOpenId(sellerOpenId);
			Double balance = 0d;
			if (mapp != null && mapp.size() > 0) {
				//当前账户余额
				balance = (Double) mapp.get("balance");
			}
			//累加后余额
			BigDecimal ban = BigDecimal.valueOf(balance);
			BigDecimal totalMoney = saleMoney.add(ban);
			Map<String, Object> drawMap = new HashMap<String, Object>();
			drawMap.put("openId", sellerOpenId);
			drawMap.put("divideOrWithdraw", saleMoney);
			drawMap.put("balanceMoney", totalMoney);
			drawMap.put("state", 0);
			drawMap.put("divideOrWithdrawTime", new Date());
			drawMap.put("orderId", orderId);
			//分成记录表插入数据start
			refundsService.divideOrWithDrawRecord(drawMap);
			//分成记录表插入数据end
			//钱包表插入或更新数据start
			refundsService.updateWallet(mapp, drawMap);
			//钱包表插入或更新数据end
			//修改订单状态start
			Map<String, Object> orderMap = new HashMap<String, Object>();
			orderMap.put("orderId", orderId);
			orderMap.put("state", 4);
			refundsService.updateOrderState(orderMap);
			//修改订单状态end
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 详情
	 * <p>
	 * jiangshengdong and liuyu update 20170711
	 * 
	 * @param map 
	 * @return
	 */
	@RequestMapping(value = "/details")
	public Object details(@RequestParam Map<String, Object> map, Model model) {
		model.addAttribute("refunds", refundsService.details(map).get(0));
		return "/manager/refunds/details";
	}
}
