package com.jzwl.xydk.manager.publishtype.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.upload.service.UploadImgService;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.publishtype.service.PublishTypeService;

@Controller
@RequestMapping("/publishtype")
public class PublishTypeController extends BaseWeixinController {

	@Autowired
	private PublishTypeService publishTypeService;
	@Autowired
	private UploadImgService uploadImgService;

	/**
	 * 项目名称：dlws-xiaoka-xydk 描述：列表页 创建人： sx 创建时间： 2017年5月2日 标记：
	 * 
	 * @param request
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "list")
	public String list(HttpServletRequest request, Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = publishTypeService.queryPublishforPage(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/publishtype/list";
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk 描述：跳转到新增页面 创建人： sx 创建时间： 2017年5月2日 标记：
	 * 
	 * @return
	 * @version
	 */
	@RequestMapping(value = "add")
	public String add(HttpServletRequest request, Model mov) {
		return "/manager/publishtype/add";
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk 描述：保存分类 创建人： sx 创建时间： 2017年5月2日 标记：
	 * 
	 * @param request
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "save")
	public String save(HttpServletRequest request, Model mov) {
		createParameterMap(request);
		boolean flag = true;
		try {
			flag = publishTypeService.save(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (flag) {
			return "redirect:/publishtype/list.html";
		} else {
			return "/error";
		}
	}

	@RequestMapping(value = "edit")
	public String edit(HttpServletRequest request, Model mov) {
		createParameterMap(request);
		try {
			Map<String, Object> map = publishTypeService.getData(paramsMap);
			mov.addAttribute("map", map);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "/manager/publishtype/edit";
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk 描述：修改 创建人： sx 创建时间： 2017年5月2日 标记：
	 * 
	 * @param request
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "update")
	public String update(HttpServletRequest request, Model mov) {
		createParameterMap(request);
		boolean flag = true;
		try {
			flag = publishTypeService.update(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (flag) {
			return "redirect:/publishtype/list.html";
		} else {
			return "/error";
		}
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk 描述：图片上传 创建人： sx 创建时间： 2017年5月2日 标记：
	 * 
	 * @param pic
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "uploadImg", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> uploadImg(
			@RequestParam(required = false) MultipartFile pic,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			int type = 2;
			String url = uploadImgService.uploadImg(type, pic, null);
			map.put("path", url);
			map.put("url", url);
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "uploadImg2", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> uploadImg2(
			@RequestParam(required = false) MultipartFile picselect,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			int type = 2;
			String selecturl = uploadImgService
					.uploadImg(type, picselect, null);// 返回全路径
			map.put("selectpath", selecturl);
			map.put("selecturl", selecturl);
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "uploadImg3", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> uploadImg3(
			@RequestParam(required = false) MultipartFile picnoselect,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			int type = 2;
			String noselecturl = uploadImgService.uploadImg(type, picnoselect,
					null);// 返回全路径
			map.put("noselectpath", noselecturl);
			map.put("noselecturl", noselecturl);
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
