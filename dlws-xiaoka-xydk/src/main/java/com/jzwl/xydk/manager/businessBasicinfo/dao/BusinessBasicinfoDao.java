/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.businessBasicinfo.dao;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;



@Repository("businessBasicinfoDao")
public class BusinessBasicinfoDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	
	public String getColumns() {
		return ""
				+" id as id,"
				+" openId as openId,"
				+" activityId as activityId,"
				+" startDate as startDate,"
				+" endDate as endDate,"
				+" cityId as cityId,"
				+" schoolId as schoolId,"
				+" levelId as levelId,"
				+" createDate as createDate,"
				+" outputIndex as outputIndex,"
				+" phone as phone,"
				+" remark as remark"
				;
	}
	

	public PageObject queryBusinessBasicinfoList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select " + getColumns() + " from `xiaoka-xydk`.business_basicinfo t where 1=1 ";
		
		  	if(null !=map.get("openId") && StringUtils.isNotEmpty(map.get("openId").toString())){
			  		sql=sql+ " and t.openId  = " + map.get("openId") +"";
		  	}
		  	if(null !=map.get("activityId") && StringUtils.isNotEmpty(map.get("activityId").toString())){
			  		sql=sql+ " and t.activityId  = " + map.get("activityId") +"";
		  	}
		  	if(null !=map.get("startDate") && StringUtils.isNotEmpty(map.get("startDate").toString())){
			  		sql=sql+ " and t.startDate >= " + map.get("startDateBegin") +"";
			  		sql=sql+ " and t.startDate <= " + map.get("startDateEnd"  ) +"";
		  	}
		  	if(null !=map.get("endDate") && StringUtils.isNotEmpty(map.get("endDate").toString())){
			  		sql=sql+ " and t.endDate >= " + map.get("endDateBegin") +"";
			  		sql=sql+ " and t.endDate <= " + map.get("endDateEnd"  ) +"";
		  	}
		  	if(null !=map.get("cityId") && StringUtils.isNotEmpty(map.get("cityId").toString())){
			  		sql=sql+ " and t.cityId  = " + map.get("cityId") +"";
		  	}
		  	if(null !=map.get("schoolId") && StringUtils.isNotEmpty(map.get("schoolId").toString())){
			  		sql=sql+ " and t.schoolId  = " + map.get("schoolId") +"";
		  	}
		  	if(null !=map.get("levelId") && StringUtils.isNotEmpty(map.get("levelId").toString())){
			  		sql=sql+ " and t.levelId  = " + map.get("levelId") +"";
		  	}
		  	if(null !=map.get("createDate") && StringUtils.isNotEmpty(map.get("createDate").toString())){
			  		sql=sql+ " and t.createDate >= " + map.get("createDateBegin") +"";
			  		sql=sql+ " and t.createDate <= " + map.get("createDateEnd"  ) +"";
		  	}
		  	if(null !=map.get("outputIndex") && StringUtils.isNotEmpty(map.get("outputIndex").toString())){
			  		sql=sql+ " and t.outputIndex  = " + map.get("outputIndex") +"";
		  	}
		  	if(null !=map.get("phone") && StringUtils.isNotEmpty(map.get("phone").toString())){
			  		sql=sql+ " and t.phone  = " + map.get("phone") +"";
		  	}
		  	if(null !=map.get("remark") && StringUtils.isNotEmpty(map.get("remark").toString())){
			  		sql=sql+ " and t.remark  = " + map.get("remark") +"";
		  	}
					
					
		sql=sql+ " order by id ";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}

	
	
	public String getColumns2() {
		return ""
				+" t.id as id,"
				+" t.openId as openId,"
				+" t.activityId as activityId,"
				+" t.startDate as startDate,"
				+" t.endDate as endDate,"
				+" t.cityId as cityId,"
				+" t.schoolId as schoolId,"
				+" t.levelId as levelId,"
				+" t.createDate as createDate,"
				+" t.outputIndex as outputIndex,"
				+" t.phone as phone,"
				+" t.remark as remark,"
				+" d.dic_name as activityName,"
				+" dd.dic_name as levelName,"
				+" c.cityName as cityName,"
				+" si.schoolName as schoolName"
				;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,Object> getById(Map<String, Object> map) {
		
		Map<String,Object> resMap = new HashMap<String,Object>();

		String sql = "select " + getColumns2() + " from `xiaoka-xydk`.business_basicinfo t"
				+ " left join `xiaoka`.v2_dic_data dd on dd.id = t.levelId"
				+ " left join `xiaoka`.v2_dic_data d on d.id = t.activityId "
				+ " LEFT JOIN `xiaoka`.tg_city c ON t.cityId = c.id "
				+ " LEFT JOIN `xiaoka`.tg_school_info si ON si.id = t.schoolId "
				+ "  where t.id = "+ map.get("id") + "";
		
		resMap = baseDAO.queryForMap(sql);
		
		return resMap;
	
	}
	

}
