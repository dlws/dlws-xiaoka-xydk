package com.jzwl.xydk.manager.dividewithdraw.pojo;

import java.io.Serializable;
import java.util.Date;

import com.jzwl.system.base.pojo.BasePojo;
/**
 * 
 * @author Administrator
 *提现或分成
 */
public class DivideDraw extends BasePojo implements Serializable{

	/**
	 * serialVersionUID .
	 */
	private static final long serialVersionUID = -8101396204446990608L;
	
	//alias
	public static final String TABLE_ALIAS = "Divideorwithdrawrecord";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_OPENID = "用户openId";
	public static final String ALIAS_DIVIDEOR_WITH_DRAW = "提现或分成金额";
	public static final String ALIAS_BALANCE_MONEY = "账户余额";
	public static final String ALIAS_STATE = "状态";
	public static final String ALIAS_DIVIDE_OR_WITH_DRAW_TIME = "时间";
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;

	private Long id;
	private String openId;
	private Double divideOrWithdraw;
	private Double balanceMoney;
	private int state;
	private Date divideOrWithdrawTime;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public Double getDivideOrWithdraw() {
		return divideOrWithdraw;
	}
	public void setDivideOrWithdraw(Double divideOrWithdraw) {
		this.divideOrWithdraw = divideOrWithdraw;
	}
	public Double getBalanceMoney() {
		return balanceMoney;
	}
	public void setBalanceMoney(Double balanceMoney) {
		this.balanceMoney = balanceMoney;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public Date getDivideOrWithdrawTime() {
		return divideOrWithdrawTime;
	}
	public void setDivideOrWithdrawTime(Date divideOrWithdrawTime) {
		this.divideOrWithdrawTime = divideOrWithdrawTime;
	}
	public String getDivideOrWithdrawTimeString() {
		return dateToStr(getDivideOrWithdrawTime(), FORMAT_CREATE_DATE);
	}
	public void setDivideOrWithdrawTimeString(String value) {
		setDivideOrWithdrawTime(strToDate(value, Date.class, FORMAT_CREATE_DATE));
	}
}
