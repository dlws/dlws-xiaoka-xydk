package com.jzwl.xydk.manager.acurateDelivery.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.acurateDelivery.dao.OrderJztfDao;

@Service("OrderJztfService")
public class OrderJztfService {
	@Autowired
	private OrderJztfDao orderJztfDao;
	public PageObject queryOrderJztfList(Map<String,Object> map){
		return orderJztfDao.queryOrderJztfList(map);
	}
	
	
	
	public boolean auditOrder(Map<String, Object> map) {
		return orderJztfDao.auditOrder(map);

	}
		
	
}
