package com.jzwl.xydk.manager.maxCategory.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.maxCategory.dao.MinCategoryDao;

@Service("minCategoryService")
public class MinCategoryService {
	@Autowired
	private MinCategoryDao minCategoryDao;
	
	
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：二级技能分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public PageObject minCategoryList(Map<String, Object> paramsMap) {
		
		return minCategoryDao.minCategoryList(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：添加二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean addMinCategory(Map<String, Object> paramsMap) {
		return minCategoryDao.addMinCategory(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去修改二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public Map<String, Object> toEditMinCategory(Map<String, Object> paramsMap) {
		return minCategoryDao.toEditMinCategory(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean editMinCategory(Map<String, Object> paramsMap) {
		return minCategoryDao.editMinCategory(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：删除二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean deleteMinCategory(Map<String, Object> paramsMap) {
		return minCategoryDao.deleteMinCategory(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取一级分类，不分页（新建二级分类使用）
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getMaxCategoryList(
			Map<String, Object> paramsMap) {
		
		return minCategoryDao.getMaxCategoryList(paramsMap);
	}
	
	
	
	
	
	
}
