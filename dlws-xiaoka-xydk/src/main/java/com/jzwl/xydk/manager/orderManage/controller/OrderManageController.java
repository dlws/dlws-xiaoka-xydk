package com.jzwl.xydk.manager.orderManage.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.orderManage.service.OrderManageService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.service.SkillOrderService;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.controller.FeedBackController;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.service.FeedBackService;

@Controller
@RequestMapping("/orderManage")
public class OrderManageController extends BaseController {
	@Autowired
	private OrderManageService orderManageService;
	@Autowired
	private SkillOrderService skillOrderService;
	@Autowired
	private FeedBackService feedBackService;
	@Autowired
	private FeedBackController feedbackContr;

	/**
	 * 订单列表
	 * dxf
	 * orderMangeList
	 */
	@RequestMapping(value = "/orderMangeList")
	public ModelAndView orderMangeList(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		PageObject po = new PageObject();
		try {

			po = orderManageService.orderMangeList(paramsMap);
			mov.addObject("paramsMap", paramsMap);
			mov.addObject("po", po);

		} catch (Exception e) {
			mov.addObject("msg", "查询订单列表失败！");
			mov.setViewName("/error");
			e.printStackTrace();
			return mov;
		}
		mov.setViewName("manager/orderManage/orderMangeList");
		return mov;
	}

	/**
	 * 订单详情
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/getOrderInfo")
	public String getOrderInfo(HttpServletRequest request, HttpServletResponse response, Model model, String orderId) {

		try {
			createParameterMap(request);
			Map<String, Object> objMap = orderManageService.getOrderInfo(paramsMap);
			List<Map<String, Object>> settleList = skillOrderService.querySettleList(paramsMap);//查询子表详细信息
			//当前最新反馈记录
			model = feedbackContr.feedBackCommon(paramsMap, model);
			//历史信息反馈记录 暂时放置

			model.addAttribute("settleList", settleList);

			Map<String, Object> skillInfo = new HashMap<String, Object>();
			for (int i = 0; i < settleList.size(); i++) {
				if (settleList.size() > 0) {

					if ("skillId".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						String skillId = String.valueOf(settleList.get(i).get("typeValue"));//技能Id
						skillInfo.put("skillId", skillId);
					}
					if ("payMoney".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						String payMoney = String.valueOf(settleList.get(i).get("typeValue"));//付款金额
						skillInfo.put("payMoney", payMoney);
					}
					if ("skillName".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						String skillName = String.valueOf(settleList.get(i).get("typeValue"));//技能名称
						skillInfo.put("skillName", skillName);
					}
					if ("serviceType".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						String serviceType = String.valueOf(settleList.get(i).get("typeValue"));//服务类型
						skillInfo.put("serviceType", serviceType);
					}
					if ("skillPrice".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						String skillPrice = String.valueOf(settleList.get(i).get("typeValue"));//单价
						if ("null".equals(skillPrice) || skillPrice == null) {
							skillPrice = "0";
						}
						skillInfo.put("skillPrice", skillPrice);
					}
					if ("tranNumber".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						String tranNumber = String.valueOf(settleList.get(i).get("typeValue"));//购买数量
						skillInfo.put("tranNumber", tranNumber);
					}
				}
			}
			//表示占位
			model.addAttribute("sellerMessage", new HashMap<String, Object>());
			if (skillInfo.containsKey("skillId")) {
				String skillId = skillInfo.get("skillId").toString();
				searchUserBySkillId(skillId, model);
			} else {
				System.out.println("--------当前订单系统未存入SkillId---------" + settleList.get(0).get("orderId"));
				;
			}

			Map<String, Object> seller = orderManageService.searchUserByOpenId(orderId);
			model.addAttribute("seller", seller);

			model.addAttribute("objMap", objMap);
			model.addAttribute("skillInfo", skillInfo);

			return "/manager/orderManage/orderManageDetail";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 根据当前获取SkillId 获取当前的用户信息放到Model中
	 * @param map
	 * @param model
	 * @return
	 */
	public Model searchUserBySkillId(String skillId, Model model) {
		Map<String, Object> sellerMessage = orderManageService.searchUserBySkillId(skillId);//查询子表详细信息
		model.addAttribute("sellerMessage", sellerMessage);
		return model;
	}

}
