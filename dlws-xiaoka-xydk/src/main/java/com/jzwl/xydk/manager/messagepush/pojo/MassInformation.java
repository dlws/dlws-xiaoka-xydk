package com.jzwl.xydk.manager.messagepush.pojo;

import java.util.Date;

import com.jzwl.system.base.pojo.BasePojo;



/**
 * 消息管理类POJO
 * @author Administrator
 *
 */
public class MassInformation extends BasePojo implements java.io.Serializable{

	/**
	 * 便于序列化
	 */
	private static final long serialVersionUID = 1L;
	
	  private Long id;//id
	  private String title;//标题
	  private String content;//通知内容
	  private Date createDate;//创建时间
	  private String remark;//备注
	  private int isDelete; //是否删除
	  private int isSend; //是否发送
	  
	/**
	 * 带参构造器  
	 * @param id
	 * @param title
	 * @param content
	 * @param createDate
	 * @param remark
	 * @param isDelete
	 * @param isSend
	 */
	public MassInformation(long id, String title, String content,
			Date createDate, String remark, int isDelete, int isSend) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.createDate = createDate;
		this.remark = remark;
		this.isDelete = isDelete;
		this.isSend = isSend;
	}
	
	/**
	 * 无参数构造器
	 */
	public MassInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public int getIsSend() {
		return isSend;
	}

	public void setIsSend(int isSend) {
		this.isSend = isSend;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
