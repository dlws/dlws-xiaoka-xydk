package com.jzwl.xydk.manager.user.userBasicinfo.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.manager.user.userBasicinfo.pojo.UserSkillinfoData;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassS;

@Repository("userEnterInfoDao")
public class UserEnterInfoDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	/**
	 * 增加关于场地描述和一些自定义属性的参数
	 * typeName:为字典表中的对应CODE
	 * typeValue:直接存入回传回来的值
	 */
	public boolean addEnertInfo(Map<String, Object> map, String basId) {

		String sql = "insert into `xiaoka-xydk`.user_skillinfo_data(id,pid,typeName,typeValue,isDelete,createDate)"
				+ "values" + "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";

		for (Entry<String, Object> entry : map.entrySet()) {
			Map<String, Object> inserMap = new HashMap<String, Object>();
			inserMap.put("id", Sequence.nextId());
			inserMap.put("pid", basId);
			inserMap.put("typeName", entry.getKey());
			inserMap.put("typeValue", entry.getValue());
			inserMap.put("isDelete", 0);
			inserMap.put("createDate", new Date());
			baseDAO.executeNamedCommand(sql, inserMap);
		}
		return false;
	}

	/**
	 * cfz  以更改查询个性化字段的方式
	 * @param map
	 * @return List
	 */
	public List<UserSkillinfoData> querySettleListVersion3(Map<String, Object> map) {
		String sql = "SELECT id,pid,typeName,typeValue,isDelete,createDate FROM `xiaoka-xydk`.user_skillinfo_data"
				+" WHERE pid='"+map.get("id")+"'";
		List<UserSkillinfoData> list = baseDAO.getJdbcTemplate().query(sql, new Object[]{},ParameterizedBeanPropertyRowMapper.newInstance(UserSkillinfoData.class));
		return list;

	}

}
