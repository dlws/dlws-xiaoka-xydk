package com.jzwl.xydk.manager.publishtype.dao;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;
@Repository
public class PublishTypeDao {
	
	@Autowired
	private BaseDAO baseDAO;

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：分页查询
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param map
	 * @return
	 * @version
	 */
	public PageObject queryPublishforPage(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, className, classValue, imageUrl, selectimage, noSelectimage, status, ord, createDate, createUser from "
				+ " `xiaoka-xydk`.user_skillclass_father where isDelete = '0' and systemType='1' ");
		
		sb.append(" order by ord");
		PageObject po = baseDAO.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：保存分类
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean save(Map<String, Object> paramsMap) {
		StringBuffer sb = new StringBuffer();
		paramsMap.put("id", Sequence.nextId());
		paramsMap.put("createDate", new Date());
		sb.append("insert into `xiaoka-xydk`.user_skillclass_father "
				+ "(id,className,imageURL,selectimage,noSelectimage,status,ord,createDate,classValue,isDelete,systemType)  "
				+ "values  (:id,:className,:imageURL,:selectimage,:noSelectimage,:status,:ord,:createDate,:classValue,0,1)");
		return baseDAO.executeNamedCommand(sb.toString(), paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：编辑时查询数据
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getData(Map<String, Object> paramsMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, className, classValue, imageURL, selectimage, noSelectimage, status, ord from `xiaoka-xydk`.user_skillclass_father where id=:id");
		return baseDAO.queryForMap(sb.toString(), paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改保存
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean update(Map<String, Object> paramsMap) {
		StringBuffer sb = new StringBuffer();
		sb.append(" update `xiaoka-xydk`.user_skillclass_father set "
				+ " className=:className, imageURL=:imageURL, selectimage=:selectimage, noSelectimage=:noSelectimage, "
				+ "	ord=:ord, classValue=:classValue, status=:status "
				+ " where id=:id");
		return baseDAO.executeNamedCommand(sb.toString(), paramsMap);
	}
}
