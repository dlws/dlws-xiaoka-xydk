/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.user.userskillinfo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.user.userskillinfo.dao.UserSkillinfoDao;

@Service("userSkillinfoService")
public class UserSkillinfoService {

	@Autowired
	private UserSkillinfoDao userSkillinfoDao;

	public String addUserSkillinfo(Map<String, Object> map) {

		return userSkillinfoDao.addUserSkillinfo(map);

	}

	public PageObject queryUserSkillinfoList(Map<String, Object> map) {

		return userSkillinfoDao.queryUserSkillinfoList(map);

	}

	public boolean updateUserSkillinfo(Map<String, Object> map) {

		return userSkillinfoDao.updateUserSkillinfo(map);

	}

	public boolean deleteUserSkillinfo(Map<String, Object> map) {

		return userSkillinfoDao.deleteUserSkillinfo(map);

	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return userSkillinfoDao.getById(map);

	}

	public List<Map<String, Object>> getClassF() {

		return userSkillinfoDao.getClassF();
	}

	public List<Map<String, Object>> getClassS() {

		return userSkillinfoDao.getClassS();
	}

	public List<Map<String, Object>> getImages(String id) {

		return userSkillinfoDao.getImages(id);
	}

	public List getClassSBySid(Map<String, Object> map) {

		return userSkillinfoDao.getClassSBySid(map);
	}

	public boolean addImage(String answer, String basicId) {

		return userSkillinfoDao.addImage(answer, basicId);

	}

	public boolean deleteImage(Map<String, Object> map) {
		return userSkillinfoDao.deleteImage(map);
	}

	public PageObject querySkillinfoList(Map<String, Object> map) {

		return userSkillinfoDao.querySkillinfoList(map);

	}

	public boolean updateHome(Map<String, Object> paramsMap) {

		// TODO Auto-generated method stub
		return userSkillinfoDao.updateHome(paramsMap);

	}

	public List<Map<String, Object>> canpanyTypes() {

		return userSkillinfoDao.canpanyTypes();

	}

	public List<Map<String, Object>> getCanpanyMap() {

		// TODO Auto-generated method stub
		return userSkillinfoDao.getCanpanyMap();

	}

	public List<Map<String, Object>> queryClassSonByFatherId(String skillFatId) {

		// TODO Auto-generated method stub
		return userSkillinfoDao.queryClassSonByFatherId(skillFatId);

	}

	public List<Map<String, Object>> getClassSon(Map<String, Object> paramsMap) {

		// TODO Auto-generated method stub
		return userSkillinfoDao.getClassSon(paramsMap);

	}

}
