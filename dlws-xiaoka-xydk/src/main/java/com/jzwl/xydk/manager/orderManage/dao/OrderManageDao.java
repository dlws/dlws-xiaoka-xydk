package com.jzwl.xydk.manager.orderManage.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("OrderManageDao")
public class OrderManageDao {

	Log log = LogFactory.getLog(getClass());

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	public PageObject orderMangeList(Map<String, Object> map) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT od.orderId,od.linkman,od.phone,od.realityMoney,od.payTime,od.orderStatus,od.enterType,createDate,");
		sql.append(" (select typeValue from `xiaoka-xydk`.order_detail odt where typeName='sellerNickname' and odt.orderId=od.orderId)sellerNickName ");
		sql.append(" FROM `xiaoka-xydk`.`order` od LEFT JOIN tg_school_info shl ON od.cityId=shl.id ");
		sql.append(" LEFT JOIN tg_city city ON od.cityId=city.id  where 1=1");

		if (null != map.get("beginDate") && StringUtils.isNotEmpty(map.get("beginDate").toString())) {
			sql.append(" and  DATE_FORMAT(od.createDate,'%Y-%m-%d') >= DATE_FORMAT('" + map.get("beginDate")
					+ "','%Y-%m-%d')");
		}
		if (null != map.get("endDate") && StringUtils.isNotEmpty(map.get("endDate").toString())) {
			sql.append(" and  DATE_FORMAT(od.createDate,'%Y-%m-%d') <= DATE_FORMAT('" + map.get("endDate")
					+ "','%Y-%m-%d')");
		}
		if (null != map.get("orderStatus") && StringUtils.isNotEmpty(map.get("orderStatus").toString())) {
			sql.append(" and  od.orderStatus=" + map.get("orderStatus") + "");
		}
		if (null != map.get("orderId") && StringUtils.isNotEmpty(map.get("orderId").toString())) {
			sql.append(" and  od.orderId like  '%" + map.get("orderId") + "%' ");
		}
		sql.append(" order by od.createDate desc");
		return baseDAO.queryForMPageList(sql.toString(), new Object[] {}, map);
	}

	//查看订单详情
	/**
	 * （查询买家信息）
	 * 根据当前传入的orderId查询当前用户信息
	 * @return
	 */
	public Map<String, Object> getOrderInfo(Map<String, Object> map) {
		String jzzx = GlobalConstant.ENTER_TYPE_JZZX;
		String jztf = GlobalConstant.ENTER_TYPE_JZTF;
		Map<String, Object> resMap = new HashMap<String, Object>();
		String sql = "";
		/*if(jzzx.equals(map.get("enterType"))||jztf.equals(map.get("enterType"))){
			sql+="SELECT od.orderId,city.cityName,shl.schoolName,od.realityMoney,od.payTime,od.orderStatus,od.enterType,bas.nickname "
				+" FROM `xiaoka-xydk`.`order` od LEFT JOIN tg_school_info shl ON od.cityId=shl.id"
				+" LEFT JOIN tg_city city ON od.cityId=city.id "
				+" LEFT JOIN `xiaoka-xydk`.user_basicinfo bas ON od.openId=bas.openId"
				+" WHERE orderId='"+map.get("orderId")+"'";
		}else*/

		if ("0".equals(map.get("openId"))) {
			sql += "SELECT od.orderId,city.cityName,shl.schoolName,od.realityMoney,od.payTime,od.orderStatus,od.sellerOpenId,"
					+ "(SELECT nickname from `xiaoka-xydk`.user_basicinfo "
					+ " WHERE id=(SELECT basicId FROM `xiaoka-xydk`.user_skillinfo "
					+ " WHERE id=(SELECT typeValue FROM `xiaoka-xydk`.order_detail WHERE typeName='skillId' AND orderId=od.orderId)))nickname"
					+ " FROM `xiaoka-xydk`.`order` od LEFT JOIN tg_school_info shl ON od.cityId=shl.id"
					+ " LEFT JOIN tg_city city ON od.cityId=city.id" + " WHERE orderId='" + map.get("orderId") + "'";
		} else {
			sql += "SELECT od.orderId,city.cityName,shl.schoolName,od.realityMoney,od.payTime,od.orderStatus,od.enterType,"
					+ "od.linkman,od.phone,od.message,bas.nickname "
					+ " FROM `xiaoka-xydk`.`order` od LEFT JOIN tg_school_info shl ON od.cityId=shl.id"
					+ " LEFT JOIN tg_city city ON od.cityId=city.id "
					+ " LEFT JOIN `xiaoka-xydk`.user_basicinfo bas ON od.openId=bas.openId"
					+ " WHERE orderId='"
					+ map.get("orderId") + "'";
		}
		resMap = baseDAO.queryForMap(sql);
		return resMap;
	}

	public Map<String, Object> getOrderInfo(String orderId) {

		String sql = "SELECT od.orderId,city.cityName,shl.schoolName,od.realityMoney,od.payTime,od.orderStatus,od.enterType,"
				+ "od.linkman,od.phone,od.message,bas.nickname "
				+ " FROM `xiaoka-xydk`.`order` od LEFT JOIN tg_school_info shl ON od.cityId=shl.id"
				+ " LEFT JOIN tg_city city ON od.cityId=city.id "
				+ " LEFT JOIN `xiaoka-xydk`.user_basicinfo bas ON od.openId=bas.openId"
				+ " WHERE orderId='"
				+ orderId
				+ "'";
		Map<String, Object> resMap = baseDAO.queryForMap(sql);
		return resMap;
	}

	public Map<String, Object> searchUserBySkillId(String skillId) {

		if (skillId == null) {
			log.error("-------调用查询当前传入参数为空--------");
			return null;
		}

		String sql = "select ub.userName,ub.phoneNumber,ub.email,shl.schoolName,city.cityName FROM `xiaoka-xydk`.user_basicinfo ub "
				+ "LEFT JOIN `xiaoka`.tg_school_info shl ON ub.cityId=shl.id "
				+ "LEFT JOIN `xiaoka`.tg_city city ON ub.cityId=city.id "
				+ "where ub.id=(select distinct(t.basicId) from `xiaoka-xydk`.user_skillinfo t where id='"
				+ skillId
				+ "')";

		Map<String, Object> resMap = baseDAO.queryForMap(sql);
		return resMap;

	}

	/**
	 * （查询卖家信息）
	 * 根据当前传入的orderId查询当前用户信息
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> searchUserByOpenId(String orderId) {
		String sql = "SELECT od.orderId,shl.schoolName,city.cityName,bb.phone,us.serviceType,us.skillName,"
				+ "(select typeValue from `xiaoka-xydk`.order_detail odt where typeName='sellerNickname' and odt.orderId=od.orderId)sellerNickName "
				+ "FROM `xiaoka-xydk`.`order` od LEFT JOIN `xiaoka`.tg_school_info shl ON od.cityId=shl.id  LEFT JOIN `xiaoka-xydk`.business_basicinfo bb on od.openId=bb.openId "
				+ "LEFT JOIN `xiaoka-xydk`.user_basicinfo ub ON od.openId=ub.openId "
				+ "LEFT JOIN `xiaoka-xydk`.user_skillinfo us on ub.id=us.basicId "
				+ "LEFT JOIN `xiaoka`.tg_city city ON od.cityId=city.id WHERE orderId='" + orderId + "'";
		Map<String, Object> resMap = baseDAO.queryForMap(sql);
		return resMap;

	}
}
