/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.label.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class Label extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "Label";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_ENTER_ID = "入住类型的id";
	public static final String ALIAS_LABEL_NAME = "标签名称";
	public static final String ALIAS_ORD = "排列顺序";
	public static final String ALIAS_IS_DELETE = "是否删除";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	
	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	

	//columns START
    /**
     * id       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 入住类型的id       db_column: enterId 
     */ 	
	private java.lang.String enterId;
    /**
     * 标签名称       db_column: labelName 
     */ 	
	private java.lang.String labelName;
    /**
     * 排列顺序       db_column: ord 
     */ 	
	private java.lang.Integer ord;
    /**
     * 是否删除       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
    /**
     * 创建时间       db_column: createDate 
     */ 	
	private java.util.Date createDate;
	//columns END

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	
	public java.lang.String getEnterId() {
		return this.enterId;
	}
	
	public void setEnterId(java.lang.String value) {
		this.enterId = value;
	}
	

	public java.lang.String getLabelName() {
		return this.labelName;
	}
	
	public void setLabelName(java.lang.String value) {
		this.labelName = value;
	}
	

	public java.lang.Integer getOrd() {
		return this.ord;
	}
	
	public void setOrd(java.lang.Integer value) {
		this.ord = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}
	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class,FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

}

