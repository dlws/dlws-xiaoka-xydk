/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.preferService.pojo;

import com.jzwl.system.base.pojo.BasePojo;

public class PreferredService extends BasePojo implements java.io.Serializable {

	private static final long serialVersionUID = 5454155825314635342L;

	//alias
	public static final String TABLE_ALIAS = "PreferredService";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_ENTER_CLASS = "入驻类型";
	public static final String ALIAS_CLASS_NAME = "类型名字";
	public static final String ALIAS_SERVICE_NAME = "服务名称";
	public static final String ALIAS_INTRODUCE = "服务介绍";
	public static final String ALIAS_PRICE = "价格";
	public static final String ALIAS_SERVICE_TYPE = "0:线上；1：线下";
	public static final String ALIAS_SALES_VOLUME = "销售量";
	public static final String ALIAS_IMG_URL = "图片路径";
	public static final String ALIAS_IS_DELETE = "是否删除";
	public static final String ALIAS_CREATE_DATE = "创建时间";

	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;

	//columns START
	/**
	 * id       db_column: id 
	 */
	private java.lang.Long id;
	/**
	 * 入驻类型       db_column: enterClass 
	 */
	private java.lang.Long enterClass;
	/**
	 * 类型名字       db_column: className 
	 */
	private java.lang.String className;
	/**
	 * 服务名称       db_column: serviceName 
	 */
	private java.lang.String serviceName;
	/**
	 * 服务介绍       db_column: introduce 
	 */
	private java.lang.String introduce;
	/**
	 * 价格       db_column: price 
	 */
	private java.lang.Double price;
	/**
	 * 0:线上；1：线下       db_column: serviceType 
	 */
	private java.lang.Integer serviceType;
	/**
	 * 销售量       db_column: salesVolume 
	 */
	private java.lang.Integer salesVolume;
	/**
	 * 图片路径       db_column: imgUrl 
	 */
	private java.lang.String imgUrl;
	/**
	 * 是否删除       db_column: isDelete 
	 */
	private java.lang.Integer isDelete;
	/**
	 * 创建时间       db_column: createDate 
	 */
	private java.util.Date createDate;

	//columns END

	public void setId(java.lang.Long value) {
		this.id = value;
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public java.lang.Long getEnterClass() {
		return this.enterClass;
	}

	public void setEnterClass(java.lang.Long value) {
		this.enterClass = value;
	}

	public java.lang.String getClassName() {
		return this.className;
	}

	public void setClassName(java.lang.String value) {
		this.className = value;
	}

	public java.lang.String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(java.lang.String value) {
		this.serviceName = value;
	}

	public java.lang.String getIntroduce() {
		return this.introduce;
	}

	public void setIntroduce(java.lang.String value) {
		this.introduce = value;
	}

	public java.lang.Double getPrice() {
		return this.price;
	}

	public void setPrice(java.lang.Double value) {
		this.price = value;
	}

	public java.lang.Integer getServiceType() {
		return this.serviceType;
	}

	public void setServiceType(java.lang.Integer value) {
		this.serviceType = value;
	}

	public java.lang.Integer getSalesVolume() {
		return this.salesVolume;
	}

	public void setSalesVolume(java.lang.Integer value) {
		this.salesVolume = value;
	}

	public java.lang.String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(java.lang.String value) {
		this.imgUrl = value;
	}

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}

	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class, FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}

}
