/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.link.pojo;

import java.util.Date;

import com.jzwl.system.base.pojo.BasePojo;

public class LinkEntity extends BasePojo {

	private static final long serialVersionUID = 5454155825314635342L;

	/**
	 * 主键id
	 */
	private long id;

	/**
	 * 链接名称
	 */
	private String linkName;

	/**
	 * 链接值
	 */
	private String linkValue;

	/**   
	 *  创建日期   
	 */
	private Date createDate;

	/**
	 * 是否启用任务（0：否，1：是） 
	 */
	private boolean isUse;

	/**
	 * 是否删除（0：否，1：是）      
	 */
	private boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getLinkValue() {
		return linkValue;
	}

	public void setLinkValue(String linkValue) {
		this.linkValue = linkValue;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public boolean isUse() {
		return isUse;
	}

	public void setUse(boolean isUse) {
		this.isUse = isUse;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
