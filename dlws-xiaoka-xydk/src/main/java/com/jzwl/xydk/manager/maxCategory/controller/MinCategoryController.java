package com.jzwl.xydk.manager.maxCategory.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.Constants;
import com.jzwl.common.log.SystemLog;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.maxCategory.service.MinCategoryService;
@Controller
@RequestMapping("/minCategory")
public class MinCategoryController extends BaseController{
	@Autowired
	private SystemLog systemLog;
	
	@Autowired
	private Constants constants;
	
	@Autowired
	private MinCategoryService minCategoryService;
	
	private static Logger log = LoggerFactory.getLogger(MinCategoryController.class);
	
	
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：一级技能分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/minCategoryList")
	public String minCategoryList(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			PageObject po = minCategoryService.minCategoryList(paramsMap);
			List<Map<String,Object>> list  =  minCategoryService.getMaxCategoryList(paramsMap);
			model.addAttribute("list",list);
			model.addAttribute("po",po);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "manager/maxCategory/minCategoryList";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去添加二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toAddMinCategory")
	public String toAddMinCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			List<Map<String,Object>> list  =  minCategoryService.getMaxCategoryList(paramsMap);
			model.addAttribute("list",list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "manager/maxCategory/minCategoryForm";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：添加二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/addMinCategory")
	public String addMinCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			boolean flag = minCategoryService.addMinCategory(paramsMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/minCategory/minCategoryList.html";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去修改二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toEditMinCategory")
	public String toEditMinCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			
			Map<String,Object> map = minCategoryService.toEditMinCategory(paramsMap);
			List<Map<String,Object>> list  =  minCategoryService.getMaxCategoryList(paramsMap);
			model.addAttribute("list",list);
			model.addAttribute("map", map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "manager/maxCategory/minCategoryForm";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/editMinCategory")
	public String editMinCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			boolean flag = minCategoryService.editMinCategory(paramsMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/minCategory/minCategoryList.html";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：删除二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/deleteMinCategory")
	public @ResponseBody Map deleteMinCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		Map<String,Object> map =  new HashMap<String, Object>();
		try {
			boolean flag = minCategoryService.deleteMinCategory(paramsMap);
			map.put("flag", flag);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	
	
}
