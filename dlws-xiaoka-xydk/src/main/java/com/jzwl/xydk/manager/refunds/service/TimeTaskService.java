package com.jzwl.xydk.manager.refunds.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.utils.DateUtil;
import com.jzwl.xydk.manager.refunds.dao.RefundsDao;

@Service("timeTaskService")
public class TimeTaskService {

	@Autowired
	private RefundsDao refundsDao;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：自动确认收货
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @version
	 */
	public void serviceOnCompleteWithNoConfirm() {
		try {
			//获取已接单用户未确认收货的订单
			List<Map<String, Object>> list = refundsDao.findListForOrderState();
			if(list != null && list.size()>0){
				for (Map<String, Object> map : list) {
					Long orderId = (Long)map.get("orderId");
					Date endDate = (Date)map.get("endDate");
					//七天后的日期
					Date sevenDate = DateUtil.addDay(endDate, 7);
					String sevenDateStr = DateUtil.dateToStr(sevenDate,"yyyyMMddHHmmss");
					//获取数据库当前时间
					Date nowDate = new Date();
					String nowDateStr = DateUtil.dateToStr(nowDate,"yyyyMMddHHmmss");
					System.out.println("sevenDate"+sevenDate);
					System.out.println("nowDate"+nowDate);
					int days = DateUtil.daysBetween(nowDateStr, sevenDateStr,"yyyyMMddHHmmss");
					//订单日期超时表示可以自动确定订单了
					if(days <= 0){
						Map<String, Object> orderMap = refundsDao.findfindOrderById(orderId);
						//商家分成比例
						BigDecimal serviceRatio = (BigDecimal)orderMap.get("serviceRatio");
						//订单金额
						BigDecimal realityMoney = (BigDecimal)orderMap.get("payMoney");
						//商家分成金额
						BigDecimal saleMoney = realityMoney.multiply(serviceRatio);
						//商家用户openId
						String sellerOpenId = (String)orderMap.get("sellerOpenId");
						//根据用户openId获取用户钱包信息
						Map<String, Object> mapp = refundsDao.findWalletBalanceByOpenId(sellerOpenId);
						Double balance = 0d;
						if(mapp != null && mapp.size()>0){
							//当前账户余额
							balance = (Double)mapp.get("balance");
						}
						//累加后余额
						BigDecimal ban = BigDecimal.valueOf(balance);
						BigDecimal totalMoney = saleMoney.add(ban);
						Map<String, Object> drawMap = new HashMap<String, Object>();
						drawMap.put("openId", sellerOpenId);
						drawMap.put("divideOrWithdraw", saleMoney);
						drawMap.put("balanceMoney", totalMoney);
						drawMap.put("state", 0);
						drawMap.put("divideOrWithdrawTime", new Date());
						drawMap.put("orderId", orderId);
						//分成记录表插入数据start
						refundsDao.divideOrWithDrawRecord(drawMap);
						//分成记录表插入数据end
						//钱包表插入或更新数据start
						refundsDao.updateWallet(mapp, drawMap);
						//钱包表插入或更新数据end
						//修改订单状态start
						Map<String, Object> order = new HashMap<String, Object>();
						order.put("orderId", orderId);
						order.put("state", 4);
						refundsDao.updateOrderState(order);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
