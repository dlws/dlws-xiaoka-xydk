package com.jzwl.xydk.manager.messagepush.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.messagepush.service.MessagePushService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;

/**
 * 
 * @author Administrator 消息管理类，主要用于管理消息的推送
 * 
 */

@Controller
@RequestMapping("/messagePush")
public class MessagePushController extends BaseController {

	@Autowired
	MessagePushService messagePushService;
	@Autowired
	private ChatMessageService chatMessageService;

	/**
	 * 推送消息信息展示
	 */
	@RequestMapping(value = "/toShowMess")
	public String toShowMess(HttpServletRequest request, HttpServletResponse response, Model mov) {

		// 暂时不添加限定条件
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = messagePushService.queryMessforPage(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);

		return "/manager/mass_information/mass_info_list";
	}

	/**
	 * 增加消息的推送
	 */
	@RequestMapping(value = "/addMessinfo")
	public String addMessinfo(HttpServletRequest request, HttpServletResponse response, Model mov) {

		boolean flage = true;
		// 获取页面传入的基础参数
		createParameterMap(request);
		// 基础参数校验
		if (checkParm(paramsMap)) {
			// 调用添加方法
			flage = messagePushService.addMessage(paramsMap);
		} else {
			// 返回提示信息
			mov.addAttribute("message", "推送信息不符合要求");
		}
		if (!flage) {
			mov.addAttribute("msg", "添加推送信息失败！");
			return "/error";
		}
		if (paramsMap.get("start") == null) {
			paramsMap.put("start", 0);
		}
		return "redirect:/messagePush/toShowMess.html?start=" + paramsMap.get("start");
	}

	/**
	 * 页面跳转到添加推送消息页
	 */
	@RequestMapping(value = "/toAddMessinfo")
	public String toAddMessinfo(HttpServletRequest request, HttpServletResponse response, Model mov) {
		createParameterMap(request);
		mov.addAttribute("start", paramsMap.get("start"));
		// 暂时无需要加载的基础参数 只作为跳转
		return "/manager/mass_information/mass_info_add";
	}

	/**
	 * 页面跳转修改推送消息页
	 */
	@RequestMapping(value = "/toUpdateMessinfo")
	public String toUpdateMessinfo(HttpServletRequest request, HttpServletResponse response, Model mov) {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		createParameterMap(request);
		// 获取当前推送信息中所需要的参数
		if (paramsMap.get("id") != null && StringUtils.isNotBlank(paramsMap.get("id").toString())) {
			list = messagePushService.queryMessforList(paramsMap);
			if (list.size() == 0) {
				mov.addAttribute("msg", "未查询到当前消息的相关讯息");
				return "/error";
			}
		} else {
			mov.addAttribute("msg", "未查询到当前消息的相关讯息");
			return "/error";
		}
		mov.addAttribute("message", list.get(0));
		mov.addAttribute("start", paramsMap.get("start"));
		// 暂时无需要加载的基础参数 只作为跳转
		return "/manager/mass_information/mass_info_edite";
	}

	/**
	 * 页面跳转修改推送内容
	 */
	@RequestMapping(value = "/updateMessinfo")
	public String updateMessinfo(HttpServletRequest request, HttpServletResponse response, Model mov) {
		boolean flage = true;
		// 获取页面传入的基础参数
		createParameterMap(request);
		// 基础参数校验
		if (checkParm(paramsMap)) {
			// 调用添加方法
			flage = messagePushService.updateMessage(paramsMap);
		} else {
			// 返回提示信息
			mov.addAttribute("message", "推送信息不符合要求");
		}
		if (!flage) {
			mov.addAttribute("msg", "添加推送信息失败！");
			return "/error";
		}
		if (paramsMap.get("start") == null) {
			paramsMap.put("start", 0);
		}
		return "redirect:/messagePush/toShowMess.html?start=" + paramsMap.get("start");
	}

	/**
	 * 根据Id值删除当前指定用户
	 */
	@RequestMapping(value = "/deleteMessinfo")
	public String deleteMessinfo(HttpServletRequest request, HttpServletResponse response, Model mov) {

		boolean flage;
		createParameterMap(request);
		// 获取当前推送信息中所需要的参数
		if (paramsMap.get("id") != null && StringUtils.isNotBlank(paramsMap.get("id").toString())) {
			// 根据ID值删除当前的用户
			flage = messagePushService.deleteMessage(paramsMap);
		} else {
			mov.addAttribute("msg", "未查询到当前消息的相关讯息");
			return "/error";
		}
		mov.addAttribute("start", paramsMap.get("start"));
		// 暂时无需要加载的基础参数 只作为跳转
		if (!flage) {
			mov.addAttribute("msg", "添加推送信息失败！");
			return "/error";
		}
		if (paramsMap.get("start") == null) {
			paramsMap.put("start", 0);
		}

		return "redirect:/messagePush/toShowMess.html?start=" + paramsMap.get("start");

	}

	/**
	 * 校验参数是否符合添加到数据库
	 */
	public boolean checkParm(Map<String, Object> parm) {

		// 参数是否通过（默认为是）
		boolean flage = false;
		if (parm.get("title") == null || StringUtils.isBlank(parm.get("title").toString())) {
			return false;
		}
		if (parm.get("content") == null || StringUtils.isBlank(parm.get("content").toString())) {
			return false;
		}
		if (parm.get("remark") == null || StringUtils.isBlank(parm.get("remark").toString())) {
			parm.put("remark", "");// 若remark 没有值则赋值为空
		}

		String title = parm.get("title").toString();
		String content = parm.get("content").toString();
		String remark = parm.get("remark").toString();

		if (StringUtils.isNotBlank(title) && StringUtils.isNotBlank(content)) {
			if (title.length() < 50 && content.length() < 255 && remark.length() < 255) {
				flage = true;
			}
		}
		return flage;
	}

	/**
	 * 根据Id值将信息推送到所有的用户
	 */
	@RequestMapping(value = "/sendMessinfo")
	public String sendMessinfo(HttpServletRequest request, HttpServletResponse response, Model mov) {

		boolean flage;
		createParameterMap(request);
		// 获取当前推送信息中所需要的参数
		if (paramsMap.get("id") != null && StringUtils.isNotBlank(paramsMap.get("id").toString())) {
			// 根据ID值删除当前的用户
			flage = messagePushService.sendMessage(paramsMap);

			// 获取要更新的信息。
			Map<String, Object> map = messagePushService.getMassInformation(paramsMap);

			chatMessageService.insertInfoByMass(map);
			/*if (flage) {// 推送发送的信息
				sendMessage.sendMassTemplateMessage(map);
			}*/

		} else {
			mov.addAttribute("msg", "未查询到当前消息的相关讯息");
			return "/error";
		}
		mov.addAttribute("start", paramsMap.get("start"));
		if (!flage) {
			mov.addAttribute("msg", "添加推送信息失败！");
			return "/error";
		}
		if (paramsMap.get("start") == null) {
			paramsMap.put("start", 0);
		}

		// 暂时无需要加载的基础参数 只作为跳转
		return "redirect:/messagePush/toShowMess.html?start=" + paramsMap.get("start");

	}
}
