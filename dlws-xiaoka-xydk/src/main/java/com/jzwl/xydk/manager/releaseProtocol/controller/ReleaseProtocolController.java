/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.releaseProtocol.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.releaseProtocol.service.ReleaseProtocolService;

/**
 * ReleaseProtocol
 * ReleaseProtocol
 * releaseProtocol
 * <p>Title:ReleaseProtocolController </p>
 * 	<p>Description:ReleaseProtocol </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller
@RequestMapping("/releaseProtocol")
public class ReleaseProtocolController extends BaseController {

	@Autowired
	private ReleaseProtocolService releaseProtocolService;

	/**
	 * 跳转到添加ReleaseProtocol的页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddReleaseProtocol")
	public ModelAndView toAddreleaseProtocol(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);
		List<Map<String, Object>> releaseTypes = releaseProtocolService.releaseTypes();
		mov.addObject("releaseTypes", releaseTypes);
		mov.setViewName("/manager/releaseProtocol/add");
		return mov;
	}

	/**
	 * 添加ReleaseProtocol
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addReleaseProtocol")
	public ModelAndView addreleaseProtocol(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			boolean flag = releaseProtocolService.addReleaseProtocol(paramsMap);
			if (!flag) {
				mov.addObject("msg", "添加ReleaseProtocol失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/releaseProtocol/releaseProtocolList.html");
			}
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "添加ReleaseProtocol失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * releaseProtocollist
	 */
	@RequestMapping(value = "/releaseProtocolList")
	public ModelAndView releaseProtocolList(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		PageObject po = new PageObject();

		try {

			po = releaseProtocolService.queryReleaseProtocolList(paramsMap);

			mov.addObject("list", po.getDatasource());
			mov.addObject("totalProperty", po.getTotalCount());
			mov.addObject("po", po);
			mov.addObject("paramsMap", paramsMap);
			mov.setViewName("/manager/releaseProtocol/list");
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "releaseProtocol查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 跳转到修改releaseProtocol页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toUpdateReleaseProtocol")
	public ModelAndView toUpdateReleaseProtocol(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			Map<String, Object> releaseProtocol = releaseProtocolService.getById(paramsMap);
			mov.addObject("obj", releaseProtocol);
			List<Map<String, Object>> releaseTypes = releaseProtocolService.releaseTypes();
			mov.addObject("releaseTypes", releaseTypes);
			mov.setViewName("/manager/releaseProtocol/update");
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "releaseProtocol查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 修改releaseProtocol
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateReleaseProtocol")
	public ModelAndView updateReleaseProtocol(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = releaseProtocolService.updateReleaseProtocol(paramsMap);

			if (!flag) {
				mov.addObject("msg", "修改失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/releaseProtocol/releaseProtocolList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "修改失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 逻辑删除releaseProtocol
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delReleaseProtocol")
	public ModelAndView delReleaseProtocol(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = releaseProtocolService.deleteReleaseProtocol(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/releaseProtocol/releaseProtocolList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	/**
	 * 物理删除releaseProtocol
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteReleaseProtocol")
	public ModelAndView deleteReleaseProtocol(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = releaseProtocolService.deleteReleaseProtocol(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/releaseProtocol/releaseProtocolList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	//auto-code-end

}
