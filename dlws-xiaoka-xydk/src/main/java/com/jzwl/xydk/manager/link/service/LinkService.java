/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.link.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.link.dao.LinkDao;

/**
 * 
 * 链接服务
 * 
 * @author   JiangShengDong(nikeodong@126.com)
 * @Date	 2017年6月28日
 */
@Service
public class LinkService {

	@Autowired
	private LinkDao linkDao;

	/**
	 * 列表
	 *
	 * @param map map对象
	 * @return 页面所需信息
	 */
	public PageObject list(Map<String, Object> map) {
		return linkDao.list(map);
	}

	/**
	 * 保存
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public boolean save(Map<String, Object> map) {
		return linkDao.save(map);
	}

	/**
	 * 改变
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public boolean change(Map<String, Object> map) {

		return linkDao.change(map);

	}

	/**
	 * 获取id
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public Map<String, Object> getById(Map<String, Object> map) {

		return linkDao.getById(map);

	}

	/**
	 * 修改
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public boolean updateLink(Map<String, Object> map) {

		return linkDao.updateLink(map);

	}

	/**
	 * 删除
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public boolean deleteLink(Map<String, Object> map) {

		return linkDao.deleteLink(map);

	}

}
