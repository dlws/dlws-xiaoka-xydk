/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.releaseProtocol.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.releaseProtocol.dao.ReleaseProtocolDao;

@Service("releaseProtocolService")
public class ReleaseProtocolService {

	@Autowired
	private ReleaseProtocolDao releaseProtocolDao;

	public boolean addReleaseProtocol(Map<String, Object> map) {

		return releaseProtocolDao.addReleaseProtocol(map);

	}

	public PageObject queryReleaseProtocolList(Map<String, Object> map) {

		return releaseProtocolDao.queryReleaseProtocolList(map);

	}

	public boolean updateReleaseProtocol(Map<String, Object> map) {

		return releaseProtocolDao.updateReleaseProtocol(map);

	}

	public boolean deleteReleaseProtocol(Map<String, Object> map) {

		return releaseProtocolDao.deleteReleaseProtocol(map);

	}

	public boolean delReleaseProtocol(Map<String, Object> map) {

		return releaseProtocolDao.delReleaseProtocol(map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return releaseProtocolDao.getById(map);
	}

	/**
	 * 
	 * 项目名称：
	 * 描述：对应数据库设计时对应的下拉，单选框 值-名
	 * 创建人： 
	 * 创建时间：
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		return releaseProtocolDao.querySelectValue(map);
	}

	public List<Map<String, Object>> releaseTypes() {

		// TODO Auto-generated method stub
		return releaseProtocolDao.releaseTypes();

	}

}
