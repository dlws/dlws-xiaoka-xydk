/**
 * SchoolShopsResourceController.java
 * com.jzwl.xydk.manager.schoolShopsResource.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.associationResource.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.associationResource.service.AssociationResourceService;

/**
 *	资源模块----校内商家Controller
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月24日 	 
 */
@Controller
@RequestMapping(value = "/associationResource")
public class AssociationResourceController extends BaseController {
	@Autowired
	private AssociationResourceService associationResourceService;

	/**
	 *	列表
	 * @param request
	 * @param response
	 * @param mov
	 */
	@RequestMapping(value = "/list")
	public String list(HttpServletRequest request, HttpServletResponse response, Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = associationResourceService.list(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/associationResource/list";
	}
}
