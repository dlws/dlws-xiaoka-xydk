package com.jzwl.xydk.manager.changemanage.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.changemanage.service.ManageChangeService;
import com.jzwl.xydk.manager.skillUser.service.SkillUserManagerService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.xkh2_3.changemanager.service.ChangeMangerService;
@Controller
@RequestMapping(value = "/managechange")
public class ManageChangeController extends BaseWeixinController{

	@Autowired
	private ManageChangeService manageChangeService;
	@Autowired
	private ChangeMangerService changeManagerService;
	@Autowired
	private SkillUserManagerService skillUserManagerService; 
	@Autowired
	private SendMessageService sendMessage;
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/list")
	public String divideDrawList(HttpServletRequest request, 
				HttpServletResponse response,Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = manageChangeService.queryManageChangeforPage(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/changemanage/list";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping("/getManageById")
	public String getManageById(HttpServletRequest request, 
				HttpServletResponse response, Model mov){
		createParameterMap(request);
		try {
			Map<String, Object> map = manageChangeService.getManageById(paramsMap);
			mov.addAttribute("manage", map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/manager/changemanage/audit_list";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：审核
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value="/audit")
	public String audit(HttpServletRequest request, 
			HttpServletResponse response, Model mov){
		createParameterMap(request);
		try {
			String radio = (String)paramsMap.get("radio");
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("id", paramsMap.get("basicId"));
			Map<String,Object> baseInfo_map = skillUserManagerService.getSkillBaseInfo(map);
			if(radio.equals("1")){//通过
				manageChangeService.updateAgreeRecord(paramsMap);
				changeManagerService.updateSkillinfo(paramsMap);
				changeManagerService.deleteBasicInfo(paramsMap);
				baseInfo_map.put("auditType", "恭喜您,管理员审核成功.");
				//发送审核信息
				sendMessage.sendAuditSuccessMessage(baseInfo_map);
				
			}else{//审核拒绝状态改为4
				manageChangeService.updateRefuseRecord(paramsMap);
				baseInfo_map.put("auditType", "很遗憾,管理员审核失败.");
				sendMessage.sendAuditSuccessMessage(baseInfo_map);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "redirect:/managechange/list.html";
	}
}
