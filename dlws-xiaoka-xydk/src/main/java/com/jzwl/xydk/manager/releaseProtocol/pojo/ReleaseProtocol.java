/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.releaseProtocol.pojo;

import com.jzwl.system.base.pojo.BasePojo;

public class ReleaseProtocol extends BasePojo implements java.io.Serializable {

	private static final long serialVersionUID = 5454155825314635342L;

	//alias
	public static final String TABLE_ALIAS = "ReleaseProtocol";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_CLASS_ID = "classId";
	public static final String ALIAS_TITLE = "title";
	public static final String ALIAS_CONTENT = "发布内容表";
	public static final String ALIAS_IS_DELETE = "是否删除";
	public static final String ALIAS_CREATE_DATE = "创建时间";

	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;

	//columns START
	/**
	 * id       db_column: id 
	 */
	private java.lang.Long id;
	/**
	 * classId       db_column: classId 
	 */
	private java.lang.Long classId;
	/**
	 * title       db_column: title 
	 */
	private java.lang.String title;
	/**
	 * 发布内容表       db_column: content 
	 */
	private java.lang.String content;
	/**
	 * 是否删除       db_column: isDelete 
	 */
	private java.lang.Integer isDelete;
	/**
	 * 创建时间       db_column: createDate 
	 */
	private java.util.Date createDate;

	//columns END

	public void setId(java.lang.Long value) {
		this.id = value;
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public java.lang.Long getClassId() {
		return this.classId;
	}

	public void setClassId(java.lang.Long value) {
		this.classId = value;
	}

	public java.lang.String getTitle() {
		return this.title;
	}

	public void setTitle(java.lang.String value) {
		this.title = value;
	}

	public java.lang.String getContent() {
		return this.content;
	}

	public void setContent(java.lang.String value) {
		this.content = value;
	}

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}

	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class, FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}

}
