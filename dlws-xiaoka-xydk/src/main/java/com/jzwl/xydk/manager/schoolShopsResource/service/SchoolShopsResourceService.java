/**
 * SchoolShopsResourceService.java
 * com.jzwl.xydk.manager.schoolShopsResource.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.schoolShopsResource.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.schoolShopsResource.dao.SchoolShopsResourceDao;

/**
 *	校内商家Service
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月24日 	 
 */
@Service
public class SchoolShopsResourceService {
	@Autowired
	private SchoolShopsResourceDao schoolShopsResourceDao;

	/**
	 * 	列表
	 * @param map
	 */
	public PageObject list(Map<String, Object> map) {

		return schoolShopsResourceDao.list(map);

	}
}
