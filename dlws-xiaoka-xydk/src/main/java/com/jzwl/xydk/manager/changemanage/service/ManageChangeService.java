package com.jzwl.xydk.manager.changemanage.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.changemanage.dao.ManageChangeDao;
@Service("manageChangeService")
public class ManageChangeService {

	@Autowired
	private ManageChangeDao manageChangeDao;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更换管理员列表
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public PageObject queryManageChangeforPage(Map<String, Object> paramsMap) {
		return manageChangeDao.queryManageChangeforPage(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取信息
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> getManageById(Map<String, Object> paramsMap) {
		return manageChangeDao.getManageById(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：管理员拒绝
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void updateRefuseRecord(Map<String, Object> paramsMap) {
		manageChangeDao.updateRefuseRecord(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：管理员审核通过
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void updateAgreeRecord(Map<String, Object> paramsMap) {
		manageChangeDao.updateAgreeRecord(paramsMap);
	}

}
