package com.jzwl.xydk.manager.refunds.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("refundsDao")
public class RefundsDao {

	@Autowired
	private BaseDAO baseDAO;

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：分页查询退款集合
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param map
	 * @return
	 * @version
	 */
	public PageObject queryRefundsforPage(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select c.name,t.id,t.openId,o.orderStatus,t.refundsOrderId,t.refundsMoney,t.checkPeople,t.checkStatus,"
				+ " t.orderSourceStatus,t.checkTime,t.createTime,t.bank,t.bankCardNumber,AccountFullName "
				+ " from `xiaoka-xydk`.applicationrefunds t left join v2_wx_customer c on t.openId = c.openId "
				+ " left join `xiaoka-xydk`.order o on o.orderId = t.refundsOrderId where 1=1 ");
		if (null != map.get("name") && StringUtils.isNotEmpty(map.get("name").toString())) {
			sb.append(" and c.name like '%" + map.get("name") + "%' ");
		}
		if (null != map.get("orderId") && StringUtils.isNotEmpty(map.get("orderId").toString())) {
			sb.append(" and t.refundsOrderId like '%" + map.get("orderId") + "%' ");
		}
		if (null != map.get("checkStatus") && StringUtils.isNotEmpty(map.get("checkStatus").toString())) {
			sb.append(" and t.checkStatus =" + map.get("checkStatus"));
		}
		if (null != map.get("bankCardNumber") && StringUtils.isNotEmpty(map.get("bankCardNumber").toString())) {
			sb.append(" and t.bankCardNumber like '%" + map.get("bankCardNumber") + "%' ");
		}
		if (null != map.get("orderSourceStatus") && StringUtils.isNotEmpty(map.get("orderSourceStatus").toString())) {
			sb.append(" and t.orderSourceStatus =" + map.get("orderSourceStatus"));
		}
		if (null != map.get("beginDate") && StringUtils.isNotEmpty(map.get("beginDate").toString())) {
			sb.append(" and  DATE_FORMAT(t.createTime,'%Y-%m-%d') >= DATE_FORMAT('" + map.get("beginDate")
					+ "','%Y-%m-%d')");
		}
		if (null != map.get("endDate") && StringUtils.isNotEmpty(map.get("endDate").toString())) {
			sb.append(" and  DATE_FORMAT(t.createTime,'%Y-%m-%d') <= DATE_FORMAT('" + map.get("endDate")
					+ "','%Y-%m-%d')");
		}
		sb.append(" order by t.createTime desc ");
		PageObject po = baseDAO.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据id获取
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> queryRefundsforList(Map<String, Object> paramsMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("select c.name,t.id, t.openId, t.refundsOrderId, t.refundsMoney, t.checkPeople, t.checkStatus,"
				+ " t.checkTime, t.createTime, t.bank, t.bankCardNumber, t.AccountFullName from `xiaoka-xydk`.applicationrefunds t "
				+ " left join v2_wx_customer c on t.openId = c.openId where id= '" + paramsMap.get("id") + "'");
		return baseDAO.queryForList(sb.toString());
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：审核不通过
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public boolean auditNotPass(Map<String, Object> map) {
		map.put("checkTime", new Date());
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.applicationrefunds set checkStatus='2',"
				+ "checkTime=:checkTime,refuseReason=:refuseReason,checkPeople=:userName where id = :id");
		return baseDAO.executeNamedCommand(sb.toString(), map);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getSourceTypeByCode() {
		StringBuffer sb = new StringBuffer();
		sb.append("select * from `xiaoka`.v2_dic_data WHERE dic_id in(select dic_id from `xiaoka`.v2_dic_type WHERE dic_code='tkfwje')");
		return baseDAO.queryForList(sb.toString());
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家拒绝订单,退款表插入数据
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean refuseOrder(Map<String, Object> map) {
		String id = Sequence.nextId();
		Date createTime = new Date();
		map.put("id", id);
		map.put("createTime", createTime);
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.applicationrefunds "
				+ "(id,openId,refundsOrderId,checkStatus,orderSourceStatus,refundsMoney,createTime,bank,bankCardNumber,AccountFullName) "
				+ "values (:id,:openId,:orderId,0,0,:realityMoney,:createTime,:bank,:bankCardNumber,:AccountFullName)");
		return baseDAO.executeNamedCommand(sb.toString(), map);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家拒绝退款,在退款表里插入数据
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param map
	 * @return
	 * @version
	 */
	public boolean refundDenied(Map<String, Object> map) {
		String id = Sequence.nextId();
		Date createTime = new Date();
		map.put("id", id);
		map.put("createTime", createTime);
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.applicationrefunds "
				+ "(id,openId,refundsOrderId,checkStatus,orderSourceStatus,refundsMoney,createTime,bank,bankCardNumber,AccountFullName) "
				+ "values (:id,:openId,:orderId,2,1,:realityMoney,:createTime,:bank,:bankCardNumber,:AccountFullName)");
		return baseDAO.executeNamedCommand(sb.toString(), map);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家同意退款
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param map
	 * @return
	 * @version
	 */
	public boolean agreeRefund(Map<String, Object> map) {
		String id = Sequence.nextId();
		Date createTime = new Date();
		map.put("id", id);
		map.put("createTime", createTime);
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.applicationrefunds "
				+ "(id,openId,refundsOrderId,checkStatus,orderSourceStatus,refundsMoney,createTime,bank,bankCardNumber,AccountFullName) "
				+ "values (:id,:openId,:orderId,0,1,:realityMoney,:createTime,:bank,:bankCardNumber,:AccountFullName)");
		return baseDAO.executeNamedCommand(sb.toString(), map);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据订单ID查询服务名称
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param orderId
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findOrderDetailNikeNameByOrderId(Long orderId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select * from `xiaoka-xydk`.order_detail where orderId = '" + orderId + "'");
		return baseDAO.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param orderId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findfindOrderById(Long orderId) {
		Map<String, Object> order = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select orderId, openId, sellerOpenId, schoolId, cityId, createDate, payMoney, payTime, "
				+ "serviceMoney, diplomatMoney, platformMoney,serviceRatio, diplomatRatio, platformRatio, "
				+ "realityMoney, orderStatus, payway, cancelTime, invalidTime, confirmTime, refundTime, "
				+ "linkman,phone, message, startDate, endDate, isDelete, enterType, partnerTradeNo "
				+ "from `xiaoka-xydk`.order where orderId = '" + orderId + "'");
		List<Map<String, Object>> orderList = baseDAO.queryForList(sb.toString());
		if (orderList != null && orderList.size() > 0) {
			order = orderList.get(0);
		}
		return order;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据用户ID查询钱包余额
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findWalletBalanceByOpenId(String openId) {
		Map<String, Object> wallet = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select id, openId, balance, createTime from `xiaoka-xydk`.wallet where openId = '" + openId + "'");
		//String sql = "select id, openId, balance, createTime from `xiaoka-xydk`.wallet where openId = '"+openId+"'";
		List<Map<String, Object>> walletList = baseDAO.queryForList(sb.toString());
		if (walletList != null && walletList.size() > 0) {
			wallet = walletList.get(0);
		}
		return wallet;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：插入到分成表
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param mapMoney
	 * @version
	 */
	public void divideOrWithDrawRecord(Map<String, Object> mapMoney) {
		String id = Sequence.nextId();
		mapMoney.put("id", id);
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.divideorwithdrawrecord "
				+ "(id,openId,divideOrWithdraw,balanceMoney,state,divideOrWithdrawTime,orderId) "
				+ " values (:id,:openId,:divideOrWithdraw,:balanceMoney,:state,:divideOrWithdrawTime,:orderId)");
		baseDAO.executeNamedCommand(sb.toString(), mapMoney);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param mapMoney
	 * @version
	 */
	public void updateWallet(Map<String, Object> mapp, Map<String, Object> mapMoney) {
		StringBuffer sb = new StringBuffer();
		mapMoney.put("createTime", new Date());
		if (mapp != null && mapp.size() > 0) {
			//原来有数据更新
			sb.append("update `xiaoka-xydk`.wallet set balance = :balanceMoney where openId=:openId ");
		} else {
			//没有数据执行插入操作
			String id = Sequence.nextId();
			mapMoney.put("id", id);
			sb.append("insert into `xiaoka-xydk`.wallet (id,openId,balance,createTime) values (:id,:openId,:balanceMoney,:createTime)");
		}
		baseDAO.executeNamedCommand(sb.toString(), mapMoney);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更新订单状态
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param orderMap
	 * @version
	 */
	public boolean updateOrderState(Map<String, Object> orderMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.order set orderStatus = :state where orderId = :orderId");
		return baseDAO.executeNamedCommand(sb.toString(), orderMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家同意退款更新本条记录
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void updateRefunds(Map<String, Object> paramsMap) {
		paramsMap.put("checkTime", new Date());
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.applicationrefunds set checkPeople = :userName,checkStatus='1',checkTime=:checkTime where id = :id");
		baseDAO.executeNamedCommand(sb.toString(), paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：自动确认收货
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findListForOrderState() {
		StringBuffer sb = new StringBuffer();
		sb.append("select orderId, openId, sellerOpenId, schoolId, cityId, createDate, payMoney, payTime, serviceMoney, "
				+ "diplomatMoney,platformMoney, serviceRatio, diplomatRatio, platformRatio, realityMoney, orderStatus, "
				+ "payway, cancelTime, invalidTime,refundTime, linkman, phone, message, startDate, endDate  "
				+ "from `xiaoka-xydk`.order where orderStatus = '0' and isDelete = '0'");
		return baseDAO.queryForList(sb.toString());
	}

	/**
	 * 详情
	 * @param map
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> details(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select c.name,t.id, t.openId, t.refundsOrderId, t.refundsMoney, t.checkPeople, t.checkStatus,"
				+ " t.checkTime, t.createTime, t.bank, t.bankCardNumber, t.AccountFullName,t.refuseReason from `xiaoka-xydk`.applicationrefunds t "
				+ " left join v2_wx_customer c on t.openId = c.openId where id= '" + map.get("id") + "'");
		return baseDAO.queryForList(sb.toString());
	}
}
