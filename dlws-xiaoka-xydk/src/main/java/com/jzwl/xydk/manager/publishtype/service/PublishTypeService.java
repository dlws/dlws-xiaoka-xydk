package com.jzwl.xydk.manager.publishtype.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.publishtype.dao.PublishTypeDao;
@Service("publishTypeService")
public class PublishTypeService {

	@Autowired
	private PublishTypeDao publishTypeDao;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：分页查询
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public PageObject queryPublishforPage(Map<String, Object> paramsMap) {
		return publishTypeDao.queryPublishforPage(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：保存分类
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean save(Map<String, Object> paramsMap) {
		return publishTypeDao.save(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：编辑时获取数据
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> getData(Map<String, Object> paramsMap) {
		return publishTypeDao.getData(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean update(Map<String, Object> paramsMap) {
		return publishTypeDao.update(paramsMap);
	}

}

