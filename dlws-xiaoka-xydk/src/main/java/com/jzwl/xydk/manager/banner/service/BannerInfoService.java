/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.banner.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.banner.dao.BannerInfoDao;

@Service("v2bannerInfoService")
public class BannerInfoService {

	@Autowired
	private BannerInfoDao bannerInfoDao;

	public boolean addBannerInfo(Map<String, Object> map) {

		return bannerInfoDao.addBannerInfo(map);

	}

	public PageObject queryBannerInfoList(Map<String, Object> map) {

		return bannerInfoDao.queryBannerInfoList(map);

	}

	public boolean updateBannerInfo(Map<String, Object> map) {

		return bannerInfoDao.updateBannerInfo(map);

	}

	public boolean deleteBannerInfo(Map<String, Object> map) {

		return bannerInfoDao.deleteBannerInfo(map);

	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return bannerInfoDao.getById(map);

	}

	public Map<String, Object> getByName(Map<String, Object> map) {

		return bannerInfoDao.getByName(map);

	}

	public boolean isUse(Map<String, Object> map) {

		return bannerInfoDao.isUse(map);

	}

	public List<Map<String, Object>> bannerTypes() {

		return bannerInfoDao.bannerTypes();

	}

}
