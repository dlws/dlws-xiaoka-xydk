package com.jzwl.xydk.manager.draw.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.draw.dao.DrawDao;

@Service("drawService")
public class DrawService {

	@Autowired
	private DrawDao drawDao;
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public PageObject queryDrawforPage(Map<String, Object> paramsMap) {
		return drawDao.queryDrawforPage(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryDrawforList(
			Map<String, Object> paramsMap) {
		return drawDao.queryDrawforList(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：提现不通过
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void auditNotPass(Map<String, Object> paramsMap) {
		drawDao.auditNotPass(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：用户点击提现
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param withdrawMoney
	 * @return
	 * @version
	 */
	public boolean addDraw(Map<String, Object> drawMap) {
		return drawDao.addDraw(drawMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据用户openid查询账户信息
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> getWalletBalanceByOpenId(String openId) {
		return drawDao.getWalletBalanceByOpenId(openId);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更新钱包
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param moneyMap
	 * @version
	 */
	public void updateWallet(Map<String, Object> moneyMap) {
		drawDao.updateWallet(moneyMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：记录表插入数据
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param withMap
	 * @version
	 */
	public void insertWithDraw(Map<String, Object> withMap) {
		drawDao.insertWithDraw(withMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：审核通过修改本身状态
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void auditPass(Map<String, Object> paramsMap) {
		drawDao.auditPass(paramsMap);
	}

}
