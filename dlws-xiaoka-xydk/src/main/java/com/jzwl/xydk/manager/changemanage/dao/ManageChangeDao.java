package com.jzwl.xydk.manager.changemanage.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;
@Repository("manageChangeDao")
public class ManageChangeDao {

	@Autowired
	private BaseDAO baseDAO;
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更换管理员列表
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param map
	 * @return
	 * @version
	 */
	public PageObject queryManageChangeforPage(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select u.userName,t.id,t.basicId,t.userId,t.managerName,t.phone,t.email,t.wx_number,t.isAgree,t.createDate "
				+ "from `xiaoka-xydk`.change_manager_record t left join `xiaoka-xydk`.user_basicinfo u on u.id=t.userId where 1=1 ");
		/*if(null !=map.get("name") && StringUtils.isNotEmpty(map.get("name").toString())){
			sb.append(" and c.name like '%" + map.get("name") +"%'");
	  	}
		if(null !=map.get("state") && StringUtils.isNotEmpty(map.get("state").toString())){
			sb.append(" and t.state =" + map.get("state"));
	  	}
		if (null != map.get("beginDate") && StringUtils.isNotEmpty(map.get("beginDate").toString())) {
			sb.append(" and  DATE_FORMAT(t.divideOrWithdrawTime,'%Y-%m-%d') >= DATE_FORMAT('"+map.get("beginDate")+"','%Y-%m-%d')");
		}
		if (null != map.get("endDate") && StringUtils.isNotEmpty(map.get("endDate").toString())) {
			sb.append(" and  DATE_FORMAT(t.divideOrWithdrawTime,'%Y-%m-%d') <= DATE_FORMAT('"+map.get("endDate")+"','%Y-%m-%d')");
		}*/
		sb.append(" order by t.createDate desc ");
		PageObject po = baseDAO.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取详情
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param map
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getManageById(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select u.userName,t.id,t.userId,t.basicId,t.managerName,t.phone,t.email,t.wx_number,t.isAgree,t.createDate "
				+ "	from `xiaoka-xydk`.change_manager_record t left join `xiaoka-xydk`.user_basicinfo u on u.id=t.userId where t.id = :id ");
		return baseDAO.queryForMap(sb.toString(), map);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：管理员拒绝
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void updateRefuseRecord(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		map.put("isAgree", 4);
		sb.append("update `xiaoka-xydk`.change_manager_record set isAgree = :isAgree where id = :id");
		baseDAO.executeNamedCommand(sb.toString(), map);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：管理员审核通过
	 * 创建人： sx
	 * 创建时间： 2017年5月25日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void updateAgreeRecord(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		map.put("isAgree", 3);
		sb.append("update `xiaoka-xydk`.change_manager_record set isAgree = :isAgree where id = :id ");
		baseDAO.executeNamedCommand(sb.toString(), map);
	}

}
