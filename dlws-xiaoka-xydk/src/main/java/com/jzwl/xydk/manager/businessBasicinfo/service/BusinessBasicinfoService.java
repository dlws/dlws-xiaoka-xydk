/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.businessBasicinfo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.businessBasicinfo.dao.BusinessBasicinfoDao;


@Service("businessBasicinfoService")
public class BusinessBasicinfoService {
	
	
	@Autowired
	private BusinessBasicinfoDao businessBasicinfoDao;
	

	public PageObject queryBusinessBasicinfoList(Map<String, Object> map) {
		
		return businessBasicinfoDao.queryBusinessBasicinfoList(map);
	
	}

	
	@SuppressWarnings("unchecked")
	public Map<String,Object> getById(Map<String, Object> map) {
		
		return businessBasicinfoDao.getById(map);
		
	}
	
	
}
