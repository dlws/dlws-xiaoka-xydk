/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.Entertype.controller;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.upload.MultipartUploadSample;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;

/**
 * Entertype
 * Entertype
 * entertype
 * <p>Title:EntertypeController </p>
 * 	<p>Description:Entertype </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller
@RequestMapping("/entertype")
public class EntertypeController extends BaseController {

	@Autowired
	private EntertypeService entertypeService;

	@Autowired
	private Constants constants;

	/**
	 * 跳转到添加Entertype的页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddEntertype")
	public ModelAndView toAddentertype(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		mov.setViewName("/manager/enterType/add");
		return mov;
	}

	/**
	 * 添加Entertype
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addEntertype")
	public ModelAndView addentertype(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			boolean flag = entertypeService.addEntertype(paramsMap);
			if (!flag) {
				mov.addObject("msg", "添加Entertype失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/entertype/entertypeList.html");
			}
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "添加Entertype失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * entertypelist
	 */
	@RequestMapping(value = "/entertypeList")
	public ModelAndView entertypeList(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		PageObject po = new PageObject();

		try {

			po = entertypeService.queryEntertypeList(paramsMap);

			mov.addObject("list", po.getDatasource());
			mov.addObject("totalProperty", po.getTotalCount());
			mov.addObject("po", po);
			mov.addObject("paramsMap", paramsMap);
			mov.setViewName("/manager/enterType/list");
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "entertype查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 跳转到修改entertype页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toUpdateEntertype")
	public ModelAndView toUpdateEntertype(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			Map<String, Object> entertype = entertypeService.getById(paramsMap);
			mov.addObject("obj", entertype);
			mov.setViewName("/manager/enterType/update");
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "entertype查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 跳转到详情entertype页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/detailEntertype")
	public ModelAndView detailEntertype(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			Map<String, Object> entertype = entertypeService.getById(paramsMap);
			mov.addObject("obj", entertype);
			mov.setViewName("/manager/enterType/detail");
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "entertype查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 修改entertype
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateEntertype")
	public ModelAndView updateEntertype(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = entertypeService.updateEntertype(paramsMap);

			if (!flag) {
				mov.addObject("msg", "修改失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/entertype/entertypeList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "修改失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 逻辑删除entertype
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delEntertype")
	public ModelAndView delEntertype(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = entertypeService.deleteEntertype(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/entertype/entertypeList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	/**
	 * 物理删除entertype
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteEntertype")
	public ModelAndView deleteEntertype(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = entertypeService.deleteEntertype(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/entertype/entertypeList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	/**
	 * 
	 * 上传图片
	 * <p>
	 *
	 * @param pic
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */

	@RequestMapping(value = "picture", method = RequestMethod.POST)
	public @ResponseBody Map uploadLogo(@RequestParam(required = false) MultipartFile pic, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Map map = new HashMap();
		String filename = URLEncoder.encode(pic.getOriginalFilename(), "utf-8");
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String name = df.format(new Date());
		Random r = new Random();
		for (int i = 0; i < 3; i++) {
			name += r.nextInt(10);
		}
		String savename = name + filename;

		boolean b = MultipartUploadSample.uploadOssImg(savename, pic);

		String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
		String picUrl = beforUrl + savename;
		map.put("imgeUrl", picUrl);// picUrl 传阿里云服务器上的全路径
		map.put("key", savename);// key值
		map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);

		return map;
	}

}
