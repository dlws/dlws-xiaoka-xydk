package com.jzwl.xydk.manager.baseInfoSkill.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.baseInfoSkill.service.BaseInfoSkillService;
import com.jzwl.xydk.manager.baseInfoSkill.service.FieldSkillService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;


@Controller
@RequestMapping(value="/schoolBusiSkill")
public class SchoolBusiSkillController extends BaseWeixinController{
	
	
	@Autowired
	private SkillUserService skillUserService;
	
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	
	@Autowired
	private FieldSkillService fieldSkillService;
	
	@Autowired
	private BaseInfoSkillService baseInfoSkillService;
	
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	
	private final String COMPARE_SIZE ="cooperaType";//合作类型
	/**
	 * 加载当前页面基本信息
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/toAddSchoolBusi")
	public String toAddSchoolBusi(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		model = toLoadCommon(model);
		model.addAttribute("basicId", paramsMap.get("id"));//enterId的传参方式更改
		
		return "/manager/baseUser/schoolBusiSkill/add_schoolbusi";
		
	}
	
	/**
	 * 获取页面传来的参数进行添加操作
	 */
	@RequestMapping(value="/addSchoolBusi")
	public String addSchoolBusi(HttpServletRequest request,Model model){
		
		String cooperaTypeJson="";  //合作形式和价格
		String cooperaCompJson="";  //合作形式和单位
		Map<String,Object> map =  createParameterMap(request);
		String basicId = paramsMap.get("basicId").toString();
		
		//处理合作形式和价格
		if(request.getParameterValues("cooperaType")!=null&&request.getParameterValues("cooperaPrice")!=null){//设置当前场地的属性
			
			String [] cooperaTyArray = request.getParameterValues("cooperaType");//合作形式
			String [] cooperaPrArray = request.getParameterValues("cooperaPrice");//报价
			int [] priceArray = StringUtil.toGetIntArray(cooperaPrArray);
			Arrays.sort(priceArray);//当前报价排序
			map.put("skillPrice", priceArray[0]);//将当前最小价格存入
			
		    cooperaTypeJson = StringUtil.dealArray(cooperaTyArray,cooperaPrArray);
		}
		//处理合作形式和单位
		if(request.getParameterValues("cooperaType")!=null&&request.getParameterValues("cooperaPrice")!=null){
			
			String [] cooperaTyArray = request.getParameterValues("cooperaType");//合作形式
			String [] cooperaCompArray = request.getParameterValues("cooperaCompany");//单位
			
			cooperaCompJson = StringUtil.dealArray(cooperaTyArray,cooperaCompArray);
			
		}
		
		map.put("cooperaCompany",cooperaCompJson);//封装合作形式和单位
		map.put("cooperaType",cooperaTypeJson);//封装合作形式和价格
		map.put("serviceType", 2);
		if(request.getParameterValues("closePhoto")!=null&&request.getParameterValues("middlePho")!=null&&request.getParameterValues("allPhoto")!=null){
			map.put("closePhoto", StringUtil.dealArray(request.getParameterValues("closePhoto")));//近景
			map.put("middlePho", StringUtil.dealArray(request.getParameterValues("middlePho")));//中景
			map.put("allPhoto", StringUtil.dealArray(request.getParameterValues("allPhoto")));//远景
		}else{
			System.out.println("近景，中景，远景照片不能为空  且每个种类照片不能小于两张");
		}
		//处理近中远景照片
		if(StringUtils.isNotBlank(baseInfoSkillService.addUserSkillinfo(map))){
			model.addAttribute("msg", "添加成功");
			return "redirect:/baseInfoSkill/list.html?basicId=" + basicId;
		}
		
		model.addAttribute("msg", "添加失败");
		return "/error";
		
	}
	
	/**
	 * 前往修改页面
	 */
	@RequestMapping(value="/toUpSchoolBusi")
	public String toUpSchoolBusi(HttpServletRequest request,Model model){
		
		//加载当前基础参数
		createParameterMap(request);
		model = toLoadCommon(model);
		model.addAttribute("basicId", paramsMap.get("id"));//enterId的传参方式更改
		
		//根据当前用户的技能ID获取技能信息，以及详情表的信息
		Map<String, Object> skillInfo = baseInfoSkillService.querySkillById(paramsMap);
		//查询当前技能的技能详情
		paramsMap.put("pid", paramsMap.get("skillId"));
		
		Map<String,Object> detailMap = personInfoDetSer.getuserSkillinfoDetail(paramsMap);
		
		//获取图片信息处理成List
		List<String> listClose = StringUtil.dealString(detailMap.get("closePhoto").toString());
		List<String> listMiddle =StringUtil.dealString(detailMap.get("allPhoto").toString()) ;
		List<String> listAll = StringUtil.dealString(detailMap.get("middlePho").toString());
		
		JSONObject  jasonObject = JSONObject.fromObject(detailMap.get("cooperaType"));
	    Map<String,Object> cooperaTypes = (Map)jasonObject;//map形式价格和合作形式
	    
	    JSONObject  jasonObjectUnit = JSONObject.fromObject(detailMap.get("cooperaCompany"));
	    Map<String,Object> cooperaComps = (Map)jasonObjectUnit;//map形式合作形式和价格单位
		
	    model.addAttribute("skillInfo", skillInfo);
	    model.addAttribute("detailMap", detailMap);
	    model.addAttribute("listClose", listClose);
	    model.addAttribute("listMiddle", listMiddle);
	    model.addAttribute("listAll", listAll);
	    model.addAttribute("cooperaTypes", cooperaTypes);
	    model.addAttribute("cooperaComps", cooperaComps);
	    
	    return "/manager/baseUser/schoolBusiSkill/edit_schoolbusi";
	}
	
	/**
	 * 更新表中的信息和字段  校内商家
	 */
	@RequestMapping(value="/upSchoolBusi")
	public String upSchoolBusi(HttpServletRequest request,Model model){
		
		String cooperaTypeJson="";  //合作形式和价格
		String cooperaCompJson="";  //合作形式和单位
		Map<String,Object> map =  createParameterMap(request);
		String basicId = paramsMap.get("basicId").toString();
		map.remove("basicId");
		//处理合作形式和价格
		if(request.getParameterValues("cooperaType")!=null&&request.getParameterValues("cooperaPrice")!=null){//设置当前场地的属性
			
			String [] cooperaTyArray = request.getParameterValues("cooperaType");//合作形式
			String [] cooperaPrArray = request.getParameterValues("cooperaPrice");//报价
			int [] priceArray = StringUtil.toGetIntArray(cooperaPrArray);
			Arrays.sort(priceArray);//当前报价排序
			map.put("skillPrice", priceArray[0]);//将当前最小价格存入
			
		    cooperaTypeJson = StringUtil.dealArray(cooperaTyArray,cooperaPrArray);
		}
		//处理合作形式和单位
		if(request.getParameterValues("cooperaType")!=null&&request.getParameterValues("cooperaPrice")!=null){
			
			String [] cooperaTyArray = request.getParameterValues("cooperaType");//合作形式
			String [] cooperaCompArray = request.getParameterValues("cooperaCompany");//单位
			
			cooperaCompJson = StringUtil.dealArray(cooperaTyArray,cooperaCompArray);
			
		}
		map.put("cooperaType",cooperaTypeJson);//封装合作形式和价格
		map.put("cooperaCompany",cooperaCompJson);//封装合作形式和价格
		map.put("serviceType", 2);//校内商家默认为线下
		if(request.getParameterValues("closePhoto")!=null&&request.getParameterValues("middlePho")!=null&&request.getParameterValues("allPhoto")!=null){
			map.put("closePhoto", StringUtil.dealArray(request.getParameterValues("closePhoto")));//近景
			map.put("middlePho", StringUtil.dealArray(request.getParameterValues("middlePho")));//中景
			map.put("allPhoto", StringUtil.dealArray(request.getParameterValues("allPhoto")));//远景
		}else{
			System.out.println("近景，中景，远景照片不能为空  且每个种类照片不能小于两张");
		}
		
		boolean flage = baseInfoSkillService.updateUserSkillinfo(map);
		
		if(flage){
			
			return "redirect:/baseInfoSkill/list.html?basicId=" + basicId;
		}
		
		model.addAttribute("msg", "修改失败");
		return "/error";
		
	}
	
	
	
	/**
	 * 公共的参数和方法
	 * @param model
	 * @return
	 */
	public Model toLoadCommon(Model model){
		
		//2.获取当前技能要在页面中加载的参数
		List<Map<String,Object>> list = fieldSkillService.getSkillType(GlobalConstant.ENTER_TYPE_XNSJ);
		//加载当前的场地规模（字典表） filed size
		List<Map<String, Object>> cooperaTypeList = userBasicinfoService.getFieldtype(COMPARE_SIZE);//合作类型
		//加载当前页面需要的所有单位
		List<Map<String,Object>> allUnit = baseInfoSkillService.getCanpanyMap();
		
		model.addAttribute("cooperaTypeList", cooperaTypeList);//合作形式
		model.addAttribute("list", list);//加载当前校内商家的合作形式
		model.addAttribute("allUnit", allUnit);
		
		return model;
	}
	

}
