package com.jzwl.xydk.manager.user.userBasicinfo.pojo;

import com.jzwl.system.base.pojo.BasePojo;

public class UserSkillinfoData extends BasePojo implements java.io.Serializable{
	
	/**
	 * 关于场地入驻和其他入驻的POJO
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String pid;
	private String typeName;
	private String typeValue;
	private String isDelete;
	private String createDate;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getTypeValue() {
		return typeValue;
	}
	public void setTypeValue(String typeValue) {
		this.typeValue = typeValue;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	
	
}
