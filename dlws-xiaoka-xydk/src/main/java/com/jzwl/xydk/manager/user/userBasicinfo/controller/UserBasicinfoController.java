/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.user.userBasicinfo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.school.schoollabel.service.SchoolInfoService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkhV2.sqdv.service.ComunityBigvService;

/**
 * userBasicinfo
 * <p>Title:UserBasicinfoController </p>
 * 	<p>Description:UserBasicinfo </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("userBasicinfo")
@RequestMapping("/userBasicinfo")
public class UserBasicinfoController extends BaseController {

	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private EntertypeService entertypeService;
	@Autowired
	private ComunityBigvService comunityBigvService;
	@Autowired
	private SchoolInfoService schoolInfoService;

	/**
	 * 跳转到添加UserBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddUserBasicinfo")
	public String toAddUserBasicinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		//获取所有的城市，从xiaoka中进行获取
		List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();

		//获取省列表
		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();

		model.addAttribute("url", "userBasicinfo/addUserBasicinfo.html");
		model.addAttribute("cityList", cityList);
		model.addAttribute("provnceList", provnceList);//省市

		return "/manager/user/add";

	}

	/**
	 * 添加UserBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addUserBasicinfo")
	public String addUserBasicinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			String userName = String.valueOf(request.getSession().getAttribute("userName"));
			if (!"".equals(userName)) {
				paramsMap.put("createUser", userName);
			} else {
				paramsMap.put("createUser", "admin");
			}
			boolean flag = userBasicinfoService.addUserBasicinfo(paramsMap);
			if (flag) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/userBasicinfo/userBasicinfoList.html?start=0";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}

	}

	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdateUserBasicinfo")
	public String toUpdateUserBasicinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			//获取省列表
			List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
			model.addAttribute("provnceList", provnceList);
			//获取所有的城市，从xiaoka中进行获取
			/*List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();
			model.addAttribute("cityList", cityList);*/

			//List<Map<String, Object>> cityList = userBasicinfoService.queryCityByPro(proId);
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息
			model.addAttribute("objMap", objMap);
			String cityId = String.valueOf(objMap.get("cityId"));
			Map<String, Object> proMap = userBasicinfoService.getProvence(cityId);//查询所属省
			model.addAttribute("proMap", proMap);

			String province = String.valueOf(proMap.get("province"));
			List<Map<String, Object>> cityList = userBasicinfoService.queryCityByPro(province);//根据省查出市
			model.addAttribute("cityList", cityList);

			String sholId = String.valueOf(objMap.get("schoolId"));
			Map<String, Object> sholMap = userBasicinfoService.getSholName(sholId);//查询所属学校
			model.addAttribute("sholMap", sholMap);
			model.addAttribute("url", "userBasicinfo/updateUserBasicinfo.html");
			return "/manager/user/edit";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 修改userBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateUserBasicinfo")
	public String updateUserBasicinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = userBasicinfoService.updateUserBasicinfo(paramsMap);

			if (flag) {
				return "redirect:/userBasicinfo/userBasicinfoList.html?start=0";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * userBasicinfolist
	 */
	@RequestMapping(value = "/userBasicinfoList")
	public String userBasicinfoList(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			PageObject po = new PageObject();
			po = userBasicinfoService.queryUserBasicinfoList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap", paramsMap);
			return "/manager/user/list";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 删除userBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteUserBasicinfo")
	public String deleteUserBasicinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = userBasicinfoService.deleteUserBasicinfo(paramsMap);

			if (flag) {
				return "redirect:/userBasicinfo/userBasicinfoList.html?start=0";
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}

	}

	/**
	 * 根据城市获取城市下的学校
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/querySchoolInfoByCityId")
	public @ResponseBody Map querySchoolInfoByCityId(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		createParameterMap(request);
		Map map = new HashMap();
		List list = new ArrayList();
		list = userBasicinfoService.querySchoolInfoByCityId(paramsMap);
		map.put("schools", list);
		return map;
	}

	/**
	 * 根据省查出市
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/queryCityByPro")
	public @ResponseBody Map queryCityByPro(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String proId = String.valueOf(paramsMap.get("proId"));
		Map map = new HashMap();
		List list = new ArrayList();
		list = userBasicinfoService.queryCityByPro(proId);
		map.put("cityList", list);
		return map;
	}

	/**
	 * 根据市查出院校类别
	 * @return  list
	 * dxf
	 */
	@RequestMapping(value = "/queryScholTypeByCity")
	public @ResponseBody Map queryScholTypeByCity(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String cityId = String.valueOf(paramsMap.get("cityId"));
		List list = new ArrayList();
		list = skillUserService.querySchollTypeVersion2(cityId);
		Map map = new HashMap();

		map.put("scholTypeList", list);
		return map;
	}

	/**
	 * 根据院校类别查出学校
	 * @return  list
	 * dxf
	 */
	@RequestMapping(value = "/queryScholByType")
	public @ResponseBody Map queryScholByType(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String cityId = String.valueOf(paramsMap.get("cityId"));
		String dcdaId = String.valueOf(paramsMap.get("dcdaId"));
		List list = new ArrayList();
		list = skillUserService.querySchollByTypeVersion2(cityId, dcdaId);
		Map map = new HashMap();

		map.put("scholList", list);
		return map;
	}

	/**
	 * 入驻信息查询
	 * userBasicinfolist
	 */
	@RequestMapping(value = "/settleList")
	public String settleList(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			PageObject po = new PageObject();
			po = userBasicinfoService.queryUserBasicinfoList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap", paramsMap);
			List cityList = schoolInfoService.cityList();
			List schoolList = schoolInfoService.getSchoolsByCityId(paramsMap);
			model.addAttribute("cityList",cityList);
			model.addAttribute("schoolList",schoolList);
			return "/manager/settled/list";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 跳转到选择入驻类型页面
	 * dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toChoseSettleType")
	//toChoseSettleType
	public String toChoseSettleType(HttpServletRequest request, HttpServletResponse response, Model model) {
		PageObject po = new PageObject();
		po = entertypeService.queryEntertypeList(paramsMap);
		model.addAttribute("enTypelist", po.getDatasource());
		return "/manager/settled/choseSettledType";
	}

	/**
	 * 跳转到添加入驻信息页面
	 * dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddSettle")
	//toAddSettle
	public String toAddSettle(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);

		//获取所有的城市，从xiaoka中进行获取
		List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();

		//获取省列表
		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();

		model.addAttribute("cityList", cityList);
		model.addAttribute("provnceList", provnceList);//省市
		//资源类型
		/*	List<Map<String, Object>> resourceList = userBasicinfoService.queryResourceListVersion3();
			model.addAttribute("resourceList", resourceList);//资源类型*/

		/***************************资源类型********start***************************/
		List<Map<String, Object>> dic_daList = comunityBigvService.getSourceTypeByCode();
		List<Map<String, Object>> sonList = comunityBigvService.getSourceTypeBySon();

		List<Map<String, Object>> resourceList = new ArrayList<Map<String, Object>>();

		String sonValue = "";
		String dicValue = "";
		String sonName = "";
		if (sonList.size() > 0 && dic_daList.size() > 0) {
			for (int i = 0; i < sonList.size(); i++) {
				sonValue = String.valueOf(sonList.get(i).get("classValue"));
				for (int j = 0; j < dic_daList.size(); j++) {
					sonName = String.valueOf(dic_daList.get(j).get("dic_name"));
					dicValue = String.valueOf(dic_daList.get(j).get("dic_value"));
					if (sonValue.equals(dicValue)) {
						Map<String, Object> sourMap = new HashMap<String, Object>();
						sourMap.put("classValue", sonValue);
						sourMap.put("className", sonName);
						resourceList.add(sourMap);
					}
				}
			}
		}
		model.addAttribute("resourceList", resourceList);//资源类型*/
		/***************************资源类型********end***************************/

		Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
		String enterTypeName = String.valueOf(setypeMap.get("typeValue"));
		model.addAttribute("paramsMap", paramsMap);
		model.addAttribute("enterTypeName", enterTypeName);
		model.addAttribute("enterId", setypeMap.get("enterId"));
		if ("jndr".equals(enterTypeName) || "gxst".equals(enterTypeName)) {
			return "/manager/settled/toAddCorprat";//技能达人入驻
		} else if ("sqdv".equals(enterTypeName)) {
			return "/manager/settled/settledType/toAddWeMedia";//社群大V入驻
		} else if ("cdzy".equals(enterTypeName)) {
			return "redirect:/enter/toAddUserByenter.html?enterId=" + setypeMap.get("id");//场地入驻
		} else if ("xnsj".equals(enterTypeName)) {
			return "redirect:/schoolBusin/toAddSchoolBusin.html?enterId=" + setypeMap.get("id");//校内商家
		}
		model.addAttribute("msg", "删除失败");
		return "/error";
	}

	/**
	 * 添加UserBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addSettleUser")
	public String addSettleUser(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			boolean flag = userBasicinfoService.addSettleUserVersion3(paramsMap);
			if (flag) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/userBasicinfo/settleList.html?start=0";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}

	}

	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdateSettled")
	public String toUpdateSettled(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			//获取省列表
			List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
			model.addAttribute("provnceList", provnceList);
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息
			model.addAttribute("objMap", objMap);
			String cityId = String.valueOf(objMap.get("cityId"));
			Map<String, Object> proMap = userBasicinfoService.getProvence(cityId);//查询所属省
			model.addAttribute("proMap", proMap);

			String province = String.valueOf(proMap.get("province"));
			List<Map<String, Object>> cityList = userBasicinfoService.queryCityByPro(province);//根据省查出市
			model.addAttribute("cityList", cityList);

			String sholId = String.valueOf(objMap.get("schoolId"));
			Map<String, Object> sholMap = userBasicinfoService.getSholName(sholId);//查询所属学校

			model.addAttribute("sholMap", sholMap);
			List<Map<String, Object>> settleList = userBasicinfoService.querySettleListVersion3(paramsMap);//查询子表list

			//资源类型
			/*List<Map<String, Object>> resourceList = userBasicinfoService.queryResourceListVersion3();
			model.addAttribute("resourceList", resourceList);*/

			/***************************资源类型********start***************************/
			List<Map<String, Object>> dic_daList = comunityBigvService.getSourceTypeByCode();
			List<Map<String, Object>> sonList = comunityBigvService.getSourceTypeBySon();

			List<Map<String, Object>> resourceList = new ArrayList<Map<String, Object>>();

			String sonValue = "";
			String dicValue = "";
			String sonName = "";
			if (sonList.size() > 0 && dic_daList.size() > 0) {
				for (int i = 0; i < sonList.size(); i++) {
					sonValue = String.valueOf(sonList.get(i).get("classValue"));
					for (int j = 0; j < dic_daList.size(); j++) {
						sonName = String.valueOf(dic_daList.get(j).get("dic_name"));
						dicValue = String.valueOf(dic_daList.get(j).get("dic_value"));
						if (sonValue.equals(dicValue)) {
							Map<String, Object> sourMap = new HashMap<String, Object>();
							sourMap.put("classValue", sonValue);
							sourMap.put("className", sonName);
							resourceList.add(sourMap);
						}
					}
				}
			}
			model.addAttribute("resourceList", resourceList);//资源类型*/
			/***************************资源类型********end***************************/

			String imgString = "";//活动图片
			String memNumberImg = "";//群成员图片
			String dairyTweetImg = "";//日常推文图片
			for (int i = 0; i < settleList.size(); i++) {
				if (settleList.size() > 0) {
					if ("activtyUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						imgString = String.valueOf(settleList.get(i).get("typeValue"));
						objMap.put("imgString", imgString);
					} else if ("memNumberImg".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						memNumberImg = String.valueOf(settleList.get(i).get("typeValue"));
						objMap.put("memNumberImg", memNumberImg);

					} else if ("dairyTweetUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						dairyTweetImg = String.valueOf(settleList.get(i).get("typeValue"));
						objMap.put("dairyTweetImg", dairyTweetImg);
					}
				}
			}
			//活动图片
			if (imgString != null && !"".equals(imgString)) {
				String[] imgArr = imgString.split(",");
				model.addAttribute("imgArr", imgArr);
			}
			//群成员图片
			if (memNumberImg != null && !"".equals(memNumberImg)) {
				String[] memNumberImgArr = memNumberImg.split(",");
				model.addAttribute("memNumberImg", memNumberImgArr);
			}

			//日常推文图片
			if (dairyTweetImg != null && !"".equals(dairyTweetImg)) {
				String[] dairyTweetImgArr = dairyTweetImg.split(",");
				model.addAttribute("dairyTweetImg", dairyTweetImgArr);
			}
			model.addAttribute("settleList", settleList);
			//判断入驻类型
			String enterId = String.valueOf(objMap.get("enterId"));
			paramsMap.put("enterId", enterId);
			Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
			String typeValue = String.valueOf(setypeMap.get("typeValue"));
			model.addAttribute("typeValue", typeValue);
			if ("sqdv".equals(typeValue)) {
				return "/manager/settled/settledType/toUpWeMedia";
			} else if ("jndr".equals(typeValue)) {
				return "/manager/settled/UpdateSettled";
			} else if ("cdzy".equals(typeValue)) {
				return "redirect:/enter/updateUserByenter.html?id=" + objMap.get("id");
			} else if ("xnsj".equals(typeValue)) {
				return "redirect:/schoolBusin/toUpdateSchoolBusin.html?id=" + objMap.get("id");
			}
			//model.addAttribute("url", "userBasicinfo/toUpdateSettled.html");
			return "/manager/settled/UpdateSettled";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 修改userBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateCorpratSettle")
	public String updateCorpratSettle(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = userBasicinfoService.upSettleVersion3(paramsMap);

			if (flag) {
				return "redirect:/userBasicinfo/settleList.html?start=0";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * 删除enterBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteEnterBasicinfo")
	public String deleteEnterBasicinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = userBasicinfoService.deleteUserBasicinfo(paramsMap);

			if (flag) {
				return "redirect:/userBasicinfo/settleList.html?start=0";
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}

	}

}
