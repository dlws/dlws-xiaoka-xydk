package com.jzwl.xydk.manager.quartz;

import org.springframework.beans.factory.annotation.Autowired;

import com.jzwl.xydk.manager.refunds.service.TimeTaskService;

/**
 * @author sx
 * 自动确认订单
 */
public class ServiceCompleteNoConfirm {

	private int isOpen = 0;// 监测开关，默认关闭

	@Autowired
	private TimeTaskService timeTaskService;
	
	//定时器轮询方法
	public void executeInternal() throws Exception {
		if (isOpen == 1) {//定时器开关打开了
			serviceOnCompleteWithNoConfirm();
		}
	}
	
	private void serviceOnCompleteWithNoConfirm(){
		timeTaskService.serviceOnCompleteWithNoConfirm();
	}
	
	public int getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(int isOpen) {
		this.isOpen = isOpen;
	}
}
