package com.jzwl.xydk.manager.refunds.pojo;

import java.io.Serializable;
import java.util.Date;

import com.jzwl.system.base.pojo.BasePojo;

public class Refunds extends BasePojo implements Serializable{

	/**
	 * serialVersionUID .
	 */
	private static final long serialVersionUID = 8305612055475377363L;

	//alias
		public static final String TABLE_ALIAS = "Refunds";
		public static final String ALIAS_ID = "id";
		public static final String ALIAS_OPENID = "退款用户openId";
		public static final String ALIAS_REFUNDS_ORDER_ID = "退款订单号";
		public static final String ALIAS_REFUNDS_MONEY = "退款金额";
		public static final String ALIAS_CHECK_PEOPLE = "审核人";
		public static final String ALIAS_CHECK_STATUS = "审核状态";
		public static final String ALIAS_CHECK_TIME = "checkTime";
		public static final String ALIAS_CREATE_TIME = "createTime";
		public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
		//id
		private Long id;
		//openid
		private String openId;
		//退款订单号
		private Long refundsOrderId;
		//退款金额
		private Double refundsMoney;
		//审核人
		private String checkPeople;
		//审核状态
		private int checkStatus;
		//审核时间
		private Date checkTime;
		//创建时间
		private Date createTime;
		//拒绝理由
		private String refuseReason;
		public String getRefuseReason() {
			return refuseReason;
		}
		public void setRefuseReason(String refuseReason) {
			this.refuseReason = refuseReason;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getOpenId() {
			return openId;
		}
		public void setOpenId(String openId) {
			this.openId = openId;
		}
		public Long getRefundsOrderId() {
			return refundsOrderId;
		}
		public void setRefundsOrderId(Long refundsOrderId) {
			this.refundsOrderId = refundsOrderId;
		}
		public Double getRefundsMoney() {
			return refundsMoney;
		}
		public void setRefundsMoney(Double refundsMoney) {
			this.refundsMoney = refundsMoney;
		}
		public String getCheckPeople() {
			return checkPeople;
		}
		public void setCheckPeople(String checkPeople) {
			this.checkPeople = checkPeople;
		}
		public int getCheckStatus() {
			return checkStatus;
		}
		public void setCheckStatus(int checkStatus) {
			this.checkStatus = checkStatus;
		}
		public Date getCheckTime() {
			return checkTime;
		}
		public void setCheckTime(Date checkTime) {
			this.checkTime = checkTime;
		}
		public String getCheckTimeString() {
			return dateToStr(getCheckTime(), FORMAT_CREATE_DATE);
		}
		public void setCheckTimeString(String value) {
			setCheckTime(strToDate(value, Date.class, FORMAT_CREATE_DATE));
		}
		public Date getCreateTime() {
			return createTime;
		}
		public void setCreateTime(Date createTime) {
			this.createTime = createTime;
		}
		public String getCreateTimeString() {
			return dateToStr(getCreateTime(), FORMAT_CREATE_DATE);
		}
		public void setCreateTimeString(String value) {
			setCreateTime(strToDate(value, Date.class, FORMAT_CREATE_DATE));
		}
}
