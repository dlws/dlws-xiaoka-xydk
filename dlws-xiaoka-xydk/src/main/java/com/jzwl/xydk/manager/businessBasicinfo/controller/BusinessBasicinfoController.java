/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.businessBasicinfo.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.businessBasicinfo.service.BusinessBasicinfoService;

/**
 * BusinessBasicinfo
 * BusinessBasicinfo
 * businessBasicinfo
 * <p>Title:BusinessBasicinfoController </p>
 * 	<p>Description:BusinessBasicinfo </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("businessBasicinfo")
@RequestMapping("/businessBasicinfo")
public class BusinessBasicinfoController extends BaseController {
	
	@Autowired
	private BusinessBasicinfoService businessBasicinfoService;
	
	/**
	 * businessBasicinfolist
	 */
	@RequestMapping(value = "/businessBasicinfoList")
	public String businessBasicinfoList(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		try{
			createParameterMap(request);
			PageObject po = new PageObject();
			po = businessBasicinfoService.queryBusinessBasicinfoList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap",paramsMap);
			return "/manager/business/businessBasicinfolist";
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg","获取数据失败");
			return "/error";
		}
		
	}
	
	/**
	 * 获取详情信息
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/getInfoById")
	public String getInfoById(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		try{
			createParameterMap(request);
			Map<String,Object> objMap = businessBasicinfoService.getById(paramsMap);
			model.addAttribute("objMap", objMap);
			return "/manager/business/businessBasicinfoEdit";
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg","获取数据失败");
			return "/error";
		}
		
	}
	
	//auto-code-end
	
	
}

