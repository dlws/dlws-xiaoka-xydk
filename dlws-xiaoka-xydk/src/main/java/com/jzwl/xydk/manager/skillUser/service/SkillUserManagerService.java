package com.jzwl.xydk.manager.skillUser.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.excel.ReadWriteExcelUtil;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.xydk.manager.skillUser.dao.SkillUserManagerDao;
import com.jzwl.xydk.manager.user.userBasicinfo.pojo.UserBasicinfo;

@Service("SkillUserManagerService")
public class SkillUserManagerService {
	@Autowired
	private SkillUserManagerDao skillUserManagerDao;
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：技能空间列表
	 * 创建人： ln
	 * 创建时间： 2016年10月13日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public PageObject skillSpaceList(Map<String, Object> paramsMap) {
		return skillUserManagerDao.skillSpaceList(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据技能空间Id获取技能空间基本信息
	 * 创建人： ln
	 * 创建时间： 2016年10月13日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> getSkillBaseInfo(Map<String, Object> paramsMap) {
		return skillUserManagerDao.getSkillBaseInfo(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据技能空间获取空间内技能信息
	 * 创建人： ln
	 * 创建时间： 2016年10月13日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySkillByUserId(
			Map<String, Object> paramsMap) {
		return skillUserManagerDao.querySkillByUserId(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：设置推荐或不推荐
	 * 创建人： ln
	 * 创建时间： 2016年10月13日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean updateSkillSpaceRecommend(Map<String, Object> paramsMap) {
		return skillUserManagerDao.updateSkillSpaceRecommend(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：设置个人空间打开或关闭
	 * 创建人： ln
	 * 创建时间： 2016年10月13日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean skillSpaceCloseOrOpen(Map<String, Object> paramsMap) {

		return skillUserManagerDao.skillSpaceCloseOrOpen(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：删除
	 * 创建人： dxf
	 * 创建时间： 2017年3月27日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean delBasInfo(Map<String, Object> paramsMap) {
		
		return skillUserManagerDao.delBasInfo(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：个人空间审核
	 * 创建人： ln
	 * 创建时间： 2016年10月13日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean skillSpaceCheck(Map<String, Object> paramsMap) {

		return skillUserManagerDao.skillSpaceCheck(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据技能ID获取技能图片
	 * 创建人： ln
	 * 创建时间： 2016年10月13日
	 * 标记：
	 * @param imgIds
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryImgBySkillIds(String imgIds) {
		return skillUserManagerDao.queryImgBySkillIds(imgIds);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改用户基本信息
	 * 创建人： ln
	 * 创建时间： 2016年11月2日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean editBaseInfo(Map<String, Object> paramsMap) {

		return skillUserManagerDao.editBaseInfo(paramsMap);
	}
	
	
	/**
	 * 导出Excel
	 * @return
	 * @throws Exception 
	 */
	public ReadWriteExcelUtil exportToExcel(Map<String,Object> map) throws Exception {
		//获取要导出的订单数据,组装excel数据
		ArrayList<ArrayList<String>> fieldData = this.findListByCondition(map);
		//表示excel的标题字段
		ArrayList<String> fieldName = getFileName();
		//表示excel的数据内容
		ReadWriteExcelUtil readWriteExcelUtil = new ReadWriteExcelUtil(fieldName, fieldData);
		return readWriteExcelUtil;
	}
	
	private ArrayList<String> getFileName() {
		ArrayList<String> list = new ArrayList();
		list.add("ID");
		list.add("入驻类型");
		list.add("用户入驻名称");
		list.add("用户入驻昵称");
		list.add("城市名称");
		list.add("学校名称");
		list.add("联系电话");
		list.add("邮箱地址");
		list.add("微信号");
		list.add("openId");
		list.add("审核状态");
		list.add("创建时间");
		list.add("关于我");
		
		return list;
	}
	
	/**
	 * 查询用户信息
	 * 不分页
	 * @return
	 * @throws Exception 
	 */
	private ArrayList<ArrayList<String>> findListByCondition(Map<String,Object> map) throws Exception {
		List<UserBasicinfo> basicinfos = skillUserManagerDao.getUserBasicinfos(map);
		ArrayList<ArrayList<String>> basicinfosList = new ArrayList<ArrayList<String>>();
		if (basicinfos != null && basicinfos.size() > 0) {
			for (UserBasicinfo info : basicinfos) {
				ArrayList<String> user = new ArrayList<String>();
				
				String s = null;
				//审核状态(0:未审核，1:审核成功，2:审核不通过）
				if(info.getAuditState()==0){//未审核
					s = "未审核";
				}else if(info.getAuditState()==1){
					s = "审核成功";
				}else{
					s = "审核不成功";
				}
				
				

				user.add(String.valueOf(info.getId()));
				user.add(StringUtil.isEmptyOrBlank(info.getTypeName())?"高校社团":info.getTypeName());
				user.add(StringUtil.isEmptyOrBlank(info.getUserName())?"无":info.getUserName());
				user.add(StringUtil.isEmptyOrBlank(info.getNickname())?"无":info.getNickname());
				user.add(StringUtil.isEmptyOrBlank(info.getCityName())?"无":info.getCityName());
				user.add(StringUtil.isEmptyOrBlank(info.getSchoolName())?"无":info.getSchoolName());
				user.add(StringUtil.isEmptyOrBlank(info.getPhoneNumber())?"无":info.getPhoneNumber());
				user.add(StringUtil.isEmptyOrBlank(info.getEmail())?"无":info.getEmail());
				user.add(StringUtil.isEmptyOrBlank(info.getWxNumber())?"无":info.getWxNumber());
				user.add(StringUtil.isEmptyOrBlank(info.getOpenId())?"无":info.getOpenId());
				user.add(s);
				user.add(StringUtil.isEmptyOrBlank(info.getCreateDate().toString())?"无":info.getCreateDate().toString());
				user.add(StringUtil.isEmptyOrBlank(info.getAboutMe())?"无":info.getAboutMe());
				
				basicinfosList.add(user);
			}
		}
		return basicinfosList;
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询资源对应标签
	 * 创建人： dxf
	 * 创建时间： 2016年2月16日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> toEditLabel(Map<String, Object> paramsMap) {

		return skillUserManagerDao.toEditLabel(paramsMap);
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询已选标签
	 * 创建人： dxf
	 * 创建时间： 2016年2月16日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySkilDicdata(Map<String, Object> paramsMap) {

		return skillUserManagerDao.querySkilDicdata(paramsMap);
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：执行修改
	 * 创建人： dxf
	 * 创建时间： 2016年2月16日
	 * 标记：manager
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean updateLabel(Map<String, Object> paramsMap) {

		return skillUserManagerDao.updateLabel(paramsMap);
	}
	
	public List<Map<String, Object>> queryExit(Map<String, Object> paramsMap) {

		return skillUserManagerDao.queryExit(paramsMap);
	}
	
	
	
	
}
