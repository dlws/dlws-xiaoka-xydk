/**
 * DiplomatExpandController.java
 * com.jzwl.xydk.manager.diplomatist.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.diplomatist.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.diplomatist.service.DiplomatExpandService;

/**
 *	外交官推广链接模块
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月12日 	 
 */
@Controller
@RequestMapping(value = "/diplomatExpand")
public class DiplomatExpandController extends BaseController {
	@Autowired
	private DiplomatExpandService diplomatExpandService;

	/**
	 * 列表
	 *
	 * @param request 请求
	 * @param response 响应
	 * @return mov 页面所需信息
	 */
	@RequestMapping(value = "/list")
	public Object list(HttpServletRequest request, HttpServletResponse response, Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = diplomatExpandService.list(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/diplomatist/diplomat_expand/list";

	}

	/**
	 * 详情
	 * 
	 * @param map对象
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping(value = "/details")
	public Object details(@RequestParam Map<String, Object> map, Model model) {
		model.addAttribute("diplomatistExpand", diplomatExpandService.details(map).get(0));
		return "/manager/diplomatist/diplomat_expand/details";
	}
}
