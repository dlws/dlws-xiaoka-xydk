/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.baseInfoSkill.controller;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.upload.MultipartUploadSample;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.admin.dicdate.pojo.DicData;
import com.jzwl.system.admin.dicdate.service.DicDataService;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.baseInfoSkill.service.BaseInfoSkillService;
import com.jzwl.xydk.manager.maxCategory.service.MaxCategoryService;
import com.jzwl.xydk.wap.xkh2_3.skill_detail.service.SkillDetailService;

/**
 * UserSkillinfo
 * UserSkillinfo
 * userSkillinfo
 * <p>Title:UserSkillinfoController </p>
 * 	<p>Description:UserSkillinfo </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("baseInfoSkill")
@RequestMapping("/baseInfoSkill")
public class BaseInfoSkillController extends BaseController {

	@Autowired
	private BaseInfoSkillService baseInfoSkillService;

	@Autowired
	private Constants constants;
	
	@Autowired
	private MaxCategoryService maxCategoryService;
	
	@Autowired
	private DicDataService dicDataService;
	
	@Autowired
	private SkillDetailService skillDetailService;

	
	
	/**
	 * 跳转到选择入驻类型页面
	 * dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/tochooseType")
	//toChoseSettleType
	public String tochooseType(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String basicId = request.getParameter("basicId");
		List<Map<String, Object>> EntertypeList = baseInfoSkillService.getClassF();
		model.addAttribute("enTypelist", EntertypeList);
		model.addAttribute("basicId", basicId);
		return "/manager/baseUser/baseSkill/skillPushType";
	}
	
	/**
	 * 判断哪个类型UserSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAdd")
	public String toAddUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		List<Map<String, Object>> classF = baseInfoSkillService.getClassF();
		//获取单位字典表项
		List<Map<String, Object>> canpanyMap = baseInfoSkillService.getCanpanyMap();
		String enterTypeName = String.valueOf(paramsMap.get("classValue"));
		model.addAttribute("enterTypeName", enterTypeName);
		if (GlobalConstant.ENTER_TYPE_JNDR.equals(enterTypeName) ||GlobalConstant.ENTER_TYPE_GXST.equals(enterTypeName)) {
			return "redirect:/baseInfoSkill/toAddSkillMan.html?basicId="+paramsMap.get("basicId")+"&fatherId="+paramsMap.get("fatherId");//技能达人入驻
		} else if (GlobalConstant.ENTER_TYPE_CDZY.equals(enterTypeName)) {
			return "redirect:/fieldSkill/toAddField.html?id="+paramsMap.get("basicId");//场地入驻
		} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(enterTypeName)) {
			return "redirect:/schoolBusiSkill/toAddSchoolBusi.html?id="+paramsMap.get("basicId");//校内商家
		} else if (GlobalConstant.ENTER_TYPE_SQDV.equals(enterTypeName)) {
			return "redirect:/baseInfoSkill/toAddWeMedia.html?basicId="+paramsMap.get("basicId")+"&fatherId="+paramsMap.get("fatherId");//社群大V技能设置
		}
		model.addAttribute("canpanyMap", canpanyMap);
		model.addAttribute("classList", classF);
		model.addAttribute("paramsMap", paramsMap);
		return "/manager/baseUser/baseSkill/add";

	}
	
	/**
	 * 技能达人、高校社团新建技能
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddSkillMan")
	public String toAddSkillMan(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String basicId = String.valueOf(paramsMap.get("basicId"));
		String fatherId = String.valueOf(paramsMap.get("fatherId"));//父表Id
		paramsMap.put("sid", fatherId);
		List<Map<String, Object>> canpanyTypes = baseInfoSkillService.canpanyTypes();//单位
		List<Map<String,Object>> sonList = baseInfoSkillService.getClassSBySid(paramsMap);
		
		model.addAttribute("basicId", basicId);
		model.addAttribute("fatherId", fatherId);
		model.addAttribute("sonList", sonList);
		model.addAttribute("canpanyTypes", canpanyTypes);
		return "/manager/baseUser/baseSkill/toAddSkillMan";
	}
	
	
	/**
	 *高校社团新建技能
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddWeMedia")
	public String toAddWeMedia(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String basicId = String.valueOf(paramsMap.get("basicId"));
		String fatherId = String.valueOf(paramsMap.get("fatherId"));//父表Id
		paramsMap.put("sid", fatherId);
		List<Map<String, Object>> canpanyTypes = baseInfoSkillService.canpanyTypes();//单位
		List<Map<String,Object>> sonList = baseInfoSkillService.getClassSBySid(paramsMap);
		
		model.addAttribute("basicId", basicId);
		model.addAttribute("fatherId", fatherId);
		model.addAttribute("sonList", sonList);
		model.addAttribute("canpanyTypes", canpanyTypes);
		return "/manager/baseUser/baseSkill/toAddMedia";
	}
	
	
	
	/**
	 * 添加UserSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add")
	public String addUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			String basicId = request.getParameter("basicId");
			//判定当前是否为社群大V
			if(paramsMap.containsKey("topLinePrice")){
				
				paramsMap.put("serviceType", 1);//TODO  社群大V默认为线上
				
				if(StringUtils.isNotBlank(paramsMap.get("topLinePrice").toString())
						&&StringUtils.isNotBlank(paramsMap.get("lessLinePrice").toString())){
					int topLinePrice = Integer.parseInt(paramsMap.get("topLinePrice").toString());
					int lessLinePrice = Integer.parseInt(paramsMap.get("lessLinePrice").toString());
					paramsMap.put("skillPrice", Math.min(topLinePrice, lessLinePrice));
					
				}
				if(StringUtils.isNotBlank(paramsMap.get("topLinePrice").toString())
						&&StringUtils.isBlank(paramsMap.get("lessLinePrice").toString())){
					paramsMap.put("skillPrice",paramsMap.get("topLinePrice").toString());
				}
				if(StringUtils.isBlank(paramsMap.get("topLinePrice").toString())
						&&StringUtils.isNotBlank(paramsMap.get("lessLinePrice").toString())){
					
					paramsMap.put("skillPrice",paramsMap.get("lessLinePrice").toString());
				}
				
			}
			//投放价格
			if(paramsMap.containsKey("deliveryPrice")&&StringUtils.isNotBlank(paramsMap.get("deliveryPrice").toString())){
				
				paramsMap.put("serviceType", 1);//TODO  社群大V默认为线上
				paramsMap.put("skillPrice",paramsMap.get("deliveryPrice").toString());
				
			}
			
			String skillId = baseInfoSkillService.addUserSkillinfo(paramsMap);

			if (skillId!=null) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/baseInfoSkill/list.html?basicId=" + basicId;
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}

	}

	
	/**
	 * 跳转到选择类型页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdatePushType")
	public String toUpdatePushType(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		List<Map<String, Object>> classF = baseInfoSkillService.getClassF();
		Map<String,Object> map = maxCategoryService.toEditMaxCategory(paramsMap);
		model.addAttribute("map", map);
		model.addAttribute("paramsMap", paramsMap);
		model.addAttribute("enTypelist", classF);
		return "/manager/baseUser/baseSkill/skillPushTypeUp";
	}
	
	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdate")
	public String toUpdateUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			
			if (GlobalConstant.ENTER_TYPE_CDZY.equals(paramsMap.get("classValue").toString())) {
				return "redirect:/fieldSkill/toUpFieldSkill.html?skillId=" + paramsMap.get("skillId")+"&id="+paramsMap.get("basicId");//场地入驻
			} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(paramsMap.get("classValue").toString())) {
				return "redirect:/schoolBusiSkill/toUpSchoolBusi.html?skillId=" + paramsMap.get("skillId")+"&id="+paramsMap.get("basicId");//校内商家
			}
//			String basicId = request.getParameter("basicId");
//			String id = request.getParameter("skillId");

			List<Map<String, Object>> classF = baseInfoSkillService.getClassF();
			List<Map<String, Object>> classS = baseInfoSkillService.getClassS();
			//获取单位字典表项
			List<Map<String, Object>> canpanyMap = baseInfoSkillService.getCanpanyMap();
			Map<String, Object> objMap = baseInfoSkillService.querySkillById(paramsMap);//查询技能主表信息
			String sid = String.valueOf(paramsMap.get("fatherId"));
			paramsMap.put("sid", sid);
			
			model = skillDetail(model,paramsMap);//设置当前技能参数
			List<Map<String,Object>> sonList = baseInfoSkillService.getClassSBySid(paramsMap);//服务类型
			model.addAttribute("sonList", sonList);
			
			
			model.addAttribute("canpanyMap", canpanyMap);
			model.addAttribute("objMap", objMap);
			model.addAttribute("classF", classF);
			model.addAttribute("classS", classS);
			model.addAttribute("paramsMap", paramsMap);
			
			String enterTypeName = String.valueOf(paramsMap.get("classValue"));
			model.addAttribute("enterTypeName", enterTypeName);
			if (GlobalConstant.ENTER_TYPE_JNDR.equals(enterTypeName) ||GlobalConstant.ENTER_TYPE_GXST.equals(enterTypeName)) {
				return "/manager/baseUser/baseSkill/toUpSkillMan";//技能达人入驻
			}else if (GlobalConstant.ENTER_TYPE_SQDV.equals(enterTypeName)) {
				return "/manager/baseUser/baseSkill/toUpMedia";//社群大V技能设置
			}
			return "/manager/baseUser/baseSkill/toUpSkillMan";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}
	
	/**
	 * 
	 * 描述:设置校内商家的单位，=====》》》Id  ====>>>Value值
	 * 作者:gyp
	 * @Date	 2017年5月22日
	 */
	public Map<String,Object> setCompanyValue(Map<String,Object> map) {
		
		if (map == null) return null;
		for (Entry<String, Object> entry : map.entrySet()) {
			String str = skillDetailService.getUnitById(entry.getValue().toString());
			map.put(entry.getKey(), str);
		}
		 return map;
		
	}
	
	public Model skillDetail(Model model,Map paramsMap){
		
		Map<String, Object> objMap = new HashMap<String, Object>();
			objMap = baseInfoSkillService.querySkillById(paramsMap);//查询技能主表信息
		model.addAttribute("objMap",objMap);
		List<Map<String, Object>> settleList = baseInfoSkillService.querySettleList(paramsMap);//查询子表详细信息
		/*model.addAttribute("settleList",settleList);*/
		
		List<Map<String, Object>> canpanyTypes = baseInfoSkillService.canpanyTypes();//单位
		model.addAttribute("canpanyTypes",canpanyTypes);
		Map<String, Object> skillInfo = new HashMap<String, Object>();
		String skillUrl = "";//头像
		
		String imgString = "";//活动图片
		String memNumberImg = "";//群成员图片
		String dairyTweetImg = "";//日常推文图片
		
		String closeStr = "";//近景
		String middleStr = "";//中景
		String AllStr = "";//远景
		for (int i = 0; i < settleList.size(); i++) {
			if (settleList.size() > 0) {
				if ("company".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String companyId = String.valueOf(settleList.get(i).get("typeValue"));//单位
					/*paramsMap.put("id", companyId);
					DicData dicData=dicDataService.getById(paramsMap);*/
					skillInfo.put("companyId", companyId);
				} 
				if ("skillPrice".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String skillPrice = String.valueOf(settleList.get(i).get("typeValue"));//技能价格
					skillInfo.put("skillPrice", skillPrice);
				} 
				if ("picUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String picUrl = String.valueOf(settleList.get(i).get("typeValue"));//用户头像
					skillInfo.put("picUrl", picUrl);
				} 
				if ("skillUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					skillUrl = String.valueOf(settleList.get(i).get("typeValue"));
				}
				if ("serviceType".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String serviceType = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("serviceType", serviceType);
				}
				if ("groupNum".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String groupNum  = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("groupNum", groupNum);//群人数
				}
				if ("fansNum".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String fansNum  = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("fansNum", fansNum);//粉丝数量
				}
				if ("deliveryPrice".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String deliveryPrice = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("deliveryPrice", deliveryPrice);//投放价格
				}
				if ("myAvatarVal".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String myAvatarVal = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("myAvatarVal", myAvatarVal);//公众号头像/社群头像
				}
				if ("pubNumId".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String pubNumId = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("pubNumId", pubNumId);//公众号Id
				}
				if ("readNum".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String readNum = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("readNum", readNum);//平均阅读量
				}
				if ("topLinePrice".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String topLinePrice = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("topLinePrice", topLinePrice);//头条报价
				}
				if ("lessLinePrice".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String lessLinePrice = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("lessLinePrice", lessLinePrice);//次条报价
				}
				if ("sqlx".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String sqlx = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("sqlx", sqlx);//社群类型
				}
				
				if ("cooperaCompany".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String cooperaCompany = String.valueOf(settleList.get(i).get("typeValue"));
					JSONObject  jasonObjectUnit = JSONObject.fromObject(cooperaCompany);
					Map<String,Object> cooperaComps = (Map)jasonObjectUnit;//map形式合作形式和价格单位
					skillInfo.put("cooperaCompany", cooperaComps);//校内商家单位
					Map<String,Object> companyValue = setCompanyValue(cooperaComps);
					skillInfo.put("companyValue", companyValue);
				}
				if ("cooperaType".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					String cooperaType = String.valueOf(settleList.get(i).get("typeValue"));
					JSONObject  jasonObjectUnit = JSONObject.fromObject(cooperaType);
					Map<String,Object> cooperaTyp = (Map)jasonObjectUnit;//map形式合作形式和价格单位
					skillInfo.put("cooperaType", cooperaTyp);//合作形式
				}
				
				
				if ("filedSize".equals(String.valueOf(settleList.get(i).get("typeName")))) {//场地规模
					String filedSize = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("filedSize", filedSize);//场地规模
				}
				if ("material".equals(String.valueOf(settleList.get(i).get("typeName")))) {//基本物资
					String material = String.valueOf(settleList.get(i).get("typeValue"));
					if(material!=""&&material!=null){
						String[] materialArr = material.split(",");
						skillInfo.put("materialArr", materialArr);//基本物资数组
						skillInfo.put("material", material);//基本物资String
					}
				}
				
				if ("begin".equals(String.valueOf(settleList.get(i).get("typeName")))) {//开始时间
					String begin = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("begin", begin);//开始时间
				}
				if ("end".equals(String.valueOf(settleList.get(i).get("typeName")))) {//结束时间
					String end = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("end", end);//结束时间
				}
				if ("partimejob".equals(String.valueOf(settleList.get(i).get("typeName")))) {//是否提供兼职人员
					String partimejob = String.valueOf(settleList.get(i).get("typeValue"));
					skillInfo.put("partimejob", partimejob);//是否提供兼职人员
				}
				if ("activtyUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					imgString = String.valueOf(settleList.get(i).get("typeValue"));
					objMap.put("imgString", imgString);
					objMap.put("xkh_imgId", settleList.get(i).get("id").toString());
				} else if ("memNumberImg".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					memNumberImg = String.valueOf(settleList.get(i).get("typeValue"));//群成员图
					objMap.put("memNumberImg", memNumberImg);
				} else if ("dairyTweetUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					dairyTweetImg = String.valueOf(settleList.get(i).get("typeValue"));
					objMap.put("dairyTweetImg", dairyTweetImg);
					objMap.put("xkh_dairyImgId", settleList.get(i).get("id").toString());
				} else if ("closePhoto".equals(String.valueOf(settleList.get(i).get("typeName")))) {
			    	  closeStr = String.valueOf(settleList.get(i).get("typeValue"));
			    	  objMap.put("closePhoto", closeStr);
			    	  if (closeStr != null && !"".equals(closeStr)) {
				  			String[] closeArr = closeStr.split(",");
				  			model.addAttribute("listClose", closeArr);
				  			model.addAttribute("closeStr",closeStr);
				  			objMap.put("closeId", settleList.get(i).get("id"));
				  	  }
			    } else if ("middlePho".equals(String.valueOf(settleList.get(i).get("typeName")))) {
			    	  middleStr = String.valueOf(settleList.get(i).get("typeValue"));
			    	  objMap.put("middlePho", middleStr);
			    	  if (middleStr != null && !"".equals(middleStr)) {
				  			String[] middleArr = middleStr.split(",");
				  			model.addAttribute("listMiddle", middleArr);
				  			model.addAttribute("middleStr",middleStr);
				  			objMap.put("middleId", settleList.get(i).get("id"));
				  	  }
			    }else if ("allPhoto".equals(String.valueOf(settleList.get(i).get("typeName")))) {
			    	AllStr = String.valueOf(settleList.get(i).get("typeValue"));
			    	objMap.put("allPhoto", AllStr);
			  		if (AllStr != null && !"".equals(AllStr)) {
			  			String[] AllStrArr = AllStr.split(",");
			  			model.addAttribute("listAll", AllStrArr);
			  			model.addAttribute("AllStr", AllStr);
			  			objMap.put("allId", settleList.get(i).get("id"));
			  		}
			    }
			}
		}
		
		//技能图片
		if (skillUrl != null && !"".equals(skillUrl)) {
			String[] imgArr = skillUrl.split(",");
			model.addAttribute("skillUrl", imgArr);
		}
		
		//活动图片
		if (imgString != null && !"".equals(imgString)) {
			String[] imgArr = imgString.split(",");
			model.addAttribute("imgArr", imgArr);
		}
		//群成员图片
		if (memNumberImg != null && !"".equals(memNumberImg)) {
			String[] memNumberImgArr = memNumberImg.split(",");
			model.addAttribute("memNumberImg", memNumberImgArr);
		}

		//日常推文图片
		if (dairyTweetImg != null && !"".equals(dairyTweetImg)) {
			String[] dairyTweetImgArr = dairyTweetImg.split(",");
			model.addAttribute("dairyTweetImg", dairyTweetImgArr);
		}
		
		model.addAttribute("settleList", settleList);
		
		model.addAttribute("skillInfo", skillInfo);
		
		return model;
		
	}

	/**
	 * 修改userSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String updateUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			
			if(paramsMap.containsKey("topLinePrice")){
				
				paramsMap.put("serviceType", 1);//TODO  社群大V默认为线上
				
				if(StringUtils.isNotBlank(paramsMap.get("topLinePrice").toString())
						&&StringUtils.isNotBlank(paramsMap.get("lessLinePrice").toString())){
					int topLinePrice = Integer.parseInt(paramsMap.get("topLinePrice").toString());
					int lessLinePrice = Integer.parseInt(paramsMap.get("lessLinePrice").toString());
					paramsMap.put("skillPrice", Math.min(topLinePrice, lessLinePrice));
					
				}
				if(StringUtils.isNotBlank(paramsMap.get("topLinePrice").toString())
						&&StringUtils.isBlank(paramsMap.get("lessLinePrice").toString())){
					paramsMap.put("skillPrice",paramsMap.get("topLinePrice").toString());
				}
				if(StringUtils.isBlank(paramsMap.get("topLinePrice").toString())
						&&StringUtils.isNotBlank(paramsMap.get("lessLinePrice").toString())){
					
					paramsMap.put("skillPrice",paramsMap.get("lessLinePrice").toString());
				}
				
			}
			//投放价格
			if(paramsMap.containsKey("deliveryPrice")&&
					StringUtils.isNotBlank(paramsMap.get("deliveryPrice").toString())){
				paramsMap.put("serviceType", 1);//TODO  社群大V默认为线上
				paramsMap.put("skillPrice",paramsMap.get("deliveryPrice").toString());
			}
			
			boolean flag = baseInfoSkillService.updateUserSkillinfo(paramsMap);

			if (flag) {
				model.addAttribute("basicId", paramsMap.get("basicId"));
				return "redirect:/baseInfoSkill/list.html?start=0";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * userSkillinfolist
	 */
	@RequestMapping(value = "/list")
	public String userSkillinfoList(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			String basicId = request.getParameter("basicId");
			createParameterMap(request);
			PageObject po = new PageObject();
			po = baseInfoSkillService.queryUserSkillinfoList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap", paramsMap);
			model.addAttribute("basicId", basicId);
			return "/manager/baseUser/baseSkill/list";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 删除userSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public String deleteUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			String basicId = request.getParameter("basicId");

			boolean flag = baseInfoSkillService.deleteUserSkillinfo(paramsMap);

			if (flag) {
				return "redirect:/baseInfoSkill/list.html?basicId=" + basicId;
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}

	}

	//auto-code-end

	//上传商品详情页图片
	@RequestMapping(value = "uploadImage", method = RequestMethod.POST)
	public @ResponseBody Map uploadImage(@RequestParam("imgs") MultipartFile[] imgs, HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (imgs == null || imgs.length <= 0) {
				map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
				map.put("msg", "图片不能为空！");
				return map;
			}
			List pics = new ArrayList();
			String imgPath = "";
			String targetPath = "";
			for (MultipartFile img : imgs) {

				String filename = URLEncoder.encode(img.getOriginalFilename(), "utf-8");
				DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				String name = df.format(new Date());
				Random r = new Random();
				for (int i = 0; i < 3; i++) {
					name += r.nextInt(10);
				}
				String savename = name + filename;

				boolean b = MultipartUploadSample.uploadOssImg(savename, img);

				String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
				String picUrl = beforUrl + savename;

				map.put("imgUrl", picUrl);// 相对路径，存数据库的。
				map.put("targetPath", picUrl);// 绝对路径，显示
				map.put("key", savename);// 绝对路径，显示
				//				map.put("filename", filename);
				pics.add(net.sf.json.JSONObject.fromObject(map));
			}
			String result = net.sf.json.JSONArray.fromObject(pics).toString();
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("pics", result);
		} catch (Exception e) {
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			map.put("msg", "上传失败！" + e.getMessage());
			e.printStackTrace();
		}
		return map;
	}

	@RequestMapping(value = "/getClassSByFid")
	public @ResponseBody Map getClassSByfid(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		Map map = new HashMap();
		List list = new ArrayList();
		list = baseInfoSkillService.getClassSBySid(paramsMap);
		map.put("classS", list);
		return map;
	}

	/**
	 * userSkillinfolist
	 */
	/**
	 * 
	 * 全部服务信息
	 * <p>
	 * wxl  2017年4月6日10:36:40  添加一级二级关联查询
	 * @param request
	 * @param response
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping(value = "/skillList")
	public String skillList(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			PageObject po = new PageObject();
			po = baseInfoSkillService.querySkillinfoList(paramsMap);
			model.addAttribute("po", po);
			List<Map<String, Object>> classF = baseInfoSkillService.getClassF();
			List<Map<String, Object>> classS = baseInfoSkillService.getClassSon(paramsMap);
			model.addAttribute("classF", classF);
			model.addAttribute("classS", classS);
			model.addAttribute("paramsMap", paramsMap);
			return "/manager/skill/skill_list";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 
	 * 根据一级分类id查二级分类
	 * <p>
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping(value = "/getClassSonByFatherId")
	public @ResponseBody Map<String, Object> getClassSonByFatherId(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String skillFatId = paramsMap.get("skillFatId").toString();
			List<Map<String, Object>> sonList = baseInfoSkillService.queryClassSonByFatherId(skillFatId);
			map.put("sonList", sonList);
			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	@RequestMapping(value = "/updateHome")
	public String updateHome(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			createParameterMap(request);
			String ishome = request.getParameter("ishome");
			if ("0".equals(ishome)) {
				paramsMap.put("ishome", 1);
				boolean b = baseInfoSkillService.updateHome(paramsMap);
			} else {
				paramsMap.put("ishome", 0);
				boolean b = baseInfoSkillService.updateHome(paramsMap);
			}
			return "redirect:/baseInfoSkill/skillList.html";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
		}
		return "/error";

	}

	/**
	 * 
	 * 去修改页面--------回显  
	 * <p>
	 * wxl
	 * 参数 id
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping(value = "/toUpdateSkill")
	public String toUpdateSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			String id = request.getParameter("id");

			List<Map<String, Object>> classF = baseInfoSkillService.getClassF();
			List<Map<String, Object>> classS = baseInfoSkillService.getClassS();
			//List<Map<String, Object>> images = baseInfoSkillService.getImages(id);
			List<Map<String, Object>> canpanyTypes = baseInfoSkillService.canpanyTypes();

			Map<String, Object> objMap = baseInfoSkillService.getById(paramsMap);
			model.addAttribute("objMap", objMap);
			model.addAttribute("classF", classF);
			model.addAttribute("classS", classS);
			//model.addAttribute("images", images);
			model.addAttribute("canpanyTypes", canpanyTypes);
			return "/manager/baseUser/baseSkill/edit";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 修改userSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateSkill")
	public String updateSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = baseInfoSkillService.updateUserSkillinfo(paramsMap);

			if (flag) {
				return "redirect:/baseInfoSkill/skillList.html";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 参数  id
	 * wxl
	 * @param request
	 * @param response
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping(value = "/deleteSkill")
	public String deleteSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = baseInfoSkillService.deleteUserSkillinfo(paramsMap);

			if (flag) {
				return "redirect:/baseInfoSkill/skillList.html";
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}

	}

}
