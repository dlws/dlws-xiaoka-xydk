/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.special.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.special.dao.SpecialDao;

@Service("specialService")
public class SpecialService {

	@Autowired
	private SpecialDao specialDao;

	public boolean addSpecial(Map<String, Object> map) {

		return specialDao.addSpecial(map);

	}

	public PageObject querySpecialList(Map<String, Object> map) {

		return specialDao.querySpecialList(map);

	}

	public boolean updateSpecial(Map<String, Object> map) {

		return specialDao.updateSpecial(map);

	}

	public boolean deleteSpecial(Map<String, Object> map) {

		return specialDao.deleteSpecial(map);

	}

	public boolean delSpecial(Map<String, Object> map) {

		return specialDao.delSpecial(map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return specialDao.getById(map);
	}

	/**
	 * 
	 * 项目名称：
	 * 描述：对应数据库设计时对应的下拉，单选框 值-名
	 * 创建人： 
	 * 创建时间：
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		return specialDao.querySelectValue(map);
	}

}
