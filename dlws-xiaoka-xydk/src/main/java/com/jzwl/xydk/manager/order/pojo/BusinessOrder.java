/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.order.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class BusinessOrder extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "BusinessOrder";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_BUSINESS_ID = "商家的id";
	public static final String ALIAS_USER_ID = "用户的id";
	public static final String ALIAS_START_DATE = "活动开始时间";
	public static final String ALIAS_END_DATE = "活动结束时间";
	public static final String ALIAS_CITY_ID = "城市的id";
	public static final String ALIAS_DEMAND = "活动的需求";
	public static final String ALIAS_PHONE = "电话号码";
	public static final String ALIAS_REMARK = "备注信息";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	public static final String ALIAS_OPEN_ID = "商家openId;";
	
	//date formats
	public static final String FORMAT_START_DATE = DATE_FORMAT;
	public static final String FORMAT_END_DATE = DATE_FORMAT;
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	

	//columns START
    /**
     * id       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 商家的id       db_column: businessId 
     */ 	
	private java.lang.Long businessId;
    /**
     * 用户的id       db_column: userId 
     */ 	
	private java.lang.Long userId;
    /**
     * 活动开始时间       db_column: startDate 
     */ 	
	private java.util.Date startDate;
    /**
     * 活动结束时间       db_column: endDate 
     */ 	
	private java.util.Date endDate;
    /**
     * 城市的id       db_column: cityId 
     */ 	
	private java.lang.Long cityId;
    /**
     * 活动的需求       db_column: demand 
     */ 	
	private java.lang.String demand;
    /**
     * 电话号码       db_column: phone 
     */ 	
	private java.lang.String phone;
    /**
     * 备注信息       db_column: remark 
     */ 	
	private java.lang.String remark;
    /**
     * 创建时间       db_column: createDate 
     */ 	
	private java.util.Date createDate;
    /**
     * 商家openId;       db_column: openId 
     */ 	
	private java.lang.String openId;
	//columns END

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	
	
	
	
	
	
	
	
	
	


	public java.lang.Long getBusinessId() {
		return this.businessId;
	}
	
	public void setBusinessId(java.lang.Long value) {
		this.businessId = value;
	}
	

	public java.lang.Long getUserId() {
		return this.userId;
	}
	
	public void setUserId(java.lang.Long value) {
		this.userId = value;
	}
	

	public String getStartDateString() {
		return dateToStr(getStartDate(), FORMAT_START_DATE);
	}
	public void setStartDateString(String value) {
		setStartDate(strToDate(value, java.util.Date.class,FORMAT_START_DATE));
	}

	public java.util.Date getStartDate() {
		return this.startDate;
	}
	
	public void setStartDate(java.util.Date value) {
		this.startDate = value;
	}
	

	public String getEndDateString() {
		return dateToStr(getEndDate(), FORMAT_END_DATE);
	}
	public void setEndDateString(String value) {
		setEndDate(strToDate(value, java.util.Date.class,FORMAT_END_DATE));
	}

	public java.util.Date getEndDate() {
		return this.endDate;
	}
	
	public void setEndDate(java.util.Date value) {
		this.endDate = value;
	}
	

	public java.lang.Long getCityId() {
		return this.cityId;
	}
	
	public void setCityId(java.lang.Long value) {
		this.cityId = value;
	}
	

	public java.lang.String getDemand() {
		return this.demand;
	}
	
	public void setDemand(java.lang.String value) {
		this.demand = value;
	}
	

	public java.lang.String getPhone() {
		return this.phone;
	}
	
	public void setPhone(java.lang.String value) {
		this.phone = value;
	}
	

	public java.lang.String getRemark() {
		return this.remark;
	}
	
	public void setRemark(java.lang.String value) {
		this.remark = value;
	}
	

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}
	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class,FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

	public java.lang.String getOpenId() {
		return this.openId;
	}
	
	public void setOpenId(java.lang.String value) {
		this.openId = value;
	}
	

}

