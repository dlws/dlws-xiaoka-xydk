package com.jzwl.xydk.manager.refunds.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.refunds.dao.RefundsDao;

@Service("refundsService")
public class RefundsService {

	@Autowired
	private RefundsDao refundsDao;

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：分页获取退款
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public PageObject queryRefundsforPage(Map<String, Object> paramsMap) {
		return refundsDao.queryRefundsforPage(paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryRefundsforList(Map<String, Object> paramsMap) {
		return refundsDao.queryRefundsforList(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：退款审核不通过
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void auditNotPass(Map<String, Object> paramsMap) {
		refundsDao.auditNotPass(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getSourceTypeByCode() {
		return refundsDao.getSourceTypeByCode();
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean refuseOrder(Map<String, Object> paramsMap) {
		boolean flag = false;
		try {
			flag = refundsDao.refuseOrder(paramsMap);
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean agreeRefund(Map<String, Object> map) {
		boolean flag = false;
		try {
			flag = refundsDao.agreeRefund(map);
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家拒绝退款
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean refundDenied(Map<String, Object> map) {
		boolean flag = false;
		try {
			flag = refundsDao.refundDenied(map);
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据订单ID查询服务名称
	 * 创建人： sx
	 * 创建时间： 2017年4月12日
	 * 标记：
	 * @param orderId
	 * @return
	 * @version
	 */
	public String findOrderDetailNikeNameByOrderId(Long orderId) {
		String serverName = "";
		List<Map<String, Object>> listMap = refundsDao.findOrderDetailNikeNameByOrderId(orderId);
		if (listMap != null && listMap.size() > 0) {
			for (Map<String, Object> map : listMap) {
				if (((String) map.get("typeName")).equals("skillId")) {
					serverName = (String) map.get("typeValue");
				}
			}
		}
		return serverName;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据订单id查询订单
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param orderId
	 * @return
	 * @version
	 */
	public Map<String, Object> findOrderById(Long orderId) {
		return refundsDao.findfindOrderById(orderId);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询钱包余额
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> findWalletBalanceByOpenId(String openId) {
		return refundsDao.findWalletBalanceByOpenId(openId);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：插入到分成表中
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param mapMoney
	 * @version
	 */
	public void divideOrWithDrawRecord(Map<String, Object> mapMoney) {
		refundsDao.divideOrWithDrawRecord(mapMoney);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更新钱包
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param mapMoney
	 * @version
	 */
	public void updateWallet(Map<String, Object> mapp, Map<String, Object> mapMoney) {
		refundsDao.updateWallet(mapp, mapMoney);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更改订单状态
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param orderMap
	 * @version
	 */
	public void updateOrderState(Map<String, Object> orderMap) {
		refundsDao.updateOrderState(orderMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家同意退款更新本条记录
	 * 创建人： sx
	 * 创建时间： 2017年4月13日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void updateRefunds(Map<String, Object> paramsMap) {
		refundsDao.updateRefunds(paramsMap);
	}

	/**
	 * 详情
	 * @param map
	 * @version
	 */
	public List<Map<String, Object>> details(Map<String, Object> map) {
		return refundsDao.details(map);
	}

}
