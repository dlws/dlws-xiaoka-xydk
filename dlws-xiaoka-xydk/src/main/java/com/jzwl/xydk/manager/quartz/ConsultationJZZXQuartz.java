package com.jzwl.xydk.manager.quartz;

import org.springframework.beans.factory.annotation.Autowired;

import com.jzwl.xydk.wap.xkh2_2.quartz.service.OrderQuartzService;

/**
 * 
 * 描述:下单支付成功后没有接单，超过24小时系统自动取消订单
 * 
 * 精准投放的定时任务也放在这里
 * 
 * 
 * @author    gyp
 * @Date	 2017年4月20日
 */
public class ConsultationJZZXQuartz {

	private int isOpen = 0;// 监测开关，默认关闭

	@Autowired
	private OrderQuartzService orderQuartzService;
	
	//定时器轮询方法
	public void executeInternal() throws Exception {
		if (isOpen == 1) {//定时器开关打开了
			serviceExpires_JZZX();
		}
	}
	
	private void serviceExpires_JZZX(){
		orderQuartzService.serviceExpires_JZZX();
	}
	
	public int getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(int isOpen) {
		this.isOpen = isOpen;
	}
}
