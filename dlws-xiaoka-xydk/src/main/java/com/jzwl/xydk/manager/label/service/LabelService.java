/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.label.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.label.dao.LabelDao;
import com.jzwl.xydk.manager.label.pojo.Label;
import com.jzwl.xydk.manager.label.pojo.SkillMapLabel;


@Service("labelService")
public class LabelService {
	
	
	@Autowired
	private LabelDao labelDao;
	
	public boolean addLabel(Map<String, Object> map) {
		
		return labelDao.addLabel(map);
		
	}

	public PageObject queryLabelList(Map<String, Object> map) {
		
		return labelDao.queryLabelList(map);
	
	}

	public boolean updateLabel(Map<String, Object> map) {
	
		return labelDao.updateLabel(map);
	
	}

	public boolean deleteLabel(Map<String, Object> map) {
		
		return labelDao.deleteLabel(map);
	
	}
	
	public boolean delLabel(Map<String, Object> map) {
		
		return labelDao.delLabel(map);
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String,Object> getById(Map<String, Object> map) {
		
		return labelDao.getById(map);
	}
	/**
	 * 
	 * 项目名称：
	 * 描述：对应数据库设计时对应的下拉，单选框 值-名
	 * 创建人： 
	 * 创建时间：
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String,Object>> querySelectValue(Map<String,Object> map){ 
		
		return labelDao.querySelectValue(map);
	}
	
	/**
	 * 传入参数为
	 * @param ids：此参数为，号分割的Id
	 * @return
	 */
	public List<Map<String,Object>> queryLabelById(String ids){ 
		
		return labelDao.queryLabelById(ids);
	}
	
	/**
	 * 查询当前技能表下的所有子技能与lable关联的表
	 */
	public List<Label> getSkillLable(String fatherId){
		
		return labelDao.getSkillLable(fatherId);
	}
	
	/**
	 * 查询当前技能表下的所有父类技能
	 * 获取所有技能和技能下的label标签
	 */
	public List<SkillMapLabel> getAllFathSkillLable(){
		
		List<SkillMapLabel> fatherLable = new ArrayList<SkillMapLabel>();
		
		//1.查询所有的父类技能
		List<Map<String,Object>>  allSkill = labelDao.getAllUserSkillFather();
		
		for (Map<String, Object> map : allSkill) {
			
			SkillMapLabel skillMapLabel = new SkillMapLabel();
			
			skillMapLabel.setSkill(map);
			
			//获取下面的子标签
			List<Label> listLabel = getSkillLable(map.get("id").toString());
			
			skillMapLabel.setLabel(listLabel);
			
			fatherLable.add(skillMapLabel);
			
		}
		
		return fatherLable;
	}
	
	/**
	 * 返回所有父类的skillId
	 * @return
	 */
	public List<Map<String,Object>> getAllUserSkillFather(){
		
		return labelDao.getAllUserSkillFather();
	}
	
	
}
