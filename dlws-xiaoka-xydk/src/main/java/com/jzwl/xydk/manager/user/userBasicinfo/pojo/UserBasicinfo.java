/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.user.userBasicinfo.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class UserBasicinfo extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "UserBasicinfo";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_USER_NAME = "用户名称";
	public static final String ALIAS_NICKNAME = "用户昵称";
	public static final String ALIAS_TYPENAME = "入驻类型";
	public static final String ALIAS_CITY_ID = "城市的id";
	public static final String ALIAS_SCHOOL_ID = "学校的id";
	public static final String ALIAS_PHONE_NUMBER = "联系电话";
	public static final String ALIAS_EMAIL = "邮箱地址";
	public static final String ALIAS_WX_NUMBER = "微信号";
	public static final String ALIAS_OPEN_ID = "openId";
	public static final String ALIAS_ABOUT_ME = "关于我的信息";
	public static final String ALIAS_AUDIT_STATE = "审核状态(0:未审核，1:审核成功，2:审核不通过）";
	public static final String ALIAS_AUDIT_DATE = "审核时间";
	public static final String ALIAS_AUDIT_USER = "审核人";
	public static final String ALIAS_AUDIT_NOTES = "审核信息备注";
	public static final String ALIAS_VIEW_NUM = "浏览量";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	public static final String ALIAS_CREATE_USER = "创建人";
	public static final String ALIAS_IS_RECOMMEND = "是否推荐（0：不推荐，1，推荐）";
	public static final String ALIAS_SKILL_STATUS = "空间状态(0:打开，1：关闭)";
	public static final String ALIAS_IS_DELETE = "是否删除（0：否，1：是）";
	
	//date formats
	public static final String FORMAT_AUDIT_DATE = DATE_FORMAT;
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	

	//columns START
    /**
     * id       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 用户名称       db_column: userName 
     */ 	
	private java.lang.String userName;
	/**
	 * 用户昵称       db_column: nickname 
	 */ 	
	private java.lang.String nickname;
	/**
	 * 入驻类型     db_column: typeName 
	 */ 	
	private java.lang.String typeName;
	
    public java.lang.String getNickname() {
		return nickname;
	}

	public void setNickname(java.lang.String nickname) {
		this.nickname = nickname;
	}

	public java.lang.String getTypeName() {
		return typeName;
	}

	public void setTypeName(java.lang.String typeName) {
		this.typeName = typeName;
	}

	/**
     * 城市的id       db_column: cityId 
     */ 	
	private java.lang.Long cityId;
    /**
     * 学校的id       db_column: schoolId 
     */ 	
	private java.lang.Long schoolId;
    /**
     * 联系电话       db_column: phoneNumber 
     */ 	
	private java.lang.String phoneNumber;
    /**
     * 邮箱地址       db_column: email 
     */ 	
	private java.lang.String email;
    /**
     * 微信号       db_column: wxNumber 
     */ 	
	private java.lang.String wxNumber;
    /**
     * openId       db_column: openId 
     */ 	
	private java.lang.String openId;
    /**
     * 关于我的信息       db_column: aboutMe 
     */ 	
	private java.lang.String aboutMe;
    /**
     * 审核状态(0:未审核，1:审核成功，2:审核不通过）       db_column: auditState 
     */ 	
	private java.lang.Integer auditState;
    /**
     * 审核时间       db_column: auditDate 
     */ 	
	private java.util.Date auditDate;
    /**
     * 审核人       db_column: auditUser 
     */ 	
	private java.lang.String auditUser;
    /**
     * 审核信息备注       db_column: auditNotes 
     */ 	
	private java.lang.String auditNotes;
    /**
     * 浏览量       db_column: viewNum 
     */ 	
	private java.lang.Integer viewNum;
    /**
     * 创建时间       db_column: createDate 
     */ 	
	private java.util.Date createDate;
    /**
     * 创建人       db_column: createUser 
     */ 	
	private java.lang.String createUser;
    /**
     * 是否推荐（0：不推荐，1，推荐）       db_column: isRecommend 
     */ 	
	private java.lang.Integer isRecommend;
    /**
     * 空间状态(0:打开，1：关闭)       db_column: skillStatus 
     */ 	
	private java.lang.Integer skillStatus;
    /**
     * 是否删除（0：否，1：是）       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
	
	private String cityName;
	
	private String schoolName;
	
	private String backPicture;// 背景图片
	//columns END

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public String getBackPicture() {
		return backPicture;
	}

	public void setBackPicture(String backPicture) {
		this.backPicture = backPicture;
	}

	public java.lang.Long getId() {
		return this.id;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public java.lang.String getUserName() {
		return this.userName;
	}
	
	public void setUserName(java.lang.String value) {
		this.userName = value;
	}
	

	public java.lang.Long getCityId() {
		return this.cityId;
	}
	
	public void setCityId(java.lang.Long value) {
		this.cityId = value;
	}
	

	public java.lang.Long getSchoolId() {
		return this.schoolId;
	}
	
	public void setSchoolId(java.lang.Long value) {
		this.schoolId = value;
	}
	

	public java.lang.String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	public void setPhoneNumber(java.lang.String value) {
		this.phoneNumber = value;
	}
	

	public java.lang.String getEmail() {
		return this.email;
	}
	
	public void setEmail(java.lang.String value) {
		this.email = value;
	}
	

	public java.lang.String getWxNumber() {
		return this.wxNumber;
	}
	
	public void setWxNumber(java.lang.String value) {
		this.wxNumber = value;
	}
	

	public java.lang.String getOpenId() {
		return this.openId;
	}
	
	public void setOpenId(java.lang.String value) {
		this.openId = value;
	}
	

	public java.lang.String getAboutMe() {
		return this.aboutMe;
	}
	
	public void setAboutMe(java.lang.String value) {
		this.aboutMe = value;
	}
	

	public java.lang.Integer getAuditState() {
		return this.auditState;
	}
	
	public void setAuditState(java.lang.Integer value) {
		this.auditState = value;
	}
	

	public String getAuditDateString() {
		return dateToStr(getAuditDate(), FORMAT_AUDIT_DATE);
	}
	public void setAuditDateString(String value) {
		setAuditDate(strToDate(value, java.util.Date.class,FORMAT_AUDIT_DATE));
	}

	public java.util.Date getAuditDate() {
		return this.auditDate;
	}
	
	public void setAuditDate(java.util.Date value) {
		this.auditDate = value;
	}
	

	public java.lang.String getAuditUser() {
		return this.auditUser;
	}
	
	public void setAuditUser(java.lang.String value) {
		this.auditUser = value;
	}
	

	public java.lang.String getAuditNotes() {
		return this.auditNotes;
	}
	
	public void setAuditNotes(java.lang.String value) {
		this.auditNotes = value;
	}
	

	public java.lang.Integer getViewNum() {
		return this.viewNum;
	}
	
	public void setViewNum(java.lang.Integer value) {
		this.viewNum = value;
	}
	

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}
	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class,FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

	public java.lang.String getCreateUser() {
		return this.createUser;
	}
	
	public void setCreateUser(java.lang.String value) {
		this.createUser = value;
	}
	

	public java.lang.Integer getIsRecommend() {
		return this.isRecommend;
	}
	
	public void setIsRecommend(java.lang.Integer value) {
		this.isRecommend = value;
	}
	

	public java.lang.Integer getSkillStatus() {
		return this.skillStatus;
	}
	
	public void setSkillStatus(java.lang.Integer value) {
		this.skillStatus = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

}

