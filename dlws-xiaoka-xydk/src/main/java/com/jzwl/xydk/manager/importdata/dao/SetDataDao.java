package com.jzwl.xydk.manager.importdata.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("setDataDao")
public class SetDataDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	/**
	 * 
	 * 描述:设置用户的信息
	 * 作者:gyp
	 * @Date	 2017年5月5日
	 */
	public List<Map<String,Object>> getBaseInfo(String enterId) {
		try {
			String sql = "SELECT t.id as id,t.aboutMe as aboutMe from `xiaoka-xydk`.user_basicinfo t where t.enterId = '"+enterId;
			List<Map<String,Object>> list = baseDAO.queryForList(sql);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/**
	 * 
	 * 描述:根据分类的Id获取到用户技能
	 * 作者:gyp
	 * @Date	 2017年5月12日
	 */
	public List<Map<String,Object>> getUserSkillInfo(String skillFatId) {
		try {
			String sql = "SELECT t.skillPrice as skillPrice,t.id as id,t.company as company  "
					+ " from `xiaoka-xydk`.user_skillinfo t where t.skillFatId = "+skillFatId;
			
			
			List<Map<String,Object>> list = baseDAO.queryForList(sql);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<Map<String,Object>> getUserSkillInfoTwo(String skillFatId) {
		try {
			String sql = "SELECT t.skillPrice as skillPrice,t.id as id,t.company as company  "
					+ " from `xiaoka-xydk`.user_skillinfo t where t.skillFatId = "+skillFatId;
			
			
			List<Map<String,Object>> list = baseDAO.queryForList(sql);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public Map<String,Object> getUserSkillInfoById(String id) {
		try {
			String sql = "SELECT *  "
					+ " from `xiaoka-xydk`.user_skillinfo t where t.id = "+id;
			
			
			Map<String,Object> map = baseDAO.queryForMap(sql);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 描述:获取图片的信息
	 * 作者:gyp
	 * @Date	 2017年5月12日
	 */
	public List<Map<String,Object>> getUserSkillImage(String skillId) {
		try {
			String sql = "SELECT * from `xiaoka-xydk`.user_skill_image t where t.skillId = "+skillId;
			
			List<Map<String,Object>> list = baseDAO.queryForList(sql);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 描述:获取用户入住时的技能
	 * 作者:gyp
	 * @Date	 2017年5月12日
	 */
	public List<Map<String,Object>> getUserSkillinfoData(String pid) {
		try {
			
			String sql = "SELECT * from `xiaoka-xydk`.user_skillinfo_data t where t.pid = '"+pid;
			
			List<Map<String,Object>> list = baseDAO.queryForList(sql);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Map<String, Object> addSkillInformation(Map<String, Object> map) {
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			map.put("id", Sequence.nextId());
			map.put("createDate", new Date());
			map.put("textURL", "");
			map.put("videoURL", "");
			String sql = " insert into `xiaoka-xydk`.user_skillinfo"
					+ "    (id,basicId,skillFatId,skillSonId,skillName,textURL,videoURL,skillPrice,serviceType,skillDepict,createDate,isDelete,company,isDisplay) "
					+ "    values "
					+ "    (:id,:basicId,:skillFatId,:skillSonId,:skillName,:textURL,:videoURL,:skillPrice,:serviceType,:skillDepict,:createDate,0,:company,0)";
			boolean flag = baseDAO.executeNamedCommand(sql, map);
			m.put("id", map.get("id"));
			m.put("flag", flag);
			return m;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * 描述:执行添加操作
	 * 作者:gyp
	 * @Date	 2017年5月12日
	 */
	public boolean addUserSkillInfoDetail(Map<String,Object> map) {
		
		boolean temp =false;
		try {
			String DataSql = "insert into `xiaoka-xydk`.user_skillinfo_detail(id,pid,typeName,typeValue,isDelete,createDate)"
					+ "values"
					+ "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
			temp = baseDAO.executeNamedCommand(DataSql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}
	
	/**
	 * subscribeScale   场地资源增加预
	 * 订金额比例字段  在key值得表中
	 * cfz
	 */
	public List<Map<String,Object>> searchUser_skillinfo(){
		
		try {
			
			List<Map<String,Object>>  list = new ArrayList<Map<String,Object>>();
		
			String sql ="select * from `xiaoka-xydk`.user_skillinfo us where us.skillFatId "
					+ "=(select id from `xiaoka-xydk`.user_skillclass_father  usf where usf.classValue='cdzy')";
			
			list = baseDAO.getJdbcTemplate().queryForList(sql);
			
			return list;
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	/**
	 * 插入数据到
	 * cfz
	 */
	public boolean insertDataForUser_skillinfo_detail(String userSkillId){
		
		Map<String,Object> map = new HashMap<String, Object>();
		
		String id = Sequence.nextId();
		map.put("id", id);
		map.put("pid",userSkillId);
		map.put("typeName","subscribeScale");
		map.put("typeValue","0.3");//因为数据库中为数值类型
		map.put("isDelete", 0);//新增数据默认未删除
		map.put("createDate", new Date());
		//插入数据到 user_skillinfo_detail 表中
		try {
			String sql = "insert into `xiaoka-xydk`.user_skillinfo_detail (id,pid,typeName,typeValue,isDelete,createDate) values (:id,:pid,:typeName,:typeValue,:isDelete,:createDate);";
			//执行当前插入数据操作
			if(!baseDAO.executeNamedCommand(sql, map)){
				System.out.println("-----------当前场地资源执行数据更新失败---------"+userSkillId+"---------------------");
				return false;
			}
		} catch (Exception e) {
			
			return false;
		}
		
		
		return true;
	}
	
	
	/**
	 * 查询当前字段User_skillinfo_detail 表中是否包含
	 * 要插入的字段
	 */
	public boolean searchDataForUser_skillinfo_detail(String userSkillId){
		
		String sql = "select typeName,typeValue from `xiaoka-xydk`.user_skillinfo_detail usd where usd.pid='"+userSkillId+"'";
		
		List<Map<String,Object>> list = baseDAO.getJdbcTemplate().queryForList(sql);
		
		//处理当前的list返回为Map  检查当前Map是否包含指定的key键
		
		Map<String,Object> map = dealList(list);
		
		if(map.containsKey("subscribeScale")){
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * 处理当前查询出来的参数 
	 * 将单条的
	 * 
	 */
	public Map<String,Object>  dealList(List<Map<String,Object>> list){
		
		Map<String,Object> dealResult = new HashMap<String, Object>();
		
		for (int i = 0; i < list.size(); i++) {
			
			String key = list.get(i).get("typeName").toString();
			dealResult.put(key,list.get(i).get("typeValue"));
			
		}
		return dealResult;
	}

}
