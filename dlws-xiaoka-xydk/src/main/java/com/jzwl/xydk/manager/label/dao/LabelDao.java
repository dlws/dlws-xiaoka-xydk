/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.label.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.manager.label.pojo.Label;



@Repository("labelDao")
public class LabelDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	
	public boolean addLabel(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		Date  date= new Date();
		map.put("id",  Sequence.nextId());
		map.put("createDate",date);
		map.put("isDelete","0");//默认为0是未删除
		String sql = "insert into  `xiaoka-xydk`.label " 
				 + " (id,enterId,labelName,ord,isDelete,createDate) " 
				 + " values "
				 + " (:id,:enterId,:labelName,:ord,:isDelete,:createDate)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	
	public String getColumns() {
		return ""
						+" id as id,"
						+" enterId as enterId,"
						+" labelName as labelName,"
						+" ord as ord,"
						+" isDelete as isDelete,"
						+" createDate as createDate"
				;
	}
	

	public PageObject queryLabelList(Map<String,Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性
		
		String sql="select " + getColumns() + " from `xiaoka-xydk`.label t where 1=1 ";
		
		  	
		  		if(null !=map.get("enterId") && StringUtils.isNotEmpty(map.get("enterId").toString())){
				  		sql=sql+ " and t.enterId  = " + map.get("enterId") +"";
			  	}
		  	
		  		if(null !=map.get("labelName") && StringUtils.isNotEmpty(map.get("labelName").toString())){
				  		sql=sql+ " and t.labelName  = " + map.get("labelName") +"";
			  	}
		  	
		  		if(null !=map.get("ord") && StringUtils.isNotEmpty(map.get("ord").toString())){
				  		sql=sql+ " and t.ord  = " + map.get("ord") +"";
			  	}
		  	
		  		if(null !=map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())){
				  		sql=sql+ " and t.isDelete  = " + map.get("isDelete") +"";
			  	}
		  	
		  		if(null !=map.get("createDateBegin") && StringUtils.isNotEmpty(map.get("createDateBegin").toString())){
	  				sql=sql+ " and t.createDate >= '" + map.get("createDateBegin") +"'";
		  		}
		  		if(null !=map.get("createDateEnd") && StringUtils.isNotEmpty(map.get("createDateEnd").toString())){
	  				sql=sql+ " and t.createDate <= '" + map.get("createDateEnd"  ) +"'";
		  		}
					
					
		sql=sql+ " order by id ";
		
		PageObject po = baseDAO.queryForMPageList(sql, new Object[]{},map);
		
		return po;
	}

	public boolean updateLabel(Map<String, Object> map) {
		
		Date  date= new Date();
		map.put("createDate",date);
		
		String sql ="update `xiaoka-xydk`.label set ";
				  	if(null !=map.get("enterId") && StringUtils.isNotEmpty(map.get("enterId").toString())){
					  	sql=sql+ "			enterId=:enterId ,";
				  	}
				  	if(null !=map.get("labelName") && StringUtils.isNotEmpty(map.get("labelName").toString())){
					  	sql=sql+ "			labelName=:labelName ,";
				  	}
				  	if(null !=map.get("ord") && StringUtils.isNotEmpty(map.get("ord").toString())){
					  	sql=sql+ "			ord=:ord ,";
				  	}
				  	if(null !=map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())){
					  	sql=sql+ "			isDelete=:isDelete ,";
				  	}
				  	if(null !=map.get("createDate") && StringUtils.isNotEmpty(map.get("createDate").toString())){
					  	sql=sql+ "			createDate=:createDate ,";
				  	}
				sql = sql.substring(0, sql.length()-1);
				sql = sql + " where id=:id";
		return  baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteLabel(Map<String, Object> map) {
		
		String sql = "delete from `xiaoka-xydk`.label where id in ("+ map.get("id")+ " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean delLabel(Map<String, Object> map) {
		
		String sql ="update `xiaoka-xydk`.label set "
				+ " isDelete =:"+map.get("isDelete")+" "
				+ " where id=:id";
		
		return  baseDAO.executeNamedCommand(sql, map);
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String,Object> getById(Map<String, Object> map) {
		
		String sql = "select " + getColumns() + " from `xiaoka-xydk`.label where id = "+ map.get("id") + "";
		
		return baseDAO.queryForMap(sql);
	}
	
	public List<Map<String,Object>> querySelectValue(Map<String,Object> map) {
		
		String sql = " SELECT dd.id,dd.dic_name "
				+ "    from dic_type  dt "
				+ "    LEFT JOIN  dic_data dd on dt.dic_id = dd.dic_id "
				+ "    where dt.dic_code = '"+map.get("code")+"'  and dd.isDelete = 0 ";
	 return	baseDAO.queryForList(sql);
	}
	
	public List<Map<String,Object>> queryLabelById(String ids){
		
		String sql = "select " + getColumns() + " from `xiaoka-xydk`.label where id in("+ids+")";
		
		 return	baseDAO.queryForList(sql);
	}
	
	/**
	 * 传入技能的父ID
	 * @param fatherId
	 * @return
	 */
	public List<Label> getSkillLable(String fatherId){
		
		try {
			String sql = "select l.id,l.enterId,l.labelName from `xiaoka-xydk`.label l"
					+ " where  l.enterId  LIKE '%"+fatherId+"%'";
			
			List<Label> listLabel = baseDAO.getJdbcTemplate().query(sql,new Object[]{}, 
					ParameterizedBeanPropertyRowMapper.newInstance(Label.class));
			
			return listLabel;
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
			
	}
	
	/**
	 * 获取所有的父类技能
	 * user_skillclass_father 
	 */
	public List<Map<String,Object>> getAllUserSkillFather(){
		
		try {
			String sql = "select t.id,t.className,t.classValue from `xiaoka-xydk`.user_skillclass_father t "
					+ "where t.isDelete = 0 and t.systemType = 0 and t.`status` = 1;";
			return baseDAO.getJdbcTemplate().queryForList(sql);
			
		} catch (Exception e) {
			return null;
		}
		
	}
 	
	
	
	

}
