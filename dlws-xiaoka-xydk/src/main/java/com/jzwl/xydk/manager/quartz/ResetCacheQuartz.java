package com.jzwl.xydk.manager.quartz;

import org.springframework.beans.factory.annotation.Autowired;

import com.jzwl.common.cache.RedisService;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.xydk.wap.xkh2_2.chat.dao.ChatMessageDao;

/**
 * 
 * 描述:更新缓存，每天凌晨两点进行设置为空
 * @author    gyp
 * @Date	 2017年5月5日
 */
public class ResetCacheQuartz {

	private int isOpen = 0;// 监测开关，默认关闭

	@Autowired
	private RedisService redisService;
	@Autowired
	private ChatMessageDao chatMessageDao;
	
	
	//定时器轮询方法
	public void executeInternal() throws Exception {
		if (isOpen == 1) {//定时器开关打开了
			redisService.del(GlobalConstant.CACHE_XKH_HOME_BANNER,GlobalConstant.CACHE_XKH_HOME_CLASS,
					GlobalConstant.CACHE_XKH_HOME_COUNT,GlobalConstant.CACHE_XKH_HOME_PROMOTE);
			//更新当前消息状态为已过期
			//chatMessageDao.updateMessageStatu();
		}
	}
	
	
	public int getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(int isOpen) {
		this.isOpen = isOpen;
	}
}
