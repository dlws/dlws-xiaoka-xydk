package com.jzwl.xydk.manager.messagepush.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * @author Administrator 信息推送Dao
 */
@Repository("messagePushDao")
public class MessagePushDao {

	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	/**
	 * 添加群发消息
	 */
	public boolean addMessage(Map<String, Object> pama) {
		Date date = new Date();
		pama.put("id", Sequence.nextId());
		pama.put("createDate", date);
		pama.put("isDelete", "0");
		pama.put("isSend", "1");// 1表示未发送

		String sql = "insert into `xiaoka-xydk`.mass_information (id,title,content,createDate,remark,isDelete,isSend) values"
				+ "(:id,:title,:content,:createDate,:remark,:isDelete,:isSend)";

		return baseDAO.executeNamedCommand(sql, pama);
	}

	/**
	 * 分页展示当前未删除需要推送的消息
	 */
	public PageObject queryMessforPage(Map<String, Object> map) {
		String sql = "select id,title,content,createDate,remark,isSend from `xiaoka-xydk`.mass_information t where 1=1  and t.isDelete=0";
		// 后续有需求再拼接SQL
		if (map != null) {
			if (map.get("isSend") != null
					&& StringUtils.isNotEmpty(map.get("isSend").toString())) {
				sql = sql + " and t.isSend = " + map.get("isSend");// 是否发送
			}
			if (map.get("title") != null
					&& StringUtils.isNotEmpty(map.get("title").toString())) {
				sql = sql + " and t.title  like '%" + map.get("title") + "%' ";
			}
		}
		sql = sql + " order by id ";
		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	/**
	 * 修改时使用，展示用户当前的信息
	 */
	public List<Map<String, Object>> queryMessforList(Map<String, Object> map) {

		String sql = "select id,title,content,createDate,remark,isSend from `xiaoka-xydk`.mass_information t where 1=1  and t.isDelete=0";

		if (map != null) {
			if (map.get("id") != null
					&& StringUtils.isNotEmpty(map.get("id").toString())) {
				sql = sql + " and t.id = " + map.get("id");// 指定当前的Id值
			}
		}

		return baseDAO.getJdbcTemplate().queryForList(sql);
	}

	/**
	 * 根据ID值进行修改当前的推送消息的信息
	 */
	public boolean updateMessage(Map<String, Object> pama) {

		Date date = new Date();
		pama.put("createDate", date);
		pama.put("isDelete", "0");
		pama.put("isSend", "1");// 1表示未发送
		String sql = "update `xiaoka-xydk`.mass_information  "
				+ "set title=:title,content=:content,createDate=:createDate,"
				+ "remark=:remark,isDelete=:isDelete,isSend=:isSend where id=:id";

		return baseDAO.executeNamedCommand(sql, pama);
	}

	/**
	 * 根据ID值更改当前数据状态表示删除
	 */
	public boolean deleteMessage(Map<String, Object> pama) {

		pama.put("isDelete", "1");
		String sql = "update `xiaoka-xydk`.mass_information  "
				+ "set isDelete=:isDelete where id=:id";

		return baseDAO.executeNamedCommand(sql, pama);
	}

	/**
	 * 根据ID值更改当前数据状态表示发送
	 */
	public boolean sendMessage(Map<String, Object> pama) {

		pama.put("isSend", "0");// 1表示未发送 更改为已发送
		String sql = "update `xiaoka-xydk`.mass_information  "
				+ "set isSend=:isSend where id=:id";

		return baseDAO.executeNamedCommand(sql, pama);
	}

	public Map<String, Object> getMassInformation(Map<String, Object> map) {

		String sql = "select id,title,content,createDate,remark,isSend from `xiaoka-xydk`.mass_information t where 1=1  and t.isDelete=0";

		if (map != null) {
			if (map.get("id") != null
					&& StringUtils.isNotEmpty(map.get("id").toString())) {
				sql = sql + " and t.id = " + map.get("id");// 指定当前的Id值
			}
		}

		return baseDAO.queryForMap(sql);

	}
}
