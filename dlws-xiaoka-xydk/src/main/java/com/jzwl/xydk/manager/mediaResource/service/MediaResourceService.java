/**
 * MediaResourceService.java
 * com.jzwl.xydk.manager.mediaResource.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.mediaResource.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.mediaResource.dao.MediaResourceDao;

/**
 *	自媒体资源Service
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月24日 	 
 */
@Service
public class MediaResourceService {
	@Autowired
	private MediaResourceDao mediaResourceDao;

	/**
	 * 列表
	 *
	 * @param map map对象
	 * @return 页面所需信息
	 */
	public PageObject list(Map<String, Object> paramsMap) {

		return mediaResourceDao.list(paramsMap);

	}
}
