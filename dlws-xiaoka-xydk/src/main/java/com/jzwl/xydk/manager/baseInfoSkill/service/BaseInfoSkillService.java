/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.baseInfoSkill.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.baseInfoSkill.dao.BaseInfoSkillDao;

@Service("baseInfoSkillService")
public class BaseInfoSkillService {

	@Autowired
	private BaseInfoSkillDao baseInfoSkillDao;
	
	public String addUserSkillinfo(Map<String, Object> map) {
		
		return baseInfoSkillDao.addUserSkillinfo(map);

	}

	public PageObject queryUserSkillinfoList(Map<String, Object> map) {

		return baseInfoSkillDao.queryUserSkillinfoList(map);

	}

	public boolean updateUserSkillinfo(Map<String, Object> map) {

		return baseInfoSkillDao.updateUserSkillinfo(map);

	}

	public boolean deleteUserSkillinfo(Map<String, Object> map) {

		return baseInfoSkillDao.deleteUserSkillinfo(map);

	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return baseInfoSkillDao.getById(map);

	}

	public List<Map<String, Object>> getClassF() {

		return baseInfoSkillDao.getClassF();
	}

	public List<Map<String, Object>> getClassS() {

		return baseInfoSkillDao.getClassS();
	}

	public List<Map<String, Object>> getImages(String id) {

		return baseInfoSkillDao.getImages(id);
	}

	public List getClassSBySid(Map<String, Object> map) {

		return baseInfoSkillDao.getClassSBySid(map);
	}

	public boolean addImage(String answer, String basicId) {

		return baseInfoSkillDao.addImage(answer, basicId);

	}

	public boolean deleteImage(Map<String, Object> map) {
		return baseInfoSkillDao.deleteImage(map);
	}

	public PageObject querySkillinfoList(Map<String, Object> map) {

		return baseInfoSkillDao.querySkillinfoList(map);

	}

	public boolean updateHome(Map<String, Object> paramsMap) {

		// TODO Auto-generated method stub
		return baseInfoSkillDao.updateHome(paramsMap);

	}

	public List<Map<String, Object>> canpanyTypes() {

		return baseInfoSkillDao.canpanyTypes();

	}

	public List<Map<String, Object>> getCanpanyMap() {

		// TODO Auto-generated method stub
		return baseInfoSkillDao.getCanpanyMap();

	}

	public List<Map<String, Object>> queryClassSonByFatherId(String skillFatId) {

		// TODO Auto-generated method stub
		return baseInfoSkillDao.queryClassSonByFatherId(skillFatId);

	}

	public List<Map<String, Object>> getClassSon(Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		return baseInfoSkillDao.getClassSon(paramsMap);

	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> querySkillById(Map<String, Object> map) {
		return baseInfoSkillDao.querySkillById(map);
	}
	
	public List<Map<String, Object>> querySettleList(Map<String, Object> paramsMap) {
		// TODO Auto-generated method stub
		return baseInfoSkillDao.querySettleList(paramsMap);
	}
	
	

}
