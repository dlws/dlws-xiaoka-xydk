/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.special.controller;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.upload.MultipartUploadSample;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.special.service.SpecialService;

/**
 * Special
 * Special
 * special
 * <p>Title:SpecialController </p>
 * 	<p>Description:Special </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller
@RequestMapping("/special")
public class SpecialController extends BaseController {

	@Autowired
	private SpecialService specialService;

	@Autowired
	private Constants constants;

	/**
	 * 跳转到添加Special的页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddSpecial")
	public ModelAndView toAddspecial(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		mov.setViewName("/manager/special/add");
		return mov;
	}

	/**
	 * 添加Special
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addSpecial")
	public ModelAndView addspecial(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			boolean flag = specialService.addSpecial(paramsMap);
			if (!flag) {
				mov.addObject("msg", "添加Special失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/special/specialList.html");
			}
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "添加Special失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * speciallist
	 */
	@RequestMapping(value = "/specialList")
	public ModelAndView specialList(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		PageObject po = new PageObject();

		try {

			po = specialService.querySpecialList(paramsMap);

			mov.addObject("list", po.getDatasource());
			mov.addObject("totalProperty", po.getTotalCount());
			mov.addObject("po", po);
			mov.addObject("paramsMap", paramsMap);
			mov.setViewName("/manager/special/list");
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "special查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 跳转到修改special页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toUpdateSpecial")
	public ModelAndView toUpdateSpecial(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			Map<String, Object> special = specialService.getById(paramsMap);
			mov.addObject("obj", special);
			mov.setViewName("/manager/special/update");
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "special查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 跳转到详情special页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/detailSpecial")
	public ModelAndView detailSpecial(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			Map<String, Object> special = specialService.getById(paramsMap);
			mov.addObject("obj", special);
			mov.setViewName("/manager/special/detail");
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "special查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 修改special
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateSpecial")
	public ModelAndView updateSpecial(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = specialService.updateSpecial(paramsMap);

			if (!flag) {
				mov.addObject("msg", "修改失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/special/specialList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "修改失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 逻辑删除special
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delSpecial")
	public ModelAndView delSpecial(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = specialService.deleteSpecial(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/special/specialList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	/**
	 * 物理删除special
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteSpecial")
	public ModelAndView deleteSpecial(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = specialService.deleteSpecial(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/special/specialList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	//auto-code-end

	//上传图片
	@RequestMapping(value = "picture", method = RequestMethod.POST)
	public @ResponseBody Map uploadLogo(@RequestParam(required = false) MultipartFile pic, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Map map = new HashMap();
		String filename = URLEncoder.encode(pic.getOriginalFilename(), "utf-8");
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String name = df.format(new Date());
		Random r = new Random();
		for (int i = 0; i < 3; i++) {
			name += r.nextInt(10);
		}
		String savename = name + filename;

		boolean b = MultipartUploadSample.uploadOssImg(savename, pic);

		String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
		String picUrl = beforUrl + savename;
		map.put("imgerUrl", picUrl);// picUrl 传阿里云服务器上的全路径
		map.put("key", savename);// key值
		map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);

		return map;
	}

	//		/**
	//		 * 判断轮播图名称是否正确
	//		 */
	//		@ResponseBody
	//		@RequestMapping(value = "/judgeRepeat", method = RequestMethod.POST)
	//		public Map<String, Object> judgeRepeat(HttpServletRequest request, HttpServletResponse response) {
	//			createParameterMap(request);
	//			Map<String, Object> resultMap = new HashMap<String, Object>();
	//			try {
	//				Map<String, Object> objMap = bannerInfoService.getByName(paramsMap);
	//
	//				if (objMap.isEmpty()) {
	//					resultMap.put("isEmpty", "yes");
	//				} else {
	//					resultMap.put("isEmpty", "no");
	//				}
	//
	//			} catch (Exception e) {
	//				// TODO Auto-generated catch block
	//				e.printStackTrace();
	//				resultMap.put("info", "数据错误");
	//				resultMap.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
	//			}
	//
	//			return resultMap;
	//		}

}
