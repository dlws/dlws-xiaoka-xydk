package com.jzwl.xydk.manager.draw.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("drawDao")
public class DrawDao {

	@Autowired
	private BaseDAO baseDAO;
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public PageObject queryDrawforPage(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select ub.phoneNumber,wt.balance,c.name,t.id,t.openId,t.withdrawMoney,t.applicationTime,t.applicationStatus,t.checkPeople,"
				+ "t.checkTime,t.createTime,t.bank,t.bankCardNumber,t.AccountFullName "
				+ "from `xiaoka-xydk`.application_withdraw t left join v2_wx_customer c on t.openId = c.openId "
				+ " left join `xiaoka-xydk`.wallet wt on wt.openId = t.openId "
				+ " left join `xiaoka-xydk`.user_basicinfo ub on ub.openId = t.openId where 1=1  ");
		if(null !=map.get("name") && StringUtils.isNotEmpty(map.get("name").toString())){
			sb.append(" and c.name like '%" + map.get("name") +"%'");
	  	}
		if(null !=map.get("applicationStatus") && StringUtils.isNotEmpty(map.get("applicationStatus").toString())){
			sb.append(" and t.applicationStatus =" + map.get("applicationStatus"));
	  	}
		if(null !=map.get("bankCardNumber") && StringUtils.isNotEmpty(map.get("bankCardNumber").toString())){
			sb.append(" and t.bankCardNumber like '%" + map.get("bankCardNumber") +"%'");
	  	}
		if (null != map.get("beginDate") && StringUtils.isNotEmpty(map.get("beginDate").toString())) {
			sb.append(" and  DATE_FORMAT(t.createTime,'%Y-%m-%d') >= DATE_FORMAT('"+map.get("beginDate")+"','%Y-%m-%d')");
		}
		if (null != map.get("endDate") && StringUtils.isNotEmpty(map.get("endDate").toString())) {
			sb.append(" and  DATE_FORMAT(t.createTime,'%Y-%m-%d') <= DATE_FORMAT('"+map.get("endDate")+"','%Y-%m-%d')");
		}
		sb.append(" and ub.isDelete = '0' order by createTime desc");
		PageObject po = baseDAO.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> queryDrawforList(
			Map<String, Object> paramsMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("select c.name, t.id, t.openId, t.withdrawMoney, t.applicationTime, t.checkPeople, t.applicationStatus,t.checkTime,"
				+ " t.createTime, t.bank, t.bankCardNumber, t.AccountFullName from `xiaoka-xydk`.application_withdraw t "
				+ "left join v2_wx_customer c on t.openId = c.openId where id= '" + paramsMap.get("id")+"'");
		return baseDAO.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public boolean auditNotPass(Map<String, Object> map) {
		map.put("checkTime", new Date());
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.application_withdraw set applicationStatus='2',"
				+ "checkTime=:checkTime,refuseReason=:refuseReason,checkPeople=:userName where id = :id");
		return baseDAO.executeNamedCommand(sb.toString(), map);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：用户点击提现
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param drawMap
	 * @return
	 * @version
	 */
	public boolean addDraw(Map<String, Object> drawMap) {
		String id = Sequence.nextId();
		drawMap.put("id", id);
		drawMap.put("applicationTime", new Date());
		drawMap.put("createTime", new Date());
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.application_withdraw "
				+ "(id,openId,withdrawMoney,applicationTime,applicationStatus,createTime,bank,bankCardNumber,AccountFullName) "
				+ "values (:id,:openId,:withdrawMoney,:applicationTime,0,:createTime,:bank,:bankCardNumber,:accountFullName)");
		return baseDAO.executeNamedCommand(sb.toString(), drawMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getWalletBalanceByOpenId(String openId) {
		Map<String, Object> map = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select id, openId, balance, createTime from `xiaoka-xydk`.wallet where openId = '"+openId+"'");
		List<Map<String, Object>> walletList = baseDAO.queryForList(sb.toString());
		if(walletList != null && walletList.size() > 0){
			map = walletList.get(0);
		}
		return map;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更新钱包
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param moneyMap
	 * @version
	 */
	public void updateWallet(Map<String, Object> moneyMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.wallet set balance = :money where openId = :openId ");
		baseDAO.executeNamedCommand(sb.toString(), moneyMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：插入记录表
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param withMap
	 * @version
	 */
	public void insertWithDraw(Map<String, Object> withMap) {
		String id = Sequence.nextId();
		withMap.put("id", id);
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.divideorwithdrawrecord "
				+ "(id,openId,divideOrWithdraw,balanceMoney,state,divideOrWithdrawTime,orderId) "
				+ "values (:id,:openId,:divideOrWithdraw,:money,1,:divideOrWithdrawTime,:orderId)");
		baseDAO.executeNamedCommand(sb.toString(), withMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：审核通过修改本身状态
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param paramsMap
	 * @version
	 */
	public void auditPass(Map<String, Object> paramsMap) {
		paramsMap.put("checkTime", new Date());
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.application_withdraw set applicationStatus='1',checkTime=:checkTime,checkPeople=:userName where id = :id");
		baseDAO.executeNamedCommand(sb.toString(), paramsMap);
	}

}
