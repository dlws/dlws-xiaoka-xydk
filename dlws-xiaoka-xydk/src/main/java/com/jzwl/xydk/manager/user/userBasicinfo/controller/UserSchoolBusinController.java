package com.jzwl.xydk.manager.user.userBasicinfo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.constant.Constants;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.user.userBasicinfo.pojo.UserSkillinfoData;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserEnterInfoService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;


/**
 * 
 * @author Administrator
 * 校内商家的Controller
 * 2017年3月13日9:25:10
 */
@Controller
@RequestMapping(value="/schoolBusin")
public class UserSchoolBusinController extends BaseWeixinController{

	
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private UserEnterInfoService userenterInfoService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private Constants constants;
	
	private final String BUSINTYPE ="businessType";//校园商家类型
	private final String COMPARE_SIZE ="cooperaType";//合作类型
	
	/**
	 * 去添加用户信息入驻（加载基本信息） 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddSchoolBusin")
	public String toAddUserBasicinfo(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		
		createParameterMap(request);
		//获取所有的城市，从xiaoka中进行获取
		List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();
		//获取省列表
		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
		//加载当前的场地类型（字典表）fieldtype
		List<Map<String, Object>> businTypeList = userBasicinfoService.getFieldtype(BUSINTYPE);//校园商家类型
		//加载当前的场地规模（字典表） filedsize
		List<Map<String, Object>> cooperaTypeList = userBasicinfoService.getFieldtype(COMPARE_SIZE);//合作类型
		
		
		model.addAttribute("paramsMap", paramsMap);//enterId的传参方式更改
		model.addAttribute("cityList", cityList);
		model.addAttribute("cityList", cityList);
		model.addAttribute("provnceList", provnceList);//省市
		model.addAttribute("businTypeList", businTypeList);//商家类型
		model.addAttribute("cooperaTypeList", cooperaTypeList);//合作形式
		return "/manager/campuMerch/add_school_merch";
	}
	/**
	 * 添加新增的用户
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addSchoolBusin")
	public String addSchoolBusin(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		String cooperaTypeJson="";  //合作形式和价格
		ModelAndView mov = new ModelAndView();
		//获取页面中的所有参数
		Map<String, Object> map = createParameterMap(request);
		if(request.getParameterValues("cooperaType")!=null&&request.getParameterValues("cooperaPrice")!=null){//设置当前场地的属性
			
			String [] cooperaTyArray = request.getParameterValues("cooperaType");//合作形式
			String [] cooperaPrArray = request.getParameterValues("cooperaPrice");//报价
			
		    cooperaTypeJson = StringUtil.dealArray(cooperaTyArray,cooperaPrArray);
		}
		map.put("cooperaType",cooperaTypeJson);
		System.out.println(cooperaTypeJson);
		//覆盖当前数组类参数
		if(request.getParameterValues("closePhoto")!=null&&request.getParameterValues("middlePho")!=null&&request.getParameterValues("allPhoto")!=null){
			map.put("closePhoto", StringUtil.dealArray(request.getParameterValues("closePhoto")));//近景
			map.put("middlePho", StringUtil.dealArray(request.getParameterValues("middlePho")));//中景
			map.put("allPhoto", StringUtil.dealArray(request.getParameterValues("allPhoto")));//远景
		}else{
			System.out.println("近景，中景，远景照片不能为空  且每个种类照片不能小于两张");
		}
		//处理参数
		String userName = String.valueOf(request.getSession().getAttribute("userName"));
		if(!"".equals(userName)){
			paramsMap.put("createUser", userName);
		}else{
			paramsMap.put("createUser", "admin");
		}
		boolean flag = userBasicinfoService.addSettleUserVersion3(map);
		
		return "redirect:/userBasicinfo/settleList.html?start=0";
		
	}
	
	/**
	 * 前往修改用户的回显页面
	 * 加载用户的基本信息
	 * 
	 */
	@RequestMapping(value="toUpdateSchoolBusin")
	public String toUpdateSchoolBusin(HttpServletRequest request, HttpServletResponse response,Model model){
		
		Map<String, Object> map = createParameterMap(request);
		
		Map<String,String> echoMap = new HashMap<String, String>();
		//加载个人基本信息
		Map<String,Object> objMap = userBasicinfoService.getById(map);//获取当前的个人信息
		//获取所有的城市，从xiaoka中进行获取
		List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();
				//获取省列表
		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
				//加载当前的场地类型（字典表）fieldtype
		List<Map<String, Object>> businTypeList = userBasicinfoService.getFieldtype(BUSINTYPE);//校园商家类型
				//加载当前的场地规模（字典表） filedsize
		List<Map<String, Object>> cooperaTypeList = userBasicinfoService.getFieldtype(COMPARE_SIZE);//合作类型
		
		List<UserSkillinfoData> list = userenterInfoService.querySettleListVersion3(paramsMap);
		
		for (UserSkillinfoData userSkillinfoData : list) {
			echoMap.put(userSkillinfoData.getTypeName(), userSkillinfoData.getTypeValue());
		}
		List listClose = dealString(echoMap.get("closePhoto"));
		List listMiddle = dealString(echoMap.get("middlePho"));
		List listAll = dealString(echoMap.get("allPhoto"));
	    JSONObject  jasonObject = JSONObject.fromObject(echoMap.get("cooperaType"));
	    Map<String,Object> cooperaTypes = (Map)jasonObject;
		model.addAttribute("objMap",objMap);//近景
		model.addAttribute("listClose",listClose);//近景
		model.addAttribute("listMiddle",listMiddle);//中景
		model.addAttribute("listAll",listAll);//远景
		model.addAttribute("echoMap",echoMap);//个性化参数
		model.addAttribute("cooperaTypes",cooperaTypes);//显示其中的场地及资金
		model.addAttribute("cityList", cityList);//城市
		model.addAttribute("provnceList", provnceList);//省市
		model.addAttribute("businTypeList", businTypeList);//商家类型
		model.addAttribute("cooperaTypeList", cooperaTypeList);//合作形式
		
		return "/manager/campuMerch/edite_school_merch";
	}
	
	/**
	 * 前往修改用户的回显页面
	 * 
	 */
	@RequestMapping(value = "/updateSchoolBusin")
	public String updateSchoolBusin(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		String cooperaTypeJson="";  //合作形式和价格
		//获取传过来的用户Id 以及页面中修改的参数
		Map<String, Object> map = createParameterMap(request);
		//设置当前场地的属性
		if(request.getParameterValues("cooperaType")!=null&&request.getParameterValues("cooperaPrice")!=null){//设置当前场地的属性
			String [] cooperaTyArray = request.getParameterValues("cooperaType");//合作形式
			String [] cooperaPrArray = request.getParameterValues("cooperaPrice");//报价
		    cooperaTypeJson = StringUtil.dealArray(cooperaTyArray,cooperaPrArray);
		}
		map.put("cooperaType",cooperaTypeJson);
		//覆盖当前数组类参数
		if(request.getParameterValues("closePhoto")!=null&&request.getParameterValues("middlePho")!=null&&request.getParameterValues("allPhoto")!=null){
			map.put("closePhoto", StringUtil.dealArray(request.getParameterValues("closePhoto")));//近景
			map.put("middlePho", StringUtil.dealArray(request.getParameterValues("middlePho")));//中景
			map.put("allPhoto", StringUtil.dealArray(request.getParameterValues("allPhoto")));//远景
		}else{
			System.out.println("近景，中景，远景照片不能为空");
		}
		String userName = String.valueOf(request.getSession().getAttribute("userName"));
		if(!"".equals(userName)){
			paramsMap.put("createUser", userName);
		}else{
			paramsMap.put("createUser", "admin");
		}
		boolean flage = userBasicinfoService.upSettleVersion3(map);
		
		if(flage){
			return "redirect:/userBasicinfo/settleList.html?start=0";  //修改成功
		}else{
			return "redirect:/userBasicinfo/settleList.html?start=0";	//修改失败
		}
	}
	

	/**
	 * 将以，号分割的STring 串以list的形式进行返回
	 * @param str
	 * @return
	 */
	public List<String> dealString(String str){
		 
		List<String> list = new ArrayList<String>();
		String[] arre = str.split(",");
		for (String string : arre) {
			list.add(string);
		}
		return list;
	}
	

}
