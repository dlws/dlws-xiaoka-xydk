package com.jzwl.xydk.manager.publish.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.publish.service.ManagerPublishService;
@Controller
@RequestMapping("/managerPublish")
public class ManagerPublishController extends BaseWeixinController{

	@Autowired
	private ManagerPublishService managerPublishService;
	
	@RequestMapping(value = "/list")
	public String list(HttpServletRequest request, HttpServletResponse response,Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = managerPublishService.queryManagerPublishforPage(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/publish/list";
	}
}
