/**
 * DiplomatistDao.java
 * com.jzwl.xydk.manager.diplomatist.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.diplomatist.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * 外交官推广后台Dao
 *
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月12日 	 
 */
@Repository("diplomatistDao")
public class DiplomatistDao {

	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	/**
	 * 列表
	 *
	 * @param map map对象
	 * @return 页面所需信息
	 */
	public PageObject list(Map<String, Object> map) {

		StringBuffer sb = new StringBuffer();
		sb.append(" select d.id,d.userName,d.cityName,d.schoolName,d.phone,d.wxNumber FROM `xiaoka-xydk`.diplomatinfo d "
				+ "LEFT JOIN `xiaoka-xydk`.diplomatsourceinfo f on d.openId=f.diplomatOpenId "
				+ "LEFT JOIN `xiaoka-xydk`.user_basicinfo u on f.sourceUserOpenId=u.openId " + " where 1=1");
		if (null != map.get("cityName") && StringUtils.isNotEmpty(map.get("cityName").toString())) {
			sb.append(" and d.cityName like '%" + map.get("cityName") + "%' ");
		}
		if (null != map.get("schoolName") && StringUtils.isNotEmpty(map.get("schoolName").toString())) {
			sb.append(" and d.schoolName like '%" + map.get("schoolName") + "%' ");
		}
		sb.append(" order by d.createTime ");
		PageObject po = baseDAO.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;
	}

	/**
	 * 详情
	 * 
	 * @param map对象
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> details(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select d.userName,d.cityName,d.schoolName,d.phone,d.wxNumber FROM `xiaoka-xydk`.diplomatinfo d "
				+ "LEFT JOIN `xiaoka-xydk`.diplomatsourceinfo f on d.openId=f.diplomatOpenId "
				+ "LEFT JOIN `xiaoka-xydk`.user_basicinfo u on " + "f.sourceUserOpenId=u.openId where d.id= '"
				+ map.get("id") + "'");
		return baseDAO.queryForList(sb.toString());

	}
}
