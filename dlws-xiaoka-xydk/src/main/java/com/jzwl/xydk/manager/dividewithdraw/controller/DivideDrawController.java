package com.jzwl.xydk.manager.dividewithdraw.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.dividewithdraw.service.DivideDrawService;
/**
 * @author Administrator
 * 记录表
 */
@Controller
@RequestMapping(value = "/dividedraw")
public class DivideDrawController extends BaseWeixinController{
	@Autowired
	private DivideDrawService divideDrawService;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：记录表分页查询
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/divideDrawList")
	public String divideDrawList(HttpServletRequest request, HttpServletResponse response,Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = divideDrawService.queryDivideDrawforPage(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/dividedraw/dividedraw_list";
	}
}
