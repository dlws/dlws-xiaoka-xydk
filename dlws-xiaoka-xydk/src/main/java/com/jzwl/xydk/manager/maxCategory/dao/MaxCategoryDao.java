package com.jzwl.xydk.manager.maxCategory.dao;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("maxCategoryDao")
public class MaxCategoryDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：一级技能分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public PageObject maxCategoryList(Map<String, Object> map) {
		try {
			String sql = " SELECT sf.id,sf.className,sf.imageURL,sf.`status`,sf.ord,sf.classValue,sf.createDate "
					+ "   FROM `xiaoka-xydk`.user_skillclass_father sf " + "   where isDelete = 0 and sf.systemType = 0";
			if (null != map.get("className") && StringUtils.isNotEmpty(map.get("className").toString())) {
				sql = sql + " and sf.className like '%" + map.get("className") + "%'";
			}
			sql = sql + " order by sf.createDate desc ";
			PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);
			return po;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去修改一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public Map<String, Object> toEditMaxCategory(Map<String, Object> map) {
		try {
			String sql = " SELECT sf.id,sf.className,sf.imageURL,sf.selectimage,sf.noSelectimage,sf.selectimage,sf.noSelectimage,sf.`status`,sf.ord,sf.classValue,sf.createDate "
					+ "   FROM `xiaoka-xydk`.user_skillclass_father sf " + "   where sf.isDelete = 0 and sf.id="
					+ map.get("id") + "";
			return baseDAO.queryForMap(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：添加一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean addMaxCategory(Map<String, Object> map) {
		try {
			map.put("id", Sequence.nextId());
			map.put("createDate", new Date());
			String sql = " insert into `xiaoka-xydk`.user_skillclass_father"
					+ "    (id,className,imageURL,selectimage,noSelectimage,status,ord,createDate,classValue,isDelete) " + "    values "
					+ "    (:id,:className,:imageURL,:selectimage,:noSelectimage,:status,:ord,:createDate,:classValue,0)";
			baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean editMaxCategory(Map<String, Object> map) {
		try {
			String sql = "update `xiaoka-xydk`.user_skillclass_father set "
					+ " className=:className,imageURL=:imageURL,selectimage=:selectimage,noSelectimage=:noSelectimage,ord=:ord,classValue=:classValue,status=:status"
					+ " where id=:id";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：删除以及分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean deleteMaxCategory(Map<String, Object> map) {
		try {
			String sql = "update `xiaoka-xydk`.user_skillclass_father set " + " isDelete= 1 " + " where id=:id";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
