/**
 * SchoolShopsResourceDao.java
 * com.jzwl.xydk.manager.schoolShopsResource.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.schoolShopsResource.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

/**
 *	校内商家Dao
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月24日 	 
 */
@Repository
public class SchoolShopsResourceDao {
	@Autowired
	private BaseDAO baseDao;

	/**
	 *	列表
	 * @param map
	 */
	public PageObject list(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT us.serviceType,us.skillName,us.skillDepict,us.skillPrice,c.cityName,si.schoolName from "
				+ "`xiaoka-xydk`.user_basicinfo ub LEFT JOIN `xiaoka-xydk`.user_skillinfo us on ub.id=us.basicId "
				+ "LEFT JOIN `xiaoka`.tg_city c ON ub.cityId = c.id "
				+ "LEFT JOIN `xiaoka`.tg_school_info si ON si.id = ub.schoolId "
				+ "LEFT JOIN `xiaoka-xydk`.entertype e ON ub.enterId=e.id where e.typeValue='xnsj' and us.isDelete=1");
		PageObject po = baseDao.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;
	}
}
