/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.user.userskillinfo.controller;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.upload.MultipartUploadSample;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.user.userskillinfo.service.UserSkillinfoService;

/**
 * UserSkillinfo
 * UserSkillinfo
 * userSkillinfo
 * <p>Title:UserSkillinfoController </p>
 * 	<p>Description:UserSkillinfo </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller("skill")
@RequestMapping("/skill")
public class UserSkillinfoController extends BaseController {

	@Autowired
	private UserSkillinfoService userSkillinfoService;

	@Autowired
	private Constants constants;

	/**
	 * 跳转到添加UserSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAdd")
	public String toAddUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		List<Map<String, Object>> classF = userSkillinfoService.getClassF();

		//获取单位字典表项
		List<Map<String, Object>> canpanyMap = userSkillinfoService.getCanpanyMap();
		String basicId = request.getParameter("basicId");

		model.addAttribute("canpanyMap", canpanyMap);
		model.addAttribute("url", "v2userSkillinfo/addUserSkillinfo.html");
		model.addAttribute("classList", classF);
		model.addAttribute("basicId", basicId);
		return "/manager/skill/add";

	}

	/**
	 * 添加UserSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add")
	public String addUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			String[] answerArr = request.getParameterValues("imgUrl");//获取的是数组值
			String basicId = request.getParameter("basicId");

			String skillId = userSkillinfoService.addUserSkillinfo(paramsMap);

			boolean flag = false;

			for (String answer : answerArr) {//存储图片
				flag = userSkillinfoService.addImage(answer, skillId);
			}

			if (flag) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/skill/list.html?basicId=" + basicId;
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}

	}

	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdate")
	public String toUpdateUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			String basicId = request.getParameter("basicId");
			String id = request.getParameter("id");

			List<Map<String, Object>> classF = userSkillinfoService.getClassF();
			List<Map<String, Object>> classS = userSkillinfoService.getClassS();
			List<Map<String, Object>> images = userSkillinfoService.getImages(id);
			//获取单位字典表项
			List<Map<String, Object>> canpanyMap = userSkillinfoService.getCanpanyMap();
			Map<String, Object> objMap = userSkillinfoService.getById(paramsMap);
			model.addAttribute("canpanyMap", canpanyMap);
			model.addAttribute("objMap", objMap);
			model.addAttribute("classF", classF);
			model.addAttribute("classS", classS);
			model.addAttribute("images", images);
			model.addAttribute("basicId", basicId);
			return "/manager/skill/edit";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 修改userSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String updateUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			String[] answerArr = request.getParameterValues("imgUrl");

			//实现图片的更新，先把图片进行删除
			boolean f = userSkillinfoService.deleteImage(paramsMap);
			String id = paramsMap.get("id").toString();
			if (f) {
				for (String answer : answerArr) {//存储图片
					userSkillinfoService.addImage(answer, id);
				}
			}

			boolean flag = userSkillinfoService.updateUserSkillinfo(paramsMap);

			if (flag) {
				model.addAttribute("basicId", paramsMap.get("basicId"));
				return "redirect:/skill/list.html?start=0";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * userSkillinfolist
	 */
	@RequestMapping(value = "/list")
	public String userSkillinfoList(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			String basicId = request.getParameter("basicId");
			createParameterMap(request);
			PageObject po = new PageObject();
			po = userSkillinfoService.queryUserSkillinfoList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap", paramsMap);
			model.addAttribute("basicId", basicId);
			return "/manager/skill/list";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 删除userSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public String deleteUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			String basicId = request.getParameter("basicId");

			boolean flag = userSkillinfoService.deleteUserSkillinfo(paramsMap);

			if (flag) {
				return "redirect:/skill/list.html?basicId=" + basicId;
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}

	}

	//auto-code-end

	//上传商品详情页图片
	@RequestMapping(value = "uploadImage", method = RequestMethod.POST)
	public @ResponseBody Map uploadImage(@RequestParam("imgs") MultipartFile[] imgs, HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (imgs == null || imgs.length <= 0) {
				map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
				map.put("msg", "图片不能为空！");
				return map;
			}
			List pics = new ArrayList();
			String imgPath = "";
			String targetPath = "";
			for (MultipartFile img : imgs) {

				String filename = URLEncoder.encode(img.getOriginalFilename(), "utf-8");
				DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				String name = df.format(new Date());
				Random r = new Random();
				for (int i = 0; i < 3; i++) {
					name += r.nextInt(10);
				}
				String savename = name + filename;

				boolean b = MultipartUploadSample.uploadOssImg(savename, img);

				String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
				String picUrl = beforUrl + savename;

				map.put("imgUrl", picUrl);// 相对路径，存数据库的。
				map.put("targetPath", picUrl);// 绝对路径，显示
				map.put("key", savename);// 绝对路径，显示
				//				map.put("filename", filename);
				pics.add(net.sf.json.JSONObject.fromObject(map));
			}
			String result = net.sf.json.JSONArray.fromObject(pics).toString();
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("pics", result);
		} catch (Exception e) {
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			map.put("msg", "上传失败！" + e.getMessage());
			e.printStackTrace();
		}
		return map;
	}

	@RequestMapping(value = "/getClassSByFid")
	public @ResponseBody Map getClassSByfid(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		Map map = new HashMap();
		List list = new ArrayList();
		list = userSkillinfoService.getClassSBySid(paramsMap);
		map.put("classS", list);
		return map;
	}

	//上传图片
	//	@RequestMapping(value = "picture", method = RequestMethod.POST)
	//	public @ResponseBody Map uploadLogo(@RequestParam(required = false) MultipartFile pic,
	//			HttpServletRequest request,HttpServletResponse response) throws Exception{
	//		Map map = new HashMap();
	//		String filename = URLEncoder.encode(pic.getOriginalFilename(), "utf-8");
	//		DateFormat df =  new SimpleDateFormat("yyyyMMddHHmmssSSS");
	//		String name = df.format(new Date());
	//		Random r = new Random();
	//		for (int i = 0; i < 3; i++) {
	//			name += r.nextInt(10);
	//		}
	//		String savename = name+filename;
	//		
	//		boolean b = MultipartUploadSample.uploadOssImg(savename,pic);
	//		
	//		String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
	//		String picUrl = beforUrl+savename;
	//		map.put("picUrl", picUrl);// picUrl 传阿里云服务器上的全路径
	//		map.put("key",savename);// key值
	//		map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
	//		
	//		return map;
	//	}
	/**
	 * userSkillinfolist
	 */
	/**
	 * 
	 * 全部服务信息
	 * <p>
	 * wxl  2017年4月6日10:36:40  添加一级二级关联查询
	 * @param request
	 * @param response
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping(value = "/skillList")
	public String skillList(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			PageObject po = new PageObject();
			po = userSkillinfoService.querySkillinfoList(paramsMap);
			model.addAttribute("po", po);
			List<Map<String, Object>> classF = userSkillinfoService.getClassF();
			List<Map<String, Object>> classS = userSkillinfoService.getClassSon(paramsMap);
			model.addAttribute("classF", classF);
			model.addAttribute("classS", classS);
			model.addAttribute("paramsMap", paramsMap);
			return "/manager/skill/skill_list";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 
	 * 根据一级分类id查二级分类
	 * <p>
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping(value = "/getClassSonByFatherId")
	public @ResponseBody Map<String, Object> getClassSonByFatherId(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String skillFatId = paramsMap.get("skillFatId").toString();
			List<Map<String, Object>> sonList = userSkillinfoService.queryClassSonByFatherId(skillFatId);
			map.put("sonList", sonList);
			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	@RequestMapping(value = "/updateHome")
	public String updateHome(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			createParameterMap(request);
			String ishome = request.getParameter("ishome");
			if ("0".equals(ishome)) {
				paramsMap.put("ishome", 1);
				boolean b = userSkillinfoService.updateHome(paramsMap);
			} else {
				paramsMap.put("ishome", 0);
				boolean b = userSkillinfoService.updateHome(paramsMap);
			}
			return "redirect:/skill/skillList.html";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
		}
		return "/error";

	}

	/**
	 * 
	 * 去修改页面--------回显  
	 * <p>
	 * wxl
	 * 参数 id
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping(value = "/toUpdateSkill")
	public String toUpdateSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			String id = request.getParameter("id");

			List<Map<String, Object>> classF = userSkillinfoService.getClassF();
			List<Map<String, Object>> classS = userSkillinfoService.getClassS();
			//List<Map<String, Object>> images = userSkillinfoService.getImages(id);
			List<Map<String, Object>> canpanyTypes = userSkillinfoService.canpanyTypes();

			Map<String, Object> objMap = userSkillinfoService.getById(paramsMap);
			model.addAttribute("objMap", objMap);
			model.addAttribute("classF", classF);
			model.addAttribute("classS", classS);
			//model.addAttribute("images", images);
			model.addAttribute("canpanyTypes", canpanyTypes);
			return "/manager/skill/skill_edit";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 
	 * 去修改页面--------回显  
	 * <p>
	 * wxl
	 * 参数 id
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping(value = "/details")
	public String details(@RequestParam Map<String, Object> map, Model model) {

		String id = (String) map.get("id");

		List<Map<String, Object>> classF = userSkillinfoService.getClassF();
		List<Map<String, Object>> classS = userSkillinfoService.getClassS();
		List<Map<String, Object>> images = userSkillinfoService.getImages(id);
		List<Map<String, Object>> canpanyTypes = userSkillinfoService.canpanyTypes();

		Map<String, Object> objMap = userSkillinfoService.getById(paramsMap);
		model.addAttribute("objMap", objMap);
		model.addAttribute("classF", classF);
		model.addAttribute("classS", classS);
		model.addAttribute("images", images);
		model.addAttribute("canpanyTypes", canpanyTypes);
		return "/manager/skill/details";

	}

	/**
	 * 修改userSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateSkill")
	public String updateSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = userSkillinfoService.updateUserSkillinfo(paramsMap);

			if (flag) {
				return "redirect:/skill/skillList.html";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 参数  id
	 * wxl
	 * @param request
	 * @param response
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping(value = "/deleteSkill")
	public String deleteSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = userSkillinfoService.deleteUserSkillinfo(paramsMap);

			if (flag) {
				return "redirect:/skill/skillList.html";
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}

	}

}
