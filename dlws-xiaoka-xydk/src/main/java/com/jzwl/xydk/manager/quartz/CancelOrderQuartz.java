package com.jzwl.xydk.manager.quartz;

import org.springframework.beans.factory.annotation.Autowired;

import com.jzwl.xydk.wap.xkh2_2.quartz.service.OrderQuartzService;

/**
 * 
 * 描述:下单支付成功后没有接单，超过24小时系统自动取消订单
 * 
 * 精准投放的定时任务也放在这里
 * 
 * 
 * @author    gyp
 * @Date	 2017年4月20日
 */
public class CancelOrderQuartz {

	private int isOpen = 0;// 监测开关，默认关闭

	@Autowired
	private OrderQuartzService orderQuartzService;
	
	//定时器轮询方法
	public void executeInternal() throws Exception {
		if (isOpen == 1) {//定时器开关打开了
			cancelTimeOutNotPayOrder();//订单超时
			cancelTimeOutNotPayOrder_fabu();//发布订单超时
			serviceExpires_JZZX();//精准咨询服务到期
			jztfSendTextMessage();//精准投放发送文本信息
			orderSuccess();//订单自动确认订单唯一完成
			orderAgreen();//订单申请退款超时自动同意
			rejectOrders();//订单支付成功后卖家没有进行接单，系统自动取消
		}
	}
	//订单超时
	private void cancelTimeOutNotPayOrder(){
		orderQuartzService.cancelTimeOutNotPayOrders();
	}
	//订单超时---发布订单超时
	private void cancelTimeOutNotPayOrder_fabu(){
		orderQuartzService.cancelTimeOutNotPayOrders_fabu();
	}
	//精准咨询服务到期
	private void serviceExpires_JZZX(){
		orderQuartzService.serviceExpires_JZZX();
	}
	//精准投放发送文本信息
	private void jztfSendTextMessage(){
		orderQuartzService.jztfSendTextMessage();
	}
	//订单自动确认
	private void orderSuccess(){
		orderQuartzService.orderSuccess();
	}
	//订单超时自动同意
	@SuppressWarnings("unused")
	private void orderAgreen(){
		orderQuartzService.orderAgreen();
	}
	//订单支付成功后卖家没有进行接单，系统自动取消
	private void rejectOrders(){
		orderQuartzService.rejectOrders();
	}
	
	public int getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(int isOpen) {
		this.isOpen = isOpen;
	}
}
