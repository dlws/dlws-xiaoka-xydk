package com.jzwl.xydk.manager.invoice.dao;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("managerInvoiceDao")
public class ManagerInvoiceDao {

	@Autowired
	private BaseDAO baseDao;

	public PageObject queryManagerInvoiceforPage(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.* from `xiaoka-xydk`.invoice_manage t where 1=1 ");
		if (null != map.get("sign") && StringUtils.isNotEmpty(map.get("sign").toString())) {
			sb.append(" and t.sign like '%" + map.get("sign") + "%' ");
		}
		if (null != map.get("recipientName") && StringUtils.isNotEmpty(map.get("recipientName").toString())) {
			sb.append(" and t.recipientName like '%" + map.get("recipientName") + "%' ");
		}
		sb.append(" order by t.createDate desc");
		return baseDao.queryForMPageList(sb.toString(), new Object[] {}, map);
	}

	public Map<String, Object> getManagerInvoiceById(Map<String, Object> map) {
		String sql = "select t.*,dd.dic_name as dicName,dd.dic_value as dicValue from `xiaoka-xydk`.invoice_manage t "
				+ " LEFT JOIN  `xiaoka`.v2_dic_data dd  ON dd.id = t.typeId where 1=1";

		if (null != map.get("invoiceId") && StringUtils.isNotEmpty(map.get("invoiceId").toString())) {
			sql = sql + " and t.id = " + map.get("invoiceId");
		}

		return baseDao.queryForMap(sql);
	}

	public boolean updateManagerInvoiceById(Map<String, Object> map) {
		map.put("auditDate", new Date());
		String sql = "update `xiaoka-xydk`.invoice_manage t set auditType = :auditType ,auditDate=:auditDate ";

		if (null != map.get("auditReason") && StringUtils.isNotEmpty(map.get("auditReason").toString())) {
			sql = sql + ", auditReason=:auditReason";
		}
		if (null != map.get("express") && StringUtils.isNotEmpty(map.get("express").toString())) {
			sql = sql + ", express=:express";
		}
		if (null != map.get("expressNumber") && StringUtils.isNotEmpty(map.get("expressNumber").toString())) {
			sql = sql + ", expressNumber=:expressNumber";
		}

		if (null != map.get("invoiceId") && StringUtils.isNotEmpty(map.get("invoiceId").toString())) {
			sql = sql + " where t.id = " + map.get("invoiceId");
		}

		return baseDao.executeNamedCommand(sql, map);
	}

	/**
	 * 查看详情
	 *
	 * @param map
	 * @return 
	 */
	public Map<String, Object> details(String id) {
		String sql = "select t.* from `xiaoka-xydk`.invoice_manage t where id=" + id;
		return baseDao.queryForMap(sql);
	}

}
