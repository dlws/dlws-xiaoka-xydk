package com.jzwl.xydk.manager.maxCategory.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.maxCategory.dao.MaxCategoryDao;

@Service("maxCategoryService")
public class MaxCategoryService {

	@Autowired
	private MaxCategoryDao maxCategoryDao;

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：一级技能分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public PageObject maxCategoryList(Map<String, Object> paramsMap) {
		
		return maxCategoryDao.maxCategoryList(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：添加一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean addMaxCategory(Map<String, Object> paramsMap) {
		return maxCategoryDao.addMaxCategory(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去修改一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public Map<String, Object> toEditMaxCategory(Map<String, Object> paramsMap) {
		return maxCategoryDao.toEditMaxCategory(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean editMaxCategory(Map<String, Object> paramsMap) {
		return maxCategoryDao.editMaxCategory(paramsMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：删除以及分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean deleteMaxCategory(Map<String, Object> paramsMap) {
		return maxCategoryDao.deleteMaxCategory(paramsMap);
	}
}
