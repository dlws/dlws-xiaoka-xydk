package com.jzwl.xydk.manager.maxCategory.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.log.SystemLog;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.upload.service.UploadImgService;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.maxCategory.service.MaxCategoryService;
import com.jzwl.xydk.wap.skillUser.controller.SkillUserController;

@Controller
@RequestMapping("/maxCategory")
public class MaxCategoryController extends BaseController{

	@Autowired
	private SystemLog systemLog;
	
	@Autowired
	private Constants constants;
	
	@Autowired
	private MaxCategoryService maxCategoryService;
	@Autowired
	private UploadImgService uploadImgService;
	
	
	private static Logger log = LoggerFactory.getLogger(SkillUserController.class);
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：一级技能分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/maxCategoryList")
	public String maxCategoryList(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			PageObject po = maxCategoryService.maxCategoryList(paramsMap);
			model.addAttribute("po",po);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "manager/maxCategory/maxCategoryList";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去添加一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toAddMaxCategory")
	public String toAddMaxCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			//maxCategoryService.toAddMaxCategory(paramsMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "manager/maxCategory/maxCategoryForm";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：添加一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/addMaxCategory")
	public String addMaxCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			boolean flag = maxCategoryService.addMaxCategory(paramsMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/maxCategory/maxCategoryList.html";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去修改一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toEditMaxCategory")
	public String toEditMaxCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			Map<String,Object> map = maxCategoryService.toEditMaxCategory(paramsMap);
			model.addAttribute("map", map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "manager/maxCategory/maxCategoryForm";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改一级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/editMaxCategory")
	public String editMaxCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			boolean flag = maxCategoryService.editMaxCategory(paramsMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/maxCategory/maxCategoryList.html";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：删除以及分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/deleteMaxCategory")
	public @ResponseBody Map deleteMaxCategory(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		Map<String,Object> map =  new HashMap<String, Object>();
		try {
			boolean flag = maxCategoryService.deleteMaxCategory(paramsMap);
			map.put("flag", flag);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：auto-code-v5
	 * 描述：异步上传图片，单张
	 * 创建人： ln
	 * 创建时间： 2016年9月6日
	 * 标记：manager
	 * @param img
	 * @param request
	 * @param response
	 * @return
	 * @version
	 * para1:type表示上传到某一个资源上去1：表示静态资源，2：表示上传到oss服务器上去
	 * para2:img 要上传的图片
	 * para3:flag 当type 为2时 flag表示是那个项目的图片eg：bbs，tg,zb..当为1时可传null,
	 */
	@RequestMapping(value = "uploadImg", method = RequestMethod.POST)
	public @ResponseBody Map uploadImg(@RequestParam(required = false) MultipartFile pic, HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			int type = 2;
			String url = uploadImgService.uploadImg(type,pic,null);//返回全路径
			/*uploadImgService.uploadImg(int 2,img,"bbs");*/
			map.put("path", url);
			map.put("url",url); 
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "uploadImg2", method = RequestMethod.POST)
	public @ResponseBody Map uploadImg2(@RequestParam(required = false) MultipartFile picselect, HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			int type = 2;
			String selecturl = uploadImgService.uploadImg(type,picselect,null);//返回全路径
			/*uploadImgService.uploadImg(int 2,img,"bbs");*/
			map.put("selectpath", selecturl);
			map.put("selecturl",selecturl); 
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "uploadImg3", method = RequestMethod.POST)
	public @ResponseBody Map uploadImg3(@RequestParam(required = false) MultipartFile picnoselect, HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			int type = 2;
			String noselecturl = uploadImgService.uploadImg(type,picnoselect,null);//返回全路径
			/*uploadImgService.uploadImg(int 2,img,"bbs");*/
			map.put("noselectpath", noselecturl);
			map.put("noselecturl",noselecturl); 
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
}
