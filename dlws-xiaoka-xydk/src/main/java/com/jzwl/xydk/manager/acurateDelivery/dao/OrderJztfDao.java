package com.jzwl.xydk.manager.acurateDelivery.dao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("OrderJztfDao")
public class OrderJztfDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	public PageObject queryOrderJztfList(Map<String, Object> map) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT orderId,linkman,phone,(SELECT typeValue FROM `xiaoka-xydk`.order_detail as d WHERE typeName='title' AND d.orderId=od.orderId)title,");
		sql.append("  (SELECT typeValue FROM `xiaoka-xydk`.order_detail AS d WHERE typeName='textContent' AND d.orderId=od.orderId)textContent,");
		sql.append(" od.endDate,orderStatus,od.createDate,ub.userName as userName");
		sql.append(" FROM `xiaoka-xydk`.order AS od");
		sql.append(" INNER JOIN `xiaoka-xydk`.user_basicinfo AS ub ON (ub.openId = od.openId)");
		sql.append(" WHERE od.enterType='jztf'");
		if (null != map.get("orderId") && StringUtils.isNotEmpty(map.get("orderId").toString())) {
			sql.append(" and od.orderId ='" + map.get("orderId").toString() + "'");
		}
		sql.append("  and od.orderStatus=2  order by od.createDate desc");
		PageObject po = baseDAO.queryForMPageList(sql.toString(), new Object[] {}, map);
		return po;
	}

	public boolean auditOrder(Map<String, Object> map) {
		String sql = "update `xiaoka-xydk`.order set orderStatus=:orderStatus" + " where orderId=:orderId";
		return baseDAO.executeNamedCommand(sql, map);
	}

}
