/**
 * LinkController.java
 * com.jzwl.xydk.manager.link.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.link.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jzwl.xydk.manager.link.service.LinkService;

/**
 * 链接模块
 *
 * @author   jiangshengdong(nikeodong@126.com)
 * @Date	 2017年6月28日 	 
 */
@Controller
@RequestMapping("/link")
public class LinkController {

	@Autowired
	private LinkService linkService;

	/**
	 * 列表
	 *
	 * @param request 请求
	 * @param response 响应
	 * @return mov 页面所需信息
	 */
	@RequestMapping(value = "/list")
	public Object list(@RequestParam Map<String, Object> map, Model model) {
		//TODO  这里是标杆啊，一个注解解释完接收map参数的值
		model.addAttribute("po", linkService.list(map));
		return "manager/link/list";
	}

	/**
	 * 跳转到添加页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add")
	public String add(HttpServletRequest request, HttpServletResponse response) {
		return "manager/link/add";

	}

	/**
	 * 保存
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/save")
	public String save(@RequestParam Map<String, Object> map, Model model) {

		boolean flag = linkService.save(map);
		if (flag) {
			model.addAttribute("msg", "添加成功");
			return "redirect:/link/list.html?start=0";
		} else {
			model.addAttribute("msg", "添加失败");
			return "/error";
		}
	}

	/**
	 * 改变
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping(value = "/change")
	public String change(@RequestParam Map<String, Object> map, Model model) {

		boolean flag = linkService.change(map);

		if (flag) {
			return "redirect:/link/list.html?start=0";
		} else {
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdateLink")
	public String toUpdateLink(@RequestParam Map<String, Object> map, Model model) {

		model.addAttribute("objMap", linkService.getById(map));
		return "/manager/link/update";

	}

	/**
	 * 修改
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateLink")
	public String updateLink(@RequestParam Map<String, Object> map, Model model) {

		boolean flag = linkService.updateLink(map);

		if (flag) {
			return "redirect:/link/list.html?start=0";
		} else {
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * 删除
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteLink")
	public String deleteLink(@RequestParam Map<String, Object> map, Model model) {

		boolean flag = linkService.deleteLink(map);

		if (flag) {
			return "redirect:/link/list.html?start=0";
		} else {
			model.addAttribute("msg", "删除失败");
			return "/error";
		}
	}
}
