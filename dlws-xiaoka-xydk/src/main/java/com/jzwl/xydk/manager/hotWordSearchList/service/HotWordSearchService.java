/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.hotWordSearchList.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.hotWordSearchList.dao.HotWordSearchDao;

@Service("hotWordSearchService")
public class HotWordSearchService {

	@Autowired
	private HotWordSearchDao hotWordSearchDao;

	public boolean addHotWordSearch(Map<String, Object> map) {

		return hotWordSearchDao.addHotWordSearch(map);

	}

	public PageObject queryHotWordSearchList(Map<String, Object> map) {

		return hotWordSearchDao.queryHotWordSearchList(map);

	}

	public boolean updateHotWordSearch(Map<String, Object> map) {

		return hotWordSearchDao.updateHotWordSearch(map);

	}

	public boolean deleteHotWordSearch(Map<String, Object> map) {

		return hotWordSearchDao.deleteHotWordSearch(map);

	}

	public boolean delHotWordSearch(Map<String, Object> map) {

		return hotWordSearchDao.delHotWordSearch(map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return hotWordSearchDao.getById(map);
	}

	/**
	 * 
	 * 项目名称：
	 * 描述：对应数据库设计时对应的下拉，单选框 值-名
	 * 创建人： 
	 * 创建时间：
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		return hotWordSearchDao.querySelectValue(map);
	}

	public List<Map<String, Object>> hotWords() {

		// TODO Auto-generated method stub
		return hotWordSearchDao.hotWords();

	}

}
