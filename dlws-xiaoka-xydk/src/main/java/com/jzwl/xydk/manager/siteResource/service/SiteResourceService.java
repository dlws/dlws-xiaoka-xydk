/**
 * SiteResourceService.java
 * com.jzwl.xydk.manager.siteResource.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.siteResource.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.siteResource.dao.SiteResourceDao;

/**
 *	场地资源Service
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月24日 	 
 */
@Service
public class SiteResourceService {
	@Autowired
	private SiteResourceDao siteResourceDao;

	/**
	 * 列表
	 * @param map
	 */
	public PageObject list(Map<String, Object> map) {

		return siteResourceDao.list(map);

	}
}
