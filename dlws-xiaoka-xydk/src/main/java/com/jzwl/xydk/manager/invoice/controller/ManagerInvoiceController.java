package com.jzwl.xydk.manager.invoice.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.dic.dicService.DicService;
import com.jzwl.xydk.manager.invoice.service.ManagerInvoiceService;
import com.jzwl.xydk.manager.orderManage.service.OrderManageService;
import com.jzwl.xydk.sendmessage.SendMessageService;

@Controller
@RequestMapping("/managerInvoice")
public class ManagerInvoiceController extends BaseWeixinController {

	@Autowired
	private ManagerInvoiceService managerInvoiceService;

	@Autowired
	private DicService dicService;

	@Autowired
	private OrderManageService orderManageService;

	@Autowired
	private SendMessageService sendMessage;

	@RequestMapping(value = "/list")
	public String list(HttpServletRequest request, HttpServletResponse response, Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = managerInvoiceService.queryManagerInvoiceforPage(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/invoice/list";
	}

	/**
	 * 
	 * 描述:根据发票的Id,获取发票的详情
	 * 作者:gyp
	 * @Date	 2017年6月23日
	 */
	@RequestMapping(value = "/getInvoiceDetail")
	public @ResponseBody Map<String, Object> getInvoiceDetail(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		Map<String, Object> invoice_map = managerInvoiceService.getManagerInvoiceById(paramsMap);//获取信息
		String orderIds = invoice_map.get("orderIds").toString();
		if (orderIds.contains(",")) {
			String[] arr = orderIds.split(",");
			for (String string : arr) {
				Map<String, Object> orderMap = orderManageService.getOrderInfo(string);
				list.add(orderMap);
			}
			invoice_map.put("orders", list);
		} else {
			Map<String, Object> orderMap = orderManageService.getOrderInfo(orderIds);
			list.add(orderMap);
			invoice_map.put("orders", list);
		}

		invoice_map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
		return invoice_map;
	}

	/**
	 * 
	 * 描述:审核信息
	 * 作者:gyp
	 * @Date	 2017年6月26日
	 */
	@RequestMapping(value = "/audit")
	public @ResponseBody Map<String, Object> audit(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();

		String auditType = paramsMap.get("auditType").toString();

		boolean flag = managerInvoiceService.updateManagerInvoiceById(paramsMap);//获取信息

		if (flag) {
			Map<String, Object> invoice_map = managerInvoiceService.getManagerInvoiceById(paramsMap);//获取信息
			if (auditType.equals("1")) {//审核成功
				invoice_map.put("auditType", "您的发票审核成功.");
				invoice_map.put("remark", "如有任何问题请联系平台客服。");
				//发送审核信息
				sendMessage.sendAuditSuccessMessage_invoice(invoice_map);

			} else {//审核失败
				invoice_map.put("auditType", "您的发票审核失败.");
				invoice_map.put("remark", "失败原因:" + invoice_map.get("auditReason"));
				//发送审核信息
				sendMessage.sendAuditSuccessMessage_invoice(invoice_map);
			}
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
		} else {
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
		}
		return map;
	}

	/**
	 * 查看详情
	 *
	 * @param map
	 * @param mov
	 * @return 
	 */
	@RequestMapping(value = "/details")
	public String details(@RequestParam Map<String, Object> map, Model mov) {
		String id = (String) map.get("id");
		Map<String, Object> objMap = managerInvoiceService.details(id);
		mov.addAttribute("po", objMap);
		return "/manager/invoice/details";
	}

}
