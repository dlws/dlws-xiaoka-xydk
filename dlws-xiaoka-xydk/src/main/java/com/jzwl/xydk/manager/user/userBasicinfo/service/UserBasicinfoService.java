/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.user.userBasicinfo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.user.userBasicinfo.dao.UserBasicinfoDao;

@Service("userBasicinfoService")
public class UserBasicinfoService {

	@Autowired
	private UserBasicinfoDao userBasicinfoDao;

	public boolean addUserBasicinfo(Map<String, Object> map) {

		return userBasicinfoDao.addUserBasicinfo(map);

	}

	public PageObject queryUserBasicinfoList(Map<String, Object> map) {

		return userBasicinfoDao.queryUserBasicinfoList(map);

	}

	public boolean updateUserBasicinfo(Map<String, Object> map) {

		boolean flag = userBasicinfoDao.updateUserBasicinfo(map);
		boolean flag2 = userBasicinfoDao.updateUserTemporary(map);

		return flag & flag2;

	}

	public boolean deleteUserBasicinfo(Map<String, Object> map) {

		return userBasicinfoDao.deleteUserBasicinfo(map);

	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return userBasicinfoDao.getById(map);

	}

	//查询所属省
	@SuppressWarnings("unchecked")
	public Map<String, Object> getProvence(String cityId) {

		return userBasicinfoDao.getProvence(cityId);

	}

	//查询所属学校
	@SuppressWarnings("unchecked")
	public Map<String, Object> getSholName(String sholId) {

		return userBasicinfoDao.getSholName(sholId);

	}

	/**
	 * 根据城市的id,获取城市下的学校
	 * @param paramsMap
	 * @return
	 */
	public List querySchoolInfoByCityId(Map<String, Object> map) {

		return userBasicinfoDao.querySchoolInfoByCityId(map);
	}

	/**
	 * 根据省查出市
	 * @param paramsMap
	 * @return
	 */
	public List queryCityByPro(String provence) {
		return userBasicinfoDao.queryCityByPro(provence);
	}

	//从校咖（xiaoka）数据库中获取全部城市数据
	public List<Map<String, Object>> getCityInfo() {

		return userBasicinfoDao.getCityInfo();
	}

	/**
	 * cfz
	 * 2017年3月9日11:23:45
	 * 根据字典中对应的
	 * 标示查询出对应的子目录
	 * @param code 字典中配置的标示
	 * @return
	 */
	public List<Map<String, Object>> getFieldtype(String code) {

		return userBasicinfoDao.getFieldtype(code);

	}

	/*
	 * 添加入驻资源
	 * dxf
	 */
	public boolean addSettleUserVersion3(Map<String, Object> map) {

		return userBasicinfoDao.addSettleUser(map);

	}

	/**
	 * dxf
	 * 查询当前入驻资源子表
	 */
	public List querySettleListVersion3(Map<String, Object> map) {

		return userBasicinfoDao.querySettleListVersion3(map);
	}
	
	public Map<String,Object> getSkillinfoData(Map<String, Object> map) {

		return userBasicinfoDao.getSkillinfoData(map);
	}
	
	/*
	 * 修改入驻资源
	 * dxf
	 */
	public boolean upSettleVersion3(Map<String, Object> map) {

		return userBasicinfoDao.upSettleVersion3(map);

	}
	/**
	 * 创建人：gyp
	 * 创建时间：2017年3月16日
	 * 描述：更新数据
	 * @param map
	 * @return
	 */
	public boolean updateSkillinfoData(Map<String, Object> map) {
		return userBasicinfoDao.updateSkillinfoData(map);
	}
	
	/*
	 * dxf
	 * return list
	 * 查询资源类型
	 */
	public List<Map<String, Object>> queryResourceListVersion3() {
		return userBasicinfoDao.queryResourceListVersion3();
	}

	/*
	 * dxf
	 * return Map
	 * 查询用户所属入驻类型
	 */
	public Map<String, Object> getEnterTypeValueVersion3(Map<String, Object> map) {
		return userBasicinfoDao.getEnterTypeValueVersion3(map);
	}

	public Map<String, Object> getCity(String cityId) {

		// TODO Auto-generated method stub
		return userBasicinfoDao.getCity(cityId);

	}
}
