/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.user.userBasicinfo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.manager.user.userBasicinfo.dao.UserEnterInfoDao;
import com.jzwl.xydk.manager.user.userBasicinfo.pojo.UserSkillinfoData;

@Service("UserEnterInfoService")
public class UserEnterInfoService {

	@Autowired
	private UserEnterInfoDao userEnterInfoDao;

	/**
	 * cfz
	 * 2017年3月9日19:27:45
	 * @param map 存储关于自定义属性的一些东西
	 * @param pId 要存储数据的父Id
	 * @return
	 */
	public boolean addEnertInfo(Map<String, Object> map, String pId) {

		return userEnterInfoDao.addEnertInfo(map, pId);

	}
	
	public List<UserSkillinfoData> querySettleListVersion3(Map<String, Object> map) {
		
		return userEnterInfoDao.querySettleListVersion3(map);
	}

}
