/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.banner.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("bannerInfoDao")
public class BannerInfoDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	public boolean addBannerInfo(Map<String, Object> map) {

		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id", Sequence.nextId());
		map.put("createDate", date);
		map.put("isUse", 0);
		map.put("isDelete", 0);
		map.put("createUser", "admin");

		String sql = "insert into `xiaoka-xydk`.banner_info "
				+ " (id,name,picUrl,keyValue,bannerType,redirectUrl,ord,createDate,isUse,createUser,isDelete) "
				+ " values "
				+ " (:id,:name,:picUrl,:keyValue,:bannerType,:redirectUrl,:ord,:createDate,:isUse,:createUser,:isDelete)";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public String getColumnsTwo() {
		return "" + " t.id as id," + "t.name as name," + " t.picUrl as picUrl," + " t.keyValue as keyValue,"
				+ " t.redirectUrl as redirectUrl," + " t.bannerType as bannerType," + " t.ord as ord,"
				+ " t.createDate as createDate," + " t.createUser as createUser," + " t.isUse as isUse,"
				+ " t.isDelete as isDelete," + " d.dic_name as dic_name," + " d.dic_value as dic_value";
	}

	public String getColumns() {
		return "" + " id as id," + " name as name," + " picUrl as picUrl," + " keyValue as keyValue,"
				+ " redirectUrl as redirectUrl," + " bannerType as bannerType," + " ord as ord,"
				+ " createDate as createDate," + " createUser as createUser," + " isUse as isUse,"
				+ " isDelete as isDelete";
	}

	public PageObject queryBannerInfoList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性    " + getColumns() + "

		String dicCode = "XiaoKaBanner";
		String sql = "select " + getColumnsTwo() + " from `xiaoka-xydk`.banner_info t "
				+ " LEFT JOIN v2_dic_data d ON t.bannerType = d.dic_value "
				+ " LEFT JOIN v2_dic_type dt ON dt.dic_id = d.dic_id " + "where t.isDelete = 0 and dt.dic_code= '"
				+ dicCode + "' and d.isDelete= 0 and dt.isDelete=0";

		if (null != map.get("name") && StringUtils.isNotEmpty(map.get("name").toString())) {
			sql = sql + " and t.name  = " + map.get("name") + "";
		}
		if (null != map.get("picUrl") && StringUtils.isNotEmpty(map.get("picUrl").toString())) {
			sql = sql + " and t.picUrl  = " + map.get("picUrl") + "";
		}
		if (null != map.get("redirectUrl") && StringUtils.isNotEmpty(map.get("redirectUrl").toString())) {
			sql = sql + " and t.redirectUrl  = " + map.get("redirectUrl") + "";
		}
		if (null != map.get("bannerType") && StringUtils.isNotEmpty(map.get("bannerType").toString())) {
			sql = sql + " and t.bannerType  = " + map.get("bannerType") + "";
		}
		if (null != map.get("ord") && StringUtils.isNotEmpty(map.get("ord").toString())) {
			sql = sql + " and t.ord  = " + map.get("ord") + "";
		}
		if (null != map.get("createUser") && StringUtils.isNotEmpty(map.get("createUser").toString())) {
			sql = sql + " and t.createUser  = " + map.get("createUser") + "";
		}
		if (null != map.get("isUse") && StringUtils.isNotEmpty(map.get("isUse").toString())) {
			sql = sql + " and t.isUse  = " + map.get("isUse") + "";
		}
		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + " and t.isDelete  = " + map.get("isDelete") + "";
		}

		sql = sql + " order by t.bannerType,t.isUse DESC,t.createDate ";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	public boolean updateBannerInfo(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.banner_info set "
				+ " name=:name,bannerType=:bannerType,picUrl=:picUrl,redirectUrl=:redirectUrl,ord=:ord "
				+ " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteBannerInfo(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.banner_info set " + " isDelete = :isDelete " + " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);

	}

	public boolean isUse(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.banner_info set " + " isUse = :isUse " + " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		Map<String, Object> resMap = new HashMap<String, Object>();

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.banner_info where id = " + map.get("id") + "";

		resMap = baseDAO.queryForMap(sql);

		return resMap;

	}

	public Map<String, Object> getByName(Map<String, Object> map) {

		Map<String, Object> resMap = new HashMap<String, Object>();

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.banner_info where name = '" + map.get("name")
				+ "' and id <>" + map.get("id") + "";

		resMap = baseDAO.queryForMap(sql);

		return resMap;

	}

	public List<Map<String, Object>> bannerTypes() {

		String sql = "select * from v2_dic_data d ";
		sql = sql
				+ " where d.isDelete=0 and d.dic_id in (select dic_id from v2_dic_type where isDelete = 0 and dic_code='XiaoKaBanner')";
		sql = sql + " order by id ";

		return baseDAO.queryForList(sql);

	}

}
