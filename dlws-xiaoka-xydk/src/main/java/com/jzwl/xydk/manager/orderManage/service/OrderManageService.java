package com.jzwl.xydk.manager.orderManage.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.orderManage.dao.OrderManageDao;

@Service("OrderManageService")
public class OrderManageService {

	@Autowired
	private OrderManageDao orderManageDao;

	public PageObject orderMangeList(Map<String, Object> map) {
		return orderManageDao.orderMangeList(map);
	}

	public Map<String, Object> getOrderInfo(Map<String, Object> map) {

		return orderManageDao.getOrderInfo(map);

	}

	public Map<String, Object> getOrderInfo(String orderId) {

		return orderManageDao.getOrderInfo(orderId);

	}

	/**
	 * 查询当前用户信息通过SkillId
	 * @param skillId
	 * @return
	 */
	public Map<String, Object> searchUserBySkillId(String skillId) {

		return orderManageDao.searchUserBySkillId(skillId);
	}

	/**
	 * 查询卖家信息
	 * @param orderId
	 * @return
	 */
	public Map<String, Object> searchUserByOpenId(String orderId) {

		return orderManageDao.searchUserByOpenId(orderId);

	}

}
