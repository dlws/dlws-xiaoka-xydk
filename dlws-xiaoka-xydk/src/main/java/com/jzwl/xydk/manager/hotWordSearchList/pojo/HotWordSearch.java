/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.hotWordSearchList.pojo;

import com.jzwl.system.base.pojo.BasePojo;

public class HotWordSearch extends BasePojo implements java.io.Serializable {

	private static final long serialVersionUID = 5454155825314635342L;

	//alias
	public static final String TABLE_ALIAS = "HotWordSearch";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_CLASS_ID = "分类的id";
	public static final String ALIAS_WORD_VALUE = "热搜词值";
	public static final String ALIAS_ORD = "排列顺序";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	public static final String ALIAS_IS_DELETE = "是否删除";

	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;

	//columns START
	/**
	 * id       db_column: id 
	 */
	private java.lang.Long id;
	/**
	 * 分类的id       db_column: classId 
	 */
	private java.lang.Long classId;
	/**
	 * 热搜词值       db_column: wordValue 
	 */
	private java.lang.String wordValue;
	/**
	 * 排列顺序       db_column: ord 
	 */
	private java.lang.Integer ord;
	/**
	 * 创建时间       db_column: createDate 
	 */
	private java.util.Date createDate;
	/**
	 * 是否删除       db_column: isDelete 
	 */
	private java.lang.Integer isDelete;

	//columns END

	public void setId(java.lang.Long value) {
		this.id = value;
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public java.lang.Long getClassId() {
		return this.classId;
	}

	public void setClassId(java.lang.Long value) {
		this.classId = value;
	}

	public java.lang.String getWordValue() {
		return this.wordValue;
	}

	public void setWordValue(java.lang.String value) {
		this.wordValue = value;
	}

	public java.lang.Integer getOrd() {
		return this.ord;
	}

	public void setOrd(java.lang.Integer value) {
		this.ord = value;
	}

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}

	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class, FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}

}
