/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.hotWordSearchList.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("hotWordSearchDao")
public class HotWordSearchDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	public boolean addHotWordSearch(Map<String, Object> map) {

		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id", Sequence.nextId());
		map.put("isDelete", 0);
		map.put("createDate", new Date());

		String sql = "insert into `xiaoka-xydk`.hot_word_search " + " (id,classId,wordValue,ord,createDate,isDelete) "
				+ " values " + " (:id,:classId,:wordValue,:ord,:createDate,:isDelete)";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public String getColumns() {
		return "" + " id as id," + " classId as classId," + " wordValue as wordValue," + " ord as ord,"
				+ " createDate as createDate," + " isDelete as isDelete";
	}

	public String getColumnsTwo() {
		return "" + " t.id as id," + " t.classId as classId," + " t.wordValue as wordValue," + " t.ord as ord,"
				+ " t.createDate as createDate," + " t.isDelete as isDelete," + " d.dic_name as dic_name,"
				+ " d.id as d_id," + " d.dic_value as dic_value";
	}

	public PageObject queryHotWordSearchList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性

		String dicCode = "hotWord";
		String sql = "select " + getColumnsTwo() + " from `xiaoka-xydk`.hot_word_search t "
				+ " LEFT JOIN v2_dic_data d ON t.classId = d.id "
				+ " LEFT JOIN v2_dic_type dt ON dt.dic_id = d.dic_id " + "where t.isDelete = 0 and dt.dic_code= '"
				+ dicCode + "' and d.isDelete= 0 and dt.isDelete=0";

		sql = sql + " order by t.createDate ";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	public boolean updateHotWordSearch(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.hot_word_search set ";
		if (null != map.get("classId") && StringUtils.isNotEmpty(map.get("classId").toString())) {
			sql = sql + "			classId=:classId ,";
		}
		if (null != map.get("wordValue") && StringUtils.isNotEmpty(map.get("wordValue").toString())) {
			sql = sql + "			wordValue=:wordValue ,";
		}
		if (null != map.get("ord") && StringUtils.isNotEmpty(map.get("ord").toString())) {
			sql = sql + "			ord=:ord ,";
		}
		if (null != map.get("createDate") && StringUtils.isNotEmpty(map.get("createDate").toString())) {
			sql = sql + "			createDate=:createDate ,";
		}
		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + "			isDelete=:isDelete ,";
		}
		sql = sql.substring(0, sql.length() - 1);
		sql = sql + " where id=:id";
		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteHotWordSearch(Map<String, Object> map) {

		String sql = "delete from `xiaoka-xydk`.hot_word_search where id in (" + map.get("id") + " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean delHotWordSearch(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.hot_word_search set " + " isDelete =:" + map.get("isDelete") + " "
				+ " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.hot_word_search where id = " + map.get("id") + "";

		return baseDAO.queryForMap(sql);
	}

	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		String sql = " SELECT dd.id,dd.dic_name " + "    from dic_type  dt "
				+ "    LEFT JOIN  dic_data dd on dt.dic_id = dd.dic_id " + "    where dt.dic_code = '"
				+ map.get("code") + "'  and dd.isDelete = 0 ";
		return baseDAO.queryForList(sql);
	}

	public List<Map<String, Object>> hotWords() {

		String sql = "select * from v2_dic_data d ";
		sql = sql
				+ " where d.isDelete=0 and d.dic_id in (select dic_id from v2_dic_type where isDelete = 0 and dic_code='HotWord')";
		sql = sql + " order by id ";

		return baseDAO.queryForList(sql);

	}

}
