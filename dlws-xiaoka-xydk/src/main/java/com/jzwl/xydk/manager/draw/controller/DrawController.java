package com.jzwl.xydk.manager.draw.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.constant.Constants;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.draw.service.DrawService;
/**
 * @author sx
 * 提现管理
 */
@Controller
@RequestMapping(value = "/draw")
public class DrawController extends BaseWeixinController{

	@Autowired
	private DrawService drawService;
	@Autowired
	private Constants constants;
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：分页查询
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/drawList")
	public String drawList(HttpServletRequest request, HttpServletResponse response,Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = drawService.queryDrawforPage(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/withdrawals/draw_list";
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据id获取要审核的条目
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/getDrawById")
	public String getDrawById(HttpServletRequest request, HttpServletResponse response,Model mov){
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		createParameterMap(request);
		try {
			if(paramsMap.get("id")!=null && StringUtils.isNotBlank(paramsMap.get("id").toString())){
				list = drawService.queryDrawforList(paramsMap);
				if(list.size()==0){
					mov.addAttribute("msg", "未查询到当前退款条目");
					return "/error";
				}
			}else{
				mov.addAttribute("msg", "未查询到当前退款条目");
				return "/error";
			}
			mov.addAttribute("draw",list.get(0));
			mov.addAttribute("start",paramsMap.get("start"));
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "/manager/withdrawals/draw_review";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：审核
	 * 创建人： sx
	 * 创建时间： 2017年4月11日
	 * 标记：
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/audit")
	public String audit(HttpServletRequest request, HttpServletResponse response){
		createParameterMap(request);
		try {
			paramsMap.put("userName", request.getSession().getAttribute("userName"));
			//用于判断审核是否通过
			String radio = (String)paramsMap.get("radio");
			//提现用户openId
			String openId = (String)paramsMap.get("openId");
			//提现金额
			String amount = (String)paramsMap.get("withdrawMoney");
			if("1".equals(radio)){
				//审核通过 1.调用接口打款 2.记录表插入数据  3.更新钱包
				//根据openId获取账户余额
				Map<String, Object> balanceMap = drawService.getWalletBalanceByOpenId(openId);
				Map<String, Object> moneyMap = new HashMap<String, Object>();
				Double balance = (Double)balanceMap.get("balance");
				//提现后余额
				Double money = balance - Double.valueOf(amount);
				if(money < 0){
					money = 0d;
				}
				moneyMap.put("money", money);
				moneyMap.put("openId", openId);
				//更新用户钱包
				drawService.updateWallet(moneyMap);
				//接口调用成功,往记录表里插入数据
				Map<String, Object> withMap = new HashMap<String, Object>();
				withMap.put("openId", openId);
				//提现金额
				withMap.put("divideOrWithdraw", amount);
				//账户余额
				withMap.put("money", money);
				withMap.put("state", 1);
				withMap.put("divideOrWithdrawTime", new Date());
				drawService.insertWithDraw(withMap);
				//更改本身状态
				drawService.auditPass(paramsMap);
			}else{
				//审核不通过
				drawService.auditNotPass(paramsMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "redirect:/draw/drawList.html";
	}
	/* 微信付款(暂时不用)
	 * @RequestMapping(value = "/audit")
	 * public String audit(HttpServletRequest request, HttpServletResponse response){
		createParameterMap(request);
		//ModelAndView mov = new ModelAndView();
		Map<String, Object> signMap = new HashMap<String, Object>();
		try {
			paramsMap.put("userName", request.getSession().getAttribute("userName"));
			//用于判断审核是否通过
			String radio = (String)paramsMap.get("radio");
			//提现用户openId
			String openId = (String)paramsMap.get("openId");
			//提现金额
			String amount = (String)paramsMap.get("withdrawMoney");
			//银行账号
			String bankCardNumber = (String)paramsMap.get("bankCardNumber");
			System.out.println(bankCardNumber);
			Double mount = Double.parseDouble(amount)*100;
			String str = mount+"";
			String[] split = str.split("\\.");
			String samount = split[0];
			//描述
			String desc = "";
			if("1".equals(radio)){
				//审核通过 1.调用接口打款 2.记录表插入数据  3.更新钱包
				desc = "用户:"+openId+"_的提现申请";
				signMap.put("openid", openId);
				signMap.put("amount", samount);
				signMap.put("desc", desc);
				String sign = AccessCore.sign(signMap, constants.getWx_signKey());//拿到签名数据
				String path = constants.getXiaoka_weixin_xydk_tuikuan()+"?openid="+openId +"&amount="+samount+"&desc="+desc+"&sign="+sign;
				Map<String, Object> urlMap = HttpClientUtil.requestByGetMethod(path);
				boolean flag = (boolean)urlMap.get("flag");
				if(flag){
					//根据openId获取账户余额
					Map<String, Object> balanceMap = drawService.getWalletBalanceByOpenId(openId);
					Map<String, Object> moneyMap = new HashMap<String, Object>();
					Double balance = (Double)balanceMap.get("balance");
					//提现后余额
					Double money = balance - Double.valueOf(amount);
					if(money < 0){
						money = 0d;
					}
					moneyMap.put("money", money);
					moneyMap.put("openId", openId);
					//更新用户钱包
					drawService.updateWallet(moneyMap);
					//接口调用成功,往记录表里插入数据
					Map<String, Object> withMap = new HashMap<String, Object>();
					withMap.put("openId", openId);
					//提现金额
					withMap.put("divideOrWithdraw", amount);
					//账户余额
					withMap.put("money", money);
					withMap.put("state", 1);
					withMap.put("divideOrWithdrawTime", new Date());
					drawService.insertWithDraw(withMap);
					//更改本身状态
					drawService.auditPass(paramsMap);
				}
			}else{
				//审核不通过
				drawService.auditNotPass(paramsMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		//mov.setViewName("redirect:/draw/drawList.html");
		return "redirect:/draw/drawList.html";
	}*/
}
