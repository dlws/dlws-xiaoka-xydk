package com.jzwl.xydk.manager.wallet.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.wallet.dao.ManagerWalletDao;

@Service("managerWalletService")
public class ManagerWalletService {

	@Autowired
	private ManagerWalletDao managerWalletDao;

	public PageObject queryWalletforPage(Map<String, Object> paramsMap) {
		return managerWalletDao.queryWalletforPage(paramsMap);
	}

}
