package com.jzwl.xydk.manager.user.userBasicinfo.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.upload.MultipartUploadSample;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.user.userBasicinfo.pojo.UserSkillinfoData;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserEnterInfoService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;

@Controller
@RequestMapping(value = "/enter")
public class UserEnterInfoController extends BaseWeixinController {

	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private UserEnterInfoService userenterInfoService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private Constants constants;

	private final String FIELD_TYPE = "fieldtype";//场地类型
	private final String FILED_SIZE = "filedsize";//场地规模
	private final String BASIC_MATERIAL = "basicmaterial";//基本物资配备

	/**
	 * 添加用户信息入驻
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddUserByenter")
	public String toAddUserBasicinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		//获取所有的城市，从xiaoka中进行获取
		List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();
		//获取省列表
		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
		//加载当前的场地类型（字典表）fieldtype
		List<Map<String, Object>> fieldTypeList = userBasicinfoService.getFieldtype(FIELD_TYPE);
		//加载当前的场地规模（字典表） filedsize
		List<Map<String, Object>> filedSizeList = userBasicinfoService.getFieldtype(FILED_SIZE);
		//加载当前的基本物资配备（字典表）basicmaterial 
		List<Map<String, Object>> materialList = userBasicinfoService.getFieldtype(BASIC_MATERIAL);

		model.addAttribute("paramsMap", paramsMap);
		model.addAttribute("url", "userBasicinfo/addUserBasicinfo.html");
		model.addAttribute("cityList", cityList);
		model.addAttribute("provnceList", provnceList);//省市
		model.addAttribute("fieldTypeList", fieldTypeList);//场地类型
		model.addAttribute("filedSizeList", filedSizeList);//场地规模
		model.addAttribute("materialList", materialList);//基本物资配备

		return "/manager/user/addEnter";

	}

	/**
	 * 日常推文图片上传
	 * @param pic
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "dairyTweet", method = RequestMethod.POST)
	public @ResponseBody List<String> dairyTweet(@RequestParam(required = false) MultipartFile[] dairyTweet,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		return common(dairyTweet);
	}
	 
	/**
	 * 群成员图片上传
	 * @param pic
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "memNumberImg", method = RequestMethod.POST)
	public @ResponseBody List<String> uploadMemberImg(@RequestParam(required = false) MultipartFile[] memberImg,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		return common(memberImg);
	}

	@RequestMapping(value = "picture", method = RequestMethod.POST)
	public @ResponseBody List<String> uploadLogo(@RequestParam(required = false) MultipartFile[] pic, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		return common(pic);
	}

	/**
	 * 活动图片
	 * @param pic
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "activtyUrl", method = RequestMethod.POST)
	public @ResponseBody List<String> activtyUrl(@RequestParam(required = false) MultipartFile[] activtyUrl,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		return common(activtyUrl);
	}

	/**
	 * 近景上传
	 * @param pic
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "pictureSmall", method = RequestMethod.POST)
	public @ResponseBody List<String> uploadEnterSmall(@RequestParam(required = false) MultipartFile[] picSmall,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		return common(picSmall);
	}

	/**
	 * 中景上传
	 * @param pic
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "pictureMiddle", method = RequestMethod.POST)
	public @ResponseBody List<String> uploadEnterMiddle(@RequestParam(required = false) MultipartFile[] picMiddle,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		return common(picMiddle);
	}

	/**
	 * 全景上传
	 * @param pic
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "pictureAll", method = RequestMethod.POST)
	public @ResponseBody List<String> uploadEnterAll(@RequestParam(required = false) MultipartFile[] picAll,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		return common(picAll);
	}

	/**
	 * 单图上传图片的公共方法
	 * 非得让改成多图上传的原单图上传
	 * @param pic
	 * @return
	 * @throws IOException
	 */
	public List<String> common(MultipartFile[] pic) throws IOException {

		Map<String,List<String>> map = new HashMap();
		
		List<String> list = new ArrayList<String>();
		
		for(int j=0;j<pic.length;j++){
			
			String filename = URLEncoder.encode(pic[j].getOriginalFilename(), "utf-8");
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String name = df.format(new Date());
			Random r = new Random();
			for (int i = 0; i < 3; i++) {
				name += r.nextInt(10);
			}
			String savename = name+filename;
	
			boolean b = MultipartUploadSample.uploadOssImg(savename, pic[j]);
	
			String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
			String picUrl = beforUrl + savename;
			
			list.add(picUrl);
			//map.put("picUrl", picUrl);// picUrl 传阿里云服务器上的全路径
			//map.put("key", savename);// key值
			//map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
		}
		return list;
	}

	/**
	 * 添加用户信息入驻
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addUserByEnter")
	public ModelAndView addUserByEnter(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mov = new ModelAndView();
		//增加校验规则   在后台增加校验规则
		//获取页面中的所有参数
		Map<String, Object> map = createParameterMap(request);
		map.put("enterId", paramsMap.get("enterId"));//无此参数默认设置为空
		if (request.getParameterValues("material") != null) {//设置当前场地的属性

			map.put("material", dealArray(request.getParameterValues("material")));
		}
		//覆盖当前数组类参数
		if (request.getParameterValues("closePhoto") != null && request.getParameterValues("middlePho") != null
				&& request.getParameterValues("allPhoto") != null) {
			map.put("closePhoto", dealArray(request.getParameterValues("closePhoto")));//近景
			map.put("middlePho", dealArray(request.getParameterValues("middlePho")));//中景
			map.put("allPhoto", dealArray(request.getParameterValues("allPhoto")));//远景
		} else {
			System.out.println("近景，中景，远景照片不能为空  且每个种类照片不能小于两张");
		}
		//addSettleUserVersion3(map);
		String userName = String.valueOf(request.getSession().getAttribute("userName"));
		if (!"".equals(userName)) {
			paramsMap.put("createUser", userName);
		} else {
			paramsMap.put("createUser", "admin");
		}
		boolean flag = userBasicinfoService.addSettleUserVersion3(map);

		mov.setViewName("redirect:/userBasicinfo/settleList.html?start=0");

		return mov;

	}

	/**
	 * 处理参数用，号隔开的方式进行返回String类型
	 */
	public String dealArray(String[] str) {
		StringBuffer all = new StringBuffer();
		for (int i = 0; i < str.length; i++) {

			if (i == str.length - 1) {
				all.append(str[i]);
			} else {
				all.append(str[i] + ",");
			}

		}
		return all.toString();
	}

	/**
	 * 将以，号分割的STring 串以list的形式进行返回
	 * @param str
	 * @return
	 */
	public static List<String> dealString(String str) {

		List<String> list = new ArrayList<String>();
		String[] arre = str.split(",");
		for (String string : arre) {
			list.add(string);
		}
		return list;
	}

	/**
	 * 当前页面回显功能展示用作修改
	 */
	@RequestMapping(value = "/updateUserByenter")
	public ModelAndView updateUserByEnter(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		//获取传过来的用户Id
		createParameterMap(request);
		//获取省列表
		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();

		//获取所有的城市，从xiaoka中进行获取
		List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();
		model.addObject("provnceList", provnceList);
		model.addObject("cityList", cityList);

		Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息
		model.addObject("objMap", objMap);
		String cityId = String.valueOf(objMap.get("cityId"));
		Map<String, Object> proMap = userBasicinfoService.getProvence(cityId);//查询所属省
		model.addObject("proMap", proMap);

		String province = String.valueOf(proMap.get("province"));
		String sholId = String.valueOf(objMap.get("schoolId"));
		Map<String, Object> sholMap = userBasicinfoService.getSholName(sholId);//查询所属学校

		model.addObject("sholMap", sholMap);
		List<Map<String, Object>> settleList = userBasicinfoService.querySettleListVersion3(paramsMap);//查询子表list
		model.addObject("settleList", settleList);

		//加载当前的场地类型（字典表）fieldtype
		List<Map<String, Object>> fieldTypeList = userBasicinfoService.getFieldtype(FIELD_TYPE);
		//加载当前的场地规模（字典表） filedsize
		List<Map<String, Object>> filedSizeList = userBasicinfoService.getFieldtype(FILED_SIZE);
		//加载当前的基本物资配备（字典表）basicmaterial 
		List<Map<String, Object>> materialList = userBasicinfoService.getFieldtype(BASIC_MATERIAL);

		model.addObject("fieldTypeList", fieldTypeList);//场地类型
		model.addObject("filedSizeList", filedSizeList);//场地规模
		model.addObject("materialList", materialList);//基本物资配备

		Map<String, String> echoMap = new HashMap<String, String>();
		//查询个性化参数
		List<UserSkillinfoData> list = userenterInfoService.querySettleListVersion3(paramsMap);
		for (UserSkillinfoData userSkillinfoData : list) {
			echoMap.put(userSkillinfoData.getTypeName(), userSkillinfoData.getTypeValue());
		}
		List listClose = dealString(echoMap.get("closePhoto"));
		List listMiddle = dealString(echoMap.get("middlePho"));
		List listAll = dealString(echoMap.get("allPhoto"));

		model.addObject("listClose", listClose);//近景
		model.addObject("listMiddle", listMiddle);//中景
		model.addObject("listAll", listAll);//远景
		model.addObject("echoMap", echoMap);
		model.addObject("list", list);
		model.setViewName("/manager/user/editEnter");
		return model;

	}

	/**
	 * 场地入驻修改方法
	 */
	@RequestMapping(value = "/toUpdateEnterrMessage")
	public ModelAndView toUpdateEnterMessage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		//获取传过来的用户Id 以及页面中修改的参数
		Map<String, Object> map = createParameterMap(request);

		if (request.getParameterValues("material") != null) {//设置当前场地的属性
			map.put("material", dealArray(request.getParameterValues("material")));
		}
		//覆盖当前数组类参数
		if (request.getParameterValues("closePhoto") != null && request.getParameterValues("middlePho") != null
				&& request.getParameterValues("allPhoto") != null) {
			map.put("closePhoto", dealArray(request.getParameterValues("closePhoto")));//近景
			map.put("middlePho", dealArray(request.getParameterValues("middlePho")));//中景
			map.put("allPhoto", dealArray(request.getParameterValues("allPhoto")));//远景
		} else {
			System.out.println("近景，中景，远景照片不能为空");
		}
		//addSettleUserVersion3(map);
		String userName = String.valueOf(request.getSession().getAttribute("userName"));
		if (!"".equals(userName)) {
			paramsMap.put("createUser", userName);
		} else {
			paramsMap.put("createUser", "admin");
		}

		boolean flag = userBasicinfoService.upSettleVersion3(map);

		model.setViewName("redirect:/userBasicinfo/settleList.html?start=0");
		return model;

	}

	/**
	 * 
	 */
	@RequestMapping(value = "/deleteEnterrMessage")
	public ModelAndView deleteEnterMessage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		//获取传过来的用户Id 以及页面中修改的参数
		Map<String, Object> map = createParameterMap(request);//获取要删除用户的ID值
		userBasicinfoService.deleteUserBasicinfo(map);
		model.setViewName("/manager/user/");
		return model;

	}

	/**
	 * 从服务器上删除当前key的图片
	 */
	@RequestMapping(value = "/deleteImage")
	public @ResponseBody String deleteImage(HttpServletRequest request, HttpServletResponse response) {

		String url = request.getParameter("url");
		String message = "";
		try {
			String key = MultipartUploadSample.dealUrl(url);
			MultipartUploadSample.delete(key);
			message = "删除成功";
		} catch (IOException e) {
			e.printStackTrace();
			message = "删除";
		}
		return message;

	}

}
