/**
 * DiplomatExpandService.java
 * com.jzwl.xydk.manager.diplomatist.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.diplomatist.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.diplomatist.dao.DiplomatExpandDao;

/**
 *	外交官推广链接Service
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月12日 	 
 */
@Service
public class DiplomatExpandService {
	@Autowired
	private DiplomatExpandDao diplomatExpandDao;

	/**
	 * 列表
	 *
	 * @param request 请求
	 * @param response 响应
	 * @return mov 页面所需信息
	 */
	public PageObject list(Map<String, Object> paramsMap) {

		return diplomatExpandDao.list(paramsMap);

	}

	/**
	 * 详情
	 * 
	 * @param map对象
	 */
	public List<Map<String, Object>> details(Map<String, Object> map) {

		return diplomatExpandDao.details(map);

	}
}
