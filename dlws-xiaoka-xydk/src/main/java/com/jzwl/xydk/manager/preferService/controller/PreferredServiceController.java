/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.preferService.controller;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.upload.MultipartUploadSample;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;

/**
 * PreferredService PreferredService preferredService
 * <p>
 * Title:PreferredServiceController
 * </p>
 * <p>
 * Description:PreferredService
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author aotu-code
 */
@Controller
@RequestMapping("/preferredService")
public class PreferredServiceController extends BaseController {

	@Autowired
	private PreferredServiceService preferredServiceService;

	@Autowired
	private Constants constants;

	@Autowired
	private EntertypeService entertypeService;

	/**
	 * 跳转到添加PreferredService的页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddPreferredService")
	public ModelAndView toAddpreferredService(HttpServletRequest request, HttpServletResponse response) {

		PageObject po = new PageObject();
		ModelAndView mov = new ModelAndView();
		po = entertypeService.queryEntertypeList(paramsMap);
		List<Map<String, Object>> preferServiceClass = preferredServiceService.preferClass();
		mov.addObject("preferServiceClass", preferServiceClass);
		mov.addObject("enterList", po.getDatasource());
		createParameterMap(request);

		mov.setViewName("/manager/preferService/add");
		return mov;
	}

	/**
	 * 添加PreferredService
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addPreferredService")
	public ModelAndView addpreferredService(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			Map<String, Object> dicMap = preferredServiceService.getDicDataInfo(paramsMap);
			paramsMap.put("enterTypeValue", dicMap.get("dic_value"));
			boolean flag = preferredServiceService.addPreferredService(paramsMap);
			if (!flag) {
				mov.addObject("msg", "添加PreferredService失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/preferredService/preferredServiceList.html");
			}
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "添加PreferredService失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * preferredServicelist
	 */
	@RequestMapping(value = "/preferredServiceList")
	public ModelAndView preferredServiceList(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		PageObject po = new PageObject();

		try {
			po = preferredServiceService.queryPreferredServiceList(paramsMap);

			mov.addObject("list", po.getDatasource());
			mov.addObject("totalProperty", po.getTotalCount());
			mov.addObject("po", po);
			mov.addObject("paramsMap", paramsMap);
			mov.setViewName("/manager/preferService/list");
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "preferredService查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 跳转到修改preferredService页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toUpdatePreferredService")
	public ModelAndView toUpdatePreferredService(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = entertypeService.queryEntertypeList(paramsMap);
			mov.addObject("enterList", po.getDatasource());
			Map<String, Object> preferredService = preferredServiceService.getById(paramsMap);

			List<Map<String, Object>> preferServiceClass = preferredServiceService.preferClass();
			mov.addObject("preferServiceClass", preferServiceClass);

			mov.addObject("obj", preferredService);
			mov.setViewName("/manager/preferService/update");
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "preferredService查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 跳转到详情preferredService页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/detailPreferredService")
	public ModelAndView detailPreferredService(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			Map<String, Object> preferredService = preferredServiceService.getById(paramsMap);
			mov.addObject("obj", preferredService);
			mov.setViewName("/manager/preferService/detail");
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "preferredService查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 修改preferredService
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updatePreferredService")
	public ModelAndView updatePreferredService(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			Map<String, Object> dicMap = preferredServiceService.getDicDataInfo(paramsMap);
			paramsMap.put("enterTypeValue", dicMap.get("dic_value"));
			boolean flag = preferredServiceService.updatePreferredService(paramsMap);

			if (!flag) {
				mov.addObject("msg", "修改失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/preferredService/preferredServiceList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "修改失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 逻辑删除preferredService
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delPreferredService")
	public ModelAndView delPreferredService(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = preferredServiceService.deletePreferredService(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/preferredService/preferredServiceList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	/**
	 * 物理删除preferredService
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deletePreferredService")
	public ModelAndView deletePreferredService(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = preferredServiceService.deletePreferredService(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/preferredService/preferredServiceList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	// 上传图片
	@RequestMapping(value = "picture", method = RequestMethod.POST)
	public @ResponseBody Map uploadLogo(@RequestParam(required = false) MultipartFile pic, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Map map = new HashMap();
		String filename = URLEncoder.encode(pic.getOriginalFilename(), "utf-8");
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String name = df.format(new Date());
		Random r = new Random();
		for (int i = 0; i < 3; i++) {
			name += r.nextInt(10);
		}
		String savename = name + filename;

		boolean b = MultipartUploadSample.uploadOssImg(savename, pic);

		String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
		String picUrl = beforUrl + savename;
		map.put("imgUrl", picUrl);// picUrl 传阿里云服务器上的全路径
		map.put("key", savename);// key值
		map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);

		return map;
	}

	// 上传图片
	@RequestMapping(value = "bannerpicture", method = RequestMethod.POST)
	public @ResponseBody Map uploadBannerpictureLogo(@RequestParam(required = false) MultipartFile bic,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map map = new HashMap();
		String filename = URLEncoder.encode(bic.getOriginalFilename(), "utf-8");
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String name = df.format(new Date());
		Random r = new Random();
		for (int i = 0; i < 3; i++) {
			name += r.nextInt(10);
		}
		String savename = name + filename;

		boolean b = MultipartUploadSample.uploadOssImg(savename, bic);

		String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
		String picUrl = beforUrl + savename;
		map.put("bannerUrl", picUrl);// picUrl 传阿里云服务器上的全路径
		map.put("key", savename);// key值
		map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);

		return map;
	}

}
