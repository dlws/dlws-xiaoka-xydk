package com.jzwl.xydk.manager.baseInfoSkill.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.manager.baseInfoSkill.dao.FieldSkillDao;

@Service
public class FieldSkillService {

	@Autowired
	FieldSkillDao fieldSkillDao;
	
	
	public List<Map<String,Object>> getSkillType(String pamater){
		
		
		return fieldSkillDao.getSkillType(pamater);
	}
	
}
