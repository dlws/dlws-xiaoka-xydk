package com.jzwl.xydk.manager.importdata.pojo;

import com.jzwl.system.base.pojo.BasePojo;

public class ImportData extends BasePojo implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private java.lang.Long id;
	private java.lang.String provice;
	private java.lang.String school;
	private java.lang.String city;
	private java.lang.String level;
	
	
	public java.lang.Long getId() {
		return id;
	}
	public void setId(java.lang.Long id) {
		this.id = id;
	}
	public java.lang.String getProvice() {
		return provice;
	}
	public void setProvice(java.lang.String provice) {
		this.provice = provice;
	}
	public java.lang.String getSchool() {
		return school;
	}
	public void setSchool(java.lang.String school) {
		this.school = school;
	}
	public java.lang.String getCity() {
		return city;
	}
	public void setCity(java.lang.String city) {
		this.city = city;
	}
	public java.lang.String getLevel() {
		return level;
	}
	public void setLevel(java.lang.String level) {
		this.level = level;
	}
	
	
	

}
