/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.Entertype.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.Entertype.dao.EntertypeDao;

@Service("entertypeService")
public class EntertypeService {

	@Autowired
	private EntertypeDao entertypeDao;

	public boolean addEntertype(Map<String, Object> map) {

		return entertypeDao.addEntertype(map);

	}

	public PageObject queryEntertypeList(Map<String, Object> map) {

		return entertypeDao.queryEntertypeList(map);

	}

	public boolean updateEntertype(Map<String, Object> map) {

		return entertypeDao.updateEntertype(map);

	}

	public boolean deleteEntertype(Map<String, Object> map) {

		return entertypeDao.deleteEntertype(map);

	}

	public boolean delEntertype(Map<String, Object> map) {

		return entertypeDao.delEntertype(map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return entertypeDao.getById(map);
	}

	/**
	 * 
	 * 项目名称：
	 * 描述：对应数据库设计时对应的下拉，单选框 值-名
	 * 创建人： 
	 * 创建时间：
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		return entertypeDao.querySelectValue(map);
	}

	public Map<String, Object> getByEnterId(Map<String, Object> paramsMap) {

		return entertypeDao.getByEnterId(paramsMap);

	}
	
	public Map<String, Object> getByEnterValue(Map<String, Object> paramsMap) {

		return entertypeDao.getByEnterValue(paramsMap);

	}

}
