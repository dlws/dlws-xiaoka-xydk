package com.jzwl.xydk.manager.publish.dao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("managerPublishDao")
public class ManagerPublishDao {

	@Autowired
	private BaseDAO baseDao;

	public PageObject queryManagerPublishforPage(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.id,t.createDate,"
				+ "	(select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='resourceType' and brd.releaseId = t.id)resourceType,"
				+ "	(select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='time' and brd.releaseId = t.id)time,"
				+ "	(select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='output' and brd.releaseId = t.id)output,"
				+ "	(select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='company' and brd.releaseId = t.id)company,"
				+ "	(select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='city' and brd.releaseId = t.id)city,"
				+ " (select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='school' and brd.releaseId = t.id)school,"
				+ "	(select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='name' and brd.releaseId = t.id)name,"
				+ " (select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='phone' and brd.releaseId = t.id)phone,"
				+ "	(select typeValue from `xiaoka-xydk`.business_release_detail brd where typeName='money' and brd.releaseId = t.id)money "
				+ " FROM `xiaoka-xydk`.business_release t where 1=1 ");
		if (null != map.get("name") && StringUtils.isNotEmpty(map.get("name").toString())) {
			sb.append(" and t.id in (select t.releaseId FROM `xiaoka-xydk`.business_release_detail t where t.typeValue LIKE '%"
					+ map.get("name").toString() + "%' )");
		}
		sb.append(" order by t.createDate desc");
		return baseDao.queryForMPageList(sb.toString(), new Object[] {}, map);
	}

}
