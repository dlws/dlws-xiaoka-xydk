package com.jzwl.xydk.manager.baseInfoSkill.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.system.base.dao.BaseDAO;


@Repository
public class FieldSkillDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	/**
	 * 传入当前父类的classValue 来获取
	 * 当前下的二级分类的所有Map
	 * @return
	 */
	public List<Map<String,Object>> getSkillType(String pamater){
		
		if(StringUtils.isNotBlank(pamater)){
			
			String sql = "select * from `xiaoka-xydk`.user_skillclass_son uss where uss.fatherId in(select  suf.id  from `xiaoka-xydk`.user_skillclass_father suf where suf.classValue='"+pamater+"')";
			
			return baseDAO.getJdbcTemplate().queryForList(sql);
		}
		return null;
	}
	
	
}
