/**
 * SiteResourceController.java
 * com.jzwl.xydk.manager.siteResource.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.siteResource.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.csv.CSVUtils;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.siteResource.service.SiteResourceService;

/**
 *	资源模块--场地资源Controller
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月24日 	 
 */
@Controller
@RequestMapping(value = "/siteResource")
public class SiteResourceController extends BaseController {
	@Autowired
	private SiteResourceService siteResourceService;

	/**
	 *	列表
	 * @param request
	 * @param response
	 * @param mov
	 */
	@RequestMapping(value = "/list")
	public String list(HttpServletRequest request, HttpServletResponse response, Model mov) {
		createParameterMap(request);
		PageObject po = new PageObject();
		try {
			po = siteResourceService.list(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mov.addAttribute("po", po);
		mov.addAttribute("list", po.getDatasource());
		mov.addAttribute("totalProperty", po.getTotalCount());
		mov.addAttribute("paramsMap", paramsMap);
		return "/manager/siteResource/list";
	}

	/**
	 * 	导入
	 */
	@RequestMapping(value = "/exportToExcel")
	public void exportToExcel() {
		System.out.println("==========进入导入方法中==========");
		List<String> dataList = CSVUtils.importCsv(new File("D:/excel创建csv格式文件.csv"));
		if (dataList != null && !dataList.isEmpty()) {
			for (String data : dataList) {
				System.out.println(data);
			}
		}
	}
}
