package com.jzwl.xydk.manager.publish.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.publish.dao.ManagerPublishDao;

@Service("managerPublishService")
public class ManagerPublishService {

	@Autowired
	private ManagerPublishDao managerPublishDao;
	
	public PageObject queryManagerPublishforPage(Map<String, Object> paramsMap) {
		return managerPublishDao.queryManagerPublishforPage(paramsMap);
	}

}
