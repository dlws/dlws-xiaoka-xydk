/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.order.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.order.service.BusinessOrderService;

/**
 * BusinessOrder BusinessOrder businessOrder
 * <p>
 * Title:BusinessOrderController
 * </p>
 * <p>
 * Description:BusinessOrder
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author aotu-code
 */
@Controller("order")
@RequestMapping("/order")
public class BusinessOrderController extends BaseController {

	@Autowired
	private BusinessOrderService businessOrderService;

	/**
	 * 跳转到添加BusinessOrder
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddBusinessOrder")
	public String toAddBusinessOrder(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		model.addAttribute("url", "v2businessOrder/addBusinessOrder.html");
		return "/v2/jsp/businessOrder/businessOrderform";

	}

	/**
	 * 添加BusinessOrder
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addBusinessOrder")
	public String addBusinessOrder(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			boolean flag = businessOrderService.addBusinessOrder(paramsMap);
			if (flag) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/v2businessOrder/businessOrderList.html?start=0";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}

	}

	/**
	 * 跳转到修改页面
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdateBusinessOrder")
	public String toUpdateBusinessOrder(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			Map<String, Object> objMap = businessOrderService
					.getById(paramsMap);
			model.addAttribute("objMap", objMap);
			model.addAttribute("url",
					"v2businessOrder/updateBusinessOrder.html");
			return "/v2/jsp/businessOrder/businessOrderform";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 修改businessOrder
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateBusinessOrder")
	public String updateBusinessOrder(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			boolean flag = businessOrderService.updateBusinessOrder(paramsMap);

			if (flag) {
				return "redirect:/v2businessOrder/businessOrderList.html?start=0";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * businessOrderlist
	 */
	@RequestMapping(value = "/orderList")
	public String businessOrderList(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			PageObject po = new PageObject();
			po = businessOrderService.queryBusinessOrderList(paramsMap);
			model.addAttribute("po", po);
			model.addAttribute("paramsMap", paramsMap);
			return "/manager/order/orderlist";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 删除businessOrder
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteBusinessOrder")
	public String deleteBusinessOrder(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = businessOrderService.deleteBusinessOrder(paramsMap);

			if (flag) {
				return "redirect:/v2businessOrder/businessOrderList.html?start=0";
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}

	}

	/**
	 * 获取详情信息
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/getInfoById")
	public String getInfoById(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			Map<String, Object> objMap = businessOrderService
					.getById(paramsMap);
			model.addAttribute("objMap", objMap);
			return "/manager/order/orderEdit";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	// auto-code-end

}
