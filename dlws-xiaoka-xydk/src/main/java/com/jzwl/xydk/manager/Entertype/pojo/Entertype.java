/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.Entertype.pojo;

import com.jzwl.system.base.pojo.BasePojo;

public class Entertype extends BasePojo implements java.io.Serializable {

	private static final long serialVersionUID = 5454155825314635342L;

	//alias
	public static final String TABLE_ALIAS = "Entertype";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_TYPE_NAME = "类型名称";
	public static final String ALIAS_TYPE_VALUE = "入驻类型";
	public static final String ALIAS_IMGE_URL = "图片路径";
	public static final String ALIAS_IS_DELETE = "是否删除";
	public static final String ALIAS_ORD = "顺序";
	public static final String ALIAS_CREATE_DATE = "createDate";

	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;

	//columns START
	/**
	 * id       db_column: id 
	 */
	private java.lang.Long id;
	/**
	 * 类型名称       db_column: typeName 
	 */
	private java.lang.String typeName;
	/**
	 * 入驻类型       db_column: typeValue 
	 */
	private java.lang.String typeValue;
	/**
	 * 图片路径       db_column: imgeUrl 
	 */
	private java.lang.String imgeUrl;
	/**
	 * 是否删除       db_column: isDelete 
	 */
	private java.lang.Integer isDelete;
	/**
	 * 顺序       db_column: ord 
	 */
	private java.lang.Integer ord;
	/**
	 * createDate       db_column: createDate 
	 */
	private java.util.Date createDate;

	//columns END

	public void setId(java.lang.Long value) {
		this.id = value;
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public java.lang.String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(java.lang.String value) {
		this.typeName = value;
	}

	public java.lang.String getTypeValue() {
		return this.typeValue;
	}

	public void setTypeValue(java.lang.String value) {
		this.typeValue = value;
	}

	public java.lang.String getImgeUrl() {
		return this.imgeUrl;
	}

	public void setImgeUrl(java.lang.String value) {
		this.imgeUrl = value;
	}

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}

	public java.lang.Integer getOrd() {
		return this.ord;
	}

	public void setOrd(java.lang.Integer value) {
		this.ord = value;
	}

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}

	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class, FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}

}
