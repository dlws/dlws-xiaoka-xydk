package com.jzwl.xydk.manager.school.schoollabel.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.json.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.log.SystemLog;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.school.schoollabel.service.SchoolInfoService;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;

@Controller
@RequestMapping("/school")
public class SchoolInfoController extends BaseController{
	@Autowired
	private SystemLog systemLog;
	@Autowired
	private SchoolInfoService schoolInfoService;
	@Autowired
	private Constants constants;
	
	/**
	 * 学校列表
	 * 
	 */
	@RequestMapping(value = "/schoolList")
	public ModelAndView schoolList(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mov = new ModelAndView();
		PageObject po = new PageObject();
		createParameterMap(request);
		try{
			List cityList = schoolInfoService.cityList();
			po =  schoolInfoService.querySchoolList(paramsMap);
			mov.addObject("cityList",cityList);
			mov.addObject("po",po);
			mov.addObject("schoolName",paramsMap.get("schoolName"));
			mov.addObject("cityId",paramsMap.get("cityId"));
		}catch(Exception e){
			systemLog.errorLog(getClass(), "-", "-", "cityList", "学校列表", "查询学校列表失败！paramsMap：" + paramsMap, null);
			mov.addObject("msg", "查询学校列表异常！");
			mov.setViewName("/error");
			e.printStackTrace();
			return mov;
		}
		mov.setViewName("manager/schoolLabel/schoolList");
		 return mov;
		
	}
	
	@RequestMapping(value = "/toSchoolLabel")
	public String toSchoolLabel(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		try {
			List<Map<String, Object>> Labelist = schoolInfoService.toSchoolLabel(paramsMap);//查询所有标签
			List<Map<String, Object>> DicSkillist = schoolInfoService.querySkilDicdata(paramsMap);//查询字典资源中间表
			model.addAttribute("Labelist", Labelist);
			model.addAttribute("DicSkillist", DicSkillist);
			model.addAttribute("map",paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "manager/schoolLabel/toEditLabel";
	}
	
	
	@RequestMapping(value = "/updateLabel")
	public String updateLabel(HttpServletRequest request, HttpServletResponse response,Model model) {
		createParameterMap(request);
		boolean flag=false;
		try {
			//List<Map<String, Object>> skilList = skillUserManagerService.queryExit(paramsMap);
			flag = schoolInfoService.updateLabel(paramsMap);//修改所属标签
			
			if(flag==true){
				
				Map<String,Object> map = schoolInfoService.getSchoolInfo(paramsMap.get("schoolId").toString());
				
				return "redirect:/school/schoolList.html?cityId="+map.get("cityId");
			}else{
				model.addAttribute("msg","保存失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","保存失败");
			return "/error";
			
		}
	}
	
	/**
	 * 根据城市的id获取学校
	 * 创建人：gyp
	 * 创建时间：2017年3月15日
	 * 描述：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getSchoolsByCityId")
	public @ResponseBody Map<String,Object> getSchoolsByCityId(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List list = schoolInfoService.getSchoolsByCityId(paramsMap);
			map.put("list",list);
			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}
	
	
	
	
}






























