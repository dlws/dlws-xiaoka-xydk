package com.jzwl.xydk.manager.diplomatist.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;


@Repository
public class DiplomatDrawDao {

	
	@Autowired
	private BaseDAO baseDAO;//注入当前容器中
	
	Log log = LogFactory.getLog(getClass());
	
	public PageObject getApplyWithDraw(Map<String,Object> map){
		
		PageObject pageObject = new PageObject();
		
		String sql = "select t.*,d.userName,d.phone,d.isDelete,w.balance from `xiaoka-xydk`.diplomat_withdrawals t "
				+ "LEFT JOIN `xiaoka-xydk`.diplomatinfo d on t.openId = d.openId "
				+ "LEFT JOIN `xiaoka-xydk`.wallet w on w.openId = t.openId  where 1=1 ";
		
		//搜索条件 1.用户名
		if(null !=map.get("userName") && StringUtils.isNotEmpty(map.get("userName").toString())){
			
			sql +=" and userName like '%"+map.get("userName").toString()+"%'";
		}
		//2.审核最小时间
		if (null != map.get("beginDate") && StringUtils.isNotEmpty(map.get("beginDate").toString())) {
			sql +=" and  DATE_FORMAT(t.applicationTime,'%Y-%m-%d') >= DATE_FORMAT('"+map.get("beginDate")+"','%Y-%m-%d')";
		}
		//3.审核最大时间
		if (null != map.get("endDate") && StringUtils.isNotEmpty(map.get("endDate").toString())) {
			sql +=" and  DATE_FORMAT(t.applicationTime,'%Y-%m-%d') <= DATE_FORMAT('"+map.get("endDate")+"','%Y-%m-%d')";
		}
		//4.审核状态
		if(null !=map.get("applicationStatus") && StringUtils.isNotEmpty(map.get("applicationStatus").toString())){
			
			sql +=" and applicationStatus = '"+map.get("applicationStatus").toString()+"'";
		}
		//搜索条件 1.用户名
		if(null !=map.get("id") && StringUtils.isNotEmpty(map.get("id").toString())){
			
			sql +=" and t.id = '"+map.get("id").toString()+"'";
		}
		
		sql+=" and d.isDelete = '0' order by createTime desc";
		
		pageObject =  baseDAO.queryForMPageList(sql, new Object[] {}, map);
		
		return pageObject;
	}
	
	/**
	 * 更新当前的外交官提现申请表
	 * @param map
	 * @return
	 */
	public boolean updateDiplomat_withdrawals(Map<String,Object> map){
		
		
		//审核日期
		map.put("checkTime", new Date());
		//审核人
		map.put("checkPeople",map.get("userName"));
		
		if(map.get("id")==null||StringUtils.isBlank(map.get("id").toString())){
			
			log.info("------当前必传id为空-------");
			return false;
		}
		String  sql = "update `xiaoka-xydk`.diplomat_withdrawals set ";
		
		if(map.get("applicationStatus")!=null||StringUtils.isNotBlank(map.get("applicationStatus").toString())){
			
			sql +=" applicationStatus = :applicationStatus ,";
		}
		//不通过的情况下更新 refuseReason 字段
		if("2".equals(map.get("applicationStatus").toString())){
			
			if(map.get("refuseReason")!=null||StringUtils.isNotBlank(map.get("refuseReason").toString())){
				
				sql +=" refuseReason = :refuseReason ,";
			}
		}
		sql +=" checkTime = :checkTime , checkPeople = :checkPeople ,";
		
		//去除当前的，号
		sql = sql.substring(0, sql.length()-1);
		//增加update 条件
		sql +=" where id='"+map.get("id").toString()+"'";
		
		return  baseDAO.executeNamedCommand(sql, map);
		
	}
	
	
	
	
	
	
}
