package com.jzwl.xydk.manager.invoice.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.invoice.dao.ManagerInvoiceDao;

@Service("managerInvoiceService")
public class ManagerInvoiceService {

	@Autowired
	private ManagerInvoiceDao managerInvoiceDao;

	public PageObject queryManagerInvoiceforPage(Map<String, Object> paramsMap) {
		return managerInvoiceDao.queryManagerInvoiceforPage(paramsMap);
	}

	/**
	 * 
	 * 描述:获取发票信息
	 * 作者:gyp
	 * @Date	 2017年6月23日
	 */
	public Map<String, Object> getManagerInvoiceById(Map<String, Object> map) {
		return managerInvoiceDao.getManagerInvoiceById(map);
	}

	/**
	 * 
	 * 描述:更新审核信息
	 * 作者:gyp
	 * @Date	 2017年6月26日
	 */
	public boolean updateManagerInvoiceById(Map<String, Object> map) {
		return managerInvoiceDao.updateManagerInvoiceById(map);
	}

	/**
	 * 查看详情
	 */
	public Map<String, Object> details(String id) {
		return managerInvoiceDao.details(id);
	}

}
