package com.jzwl.xydk.manager.messagepush.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.messagepush.dao.MessagePushDao;

@Service("messagePushService")
public class MessagePushService {

	@Autowired
	private MessagePushDao messagePushDao;

	/**
	 * 添加推送消息接口
	 * 
	 * @return
	 */
	public boolean addMessage(Map<String, Object> pama) {

		return messagePushDao.addMessage(pama);
	}

	/**
	 * 展示当前推送给用户的讯息
	 */
	public PageObject queryMessforPage(Map<String, Object> pama) {

		return messagePushDao.queryMessforPage(pama);
	}

	/**
	 * 展示当前用户指定Id值的要推送的信息
	 */
	public List<Map<String, Object>> queryMessforList(Map<String, Object> pama) {

		return messagePushDao.queryMessforList(pama);
	}

	/**
	 * 修改当前信息内容
	 * 
	 * @param pama
	 * @return
	 */
	public boolean updateMessage(Map<String, Object> pama) {

		return messagePushDao.updateMessage(pama);
	}

	/**
	 * 删除当前推送的用户信息
	 */
	public boolean deleteMessage(Map<String, Object> pama) {

		return messagePushDao.deleteMessage(pama);
	}

	/**
	 * 推送当前用户的信息
	 */
	public boolean sendMessage(Map<String, Object> pama) {

		return messagePushDao.sendMessage(pama);
	}

	public Map<String, Object> getMassInformation(Map<String, Object> pama) {

		return messagePushDao.getMassInformation(pama);
	}

}
