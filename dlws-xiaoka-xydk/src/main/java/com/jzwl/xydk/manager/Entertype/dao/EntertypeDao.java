/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.Entertype.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("entertypeDao")
public class EntertypeDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	public boolean addEntertype(Map<String, Object> map) {

		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id", Sequence.nextId());
		map.put("isDelete", 0);
		map.put("createDate", new Date());

		String sql = "insert into `xiaoka-xydk`.entertype "
				+ " (id,typeName,typeValue,imgeUrl,isDelete,ord,createDate) " + " values "
				+ " (:id,:typeName,:typeValue,:imgeUrl,:isDelete,:ord,:createDate)";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public String getColumns() {
		return "" + " id as id," + " typeName as typeName," + " typeValue as typeValue," + " imgeUrl as imgeUrl,"
				+ " isDelete as isDelete," + " ord as ord," + " createDate as createDate";
	}

	public PageObject queryEntertypeList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.entertype t where 1=1 ";

		if (null != map.get("typeName") && StringUtils.isNotEmpty(map.get("typeName").toString())) {
			sql = sql + " and t.typeName  like '%" + map.get("typeName") + "%' ";
		}

		if (null != map.get("typeValue") && StringUtils.isNotEmpty(map.get("typeValue").toString())) {
			sql = sql + " and t.typeValue  = " + map.get("typeValue") + "";
		}

		if (null != map.get("imgeUrl") && StringUtils.isNotEmpty(map.get("imgeUrl").toString())) {
			sql = sql + " and t.imgeUrl  = " + map.get("imgeUrl") + "";
		}

		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + " and t.isDelete  = " + map.get("isDelete") + "";
		}

		if (null != map.get("ord") && StringUtils.isNotEmpty(map.get("ord").toString())) {
			sql = sql + " and t.ord  = " + map.get("ord") + "";
		}

		if (null != map.get("createDateBegin") && StringUtils.isNotEmpty(map.get("createDateBegin").toString())) {
			sql = sql + " and t.createDate >= '" + map.get("createDateBegin") + "'";
		}
		if (null != map.get("createDateEnd") && StringUtils.isNotEmpty(map.get("createDateEnd").toString())) {
			sql = sql + " and t.createDate <= '" + map.get("createDateEnd") + "'";
		}

		sql = sql + " order by ord ";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	public boolean updateEntertype(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.entertype set ";
		if (null != map.get("typeName") && StringUtils.isNotEmpty(map.get("typeName").toString())) {
			sql = sql + "			typeName=:typeName ,";
		}
		if (null != map.get("typeValue") && StringUtils.isNotEmpty(map.get("typeValue").toString())) {
			sql = sql + "			typeValue=:typeValue ,";
		}
		if (null != map.get("imgeUrl") && StringUtils.isNotEmpty(map.get("imgeUrl").toString())) {
			sql = sql + "			imgeUrl=:imgeUrl ,";
		}
		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + "			isDelete=:isDelete ,";
		}
		if (null != map.get("ord") && StringUtils.isNotEmpty(map.get("ord").toString())) {
			sql = sql + "			ord=:ord ,";
		}
		if (null != map.get("createDate") && StringUtils.isNotEmpty(map.get("createDate").toString())) {
			sql = sql + "			createDate=:createDate ,";
		}
		sql = sql.substring(0, sql.length() - 1);
		sql = sql + " where id=:id";
		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteEntertype(Map<String, Object> map) {

		String sql = "delete from `xiaoka-xydk`.entertype where id in (" + map.get("id") + " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean delEntertype(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.entertype set " + " isDelete =:" + map.get("isDelete") + " "
				+ " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.entertype where id = " + map.get("id") + "";

		return baseDAO.queryForMap(sql);
	}

	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		String sql = " SELECT dd.id,dd.dic_name " + "    from dic_type  dt "
				+ "    LEFT JOIN  dic_data dd on dt.dic_id = dd.dic_id " + "    where dt.dic_code = '"
				+ map.get("code") + "'  and dd.isDelete = 0 ";
		return baseDAO.queryForList(sql);
	}

	public Map<String, Object> getByEnterId(Map<String, Object> paramsMap) {

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.entertype where id = " + paramsMap.get("enterId")
				+ "";

		return baseDAO.queryForMap(sql);

	}
	
	public Map<String, Object> getByEnterValue(Map<String, Object> paramsMap) {

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.entertype where typeValue = '" + paramsMap.get("typeValue")
				+ "'";
		System.out.println("*************当前执行SQL结果**************"+sql);
		return baseDAO.queryForMap(sql);

	}

}
