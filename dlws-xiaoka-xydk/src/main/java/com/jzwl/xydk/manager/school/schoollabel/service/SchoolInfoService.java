package com.jzwl.xydk.manager.school.schoollabel.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.school.schoollabel.dao.SchoolInfoDao;

@Service("schoolInfoService")
public class SchoolInfoService {
	
	@Autowired
	private SchoolInfoDao schoolInfoDao;
	
	/**
	 * 查询学校列表
	 * @param paramsMap
	 * @return
	 */
	public PageObject querySchoolList(Map<String, Object> paramsMap) {
		return schoolInfoDao.querySchoolList(paramsMap);
	}
	
	/**
	 * 获取城市的id
	 * @return
	 */
	public List cityList() {
		return schoolInfoDao.cityList();
	}
	/**
	 * 根据城市的id获取学校
	 * 创建人：gyp
	 * 创建时间：2017年3月15日
	 * 描述：
	 * @return
	 */
	public List getSchoolsByCityId(Map<String,Object> map) {
		
		return schoolInfoDao.getSchoolsByCityId(map);
	}
	
	public List<Map<String, Object>> toSchoolLabel(Map<String, Object> paramsMap) {

		return schoolInfoDao.toSchoolLabel(paramsMap);
	}
	
	public List<Map<String, Object>> querySkilDicdata(Map<String, Object> paramsMap) {

		return schoolInfoDao.querySkilDicdata(paramsMap);
	}
	
	public Map<String, Object> getSchoolInfo(String schoolId) {

		return schoolInfoDao.getSchoolInfo(schoolId);
	}
	
	public boolean updateLabel(Map<String, Object> paramsMap) {

		return schoolInfoDao.updateLabel(paramsMap);
	}
	

}
