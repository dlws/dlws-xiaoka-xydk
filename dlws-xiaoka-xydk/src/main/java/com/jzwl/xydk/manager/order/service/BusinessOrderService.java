/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.order.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.order.dao.BusinessOrderDao;


@Service("businessOrderService")
public class BusinessOrderService {
	
	
	@Autowired
	private BusinessOrderDao businessOrderDao;
	
	public boolean addBusinessOrder(Map<String, Object> map) {
		
		return businessOrderDao.addBusinessOrder(map);
		
	}

	public PageObject queryBusinessOrderList(Map<String, Object> map) {
		
		return businessOrderDao.queryBusinessOrderList(map);
	
	}

	public boolean updateBusinessOrder(Map<String, Object> map) {
	
		return businessOrderDao.updateBusinessOrder(map);
	
	}

	public boolean deleteBusinessOrder(Map<String, Object> map) {
		
		return businessOrderDao.deleteBusinessOrder(map);
	
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String,Object> getById(Map<String, Object> map) {
		
		return businessOrderDao.getById(map);
		
	}
	
	
}
