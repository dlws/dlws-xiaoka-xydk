/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.preferService.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("preferredServiceDao")
public class PreferredServiceDao {

	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	public boolean addPreferredService(Map<String, Object> map) {

		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		map.put("id", Sequence.nextId());
		map.put("isDelete", 0);
		map.put("createDate", new Date());

		String sql = "insert into `xiaoka-xydk`.preferred_service "
				+ " (id,className,serviceName,introduce,price,serviceType,salesVolume,imgUrl,isDelete,createDate,serviceRatio,diplomatRatio,platformRatio,classId,enterTypeValue,bannerUrl) "
				+ " values "
				+ " (:id,:className,:serviceName,:introduce,:price,:serviceType,:salesVolume,:imgUrl,:isDelete,:createDate,:serviceRatio,:diplomatRatio,:platformRatio,:classId,:enterTypeValue,:bannerUrl)";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public String getColumns() {
		return "" + " id as id," + " enterClass as enterClass," + " className as className,"
				+ " serviceName as serviceName," + " introduce as introduce," + " price as price,"
				+ " serviceRatio as serviceRatio," + " diplomatRatio as diplomatRatio,"
				+ " platformRatio as platformRatio," + " classId as classId," + " serviceType as serviceType,"
				+ " salesVolume as salesVolume," + " imgUrl as imgUrl," + " isDelete as isDelete,"
				+ " createDate as createDate," + " enterTypeValue as enterTypeValue";
	}

	public String getColumnsTwo() {
		return "" + " t.id as id," + " t.enterClass as enterClass," + " t.className as className,"
				+ " t.serviceName as serviceName," + " t.introduce as introduce," + " t.price as price,"
				+ " t.serviceRatio as serviceRatio," + " t.diplomatRatio as diplomatRatio,"
				+ " t.platformRatio as platformRatio," + " t.enterTypeValue as enterTypeValue,"
				+ " t.classId as classId," + " t.serviceType as serviceType," + " t.salesVolume as salesVolume,"
				+ " t.imgUrl as imgUrl," + " t.isDelete as isDelete," + " t.createDate as createDate,"
				+ " d.dic_name as dic_name," + " d.id as d_id," + " d.dic_value as dic_value";
	}

	public PageObject queryPreferredServiceList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接
		// [column] 为PageRequest的属性
		String dicCode = "preferServiceClass";
		String sql = "select " + getColumnsTwo() + " from `xiaoka-xydk`.preferred_service t "
				+ " LEFT JOIN v2_dic_data d ON t.classId = d.id "
				+ " LEFT JOIN v2_dic_type dt ON dt.dic_id = d.dic_id " + "where t.isDelete = 0 and dt.dic_code= '"
				+ dicCode + "' and d.isDelete= 0 and dt.isDelete=0";

		if (null != map.get("enterClass") && StringUtils.isNotEmpty(map.get("enterClass").toString())) {
			sql = sql + " and t.enterClass  like '%" + map.get("enterClass") + "%' ";
		}

		if (null != map.get("className") && StringUtils.isNotEmpty(map.get("className").toString())) {
			sql = sql + " and t.className  like '%" + map.get("className") + "%' ";
		}

		if (null != map.get("serviceName") && StringUtils.isNotEmpty(map.get("serviceName").toString())) {
			sql = sql + " and t.serviceName  = " + map.get("serviceName") + "";
		}

		if (null != map.get("introduce") && StringUtils.isNotEmpty(map.get("introduce").toString())) {
			sql = sql + " and t.introduce  = " + map.get("introduce") + "";
		}

		if (null != map.get("price") && StringUtils.isNotEmpty(map.get("price").toString())) {
			sql = sql + " and t.price  = " + map.get("price") + "";
		}

		if (null != map.get("serviceType") && StringUtils.isNotEmpty(map.get("serviceType").toString())) {
			sql = sql + " and t.serviceType  = " + map.get("serviceType") + "";
		}

		if (null != map.get("salesVolume") && StringUtils.isNotEmpty(map.get("salesVolume").toString())) {
			sql = sql + " and t.salesVolume  = " + map.get("salesVolume") + "";
		}

		if (null != map.get("imgUrl") && StringUtils.isNotEmpty(map.get("imgUrl").toString())) {
			sql = sql + " and t.imgUrl  = " + map.get("imgUrl") + "";
		}

		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + " and t.isDelete  = " + map.get("isDelete") + "";
		}

		if (null != map.get("createDateBegin") && StringUtils.isNotEmpty(map.get("createDateBegin").toString())) {
			sql = sql + " and t.createDate >= '" + map.get("createDateBegin") + "'";
		}
		if (null != map.get("createDateEnd") && StringUtils.isNotEmpty(map.get("createDateEnd").toString())) {
			sql = sql + " and t.createDate <= '" + map.get("createDateEnd") + "'";
		}

		sql = sql + " order by id ";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	public boolean updatePreferredService(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.preferred_service set ";
		if (null != map.get("enterClassTwo") && StringUtils.isNotEmpty(map.get("enterClassTwo").toString())) {
			sql = sql + "			enterClass=:enterClassTwo,";
		}
		if (null != map.get("className") && StringUtils.isNotEmpty(map.get("className").toString())) {
			sql = sql + "			className=:className,";
		}
		if (null != map.get("serviceName") && StringUtils.isNotEmpty(map.get("serviceName").toString())) {
			sql = sql + "			serviceName=:serviceName,";
		}
		if (null != map.get("introduce") && StringUtils.isNotEmpty(map.get("introduce").toString())) {
			sql = sql + "			introduce=:introduce,";
		}
		if (null != map.get("price") && StringUtils.isNotEmpty(map.get("price").toString())) {
			sql = sql + "			price=:price,";
		}

		if (null != map.get("serviceRatio") && StringUtils.isNotEmpty(map.get("serviceRatio").toString())) {
			sql = sql + "			serviceRatio=:serviceRatio,";
		}
		if (null != map.get("diplomatRatio") && StringUtils.isNotEmpty(map.get("diplomatRatio").toString())) {
			sql = sql + "			diplomatRatio=:diplomatRatio,";
		}
		if (null != map.get("platformRatio") && StringUtils.isNotEmpty(map.get("platformRatio").toString())) {
			sql = sql + "			platformRatio=:platformRatio,";
		}
		if (null != map.get("classId") && StringUtils.isNotEmpty(map.get("classId").toString())) {
			sql = sql + "			classId=:classId,";
		}

		if (null != map.get("serviceType") && StringUtils.isNotEmpty(map.get("serviceType").toString())) {
			sql = sql + "			serviceType=:serviceType,";
		}
		if (null != map.get("salesVolume") && StringUtils.isNotEmpty(map.get("salesVolume").toString())) {
			sql = sql + "			salesVolume=:salesVolume,";
		}
		if (null != map.get("imgUrl") && StringUtils.isNotEmpty(map.get("imgUrl").toString())) {
			sql = sql + "			imgUrl=:imgUrl,";
		}
		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + "			isDelete=:isDelete,";
		}
		if (null != map.get("createDate") && StringUtils.isNotEmpty(map.get("createDate").toString())) {
			sql = sql + "			createDate=:createDate,";
		}
		if (null != map.get("enterTypeValue") && StringUtils.isNotEmpty(map.get("enterTypeValue").toString())) {
			sql = sql + "			enterTypeValue=:enterTypeValue,";
		}
		sql = sql.substring(0, sql.length() - 1);
		sql = sql + " where id=:id";
		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deletePreferredService(Map<String, Object> map) {

		String sql = "delete from `xiaoka-xydk`.preferred_service where id in (" + map.get("id") + " ) ";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean delPreferredService(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.preferred_service set " + " isDelete =:" + map.get("isDelete") + " "
				+ " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.preferred_service where id = " + map.get("id")
				+ "";

		return baseDAO.queryForMap(sql);
	}

	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		String sql = " SELECT dd.id,dd.dic_name " + "    from dic_type  dt "
				+ "    LEFT JOIN  dic_data dd on dt.dic_id = dd.dic_id " + "    where dt.dic_code = '"
				+ map.get("code") + "'  and dd.isDelete = 0 ";
		return baseDAO.queryForList(sql);
	}

	public String getColumnsService() {
		return "" + " t.id as id," + " t.enterClass as enterClass," + " t.className as className,"
				+ " t.serviceName as serviceName," + " t.introduce as introduce," + " t.price as price,"
				+ " t.serviceRatio as serviceRatio," + " t.diplomatRatio as diplomatRatio,"
				+ " t.platformRatio as platformRatio," + " t.enterTypeValue as enterTypeValue,"
				+ " t.serviceType as serviceType," + " t.salesVolume as salesVolume," + " t.imgUrl as imgUrl,"
				+ " t.isDelete as isDelete," + " t.createDate as createDate";
	}

	/**
	 * 获取优质服务 创建人：gyp 创建时间：2017年3月23日 描述：
	 * 
	 * @param enterClass
	 * @return
	 */
	public List<Map<String, Object>> getService(String enterClass) {

		String sql = "select " + getColumnsService()
				+ " from `xiaoka-xydk`.preferred_service  t where 1=1  and t.isDelete=0 and t.enterClass like '%"
				+ enterClass + "%'";
		return baseDAO.queryForList(sql);
	}

	public List<Map<String, Object>> preferClass() {

		String sql = "select * from v2_dic_data d ";
		sql = sql
				+ " where d.isDelete=0 and d.dic_id in (select dic_id from v2_dic_type where isDelete = 0 and dic_code='preferServiceClass')";
		sql = sql + " order by id ";

		return baseDAO.queryForList(sql);

	}

	public Map<String, Object> getDicDataInfo(Map<String, Object> map) {

		String sql = "select * from `xiaoka`.v2_dic_data d where d.id = " + map.get("classId") + "";

		return baseDAO.queryForMap(sql);
	}

	public boolean updateSkillPayNoPre(Map<String, Object> map) {
		String sql = "";
		if (GlobalConstant.ENTER_TYPE_SQDV.equals(map.get("enterType"))) {
			sql = "update `xiaoka-xydk`.user_skillinfo set sellNo=:solnum+sellNo where id=:skillId";
		} else {
			sql = "update `xiaoka-xydk`.preferred_service set salesVolume=:solnum+salesVolume where id=:skillId";
		}
		return baseDAO.executeNamedCommand(sql, map);

	}

	public Map<String, Object> getPreferredService(Map<String, Object> map) {

		String sql = "select * from `xiaoka-xydk`.preferred_service where 1=1 ";
		if (map.containsKey("enterTypeValue")) {

			sql = sql + "and enterTypeValue = '" + map.get("enterTypeValue") + "'";
		}
		if (map.containsKey("isDelete")) {

			sql = sql + " and isDelete = " + map.get("isDelete");
		}

		return baseDAO.queryForMap(sql);
	}
}
