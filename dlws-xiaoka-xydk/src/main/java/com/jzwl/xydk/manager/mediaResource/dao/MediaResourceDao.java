/**
 * MediaResourceDao.java
 * com.jzwl.xydk.manager.mediaResource.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.manager.mediaResource.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

/**
 *	自媒体资源Dao
 * @author   liuyu(99957924@qq.com)
 * @Date	 2017年7月24日 	 
 */
@Repository("mediaResourceDao")
public class MediaResourceDao {
	@Autowired
	private BaseDAO baseDao;

	/**
	 * 列表
	 *
	 * @param map map对象
	 * @return 页面所需信息
	 */
	public PageObject list(Map<String, Object> map) {

		StringBuffer sb = new StringBuffer();
		sb.append("");
		sb.append(" order by ");
		PageObject po = baseDao.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;

	}
}
