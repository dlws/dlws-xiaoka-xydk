package com.jzwl.xydk.manager.diplomatist.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.exception.AppException;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.utils.DoubleArithUtil;
import com.jzwl.xydk.manager.diplomatist.dao.DiplomatDrawDao;
import com.jzwl.xydk.manager.draw.service.DrawService;
import com.jzwl.xydk.wap.xkh2_4.diplomat.dao.DiplomatDao;

@Service
public class DiplomatDrawService {

	@Autowired
	private DiplomatDrawDao diplomatDrawDao;

	@Autowired
	private DrawService drawService;

	@Autowired
	private DiplomatDao diplomatDao;

	/**
	 * 查询当前所有的申请记录
	 * @param map
	 * @return
	 */
	public PageObject getApplyWithDraw(Map<String, Object> map) {

		return diplomatDrawDao.getApplyWithDraw(map);
	}

	/**
	 * @param map 传入成功修改的条件
	 * @return
	 */
	public boolean accessAppaly(Map<String, Object> map) {

		double money = 0d;

		Map<String, Object> confidence = new HashMap<String, Object>();
		confidence.put("id", map.get("id"));
		Map<String, Object> singleHistry = getApplyHistory(confidence);
		if (singleHistry == null) {
			System.out.println("--------未查询到当前修改参数的Id--------");
			return false;
		}
		//更新当前申请表中的状态
		if (diplomatDrawDao.updateDiplomat_withdrawals(map)) {
			//更新钱包中的钱
			Map<String, Object> balanceMap = drawService.getWalletBalanceByOpenId(map.get("openId").toString());
			Map<String, Object> moneyMap = new HashMap<String, Object>();
			Double balance = (Double) balanceMap.get("balance");
			Double take = Double.valueOf(singleHistry.get("withdrawMoney").toString());
			//提现后余额
			money = DoubleArithUtil.sub(balance, take);
			if (money < 0) {
				//直接手动抛出异常 让之前执行回滚
				throw new AppException("------当前用户的余额不足-------");
			}
			moneyMap.put("money", money);
			moneyMap.put("openId", singleHistry.get("openId").toString());
			//更新用户钱包
			drawService.updateWallet(moneyMap);
			//插入一条提现记录到diplomat_divide_withdraw 表中
			Map<String, Object> history = new HashMap<String, Object>();
			history.put("balanceMoney", balance);
			history.put("divideOrWithdraw", take);
			history.put("openId", singleHistry.get("openId").toString());
			history.put("state", 1);//提现:1 分成：0
			history.put("orderId", null);//提现:1 分成：0
			boolean flage = diplomatDao.insertDiplomatWithdraw(history);

			if (!flage) {
				throw new AppException("---------更新记录失败 回滚所有方法执行的状态---------");
			}
			return true;
		}

		return false;
	}

	/**
	 * 查询当前的申请返回单条记录
	 * @param map
	 * @return
	 */
	public Map<String, Object> getApplyHistory(Map<String, Object> map) {

		//查询当前申请记录中的记录
		PageObject po = diplomatDrawDao.getApplyWithDraw(map);
		List<Map<String, Object>> list = po.getDatasource();
		if (list.isEmpty()) {
			return null;
		}
		Map<String, Object> withdrawals = list.get(0);

		return withdrawals;
	}

	/**
	 * 更改当前提现申请状态为不通过
	 */
	public boolean notAccessAppaly(Map<String, Object> map) {

		return diplomatDrawDao.updateDiplomat_withdrawals(map);

	}

}
