/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.businessBasicinfo.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class BusinessBasicinfo extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "BusinessBasicinfo";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_OPEN_ID = "店家的openId";
	public static final String ALIAS_ACTIVITY_ID = "活动类型的id";
	public static final String ALIAS_START_DATE = "活动开始时间";
	public static final String ALIAS_END_DATE = "活动结束时间";
	public static final String ALIAS_CITY_ID = "城市的ID";
	public static final String ALIAS_SCHOOL_ID = "学校的id";
	public static final String ALIAS_LEVEL_ID = "量级的id";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	public static final String ALIAS_OUTPUT_INDEX = "产出指标";
	public static final String ALIAS_PHONE = "手机号码";
	public static final String ALIAS_REMARK = "备注信息";
	
	//date formats
	public static final String FORMAT_START_DATE = DATE_FORMAT;
	public static final String FORMAT_END_DATE = DATE_FORMAT;
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	

	//columns START
    /**
     * id       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 店家的openId       db_column: openId 
     */ 	
	private java.lang.String openId;
    /**
     * 活动类型的id       db_column: activityId 
     */ 	
	private java.lang.Long activityId;
    /**
     * 活动开始时间       db_column: startDate 
     */ 	
	private java.util.Date startDate;
    /**
     * 活动结束时间       db_column: endDate 
     */ 	
	private java.util.Date endDate;
    /**
     * 城市的ID       db_column: cityId 
     */ 	
	private java.lang.Long cityId;
    /**
     * 学校的id       db_column: schoolId 
     */ 	
	private java.lang.Long schoolId;
    /**
     * 量级的id       db_column: levelId 
     */ 	
	private java.lang.Long levelId;
    /**
     * 创建时间       db_column: createDate 
     */ 	
	private java.util.Date createDate;
    /**
     * 产出指标       db_column: outputIndex 
     */ 	
	private java.lang.String outputIndex;
    /**
     * 手机号码       db_column: phone 
     */ 	
	private java.lang.String phone;
    /**
     * 备注信息       db_column: remark 
     */ 	
	private java.lang.String remark;
	//columns END

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	
	
	
	
	
	
	
	
	
	
	


	public java.lang.String getOpenId() {
		return this.openId;
	}
	
	public void setOpenId(java.lang.String value) {
		this.openId = value;
	}
	

	public java.lang.Long getActivityId() {
		return this.activityId;
	}
	
	public void setActivityId(java.lang.Long value) {
		this.activityId = value;
	}
	

	public String getStartDateString() {
		return dateToStr(getStartDate(), FORMAT_START_DATE);
	}
	public void setStartDateString(String value) {
		setStartDate(strToDate(value, java.util.Date.class,FORMAT_START_DATE));
	}

	public java.util.Date getStartDate() {
		return this.startDate;
	}
	
	public void setStartDate(java.util.Date value) {
		this.startDate = value;
	}
	

	public String getEndDateString() {
		return dateToStr(getEndDate(), FORMAT_END_DATE);
	}
	public void setEndDateString(String value) {
		setEndDate(strToDate(value, java.util.Date.class,FORMAT_END_DATE));
	}

	public java.util.Date getEndDate() {
		return this.endDate;
	}
	
	public void setEndDate(java.util.Date value) {
		this.endDate = value;
	}
	

	public java.lang.Long getCityId() {
		return this.cityId;
	}
	
	public void setCityId(java.lang.Long value) {
		this.cityId = value;
	}
	

	public java.lang.Long getSchoolId() {
		return this.schoolId;
	}
	
	public void setSchoolId(java.lang.Long value) {
		this.schoolId = value;
	}
	

	public java.lang.Long getLevelId() {
		return this.levelId;
	}
	
	public void setLevelId(java.lang.Long value) {
		this.levelId = value;
	}
	

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}
	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class,FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

	public java.lang.String getOutputIndex() {
		return this.outputIndex;
	}
	
	public void setOutputIndex(java.lang.String value) {
		this.outputIndex = value;
	}
	

	public java.lang.String getPhone() {
		return this.phone;
	}
	
	public void setPhone(java.lang.String value) {
		this.phone = value;
	}
	

	public java.lang.String getRemark() {
		return this.remark;
	}
	
	public void setRemark(java.lang.String value) {
		this.remark = value;
	}
	

}

