package com.jzwl.xydk.manager.importdata.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.manager.importdata.dao.ImportDataDao;
import com.jzwl.xydk.manager.importdata.pojo.ImportData;

@Service("importDataService")
public class ImportDataService {
	
	@Autowired
	private ImportDataDao importDataDao;
	
	public List<ImportData> getData(){
		
		List<ImportData> list = importDataDao.getInfo();
		int i = 0;
		//对业务进行处理
		for(ImportData importData:list){
			
			//根据城市名称判断该城市下是否有没有需要的城市 如果没有数据，则进行添加
			List<ImportData> citylist=importDataDao.getCityInfo(importData.getCity());
			if(citylist.size() == 0){
				//添加城市
				boolean flag_city = importDataDao.addCity(importData.getProvice(), importData.getCity(), importData.getId());
				//存储城市成功，存储该城市下的学校
				if(flag_city){
					//从xk-city-school-info获取学校的数据
					List<ImportData> listCity =  importDataDao.getInfoByCity(importData.getCity());
					for(ImportData cityData:listCity){
						//根据学校名称判断该城市下是否含有这个学校
						List<ImportData> listSchool = importDataDao.getSchoolInfo(cityData.getSchool());
						//if(listSchool.size() == 0){//没有该学校，进行添加
							boolean temp_school = importDataDao.addSchool(cityData.getProvice(), cityData.getCity(), cityData.getId(), cityData.getSchool(), importData.getId());
							if(temp_school){
								//得到标签表
								//System.out.println("===============success====================="+cityData.getLevel());
								String  str = cityData.getLevel().replace(" ", "");
								 Map<String,Object> map =  getLabelData(str);
								 Long dicDataId = Long.parseLong(map.get("id").toString());
								//学校添加成功，维护标签表
								 boolean temp_school_label = importDataDao.addSchoolLabel(dicDataId, cityData.getId());
								 if(temp_school_label){
									 i=i+1;
									 System.out.println("===============success====================="+i+"school"+cityData.getSchool());
								 }
							}
						//}
					}
				}
			}
			
		}
		
		
		
		return list;
	}
	
	public Map<String,Object> getLabelData(String temp){
		Map<String,Object> map = importDataDao.getLabelData(temp);
		return map;
	}
	
	public boolean setBaseInfo1(){
		boolean flag = false;
		List<Map<String,Object>> list = importDataDao.getBaseInfo("1489632337738001");
		for(Map<String,Object> map:list){
			String id = map.get("id").toString();
			System.out.println("==========================================1489632337738001==========================id:"+id);
			map.put("openId", "o3P2SwV4K6JCkhtimEzme4YQWrHY");
			flag = importDataDao.updateBaseInfo(map);
			if(!flag){
				System.out.println("===========================================出现错误=========================id:"+id);
			}
		}
		
		
		return flag;
	}
	
	public boolean setBaseInfo2(){
		boolean flag = false;
		List<Map<String,Object>> list = importDataDao.getBaseInfo("1489632337738002");
		for(Map<String,Object> map:list){
			String id = map.get("id").toString();
			System.out.println("===========================================1489632337738002=========================id:"+id);
			map.put("openId", "o3P2SwQIFncAFXcNFkXZjZ-cKiyc");
			flag = importDataDao.updateBaseInfo(map);
			if(!flag){
				System.out.println("===========================================出现错误=========================id:"+id);
			}
		}
		
		
		return flag;
	}
	
	public boolean setBaseInfo3(){
		boolean flag = false;
		List<Map<String,Object>> list = importDataDao.getBaseInfo("1489632337738003");
		for(Map<String,Object> map:list){
			String id = map.get("id").toString();
			System.out.println("===========================================1489632337738003=========================id:"+id);
			map.put("openId", "o3P2SwY4fFsCOw0jR2DQ2dI1ba7M");
			flag = importDataDao.updateBaseInfo(map);
			if(!flag){
				System.out.println("===========================================出现错误=========================id:"+id);
			}
		}
		
		
		return flag;
	}
	
	
	public boolean setBaseInfo4(){
		boolean flag = false;
		List<Map<String,Object>> list = importDataDao.getBaseInfo("1489632337738004");
		for(Map<String,Object> map:list){
			String id = map.get("id").toString();
			System.out.println("===========================================1489632337738004=========================id:"+id);
			map.put("openId", "o3P2SweEVpVDt4sK4Rwwb-H5uuQw");
			flag = importDataDao.updateBaseInfo(map);
			if(!flag){
				System.out.println("===========================================出现错误=========================id:"+id);
			}
		}
		
		
		return flag;
	}
	
	public boolean setBaseInfo0(){
		boolean flag = false;
		List<Map<String,Object>> list = importDataDao.getBaseInfo("1489632337738000");
		for(Map<String,Object> map:list){
			String id = map.get("id").toString();
			System.out.println("===========================================1489632337738000=========================id:"+id);
			map.put("openId", "o3P2SwTg8mRMDosd27dZNqIMrDzQ");
			flag = importDataDao.updateBaseInfo(map);
			if(!flag){
				System.out.println("===========================================出现错误=========================id:"+id);
			}
		}
		return flag;
	}
	
	public boolean setBaseInfoTwo(){
		boolean flag = false;
		List<Map<String,Object>> list = importDataDao.getBaseInfoTwo();
		for(Map<String,Object> map:list){
			String id = map.get("id").toString();
			System.out.println("===========================================1489632337738001=========================id:"+id);
			map.put("openId", "o3P2SwV4K6JCkhtimEzme4YQWrHY");
			flag = importDataDao.updateBaseInfo(map);
			if(!flag){
				System.out.println("===========================================出现错误=========================id:"+id);
			}
		}
		return flag;
	}
	

}
