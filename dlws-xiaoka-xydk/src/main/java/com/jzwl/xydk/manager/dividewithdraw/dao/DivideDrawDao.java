package com.jzwl.xydk.manager.dividewithdraw.dao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("divideDrawDao")
public class DivideDrawDao {
	@Autowired
	private BaseDAO baseDAO;
	
	public PageObject queryDivideDrawforPage(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select c.name,t.id,t.openId,t.divideOrWithdraw,t.balanceMoney,t.state,t.divideOrWithdrawTime "
				+ "from `xiaoka-xydk`.divideorwithdrawrecord t left join v2_wx_customer c on t.openId = c.openId where 1=1 ");
		if(null !=map.get("name") && StringUtils.isNotEmpty(map.get("name").toString())){
			sb.append(" and c.name like '%" + map.get("name") +"%'");
	  	}
		if(null !=map.get("state") && StringUtils.isNotEmpty(map.get("state").toString())){
			sb.append(" and t.state =" + map.get("state"));
	  	}
		if (null != map.get("beginDate") && StringUtils.isNotEmpty(map.get("beginDate").toString())) {
			sb.append(" and  DATE_FORMAT(t.divideOrWithdrawTime,'%Y-%m-%d') >= DATE_FORMAT('"+map.get("beginDate")+"','%Y-%m-%d')");
		}
		if (null != map.get("endDate") && StringUtils.isNotEmpty(map.get("endDate").toString())) {
			sb.append(" and  DATE_FORMAT(t.divideOrWithdrawTime,'%Y-%m-%d') <= DATE_FORMAT('"+map.get("endDate")+"','%Y-%m-%d')");
		}
		sb.append(" order by id");
		PageObject po = baseDAO.queryForMPageList(sb.toString(), new Object[] {}, map);
		return po;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：插入数据
	 * 创建人： sx
	 * 创建时间： 2017年5月11日
	 * 标记：
	 * @param recordMap
	 * @version
	 */
	public void addDraw(Map<String, Object> recordMap) {
		String id = Sequence.nextId();
		recordMap.put("id", id);
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.divideorwithdrawrecord "
				+ " (id, openId, divideOrWithdraw, balanceMoney, state, divideOrWithdrawTime) "
				+ " values(:id, :openId, :divideOrWithdraw, :balanceMoney, :state, :divideOrWithdrawTime)");
		baseDAO.executeNamedCommand(sb.toString(), recordMap);
	}

}
