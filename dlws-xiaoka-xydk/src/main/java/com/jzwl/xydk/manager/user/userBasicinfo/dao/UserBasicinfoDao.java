/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.user.userBasicinfo.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("userBasicinfoDao")
public class UserBasicinfoDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	public boolean addUserBasicinfo(Map<String, Object> map) {
		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id", Sequence.nextId());
		map.put("openId", "0");
		map.put("auditState", 0);
		map.put("createDate", date);
		map.put("isRecommend", 0);//是否推荐
		map.put("skillStatus", 0);
		map.put("isDelete", 0);
		//map.put("viewNum",0);

		//添加用户临时存储的图片和昵称
		addUserTemporary(map);
		String sql = "insert into `xiaoka-xydk`.user_basicinfo "
				+ " (id,userName,cityId,schoolId,phoneNumber,email,wxNumber,openId,aboutMe,"
				+ "checkType,nickname,headPortrait,auditState,viewNum,createDate,createUser,isRecommend,skillStatus,isDelete) "
				+ " values "
				+ " (:id,:userName,:cityId,:schoolId,:phoneNumber,:email,:wxNumber,:openId,:aboutMe,"
				+ ":checkType,:nickname,:picUrl,:auditState,:viewNum,:createDate,:createUser,:isRecommend,:skillStatus,:isDelete)";
		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean addUserTemporary(Map<String, Object> map) {
		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id", Sequence.nextId());
		map.put("userId", map.get("id"));

		String sql = "insert into `xiaoka-xydk`.user_temporary " + " (id,userId,nickname,picUrl) " + " values "
				+ " (:id,:userId,:nickname,:picUrl)";

		return baseDAO.executeNamedCommand(sql, map);
	}

	//查询列表使用，多了一个字段wxl   e.id  typeName   typeValue
	public String getColumnsList() {
		return "" + " t.id as id," + " t.userName as userName," + " t.cityId as cityId," + " c.cityName as cityName,"
				+ " t.schoolId as schoolId," + " si.schoolName as schoolName," + " t.phoneNumber as phoneNumber,"
				+ " t.email as email," + " t.wxNumber as wxNumber," + " t.openId as openId," + " t.aboutMe as aboutMe,"
				+ " t.auditState as auditState," + " t.auditDate as auditDate," + " t.auditUser as auditUser,"
				+ " t.auditNotes as auditNotes," + " t.viewNum as viewNum," + " t.createDate as createDate,"
				+ " t.createUser as createUser," + " t.isRecommend as isRecommend," + " t.skillStatus as skillStatus,"
				+ " t.isDelete as isDelete," + " e.id as eid," + " e.typeName as typeName,"
				+ " e.typeValue as typeValue";
	}

	//查询单个使用，多了一个字段，未改动wxl
	public String getColumns() {
		return "" + " t.id as id," + " t.userName as userName," + " t.cityId as cityId," + " c.cityName as cityName,"
				+ " t.schoolId as schoolId," + " si.schoolName as schoolName," + " t.phoneNumber as phoneNumber,"
				+ " t.email as email," + " t.wxNumber as wxNumber," + " t.openId as openId," + " t.aboutMe as aboutMe,"
				+ " t.auditState as auditState," + " t.auditDate as auditDate," + " t.auditUser as auditUser,"
				+ " t.auditNotes as auditNotes," + " t.viewNum as viewNum," + " t.createDate as createDate,"
				+ " t.createUser as createUser," + " t.isRecommend as isRecommend," + " t.skillStatus as skillStatus,"
				+ " t.isDelete as isDelete";
	}

	public PageObject queryUserBasicinfoList(Map<String, Object> map) {
		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性

		String sql = "select " + getColumnsList() + " from `xiaoka-xydk`.user_basicinfo t "
				+ " LEFT JOIN `xiaoka`.tg_city c ON t.cityId = c.id "
				+ " LEFT JOIN `xiaoka`.tg_school_info si ON si.id = t.schoolId "
				+ " LEFT JOIN `xiaoka-xydk`.entertype e ON e.id=t.enterId " + " where t.openId='0' and t.isDelete = 0 ";

		if (null != map.get("userName") && StringUtils.isNotEmpty(map.get("userName").toString())) {
			sql = sql + " and t.userName  like '%" + map.get("userName") + "%'";
		}
		if (null != map.get("cityId") && StringUtils.isNotEmpty(map.get("cityId").toString())) {
			sql = sql + " and t.cityId  = " + map.get("cityId") + "";
		}
		if (null != map.get("schoolId") && StringUtils.isNotEmpty(map.get("schoolId").toString())) {
			sql = sql + " and t.schoolId  = " + map.get("schoolId") + "";
		}
		if (null != map.get("phoneNumber") && StringUtils.isNotEmpty(map.get("phoneNumber").toString())) {
			sql = sql + " and t.phoneNumber  = " + map.get("phoneNumber") + "";
		}
		if (null != map.get("email") && StringUtils.isNotEmpty(map.get("email").toString())) {
			sql = sql + " and t.email  = " + map.get("email") + "";
		}
		if (null != map.get("wxNumber") && StringUtils.isNotEmpty(map.get("wxNumber").toString())) {
			sql = sql + " and t.wxNumber  = " + map.get("wxNumber") + "";
		}
		if (null != map.get("openId") && StringUtils.isNotEmpty(map.get("openId").toString())) {
			sql = sql + " and t.openId  = " + map.get("openId") + "";
		}
		if (null != map.get("aboutMe") && StringUtils.isNotEmpty(map.get("aboutMe").toString())) {
			sql = sql + " and t.aboutMe  = " + map.get("aboutMe") + "";
		}
		if (null != map.get("auditState") && StringUtils.isNotEmpty(map.get("auditState").toString())) {
			sql = sql + " and t.auditState  = " + map.get("auditState") + "";
		}
		if (null != map.get("auditDate") && StringUtils.isNotEmpty(map.get("auditDate").toString())) {
			sql = sql + " and t.auditDate >= " + map.get("auditDateBegin") + "";
			sql = sql + " and t.auditDate <= " + map.get("auditDateEnd") + "";
		}
		if (null != map.get("auditUser") && StringUtils.isNotEmpty(map.get("auditUser").toString())) {
			sql = sql + " and t.auditUser  = " + map.get("auditUser") + "";
		}
		if (null != map.get("auditNotes") && StringUtils.isNotEmpty(map.get("auditNotes").toString())) {
			sql = sql + " and t.auditNotes  = " + map.get("auditNotes") + "";
		}
		if (null != map.get("viewNum") && StringUtils.isNotEmpty(map.get("viewNum").toString())) {
			sql = sql + " and t.viewNum  = " + map.get("viewNum") + "";
		}
		if (null != map.get("createDate") && StringUtils.isNotEmpty(map.get("createDate").toString())) {
			sql = sql + " and t.createDate >= " + map.get("createDateBegin") + "";
			sql = sql + " and t.createDate <= " + map.get("createDateEnd") + "";
		}
		if (null != map.get("createUser") && StringUtils.isNotEmpty(map.get("createUser").toString())) {
			sql = sql + " and t.createUser  = " + map.get("createUser") + "";
		}
		if (null != map.get("isRecommend") && StringUtils.isNotEmpty(map.get("isRecommend").toString())) {
			sql = sql + " and t.isRecommend  = " + map.get("isRecommend") + "";
		}
		if (null != map.get("skillStatus") && StringUtils.isNotEmpty(map.get("skillStatus").toString())) {
			sql = sql + " and t.skillStatus  = " + map.get("skillStatus") + "";
		}
		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + " and t.isDelete  = " + map.get("isDelete") + "";
		}
		if (null != map.get("typeName") && StringUtils.isNotEmpty(map.get("typeName").toString())) {
			sql = sql + " and t.typeName  = " + map.get("typeName") + "";
		}
		if (null != map.get("typeValue") && StringUtils.isNotEmpty(map.get("typeValue").toString())) {
			sql = sql + " and t.typeValue  = " + map.get("typeValue") + "";
		}

		sql = sql + " order by t.id desc";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);

		return po;
	}

	public boolean updateUserTemporary(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.user_temporary set " + " nickname=:nickname,picUrl=:picUrl "
				+ " where userId=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean updateUserBasicinfo(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.user_basicinfo set "
				+ " userName=:userName,cityId=:cityId,schoolId=:schoolId,phoneNumber=:phoneNumber,email=:email,wxNumber=:wxNumber,aboutMe=:aboutMe,checkType=:checkType,viewNum=:viewNum "
				+ " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	public boolean deleteUserBasicinfo(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.user_basicinfo set " + " isDelete = 1 " + " where id=:id";
		String infoDataSql = "update `xiaoka-xydk`.user_skillinfo_data  set " + " isDelete = 1 " + " where pid=:id";
		baseDAO.executeNamedCommand(infoDataSql, map);
		return baseDAO.executeNamedCommand(sql, map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		Map<String, Object> resMap = new HashMap<String, Object>();

		String sql = "select "
				+ getColumns()
				+ ",t.enterId,t.contactUser,t.headPortrait,t.viewNum,t.checkType,t.nickname as nickname from `xiaoka-xydk`.user_basicinfo t "
				//+ " left join `xiaoka-xydk`.user_temporary ut on t.id = ut.userId "
				+ " LEFT JOIN `xiaoka`.tg_city c ON t.cityId = c.id "
				+ " LEFT JOIN `xiaoka`.tg_school_info si ON si.id = t.schoolId " + " where t.id = " + map.get("id")
				+ "";

		resMap = baseDAO.queryForMap(sql);

		return resMap;

	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getProvence(String cityId) {
		Map<String, Object> resMap = new HashMap<String, Object>();
		String sql = "SELECT id,cityName,province,number,cityOperateManager,phone,longitude,latitude,remark "
				+ "FROM tg_city WHERE isDelete=0 and id=" + cityId + " AND province IS NOT NULL  GROUP BY   province ";
		resMap = baseDAO.queryForMap(sql);
		return resMap;
	}

	//查询所属学校
	@SuppressWarnings("unchecked")
	public Map<String, Object> getSholName(String sholId) {
		Map<String, Object> resMap = new HashMap<String, Object>();
		String sql = "SELECT * FROM tg_school_info shol WHERE id=" + sholId + "";
		resMap = baseDAO.queryForMap(sql);
		return resMap;
	}

	/**
	 * @param map
	 * @return
	 */
	public List querySchoolInfoByCityId(Map<String, Object> map) {
		String sql = "select *,t.id as schoolId from xiaoka.tg_school_info t left join xiaoka.tg_city tc on t.cityId = tc.id where t.cityId='"
				+ map.get("cityId") + "'";
		return baseDAO.queryForList(sql);
	}

	/**
	 * 根据省查市
	 * @param map
	 * @return list
	 * dxf
	 */
	public List queryCityByPro(String provence) {
		String sql = "SELECT id as value,cityName as text " + " FROM tg_city  WHERE isDelete=0 and province='"
				+ provence + "'";
		return baseDAO.queryForList(sql);
	}

	/**
	 * 获取城市的基本信息从校咖（xiaoka）中获取
	 * 
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getCityInfo() {

		String sql = "select * from xiaoka.tg_city where isDelete = 0";

		return baseDAO.queryForList(sql);

	}

	/**
	 * 获取字典表中的信息
	 * 根据CODE值
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getFieldtype(String code) {

		String sql = "SELECT id,dic_name,dic_value FROM v2_dic_data WHERE v2_dic_data.dic_id =(SELECT dic_id FROM xiaoka.v2_dic_type WHERE dic_code =?) and isDelete='0'";
		List<Map<String, Object>> fieldList = baseDAO.getJdbcTemplate().queryForList(sql, new Object[] { code });
		return fieldList;

	}

	/* 添加入驻资源
	* 
	* @param map
	* @return flag
	*/
	public boolean addSettleUser(Map<String, Object> map) {
		Date date = new Date();

		String openId = String.valueOf(map.get("openId"));
		map.put("auditState", 0);//移动端默认未审核  PC端也默认未审核
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		
		String basId = Sequence.nextId();
		map.put("id", basId);
		map.put("skillStatus", 0);
		map.put("createDate", date);
		map.put("isRecommend", 0);//是否推荐
		map.put("isDelete", 0);
		
		if ("".equals(openId) || openId == null || "null".equals(openId) || "0".equals(openId)) {
			map.put("openId", "0");
			map.put("skillStatus", 1);//PC端默认为关闭  '空间状态(0:打开，1：关闭)',
		}
		String viewNum = String.valueOf(map.get("viewNum"));
		if ("".equals(viewNum) || viewNum == null || "null".equals(viewNum)) {
			map.put("viewNum", 0);
		}
		//map.put("viewNum",0);
		Map<String, Object> daMap = new HashMap<String, Object>();
		daMap = map;

		String addBasiSql = "insert into `xiaoka-xydk`.user_basicinfo "
				+ " (id,userName,nickname,cityId,schoolId,phoneNumber,email,wxNumber,openId,aboutMe,"
				+ "headPortrait,auditState,viewNum,createDate,isRecommend,skillStatus,isDelete,enterId,contactUser) "
				+ " values "
				+ " (:id,:userName,:userName,:cityId,:schoolId,:phoneNumber,:email,:wxNumber,:openId,:aboutMe,"
				+ ":picUrl,:auditState,:viewNum,:createDate,:isRecommend,:skillStatus,:isDelete,:enterId,:contactUser)";

		boolean flag = baseDAO.executeNamedCommand(addBasiSql, map);
		daMap.remove("id");
		daMap.remove("openId");
		daMap.remove("auditState");
		daMap.remove("proId");
		daMap.remove("cityId");
		daMap.remove("cityIds");
		daMap.remove("enterId");
		daMap.remove("enterType");
		daMap.remove("wxNumber");
		daMap.remove("viewNum");
		daMap.remove("picUrl");
		daMap.remove("aboutMe");
		daMap.remove("phoneNumber");
		daMap.remove("activUrl");
		daMap.remove("contactUser");
		daMap.remove("email");
		daMap.remove("scholType");
		daMap.remove("schoolId");
		daMap.remove("userName");
		daMap.remove("keyValue");
		daMap.remove("auditState");
		daMap.remove("createDate");
		daMap.remove("isRecommend");
		daMap.remove("skillStatus");
		daMap.remove("isDelete");
		daMap.remove("nickname");
		daMap.remove("headPortrait");
		daMap.remove("createUser");// chu +

		String DataSql = "insert into `xiaoka-xydk`.user_skillinfo_data(id,pid,typeName,typeValue,isDelete,createDate)"
				+ "values" + "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
		boolean temp = false;
		if (flag) {
			for (Entry<String, Object> entry : daMap.entrySet()) {
				Map<String, Object> inserMap = new HashMap<String, Object>();
				inserMap.put("id", Sequence.nextId());
				inserMap.put("pid", basId);
				inserMap.put("typeName", entry.getKey());
				inserMap.put("typeValue", entry.getValue());
				inserMap.put("isDelete", 0);
				inserMap.put("createDate", new Date());
				temp = baseDAO.executeNamedCommand(DataSql, inserMap);
			}
		}

		return temp;
	}

	/**
	 * dxf
	 * 修改前回显
	 * @param map
	 * @return List
	 */
	public List querySettleListVersion3(Map<String, Object> map) {

		String sql = "SELECT id,pid,typeName,typeValue,isDelete,createDate FROM `xiaoka-xydk`.user_skillinfo_data"
				+ " WHERE pid='" + map.get("id") + "' and isDelete=0";
		return baseDAO.queryForList(sql);
	}

	/**
	 * 根据id获取用户的子集信息
	 * 创建人：gyp
	 * 创建时间：2017年3月16日
	 * 描述：
	 * @param map
	 * @return
	 */
	public Map<String, Object> getSkillinfoData(Map<String, Object> map) {

		String sql = "SELECT id,pid,typeName,typeValue,isDelete,createDate FROM `xiaoka-xydk`.user_skillinfo_detail"
				+ " WHERE id='" + map.get("imgeId") + "'";
		return baseDAO.queryForMap(sql);
	}

	public boolean updateSkillinfoData(Map<String, Object> map) {
		String sql = "update  `xiaoka-xydk`.user_skillinfo_detail set typeValue=:typeValue where id= '"
				+ map.get("imgeId") + "'";
		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 修改入驻
	 * dxf
	 * @param map
	 * @return
	 */
	public boolean upSettleVersion3(Map<String, Object> map) {

		String sql = "DELETE FROM `xiaoka-xydk`.user_skillinfo_data WHERE pid=:id";

		baseDAO.executeNamedCommand(sql, map);

		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		String basId = String.valueOf(map.get("id"));
		String myself = String.valueOf(map.get("myself"));
		String openId = String.valueOf(map.get("openId"));
		
		map.put("auditState", 0);//pc端和移动端默认未审核
		map.put("createDate", date);
		map.put("isRecommend", 0);//是否推荐
		map.put("skillStatus", 0);//技能空间(0:打开，1：关闭)
		map.put("isDelete", 0);

		if ("".equals(openId) || openId == null || "null".equals(openId) || "0".equals(openId)) {
			map.put("openId", "0");
			map.put("skillStatus", 1);//PC 技能空间状态默认为关闭 (0:打开，1：关闭)
		}
		Map<String, Object> daMap = new HashMap<String, Object>();
		daMap = map;

		String upBasiSql = "update  `xiaoka-xydk`.user_basicinfo set userName=:userName,nickname=:userName,auditState=:auditState,"
				+ " cityId=:cityId,schoolId=:schoolId,phoneNumber=:phoneNumber,email=:email,wxNumber=:wxNumber,aboutMe=:aboutMe,"
				+ "headPortrait=:picUrl,viewNum=:viewNum,createDate=:createDate,contactUser=:contactUser where id= '"
				+ map.get("id") + "'";
		baseDAO.executeNamedCommand(upBasiSql, map);
		daMap.remove("id");
		daMap.remove("openId");
		daMap.remove("auditState");
		daMap.remove("proId");
		daMap.remove("cityId");
		daMap.remove("cityIds");
		daMap.remove("enterId");
		daMap.remove("enterType");
		daMap.remove("wxNumber");
		daMap.remove("viewNum");
		daMap.remove("picUrl");
		daMap.remove("aboutMe");
		daMap.remove("phoneNumber");
		daMap.remove("activUrl");
		daMap.remove("contactUser");
		daMap.remove("email");
		daMap.remove("scholType");
		daMap.remove("schoolId");
		daMap.remove("userName");
		daMap.remove("keyValue");
		daMap.remove("auditState");
		daMap.remove("createDate");
		daMap.remove("isRecommend");
		daMap.remove("skillStatus");
		daMap.remove("isDelete");
		daMap.remove("province");
		daMap.remove("createUser");//chu +

		String DataSql = "insert into `xiaoka-xydk`.user_skillinfo_data(id,pid,typeName,typeValue,isDelete,createDate)"
				+ "values" + "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
		for (Entry<String, Object> entry : daMap.entrySet()) {
			Map<String, Object> inserMap = new HashMap<String, Object>();
			inserMap.put("id", Sequence.nextId());
			inserMap.put("pid", basId);
			inserMap.put("typeName", entry.getKey());
			inserMap.put("typeValue", entry.getValue());
			inserMap.put("isDelete", 0);
			inserMap.put("createDate", new Date());
			baseDAO.executeNamedCommand(DataSql, inserMap);
		}

		return true;

	}

	//查询资源类型
	public List<Map<String, Object>> queryResourceListVersion3() {
		String sql = "SELECT * FROM `xiaoka-xydk`.user_skillclass_father WHERE classValue='dxf' AND isDelete=0";
		return baseDAO.queryForList(sql);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getEnterTypeValueVersion3(Map<String, Object> map) {
		Map<String, Object> resMap = new HashMap<String, Object>();
		String sql = "SELECT * FROM `xiaoka-xydk`.entertype WHERE 1=1 and ";//这条sql 谁也不准动

		if (null != map.get("enterId") && StringUtils.isNotEmpty(map.get("enterId").toString())
				&& !"null".equals(map.get("enterId"))) {
			sql = sql + "id= '" + map.get("enterId") + "'";
		} else {
			sql = sql + "typeValue= 'gxst'";
		}

		resMap = baseDAO.queryForMap(sql);
		return resMap;
	}

	public Map<String, Object> getCity(String cityId) {

		Map<String, Object> resMap = new HashMap<String, Object>();
		String sql = "SELECT * FROM tg_city shol WHERE id=" + cityId + "";
		resMap = baseDAO.queryForMap(sql);
		return resMap;

	}

}
