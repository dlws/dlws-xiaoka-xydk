/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.preferService.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.preferService.dao.PreferredServiceDao;

@Service("preferredServiceService")
public class PreferredServiceService {

	@Autowired
	private PreferredServiceDao preferredServiceDao;

	public boolean addPreferredService(Map<String, Object> map) {

		return preferredServiceDao.addPreferredService(map);

	}

	public PageObject queryPreferredServiceList(Map<String, Object> map) {

		return preferredServiceDao.queryPreferredServiceList(map);

	}

	public boolean updatePreferredService(Map<String, Object> map) {

		return preferredServiceDao.updatePreferredService(map);

	}

	public boolean deletePreferredService(Map<String, Object> map) {

		return preferredServiceDao.deletePreferredService(map);

	}

	public boolean delPreferredService(Map<String, Object> map) {

		return preferredServiceDao.delPreferredService(map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getById(Map<String, Object> map) {

		return preferredServiceDao.getById(map);
	}

	/**
	 * 
	 * 项目名称： 描述：对应数据库设计时对应的下拉，单选框 值-名 创建人： 创建时间： 标记：
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySelectValue(Map<String, Object> map) {

		return preferredServiceDao.querySelectValue(map);
	}

	/**
	 * 获取优质服务 创建人：gyp 创建时间：2017年3月23日 描述：
	 * 
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getService(String enterClass) {

		return preferredServiceDao.getService(enterClass);
	}

	public List<Map<String, Object>> preferClass() {

		// TODO Auto-generated method stub
		return preferredServiceDao.preferClass();

	}

	public Map<String, Object> getDicDataInfo(Map<String, Object> map) {
		return preferredServiceDao.getDicDataInfo(map);
	}
	
	public boolean updateSkillPayNoPre(Map<String, Object> map){
		return preferredServiceDao.updateSkillPayNoPre(map);
	}
	
	//根据当前
	public Map<String, Object> getPreferredService(Map<String, Object> map){
		return preferredServiceDao.getPreferredService(map);
	}
	
	

}
