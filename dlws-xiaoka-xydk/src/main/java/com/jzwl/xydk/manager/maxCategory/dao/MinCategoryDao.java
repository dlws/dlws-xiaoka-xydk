package com.jzwl.xydk.manager.maxCategory.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("minCategoryDao")
public class MinCategoryDao {
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：二级技能分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public PageObject minCategoryList(Map<String, Object> map) {
		try {
			String sql = " SELECT ss.*, sf.className AS fclassName " + "   FROM `xiaoka-xydk`.user_skillclass_son ss "
					+ "   LEFT JOIN `xiaoka-xydk`.user_skillclass_father sf ON sf.id = ss.fatherId "
					+ "   where ss.isDelete = 0 ";
			if (null != map.get("className") && StringUtils.isNotEmpty(map.get("className").toString())) {
				sql = sql + " and ss.className like '%" + map.get("className") + "%'";
			}
			if (null != map.get("fatherId") && StringUtils.isNotEmpty(map.get("fatherId").toString())) {
				sql = sql + " and ss.fatherId  = '" + map.get("fatherId") + "'";
			}
			sql = sql + " order by sf.createDate desc ";
			PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);
			return po;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去修改二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public Map<String, Object> toEditMinCategory(Map<String, Object> map) {
		try {
			String sql = " SELECT ss.id,ss.className,ss.fatherId,ss.`status`,ss.ord,ss.classValue,ss.createDate,ss.serviceRatio,ss.diplomatRatio,ss.platformRatio "
					+ "   FROM `xiaoka-xydk`.user_skillclass_son ss " + "   where ss.isDelete = 0 and ss.id="
					+ map.get("id") + "";
			return baseDAO.queryForMap(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：添加二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean addMinCategory(Map<String, Object> map) {
		try {
			map.put("id", Sequence.nextId());
			map.put("createDate", new Date());
			String sql = " insert into `xiaoka-xydk`.user_skillclass_son "
					+ "    (id,fatherId,className,status,ord,createDate,classValue,isDelete,serviceRatio,diplomatRatio,platformRatio) " + "    values "
					+ "    (:id,:fatherId,:className,:status,:ord,:createDate,:classValue,0,:serviceRatio,:diplomatRatio,:platformRatio)";
			baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean editMinCategory(Map<String, Object> map) {
		try {
			String sql = "update `xiaoka-xydk`.user_skillclass_son set "
					+ " className=:className,fatherId=:fatherId,status=:status,classValue=:classValue,ord=:ord,serviceRatio=:serviceRatio,diplomatRatio=:diplomatRatio,platformRatio=:platformRatio"
					+ " where id=:id";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：删除二级分类
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	public boolean deleteMinCategory(Map<String, Object> map) {
		try {
			String sql = "update `xiaoka-xydk`.user_skillclass_son set " + " isDelete= 1 " + " where id=:id";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取一级分类列表
	 * 创建人： ln
	 * 创建时间： 2016年10月14日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getMaxCategoryList(Map<String, Object> paramsMap) {
		try {
			String sql = " SELECT sf.id,sf.className,sf.imageURL,sf.`status`,sf.ord,sf.createDate "
					+ "   FROM `xiaoka-xydk`.user_skillclass_father sf " + "   where isDelete = 0 and sf.systemType = 0";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
