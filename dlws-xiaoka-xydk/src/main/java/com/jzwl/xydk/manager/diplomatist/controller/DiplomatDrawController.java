package com.jzwl.xydk.manager.diplomatist.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.diplomatist.service.DiplomatDrawService;

/**
 * 后台外交官提现Controller
 * @author Administrator
 */

@Controller
@RequestMapping(value = "/diplomatDraw")
public class DiplomatDrawController extends BaseWeixinController {

	@Autowired
	private DiplomatDrawService diplomatDrawService;

	Log log = LogFactory.getLog(getClass());

	/**
	 * 展示当前外交官的提现申请信息（所有）
	 * @return
	 */
	@RequestMapping(value = "/getApplyWithDraw")
	public String getApplyWithDraw(HttpServletRequest request, Model model) {

		createParameterMap(request);
		PageObject po = diplomatDrawService.getApplyWithDraw(paramsMap);

		model.addAttribute("po", po);
		model.addAttribute("list", po.getDatasource());
		model.addAttribute("totalProperty", po.getTotalCount());
		model.addAttribute("paramsMap", paramsMap);

		return "/manager/diplomatist/diplomat_draw/diplomat_draw_list";
	}

	/**
	 * 展示当前提现的单条信息
	 * 必传参数当前信息的id值
	 * @param id
	 */
	@RequestMapping(value = "/getSingleApplyWithDraw", params = { "id" })
	public String getSingleApplyWithDraw(HttpServletRequest request, Model model) {

		createParameterMap(request);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", paramsMap.get("id"));
		PageObject po = diplomatDrawService.getApplyWithDraw(paramsMap);
		List<Map<String, Object>> list = po.getDatasource();

		if (list.size() == 0) {
			model.addAttribute("msg", "未查询到当前退款条目");
			return "/error";
		}
		model.addAttribute("draw", list.get(0));
		model.addAttribute("start", paramsMap.get("start"));
		return "/manager/diplomatist/diplomat_draw/diplomat_draw_review";
	}

	@RequestMapping(value = "/updateApplyWithDraw", params = { "id", "openId", "applicationStatus" })
	public String updateApplyWithDraw(HttpServletRequest request, Model model) {

		createParameterMap(request);
		//1：为通过，2不通过
		if ("1".equals(paramsMap.get("applicationStatus").toString())) {

			paramsMap.put("userName", request.getSession().getAttribute("userName"));
			diplomatDrawService.accessAppaly(paramsMap);
			return "redirect:/diplomatDraw/getApplyWithDraw.html";

		} else if ("2".equals(paramsMap.get("applicationStatus").toString())) {

			if (StringUtils.isNotBlank(paramsMap.get("refuseReason").toString())) {

				Map<String, Object> temporaryMap = new HashMap<String, Object>();
				temporaryMap.put("id", paramsMap.get("id").toString());
				temporaryMap.put("applicationStatus", 2);
				temporaryMap.put("refuseReason", paramsMap.get("refuseReason").toString());
				temporaryMap.put("userName", request.getSession().getAttribute("userName"));
				diplomatDrawService.notAccessAppaly(temporaryMap);

				return "redirect:/diplomatDraw/getApplyWithDraw.html";
			}
			log.info("-----审核不通过理由不能为空----------");
			model.addAttribute("msg", "修改失败");
			return "/error";

		} else {
			log.info("-----当前传入状态有误----" + paramsMap.get("applicationStatus").toString());
			model.addAttribute("msg", "修改失败");
			return "/error";
		}

	}

}
