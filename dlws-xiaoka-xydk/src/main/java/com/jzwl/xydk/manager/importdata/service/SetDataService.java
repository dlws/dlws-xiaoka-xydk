package com.jzwl.xydk.manager.importdata.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.jzwl.common.id.Sequence;
import com.jzwl.xydk.manager.importdata.dao.ImportDataDao;
import com.jzwl.xydk.manager.importdata.dao.SetDataDao;
import com.jzwl.xydk.manager.importdata.pojo.ImportData;

@Service("setDataService")
public class SetDataService {
	
	@Autowired
	private ImportDataDao importDataDao;
	
	@Autowired
	private SetDataDao setDataDao;
	
	public List<ImportData> getData(){
		
		List<ImportData> list = importDataDao.getInfo();
		int i = 0;
		//对业务进行处理
		for(ImportData importData:list){
			
			//根据城市名称判断该城市下是否有没有需要的城市 如果没有数据，则进行添加
			List<ImportData> citylist=importDataDao.getCityInfo(importData.getCity());
			if(citylist.size() == 0){
				//添加城市
				boolean flag_city = importDataDao.addCity(importData.getProvice(), importData.getCity(), importData.getId());
				//存储城市成功，存储该城市下的学校
				if(flag_city){
					//从xk-city-school-info获取学校的数据
					List<ImportData> listCity =  importDataDao.getInfoByCity(importData.getCity());
					for(ImportData cityData:listCity){
						//根据学校名称判断该城市下是否含有这个学校
						List<ImportData> listSchool = importDataDao.getSchoolInfo(cityData.getSchool());
						//if(listSchool.size() == 0){//没有该学校，进行添加
							boolean temp_school = importDataDao.addSchool(cityData.getProvice(), cityData.getCity(), cityData.getId(), cityData.getSchool(), importData.getId());
							if(temp_school){
								//得到标签表
								//System.out.println("===============success====================="+cityData.getLevel());
								String  str = cityData.getLevel().replace(" ", "");
								 Map<String,Object> map =  getLabelData(str);
								 Long dicDataId = Long.parseLong(map.get("id").toString());
								//学校添加成功，维护标签表
								 boolean temp_school_label = importDataDao.addSchoolLabel(dicDataId, cityData.getId());
								 if(temp_school_label){
									 i=i+1;
									 System.out.println("===============success====================="+i+"school"+cityData.getSchool());
								 }
							}
						//}
					}
				}
			}
			
		}
		return list;
	}
	
	public Map<String,Object> getLabelData(String temp){
		Map<String,Object> map = importDataDao.getLabelData(temp);
		return map;
	}
	//设置高校社团的数据
	
	/*serviceType (服务性质 线上、线下)   
	skillPrice（服务价格、技能价格、投放价格）
	company（服务单位）
	activtyUrl（活动图片/图片）
	cooperaType（合作形式）  存储方式（{"合作形式标识":"价格","合作形式标识":"价格"}）
	closePhoto（近景）
	allPhoto（全景）
	middlePho（中景）
	partimejob(是否提供兼职人员)
	endDate(结束时间)
	beginDate（开始时间）
	filedSize(场地规模)   字典的Id
	pubNumId( 公众号Id )
	fansNum（粉丝人数）
	readNum（平均阅读量）
	topLinePrice（头条报价）
	lessLinePrice（次条报价）
	myAvatarVal（公众号头像/社群头像）
	dairyTweetUrl（日常推文图片/群成员照片）*/
	public boolean setBaseInfoGXST(){
		boolean flag = false;
		//得到高校社团的用户的Id
		try {
			List<Map<String,Object>> list = setDataDao.getUserSkillInfo("1476947596301004");
			for(Map<String,Object> map:list){
				String pid = map.get("id").toString();
				List<Map<String,Object>> images = setDataDao.getUserSkillImage(pid);
				String iUrl = "";
				for(Map<String,Object> map_images :images){
					String imageUrl = map_images.get("imgURL").toString();
					iUrl = iUrl+imageUrl+",";
				}
				map.put("activtyUrl", iUrl);
				
				//获取高校社团的技能
				for (Entry<String, Object> entry : map.entrySet()) {
					Map<String, Object> inserMap = new HashMap<String, Object>();
					if("0".equals(entry.getValue())){
						entry.setValue("1487926190578001");
					}
					inserMap.put("id", Sequence.nextId());
					inserMap.put("pid", pid);
					inserMap.put("typeName", entry.getKey());
					inserMap.put("typeValue", entry.getValue());
					inserMap.put("isDelete", 0);
					inserMap.put("createDate", new Date());
					flag = setDataDao.addUserSkillInfoDetail(inserMap);
					if(flag){
						System.out.println("=================================GXST存储成功=======================================");
					}else{
						System.out.println("=================================GXST存储失败=======================================");
					}
				}
			}
		} catch (Exception e) {
			System.out.println("********************************************************GXST出现异常*************************");
		}
		return flag;
	}
	
	//技能达人
	public boolean setBaseInfoJNDR(){
		boolean flag = false;
		try {
			//得到高校社团的用户的Id
			List<Map<String,Object>> list = setDataDao.getUserSkillInfo("1476964838236008");
			for(Map<String,Object> map:list){
				String pid = map.get("id").toString();
				List<Map<String,Object>> images = setDataDao.getUserSkillImage(pid);
				String iUrl = "";
				for(Map<String,Object> map_images :images){
					String imageUrl = map_images.get("imgURL").toString();
					iUrl = iUrl+imageUrl+",";
				}
				map.put("activtyUrl", iUrl);
				
				//获取高校社团的技能
				for (Entry<String, Object> entry : map.entrySet()) {
					Map<String, Object> inserMap = new HashMap<String, Object>();
					inserMap.put("id", Sequence.nextId());
					inserMap.put("pid", pid);
					inserMap.put("typeName", entry.getKey());
					inserMap.put("typeValue", entry.getValue());
					inserMap.put("isDelete", 0);
					inserMap.put("createDate", new Date());
					flag = setDataDao.addUserSkillInfoDetail(inserMap);
					if(flag){
						System.out.println("=================================JNDR存储成功=======================================");
					}else{
						System.out.println("=================================JNDR存储失败=======================================");
					}
				}
			}
		} catch (Exception e) {
			System.out.println("********************************************************JNDR出现异常*************************");
		}
		return flag;
	}
	
	
	/*partimejob  //是否提供兼职
	planprice   //场地价格
	material    //资源配置
	fieldType   //场地类型
	filedSize   //场地规模 */	
	
	/*serviceType (服务性质 线上、线下)   
	skillPrice（服务价格、技能价格、投放价格）
	company（服务单位）
	activtyUrl（活动图片）
	cooperaType（合作形式）  存储方式（{"合作形式标识":"价格","合作形式标识":"价格"}）
	closePhoto（近景）
	allPhoto（全景）
	middlePho（中景）
	partimejob(是否提供兼职人员)
	endDate(结束时间)
	beginDate（开始时间）
	filedSize(场地规模)   字典的Id
	pubNumId( 公众号Id )
	fansNum（粉丝人数）
	readNum（平均阅读量）
	topLinePrice（头条报价）
	lessLinePrice（次条报价）
	myAvatarVal（公众号头像/社群头像）
	dairyTweetUrl（日常推文图片/群成员照片）*/
	//场地资源
	public boolean setBaseInfoCDZY(){
		boolean flag = false;
			/*List<Map<String,Object>> list_bas = setDataDao.getBaseInfo("1489632337738002");//获取场地资源的用户
			for(Map<String,Object> map_bas:list_bas){
				String pid = map_bas.get("id").toString();
				//获取user_skillinfo_data中的数据
				List<Map<String,Object>> list_skillDate = setDataDao.getUserSkillinfoData(pid);
				Map<String,Object> map = new HashMap<String,Object>();
				String typeName = "";
				for(Map<String,Object> map_skillDate:list_skillDate){
					if((map_skillDate.get("typeName").toString()).equals("end")){
						typeName = "endDate";
					}else if((map_skillDate.get("typeName").toString()).equals("begin")){
						typeName = "beginDate";
					}else if((map_skillDate.get("typeName").toString()).equals("planprice")){
						typeName = "skillPrice";
					}else{
						map.put(map_skillDate.get("typeName").toString(), map_skillDate.get("typeValue"));
					}
				}
			}*/
			
			try {
				//得到高校社团的用户的Id  
				List<Map<String,Object>> list = setDataDao.getUserSkillInfoTwo("1476947596301006");
				for(Map<String,Object> map:list){
					String pid = map.get("id").toString();
					List<Map<String,Object>> images = setDataDao.getUserSkillImage(pid);
					String iUrl = "";
					for(Map<String,Object> map_images :images){
						String imageUrl = map_images.get("imgURL").toString();
						iUrl = iUrl+imageUrl+",";
					}
					
					map.put("allPhoto", iUrl);
					map.put("closePhoto", iUrl);
					map.put("middlePho", iUrl);
					map.put("material", "1494582970192006");
					map.put("beginDate", new Date());
					map.put("endDate", new Date());
					map.put("partimejob", "0");
					map.put("filedSize", "1489666326704008");
					
					//获取高校社团的技能
					for (Entry<String, Object> entry : map.entrySet()) {
						Map<String, Object> inserMap = new HashMap<String, Object>();
						inserMap.put("id", Sequence.nextId());
						inserMap.put("pid", pid);
						inserMap.put("typeName", entry.getKey());
						inserMap.put("typeValue", entry.getValue());
						inserMap.put("isDelete", 0);
						inserMap.put("createDate", new Date());
						flag = setDataDao.addUserSkillInfoDetail(inserMap);
						if(flag){
							System.out.println("=================================CDZY存储成功=======================================");
						}else{
							System.out.println("=================================CDZY存储失败=======================================");
						}
					}
				}
			} catch (Exception e) {
				System.out.println("********************************************************CDZY出现异常*************************");
			}
			return flag;
	}	
	
	/**
	 * 
	 * 描述:校内商家
	 * 作者:gyp
	 * @Date	 2017年5月12日
	 */
	public boolean setBaseInfoXNSJ(){
		boolean flag = false;
		try {
			//得到高校社团的用户的Id  
			List<Map<String,Object>> list = setDataDao.getUserSkillInfoTwo("1477031814287004");
			for(Map<String,Object> map:list){
				String pid = map.get("id").toString();
				List<Map<String,Object>> images = setDataDao.getUserSkillImage(pid);
				String iUrl = "";
				for(Map<String,Object> map_images :images){
					String imageUrl = map_images.get("imgURL").toString();
					iUrl = iUrl+imageUrl+",";
				}
				map.put("allPhoto", iUrl);
				map.put("closePhoto", iUrl);
				map.put("middlePho", iUrl);
				map.put("cooperate", "1489749409858004");
				Map<String,Object> map1 = new HashMap<String,Object>();
				map1.put("1489749409858004", "1487926190578001");
				String str=JSON.toJSON(map1).toString();
				map.put("cooperaCompany", str);
				 
				
				Map<String,Object> map2 = new HashMap<String,Object>();
				map2.put("1489749409858004", map.get("skillPrice"));
				String str2=JSON.toJSON(map2).toString();
				map.put("cooperaType", str2);
				
				map.put("material", "1494582970192006");
				
				//获取高校社团的技能
				for (Entry<String, Object> entry : map.entrySet()) {
					Map<String, Object> inserMap = new HashMap<String, Object>();
					inserMap.put("id", Sequence.nextId());
					inserMap.put("pid", pid);
					inserMap.put("typeName", entry.getKey());
					inserMap.put("typeValue", entry.getValue());
					inserMap.put("isDelete", 0);
					inserMap.put("createDate", new Date());
					flag = setDataDao.addUserSkillInfoDetail(inserMap);
					if(flag){
						System.out.println("=================================XNSJ存储成功=======================================");
					}else{
						System.out.println("=================================XNSJ存储失败=======================================");
					}
				}
			}
		} catch (Exception e) {
			System.out.println("********************************************************XNSJ出现异常*************************");
		}
		return flag;
	}
	
	//社群大V ---自媒体
	public boolean setBaseInfoVZMT(){
		boolean flag = false;
		try {
			//得到高校社团的用户的Id  
			List<Map<String,Object>> list = setDataDao.getUserSkillInfoTwo("1476947596301002");
			for(Map<String,Object> map:list){
				
				String pid = map.get("id").toString();
				
				List<Map<String,Object>> images = setDataDao.getUserSkillImage(pid);
				String iUrl = "";
				for(Map<String,Object> map_images :images){
					String imageUrl = map_images.get("imgURL").toString();
					iUrl = iUrl+imageUrl+",";
				}
				
				map.put("dairyTweetUrl",iUrl);
				map.put("activtyUrl", iUrl);
				Map<String,Object> m = setDataDao.getUserSkillInfoById(pid);
				if(("1489635097960005".equals(m.get("skillSonId").toString()))){//自媒体
					map.put("topLinePrice", m.get("skillPrice"));
					map.put("lessLinePrice", m.get("skillPrice"));
				}else{//非自媒体
					map.put("skillPrice", m.get("skillPrice"));
					map.put("fansNum", 20);
					map.put("company", "1487926190578001");
				}
				
				//获取高校社团的技能
				for (Entry<String, Object> entry : map.entrySet()) {
					Map<String, Object> inserMap = new HashMap<String, Object>();
					inserMap.put("id", Sequence.nextId());
					inserMap.put("pid", pid);
					inserMap.put("typeName", entry.getKey());
					inserMap.put("typeValue", entry.getValue());
					inserMap.put("isDelete", 0);
					inserMap.put("createDate", new Date());
					flag = setDataDao.addUserSkillInfoDetail(inserMap);
					if(flag){
						System.out.println("=================================V存储成功=======================================");
					}else{
						System.out.println("=================================V存储失败=======================================");
					}
				}
			}
		} catch (Exception e) {
			
			System.out.println("********************************************************V出现异常*************************");
			e.printStackTrace();
		}
		return flag;
	}
	
	
	
	/**
	 * 整理当前的数据
	 */
	public void setSubscribeScaleForUserDteail(){
		
		//1.查询所有场地资源入驻的信息
		List<Map<String,Object>> list = setDataDao.searchUser_skillinfo();
		
		for (Map<String, Object> map : list) {
			
			String skillId = map.get("id").toString();
			//查询当前数据是否有当前key值
			if(setDataDao.searchDataForUser_skillinfo_detail(skillId)){
				//如果没有则执行插入操作
				setDataDao.insertDataForUser_skillinfo_detail(skillId);
			}
			
		}
		
	}
	
	
	
	

}
