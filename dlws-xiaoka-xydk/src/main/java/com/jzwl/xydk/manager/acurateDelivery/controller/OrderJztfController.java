package com.jzwl.xydk.manager.acurateDelivery.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.acurateDelivery.service.OrderJztfService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.Order;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;

@Controller
@RequestMapping("/orderJztf")
public class OrderJztfController extends BaseController {
	@Autowired
	private OrderJztfService orderJztfService;

	@Autowired
	private SendMessageService sendMessage;
	@Autowired
	private OrderSeaService orderSeaService;

	/**
	 * 精准投放列表
	 * <p>
	 * nikeodong update 20170710
	 */
	@RequestMapping(value = "/orderJztfList")
	public ModelAndView orderJztfList(@RequestParam Map<String, Object> map, ModelAndView mov) {

		mov.addObject("po", orderJztfService.queryOrderJztfList(map));
		mov.addObject("orderId", map.get("orderId"));
		mov.setViewName("manager/orderJztf/orderJztfList");
		return mov;
	}

	/**
	 * 修改bannerInfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/auditOrder")
	public String auditOrder(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			boolean flag = orderJztfService.auditOrder(paramsMap);
			Order orderInfo = orderSeaService.getOrderInfo(paramsMap.get("orderId").toString());
			Map<String, Object> map = new HashMap<String, Object>();
			if (flag) {

				//推送消息给卖家的消息模板
				sendMessage.remindOrderToSellerOrderSendMessage(paramsMap.get("orderId").toString());
				//推送关于提醒买家的模板消息
				map.put("openId", orderInfo.getOpenId());
				map.put("auditType", "你的审核请求已经处理。");
				map.put("matter", "精准投放");
				map.put("auditStatus", "成功");
				sendMessage.sendAuditSuccessMessage_JZTF(map);//发送文本消息
				return "redirect:/orderJztf/orderJztfList.html?start=0";
			} else {
				map.put("openId", orderInfo.getOpenId());
				map.put("auditType", "你的审核请求已经处理。");
				map.put("matter", "精准投放");
				map.put("auditStatus", "失败");
				sendMessage.sendAuditSuccessMessage_JZTF(map);//发送文本消息
				model.addAttribute("msg", "审核失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "审核失败");
			return "/error";
		}
	}

	/**
	 * 
	 * 描述: 获取投放的详情
	 * 作者:gyp
	 * @Date	 2017年5月8日
	 */
	@RequestMapping(value = "/getOrderDetail")
	public ModelAndView getOrderDetail(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();
		createParameterMap(request);
		try {
			Order orderInfo = orderSeaService.getOrderInfo(paramsMap.get("orderId").toString());
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(paramsMap);
			Map<String, Object> skillInfo = dealListForOrdDet(orderList);

			String picUrl = skillInfo.get("picUrl").toString();
			String[] picUrls = picUrl.split(",");

			mov.addObject("skillInfo", skillInfo);
			mov.addObject("picUrls", picUrls);
			mov.addObject("orderInfo", orderInfo);
		} catch (Exception e) {
			mov.addObject("msg", "查询精准投放列表异常！");
			mov.setViewName("/error");
			e.printStackTrace();
			return mov;
		}
		mov.setViewName("manager/orderJztf/orderDetail");
		return mov;
	}

	/**
	 * 
	 * 描述:list ---->>>>>>>> map
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public Map<String, Object> dealListForOrdDet(List<OrderDetail> orderList) {
		Map<String, Object> result = new HashMap<String, Object>();
		for (OrderDetail orderDetail : orderList) {
			result.put(orderDetail.getTypeName(), orderDetail.getTypeValue());
		}
		return result;

	}
}
