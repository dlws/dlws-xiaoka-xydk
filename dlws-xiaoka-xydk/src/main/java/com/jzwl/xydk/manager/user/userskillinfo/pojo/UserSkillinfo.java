/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.user.userskillinfo.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class UserSkillinfo extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "UserSkillinfo";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_BASIC_ID = "用户表的id";
	public static final String ALIAS_SKILL_FAT_ID = "技能分类父级的Id";
	public static final String ALIAS_SKILL_SON_ID = "技能分类子级的Id";
	public static final String ALIAS_SKILL_NAME = "技能名称";
	public static final String ALIAS_TEXT_URL = "图文展示的url";
	public static final String ALIAS_OTHER_OPUS = "其他作品的url";
	public static final String ALIAS_VIDEO_URL = "视屏展示url";
	public static final String ALIAS_SKILL_PRICE = "技能价格";
	public static final String ALIAS_SERVICE_TYPE = "服务类型（1：线上服务，2：线下服务）";
	public static final String ALIAS_SKILL_DEPICT = "技能描述";
	public static final String ALIAS_VIEW_NUM = "用户技能的浏览量";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	public static final String ALIAS_IS_DELETE = "是否删除（0：否，1：是）";
	
	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	

	//columns START
    /**
     * id       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 用户表的id       db_column: basicId 
     */ 	
	private java.lang.Long basicId;
    /**
     * 技能分类父级的Id       db_column: skillFatId 
     */ 	
	private java.lang.Long skillFatId;
    /**
     * 技能分类子级的Id       db_column: skillSonId 
     */ 	
	private java.lang.Long skillSonId;
    /**
     * 技能名称       db_column: skillName 
     */ 	
	private java.lang.String skillName;
    /**
     * 图文展示的url       db_column: textURL 
     */ 	
	private java.lang.String textUrl;
    /**
     * 其他作品的url       db_column: otherOpus 
     */ 	
	private java.lang.String otherOpus;
    /**
     * 视屏展示url       db_column: videoURL 
     */ 	
	private java.lang.String videoUrl;
    /**
     * 技能价格       db_column: skillPrice 
     */ 	
	private java.lang.Double skillPrice;
    /**
     * 服务类型（1：线上服务，2：线下服务）       db_column: serviceType 
     */ 	
	private java.lang.Integer serviceType;
    /**
     * 技能描述       db_column: skillDepict 
     */ 	
	private java.lang.String skillDepict;
    /**
     * 用户技能的浏览量       db_column: viewNum 
     */ 	
	private java.lang.Integer viewNum;
    /**
     * 创建时间       db_column: createDate 
     */ 	
	private java.util.Date createDate;
    /**
     * 是否删除（0：否，1：是）       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
	//columns END

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	


	public java.lang.Long getBasicId() {
		return this.basicId;
	}
	
	public void setBasicId(java.lang.Long value) {
		this.basicId = value;
	}
	

	public java.lang.Long getSkillFatId() {
		return this.skillFatId;
	}
	
	public void setSkillFatId(java.lang.Long value) {
		this.skillFatId = value;
	}
	

	public java.lang.Long getSkillSonId() {
		return this.skillSonId;
	}
	
	public void setSkillSonId(java.lang.Long value) {
		this.skillSonId = value;
	}
	

	public java.lang.String getSkillName() {
		return this.skillName;
	}
	
	public void setSkillName(java.lang.String value) {
		this.skillName = value;
	}
	

	public java.lang.String getTextUrl() {
		return this.textUrl;
	}
	
	public void setTextUrl(java.lang.String value) {
		this.textUrl = value;
	}
	

	public java.lang.String getOtherOpus() {
		return this.otherOpus;
	}
	
	public void setOtherOpus(java.lang.String value) {
		this.otherOpus = value;
	}
	

	public java.lang.String getVideoUrl() {
		return this.videoUrl;
	}
	
	public void setVideoUrl(java.lang.String value) {
		this.videoUrl = value;
	}
	

	public java.lang.Double getSkillPrice() {
		return this.skillPrice;
	}
	
	public void setSkillPrice(java.lang.Double value) {
		this.skillPrice = value;
	}
	

	public java.lang.Integer getServiceType() {
		return this.serviceType;
	}
	
	public void setServiceType(java.lang.Integer value) {
		this.serviceType = value;
	}
	

	public java.lang.String getSkillDepict() {
		return this.skillDepict;
	}
	
	public void setSkillDepict(java.lang.String value) {
		this.skillDepict = value;
	}
	

	public java.lang.Integer getViewNum() {
		return this.viewNum;
	}
	
	public void setViewNum(java.lang.Integer value) {
		this.viewNum = value;
	}
	

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}
	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class,FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

}

