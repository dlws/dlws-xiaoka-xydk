package com.jzwl.xydk.manager.dividewithdraw.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.page.PageObject;
import com.jzwl.xydk.manager.dividewithdraw.dao.DivideDrawDao;

@Service("divideDrawService")
public class DivideDrawService {

	@Autowired
	private DivideDrawDao divideDrawDao;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：记录表数据查询
	 * 创建人： sx
	 * 创建时间： 2017年4月14日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public PageObject queryDivideDrawforPage(Map<String, Object> paramsMap) {
		return divideDrawDao.queryDivideDrawforPage(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：插入数据到记录表
	 * 创建人： sx
	 * 创建时间： 2017年5月11日
	 * 标记：
	 * @param recordMap
	 * @version
	 */
	public void addDraw(Map<String, Object> recordMap) {
		divideDrawDao.addDraw(recordMap);
	}
	
}
