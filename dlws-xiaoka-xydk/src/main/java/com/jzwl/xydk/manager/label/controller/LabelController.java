/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.label.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.page.PageObject;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.label.service.LabelService;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;

/**
 * Label
 * Label
 * label
 * <p>Title:LabelController </p>
 * 	<p>Description:Label </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller
@RequestMapping("/label")
public class LabelController  extends BaseController {
	
	@Autowired
	private LabelService labelService;
	
	@Autowired
	private EntertypeService entertypeService;
	/**
	 * 跳转到添加Label的页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddLabel")
	public ModelAndView toAddlabel(HttpServletRequest request, HttpServletResponse response) {
		
		
		ModelAndView mov = new ModelAndView();
		createParameterMap(request);
		List<Map<String,Object>> enterList = labelService.getAllUserSkillFather();
		mov.addObject("enterList", enterList);
		mov.setViewName("/manager/label/add");
		return mov;
	}
	
	/**
	 * 添加Label
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addLabel")
	public ModelAndView addlabel(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new ModelAndView();
		//处理前端传过来的入驻类型
		if(request.getParameterValues("enterId")==null||
				StringUtils.isBlank(request.getParameterValues("enterId").toString())){
			mov.addObject("msg", "请选择入驻类型!");
			mov.setViewName("/error");
		}
		
		String[] enterIds = request.getParameterValues("enterId");
		String enterId = StringUtil.dealArray(enterIds);
		
		paramsMap.put("enterId", enterId);
		createParameterMap(request);
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		try{
			boolean flag = labelService.addLabel(paramsMap);
			if(!flag){
				mov.addObject("msg", "添加Label失败!");
				mov.setViewName("/error");
			}else{
				mov.setViewName("redirect:/label/labelList.html");
			}
		}catch(Exception e){
			e.printStackTrace();
			mov.addObject("msg", "添加Label失败!");
			mov.setViewName("/error");
		}
		
		
		return mov;
	}
	
	/**
	 * labellist
	 */
	@RequestMapping(value = "/labelList")
	public ModelAndView labelList(HttpServletRequest request, HttpServletResponse response) {
	
		ModelAndView mov = new ModelAndView();
		
		createParameterMap(request);
		
		PageObject po = new PageObject();
		
		try{
		
			po = labelService.queryLabelList(paramsMap);
			
			mov.addObject("list", po.getDatasource());
			mov.addObject("totalProperty", po.getTotalCount());
			mov.addObject("po", po);
			mov.addObject("paramsMap", paramsMap);
			mov.setViewName("/manager/label/list");
		}catch(Exception e){
		
			e.printStackTrace();
			mov.addObject("msg", "label查询失败");
			mov.setViewName("/error");
		}
		
		return mov;
	}
	
	
	/**
	 * 跳转到修改label页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toUpdateLabel")
	public ModelAndView toUpdateLabel(HttpServletRequest request, HttpServletResponse response) {
		
		
		ModelAndView mov = new ModelAndView();
		//PageObject po = new PageObject();
		//po = entertypeService.queryEntertypeList(paramsMap);
		List<Map<String,Object>> enterList = labelService.getAllUserSkillFather();
		createParameterMap(request);
		
		try{
			Map<String,Object> label=labelService.getById(paramsMap);
			mov.addObject("enterList", enterList);
			mov.addObject("obj", label);
			mov.setViewName("/manager/label/update");
		}catch(Exception e){
			e.printStackTrace();
			mov.addObject("msg", "label查询失败");
			mov.setViewName("/error");
		}
		
		return mov;
	}
	
	/**
	 * 跳转到详情label页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/detailLabel")
public ModelAndView detailLabel(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new ModelAndView();
		
		createParameterMap(request);
		
		try{
			Map<String,Object> label=labelService.getById(paramsMap);
			mov.addObject("obj", label);
			mov.setViewName("/manager/label/detail");
		}catch(Exception e){
			e.printStackTrace();
			mov.addObject("msg", "label查询失败");
			mov.setViewName("/error");
		}
		
		return mov;
	}
	/**
	 * 修改label
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateLabel")
	public ModelAndView updateLabel(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mov = new ModelAndView(); 
		createParameterMap(request);
		//处理前端传过来的入驻类型
		if(request.getParameterValues("enterId")==null||
				StringUtils.isBlank(request.getParameterValues("enterId").toString())){
			mov.addObject("msg", "请选择入驻类型!");
			mov.setViewName("/error");
		}
		String[] enterIds = request.getParameterValues("enterId");
		String enterId = StringUtil.dealArray(enterIds);
		
		paramsMap.put("enterId", enterId);
		
		try{
		
			boolean flag = labelService.updateLabel(paramsMap);
			
			if(!flag){
				mov.addObject("msg", "修改失败!");
				mov.setViewName("/error");
			}else{
				mov.setViewName("redirect:/label/labelList.html");
			}
		}catch(Exception e){
		
			e.printStackTrace();
			mov.addObject("msg", "修改失败!");
			mov.setViewName("/error");
		}
		
		return mov;
	}
	
	
	/**
	 * 逻辑删除label
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delLabel")
	public ModelAndView delLabel(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new ModelAndView(); 
		
		createParameterMap(request);
		
		try{
		
			boolean flag = labelService.deleteLabel(paramsMap);
			
			if(!flag){
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			}else{
				mov.setViewName("redirect:/label/labelList.html");
			}
		}catch(Exception e){
			
			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");
		
		}
		
		return mov;
	}
	
	
	/**
	 * 物理删除label
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteLabel")
	public ModelAndView deleteLabel(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mov = new ModelAndView(); 
		
		createParameterMap(request);
		
		try{
		
			boolean flag = labelService.deleteLabel(paramsMap);
			
			if(!flag){
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			}else{
				mov.setViewName("redirect:/label/labelList.html");
			}
		}catch(Exception e){
			
			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");
		
		}
		
		return mov;
	}
	
	//auto-code-end
	
	
}

