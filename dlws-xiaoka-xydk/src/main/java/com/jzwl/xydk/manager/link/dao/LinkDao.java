/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.link.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * 
 * 链接业务
 * 
 * @author   JiangShengDong(nikeodong@126.com)
 * @Date	 2017年6月28日
 */
@Repository("linkDao")
public class LinkDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	/**
	 * 列表
	 *
	 * @param map 页面所需信息
	 * @return po 分页对象
	 */
	public PageObject list(Map<String, Object> map) {

		String sql = " select * from `xiaoka-xydk`.manage_link  where isDelete = false and isUse= true order by createDate DESC";

		PageObject po = baseDAO.queryForMPageList(sql, new Object[] {}, map);
		return po;
	}

	/**
	 * 保存
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public boolean save(Map<String, Object> map) {

		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id", Sequence.nextId());
		map.put("linkName", map.get("linkName"));
		map.put("linkValue", map.get("linkValue"));
		map.put("isUse", map.get("isUse"));
		map.put("createDate", date);
		map.put("isDelete", false);

		String sql = "insert into `xiaoka-xydk`.manage_link " + " (id,linkName,linkValue,isUse,createDate,isDelete) "
				+ " values " + " (:id,:linkName,:linkValue,:isUse,:createDate,:isDelete)";

		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 改变
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public boolean change(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.manage_link set " + " isUse = :isUse " + " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 获取id
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public Map<String, Object> getById(Map<String, Object> map) {

		Map<String, Object> resMap = new HashMap<String, Object>();

		String sql = "select *  from `xiaoka-xydk`.manage_link where id = " + map.get("id") + "";

		resMap = baseDAO.queryForMap(sql);

		return resMap;

	}

	/**
	 * 修改
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public boolean updateLink(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.manage_link set " + " linkName=:linkName,linkValue=:linkValue,isUse=:isUse "
				+ " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);
	}

	/**
	 * 删除
	 *
	 * @param map
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public boolean deleteLink(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.manage_link set " + " isDelete = :isDelete " + " where id=:id";

		return baseDAO.executeNamedCommand(sql, map);

	}

}
