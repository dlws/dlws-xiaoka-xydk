package com.jzwl.xydk.manager.label.pojo;

import java.util.List;
import java.util.Map;


/**
 * 用来存放 技能类型对应多个标签
 * @author Administrator
 *
 */

public class SkillMapLabel {
	
	private Map<String,Object> skill;
	
	private List<Label> label;
	
	

	public SkillMapLabel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SkillMapLabel(Map<String, Object> skill, List<Label> label) {
		super();
		this.skill = skill;
		this.label = label;
	}

	public Map<String, Object> getSkill() {
		return skill;
	}

	public void setSkill(Map<String, Object> skill) {
		this.skill = skill;
	}

	public List<Label> getLabel() {
		return label;
	}

	public void setLabel(List<Label> label) {
		this.label = label;
	}
	

}
