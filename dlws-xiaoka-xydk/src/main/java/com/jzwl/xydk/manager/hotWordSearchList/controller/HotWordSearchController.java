/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.manager.hotWordSearchList.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.hotWordSearchList.service.HotWordSearchService;

/**
 * HotWordSearch
 * HotWordSearch
 * hotWordSearch
 * <p>Title:HotWordSearchController </p>
 * 	<p>Description:HotWordSearch </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller
@RequestMapping("/hotWordSearch")
public class HotWordSearchController extends BaseController {

	@Autowired
	private HotWordSearchService hotWordSearchService;

	/**
	 * 跳转到添加HotWordSearch的页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddHotWordSearch")
	public ModelAndView toAddhotWordSearch(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);
		List<Map<String, Object>> hotWordType = hotWordSearchService.hotWords();
		mov.addObject("hotWordType", hotWordType);
		mov.setViewName("/manager/hotWordSearch/add");
		return mov;
	}

	/**
	 * 添加HotWordSearch
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addHotWordSearch")
	public ModelAndView addhotWordSearch(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			boolean flag = hotWordSearchService.addHotWordSearch(paramsMap);
			if (!flag) {
				mov.addObject("msg", "添加HotWordSearch失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/hotWordSearch/hotWordSearchList.html");
			}
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "添加HotWordSearch失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * hotWordSearchlist
	 */
	@RequestMapping(value = "/hotWordSearchList")
	public ModelAndView hotWordSearchList(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		PageObject po = new PageObject();

		try {

			po = hotWordSearchService.queryHotWordSearchList(paramsMap);

			mov.addObject("list", po.getDatasource());
			mov.addObject("totalProperty", po.getTotalCount());
			mov.addObject("po", po);
			mov.addObject("paramsMap", paramsMap);
			mov.setViewName("/manager/hotWordSearch/list");
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "hotWordSearch查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 跳转到修改hotWordSearch页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toUpdateHotWordSearch")
	public ModelAndView toUpdateHotWordSearch(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {
			Map<String, Object> hotWordSearch = hotWordSearchService.getById(paramsMap);
			mov.addObject("obj", hotWordSearch);
			List<Map<String, Object>> hotWordType = hotWordSearchService.hotWords();
			mov.addObject("hotWordType", hotWordType);
			mov.setViewName("/manager/hotWordSearch/update");
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "hotWordSearch查询失败");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 修改hotWordSearch
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateHotWordSearch")
	public ModelAndView updateHotWordSearch(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = hotWordSearchService.updateHotWordSearch(paramsMap);

			if (!flag) {
				mov.addObject("msg", "修改失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/hotWordSearch/hotWordSearchList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "修改失败!");
			mov.setViewName("/error");
		}

		return mov;
	}

	/**
	 * 逻辑删除hotWordSearch
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delHotWordSearch")
	public ModelAndView delHotWordSearch(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = hotWordSearchService.deleteHotWordSearch(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/hotWordSearch/hotWordSearchList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	/**
	 * 物理删除hotWordSearch
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteHotWordSearch")
	public ModelAndView deleteHotWordSearch(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		try {

			boolean flag = hotWordSearchService.deleteHotWordSearch(paramsMap);

			if (!flag) {
				mov.addObject("msg", "删除失败!");
				mov.setViewName("/error");
			} else {
				mov.setViewName("redirect:/hotWordSearch/hotWordSearchList.html");
			}
		} catch (Exception e) {

			e.printStackTrace();
			mov.addObject("msg", "删除失败!");
			mov.setViewName("/error");

		}

		return mov;
	}

	//auto-code-end

}
