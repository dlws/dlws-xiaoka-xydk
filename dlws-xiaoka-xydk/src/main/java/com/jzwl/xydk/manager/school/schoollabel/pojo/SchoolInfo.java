/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.school.schoollabel.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class SchoolInfo extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "TgSchoolInfo";
	public static final String ALIAS_ID = "id(主键)";
	public static final String ALIAS_CITY_ID = "所属城市ID";
	public static final String ALIAS_SCHOOL_NAME = "名称";
	public static final String ALIAS_SCHOOL_NUMBER = "编号";
	public static final String ALIAS_SCHOOL_RESP_PEOPLE_ID = "负责人(Id)";
	public static final String ALIAS_PHONE = "学校电话";
	public static final String ALIAS_ADDRESS = "学校地址";
	public static final String ALIAS_LONGITUDE = "学校经度";
	public static final String ALIAS_LATITUDE = "学校纬度";
	public static final String ALIAS_REMARK = "备注（预留）";
	public static final String ALIAS_IS_DELETE = "是否删除（0：未删除，1：已删除）";
	public static final String ALIAS_PIC_URL = "学校的图片";
	public static final String ALIAS_DEPARTMENT_ID = "部门Id";
	
	//date formats
	

	//columns START
    /**
     * id(主键)       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 所属城市ID       db_column: cityId 
     */ 	
	private java.lang.Long cityId;
    /**
     * 名称       db_column: schoolName 
     */ 	
	private java.lang.String schoolName;
    /**
     * 编号       db_column: schoolNumber 
     */ 	
	private java.lang.String schoolNumber;
    /**
     * 负责人(Id)       db_column: schoolRespPeopleId 
     */ 	
	private java.lang.String schoolRespPeopleId;
    /**
     * 学校电话       db_column: phone 
     */ 	
	private java.lang.String phone;
    /**
     * 学校地址       db_column: address 
     */ 	
	private java.lang.String address;
    /**
     * 学校经度       db_column: longitude 
     */ 	
	private java.lang.Double longitude;
    /**
     * 学校纬度       db_column: latitude 
     */ 	
	private java.lang.Double latitude;
    /**
     * 备注（预留）       db_column: remark 
     */ 	
	private java.lang.String remark;
    /**
     * 是否删除（0：未删除，1：已删除）       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
    /**
     * 学校的图片       db_column: picUrl 
     */ 	
	private java.lang.String picUrl;
    /**
     * 部门Id       db_column: departmentId 
     */ 	
	private java.lang.String departmentId;
	
	private Double distance;//跟客户的距离
	
	private String dis;//距离，智能单位
	

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	
	
	
	
	
	
	
	
	
	
	
	


	public String getDis() {
		return dis;
	}

	public void setDis(String dis) {
		this.dis = dis;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public java.lang.Long getCityId() {
		return this.cityId;
	}
	
	public void setCityId(java.lang.Long value) {
		this.cityId = value;
	}
	

	public java.lang.String getSchoolName() {
		return this.schoolName;
	}
	
	public void setSchoolName(java.lang.String value) {
		this.schoolName = value;
	}
	

	public java.lang.String getSchoolNumber() {
		return this.schoolNumber;
	}
	
	public void setSchoolNumber(java.lang.String value) {
		this.schoolNumber = value;
	}
	

	public java.lang.String getSchoolRespPeopleId() {
		return this.schoolRespPeopleId;
	}
	
	public void setSchoolRespPeopleId(java.lang.String value) {
		this.schoolRespPeopleId = value;
	}
	

	public java.lang.String getPhone() {
		return this.phone;
	}
	
	public void setPhone(java.lang.String value) {
		this.phone = value;
	}
	

	public java.lang.String getAddress() {
		return this.address;
	}
	
	public void setAddress(java.lang.String value) {
		this.address = value;
	}
	

	public java.lang.Double getLongitude() {
		return this.longitude;
	}
	
	public void setLongitude(java.lang.Double value) {
		this.longitude = value;
	}
	

	public java.lang.Double getLatitude() {
		return this.latitude;
	}
	
	public void setLatitude(java.lang.Double value) {
		this.latitude = value;
	}
	

	public java.lang.String getRemark() {
		return this.remark;
	}
	
	public void setRemark(java.lang.String value) {
		this.remark = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

	public java.lang.String getPicUrl() {
		return this.picUrl;
	}
	
	public void setPicUrl(java.lang.String value) {
		this.picUrl = value;
	}
	

	public java.lang.String getDepartmentId() {
		return this.departmentId;
	}
	
	public void setDepartmentId(java.lang.String value) {
		this.departmentId = value;
	}
	

}

