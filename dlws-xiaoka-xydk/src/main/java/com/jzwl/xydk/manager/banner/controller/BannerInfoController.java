/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.manager.banner.controller;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.upload.MultipartUploadSample;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.banner.service.BannerInfoService;

/**
 * BannerInfo
 * BannerInfo
 * bannerInfo
 * <p>Title:BannerInfoController </p>
 * 	<p>Description:BannerInfo </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller
@RequestMapping("/bannerInfo")
public class BannerInfoController extends BaseController {

	@Autowired
	private BannerInfoService bannerInfoService;

	@Autowired
	private Constants constants;

	/**
	 * 轮播图列表
	 * v2BannerInfolist
	 */
	@RequestMapping(value = "/bannerInfoList")
	public ModelAndView bannerInfoList(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);

		PageObject po = new PageObject();
		try {

			po = bannerInfoService.queryBannerInfoList(paramsMap);

			mov.addObject("po", po);

		} catch (Exception e) {
			mov.addObject("msg", "查询轮播图列表异常！");
			mov.setViewName("/error");
			e.printStackTrace();
			return mov;
		}
		mov.setViewName("manager/banner/bannerList");
		return mov;
	}

	/**
	 * 跳转到添加BannerInfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAdd")
	public String toAddBannerInfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		//获取字典表里的轮播图类型，从xiaoka中进行获取
		List<Map<String, Object>> bannerTypes = bannerInfoService.bannerTypes();
		model.addAttribute("bannerTypes", bannerTypes);
		return "/manager/banner/bannerAdd";

	}

	/**
	 * 添加BannerInfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add")
	public String addBannerInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			//获取登录用户的信息
			//			String createUser = (String) request.getSession().getAttribute("userName");

			//			paramsMap.put("createUser", createUser);

			boolean flag = bannerInfoService.addBannerInfo(paramsMap);
			if (flag) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/bannerInfo/bannerInfoList.html?start=0";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}

	}

	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdateBannerInfo")
	public String toUpdateBannerInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			Map<String, Object> objMap = bannerInfoService.getById(paramsMap);
			//获取字典表里的轮播图类型，从xiaoka中进行获取
			List<Map<String, Object>> bannerTypes = bannerInfoService.bannerTypes();
			model.addAttribute("bannerTypes", bannerTypes);
			model.addAttribute("objMap", objMap);
			return "/manager/banner/bannerEdit";

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}

	}

	/**
	 * 修改bannerInfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateBannerInfo")
	public String updateBannerInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			boolean flag = bannerInfoService.updateBannerInfo(paramsMap);

			if (flag) {
				return "redirect:/bannerInfo/bannerInfoList.html?start=0";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	/**
	 * 删除bannerInfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteBannerInfo")
	public String deleteBannerInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);

			boolean flag = bannerInfoService.deleteBannerInfo(paramsMap);

			if (flag) {
				return "redirect:/bannerInfo/bannerInfoList.html?start=0";
			} else {
				model.addAttribute("msg", "删除失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return "/error";
		}

	}

	@RequestMapping(value = "/isUse")
	public String isUse(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			boolean flag = bannerInfoService.isUse(paramsMap);

			if (flag) {
				return "redirect:/bannerInfo/bannerInfoList.html?start=0";
			} else {
				model.addAttribute("msg", "保存失败");
				return "/error";
			}
		} catch (Exception e) {

			e.printStackTrace();
			model.addAttribute("msg", "保存失败");
			return "/error";
		}
	}

	//auto-code-end

	//上传图片
	@RequestMapping(value = "picture", method = RequestMethod.POST)
	public @ResponseBody Map uploadLogo(@RequestParam(required = false) MultipartFile pic, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Map map = new HashMap();
		String filename = URLEncoder.encode(pic.getOriginalFilename(), "utf-8");
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String name = df.format(new Date());
		Random r = new Random();
		for (int i = 0; i < 3; i++) {
			name += r.nextInt(10);
		}
		String savename = name + filename;

		boolean b = MultipartUploadSample.uploadOssImg(savename, pic);

		String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
		String picUrl = beforUrl + savename;
		map.put("picUrl", picUrl);// picUrl 传阿里云服务器上的全路径
		map.put("key", savename);// key值
		map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);

		return map;
	}
	
	
	//上传图片
		@RequestMapping(value = "picture2", method = RequestMethod.POST)
		public @ResponseBody Map uploadLogo2(@RequestParam(required = false) MultipartFile pic, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			Map map = new HashMap();
			String filename = URLEncoder.encode(pic.getOriginalFilename(), "utf-8");
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String name = df.format(new Date());
			Random r = new Random();
			for (int i = 0; i < 3; i++) {
				name += r.nextInt(10);
			}
			String savename = name + filename;

			boolean b = MultipartUploadSample.uploadOssImg(savename, pic);

			String beforUrl = constants.getXiaoka_bbs_oosGetImg_beforeUrl();
			String picUrl = beforUrl + savename;
			map.put("picUrl", picUrl);// picUrl 传阿里云服务器上的全路径
			map.put("key", savename);// key值
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);

			return map;
		}

	/**
	 * 判断轮播图名称是否正确
	 */
	@ResponseBody
	@RequestMapping(value = "/judgeRepeat", method = RequestMethod.POST)
	public Map<String, Object> judgeRepeat(HttpServletRequest request, HttpServletResponse response) {
		createParameterMap(request);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			Map<String, Object> objMap = bannerInfoService.getByName(paramsMap);

			if (objMap.isEmpty()) {
				resultMap.put("isEmpty", "yes");
			} else {
				resultMap.put("isEmpty", "no");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultMap.put("info", "数据错误");
			resultMap.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
		}

		return resultMap;
	}

}
