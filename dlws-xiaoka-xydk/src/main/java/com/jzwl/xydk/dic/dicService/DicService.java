package com.jzwl.xydk.dic.dicService;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.dic.dicDao.DicDao;
@Service("dicService")
public class DicService {

	@Autowired
	private  DicDao dicDao;

	/**
	 * 
	 * 描述:获取字典值
	 * 作者:gyp
	 * @Date	 2017年6月23日
	 */
	public Map<String,Object> getDicDataById(Map<String, Object> map){
		return dicDao.getDicDataById(map);
	}
}
