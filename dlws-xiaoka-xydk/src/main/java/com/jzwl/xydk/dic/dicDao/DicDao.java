package com.jzwl.xydk.dic.dicDao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.system.base.dao.BaseDAO;

@Repository("dicDao")
public class DicDao {

	@Autowired
	private BaseDAO baseDao;

	public Map<String,Object> getDicDataById(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.* from `xiaoka`.v2_dic_data t where 1=1 ");
		
		if(null !=map.get("typeId") && StringUtils.isNotEmpty(map.get("typeId").toString())){
			sb.append(" and t.id = " + map.get("typeId") +" ");
	  	}
		
		return baseDao.queryForMap(sb.toString());
	}
}
