package com.jzwl.xydk.sendmessage;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.common.utils.HttpClientUtil;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.Order;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;

@Service("sendMessage")
public class SendMessageService {

	@Autowired
	private Constants constants;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private OrderSeaService orderSeaService;

	
	

	/**
	 * 
	 * 描述:审核发送模板信息 作者:gyp
	 * 
	 * @Date 2017年4月14日
	 */
	public void sendAuditSuccessMessage(Map<String, Object> map) {

		try {
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			templateMessage.setToUser(map.get("openId").toString());
			// templateMessage.setToUser("o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
			//templateMessage.setTemplateId("SiZUQrk4qT0A87hfku2nQHZcMDlTUFCq5DyQShh-4To");
			templateMessage.setUrl(constants.getTemplateMessage_to_my_url());
			templateMessage.getDatas().add(
					new WxMpTemplateData("first", map.get("auditType")
							.toString()));
			templateMessage.getDatas().add(
					new WxMpTemplateData("keyword1", map.get("userName")
							.toString()));
			templateMessage.getDatas().add(
					new WxMpTemplateData("keyword2", map.get("phoneNumber")
							.toString()));
			templateMessage.getDatas().add(
					new WxMpTemplateData("keyword3", DateUtil
							.formatDate(new Date())));

			detail.append("如有任何问题请联系平台客服。");
			templateMessage.getDatas().add(
					new WxMpTemplateData("remark", detail.toString()));
			// wxMpService.templateSend(templateMessage);
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants
					.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,
					"templateMessage", "audit", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * 描述:发票审核模板
	 * 作者:gyp
	 * @Date	 2017年6月26日
	 */
	public void sendAuditSuccessMessage_invoice(Map<String, Object> map) {

		try {
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			templateMessage.setToUser(map.get("openId").toString());
			// templateMessage.setToUser("o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
			//templateMessage.setTemplateId("SiZUQrk4qT0A87hfku2nQHZcMDlTUFCq5DyQShh-4To");
			templateMessage.setUrl(constants.getTemplateMessage_to_my_url());
			templateMessage.getDatas().add(
					new WxMpTemplateData("first", map.get("auditType")
							.toString()));
			templateMessage.getDatas().add(
					new WxMpTemplateData("keyword1", map.get("recipientName")
							.toString()));
			templateMessage.getDatas().add(
					new WxMpTemplateData("keyword2", map.get("recipientPhone")
							.toString()));
			templateMessage.getDatas().add(
					new WxMpTemplateData("keyword3", DateUtil
							.formatDate(new Date())));

			templateMessage.getDatas().add(
					new WxMpTemplateData("remark", map.get("remark").toString()));
			// wxMpService.templateSend(templateMessage);
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants
					.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,
					"templateMessage", "audit", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void sendAuditSuccessMessage_JZTF(Map<String, Object> map) {

		try {
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			templateMessage.setToUser(map.get("openId").toString());
			templateMessage.setUrl(constants.getTemplateMessage_to_my_url());
			templateMessage.getDatas().add(new WxMpTemplateData("first", map.get("auditType").toString()));
			
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("matter").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", map.get("auditStatus").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword3", DateUtil.formatDate(new Date())));
			detail.append("精准投放，效果可控。");
			templateMessage.getDatas().add(
					new WxMpTemplateData("remark", detail.toString()));
			// wxMpService.templateSend(templateMessage);
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants
					.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,
					"templateMessage", "auditJZTF", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * 
	 * 描述:支付成功发送模板通知,订单的（orderId）和订单的金额（payMoney）
	 * 作者:gyp
	 * @Date	 2017年4月25日
	 */
	public void payMoneySuccess(String orderId,String openId,String payMoney) {
		try {
			Date date = new Date();
			System.out.println("========================支付成功发送模板通知==============================");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("isDelete", 0);
			map.put("orderId", orderId);
			map.put("openId", openId);
			map.put("payMoney", payMoney);
			
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("8PpiLjL67iSRVMqBG4rKJOI5JPbqnxGrFR7QlMIfMBk");
			templateMessage.setUrl(constants.getTemplateMessage_to_myOrder_url());
			templateMessage.getDatas().add(new WxMpTemplateData("first", "您的订单已经支付成功"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword3", map.get("payMoney").toString()));
			detail.append("如有问题，请联系我们。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "paySuccess", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 创建人： sx
	 * 创建时间： 2017年5月8日
	 * 标记：
	 * @param orderId
	 * @param openId
	 * @param payMoney
	 * @version
	 */
	public void payMoneySuccessByPid(String pid,String openId,String payMoney) {
		try {
			Date date = new Date();
			System.out.println("========================支付成功发送模板通知==============================");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("isDelete", 0);
			map.put("pid", pid);
			map.put("openId", openId);
			map.put("payMoney", payMoney);
			
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetailByPid(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("8PpiLjL67iSRVMqBG4rKJOI5JPbqnxGrFR7QlMIfMBk");
			templateMessage.setUrl(constants.getTemplateMessage_to_myOrder_url());
			templateMessage.getDatas().add(new WxMpTemplateData("first", "您的订单已经支付成功"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword3", map.get("payMoney").toString()));
			detail.append("如有问题，请联系我们。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "paySuccess", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 描述:群发模板信息 作者:gyp
	 * 
	 * @Date 2017年4月14日
	 */
	public void sendMassTemplateMessage(Map<String, Object> map) {

		try {

			List<Map<String, Object>> list = skillUserService
					.getUserBasicInfoList();

			for (Map<String, Object> m : list) {
				StringBuffer detail = new StringBuffer();
				WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
				templateMessage.setToUser(m.get("openId").toString());
				//templateMessage.setToUser("o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
				//templateMessage.setTemplateId("zcAoH76tPNWPutlqOKGbPqTVnls9sOv-qrDbI1pn9_U");
				templateMessage.setUrl(constants
						.getTemplateMessage_to_chatList_url());
				templateMessage.getDatas().add(
						new WxMpTemplateData("first", map.get("title")
								.toString()));
				templateMessage.getDatas().add(
						new WxMpTemplateData("keyword1", map.get("content")
								.toString()));
				templateMessage.getDatas().add(
						new WxMpTemplateData("keyword2", DateUtil
								.formatDate(new Date())));

				detail.append(map.get("remark"));
				templateMessage.getDatas().add(
						new WxMpTemplateData("remark", detail.toString()));
				// wxMpService.templateSend(templateMessage);
				Gson gson = new Gson();
				String json = gson.toJson(templateMessage);
				String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
				HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "GroupSending", "xydkSystemAuth");
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:支付成功后卖家订单提醒。
	 * 作者:gyp
	 * @Date	 2017年5月3日
	 */
	public void remindOrderToSellerOrderSendMessage(String orderId) {
		
		
		try {
			//检查当前用户在订单表
			Order orderInfo = orderSeaService.getOrderInfo(orderId);
			Map<String,Object> typeValue = orderSeaService.getOrderDetailInfoByskillName(orderId);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(orderInfo.getSellerOpenId());
			//templateMessage.setTemplateId("8GSEEPUlQNZx18NkD-vo3qG-LS8T_MA1Wqz7P65RcWU");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+orderId);
			templateMessage.getDatas().add(new WxMpTemplateData("first", "您有新的订单。'"+typeValue.get("typeValue")+"'"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", orderId));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(new Date())));
			detail.append("您的服务被下单了，点击查看。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "remindOrder", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:取消订单，没有付款时对订单进行取消
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 * map ---->> orderId  openId  要含有这两个值
	 */
	public void cancelOrderNotPayOrderSendMessage(Map<String, Object> map) {
		
		
		try {
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("dp-bKZP3ShQfVeB0e7-CbeTnW4iXjZ9UdOKPbRpvHd0");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first", "您的"+map.get("orderId")+"订单,已手动动取消"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", skillInfo.get("skillName").toString()));
			detail.append("您的订单已经被取消。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "cancelOrder", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * 描述:申请退款，对卖家进行信息发送Buyers
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void  applyRefundToSeller(Map<String, Object> map) {
		try {
			
			//检查当前用户在订单表
			Order orderInfo = orderSeaService.getOrderInfo(map.get("orderId").toString());
			
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(orderInfo.getSellerOpenId());
			//templateMessage.setTemplateId("VNPU8naJ3Z13Z5q5o_qWFPdCHCFuricjCEhFULISRRU");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first", map.get("orderId").toString()+"订单申请退款。"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1",DateUtil.formatDate(new Date())));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", skillInfo.get("skillName").toString()));
			detail.append("买家已申请退款,点击进行操作");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "applyRefund", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * 描述:申请退款，对买家进行信息发送
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void  applyRefundToBuyers(Map<String, Object> map) {
		try {
			
			//检查当前用户在订单表
			Order orderInfo = orderSeaService.getOrderInfo(map.get("orderId").toString());
			
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(orderInfo.getOpenId());
			//templateMessage.setTemplateId("VNPU8naJ3Z13Z5q5o_qWFPdCHCFuricjCEhFULISRRU");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first", map.get("orderId").toString()+"订单已申请退款。"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1",DateUtil.formatDate(new Date())));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", skillInfo.get("skillName").toString()));
			detail.append("买家已申请退款,等待卖家确认");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "applyRefund", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:卖家同意退款，对买家进行信息发送
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void  agreeRefundToBuyers(Map<String, Object> map) {
		try {
			
			//检查当前用户在订单表
			Order orderInfo = orderSeaService.getOrderInfo(map.get("orderId").toString());
			
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(orderInfo.getOpenId());
			//templateMessage.setTemplateId("VNPU8naJ3Z13Z5q5o_qWFPdCHCFuricjCEhFULISRRU");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first", map.get("orderId").toString()+"订单已卖家同意退款。"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1",DateUtil.formatDate(new Date())));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", skillInfo.get("skillName").toString()));
			detail.append("该退款申请卖家已拒绝,请等待平台联系退款事宜。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "applyRefund", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:卖家拒绝退款，对买家进行信息发送
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void  refuseRefundToBuyers(Map<String, Object> map) {
		try {
			
			//检查当前用户在订单表
			Order orderInfo = orderSeaService.getOrderInfo(map.get("orderId").toString());
			
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(orderInfo.getOpenId());
			//templateMessage.setTemplateId("VNPU8naJ3Z13Z5q5o_qWFPdCHCFuricjCEhFULISRRU");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first", map.get("orderId").toString()+"订单已卖家拒绝退款。"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1",DateUtil.formatDate(new Date())));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", skillInfo.get("skillName").toString()));
			detail.append("该退款申请卖家已拒绝,请等待卖家服务完成");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "applyRefund", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:超时没付款取消订单发送模板通知
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public void timeOutNotPayOrderSendMessageInfo(Map<String, Object> map) {

		try {
			
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("dp-bKZP3ShQfVeB0e7-CbeTnW4iXjZ9UdOKPbRpvHd0");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId"));
			templateMessage.getDatas().add(new WxMpTemplateData("first", skillInfo.get("skillName")+"的订单,系统已自动取消"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", skillInfo.get("skillName").toString()));
			detail.append("您的订单超过1小时未支付，系统已自动取消。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "timeOut", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:超时没付款取消订单发送模板通知
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public void timeOutNotPayOrderSendMessageInfo_fabu(Map<String, Object> map) {

		try {
			
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("dp-bKZP3ShQfVeB0e7-CbeTnW4iXjZ9UdOKPbRpvHd0");
			templateMessage.setUrl(constants.getTemplateMessage_to_my_url());
			templateMessage.getDatas().add(new WxMpTemplateData("first", skillInfo.get("skillName")+"的订单,系统已自动取消"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", skillInfo.get("skillName").toString()));
			detail.append("您的订单超过1小时未支付，系统已自动取消。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "timeOut", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * 描述:卖家接单，发送的模板信息
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void acceptOrder(Map<String, Object> map) {
		
		Date date = new Date();

		try {
			
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("imgZFqQ-EftWkol-48R9uHU_ayeEig_Cg6DdqVDiHBk");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first", "'"+skillInfo.get("skillName").toString()+"'的卖家已经接单。"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			detail.append("如有问题请联系客服。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "acceptOrder", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * 描述:卖家拒绝接单的   提示信息
	 * 作者:gyp
	 * @Date	2017年4月24日
	 */
	public void refuseAcceptOrder(Map<String, Object> map) {
		
		Date date = new Date();

		try {
			Order orderInfo = orderSeaService.getOrderInfo(map.get("orderId").toString());
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(orderInfo.getOpenId());
			//templateMessage.setTemplateId("imgZFqQ-EftWkol-48R9uHU_ayeEig_Cg6DdqVDiHBk");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first", "'"+skillInfo.get("skillName").toString()+"'的卖家拒绝接单。"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			detail.append("如有问题请联系客服。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "acceptOrder", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * 描述:订单提交的模板通知
	 * 作者:gyp
	 * @Date	 2017年4月25日
	 */
	public void submissionOrder(String orderId,String openId) {
		
		Date date = new Date();
		try {
			//System.out.println("========================订单提交的模板通知==============================");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("isDelete", 0);
			map.put("orderId", orderId);
			map.put("openId", openId);
			
			
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("y781-o2rXcnM5RExWjeZ-fZ3nrnF9XKOhEyDuOLv8VU");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+orderId);
			templateMessage.getDatas().add(new WxMpTemplateData("first", skillInfo.get("skillName")+"的订单已经提交"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			detail.append("如有问题请联系客服。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "subOrder", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * 描述:精准咨询到期订单发送模板消息
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void jzzxExpiresOrder(Map<String, Object> map) {
		Date date = new Date();
		try {
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("nr9XLRM-XSatIo9TiNDF11fo6iWBcymXAFcOxsoSOtA");
			templateMessage.setUrl(constants.getTemplateMessage_to_myOrder_url());
			templateMessage.getDatas().add(new WxMpTemplateData("first","服务到期提醒"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", skillInfo.get("skillName").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", "精准咨询"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword3", DateUtil.formatDate(date)));
			detail.append("如有问题请联系客服。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "jzzxExpires", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:系统自动确认订单   bTF-fCvOMgWZbvDOqf38EwJTinxzI5VTWtX67rCxHBw
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void orderSuccess(Map<String, Object> map) {
		Date date = new Date();
		try {
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  dealListForOrdDet(orderList);
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("Vm0l1JLy0xO5Vd0fx3ywD7fNQ5y58ggIH32eCcvUXQc");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first","系统自动确认收货"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", map.get("payMoney").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword3", DateUtil.formatDate(date)));
			detail.append("如有问题请联系客服。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "orderSuccess", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:服务完成   向卖家发送信息   bTF-fCvOMgWZbvDOqf38EwJTinxzI5VTWtX67rCxHBw
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 * 备注:校咖汇2.4版本中，该模板消息已经替换成为了反馈的模板消息
	 */
	public void skillSuccess(Map<String, Object> map) {
		try {
			
			Order orderInfo = orderSeaService.getOrderInfo(map.get("orderId").toString());
			
			//检查当前用户在订单表
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			
			templateMessage.setToUser(orderInfo.getOpenId());
			//templateMessage.setTemplateId("bTF-fCvOMgWZbvDOqf38EwJTinxzI5VTWtX67rCxHBw");
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId").toString());
			templateMessage.getDatas().add(new WxMpTemplateData("first","服务已完成，请确认订单"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", map.get("orderId").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", "待确认"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword3", "服务已完成，请即时确认订单"));
			detail.append("如有问题请联系客服。");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "skillSuccess", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:开具发票支付成功
	 * 作者:gyp
	 * @Date	 2017年6月8日
	 */
	public void invoicePayMoneySuccess(String orderId,String openId,String payMoney) {
		try {
			Date date = new Date();
			
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			templateMessage.setToUser(openId);
			//templateMessage.setTemplateId("8PpiLjL67iSRVMqBG4rKJOI5JPbqnxGrFR7QlMIfMBk");
			templateMessage.setUrl(constants.getTemplateMessage_to_myOrder_url());
			templateMessage.getDatas().add(new WxMpTemplateData("first", "发票支付成功提示"));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1", orderId));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword3", payMoney));
			detail.append("您的发票已经开具成功,我们将以快递的形式");
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "paySuccess", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:管理员更换的模板信息
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void  numberChange(Map<String, Object> map) {
		try {
			System.out.println("发送模板细信息============================");
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			Date date = new Date();
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("VNPU8naJ3Z13Z5q5o_qWFPdCHCFuricjCEhFULISRRU");
			templateMessage.setUrl(constants.getTemplateMessage_to_numberChange_url()+"?recordId="+map.get("recordId"));
			templateMessage.getDatas().add(new WxMpTemplateData("first", map.get("first").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1",map.get("keyword1").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			detail.append(map.get("detail"));
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "numberChange", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	/**
	 * 描述:卖家提交反馈
	 * 作者:gyp
	 * @Date	 2017年6月21日
	 * 
	 * isbuyer   是否是向卖家发送
	 */
	public void submitFeedbackInfo(Map<String,Object> map,boolean isbuyer){
		
		try {
			System.out.println("=====================提交用户反馈信息发送模板信息========================");
			
			Order orderInfo = orderSeaService.getOrderInfo(map.get("orderId").toString());
			
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			Date date = new Date();
			if(isbuyer){
				templateMessage.setToUser(orderInfo.getOpenId());
			}else{
				templateMessage.setToUser(orderInfo.getSellerOpenId());
			}
			
			templateMessage.setUrl(constants.getTemplateMessage_to_OrderInfo_url()+"?orderId="+map.get("orderId"));
			templateMessage.getDatas().add(new WxMpTemplateData("first", map.get("first").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1",map.get("keyword1").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			detail.append(map.get("detail"));
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "feedback", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	/**
	 * 描述:外交官更换发送模板消息
	 * 作者:gyp
	 * @Date	 2017年6月22日
	 */
	public void  diplomatChange(Map<String, Object> map) {
		try {
			System.out.println("************************管理员更换发送模板消息*****************************");
			StringBuffer detail = new StringBuffer();
			WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
			Date date = new Date();
			templateMessage.setToUser(map.get("openId").toString());
			//templateMessage.setTemplateId("VNPU8naJ3Z13Z5q5o_qWFPdCHCFuricjCEhFULISRRU");
			templateMessage.setUrl(constants.getTemplateMessage_to_diplomatChange_url()+"?recordId="+map.get("recordId"));
			templateMessage.getDatas().add(new WxMpTemplateData("first", map.get("first").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword1",map.get("keyword1").toString()));
			templateMessage.getDatas().add(new WxMpTemplateData("keyword2", DateUtil.formatDate(date)));
			detail.append(map.get("detail"));
			templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
			Gson gson = new Gson();
			String json = gson.toJson(templateMessage);
			String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
			HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "numberChange", "xydkSystemAuth");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * 描述:精准投放发送模板信息，点击进入投放内容
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public void jztfSendTextMessage(Map<String, Object> map){
		
		try {
			String openid = map.get("sellerOpenId").toString();
			
			String href = constants.getTextMessage_jztf_putContent_url();
			href += "?orderId=" + map.get("orderId").toString();
			String index = "您有一条投放消息！\n\r<a href=\"" + href
					+ "\">点击查看投放内容</a>  ";

			String urlString = constants.getTextMessage_to_sendText();
			HttpClientUtil.requestByPostMethodSendOpenId(urlString, openid,index);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("orderId", "1493025499436006");
		map.put("payMoney", "400");
		map.put("openId", "o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
	//	payMoneySuccess(map);
	}
	
	/**
	 * 
	 * 描述:list ---->>>>>>>> map
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public Map<String,Object> dealListForOrdDet(List<OrderDetail> orderList){
		Map<String,Object> result = new HashMap<String, Object>();
		for (OrderDetail orderDetail : orderList) {
			 result.put(orderDetail.getTypeName(), orderDetail.getTypeValue());
		}
		return result;	
			
	}
}
