package com.jzwl.xydk.wap.xkhV2.sqdv.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkhV2.highschoolskill.service.HighSchoolSkillService;
import com.jzwl.xydk.wap.xkhV2.sqdv.service.ComunityBigvService;

/**
 * 
 * @author Administrator
 * 社群大V的Controller
 * 2017年3月14日10:07
 * dxf
 */
@Controller("comunityBigV")
@RequestMapping("/comunityBigV")
public class ComunityBigVController extends BaseWeixinController {

	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private HomeService homeService;
	@Autowired
	private Constants constants;
	@Autowired
	private SkillUserService skillUserService;

	@Autowired
	private ComunityBigvService comunityBigvService;

	@Autowired
	private PreferredServiceService preferredServiceService;
	@Autowired
	private HighSchoolSkillService highSchoolSkillService;

	/**
	 * 个人空间一级页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getPersonUrl")
	public String getUserInfo(HttpServletRequest request, HttpServletResponse response) {
		createParameterMap(request);
		try {
			// 判断入驻类型
			Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
			String typeValue = String.valueOf(setypeMap.get("typeValue"));
			if (typeValue == null || "null".equals(typeValue)) {
				return "redirect:/highSchoolSkill/getBasicInfo.html?id=" + paramsMap.get("id") + "&enterId=" + setypeMap.get("id");// 高校社团
			}
			if (GlobalConstant.ENTER_TYPE_SQDV.equals(typeValue)) {
				return "redirect:/comunityBigV/comunityBigVdetail.html?enterId=" + paramsMap.get("enterId") + "&id=" + paramsMap.get("id");
			} else if (GlobalConstant.ENTER_TYPE_CDZY.equals(typeValue)) {
				return "redirect:/fieldEnter/toShowPersonSpace.html?id=" + paramsMap.get("id");// 场地入驻
			} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(typeValue)) {
				return "redirect:/schoolBusi/toShowPerSpaceSchool.html?id=" + paramsMap.get("id");// 校内商家
			} else if (GlobalConstant.ENTER_TYPE_GXST.equals(typeValue)) {
				return "redirect:/highSchoolSkill/getBasicInfo.html?id=" + paramsMap.get("id") + "&enterId=" + setypeMap.get("id");// 高校社团
			} else if (GlobalConstant.ENTER_TYPE_JNDR.equals(typeValue)) {
				return "redirect:/highSchoolSkill/getBasicInfo.html?id=" + paramsMap.get("id") + "&enterId=" + setypeMap.get("id");// 技能达人
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
		}

		return "/home/index.html";
	}

	/**
	 * 获取社群大V用户的信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/comunityBigVdetail")
	public ModelAndView comunityBigVdetail(HttpServletRequest request, HttpServletResponse response) {

		boolean flage = true;//true is other false is own

		ModelAndView mov = new ModelAndView();

		//request.getSession().setAttribute("openId", "onONowGA527KItDPM3sQIuUJcQmo");
		String openId = getCurrentOpenId(request);

		createParameterMap(request);

		//获取入住用户的基本信息
		UserBasicinfo userInfo = null;
		try {
			/***************************资源类型********start***************************/
			List<Map<String, Object>> dic_daList = comunityBigvService.getSourceTypeByCode();
			List<Map<String, Object>> sonList = comunityBigvService.getSourceTypeBySon();
			List<Map<String, Object>> resourceList = new ArrayList<Map<String, Object>>();

			String sonValue = "";
			String dicValue = "";
			String sonName = "";
			if (sonList.size() > 0 && dic_daList.size() > 0) {
				for (int i = 0; i < sonList.size(); i++) {
					sonValue = String.valueOf(sonList.get(i).get("classValue"));
					for (int j = 0; j < dic_daList.size(); j++) {
						sonName = String.valueOf(dic_daList.get(j).get("dic_name"));
						dicValue = String.valueOf(dic_daList.get(j).get("dic_value"));
						if (sonValue.equals(dicValue)) {
							Map<String, Object> sourMap = new HashMap<String, Object>();
							sourMap.put("classValue", sonValue);
							sourMap.put("className", sonName);
							resourceList.add(sourMap);
						}
					}
				}
			}
			mov.addObject("resourceList", resourceList);//资源类型*/
			/***************************资源类型********end***************************/

			List<Map<String, Object>> settleList = userBasicinfoService.querySettleListVersion3(paramsMap);//查询子表详细信息
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息
			String imgString = "";//活动图片
			String memNumberImg = "";//群成员图片
			String dairyTweetImg = "";//日常推文图片
			for (int i = 0; i < settleList.size(); i++) {
				if (settleList.size() > 0) {
					if ("activtyUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						imgString = String.valueOf(settleList.get(i).get("typeValue"));
						objMap.put("imgString", imgString);
					} else if ("memNumberImg".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						memNumberImg = String.valueOf(settleList.get(i).get("typeValue"));
						objMap.put("memNumberImg", memNumberImg);
					} else if ("dairyTweetUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						dairyTweetImg = String.valueOf(settleList.get(i).get("typeValue"));
						objMap.put("dairyTweetImg", dairyTweetImg);
					}
				}
			}
			//活动图片
			if (imgString != null && !"".equals(imgString)) {
				String[] imgArr = imgString.split(",");
				mov.addObject("imgArr", imgArr);
			}
			//群成员图片
			if (memNumberImg != null && !"".equals(memNumberImg)) {
				String[] memNumberImgArr = memNumberImg.split(",");
				mov.addObject("memNumberImg", memNumberImgArr);
			}

			//日常推文图片
			if (dairyTweetImg != null && !"".equals(dairyTweetImg)) {
				String[] dairyTweetImgArr = dairyTweetImg.split(",");
				mov.addObject("dairyTweetImg", dairyTweetImgArr);
			}

			userInfo = homeService.getUserInfo(paramsMap);

			//获取该类型下的优选服务
			PageObject po = new PageObject();
			String enterId = String.valueOf(objMap.get("enterId"));
			paramsMap.put("enterId", enterId);
			Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
			String typeValue = String.valueOf(setypeMap.get("typeValue"));
			mov.addObject("typeValue", typeValue);
			paramsMap.put("enterClass", enterId);
			po = preferredServiceService.queryPreferredServiceList(paramsMap);//优选服务
			mov.addObject("po", po.getDatasource());
			/**区分通过对比openId关联的ID  CFZ begin*/
			if (userInfo != null) {
				if (openId.equals(userInfo.getOpenId())) {
					flage = false;
				}
			}
			mov.addObject("settleList", settleList);
			mov.addObject("flage", flage);
			/**区分通过个人还是他人进行点击  CFZ end*/
			/**test scene */
			mov.addObject("userInfo", userInfo);
			mov.addObject("id", userInfo.getId());
			if (userInfo.getSkillInfos().size() > 0) {
				mov.addObject("skillId", userInfo.getSkillInfos().get(0).getId());
			}
			mov.setViewName("/wap/xkh_version_2/person_community");//跳转到社群大V
			return mov;

		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("redirect:/home/index.html");//跳转到首页  
		}
		return mov;
	}

	/**
	 * 跳转到添加入驻信息页面
	 * dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddSettle")
	//toAddSettle
	public String toAddSettle(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);

		//获取轮播图的集合
		List<BannerInfo> bannerInfos = null;
		bannerInfos = homeService.getBannerInfoListVersion3(4);//轮播图
		if (bannerInfos.size() > 0) {
			model.addAttribute("picUrl", bannerInfos);
		}
		//获取技能达人
		List<BannerInfo> jnImg = homeService.getBannerInfoListVersion3(5);//轮播图
		if (jnImg.size() > 0) {
			model.addAttribute("jnImg", jnImg.get(0));
		} else {
			model.addAttribute("jnImg", null);
		}
		//获取高校社团
		List<BannerInfo> gxImg = homeService.getBannerInfoListVersion3(6);//轮播图
		if (gxImg.size() > 0) {
			model.addAttribute("gxImg", gxImg.get(0));
		} else {
			model.addAttribute("gxImg", null);
		}
		Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
		String enterTypeName = String.valueOf(setypeMap.get("typeValue"));

		if (GlobalConstant.ENTER_TYPE_CDZY.equals(enterTypeName)) {
			return "redirect:/fieldEnter/toAddEnter.html?enterId=" + setypeMap.get("id");//场地入驻
		} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(enterTypeName)) {
			return "redirect:/schoolBusi/toAddSchoolBusi.html?enterId=" + setypeMap.get("id");//校内商家
		}
		//		String openId = "onONowGA527KItDPM3sQIuUJcQmo23";
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表

		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
		for (int i = 0; i < provnceList.size(); i++) {
			String provnce = String.valueOf(provnceList.get(i).get("province"));
			Map<String, Object> map = new HashMap<String, Object>();
			// 获取市信息
			List<Map<String, Object>> CityByProvnce = skillUserService.queryCityListVersion2(provnce);
			if (CityByProvnce != null && CityByProvnce.size() > 0) {
				map.put("value", provnce);// 市
				map.put("text", provnce);
				map.put("children", CityByProvnce);
			}
			proList.add(map);
		}
		String jsonProList = JSONObject.toJSONString(proList);

		Map<String, Object> userInfoMap = skillUserService.isUserExist(openId);// 用户基本信息

		model.addAttribute("proList", jsonProList);
		String cityId = String.valueOf(userInfoMap.get("cityId"));
		Map<String, Object> proMap = userBasicinfoService.getProvence(cityId);//查询所属省
		model.addAttribute("proMap", proMap);
		String sholId = String.valueOf(userInfoMap.get("schoolId"));
		Map<String, Object> sholMap = userBasicinfoService.getSholName(sholId);//查询所属学校

		model.addAttribute("sholMap", sholMap);
		//获取学校类型
		Map<String, Object> sholType = highSchoolSkillService.getSholType(sholId);
		model.addAttribute("sholType", sholType);

		/***************************资源类型********start***************************/
		List<Map<String, Object>> dic_daList = comunityBigvService.getSourceTypeByCode();
		List<Map<String, Object>> sonList = comunityBigvService.getSourceTypeBySon();

		List<Map<String, Object>> resourceList = new ArrayList<Map<String, Object>>();

		String sonValue = "";
		String dicValue = "";
		String sonName = "";
		if (sonList.size() > 0 && dic_daList.size() > 0) {
			for (int i = 0; i < sonList.size(); i++) {
				sonValue = String.valueOf(sonList.get(i).get("classValue"));
				for (int j = 0; j < dic_daList.size(); j++) {
					sonName = String.valueOf(dic_daList.get(j).get("dic_name"));
					dicValue = String.valueOf(dic_daList.get(j).get("dic_value"));
					if (sonValue.equals(dicValue)) {
						Map<String, Object> sourMap = new HashMap<String, Object>();
						sourMap.put("classValue", sonValue);
						sourMap.put("className", sonName);
						resourceList.add(sourMap);
					}
				}
			}
		}
		model.addAttribute("resourceList", resourceList);//资源类型*/
		/***************************资源类型********end***************************/

		String addFlag = "add";
		if (userInfoMap != null && userInfoMap.size() > 0) {
			String id = String.valueOf(userInfoMap.get("id"));
			paramsMap.put("id", id);
			addFlag = "edit";
			paramsMap.put("addFlag", addFlag);
		}
		paramsMap.put("addFlag", addFlag);
		List<Map<String, Object>> settleList = userBasicinfoService.querySettleListVersion3(paramsMap);//查询子表list
		String imgString = "";//活动图片
		String imgStringId = "";
		String memNumberImg = "";//群成员图片
		String dairyTweetImg = "";//日常推文图片
		String dairyTweetImgId = "";//日常推文图片Id
		for (int i = 0; i < settleList.size(); i++) {
			if (settleList.size() > 0) {
				if ("activtyUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					imgString = String.valueOf(settleList.get(i).get("typeValue"));
					userInfoMap.put("imgString", imgString);
					imgStringId = settleList.get(i).get("id").toString();
					userInfoMap.put("xkh_imgId", imgStringId);
				} else if ("memNumberImg".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					memNumberImg = String.valueOf(settleList.get(i).get("typeValue"));
					userInfoMap.put("memNumberImg", memNumberImg);
				} else if ("dairyTweetUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					dairyTweetImg = String.valueOf(settleList.get(i).get("typeValue"));
					userInfoMap.put("dairyTweetImg", dairyTweetImg);
					dairyTweetImgId = settleList.get(i).get("id").toString();
					userInfoMap.put("xkh_dairyImgId", dairyTweetImgId);
				}
			}
		}
		//活动图片
		if (imgString != null && !"".equals(imgString)) {
			String[] imgArr = imgString.split(",");
			model.addAttribute("imgArr", imgArr);
		}
		//群成员图片
		if (memNumberImg != null && !"".equals(memNumberImg)) {
			String[] memNumberImgArr = memNumberImg.split(",");
			model.addAttribute("memNumberImg", memNumberImgArr);
		}

		//日常推文图片
		if (dairyTweetImg != null && !"".equals(dairyTweetImg)) {
			String[] dairyTweetImgArr = dairyTweetImg.split(",");
			model.addAttribute("dairyTweetImg", dairyTweetImgArr);
		}
		model.addAttribute("settleList", settleList);

		boolean isUser = false;

		if (userInfoMap != null && userInfoMap.get("id") != null) {
			isUser = true;// 表示存在
			model.addAttribute("basicInfo", userInfoMap);
		} else {
			model.addAttribute("basicInfo", null);
		}

		paramsMap.put("openId", openId);
		paramsMap.put("enterId", paramsMap.get("enterId"));
		paramsMap.put("enterTypeName", enterTypeName);
		model.addAttribute("paramsMap", paramsMap);
		if (GlobalConstant.ENTER_TYPE_JNDR.equals(enterTypeName) || GlobalConstant.ENTER_TYPE_GXST.equals(enterTypeName)) {
			return "/wap/xkh_version_2/school_enterInfo";//技能达人入驻
		} else if (GlobalConstant.ENTER_TYPE_SQDV.equals(enterTypeName)) {
			//System.out.println("***************************community_enterInfo********************");
			return "/wap/xkh_version_2/community_enterInfo";//社群大V入驻
		}
		model.addAttribute("msg", "查询失败");
		return "/error";
	}

	/**
	 * 跳转到添加入驻信息页面
	 * dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toQuerySettle")
	//toAddSettle
	public String toQueyaSettle(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);

		//跳转到场地入驻和校内商家页面
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		Map<String, Object> userInfoMap = skillUserService.isUserExist(openId);// 用户基本信息
		paramsMap.put("enterId", userInfoMap.get("enterId"));
		Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
		String enterTypeName = String.valueOf(setypeMap.get("typeValue"));

		if (GlobalConstant.ENTER_TYPE_CDZY.equals(enterTypeName)) {
			return "redirect:/fieldEnter/toShowPerMessageEnter.html?enterId=" + setypeMap.get("id");//场地入驻
		} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(enterTypeName)) {
			return "redirect:/schoolBusi/toShowPerSchBusi.html?enterId=" + setypeMap.get("id");//校内商家
		}
		//获取轮播图的集合
		List<BannerInfo> bannerInfos = null;
		bannerInfos = homeService.getBannerInfoListVersion3(4);//轮播图
		if (bannerInfos.size() > 0) {
			model.addAttribute("picUrl", bannerInfos);
		}
		//获取技能达人
		List<BannerInfo> jnImg = homeService.getBannerInfoListVersion3(5);//轮播图
		if (jnImg.size() > 0) {
			model.addAttribute("jnImg", jnImg.get(0));
		} else {
			model.addAttribute("jnImg", null);
		}
		//获取高校社团
		List<BannerInfo> gxImg = homeService.getBannerInfoListVersion3(6);//轮播图
		if (gxImg.size() > 0) {
			model.addAttribute("gxImg", gxImg.get(0));
		} else {
			model.addAttribute("gxImg", null);
		}

		List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表

		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
		for (int i = 0; i < provnceList.size(); i++) {
			String provnce = String.valueOf(provnceList.get(i).get("province"));
			Map<String, Object> map = new HashMap<String, Object>();
			// 获取市信息
			List<Map<String, Object>> CityByProvnce = skillUserService.queryCityListVersion2(provnce);
			if (CityByProvnce != null && CityByProvnce.size() > 0) {
				map.put("value", provnce);// 市
				map.put("text", provnce);
				map.put("children", CityByProvnce);
			}
			proList.add(map);
		}
		String jsonProList = JSONObject.toJSONString(proList);

		model.addAttribute("proList", jsonProList);
		String cityId = String.valueOf(userInfoMap.get("cityId"));
		Map<String, Object> proMap = userBasicinfoService.getProvence(cityId);//查询所属省
		model.addAttribute("proMap", proMap);
		String sholId = String.valueOf(userInfoMap.get("schoolId"));
		Map<String, Object> sholMap = userBasicinfoService.getSholName(sholId);//查询所属学校

		model.addAttribute("sholMap", sholMap);
		//获取学校类型
		Map<String, Object> sholType = highSchoolSkillService.getSholType(sholId);
		model.addAttribute("sholType", sholType);

		/***************************资源类型********start***************************/
		List<Map<String, Object>> dic_daList = comunityBigvService.getSourceTypeByCode();
		List<Map<String, Object>> sonList = comunityBigvService.getSourceTypeBySon();

		List<Map<String, Object>> resourceList = new ArrayList<Map<String, Object>>();

		String sonValue = "";
		String dicValue = "";
		String sonName = "";
		if (sonList.size() > 0 && dic_daList.size() > 0) {
			for (int i = 0; i < sonList.size(); i++) {
				sonValue = String.valueOf(sonList.get(i).get("classValue"));
				for (int j = 0; j < dic_daList.size(); j++) {
					sonName = String.valueOf(dic_daList.get(j).get("dic_name"));
					dicValue = String.valueOf(dic_daList.get(j).get("dic_value"));
					if (sonValue.equals(dicValue)) {
						Map<String, Object> sourMap = new HashMap<String, Object>();
						sourMap.put("classValue", sonValue);
						sourMap.put("className", sonName);
						resourceList.add(sourMap);
					}
				}
			}
		}
		model.addAttribute("resourceList", resourceList);//资源类型*/
		/***************************资源类型********end***************************/

		String addFlag = "add";
		if (userInfoMap != null && userInfoMap.size() > 0) {
			String id = String.valueOf(userInfoMap.get("id"));
			paramsMap.put("id", id);
			addFlag = "edit";
			paramsMap.put("addFlag", addFlag);
		}
		paramsMap.put("addFlag", addFlag);
		List<Map<String, Object>> settleList = userBasicinfoService.querySettleListVersion3(paramsMap);//查询子表list
		String imgString = "";//活动图片
		String imgStringId = "";
		String memNumberImg = "";//群成员图片
		String dairyTweetImg = "";//日常推文图片
		String dairyTweetImgId = "";//日常推文图片Id
		for (int i = 0; i < settleList.size(); i++) {
			if (settleList.size() > 0) {
				if ("activtyUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					imgString = String.valueOf(settleList.get(i).get("typeValue"));
					userInfoMap.put("imgString", imgString);
					imgStringId = settleList.get(i).get("id").toString();
					userInfoMap.put("xkh_imgId", imgStringId);
				} else if ("memNumberImg".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					memNumberImg = String.valueOf(settleList.get(i).get("typeValue"));
					userInfoMap.put("memNumberImg", memNumberImg);
				} else if ("dairyTweetUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					dairyTweetImg = String.valueOf(settleList.get(i).get("typeValue"));
					userInfoMap.put("dairyTweetImg", dairyTweetImg);
					dairyTweetImgId = settleList.get(i).get("id").toString();
					userInfoMap.put("xkh_dairyImgId", dairyTweetImgId);
				}
			}
		}
		//活动图片
		if (imgString != null && !"".equals(imgString)) {
			String[] imgArr = imgString.split(",");
			model.addAttribute("imgArr", imgArr);
		}
		//群成员图片
		if (memNumberImg != null && !"".equals(memNumberImg)) {
			String[] memNumberImgArr = memNumberImg.split(",");
			model.addAttribute("memNumberImg", memNumberImgArr);
		}

		//日常推文图片
		if (dairyTweetImg != null && !"".equals(dairyTweetImg)) {
			String[] dairyTweetImgArr = dairyTweetImg.split(",");
			model.addAttribute("dairyTweetImg", dairyTweetImgArr);
		}
		model.addAttribute("settleList", settleList);

		boolean isUser = false;

		if (userInfoMap != null && userInfoMap.get("id") != null) {
			isUser = true;// 表示存在
			model.addAttribute("basicInfo", userInfoMap);
		} else {
			model.addAttribute("basicInfo", null);
		}
		//处理入驻类型

		paramsMap.put("openId", openId);
		paramsMap.put("enterTypeName", enterTypeName);//入驻类型
		model.addAttribute("paramsMap", paramsMap);
		if (GlobalConstant.ENTER_TYPE_JNDR.equals(enterTypeName) || GlobalConstant.ENTER_TYPE_GXST.equals(enterTypeName)) {
			return "/wap/xkh_version_2/school_enterInfoDetail";//技能达人高校社团我的信息
		} else if (GlobalConstant.ENTER_TYPE_SQDV.equals(enterTypeName)) {
			//System.out.println("***************************community_enterInfo********************");
			return "/wap/xkh_version_2/community_enterDeail";//社群大V入驻
		}
		model.addAttribute("msg", "查询失败");
		return "/error";
	}

	/**
	 * 添加入驻信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addSettleUser")
	public String addSettleUser(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			//String openId = "onONowGA527KItDPM3sQIuUJcQmo3";
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			paramsMap.put("openId", openId);
			String basId = String.valueOf(paramsMap.get("id"));

			boolean flag = false;
			if (paramsMap.get("id") == null || "".equals(basId) || basId == null) {
				flag = comunityBigvService.addSettleUserVersion3(paramsMap);
			} else {
				flag = comunityBigvService.upSettleVersion3(paramsMap);
			}

			if (flag) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/home/getmyInform.html";
			} else {
				model.addAttribute("msg", "添加失败");
				return "redirect:/home/index.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "redirect:/home/index.html";
		}

	}
}
