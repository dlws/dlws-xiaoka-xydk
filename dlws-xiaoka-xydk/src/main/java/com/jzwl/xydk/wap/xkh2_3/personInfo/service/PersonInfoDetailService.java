package com.jzwl.xydk.wap.xkh2_3.personInfo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_3.personInfo.dao.PersonInfoDetailDao;
import com.jzwl.xydk.wap.xkh2_3.personInfo.pojo.PersonInfoDetail;

@Service
public class PersonInfoDetailService {
	
	@Autowired
	PersonInfoDetailDao perInfoDetDao;
	
	
	/**
	 * 获取当前用户的技能详情信息 根据skillId查出单条
	 * @param map
	 * @return
	 */
	public Map<String,Object> getuserSkillinfoDetail(Map<String,Object> map){
		
		return perInfoDetDao.getuserSkillinfoDetail(map);
	}
	
	/**
	 * 根據當前的basicId获取链表查询父技能表ID和子技能表ID值
	 */
	public List<PersonInfoDetail> getUserSkillInfo(String basicId){
		
		return perInfoDetDao.getUserSkillInfo(basicId);
	}
	
	//获取当前用户的
	public Map<String,Object> getUnit(Map<String,Object> map){
			
		return perInfoDetDao.getUnit(map);
	}

}
