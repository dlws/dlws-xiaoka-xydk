/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillImage;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassF;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassS;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillinfo;



@Repository("pubDao")
public class PublishDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	
	//查询字典类型
	public boolean addOrderInfo(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id",  Sequence.nextId());
		map.put("createDate",  new Date());
		String sql = "insert into `xiaoka-xydk`.business_order " 
				 + " (id,businessId,userId,startDate,endDate,cityId,createDate,phone,demand,remark,openId) " 
				 + " values "
				 + " (:id,:businessId,:userId,:startDate,:endDate,:cityId,:createDate,:phone,:demand,:remark,:openId)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	//查询用户信息
	public Map<String, Object> queryHeadImg(String basicId) {
		
		String sql="select u.id,u.userName,u.openId,c.headImgUrl,u.openId from v2_wx_customer c,`xiaoka-xydk`.user_basicinfo u";
		sql=sql+ " where  c.openId = u.openId and u.isDelete = 0 and u.id =" + basicId;			
		
		return baseDAO.queryForMap(sql);
	}
	
	//查询技能信息
	public Map<String, Object> querySkillInfo(Map<String, Object> map) {
		
		String sql="select s.id,s.basicId,s.skillName,s.skillPrice,s.skillDepict,g.imgURL from `xiaoka-xydk`.user_skillinfo s,`xiaoka-xydk`.user_skill_image g";
		sql=sql+ " where g.skillId = s.id and  s.isDelete = 0 and s.id =" + map.get("id");			
		sql=sql+ " limit 1 ";
		return baseDAO.queryForMap(sql);
	}
	
	/////////////////////////////////////////////////////////////
	
	//查询字典类型
	public boolean addPubInfo(Map<String, Object> map) {
		
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id",  Sequence.nextId());
		map.put("createDate",  new Date());
		String sql = "insert into `xiaoka-xydk`.business_basicinfo " 
				 + " (id,openId,activityId,startDate,endDate,cityId,schoolId,levelId,createDate,phone,outputIndex,remark) " 
				 + " values "
				 + " (:id,:openId,:activityId,:startDate,:endDate,:cityId,:schoolId,:levelId,:createDate,:phone,:outputIndex,:remark)";
		
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	//查询字典类型
	public List<Map<String, Object>> queryActionType(Map<String, Object> map) {
		
		String sql="select * from v2_dic_data d ";
		sql=sql+ " where d.isDelete=0 and d.dic_id in (select dic_id from v2_dic_type where isDelete = 0 and dic_code='" + map.get("pid") + "')";			
		sql=sql+ " order by id ";
		
		return baseDAO.queryForList(sql);
	}
	
	//查询城市
	public List<Map<String, Object>> queryCity() {
		
		String sql="select * from tg_city  c";
		sql=sql+ " where c.isDelete=0 ";			
		sql=sql+ " order by c.number  ";
		
		return baseDAO.queryForList(sql);
	}
	
	//查询学校
	public List<Map<String, Object>> querySchool(Map<String, Object> map) {
		
		String sql="select * from tg_school_info s ";
		sql=sql+ " where s.isDelete=0  ";
		if(map.get("schoolId") != null && !"".equals(map.get("schoolId").toString())){
			sql=sql		+ " and s.cityId = " + map.get("schoolId");
		}
					
		sql=sql+ " order by s.schoolNumber,s.id ";
		
		return baseDAO.queryForList(sql);
	}
}
