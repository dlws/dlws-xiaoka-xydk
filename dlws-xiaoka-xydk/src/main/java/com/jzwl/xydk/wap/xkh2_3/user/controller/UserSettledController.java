/**
 * UserSettledController.java
 * com.jzwl.xydk.wap.xkh2_3.user.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_3.user.controller;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.baseInfoUser.service.BasInfoUserService;
import com.jzwl.xydk.manager.label.pojo.SkillMapLabel;
import com.jzwl.xydk.manager.label.service.LabelService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_3.user.service.PublicService;
import com.jzwl.xydk.wap.xkh2_3.user.service.UserSettledService;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * TODO 用户入驻的流程
 * @author   gyp
 * @Date 2017年5月9日
 * @Time 下午6:28:48
 */
@Controller
@RequestMapping("/userSettled")
public class UserSettledController extends BaseWeixinController{
	
	@Autowired
	private UserSettledService userSettledService;
	@Autowired
	private PublicService publicService;
	
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	
	@Autowired
	private SkillUserService skillUserService;
	
	@Autowired
	private BasInfoUserService basInfoUserService;
	
	@Autowired
	private HomeService homeService;
	
	@Autowired
	private LabelService labelService;
	
	private static Logger log = LoggerFactory
			.getLogger(UserSettledController.class);
	
	
	/**
	 * 
	 * 描述:显示平台的协议
	 * 作者:gyp
	 * @Date	 2017年5月9日
	 */
	@RequestMapping(value = "/showAgreement")
	public String showAgreement(HttpServletRequest request,HttpServletResponse response, Model model) {
		createParameterMap(request);
		return "/wap/xkh_version_2_3/enterInfo_agreement";// 跳转到协议展示页
	}
	/**
	 * 去入驻
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/toEnterUser")
	public String toAddSkillUser(HttpServletRequest request,HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			String openId = String.valueOf(request.getSession().getAttribute(
					"dkOpenId"));
			List<BannerInfo> banners = publicService.getSkillBanner(GlobalConstant.BANNER_RELEASE);
			List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表

			List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
			for (int i = 0; i < provnceList.size(); i++) {
				String provnce = String.valueOf(provnceList.get(i).get("province"));
				Map<String, Object> map = new HashMap<String, Object>();
				// 获取市信息
				List<Map<String, Object>> CityByProvnce = skillUserService.queryCityListVersion2(provnce);
				if (CityByProvnce != null && CityByProvnce.size() > 0) {
					map.put("value", provnce);// 市
					map.put("text", provnce);
					map.put("children", CityByProvnce);
				}
				proList.add(map);
			}
			String jsonProList = JSONObject.toJSONString(proList);

			//获取标签列表
			List<Map<String, Object>> Labe = skillUserService.queryLabeList();
			String Labelist = JSONObject.toJSONString(Labe);
			List<SkillMapLabel>  listLabel =  labelService.getAllFathSkillLable();
			//使用openId 去查需要的参数   用户id  入驻类型
			Map<String, Object> userInfoMap = skillUserService.isUserExist(openId);// 用户基本信息
			String labelId = String.valueOf(userInfoMap.get("labelId"));
			String [] labelArr = null;
			if(labelId!=null&&!"".equals(labelId)&&!"null".equals(labelId)){
				 labelArr = labelId.split(",");
			}
			model.addAttribute("labelArr", labelArr);
			if(labelArr!=null){
				model.addAttribute("labelArr", Arrays.asList(labelArr));
			}
			
			model.addAttribute("listLabel", listLabel);
			model.addAttribute("proList", jsonProList);
			model.addAttribute("banners", banners);//轮播图
			model.addAttribute("provnceList", provnceList);//省市
			model.addAttribute("Labe", Labe);//主页标签
			model.addAttribute("Labelist", Labelist);//主页标签Json格式
			model.addAttribute("userInfoMap", userInfoMap);
			model.addAttribute("paramsMap", paramsMap);
			
			return "/wap/xkh_version_2_3/enterInfo_write";//跳转至用户添加界面
			
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}
	
	/**
	 * 入驻
	 * 作者:gyp
	 * @Date	 2017年5月9日
	 */
	@RequestMapping(value = "/addEnterUser")
	public String addSkillUser(HttpServletRequest request,HttpServletResponse response, Model model) {
		createParameterMap(request);
		
		//获取上一个页面的请求地址
		String fromURL = request.getHeader("Referer");
		System.out.println("--------当前请求地址的请求来源-------"+fromURL);
		try {
			String openId = String.valueOf(request.getSession().getAttribute(
					"dkOpenId"));
			
			paramsMap.put("openId", openId);
			String basId = String.valueOf(paramsMap.get("baseId"));
			boolean flag = false;
			if(!"".equals(basId)&&basId!=null&&basId!="null"){
				 flag = userSettledService.editUserBaseInfo(paramsMap);
			}else{
				 flag = userSettledService.addUserBaseInfo(paramsMap);
			}
			model.addAttribute("paramsMap", paramsMap);
			
			if (!"".equals(openId) && !"null".equals(openId) && openId != null) {
				Map<String, Object> map = homeService.getCustomerByOpenId(openId);
				Map<String, Object> reduMap = homeService.getPersonInform(openId);
				boolean exist = true;
				exist = homeService.getIsExist(openId);
				if (exist == false) {
					map.put("auditState", "3");
				}
				model.addAttribute("map", map);
				model.addAttribute("reduMap", reduMap);
			}
			
			if(flag){
				return "/wap/xkh_version_2_2/buyer";// 跳转到我的页面
			}else{
				log.info("*******************************************添加出现异常**********************");
				return "redirect:/home/index.html";// 跳转到首页
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "操作失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}
	

}

