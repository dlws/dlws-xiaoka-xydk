package com.jzwl.xydk.wap.xkh2_2.ordseach.pojo;

import java.util.Date;

public class OrderDetail implements java.io.Serializable{

	/**
	 * 订单表的特殊化字段
	 */
	private static final long serialVersionUID = 1L;
	
	private long id ;
	private long orderId;
	private String typeName; // '名称',
	private String typeValue; // 值
	private int isDelete;//是否删除',
	private Date createDate;
	
	
	public OrderDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrderDetail(int id, int orderId, String typeName, String typeValue,
			int isDelete, Date createDate) {
		this.id = id;
		this.orderId = orderId;
		this.typeName = typeName;
		this.typeValue = typeValue;
		this.isDelete = isDelete;
		this.createDate = createDate;
	}
	
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getTypeValue() {
		return typeValue;
	}
	public void setTypeValue(String typeValue) {
		this.typeValue = typeValue;
	}
	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	

}
