package com.jzwl.xydk.wap.xkh2_3.invoice.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("invoiceDao")
public class InvoiceDao {

	@Autowired
	private BaseDAO baseDao;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询发票(未开发票/已开发票)  
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findInvoiceList(String openId, boolean flag, String status) {
		StringBuffer sb = new StringBuffer();
		if(flag){//已经入驻
			sb.append("select w.createDate,t.orderId,t.pid,t.openId,t.sellerOpenId,t.schoolId,t.cityId,t.payMoney,t.payTime,t.serviceMoney,"
					+ "	t.diplomatMoney,t.platformMoney,t.serviceRatio,t.diplomatRatio,t.platformRatio,t.realityMoney,t.orderStatus,"
					+ "	t.isFinish,t.finishTime,t.payway,t.cancelTime,t.invalidTime,t.receiveTime,t.confirmTime,t.refundTime,"
					+ "	t.applyRefundTime,t.linkman,t.phone,t.message,t.startDate,t.endDate,t.isDelete,t.enterType,t.partnerTradeNo,"
					+ "	t.orderType,t.invoiceId,u.userName as name ,u.headPortrait as url ,u.id as id, "
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='serviceType' and brd.orderId = t.orderId)serviceType ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='skillPrice' and brd.orderId = t.orderId)skillPrice ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='sellerNickname' and brd.orderId = t.orderId)sellerNickname ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='skillName' and brd.orderId = t.orderId)skillName ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='sellPhone' and brd.orderId = t.orderId)sellPhone ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='tranNumber' and brd.orderId = t.orderId)tranNumber ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='sellerHeadPortrait' and brd.orderId = t.orderId)sellerHeadPortrait ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='dic_name' and brd.orderId = t.orderId)dic_name ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='imgUrl' and brd.orderId = t.orderId)imgUrl ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='skillId' and brd.orderId = t.orderId)skillId ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='skillDepict' and brd.orderId = t.orderId)skillDepict "
					+ " from `xiaoka-xydk`.order t left join `xiaoka-xydk`.user_basicinfo u "
					+ " on t.openId = u.openId left join `xiaoka-xydk`.invoice_manage w on w.id = t.invoiceId "
					+ " where t.openId = '"+openId+"' ");
		}else{//未入驻
			sb.append("select w.createDate,t.orderId,t.pid,t.openId,t.sellerOpenId,t.schoolId,t.cityId,t.payMoney,t.payTime,"
					+ "	t.serviceMoney,t.diplomatMoney,t.platformMoney,t.serviceRatio,t.diplomatRatio,t.platformRatio,"
					+ "	t.realityMoney,t.orderStatus,t.isFinish,t.finishTime,t.payway,t.cancelTime,t.invalidTime,t.receiveTime,"
					+ "	t.confirmTime,t.refundTime,t.applyRefundTime,t.linkman,t.phone,t.message,t.startDate,t.endDate,t.isDelete,"
					+ "	t.enterType,t.partnerTradeNo,t.orderType,t.invoiceId,u.name,u.headImgUrl as url ,u.openId as id , "
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='serviceType' and brd.orderId = t.orderId)serviceType ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='skillPrice' and brd.orderId = t.orderId)skillPrice ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='sellerNickname' and brd.orderId = t.orderId)sellerNickname ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='skillName' and brd.orderId = t.orderId)skillName ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='sellPhone' and brd.orderId = t.orderId)sellPhone ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='tranNumber' and brd.orderId = t.orderId)tranNumber ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='sellerHeadPortrait' and brd.orderId = t.orderId)sellerHeadPortrait ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='dic_name' and brd.orderId = t.orderId)dic_name ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='imgUrl' and brd.orderId = t.orderId)imgUrl ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='skillId' and brd.orderId = t.orderId)skillId ,"
					+ " (select typeValue from `xiaoka-xydk`.order_detail brd where typeName='skillDepict' and brd.orderId = t.orderId)skillDepict "
					+ "	from `xiaoka-xydk`.order t left join `xiaoka`.v2_wx_customer u "
					+ " on t.openId = u.openId left join `xiaoka-xydk`.invoice_manage w on w.id = t.invoiceId "
					+ " where t.openId = '"+openId+"' ");
		}
		if(status.equals("0")){//未开发票
			sb.append(" and invoiceId is null and orderStatus =4 order by w.createDate desc");
		}else{//已开发票
			sb.append(" and invoiceId is not null and orderStatus =4 order by w.createDate desc");
		}
		return baseDao.queryForList(sb.toString());
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：判断是否入驻
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getUserBaseInfo(String openId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select * from `xiaoka-xydk`.user_basicinfo where openId = '"+openId+"' ");
		return baseDao.queryForMap(sb.toString());
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询发票类型
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getInvoType() {
		StringBuffer sb = new StringBuffer();
		sb.append("select dd.* from v2_dic_data dd left join v2_dic_type dt on dd.dic_id = dt.dic_id where dt.dic_code = 'invoiceType' and dd.isDelete = '0' and dt.isDelete = '0' ");
		return baseDao.queryForList(sb.toString());
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取税点
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getTaxPoint() {
		StringBuffer sb = new StringBuffer();
		sb.append("select dd.* from v2_dic_data dd left join v2_dic_type dt on dd.dic_id = dt.dic_id "
				+ " where dt.dic_code = 'taxpoint' and dd.dic_value = 'taxpointValue' and dd.isDelete = '0' and dt.isDelete = '0' ");
		return baseDao.queryForMap(sb.toString());
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：存储信息到发票表
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public String addInvoice(Map<String, Object> map) {
		String id = Sequence.nextId();
		Date createDate = new Date();
		map.put("id", id);
		map.put("createDate", createDate);
		StringBuffer sb = new StringBuffer();
		String invoiceTopType = (String)map.get("invoiceTopType");
		String obj = (String)map.get("obj");
		String orderIds = "";
		JSONArray json = JSONArray.fromObject(obj);
		if(json.size()>47){
			return "";
		}
		for(int i = 0;i < json.size();i++){
			JSONObject job = json.getJSONObject(i);
			if(i == json.size()-1){
				orderIds +=job.get("orderId");
			}else{
				orderIds +=job.get("orderId") + ",";
			}
		}
		map.put("orderIds", orderIds);
		if(invoiceTopType.equals("0")){
			sb.append("insert into `xiaoka-xydk`.invoice_manage "
					+ "	(id, typeId, sign, signTypeId,openId, "
					+ "	taxPointId, taxPointValue, expressName, expressPrice, recipientName, "
					+ "	recipientPhone, recipientAdress, "
					+ " payMoney, payStatus, invoiceType, auditType, createDate, orderIds) "
					+ "	values (:id, :typeId, :sign, :signTypeId,:openId, "
					+ " :taxPointId, :taxPointValue, :expressName, :expressPrice, "
					+ "	:recipientName, :recipientPhone, :recipientAdress, "
					+ "	:payMoney, :payStatus, :invoiceType,0, :createDate, :orderIds )");
		}else{
			sb.append("insert into `xiaoka-xydk`.invoice_manage "
					+ "	(id, typeId, sign, openId,dataName, "
					+ "	taxPointId, taxPointValue, expressName, expressPrice, recipientName, "
					+ "	recipientPhone, recipientAdress, firmAdress, firmPhone, firmNumber, "
					+ "	bankName, payMoney, payStatus, invoiceType, auditType, createDate, orderIds) "
					+ "	values (:id, :typeId, :sign,:openId, "
					+ "	:dataName, :taxPointId, :taxPointValue, :expressName, :expressPrice, "
					+ "	:recipientName, :recipientPhone, :recipientAdress, :firmAdress, "
					+ "	:firmPhone, :firmNumber, :bankName, :payMoney, :payStatus, :invoiceType,0, :createDate, :orderIds )");
		}
		boolean flag = baseDao.executeNamedCommand(sb.toString(), map);
		if(flag){
			return id;
		}
		return "";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：在支付页面点击否,删除当前数据防止产生垃圾数据
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param id
	 * @return
	 * @version
	 */
	public boolean deleteInvoice(String id) {
		StringBuffer sb = new StringBuffer();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		sb.append("delete from `xiaoka-xydk`.invoice_manage where id = :id ");
		return baseDao.executeNamedCommand(sb.toString(), map);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取发票类型
	 * 创建人： sx
	 * 创建时间： 2017年5月22日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getLogisticsTypes() {
		StringBuffer sb = new StringBuffer();
		sb.append("select dd.* from v2_dic_data dd left join v2_dic_type dt on dd.dic_id = dt.dic_id "
				+ "	where dt.dic_code = 'logisticsTypes' and dd.isDelete = '0' and dt.isDelete = '0' order by dd.dic_value ");
		return baseDao.queryForList(sb.toString());
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：专用发票税点
	 * 创建人： sx
	 * 创建时间： 2017年5月23日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getSpecPoint() {
		StringBuffer sb = new StringBuffer();
		sb.append("select dd.* from v2_dic_data dd left join v2_dic_type dt on dd.dic_id = dt.dic_id "
				+ "	where dt.dic_code = 'taxpoint' and dd.dic_value = 'pointValue' and dd.isDelete = '0' and dt.isDelete = '0' ");
		return baseDao.queryForMap(sb.toString());
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据发票ID查询订单ID
	 * 创建人： sx
	 * 创建时间： 2017年5月23日
	 * 标记：
	 * @param id
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object> findOrderIdsById(String id) {
		StringBuffer sb = new StringBuffer();
		sb.append("select orderIds,payMoney,openId from `xiaoka-xydk`.invoice_manage where id = '"+id+"'");
		Map<String, Object> map = baseDao.queryForMap(sb.toString());
		return map;
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据发票id修改发票状态
	 * 创建人： sx
	 * 创建时间： 2017年5月24日
	 * 标记：
	 * @param id
	 * @version
	 */
	public boolean updateInvoiceById(String id, String outTradeNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("tradeNo", outTradeNo);
		map.put("payStatus", 1);
		map.put("payDate", new Date());
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.invoice_manage set tradeNo = :tradeNo, payStatus = :payStatus, payDate = :payDate where id = :id");
		return baseDao.executeNamedCommand(sb.toString(), map);
	}
	/**
	 * 
	 * 描述:根据订单的Id更新订单的发票的id
	 * 作者:gyp
	 * @Date	 2017年5月27日
	 */
	public boolean updateOrderByOrderId(String invoiceId,String orderId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("invoiceId", invoiceId);
		map.put("orderId", orderId);
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.order set invoiceId = :invoiceId where orderId = :orderId");
		return baseDao.executeNamedCommand(sb.toString(), map);
	}

}
