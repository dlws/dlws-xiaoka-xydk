package com.jzwl.xydk.wap.xkh2_2.payment.cust_interven.pojo;

import java.util.Date;


public class CustomerIntervention {

	private Long id;// 'id',
	private Long feedbackId; // '反馈表的Id',
	private Long orderId;// '订单表的Id',
	private String reason;// '申诉理由',
	private int interveneType; //区分客户介入类型 0：反馈介入 1：退款介入',
	private int isDelete;// '是否删除',
	private Date createDate;// '创建时间',

	public CustomerIntervention() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomerIntervention(Long id, Long feedbackId, Long orderId,
			String reason, int isDelete, Date createDate) {
		super();
		this.id = id;
		this.feedbackId = feedbackId;
		this.orderId = orderId;
		this.reason = reason;
		this.isDelete = isDelete;
		this.createDate = createDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(Long feedbackId) {
		this.feedbackId = feedbackId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public int getInterveneType() {
		return interveneType;
	}

	public void setInterveneType(int interveneType) {
		this.interveneType = interveneType;
	}

}
