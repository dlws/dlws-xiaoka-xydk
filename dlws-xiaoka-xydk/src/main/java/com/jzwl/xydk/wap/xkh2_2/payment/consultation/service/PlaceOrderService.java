/**
 * PlaceOrderService.java
 * com.jzwl.xydk.wap.xkh2_2.payment.consultation.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.payment.consultation.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.payment.consultation.dao.PlaceOrderDao;

/**
 * 下单方法Service
 *
 * @author   wxl
 * @Date	 2017年4月11日 	 
 */
@Service("placeOrderService")
public class PlaceOrderService {

	@Autowired
	private PlaceOrderDao placeOrderDao;

	public Map<String, Object> getSkillBaseInfo(Map<String, Object> paramsMap) {

		return placeOrderDao.getSkillBaseInfo(paramsMap);

	}

	/*
	 * 添加订单
	 */
	public String insertOrder(Map<String, Object> paramsMap) {

		return placeOrderDao.insertOrder(paramsMap);

	}

	/*
	 * 获取卖家信息
	 */
	public Map<String, Object> getSelBaseInfo(Map<String, Object> paramsMap) {

		return placeOrderDao.getSelBaseInfo(paramsMap);

	}

	/*
	 * 订单详情
	 */
	public Map<String, Object> getOrderDetail(Map<String, Object> paramsMap) {

		return placeOrderDao.getOrderDetail(paramsMap);

	}

	/*
	 * 查询订单详情--字表list
	 */
	public List<Map<String, Object>> getOrderDetailData(Map<String, Object> paramsMap) {

		return placeOrderDao.getOrderDetailData(paramsMap);

	}

	/*
	 * 修改订单状态--添加一些字段值
	 */
	public String updateOrderStatus(Map<String, Object> paramsMap) {

		return placeOrderDao.updateOrderStatus(paramsMap);

	}
	/**
	 * 
	 * 描述:更新订单状态
	 * 作者:gyp
	 * @Date	 2017年4月26日
	 */
	public String updateOrder(Map<String, Object> paramsMap) {

		return placeOrderDao.updateOrder(paramsMap);

	}

	/*
	 * 优选服务 购买量+1
	 */
	public boolean addBuyNum(Map<String, Object> paramsMap) {

		return placeOrderDao.addBuyNum(paramsMap);

	}

}
