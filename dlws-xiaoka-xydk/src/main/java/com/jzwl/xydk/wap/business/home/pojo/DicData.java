/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class DicData extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "DicData";
	public static final String ALIAS_ID = "主键";
	public static final String ALIAS_DIC_ID = "字典类型id";
	public static final String ALIAS_DIC_NAME = "字典数据名";
	public static final String ALIAS_PARENT_ID = "父Id";
	public static final String ALIAS_DIC_VALUE = "字典数据值";
	public static final String ALIAS_IS_DELETE = "是否删除";
	
	//date formats
	

	//columns START
    /**
     * 主键       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 字典类型id       db_column: dic_id 
     */ 	
	private java.lang.Long dicId;
    /**
     * 字典数据名       db_column: dic_name 
     */ 	
	private java.lang.String dicName;
    /**
     * 父Id       db_column: parentId 
     */ 	
	private java.lang.Long parentId;
    /**
     * 字典数据值       db_column: dic_value 
     */ 	
	private java.lang.String dicValue;
    /**
     * 是否删除       db_column: isDelete 
     */ 	
	private java.lang.Double isDelete;
	//columns END

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	
	
	
	
	


	public java.lang.Long getDicId() {
		return this.dicId;
	}
	
	public void setDicId(java.lang.Long value) {
		this.dicId = value;
	}
	

	public java.lang.String getDicName() {
		return this.dicName;
	}
	
	public void setDicName(java.lang.String value) {
		this.dicName = value;
	}
	

	public java.lang.Long getParentId() {
		return this.parentId;
	}
	
	public void setParentId(java.lang.Long value) {
		this.parentId = value;
	}
	

	public java.lang.String getDicValue() {
		return this.dicValue;
	}
	
	public void setDicValue(java.lang.String value) {
		this.dicValue = value;
	}
	

	public java.lang.Double getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Double value) {
		this.isDelete = value;
	}
	

}

