package com.jzwl.xydk.wap.xkh2_3.changemanager.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_3.changemanager.dao.ChangeManagerDao;

@Service("changeMangerService")
public class ChangeMangerService {

	@Autowired
	private ChangeManagerDao changeManagerDao;

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据openId获取入驻信息
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> findByOpenId(String openId) {
		return changeManagerDao.findByOpenId(openId);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据微信号查询入驻信息
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param wxNumber
	 * @return
	 * @version
	 */
	public Map<String, Object> findByWxNumber(Map<String, Object> flagMap) {
		return changeManagerDao.findByWxNumber(flagMap);
	}

	public String addRecord(Map<String, Object> flagMap) {
		return changeManagerDao.addRecord(flagMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取记录信息
	 * 创建人： sx
	 * 创建时间： 2017年5月16日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> findChangeManager(String openId) {
		return changeManagerDao.findChangeManager(openId);
	}

	public boolean updateChangeManage(Map<String, Object> insertMap) {
		return changeManagerDao.updateChangeManage(insertMap);
	}
	
	public boolean updateSkillinfo(Map<String, Object> insertMap) {
		return changeManagerDao.updateSkillinfo(insertMap);
	}
	
	public boolean deleteBasicInfo(Map<String, Object> insertMap) {
		return changeManagerDao.updateBasicinfo(insertMap);
	}
	
	public Map<String, Object> getBasicInfoByRecordId(String recordId) {
		return changeManagerDao.getBasicInfoByRecordId(recordId);
	}
}
