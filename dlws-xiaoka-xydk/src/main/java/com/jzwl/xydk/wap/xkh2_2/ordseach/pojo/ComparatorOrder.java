package com.jzwl.xydk.wap.xkh2_2.ordseach.pojo;

import java.util.Comparator;

/**
 * 比较订单规则类
 * 根据时间进行比较list中的时间进行排序
 * 比较贴近当前时间排在前面
 * @author Administrator
 * 
 */
public class ComparatorOrder implements Comparator<OrderSea>{

	/**
	 * 根据第一个参数小于、等于或大于第
	 * 二个参数分别返回负整数、零或正整数。
	 */
	@Override
	public int compare(OrderSea o1, OrderSea o2) {
		
		OrderSea order0=(OrderSea)o1;
		OrderSea order1=(OrderSea)o2;
		//比较当前
		
		if(order0.getOrder().getCreateDate().getTime()>order1.getOrder().getCreateDate().getTime()){
			
			return -1;
		}else if(order0.getOrder().getCreateDate().getTime()==order1.getOrder().getCreateDate().getTime()){
			
			return 0;
		}else{
			
			return 1;
		}
	    
	}

}
