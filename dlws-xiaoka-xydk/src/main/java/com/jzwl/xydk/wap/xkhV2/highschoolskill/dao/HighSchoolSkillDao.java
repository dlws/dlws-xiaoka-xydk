/**
 * HighSchoolSkillDao.java
 * com.jzwl.xydk.wap.xkhV2.highschoolskill.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkhV2.highschoolskill.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.system.base.dao.BaseDAO;

/**
 * 高校社团，技能达人dao
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   wxl
 * @Date	 2017年3月15日 	 
 */
@Repository("highSchoolSkillDao")
public class HighSchoolSkillDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	public Map getUseridByopenid(Map<String, Object> paramsMap) {

		String sql = " select * from `xiaoka-xydk`.user_basicinfo where openid='" + paramsMap.get("openId") + "'";

		return baseDAO.queryForMap(sql, paramsMap);

	}

	/**
	 * 
	 * 获取学校type
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 *
	 * @param sholId
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public Map<String, Object> getSholType(String sholId) {

		String sql = "SELECT dcda.id as value,dcda.dic_name as text "
				+ " FROM `xiaoka-xydk`.schoolLabel sl INNER JOIN `xiaoka`.v2_dic_data dcda ON dcda.id=sl.dicDataId "
				+ " INNER JOIN `xiaoka`.tg_school_info shol ON sl.schoolId=shol.id where sl.schoolId= '" + sholId + "'";
		return baseDAO.queryForMap(sql);

	}

}
