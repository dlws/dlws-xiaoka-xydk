/**
 * OrderQuartzService.java
 * com.jzwl.xydk.wap.xkh2_2.quartz.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.quartz.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.utils.DateUtil;
import com.jzwl.xydk.manager.refunds.dao.RefundsDao;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.xkh2_2.quartz.dao.OrderQuartzDao;
import com.jzwl.xydk.wap.xkh2_4.diplomat.service.DiplomatService;

/**
 * 
 * @author   gyp
 * @Date 2017年4月20日
 * @Time 下午6:17:12
 */
@Service("orderQuartzService")
public class OrderQuartzService {
	
	
	
	@Autowired
	private OrderQuartzDao orderQuartzDao;
	@Autowired
	private SendMessageService sendMessage;
	
	@Autowired
	private RefundsDao refundsDao;
	
	@Autowired
	private DiplomatService diplomate;
	
	/**
	 * 描述:下单后超过一小时没有进行付款，系统
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public void cancelTimeOutNotPayOrders() {
		Date currDate = new Date();
		//Date beforeDate = DateUtil.addDay(currDate, 1);
		Date beforeDate = DateUtil.addHour(currDate, -1);
		String currTime = DateUtil.formatDate(currDate);
		String beforeTime = DateUtil.formatDate(beforeDate);
		//查询中超过1小时未付款的订单
		List<Map<String, Object>> list = orderQuartzDao.findTimeOutNotPayOrderList(beforeTime);
		//取消超时未付款订单
		try {
			
			//TODO 把取消订单的商品库存释放
			//发微信模板消息  更新订单状态
			if (list.size() > 0) {
				System.out.println(currTime+"：下单后1小时没有付款，系统取消订单！已取消"+list.size()+"单");
				for (Map<String, Object> map : list) {
					if (map != null) {
						boolean flag = orderQuartzDao.cancelTimeOutNotPayOrder(currTime, beforeTime,map.get("orderId").toString());
						if(flag){
							sendMessage.timeOutNotPayOrderSendMessageInfo(map);
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void cancelTimeOutNotPayOrders_fabu() {
		Date currDate = new Date();
		//Date beforeDate = DateUtil.addDay(currDate, 1);
		Date beforeDate = DateUtil.addHour(currDate, -1);
		String currTime = DateUtil.formatDate(currDate);
		String beforeTime = DateUtil.formatDate(beforeDate);
		//查询中超过1小时未付款的订单
		List<Map<String, Object>> list = orderQuartzDao.findTimeOutNotPayOrderList_fabu(beforeTime);
		//取消超时未付款订单
		try {
			
			//TODO 把取消订单的商品库存释放
			//发微信模板消息  更新订单状态
			if (list.size() > 0) {
				System.out.println(currTime+"：下单后1小时没有付款，系统取消订单！已取消"+list.size()+"单");
				for (Map<String, Object> map : list) {
					if (map != null) {
						boolean flag = orderQuartzDao.cancelTimeOutNotPayOrder_fabu(currTime, beforeTime,map.get("pid").toString());
						if(flag){
							sendMessage.timeOutNotPayOrderSendMessageInfo_fabu(map);
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/**
	 * 
	 * 描述:精准咨询，卖家接单24小时后咨询到期
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void serviceExpires_JZZX() {
		Date currDate = new Date();
		Date beforeDate = DateUtil.addDay(currDate, -1);
		//Date beforeDate = DateUtil.addHour(currDate, 24);
		String currTime = DateUtil.formatDate(currDate);//当前时间
		String expiresTime = DateUtil.formatDate(beforeDate);//接单后咨询到期的时间
		//获取精准咨询到期的订单
		List<Map<String, Object>> list = orderQuartzDao.findExpiresOrderList_jzzx(expiresTime);
		
		//发微信模板消息
		if (list != null && list.size() > 0) {
			System.out.println(currTime+"：进准咨询接单后超过1天，"+list.size()+"单订单咨询到期");
			for (Map<String, Object> map : list) {
				if (map != null) {
					try {
						map.put("isDelete", 0);
						boolean flag = orderQuartzDao.jzzxUpdateExpiresOrderList(map);
						if(flag){
							sendMessage.jzzxExpiresOrder(map);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	
	/**
	 * 
	 * 描述:订单7天未确认，系统自动确认
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public void orderSuccess() {
		Date currDate = new Date();
		Date beforeDate = DateUtil.addDay(currDate, -7);
		//Date beforeDate = DateUtil.addHour(currDate, 24);
		String currTime = DateUtil.formatDate(currDate);//当前时间
		String expiresTime = DateUtil.formatDate(beforeDate);//七天后的时间
		//获取要查询的订单
		List<Map<String, Object>> list = orderQuartzDao.orderSuccess(expiresTime);
		
		//发微信模板消息
		if (list != null && list.size() > 0) {
			System.out.println(currTime+"：系统自动确认，"+list.size()+"单订系统自动确认");
			for (Map<String, Object> map : list) {
				/*if (map != null) {
					try {
						map.put("isDelete", 0);
						boolean flag = orderQuartzDao.jzzxUpdateExpiresOrderList(map);
						if(flag){
							sendMessage.orderSuccess(map);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}*/
				if (map != null) {
					Long orderId = Long.parseLong(map.get("orderId").toString());
					Map<String, Object> orderMap = refundsDao.findfindOrderById(orderId);
					//商家分成比例
					BigDecimal serviceRatio = (BigDecimal)orderMap.get("serviceRatio");
					//订单金额
					BigDecimal realityMoney = (BigDecimal)orderMap.get("payMoney");
					//商家分成金额
					BigDecimal saleMoney = realityMoney.multiply(serviceRatio);
					//商家用户openId
					String sellerOpenId = (String)orderMap.get("sellerOpenId");
					//根据用户openId获取用户钱包信息
					Map<String, Object> mapp = refundsDao.findWalletBalanceByOpenId(sellerOpenId);
					Double balance = 0d;
					if(mapp != null && mapp.size()>0){
						//当前账户余额
						balance = (Double)mapp.get("balance");
					}
					//累加后余额
					BigDecimal ban = BigDecimal.valueOf(balance);
					BigDecimal totalMoney = saleMoney.add(ban);
					Map<String, Object> drawMap = new HashMap<String, Object>();
					drawMap.put("openId", sellerOpenId);
					drawMap.put("divideOrWithdraw", saleMoney);
					drawMap.put("balanceMoney", totalMoney);
					drawMap.put("state", 0);
					drawMap.put("divideOrWithdrawTime", new Date());
					drawMap.put("orderId", orderId);
					//分成记录表插入数据start
					refundsDao.divideOrWithDrawRecord(drawMap);
					//分成记录表插入数据end
					//钱包表插入或更新数据start
					refundsDao.updateWallet(mapp, drawMap);
					//钱包表插入或更新数据end
					//修改订单状态start
					Map<String, Object> order = new HashMap<String, Object>();
					order.put("orderId", orderId);
					order.put("state", 4);
					boolean flag = refundsDao.updateOrderState(order);
					//外交官分成
					//增加外交官分账
					diplomate.increaseDiplomatMoney(sellerOpenId, Double.parseDouble(order.get("DiplomatMoney").toString()), 
							"0", order.get("orderId").toString());
					if(flag){
						sendMessage.orderSuccess(map);
					}
				}
			}
		}
	}
	
	
	
	
	
	/**
	 * 
	 * 描述:精准投放发送模板信息
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public void jztfSendTextMessage() {
		Date currDate = new Date();
		String currTime = DateUtil.formatDate(currDate);
		List<Map<String, Object>> list = orderQuartzDao.jztfGetOrderList(currTime);
		
		if (list != null && list.size() > 0) {
			System.out.println(currTime+"：发送精准投放的文本信息！已成功确认"+list.size()+"单");
			for (Map<String, Object> map : list) {
				if (map != null) {
					Long orderId = Long.parseLong(map.get("orderId").toString());
					Map<String, Object> orderMap = refundsDao.findfindOrderById(orderId);
					//商家分成比例
					BigDecimal serviceRatio = (BigDecimal)orderMap.get("serviceRatio");
					//订单金额
					BigDecimal realityMoney = (BigDecimal)orderMap.get("payMoney");
					//商家分成金额
					BigDecimal saleMoney = realityMoney.multiply(serviceRatio);
					//商家用户openId
					String sellerOpenId = (String)orderMap.get("sellerOpenId");
					//根据用户openId获取用户钱包信息
					Map<String, Object> mapp = refundsDao.findWalletBalanceByOpenId(sellerOpenId);
					Double balance = 0d;
					if(mapp != null && mapp.size()>0){
						//当前账户余额
						balance = (Double)mapp.get("balance");
					}
					//累加后余额
					BigDecimal ban = BigDecimal.valueOf(balance);
					BigDecimal totalMoney = saleMoney.add(ban);
					Map<String, Object> drawMap = new HashMap<String, Object>();
					drawMap.put("openId", sellerOpenId);
					drawMap.put("divideOrWithdraw", saleMoney);
					drawMap.put("balanceMoney", totalMoney);
					drawMap.put("state", 0);
					drawMap.put("divideOrWithdrawTime", new Date());
					drawMap.put("orderId", orderId);
					//分成记录表插入数据start
					refundsDao.divideOrWithDrawRecord(drawMap);
					//分成记录表插入数据end
					//钱包表插入或更新数据start
					refundsDao.updateWallet(mapp, drawMap);
					//钱包表插入或更新数据end
					boolean flag = orderQuartzDao.jztfUpdateOrderList(map);
					try {
						sendMessage.jztfSendTextMessage(map);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				/*boolean flag = orderQuartzDao.jztfUpdateOrderList(map);
				if (flag) {
					try {
						sendMessage.jztfSendTextMessage(map);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}*/
			}
		}
	}
	
	/**
	 * 描述:买家申请退款，卖家超过七天未同意退款
	 * 自动更改为同意退款
	 * 作者：cfz
	 */
	public void orderAgreen(){
		
		//1.查询出当前订单状态为6的所有订单
		List<Map<String, Object>> list = orderQuartzDao.orderAgreen();
		//2.比较超时时间超过当前时间的订单超过自动将订单状态更改为7
		for (Map<String, Object> map : list) {
			//3.发送消息给用户
			if(orderQuartzDao.toChangeOrderStatus(map)){
				try {
					sendMessage.agreeRefundToBuyers(map);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				System.out.println("*********订单编号："+map.get("openId")+"***更改为待退款状态失败******");
			}
		}
		
	}
	/**
	 * 
	 * 描述:订单支付成功后卖家没有进行接单，系统自动取消
	 * 作者:gyp
	 * @Date	 2017年5月10日
	 */
	public void rejectOrders() {
		Date currDate = new Date();
		Date beforeDate = DateUtil.addDay(currDate, -1);
		//Date beforeDate = DateUtil.addHour(currDate, 24);
		String currTime = DateUtil.formatDate(currDate);//当前时间
		String expiresTime = DateUtil.formatDate(beforeDate);//一天后的时间
		//获取要查询的订单
		List<Map<String, Object>> list = orderQuartzDao.getPaySuccessOrders(expiresTime);
		
		//发微信模板消息
		if (list != null && list.size() > 0) {
			System.out.println(currTime+"：系统确认，"+list.size()+"单卖家没有接单");
			for (Map<String, Object> map : list) {
				if (map != null) {
					try {
						map.put("isDelete", 0);
						map.put("orderStatus", 11);
						boolean flag = orderQuartzDao.updateOrder(map);
						if(flag){
							sendMessage.refuseAcceptOrder(map);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
}

