package com.jzwl.xydk.wap.xkh2_2.payment.feedback.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.business.home.controller.HomeController;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassF;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.pojo.FeedBack;


/**
 * 
 * @author Administrator
 * the dao is mapping feedback table
 * use this dao to operate feedback Table
 */
@Repository
public class FeedbackDao{
	
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	Logger log = Logger.getLogger(getClass());
	
	/**
	 * search the feedback by 
	 * condition Map<String,Object>
	 */
	public List<FeedBack> searchFeeaBack(Map<String,Object> condition){
		
		if(!condition.isEmpty()){
			
			String sql = "select t.id,t.createTime,t.`describe`,t.feedbackstatus,t.feedbackurl,"
					+ "t.isDelete,t.orderId,t.reseon,t.suggestion,t.title,t.sellerRefuse,t.buyerReject from `xiaoka-xydk`.feedback t where 1=1 ";
			if(condition.containsKey("orderId")&&StringUtils.isNotBlank(condition.get("orderId").toString())){
				
				sql += "and t.orderId="+condition.get("orderId").toString()+" ";
				
			}
			if(condition.containsKey("isDelete")&&StringUtils.isNotBlank(condition.get("isDelete").toString())){
				
				sql += "and t.isDelete = "+condition.get("isDelete").toString()+" ";
				
			}
			//condition ++
			sql = sql.substring(0, sql.length()-1);
			
			
			List<FeedBack>  list= baseDAO.getJdbcTemplate().query(sql,
					new Object[] {},
					ParameterizedBeanPropertyRowMapper
							.newInstance(FeedBack.class));
			
			return list;
		}
		log.debug("------of no avail the Map condition--------");
		return null;
	}
	
	
	/**
	 * update the feedback 
	 */
	public boolean updateFeedBack(Map<String,Object> updateContent){
		
		updateContent.put("createTime",new Date());
		
		if(!updateContent.isEmpty()){
			
			String sql = "update `xiaoka-xydk`.feedback set ";
			
			if(updateContent.containsKey("title")){
				
				sql +=" title=:title ,";
			}
			if(updateContent.containsKey("feedbackurl")){
				
				sql +="feedbackurl =:feedbackurl ,";
			}
			if(updateContent.containsKey("describe")){
				
				sql +="`describe` =:describe  ,";
			}
			if(updateContent.containsKey("suggestion")){
				
				sql +="suggestion = :suggestion,";
			}
			if(updateContent.containsKey("reseon")){
				
				sql +="reseon =:reseon ,";
			}
			if(updateContent.containsKey("createTime")){
				
				sql +="createTime = :createTime,";
			}
			if(updateContent.containsKey("buyerReject")){//买家驳回状态
				
				sql +="buyerReject = :buyerReject,";
			}
			if(updateContent.containsKey("sellerRefuse")){//卖家拒绝状态
				
				sql +="sellerRefuse = :sellerRefuse,";
			}
			if(updateContent.containsKey("feedbackstatus")){//更新当前状态为（未阅读：1，已阅读：0）
				
				sql +="feedbackstatus = :feedbackstatus,";
			}
			
			sql=sql.substring(0, sql.length()-1);
			
			sql+="  where  orderId = "+updateContent.get("orderId");
				
			if(baseDAO.executeNamedCommand(sql, updateContent)){
				
				return true;
			}
			
			log.debug("------------the number of rows affected is 0-------------");
			return false;
		}
		
		return false;
	}
	
	/**
	 * insert the feed back 
	 */
	public boolean insertFeedBack(Map<String,Object> map){
		
		//生成当前的主键值
		map.put("id",  Sequence.nextId());
		//默认标记为未阅读
		map.put("feedbackstatus",1);
		map.put("createTime", new Date());
		if(!map.isEmpty()){
			
			String sql = "INSERT INTO `xiaoka-xydk`.feedback (id,orderId,title,feedbackurl,`DESCRIBE`,feedbackstatus,createTime)"
					+ "values(:id,:orderId,:title,:feedbackurl,:describe,:feedbackstatus,:createTime)";
			
			return baseDAO.executeNamedCommand(sql, map);
		}
		return false;
	}
	
	
	/**
	 * 将当前反馈表中的数据插入到feedbackold中
	 * 作为历史记录
	 */
	public boolean insertFeedBackOld(String orderId){
		
		if(StringUtils.isBlank(orderId)){
			
			log.debug("----------当前传入的orderId为 null 或 ''-------------");
			return false;
		}
		
		String sql = "Insert into `xiaoka-xydk`.feedback_old "
				+ "(orderId,title,feedbackurl,`describe`,suggestion,reseon,createTime,isDelete,feedbackstatus,feedbackId) "
				+ "select fbn.orderId,fbn.title,fbn.feedbackurl,fbn.`describe`,fbn.suggestion,fbn.reseon,fbn.createTime,fbn.isDelete,fbn.feedbackstatus,fbn.id "
				+ "from `xiaoka-xydk`.feedback fbn "
				+ "where fbn.orderId = ?";
		
		return baseDAO.executeCommand(sql, new Object[]{orderId});
	}
	
	
	/**
	 * search the feedback by 
	 * condition Map<String,Object>
	 */
	public List<FeedBack> searchFeeaBackOld(Map<String,Object> condition){
		
		if(!condition.isEmpty()){
			
			String sql = "select t.id,t.createTime,t.`describe`,t.feedbackstatus,t.feedbackurl,"
					+ "t.isDelete,t.orderId,t.reseon,t.suggestion,t.title,t.sellerRefuse,t.buyerReject from `xiaoka-xydk`.feedbackold t where 1=1 ";
			if(condition.containsKey("orderId")&&StringUtils.isNotBlank(condition.get("orderId").toString())){
				
				sql += "and t.orderId="+condition.get("orderId").toString()+" ";
				
			}
			if(condition.containsKey("isDelete")&&StringUtils.isNotBlank(condition.get("isDelete").toString())){
				
				sql += "and t.isDelete = "+condition.get("isDelete").toString()+" ";
				
			}
			//condition ++
			sql = sql.substring(0, sql.length()-1);
			
			
			List<FeedBack>  list= baseDAO.getJdbcTemplate().query(sql,
					new Object[] {},
					ParameterizedBeanPropertyRowMapper
							.newInstance(FeedBack.class));
			
			return list;
		}
		log.debug("------of no avail the Map condition--------");
		return null;
	}
	
	
	
	

}
