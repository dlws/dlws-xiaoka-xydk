package com.jzwl.xydk.wap.xkh2_3.publish.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_3.publish.dao.PublishDao;
import com.jzwl.xydk.wap.xkh2_3.publish.pojo.LeaveAndSchool;
import com.jzwl.xydk.wap.xkh2_3.publish.pojo.SkillDate;

@Service("publishService")
public class PublishService {

	@Autowired
	private PublishDao publishDao;

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询已经阅读页面
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @return
	 * @version
	 */
	public Map<String, Object> getRelease() {
		return publishDao.getRelease();
	}
	
	/**
	 * 根据个人的openId获取信息
	 * 描述:
	 * 作者:gyp
	 * @Date	 2017年6月7日
	 */
	public Map<String, Object> getBusinessRead(String openId) {
		return publishDao.getBusinessRead(openId);
	}
	
	public boolean addBusinessRead(Map<String, Object> map) {
		return publishDao.addBusinessRead(map);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询发布类型
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getPublishType() {
		return publishDao.getPublishType();
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取商家发布轮播图
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @return
	 * @version
	 */
	public Map<String, Object> getBanner() {
		return publishDao.getBanner();
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取商家发布轮播图
	 * 创建人： cfz
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @return
	 * @version
	 */
	public Map<String, Object> getBanner(String type) {
		return publishDao.getBanner(type);
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询一级分类
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	public String getFirstCategory() {
		
		
		List<Map> toJsonlist = new ArrayList<Map>();
		List<SkillDate> list = new ArrayList<SkillDate>();
		SkillDate singleSkillDate = new SkillDate();
		List<Map<String, Object>> fatherList = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> sonList = new ArrayList<Map<String,Object>>();
		fatherList = publishDao.getFirstCategory();
		for (Map<String, Object> map : fatherList) {
			//调用查询子技能
			singleSkillDate.setFatherSkill(map);
			sonList = publishDao.getSonSkillMessage(map);
			list.add(singleSkillDate);
			Map tempMap = new HashMap();
			tempMap.put("value", map.get("id"));
			tempMap.put("text", map.get("className"));
			List<Map<String,Object>> newSonList = new ArrayList<Map<String,Object>>();
			//存入当前子类的数据
			for (Map<String, Object> map2 : sonList) {
				
				Map<String,Object> mapCurren = new HashMap<String, Object>();
				mapCurren.put("value", map2.get("id"));
				mapCurren.put("text", map2.get("className"));
				newSonList.add(mapCurren);
			}
			tempMap.put("children", newSonList);
			toJsonlist.add(tempMap);
		}
		Gson gson = new Gson(); 
		String str = gson.toJson(toJsonlist);  
		return str;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据一级分类查询二级分类
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @param id
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getSecondCategory(String id) {
		return publishDao.getSecondCategory(id);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取城市
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getCityList() {
		return publishDao.getCityList();
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询列表
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getLabelList() {
		return publishDao.getLabelList();
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询985/211院校
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getNineList() {
		return publishDao.getNineList();
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询本课院校
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getGradList() {
		return publishDao.getGradList();
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询专科院校
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getSpecList() {
		return publishDao.getSpecList();
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：提交精准需求
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean addJzxq(Map<String, Object> paramsMap) {
		return publishDao.addJzxq(paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询账号信息(精准咨询)
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getInfoList(Map<String, Object> paramsMap) {
		
		
		System.out.println(paramsMap.toString());
		String condiPeople = "";
		//get current condition for this select
		if(paramsMap.containsKey("resources")){
			System.out.println("*****************resources************************="+paramsMap.get("resources"));
			if(StringUtils.isNotBlank(paramsMap.get("resources").toString())){
				
				//get userId
				condiPeople = publishDao.getSonSkillUserId(paramsMap.get("resources").toString());
				
			}
		}
		System.out.println("------------condiPeople is :----------------"+condiPeople);
		paramsMap.put("resources", condiPeople);
		return publishDao.getInfoList(paramsMap);
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询账号信息(精准投放)
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getInfoListJztf(Map<String, Object> paramsMap) {
		
		System.out.println(paramsMap.toString());
		String condiPeople = "";
		//get current condition for this select
		if(paramsMap.containsKey("resources")){
			if(StringUtils.isNotBlank(paramsMap.get("resources").toString())){
				
				//get userId
				condiPeople = publishDao.getSonSkillUserId(paramsMap.get("resources").toString());
				
			}
		}
		System.out.println("------------condiPeople is :----------------"+condiPeople);
		paramsMap.put("resources", condiPeople);
		
		return publishDao.getInfoListJztf(paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询精准咨询优选服务
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @return
	 * @version
	 */
	public Map<String, Object> preferrService() {
		return publishDao.preferrService();
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询精准投放优选服务
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @return
	 * @version
	 */
	public Map<String, Object> preferrServiceJztf() {
		return publishDao.preferrServiceJztf();
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：添加到已选
	 * 创建人： sx
	 * 创建时间： 2017年5月5日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean addPreferr(Map<String, Object> paramsMap) {
		String obj = (String)paramsMap.get("obj");
		if(obj != null && !"".equals(obj)){
			JSONArray json = JSONArray.fromObject(obj);
			for(int i = 0;i < json.size();i++){
				JSONObject job = json.getJSONObject(i); 
				String openId = job.getString("openId");
				String skillId = job.getString("skillId");
				String enterType = job.getString("enterType");
				Map<String, Object> map = publishDao.isExits(openId,skillId,enterType);
				if(map != null && map.size() >0){
					return false;
				}
			}
		}
		return publishDao.addPreferr(paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：立即咨询添加订单
	 * 创建人： sx
	 * 创建时间： 2017年5月5日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public String addOrder(Map<String, Object> paramsMap) {
		return publishDao.addOrder(paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询订单
	 * 创建人： sx
	 * 创建时间： 2017年5月5日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<OrderDetail> searchOrderDetail(Map<String, Object> paramsMap) {
		return publishDao.searchOrderDetail(paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：精准投放下单
	 * 创建人： sx
	 * 创建时间： 2017年5月9日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public String addPutOrder(Map<String, Object> paramsMap) {
		return publishDao.addPutOrder(paramsMap);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据pid获取订单(取第一个)
	 * 创建人： sx
	 * 创建时间： 2017年5月10日
	 * 标记：
	 * @param pid
	 * @return
	 * @version
	 */
	public Map<String, Object> findOrderByPid(String pid) {
		return publishDao.findOrderByPid(pid);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询选中的人
	 * 创建人： sx
	 * 创建时间： 2017年5月10日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> findBasicInfo(Map<String, Object> paramsMap) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		String obj = (String)paramsMap.get("obj");
		if(obj != null && !"".equals(obj)){
			JSONArray json = JSONArray.fromObject(obj);
			for(int i = 0;i < json.size();i++){
				
				Map<String, Object> map = new HashMap<String, Object>();
				JSONObject job = json.getJSONObject(i);
				if(job.containsKey("id")){
					String id = job.getString("id");
					String headPortrait = job.getString("headPortrait");
					map.put("id", id);
					map.put("headPortrait", headPortrait);
					list.add(map);
				}
			}
		}
		return list;
	}
	/**
	 * 获取当前发布信息的订单状态
	 * 处理当前的List<Map<String,object>>
	 * 更改为List<String>;
	 */
	
	public List<String> getPublishOrder(String pid){
		
		 List<Map<String,Object>> list = publishDao.getPublishOrder(pid);
		 List<String> orderStatu = new ArrayList<String>();
		 
		 for (Map<String,Object> string : list) {
			 orderStatu.add(string.get("orderStatus").toString());
		 }
		 
		 return orderStatu;
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据Pid查询当前订单的钱数
	 * 创建人： cfz
	 * 创建时间： 2017年5月10日
	 * 标记：
	 * @param pid
	 * @return
	 * @version
	 */
	public String getPubSumMoney(String pid){
		
		return publishDao.getPubSumMoney(pid);
		
	}
	/**
	 * 
	 * 描述:根据订单的pid更新订单的状态
	 * 作者:gyp
	 * @Date	 2017年5月31日
	 */
	public boolean updateOrderById(String pid,String outTradeNo){
		
		return publishDao.updateOrderById(pid,outTradeNo);
		
	}
	
	/**
	 * 描述：根据订单状态pid更新orderStatus
	 * 字段和isDelete字段
	 */
	public boolean updateOrderByPid(Map<String,Object> map){
		
		return publishDao.updateOrderByPid(map);
	}
	
	
	/**
	 * 根据省份获取所有的学校和等级
	 * @param province
	 * @return
	 */
	public List<LeaveAndSchool> getSelectProvinceSchool(String province){
		
		List<LeaveAndSchool> leaveSchoolList = new ArrayList<LeaveAndSchool>();
		province = getCondition(province);
		
		//获取当前所有选中的城市下的等级
		List<Map<String,Object>> map = publishDao.getSchoolLeave(province);
		
		for (Map<String, Object> map2 : map) {
			
			LeaveAndSchool  sigleLeave = new LeaveAndSchool();
			sigleLeave.setLeave(map2.get("level").toString());
			List<Map<String,Object>> currenMap = publishDao.getSchoolByCityAndLevel(province,map2.get("level").toString());
			sigleLeave.setSchool(currenMap);
			leaveSchoolList.add(sigleLeave);
		}
		
		return leaveSchoolList;
	}
	
	
	/**
	 * 拼接省份为条件
	 */
	private String getCondition(String province){
		
		String[] arrayProvince = province.split(",");
		String condition = "";
		for (int i = 0; i < arrayProvince.length; i++) {
			condition +="'"+arrayProvince[i]+"',";
		}
		if(condition.endsWith(",")){
			condition =condition.substring(0, condition.length()-1);
		}
		return condition;
	}
	
}

