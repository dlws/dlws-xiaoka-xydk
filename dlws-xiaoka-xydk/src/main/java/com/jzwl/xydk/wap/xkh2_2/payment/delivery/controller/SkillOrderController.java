package com.jzwl.xydk.wap.xkh2_2.payment.delivery.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.DoubleArithUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.acurateDelivery.service.OrderJztfService;
import com.jzwl.xydk.manager.baseInfoSkill.dao.BaseInfoSkillDao;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.business.home.controller.HomeController;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderSea;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderCountService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.service.SkillOrderService;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.controller.FeedBackController;
import com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.service.HighSchoolSkillOrderService;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;
import com.jzwl.xydk.wap.xkh2_3.skill_detail.service.SkillDetailService;

/**
 * 操作订单controller
 * <p>
 * 
 * @author dxf
 * @Date 2017年4月6日
 */
@Controller("skillOrder")
@RequestMapping("/skillOrder")
public class SkillOrderController extends BaseWeixinController {
	@Autowired
	private SkillOrderService skillOrderService;
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private PreferredServiceService preferredServiceService;
	@Autowired
	private HighSchoolSkillOrderService highSchoolSkillOrderService;
	@Autowired
	private ChatMessageService chatMessageService;
	@Autowired
	private EntertypeService entertypeService;
	@Autowired
	private SendMessageService sendMessage; //调用模板消息使用
	@Autowired
	private BaseInfoSkillDao skill;
	@Autowired
	private HomeService homeService;
	@Autowired
	private OrderSeaService orderSeaService;
	@Autowired
	private OrderJztfService OrderJztfService;
	@Autowired
	private SkillDetailService userSkillService;
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	@Autowired
	private FeedBackController  feedBackController;
	@Autowired
	private OrderCountService orderCountService;
	
	Logger log = Logger.getLogger(HomeController.class);

	/**
	 * 加入已选 dxf return flag
	 */
	@RequestMapping(value = "/joinSelected")
	public String joinSelected(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			createParameterMap(request);
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			//			String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8g9B";
			paramsMap.put("purOpenId", openId);
			boolean flag = skillOrderService.joinSelected(paramsMap);
			if (flag) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/skillOrder/SelectedList.html";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}
	}
	
	/**
	 * 
	 * 描述:二级页面专用加入已选，这个需要返回加入已选后的内容 ----所以和其他页面的加入已选进行一下区分
	 * 	    返回map集合来存储数据。
	 * 作者:gyp
	 * @Date	 2017年7月3日
	 */
	@RequestMapping(value = "/joinSelectedSecond")
	public @ResponseBody Map<String,Object> joinSelectedSecond(HttpServletRequest request, HttpServletResponse response, Model model) {
			
		boolean flage = false;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			createParameterMap(request);
			
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			/*String openId = GlobalConstant.openId_gyp;*/
			paramsMap.put("purOpenId", openId);//买家的openId
			List<Map<String, Object>> skill_list = null;
			flage = skillOrderService.joinSelected(paramsMap);
			if(flage){
				skill_list = skillOrderService.getSeclectSkills(paramsMap);
			}
			map.put("msg", "获取信息成功");
			map.put("ajax_status", flage);
			map.put("skill_list",skill_list);
		} catch (Exception e) {
			map.put("msg", "获取信息失败");
			map.put("ajax_status", false);
		}
		return map;
	}
	
	/**
	 * 
	 * 描述:修改添加已选的数量
	 * 作者:gyp
	 * @Date	 2017年6月29日
	 */
	@RequestMapping(value = "/modifyBuyNum")
	public @ResponseBody boolean modifyBuyNum(HttpServletRequest request, HttpServletResponse response, Model model) {
			
		boolean flage = false;
		try {
			createParameterMap(request);
			flage = skillOrderService.modifyBuyNum(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
		}
		return flage;
	}

	/**
	 * dxf 已选列表
	 */
	@RequestMapping(value = "/SelectedList")
	public String SelectedList(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			//String openId = "o3P2SwQAkziHP-B4CIxhAzliAvNo";
			String openId = getCurrentOpenId(request);
			//更新当前的selectList下的所有OpenId变成已选
			Map<String,Object> map = new HashMap<String, Object>(); 
			map.put("isRead", 0);//0 表示已读 
			map.put("purOpenId",openId);
			//将状态更新为已读
			skillOrderService.updateSelectskill(map);
			
			paramsMap.put("purOpenId", openId);
			List<Map<String, Object>> list = skillOrderService.querySelected(paramsMap);
			model.addAttribute("list", list);
			model.addAttribute("paramsMap", paramsMap);
			return "wap/xkh_version_2_2/selected";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "获取数据失败");
			return "/error";
		}
	}

	/**
	 * 删除已选订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteOrder")
	public @ResponseBody boolean deleteBannerInfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		boolean flag = false;
		try {
			createParameterMap(request);
			flag = skillOrderService.deleteOrder(paramsMap);
			if (flag) {
				return flag;
			} else {
				model.addAttribute("msg", "删除失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return flag;
		}
		return flag;

	}

	@RequestMapping(value = "/delImage")
	public @ResponseBody Map delImage(HttpServletRequest request, HttpServletResponse response) {
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();

		Map<String, Object> image = userBasicinfoService.getSkillinfoData(paramsMap);// 获取子表中的信息
		String imgString = image.get("typeValue").toString();// 活动路径
		
		String imge = paramsMap.get("imageUrl").toString();
		//如果以当前数字结尾则直接替换
		if(imgString.endsWith(imge)){
			imgString.replaceAll(imge, "");
		}else{
			imgString.replaceAll(imge+",", "");
		}
		map.put("typeValue", imgString.replaceAll(imge, ""));
		map.put("imgeId", paramsMap.get("imgeId"));
		boolean flag = userBasicinfoService.updateSkillinfoData(map);
		map.put("flag", flag);

		return map;
	}
	/**
	 * 整理参数跳转到对应的下单页面
	 */
	@RequestMapping(value = "/toBuySkill")
	public String toBuySkill(HttpServletRequest request, HttpServletResponse response, Model model){
		
		createParameterMap(request);
		
		//根据当前的技能Id查询出当前用户的父类名称和 用户信息等
		String skillId = paramsMap.get("skillId").toString();//skillId
		//改版  获取当前传过来的技能ID 获取当前参数 如果用户包含id说明为 精准投放
		if(paramsMap.containsKey("id")&&StringUtils.isNotBlank(paramsMap.get("id").toString())){
			
			String id = paramsMap.get("id").toString();
			return "redirect:/skillOrder/toAddSelected.html?skillId="+skillId+"&isPreferred="+1+"&basiId="+id+"&enterTypeValue="+GlobalConstant.ENTER_TYPE_JZTF;
		}
		
		if(StringUtils.isNotBlank(skillId)){
			
			Map<String,Object>  skillMap = new HashMap<String, Object>();
			
			skillMap.put("id",skillId);
			
			skillMap = skill.getById(skillMap);
		
			String sellerOpenId = "";//卖家OpenId
			
			String classValue = skillMap.get("f_classValue").toString();//一级分类名称
		
			Map<String, Object> objMap = new HashMap<String, Object>(); //获取当前卖家用户信息
			
			objMap.put("id", skillMap.get("basicId"));
			
			objMap = userBasicinfoService.getById(objMap);//当前用户的详细信息
			
			//获取当前卖家用户的OpenId
			
			sellerOpenId = objMap.get("openId").toString();//卖家OpenId
			
			if (GlobalConstant.ENTER_TYPE_CDZY.equals(classValue)) {
				
				return "redirect:/fileOrder/toShowFieldOrder.html?skillId=" + skillId;// 场地入驻下单页面
			} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(classValue)) {
				
				return "redirect:/schoolBusOrder/toShowSchoolOrder.html?skillId=" + skillId;// 校内商家下单页面
			}else if("jndr".equals(classValue) ||"gxst".equals(classValue)){
				
				return "redirect:/placeHighSchoolSkillOrder/toPlaceHighSchoolSkill.html?skillId="+skillId+"&selOpenId="+sellerOpenId;
			}else if("sqdv".equals(classValue)){
				
				return "redirect:/skillOrder/toAddSelected.html?skillId="+skillId+"&isPreferred="+0+"&basiId="+objMap.get("id")+"&enterTypeValue="+classValue;
			}
		
		}
		return "redirect:/home/index.html";
	}
	
	
	
	

	/**
	 * 去结算 跳转到结算页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAddSelected")
	public String toAddSelected(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);

		try {
			String isPreferred = String.valueOf(paramsMap.get("isPreferred"));// 是否是优选
			String enterTypeValue = String.valueOf(paramsMap.get("enterTypeValue"));
			if ("1".equals(isPreferred)) {
				if ("jztf".equals(enterTypeValue)) {
					paramsMap.put("id", paramsMap.get("skillId"));
					Map<String, Object> preferredService = preferredServiceService.getById(paramsMap);
					model.addAttribute("skillInfo", preferredService);// 服务详情
					Map<String, Object> base = skillOrderService.getBaseInfo(paramsMap);
					model.addAttribute("baseInfo", base);// 基本信息
					return "wap/xkh_version_2_2/accurate_delivery";
				}
			} else {
				// 判断入驻类型
				//Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
				String typeValue = enterTypeValue;
				model.addAttribute("typeValue", typeValue);// 入驻类型
				String skillId = String.valueOf(paramsMap.get("skillId"));
				Map<String, Object> skillInfo = userSkillService.getSkillInfo(paramsMap.get("skillId").toString());//订单信息
				
				String skillImg = homeService.getImageUrl(skillInfo.get("fathValue").toString(),Long.parseLong(skillId));//技能图信息
				
				//当前技能的详细信息
				Map<String,Object> detailMap = new HashMap<String, Object>();
				detailMap.put("pid", skillId);
				detailMap = personInfoDetSer.getuserSkillinfoDetail(detailMap);
				//增加加入已选中的钱数等 2.4功能优化
				detailMap.put("pid", skillId);
				getSelectSkill(request,detailMap,model);
				
				Map<String, Object> base = skillOrderService.getBaseInfo(paramsMap);
				
				model.addAttribute("skillInfo", skillInfo);// 服务详情
				base.put("basiId", paramsMap.get("basiId"));
				model.addAttribute("baseInfo", base);// 基本信息
				model.addAttribute("skillImg", skillImg);//技能图片
				model.addAttribute("detailMap", detailMap);//技能详细信息
				
				if (GlobalConstant.ENTER_TYPE_SQDV.equals(typeValue)) {
					if(("zmt").equals(skillInfo.get("sonValue").toString())){
						return "wap/xkh_version_2_2/creatorder/sqdv_order";
					}else{
						return "wap/xkh_version_2_2/creatorder/sqdv_order_other";
					}
				} else if (GlobalConstant.ENTER_TYPE_CDZY.equals(typeValue)) {
					return "redirect:/fileOrder/toShowFieldOrder.html?skillId=" + skillId;// 场地入驻
				} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(typeValue)) {
					return "redirect:/schoolBusOrder/toShowSchoolOrder.html?skillId=" + skillId;// 校内商家
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
		}
		return "/home/index.html";
	}

	/**
	 * 结算 dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/subOrder")
	public String AddSelected(HttpServletRequest request, HttpServletResponse response, Model model) {

		createParameterMap(request);
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		paramsMap.put("openId", openId);
		//订单提交
		String orderId = skillOrderService.addOrder(paramsMap);
		//系统消息
		String nowOpenId = String.valueOf(paramsMap.get("sellerOpenId"));
		paramsMap.put("nowOpenId", nowOpenId);
		paramsMap.put("orderId", orderId);
		paramsMap.put("orderStatus", 1);
		chatMessageService.insertUpdateSysMeg(paramsMap);//卖家端
		chatMessageService.insertBuyMeg(paramsMap);//买家端
		//精准投放增加用户下单时模板消息推送
		sendMessage.submissionOrder(orderId, openId);
		
		return "redirect:/skillOrder/skillOrderDetail.html?orderId="+orderId+"";
		
	}

	//取消订单
	@RequestMapping(value = "/cancelOrder")
	public @ResponseBody boolean cancelOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		boolean flag = false;
		try {
			createParameterMap(request);
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			paramsMap.put("openId", openId);
			flag = skillOrderService.UpOrderStatus(paramsMap);//改变订单状态
			//系统消息
			chatMessageService.insertUpdateSysMeg(paramsMap);
			chatMessageService.insertBuyMeg(paramsMap);//买家端
			if (flag) {
				return flag;
			} else {
				model.addAttribute("msg", "取消成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "取消失败");
			return flag;
		}
		return flag;

	}

	//立即付款
	@RequestMapping(value = "/skillOrderPay")
	public @ResponseBody boolean skillOrderPay(HttpServletRequest request, HttpServletResponse response, Model model) {
		boolean flag = false;
		//在技能表添加购买数量
		createParameterMap(request);
		String enterType = String.valueOf(paramsMap.get("enterType"));
		
		flag = preferredServiceService.updateSkillPayNoPre(paramsMap);
		flag = skillOrderService.UpOrderStatus(paramsMap);
		//系统消息
		chatMessageService.insertUpdateSysMeg(paramsMap);
		//若更改订单状态成功
		if(flag){
			//1.根据订单编号查询当前的订单信息
			if(paramsMap.containsKey("orderId")){
				
				if(paramsMap.get("orderId")!= null){
					//根据orderId查询当前的订单
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("orderId", paramsMap.get("orderId"));
					List<OrderSea> ordList = new ArrayList<OrderSea>();
					ordList = orderSeaService.toSearchOrder(map);
					if(ordList.size()>0){
						String money = ordList.get(0).getOrder().getPayMoney().toString();
						sendMessage.payMoneySuccess(paramsMap.get("orderId").toString(),ordList.get(0).getOrder().getOpenId(),money);//取消订单模板消息推送
						return true;
					}
				}
				log.error("当前请求未传入orderId");
			}
		}
		
		return flag;
	}
	
	/**
	 * jztf订单详情 dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/skillOrderDetail")
	public String skillOrderDetail(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		//区分点击  默认非自己点击
		boolean isMySelf = false;
		try {
			createParameterMap(request);
			 String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
//			String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8g9B";
			paramsMap.put("openId", openId);
			String orderId = String.valueOf(paramsMap.get("orderId"));
			if (!"".equals(orderId)) {
				
				Map<String, Object> base = new HashMap<String, Object>(); //基本信息
				
				paramsMap.put("orderId", orderId);
				List<Map<String, Object>> settleList = skillOrderService.querySettleList(paramsMap);//查询子表详细信息
				model.addAttribute("settleList",settleList);
				
				feedBackController.feedBackCommon(paramsMap, model);
				Map<String, Object> skillInfo = new HashMap<String, Object>();
				String skillId ="";//技能id
				String imgString = "";//图片
				for (int i = 0; i < settleList.size(); i++) {
					if (settleList.size() > 0) {
						if ("payMoney".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String payMoney = String.valueOf(settleList.get(i).get("typeValue"));//付款金额
							skillInfo.put("payMoney", payMoney);
						} 
						if ("skillId".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							skillId = String.valueOf(settleList.get(i).get("typeValue"));//技能id
							paramsMap.put("id", skillId);
							Map<String, Object> preferMap = preferredServiceService.getById(paramsMap);
							String salesVolume = "0";
							if(!preferMap.isEmpty()){
								 salesVolume = String.valueOf(preferMap.get("salesVolume"));
							}else{
								//查询当前技能表中技能信息
								Map<String, Object> skillMapper = skillUserService.getSkillInfoById(skillId);
								salesVolume = String.valueOf(skillMapper.get("sellNo"));
							}
							
							skillInfo.put("skillId", skillId);
							skillInfo.put("salesVolume", salesVolume);
						} 
						if ("skillName".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String skillName = String.valueOf(settleList.get(i).get("typeValue"));//技能名称
							skillInfo.put("skillName", skillName);
						} 
						if ("serviceType".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String serviceType = String.valueOf(settleList.get(i).get("typeValue"));//服务类型
							skillInfo.put("serviceType", serviceType);
						} 
						if ("skillPrice".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String skillPrice =  String.valueOf(settleList.get(i).get("typeValue"));//单价
							skillInfo.put("skillPrice", skillPrice);
						}
						if ("skillDepict".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String skillDepict =  String.valueOf(settleList.get(i).get("typeValue"));//技能描述
							skillInfo.put("skillDepict", skillDepict);
						}
						if ("imgUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String imageUrl =  String.valueOf(settleList.get(i).get("typeValue"));//技能图片
							skillInfo.put("imageUrl", imageUrl);
						}
						if ("tranNumber".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String tranNumber =  String.valueOf(settleList.get(i).get("typeValue"));//购买数量
							skillInfo.put("tranNumber", tranNumber);
						}
						if ("accurateVal".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String accurateVal =  String.valueOf(settleList.get(i).get("typeValue"));//投放版位
							skillInfo.put("accurateVal", accurateVal);
						}
						if ("sellerHeadPortrait".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String sellerHeadPortrait =  String.valueOf(settleList.get(i).get("typeValue"));//卖家头像
							base.put("headPortrait", sellerHeadPortrait);
						}
						if ("sellerNickname".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String sellerNickname =  String.valueOf(settleList.get(i).get("typeValue"));//卖家昵称
							base.put("nickname", sellerNickname);
						}
						if ("picUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							imgString = String.valueOf(settleList.get(i).get("typeValue"));
							paramsMap.put("imgString", imgString);
						}
					}
				}
				
				//图片
				if (imgString != null && !"".equals(imgString)) {
					String[] imgArr = imgString.split(",");
					model.addAttribute("picImg", imgArr);
				}
				model.addAttribute("skillInfo", skillInfo);// 服务详情
				model.addAttribute("baseInfo", base);//卖家基本信息
				Map<String,Object> orderMap = skillOrderService.getOrderById(orderId); //订单详情
				
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟  
				String invalidTime = String.valueOf(orderMap.get("invalidTime"));
			    java.util.Date chu=sdf.parse(invalidTime); 
			    
			    long timmer = chu.getTime();
			    orderMap.put("chu", chu);
			    Date now = new Date();
			    String timeDiffer = subtracDateS(chu, now); 
			    
			    //系统消息
			    String nowOpenId = String.valueOf(paramsMap.get("sellerOpenId"));
			    paramsMap.put("nowOpenId", nowOpenId);
			    orderMap.put("timeDiffer", timeDiffer);
			    
			    if(openId.equals(orderMap.get("openId").toString())){
			    	
			    	isMySelf = true;
			    }
			    model.addAttribute("isMySelf", isMySelf);//区分点击
				model.addAttribute("orderMap", orderMap);//订单详细信息
				model.addAttribute("timmer", timmer);//订单详细信息
				return "wap/xkh_version_2_2/accurate_order";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}
	}
	
	
	/**
	 * 投放内容
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/putContent")
	public String putContent(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		String orderId = String.valueOf(paramsMap.get("orderId"));
		Map<String,Object> orderMap = skillOrderService.getOrderById(orderId); //订单详情
		List<Map<String, Object>> settleList = skillOrderService.querySettleList(paramsMap);//查询子表详细信息
		
		Map<String, Object> skillInfo = new HashMap<String, Object>();
		String imgString = "";//图片
		for (int i = 0; i < settleList.size(); i++) {
			if (settleList.size() > 0) {
				if ("picUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
					imgString = String.valueOf(settleList.get(i).get("typeValue"));
					paramsMap.put("imgString", imgString);
				}
			}
		}
		
		//图片
		if (imgString != null && !"".equals(imgString)) {
			String[] imgArr = imgString.split(",");
			model.addAttribute("picImg", imgArr);
		}
		model.addAttribute("skillInfo", skillInfo);// 服务详情
		model.addAttribute("orderMap", orderMap);//订单详细信息
		model.addAttribute("settleList", settleList);//订单子表信息
		model.addAttribute("orderId", orderId);//订单的id
		return "wap/xkh_version_2_2/putContent";
	}
	/**
	 * 
	 * 描述:更新订单为已成功
	 * 作者:gyp
	 * @Date	 2017年5月8日
	 */
	@RequestMapping(value = "/updateOrderSuccess")
	public @ResponseBody Map<String, Object> updateOrderSuccess(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		try {
			//更新订单的状态
			boolean flag = OrderJztfService.auditOrder(paramsMap);
			if(flag){
				map.put("msg", "获取信息成功");
				map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			}else{
				map.put("msg", "获取信息失败！");
				map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
		}

		return map;
	}
	
	
	/**
	 * 查询当前用户是否有新增加的已选
	 * @param request
	 * @param response
	 * @return
	 * @throws InterruptedException 
	 */
	@RequestMapping(value="/isReadSelect")
	public @ResponseBody List<String> isReadSelect(HttpServletRequest request, HttpServletResponse response) throws InterruptedException{
		
		//表示当前返回的集合  对象符合当前信息 则将当前添加到集合中  
		//isSelect 表示标记当前已选
		List<String> list = new ArrayList<String>();
		
		createParameterMap(request);
		
		if(!paramsMap.containsKey("json")){
			Thread.sleep(3000);
		}
		
		String openId = getCurrentOpenId(request);
		
		if(openId!=null){
			if(skillOrderService.isReadSelect(openId)){
				list.add("isSelect");
			}
			if(chatMessageService.haveNewMessage(openId)){
				list.add("haveNewMessage");
			}
			List<String> currentArray = orderCountService.getSellerCountStaues(openId);
			//list 不为空添加标记自己
			if(!currentArray.isEmpty()){
				list.add("markMyself");
				list.addAll(currentArray);
			}
			
		}
		
		return list;
	}
	/**
	 * 获取当前的价格总计和用户购买数量
	 * @param request
	 * @param map
	 * @param model
	 * @return
	 */
	public Model getSelectSkill(HttpServletRequest request,Map<String,Object> map,Model model){
		
		double skillPrice = 0d;
		double buyNum = 0d;
		double countPrice = 0d;
		
		Map<String,Object> paramMap = new HashMap<String, Object>();
		String openId = getCurrentOpenId(request);
		
		if(map.containsKey("pid")){
			paramMap.put("skillId",map.get("pid"));
			paramMap.put("purOpenId", openId);
			//获取当前传入的数量
			paramMap = skillOrderService.getSelectSkill(paramMap);
			
			//当前数据未加入已选来自立即下单
			if(paramMap == null){
				model.addAttribute("buyNum", 1);
				model.addAttribute("countPrice", 0);
				return model;
			}
			model.addAttribute("buyNum", paramMap.get("buyNum"));
			
			buyNum = Double.parseDouble(paramMap.get("buyNum").toString());
			
			if(map.containsKey("skillPrice")){
				
				skillPrice = Double.parseDouble(map.get("skillPrice").toString());
				countPrice = DoubleArithUtil.mul(buyNum, skillPrice);
			}
			model.addAttribute("countPrice",countPrice);
			
		}
		
		return model;
	}
	
	
	
	/**
	 * @param num1 被减数 结束时间
	 * @param num2 减数 当前时间
	 * @return 返回String 类型
	 */
	public static String subtracDateS(Date d1,Date d2){
		
		String timer = "";
		long date = d1.getTime() - d2.getTime();   
	    long day = date / (1000 * 60 * 60 * 24);    
	    long hour = (date / (1000 * 60 * 60) - day * 24);    
	    long min = ((date / (60 * 1000)) - day * 24 * 60 - hour * 60);  
	    long s = (date/1000 - day*24*60*60 - hour*60*60 - min*60);  
	    if(day<=0){
	    	timer=""+hour+"小时"+min+"分"+s+"秒";
	    }else{
	    	timer=""+day+"天"+hour+"小时"+min+"分"+s+"秒";
	    }
	    if(date<=0){
	    	timer="0";
	    }
	    
	    return timer;
	}
	
}
