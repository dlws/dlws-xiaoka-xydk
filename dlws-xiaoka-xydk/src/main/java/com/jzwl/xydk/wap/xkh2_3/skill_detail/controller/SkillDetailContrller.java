package com.jzwl.xydk.wap.xkh2_3.skill_detail.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.wap.business.home.dao.HomeDao;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.service.SkillOrderService;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;
import com.jzwl.xydk.wap.xkh2_3.skill_detail.service.SkillDetailService;



/**
 * 用户技能详情的controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value="/skill_detail")
public class SkillDetailContrller extends BaseWeixinController{
	
	Logger log = Logger.getLogger(SkillDetailContrller.class);
	
	@Autowired
	private HomeService homeService;
	
	@Autowired
	private HomeDao homeDao;
	
	@Autowired
	private PreferredServiceService preferredServiceService;
	
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	
	@Autowired
	private SkillDetailService userSkillService;
	
	@Autowired
	SkillOrderService skillOrderService;
	
	
	/**
	 * 场地技能私有化参数
	 */
	private final String FILED_SIZE = "filedsize";//场地规模
	private final String BASIC_MATERIAL = "basicmaterial";//基本物资配备
	private final String PARTIME_JOB = "partimejob";//基本物资配备
	
	/**
	 * 校内商家私有化参数
	 */
	private final String COMPARE_SIZE = "cooperaType";// 合作类型
	
	/**
	 * 获取当前技能详情的信息
	 * @param request 必须传入的参数为，
	 * 技能Id和当前的技能类型
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/getskill_detail")
	public String getSkillDetail(HttpServletRequest request,Model model){
		
		//普通技能的信息
		String [] array = {GlobalConstant.ENTER_TYPE_SQDV,GlobalConstant.ENTER_TYPE_GXST,
							GlobalConstant.ENTER_TYPE_JNDR,GlobalConstant.ENTER_TYPE_XNSJ,
							GlobalConstant.ENTER_TYPE_CDZY};
		//精准投放的信息
		String [] arrayPre = {GlobalConstant.ENTER_TYPE_JZZX,GlobalConstant.ENTER_TYPE_JZTF};
		
		//获取当前点击的技能类型
		createParameterMap(request);
		
		String skillId = paramsMap.get("skillId").toString();
		
		if(paramsMap.containsKey("classValue")&&paramsMap.get("classValue")!=null){
			
			String classValue = paramsMap.get("classValue").toString();
			
			//常规技能详情页面
			if(Arrays.toString(array).contains(classValue)){
				
				return "redirect:/skill_detail/getOrdSkillDetail.html?skillId="+skillId;//跳转到普通技能处理controller
			}
			//优选服务技能详情页面 personId
			if(Arrays.toString(arrayPre).contains(classValue)){
				
				String personId = paramsMap.get("personId").toString();
				
				return "redirect:/skill_detail/getPreferredDetail.html?skillId="+skillId+"&personId="+personId;//跳转到优选处理contrller
			}
			log.info("当前技能类型异常技能名称："+classValue);
			return "redirect:/home/index.html";//返回首页证明当前类型不存在
		}
		log.info("当前技能未传递技能类型");
		return "redirect:/home/index.html";//返回到首页
	}
	
	/**
	 * 关于精准投放和精准咨询的技能信息
	 * 用户Id和当前优选服务的Id
	 */
	@RequestMapping(value="/getPreferredDetail")
	public String getPreferredDetail(HttpServletRequest request,Model model){
		
		//获取必要参数信息
		createParameterMap(request);
		if(paramsMap.containsKey("personId")&&StringUtils.isNotBlank(paramsMap.get("personId").toString())){
			if(paramsMap.containsKey("skillId")&&StringUtils.isNotBlank(paramsMap.get("skillId").toString())){

				//区分个人查看或是他人查看
				boolean isMySelf = false;
				String currenOpenId=getCurrentOpenId(request);
				
				//获取当前用户的信息
				Map<String,Object> map = new HashMap<String,Object>();
				Map<String,Object> dicMap = new HashMap<String,Object>();
				map.put("id",paramsMap.get("personId"));
				UserBasicinfo userBasic = homeDao.getUserInfo(map);
				if(userBasic==null){
					log.info("------查询当前用户为空 id：(可能用户状态为isDelete=1)------"+map.get("id"));
					return "redirect:/home/index.html";
				}
				//获取当前优选服务技能的信息
				map.put("id",paramsMap.get("skillId"));//覆盖之间的传参
				map = preferredServiceService.getById(map);
				dicMap = preferredServiceService.getDicDataInfo(map);
				
				//区分用户点击
				if(currenOpenId.equals(userBasic.getOpenId())){
					
					isMySelf = true;//相等则为本人点击
				}
				if(StringUtils.isBlank(userBasic.getNickname())){
					
					userBasic.setNickname("匿名");
					if(StringUtils.isNotBlank(userBasic.getUserName())){
						userBasic.setNickname(userBasic.getUserName());
					}
					
				}
				model.addAttribute("isMySelf",isMySelf);
				model.addAttribute("map",map);
				model.addAttribute("dicMap",dicMap);
				model.addAttribute("userBasic",userBasic);
				//存入参数跳转页面 (若页面展示不同 则根据类型判定不同跳转)
				return "/wap/xkh_version_2_3/servicedetail/preferredservice";
				
			}
			log.info("当前用户的技能Id为空");
		}
		log.info("当前用户的Id为空");
		
		return "redirect:/home/index.html";
	}
	
	/**
	 * 普通技能详情页面 社群大V 场地资源等等
	 */
	@RequestMapping(value="/getOrdSkillDetail")
	public String getOrdSkillDetail(HttpServletRequest request,Model model){
		createParameterMap(request);
		//获取当前的技能ID
		if(paramsMap.containsKey("skillId")&&StringUtils.isNotBlank(paramsMap.get("skillId").toString())){
			
			//区分用户点击
			boolean isMySelf = false;
			//getBaseSession(request).setAttribute("dkOpenId", "o3P2SwZCegQ-M6AG4-Nukz5R8gN8");
			String currenOpenId = getCurrentOpenId(request);
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("id", paramsMap.get("skillId"));
			//查询关于当前用户的技能的信息
			Map<String,Object> skillInfo = userSkillService.getSkillInfo(paramsMap.get("skillId").toString());
			//查询关于当前用户的信息
			map.put("id",skillInfo.get("basicId"));
			
			UserBasicinfo userBasic = homeDao.getUserInfo(map);
			paramsMap.put("purOpenId", currenOpenId);
			List<Map<String, Object>> skill_list = skillOrderService.getSeclectSkills(paramsMap);
			
			map.put("pid", paramsMap.get("skillId").toString());
			//查询关于当前技能的详细信息
			Map<String,Object> detailMap = personInfoDetSer.getuserSkillinfoDetail(map);
			
			//区分用户点击
			if(currenOpenId.equals(userBasic.getOpenId())){
				
				isMySelf = true;//相等则为本人点击
			}
			if(StringUtils.isBlank(userBasic.getNickname())){
				
				userBasic.setNickname("匿名");
				if(StringUtils.isNotBlank(userBasic.getUserName())){
					userBasic.setNickname(userBasic.getUserName());
				}
				
			}
			//加载当前信息中的私有参数
			model = loadCommonMessage(skillInfo,model,detailMap);
			model.addAttribute("isMySelf",isMySelf);
			model.addAttribute("skillInfo", skillInfo);
			model.addAttribute("detailMap", detailMap);
			model.addAttribute("userBasic", userBasic);
			model.addAttribute("skill_list", skill_list);
			//取出之前存入的URL
			String url = model.asMap().get("url").toString();
			
			return url;
		}
		
		log.info("当前skillId为空"+paramsMap.get("skillId").toString());
		return "redirect:/home/index.html";
		
	}
	
	/**
	 *  ENTER_TYPE_SQDV = "sqdv";
	 *	ENTER_TYPE_GXST = "gxst";
	 *	ENTER_TYPE_JNDR = "jndr";
	 *	ENTER_TYPE_XNSJ = "xnsj";
	 *	ENTER_TYPE_CDZY = "cdzy";
	 */
	/**
	 * 
	 * @param classValue 当前
	 * @param model 用来传递参数的模型对象
	 * @return
	 */
	public Model loadCommonMessage(Map<String,Object> map, Model model,Map<String,Object> detailMap){
		
		
		String  classValue = map.get("fathValue").toString();
		
		String url = "";//传递当前处理过后返回的路径
		
		if(GlobalConstant.ENTER_TYPE_SQDV.equals(classValue)){
			//判定当前是否属于自媒体
			if("zmt".equals(map.get("sonValue").toString())){
				List<String> activtyList = StringUtil.dealString(detailMap.get("activtyUrl").toString());
				List<String> dairyTwee = StringUtil.dealString(detailMap.get("dairyTweetUrl").toString());
				//model = getMessageForCdzy(model);
				url="/wap/xkh_version_2_3/servicedetail/sqdv_zmt_service";
				model.addAttribute("activtyList", activtyList);
				model.addAttribute("dairyTwee", dairyTwee);
				model.addAttribute("url", url);
			}else{
				url="/wap/xkh_version_2_3/servicedetail/sqdv_other_service";//跳转社群大V其他
				List<String> activtyList = StringUtil.dealString(detailMap.get("activtyUrl").toString());
				List<String> dairyTwee = StringUtil.dealString(detailMap.get("dairyTweetUrl").toString());
				model.addAttribute("activtyList", activtyList);
				model.addAttribute("dairyTwee", dairyTwee);
				model.addAttribute("url", url);
			}
		}else if(GlobalConstant.ENTER_TYPE_GXST.equals(classValue)){//暂无个性化信息
			
			List<String> activtyUrlList = StringUtil.dealString(detailMap.get("activtyUrl").toString());
			String unit = userSkillService.getUnitById(map.get("company").toString());
			url="/wap/xkh_version_2_3/servicedetail/gxst_jndr_service";
			model.addAttribute("url", url);
			model.addAttribute("unit", unit);
			model.addAttribute("activtyUrlList", activtyUrlList);
			
		}else if(GlobalConstant.ENTER_TYPE_JNDR.equals(classValue)){//暂无个性化信息
			url="/wap/xkh_version_2_3/servicedetail/gxst_jndr_service";
			List<String> activtyUrlList = StringUtil.dealString(detailMap.get("activtyUrl").toString());
			String unit = userSkillService.getUnitById(map.get("company").toString());
			model.addAttribute("activtyUrlList", activtyUrlList);
			model.addAttribute("unit", unit);
			model.addAttribute("url", url);
			
		}else if(GlobalConstant.ENTER_TYPE_XNSJ.equals(classValue)){
			url="/wap/xkh_version_2_3/servicedetail/xnsj_service";
			String priceSection = getSectionPrice(detailMap);
			
			String typeStr = detailMap.get("cooperaType").toString();//合作形式：合作价格
			String companyStr = detailMap.get("cooperaCompany").toString();//合作单位：合作价格
			
			JSONObject jsonType= JSON.parseObject(typeStr);
			JSONObject jsonCompany= JSON.parseObject(companyStr);
			
			Map<String,Object> typeMap = (Map)jsonType;//合作形式：合作价格
			Map<String,Object> companyMap = (Map)jsonCompany;//合作形式：合作单位
			
			Map<String,String> resultMap = new HashMap<String, String>();
			//解析返回当前当前对应的Map单位
			resultMap = dealMapToUnit(companyMap);//(合作形式：次.天....)
			
			List<String> listClose = StringUtil.dealString(detailMap.get("closePhoto").toString());
			List<String> listMiddle =StringUtil.dealString(detailMap.get("allPhoto").toString()) ;
			List<String> listAll = StringUtil.dealString(detailMap.get("middlePho").toString());
			model.addAttribute("listClose",listClose);
			model.addAttribute("listMiddle",listMiddle);
			model.addAttribute("listAll",listAll);
			model = getMessageForXnsj(model);
			model.addAttribute("url", url);
			model.addAttribute("typeMap", typeMap);
			model.addAttribute("companyMap", resultMap);
			detailMap.put("skillPrice", priceSection);
			
		}else if(GlobalConstant.ENTER_TYPE_CDZY.equals(classValue)){
			url="/wap/xkh_version_2_3/servicedetail/cdzy_service";
			List<String> listClose = StringUtil.dealString(detailMap.get("closePhoto").toString());
			List<String> listMiddle =StringUtil.dealString(detailMap.get("allPhoto").toString()) ;
			List<String> listAll = StringUtil.dealString(detailMap.get("middlePho").toString());
			int  materTime = detailMap.get("material").toString().split(",").length;
			String unit = userSkillService.getUnitById(map.get("company").toString());
			
			model.addAttribute("unit", unit);
			model.addAttribute("listClose",listClose);
			model.addAttribute("listMiddle",listMiddle);
			model.addAttribute("listAll",listAll);
			
			model = getMessageForCdzy(model);
			model.addAttribute("url", url);
			model.addAttribute("materTime",materTime);
			
		}else{
			url="-------跳转到首页的URL----------";
			log.info("当前对象中的传入参数"+classValue);
		}
		
		return model;
	}
	
	/**
	 * 加载校内商家的基础信息
	 * @return
	 */
	public Model getMessageForXnsj(Model model){
		
		// 加载当前的合作类型（字典表） filed size
		List<Map<String, Object>> cooperaTypeList = userBasicinfoService.getFieldtype(COMPARE_SIZE);// 合作类型
		
		model.addAttribute("cooperaTypeList",cooperaTypeList);
		
		return model;
	}
	
	/**
	 * 加载场地入驻的基础信息
	 */
	public Model getMessageForCdzy(Model model){
		
		//加载当前的场地规模（字典表） filed size
		List<Map<String, Object>> filedSizeList = userBasicinfoService.getFieldtype(FILED_SIZE);
		//加载当前的基本物资配备（字典表）basic material 
		List<Map<String, Object>> materialList = userBasicinfoService.getFieldtype(BASIC_MATERIAL);
		//加载是否兼职字典表partime_job
		List<Map<String, Object>> partimejobList = userBasicinfoService.getFieldtype(PARTIME_JOB);
		
		//存入当前场地私有化参数
		model.addAttribute("filedSizeList",filedSizeList);
		model.addAttribute("materialList",materialList);
		model.addAttribute("partimejobList",partimejobList);
		
		return model;
	}
	
	/**
	 * 加载社群大V的基础信息
	 */
	public Model getMessageForSqdvZmt(Model model){
		
		
		
		
		return model;
	}
	/**
	 * 获取当前最高价格和最低价格的区间
	 * 返回String类型
	 */
	public static String getSectionPrice(Map<String,Object> echoMap){
		
		double max =0;
		double min =0;
		
		Map<String,Object> maps = (Map)JSON.parse(echoMap.get("cooperaType").toString());
		
		double[] arrayInt = new double[maps.size()];
		
		if(maps.size()>0){
			int i=0;
			for (Entry<String, Object> entry : maps.entrySet()) {
				
				arrayInt[i] = Double.valueOf(entry.getValue().toString());
				i++;
			}
			Arrays.sort(arrayInt);
			//最大值 
			max = arrayInt[arrayInt.length-1];
			//最小值
			min = arrayInt[0];
		}
		return min+"~"+max;
	}
	
	/**
	 * 处理当前的单位由（合作形式：id）变为（合作形式：次.天..）
	 * @return
	 */
	private Map<String,String> dealMapToUnit(Map<String,Object> map){
		
		Map<String,String> resultMap = new HashMap<String, String>();
		
		for (Entry<String, Object> iterable_element : map.entrySet()) {
			
			//1.获取当期compny中的单位Id
			String value = iterable_element.getValue().toString();
			//2.获取当前compny中的key值
			String key = iterable_element.getKey();
			//3.根据id值查询，返回字段中的一个单位
			String newValue = userSkillService.getUnitById(value);
			//4.重建Map值
			resultMap.put(key, newValue);
			
		}
		//5.返回
		return resultMap;
	}

}
