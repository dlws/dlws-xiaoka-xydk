package com.jzwl.xydk.wap.xkh2_2.payment.cust_interven.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.payment.cust_interven.dao.CustomerInterventionDao;
import com.jzwl.xydk.wap.xkh2_2.payment.cust_interven.pojo.CustomerIntervention;

@Service
public class CustomerInterventionService {

	
	@Autowired
	private CustomerInterventionDao customDao;
	
	/**
	 * 增加一条信息到客服介入表中
	 * @param conditMap
	 * @return
	 */
	public boolean insertCustIntervent(Map<String,Object> conditMap){
		
		
		return customDao.insertCustIntervent(conditMap);
	}
	
	/**
	 * 查询当前客服介入表中的信息
	 * @param map 查询条件
	 * @return
	 */
	public List<CustomerIntervention> searchCustInter(Map<String,Object> map){
		
		return customDao.searchCustInter(map);
	}
	
	
	/**
	 * 更新当前客服介入表中的信息
	 * @param map
	 * @return
	 */
	public boolean updateCustInter(Map<String,Object> map){
		
		return customDao.updateCustInter(map);
	}
	
}
