package com.jzwl.xydk.wap.xkh2_2.payment.evaluate.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.payment.evaluate.dao.ServiceEvaluateDao;
import com.jzwl.xydk.wap.xkh2_2.payment.evaluate.pojo.ServiceEvaluate;


@Service
public class ServiceEvaluateService {
	
	@Autowired
	private ServiceEvaluateDao serviceEvalDao;
	
	
	
	/**
	 * 新增一条记录到 ServiceEvaluat表
	 * @param map
	 * @return
	 */
	public boolean insertServiceEvaluate(Map<String,Object> map){
		
		return serviceEvalDao.insertServiceEvaluate(map);
	}
	
	/**
	 * 输入当前查询条件查询表
	 * @param map
	 * @return
	 */
	public List<ServiceEvaluate> searchServiceEvaluate(Map<String,Object> map){
		
		return serviceEvalDao.searchServiceEvaluate(map); 
	}
	
	

}
