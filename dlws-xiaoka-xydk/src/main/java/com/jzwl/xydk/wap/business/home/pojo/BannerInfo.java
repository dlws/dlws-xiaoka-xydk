/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class BannerInfo extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "BannerInfo";
	public static final String ALIAS_ID = "主键";
	public static final String ALIAS_NAME = "轮播图名称";
	public static final String ALIAS_PIC_URL = "图片的链接地址";
	public static final String ALIAS_REDIRECT_URL = "点击跳转地址（外部跳转）";
	public static final String ALIAS_ORD = "轮播图的播放顺序(默认为0）";
	public static final String ALIAS_CREATE_DATE = "创建日期";
	public static final String ALIAS_CREATE_USER = "创建人";
	public static final String ALIAS_IS_USE = "是否启用任务（0：否，1：是）";
	public static final String ALIAS_IS_DELETE = "是否删除（0：否，1：是）";
	
	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	

	//columns START
    /**
     * 主键       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 轮播图名称       db_column: name 
     */ 	
	private java.lang.String name;
    /**
     * 图片的链接地址       db_column: picUrl 
     */ 	
	private java.lang.String picUrl;
    /**
     * 点击跳转地址（外部跳转）       db_column: redirectUrl 
     */ 	
	private java.lang.String redirectUrl;
    /**
     * 轮播图的播放顺序(默认为0）       db_column: ord 
     */ 	
	private java.lang.Integer ord;
    /**
     * 创建日期       db_column: createDate 
     */ 	
	private java.util.Date createDate;
    /**
     * 创建人       db_column: createUser 
     */ 	
	private java.lang.String createUser;
    /**
     * 是否启用任务（0：否，1：是）       db_column: isUse 
     */ 	
	private java.lang.Integer isUse;
    /**
     * 是否删除（0：否，1：是）       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
	//columns END

	
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	
	
	
	
	
	
	
	


	public java.lang.String getName() {
		return this.name;
	}
	
	public void setName(java.lang.String value) {
		this.name = value;
	}
	

	public java.lang.String getPicUrl() {
		return this.picUrl;
	}
	
	public void setPicUrl(java.lang.String value) {
		this.picUrl = value;
	}
	

	public java.lang.String getRedirectUrl() {
		return this.redirectUrl;
	}
	
	public void setRedirectUrl(java.lang.String value) {
		this.redirectUrl = value;
	}
	

	public java.lang.Integer getOrd() {
		return this.ord;
	}
	
	public void setOrd(java.lang.Integer value) {
		this.ord = value;
	}
	

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}
	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class,FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

	public java.lang.String getCreateUser() {
		return this.createUser;
	}
	
	public void setCreateUser(java.lang.String value) {
		this.createUser = value;
	}
	

	public java.lang.Integer getIsUse() {
		return this.isUse;
	}
	
	public void setIsUse(java.lang.Integer value) {
		this.isUse = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

}

