package com.jzwl.xydk.wap.xkh2_2.payment.cust_interven.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.xkh2_2.payment.cust_interven.pojo.CustomerIntervention;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.pojo.FeedBack;



@Repository
public class CustomerInterventionDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	Logger log = Logger.getLogger(getClass());
	
	
	/**
	 * 插入当前用户的
	 * @param conditMap
	 * @return
	 */
	public boolean insertCustIntervent(Map<String,Object> conditMap){
		
		if(!conditMap.isEmpty()){
			
			conditMap.put("id", Sequence.nextId());
			conditMap.put("isDelete", 0);
			conditMap.put("createDate", new Date());
			String sql ="insert into `xiaoka-xydk`.customer_intervention (id,feedbackId,orderId,Reason,isDelete,createDate,interveneType)"
					+ "values (:id,:feedbackId,:orderId,:Reason,:isDelete,:createDate,:interveneType)";
			if(baseDAO.executeNamedCommand(sql, conditMap)){
				
				log.debug("-------当前用户插入客服反馈成功：--------"+conditMap.toString());
				return true;
			}
			log.debug("--------当前用户插入反馈失败：--------"+conditMap.toString());
		}
		
		return false;
	}
	
	/**
	 * 根据传入的查询条件进行查询
	 * @param map 查询条件
	 * @return
	 */
	public List<CustomerIntervention> searchCustInter(Map<String,Object> map){
		
		try {
			
			String sql ="select t.createDate,t.feedbackId,t.id,t.isDelete,t.orderId,t.Reason "
					+ "from `xiaoka-xydk`.customer_intervention t where 1=1 ";
				if(map.containsKey("orderId")){
					sql += " and t.orderId ="+map.get("orderId");
				}
				if(map.containsKey("feedbackId")){
					sql +=" and t.feedbackId ="+map.get("feedbackId");
				}
				if(map.containsKey("interveneType")){
					sql +=" and t.interveneType ="+map.get("interveneType");
				}
				List<CustomerIntervention> list = baseDAO.getJdbcTemplate().query(sql, new Object[] {},
						ParameterizedBeanPropertyRowMapper
						.newInstance(CustomerIntervention.class));
			return list;	
			
		} catch (Exception e) {
			log.debug("--------查询当前表出现异常 tableName:`xiaoka-xydk`.customer_intervention----------"+map.toString());
			
			return null;
		}
		
		
	}
	
	
	/**
	 * 更新当前表中的信息
	 * `xiaoka-xydk`.customer
	 * @param map
	 * @return
	 */
	public boolean updateCustInter(Map<String,Object> map){
		
		if(map.isEmpty()){
			log.info("-----------当前入参map值为空-------------");
			return false;
		}
		String sql = "UPDATE `xiaoka-xydk`.customer_intervention SET Reason=:Reason where id=:id";
		
		if(baseDAO.executeNamedCommand(sql, map)){
			log.debug("----------更改Reason 值成功 -------------");
			return true;
		}
		log.debug("-----------更新`xiaoka-xydk`.customer_intervention 表失败返回 false--------------"+map.toString());
		return false;
	}
	
	
	
	
	
}
