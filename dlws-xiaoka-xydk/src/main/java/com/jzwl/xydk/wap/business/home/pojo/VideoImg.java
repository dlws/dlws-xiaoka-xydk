/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.pojo;

import java.util.List;
import java.util.Map;

import com.jzwl.system.base.pojo.BasePojo;



public class VideoImg extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	

	private java.lang.String videoUrl;
	private java.lang.String imgUrl;
	private java.lang.String otherUrl;
	private java.lang.String opusName;//其他作品的名称
	
	
	public java.lang.String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(java.lang.String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public java.lang.String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(java.lang.String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public java.lang.String getOtherUrl() {
		return otherUrl;
	}
	public void setOtherUrl(java.lang.String otherUrl) {
		this.otherUrl = otherUrl;
	}
	public java.lang.String getOpusName() {
		return opusName;
	}
	public void setOpusName(java.lang.String opusName) {
		this.opusName = opusName;
	}





}

