package com.jzwl.xydk.wap.xkh2_2.payment.evaluate.pojo;

import java.util.Date;


/**
 * consumer order
 *  evaluate POJO
 */

public class ServiceEvaluate {
	
	private Long id; //'效率等级',
	private Long  orderId;// '要进行评价的订单Id',
	private int efficiencyGrade ;//
	private int  qualityGrade ;// COMMENT '质量等级',
	private int  attitudeGrade ;//COMMENT '态度等级',
	private String evaluate; //'服务评价',
	private Date createDate; //'创建时间',
	
	
	public ServiceEvaluate() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ServiceEvaluate(Long id, Long orderId, int efficiencyGrade,
			int qualityGrade, int attitudeGrade, String evaluate,
			Date createDate) {
		super();
		this.id = id;
		this.orderId = orderId;
		this.efficiencyGrade = efficiencyGrade;
		this.qualityGrade = qualityGrade;
		this.attitudeGrade = attitudeGrade;
		this.evaluate = evaluate;
		this.createDate = createDate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public int getEfficiencyGrade() {
		return efficiencyGrade;
	}
	public void setEfficiencyGrade(int efficiencyGrade) {
		this.efficiencyGrade = efficiencyGrade;
	}
	public int getQualityGrade() {
		return qualityGrade;
	}
	public void setQualityGrade(int qualityGrade) {
		this.qualityGrade = qualityGrade;
	}
	public int getAttitudeGrade() {
		return attitudeGrade;
	}
	public void setAttitudeGrade(int attitudeGrade) {
		this.attitudeGrade = attitudeGrade;
	}
	public String getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
}
