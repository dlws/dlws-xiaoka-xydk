package com.jzwl.xydk.wap.xkh2_4.diplomat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.id.Sequence;
import com.jzwl.xydk.wap.xkh2_2.payment.wallet.dao.WalletDao;
import com.jzwl.xydk.wap.xkh2_4.diplomat.dao.DiplomatDao;
import com.jzwl.xydk.wap.xkh2_4.diplomat.pojo.CityDate;

@Service("diplomatService")
public class DiplomatService {


	@Autowired
	private DiplomatDao diplomatDao;
	
	@Autowired
	private WalletDao walletDao;
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据微信号查询入驻信息
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param wxNumber
	 * @return
	 * @version
	 */
	public Map<String, Object> findByWxNumber(Map<String, Object> flagMap) {
		return diplomatDao.findByWxNumber(flagMap);
	}
	/**
	 *根据用户的记录获取用户信息
	 * @param recordId
	 * @return
	 */
	public Map<String, Object> getBasicInfoByRecordId(String recordId) {
		return diplomatDao.getBasicInfoByRecordId(recordId);
	}
	/**
	 * 是否同意
	 * @param insertMap
	 * @return
	 */
	public boolean updateChangeManage(Map<String, Object> insertMap) {
		return diplomatDao.updateChangeManage(insertMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据openId获取入驻信息
	 * 创建人： dxf
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> findByOpenId(String openId) {
		return diplomatDao.findByOpenId(openId);
	}
	
	public String addRecord(Map<String, Object> flagMap) {
		return diplomatDao.addRecord(flagMap);
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：判断外交官是否存在，返回true，false
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public boolean isDiplomatExistence(String xkhOpenId) {
		try {
			Map<String,Object> diplomatMap = diplomatDao.isDiplomatExistence(xkhOpenId);
			if(null!=diplomatMap&&null!=diplomatMap.get("openId")){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取省市学校信息
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryProviceCitySchoolInfo() {
		try {
			List<Map<String, Object>> list = diplomatDao.queryProviceCitySchoolInfo();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取个人信息
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public Map<String, Object> getUserInfo(String xkhOpenId) {
		try {
			Map<String,Object> map = diplomatDao.getUserInfo(xkhOpenId);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取外交官下的资源
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySource(String xkhOpenId) {
		try {
			//xkhOpenId 为外交官OPENID
			List<Map<String,Object>> list  = diplomatDao.querySource(xkhOpenId);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：保存外交官信息
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @return
	 * @version
	 */
	public boolean addDiplomatInfo(Map<String,Object> map) {
		try {
			boolean flag = diplomatDao.addDiplomatInfo(map);
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取外交官钱包金额
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public Map<String, Object> getWallet(String xkhOpenId) {
		try {
			Map<String,Object> map = diplomatDao.getWallet(xkhOpenId);
			return map;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询交易记录(分成或者提现)
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryDiplomatRecord(String xkhOpenId) {
		try {
			List<Map<String,Object>> list = diplomatDao.queryDiplomatRecord(xkhOpenId);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取未审核记录总金额
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public Map<String, Object> getMoneyByOpenId(String xkhOpenId) {
		try {
			Map<String,Object> map = diplomatDao.getMoneyByOpenId(xkhOpenId);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：插入提现申请记录
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param paraMap
	 * @return
	 * @version
	 */
	public String withdrawals(Map<String, Object> paraMap) {
		try {
			String id = diplomatDao.withdrawals(paraMap);
			return id;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据ID获取提现记录
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param id
	 * @return
	 * @version
	 */
	public Map<String, Object> getWithdrawalsRecordById(String id) {
		try {
			Map<String,Object> map = diplomatDao.getWithdrawalsRecordById(id);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：判断用户是否已经入驻(true:表示存在)
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public boolean isSettledByOpenId(String xkhOpenId) {
		try {
			Map<String,Object> map = diplomatDao.isSettledByOpenId(xkhOpenId);
			if(null!=map&&null!=map.get("id")){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：绑定关系，用户与外交官关系
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param xkhOpenId 用户openId
	 * @param openId 外交官openId
	 * @return
	 * @version
	 */
	public boolean band(String xkhOpenId, String openId) {
		try {
			boolean band = diplomatDao.band(xkhOpenId,openId);
			return band;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}	
	
	/**
	 * 获取当前单条的交易记录
	 */
	public Map<String,Object> getSingleHistory(String id){
		
		return diplomatDao.getSingleHistory(id);
	}
	
	
	/**
	 * 插入记录到 
	 * diplomat_divide_withdraw 外交官提现或分成记录表中
	 * @return
	 */
	public boolean insertDiplomatWithdraw(Map<String,Object> map){
		
		return diplomatDao.insertDiplomatWithdraw(map);
	}
	
	/**
	 * 查询当前提供服务方的OpenId是否
	 * 存在 相关联的外交官 有则返回外交官的OpenId
	 * 没有则返回"";
	 * @return
	 */
	public String correlatDiplomat(String sellerOpenId){
		
		return diplomatDao.correlatDiplomat(sellerOpenId);
	}
	
	
	/**
	 * 增加当前seller的外交官在钱包中的钱
	 * 插入一条记录到 diplomat_divide_withdraw 
	 * 外交官提现到分成记录中
	 * @param sellerOpenId 卖家用户的OpenId
	 * @param increaseMoney 变动的现金
	 * @param state  提现或者是分成
	 * @return
	 */
	public boolean increaseDiplomatMoney(String sellerOpenId,double increaseMoney,String state,String orderId){
		
		boolean mark = false;
		double balance = 0d;
		String diplomateOpenId = diplomatDao.correlatDiplomat(sellerOpenId);
		Map<String,Object> paramMap = new HashMap<String, Object>();
		Map<String,Object> paramMap_1 = new HashMap<String, Object>();
		
		if(StringUtils.isNotBlank(diplomateOpenId)){
			
			//1.先去更新wallet 钱包中的钱
			Map<String,Object> map = walletDao.getWalletByOpenId(diplomateOpenId);
			if(map.get("id")==null){
				//更新当前的钱包
				mark = walletDao.insertWallet(diplomateOpenId, increaseMoney,1);
			}else{
				balance = Double.parseDouble(map.get("balance").toString()) ;
				paramMap.put("openId", diplomateOpenId);
				paramMap.put("balance", increaseMoney+balance);
				paramMap.put("isDiplomat", 1);
				//在钱包中新增一条当前外交官的钱包记录
				mark = walletDao.updateWallet(paramMap);
			}
			//2.更新成功值则插入一条记录到 divideorwithdrawrecord 提现或分成表
			if(mark){
				paramMap_1.put("openId", diplomateOpenId);
				paramMap_1.put("divideOrWithdraw", increaseMoney);
				paramMap_1.put("balanceMoney", increaseMoney+balance);
				paramMap_1.put("state", 0);//状态0：分成 1：提现 
				paramMap_1.put("orderId", orderId);
				mark = diplomatDao.insertDiplomatWithdraw(paramMap_1);
				return true;
			}
		}
		System.out.println("----------当前服务商 未与外交官进行绑定-------------");
		return false;
	}
	
	/**
	 * 更新当前用户资源关联中间表
	 * @param map
	 * @return
	 */
	public boolean updateDiplomatsourceinfo(Map<String,Object> map){
		
		return diplomatDao.updateDiplomatsourceinfo(map);
	}
	
}
