/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class UserSkillclassF extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;

	//columns START
    /**
     * id       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 分类名称
     */ 	
	private java.lang.String className;
    /**
     * 图片地址
     */ 	
	private java.lang.String imageURL;
    /**
     * 状态
     */ 	
	private java.lang.Integer status;
    /**
     * 排列顺序
     */ 	
	private java.lang.Integer ord;
    /**
     * 是否删除
     */ 	
	private java.lang.Integer isDelete;
	public java.lang.Long getId() {
		return id;
	}
	public void setId(java.lang.Long id) {
		this.id = id;
	}
	public java.lang.String getClassName() {
		return className;
	}
	public void setClassName(java.lang.String className) {
		this.className = className;
	}
	public java.lang.String getImageURL() {
		return imageURL;
	}
	public void setImageURL(java.lang.String imageURL) {
		this.imageURL = imageURL;
	}
	public java.lang.Integer getStatus() {
		return status;
	}
	public void setStatus(java.lang.Integer status) {
		this.status = status;
	}
	public java.lang.Integer getOrd() {
		return ord;
	}
	public void setOrd(java.lang.Integer ord) {
		this.ord = ord;
	}
	public java.lang.Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(java.lang.Integer isDelete) {
		this.isDelete = isDelete;
	}
   
	

}

