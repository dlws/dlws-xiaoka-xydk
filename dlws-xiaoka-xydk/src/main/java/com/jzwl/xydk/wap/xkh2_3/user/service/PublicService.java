/**
 * UserSettledService.java
 * com.jzwl.xydk.wap.xkh2_3.user.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_3.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.xkh2_3.user.dao.PublicDao;

/**
 * TODO(这里用一句话描述这个类的作用)
 * @author   gyp
 * @Date 2017年5月9日
 * @Time 下午7:04:43
 */
@Service("publicService")
public class PublicService {
	
	@Autowired
	private PublicDao  publicDao;
	
	/**
	 * 描述:判断用户是否已经入驻
	 * 作者:gyp
	 * @Date	 2017年5月9日
	 */
	public List<BannerInfo> getSkillBanner(String bannerType) {
		
		List<BannerInfo> list = publicDao.getSkillBanner(bannerType);
		return list;
		
	}
	
	/**
	 * 获取字典值
	 * @return
	 * list
	 */
	public List<Map<String, Object>> getCanpanyMap() {
		// TODO Auto-generated method stub
		return publicDao.getCanpanyMap();
	}
	
	public List getClassSBySid(Map<String, Object> map){
		return publicDao.getClassSBySid(map);
	}
	
	
	public Map<String,Object> getSkillDetail(Map<String, Object> map) {

		return publicDao.getSkillDetail(map);
	}
	
	public boolean updateSkillDetail(Map<String, Object> map) {
		
		return publicDao.updateSkillDetail(map);
	}
	
	
}

