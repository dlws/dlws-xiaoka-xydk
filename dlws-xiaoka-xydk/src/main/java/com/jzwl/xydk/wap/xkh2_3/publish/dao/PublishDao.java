package com.jzwl.xydk.wap.xkh2_3.publish.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.id.Sequence;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;

@Repository("publishDao")
public class PublishDao {

	@Autowired
	private BaseDAO baseDao;

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询协议
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getRelease() {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.id, t.classId, t.title, t.content from `xiaoka-xydk`.release_protocol t "
				+ " left join `xiaoka`.v2_dic_data d ON t.classId = d.id "
				+ " where t.isDelete = '0' and d.isDelete = '0' and d.dic_value = 'publish' ");
		return baseDao.queryForMap(sb.toString());
	}
	
	public Map<String, Object> getBusinessRead(String openId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.openId, t.protocolId from `xiaoka-xydk`.business_read t "
				+ " where t.openId ='"+openId+"'");
		return baseDao.queryForMap(sb.toString());
	}
	
	public boolean addBusinessRead(Map<String, Object> paramsMap) {
		paramsMap.put("openId", paramsMap.get("openId"));
		paramsMap.put("createDate", new Date());//创建时间
		paramsMap.put("protocolId", paramsMap.get("id"));
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.business_read (openId,protocolId,createDate) "
				+ " values (:openId,:protocolId,:createDate)");
		return baseDao.executeNamedCommand(sb.toString(), paramsMap);
	
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询发布类型
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getPublishType() {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, className, classValue, imageURL, selectimage, noSelectimage, status, ord, createDate "
				+ "from `xiaoka-xydk`.user_skillclass_father where isDelete = '0' and systemType='1' ");
		return baseDao.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家发布轮播图
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getBanner() {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.id, t.name, t.picUrl, t.keyValue, t.redirectUrl, t.ord from `xiaoka-xydk`.banner_info t "
				+ " left join `xiaoka`.v2_dic_data v on v.dic_value = t.bannerType "
				+ " left join `xiaoka`.v2_dic_type d on d.dic_id = v.dic_id where "
				+ " t.isUse = '1' and t.isDelete = '0' and d.dic_code='XiaoKaBanner' and t.bannerType='9' ");
		return baseDao.queryForMap(sb.toString());
	}
	
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询一级分类
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getFirstCategory() {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, className, classValue, imageURL, selectimage, noSelectimage, status, ord, createDate from "
				+ " `xiaoka-xydk`.user_skillclass_father where systemType = '0' and isDelete = '0' ");
		return baseDao.queryForList(sb.toString());
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：商家发布轮播图
	 * 创建人： cfz
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getBanner(String type) {
		
		String sql = "select t.imageURL as picUrl from `xiaoka-xydk`.user_skillclass_father t where  t.systemType = 1 and classValue = '"+type+"'";
		
		return baseDao.queryForMap(sql);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据一级分类查询二级分类
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @param id
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getSecondCategory(String id) {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, fatherId, className, classValue, status ,ord from "
				+ "`xiaoka-xydk`.user_skillclass_son where fatherId = '"+id+"' order by ord ");
		return baseDao.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询城市
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getCityList() {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, cityName, province, number, cityOperateManager from tg_city where isDelete = '0' and id !='200010' and cityName is not null AND cityName !=''" );
		return baseDao.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询标签列表
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getLabelList() {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.id, t.dic_id, t.dic_name, t.dic_value "
				+ "	from v2_dic_data t left join v2_dic_type dt on t.dic_id = dt.dic_id "
				+ " where t.isDelete = '0' and dt.isDelete = '0' and dt.dic_code = 'SchoolLabel' ");
		return baseDao.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询985院校
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getNineList() {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.* from `xiaoka`.tg_school_info t "
				+ "	LEFT JOIN `xiaoka-xydk`.schoollabel s on t.id = s.schoolId "
				+ " LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = s.dicDataId "
				+ " where t.isDelete = '0' and dd.isDelete = '0' and dd.dic_name='985/211'");
		return baseDao.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询本科院校
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getGradList() {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.* from `xiaoka`.tg_school_info t "
				+ "	LEFT JOIN `xiaoka-xydk`.schoollabel s on t.id = s.schoolId "
				+ " LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = s.dicDataId "
				+ " where t.isDelete = '0' and dd.isDelete = '0' and dd.dic_name='本科' ");
		return baseDao.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询专科院校
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getSpecList() {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.* from `xiaoka`.tg_school_info t "
				+ "	LEFT JOIN `xiaoka-xydk`.schoollabel s on t.id = s.schoolId "
				+ " LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = s.dicDataId "
				+ " where t.isDelete = '0' and dd.isDelete = '0' and dd.dic_name='专科' ");
		return baseDao.queryForList(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean addJzxq(Map<String, Object> paramsMap) {
		String id = Sequence.nextId();
		paramsMap.put("id", id);
		paramsMap.put("createDate", new Date());//创建时间
		paramsMap.put("isDelete", 0);//是否删除
		StringBuffer sb = new StringBuffer();
		sb.append("insert into `xiaoka-xydk`.business_release (id,typeId,typeValue,createDate,isDelete) "
				+ " values (:id,:typeId,:typeValue,:createDate,:isDelete)");
		boolean flag = baseDao.executeNamedCommand(sb.toString(), paramsMap);
		if(flag){
			Map<String, Object> map = new HashMap<String, Object>();
			map = paramsMap;
			map.remove("id");
			map.remove("typeId");
			map.remove("typeValue");
			StringBuffer ssb = new StringBuffer();
			ssb.append("insert into `xiaoka-xydk`.business_release_detail (id, releaseId, typeName, typeValue, isDelete, createDate) "
					+ " values (:id,:releaseId, :typeName ,:typeValue, :isDelete , :createDate)");
			boolean temp = false;
			for (Entry<String, Object> entry : map.entrySet()) {
				Map<String, Object> inserMap = new HashMap<String, Object>();
				inserMap.put("id", Sequence.nextId());
				inserMap.put("releaseId", id);
				inserMap.put("typeName", entry.getKey());
				inserMap.put("typeValue", entry.getValue());
				inserMap.put("isDelete", 0);
				inserMap.put("createDate", new Date());
				temp = baseDao.executeNamedCommand(ssb.toString(), inserMap);
			}
			return temp;
		}
		return flag;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询精准咨询信息
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getInfoList(Map<String, Object> paramsMap) {
		StringBuffer ssb = new StringBuffer();
		ssb.append("select p.enterClass from `xiaoka-xydk`.preferred_service p where p.isDelete = '0' and p.enterTypeValue = 'jzzx'");
		Map<String, Object> map = baseDao.queryForMap(ssb.toString());
		if(map != null && map.size() > 0){
			String engerClass = (String)map.get("enterClass");
			StringBuffer sb = new StringBuffer();
			sb.append("select t.id,t.phoneNumber,t.nickname,t.userName,t.headPortrait,t.openId,t.enterId,t.cityId,t.schoolId,s.schoolName,ct.cityName from "
					+ " `xiaoka-xydk`.user_basicinfo t LEFT JOIN `xiaoka`.tg_school_info s on s.id=t.schoolId "
					+ " LEFT JOIN `xiaoka`.tg_city ct ON ct.id=t.cityId ");
			if(null !=paramsMap.get("label") && StringUtils.isNotEmpty(paramsMap.get("label").toString())){
				sb.append(" LEFT JOIN `xiaoka-xydk`.schoollabel sl on s.id = sl.schoolId "
						+ "LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = sl.dicDataId ");
		  	}
			sb.append(" where s.isDelete = '0' and ct.isDelete = '0' and t.isDelete = '0' ");
			if(null !=paramsMap.get("userName") && StringUtils.isNotEmpty(paramsMap.get("userName").toString())){
				sb.append(" and t.userName like '%" + paramsMap.get("userName") +"%' ");
		  	}
			if(null !=paramsMap.get("city") && StringUtils.isNotEmpty(paramsMap.get("city").toString())){
				sb.append(" and ct.cityName = '" + paramsMap.get("city") +"' ");
		  	}
			if(null !=paramsMap.get("label") && StringUtils.isNotEmpty(paramsMap.get("label").toString())){
				sb.append(" and dd.dic_name= '"+paramsMap.get("label")+"'");
			}

			if(null !=paramsMap.get("resources") && StringUtils.isNotEmpty(paramsMap.get("resources").toString())){
				sb.append(" and t.id in ("+paramsMap.get("resources")+")");
			}

			sb.append(" GROUP BY t.openId order by t.id ");
			sb.append(" limit " + paramsMap.get("start") + "," + paramsMap.get("pageNum")+" ");
			System.out.println("-------the dressing by screening SQL is--------"+sb.toString());
			return baseDao.queryForList(sb.toString());
		}
		return null;
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询精准投放信息
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getInfoListJztf(Map<String, Object> paramsMap) {
		StringBuffer ssb = new StringBuffer();
		ssb.append("select p.* from `xiaoka-xydk`.preferred_service p where p.isDelete = '0' and p.enterTypeValue = 'jztf'");
		Map<String, Object> map = baseDao.queryForMap(ssb.toString());
		if(map != null && map.size() > 0){
			String engerClass = (String)map.get("enterClass");
			StringBuffer sb = new StringBuffer();
			sb.append("select t.id,t.phoneNumber,t.nickname,t.userName,t.headPortrait,t.openId,t.enterId,t.cityId,t.schoolId,s.schoolName,ct.cityName from "
					+ " `xiaoka-xydk`.user_basicinfo t LEFT JOIN `xiaoka`.tg_school_info s on s.id=t.schoolId "
					+ " LEFT JOIN `xiaoka`.tg_city ct ON ct.id=t.cityId ");
			if(null !=paramsMap.get("label") && StringUtils.isNotEmpty(paramsMap.get("label").toString())){
				sb.append(" LEFT JOIN `xiaoka-xydk`.schoollabel sl on s.id = sl.schoolId "
						+ "LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = sl.dicDataId ");
		  	}
			sb.append(" where s.isDelete = '0' and ct.isDelete = '0' and t.isDelete = '0' and t.enterId in ("+engerClass+")");
			if(null !=paramsMap.get("userName") && StringUtils.isNotEmpty(paramsMap.get("userName").toString())){
				sb.append(" and t.userName like '%" + paramsMap.get("userName") +"%' ");
		  	}
			if(null !=paramsMap.get("city") && StringUtils.isNotEmpty(paramsMap.get("city").toString())){
				sb.append(" and ct.cityName = '" + paramsMap.get("city") +"' ");
		  	}
			if(null !=paramsMap.get("label") && StringUtils.isNotEmpty(paramsMap.get("label").toString())){
				sb.append(" and dd.dic_name= '"+paramsMap.get("label")+"'");
			}
			if(null !=paramsMap.get("resources") && StringUtils.isNotEmpty(paramsMap.get("resources").toString())){
				sb.append(" and t.id in ("+paramsMap.get("resources")+")");
			}
			sb.append(" GROUP BY t.openId order by t.id ");
			System.out.println("-------the dressing by screening SQL is--------"+sb.toString());
			return baseDao.queryForList(sb.toString());
		}
		return null;
	}
	
	/**
	 * <p>根据当前用户输入的子技能 
	 * 查询当前拥有子技能的用户</p>
	 * return String style: id,id,id
	 */
	public String getSonSkillUserId(String skillName){
		
		System.out.println("-------print current pamater is ----------"+skillName);
		
		if(StringUtils.isNotBlank(skillName)){
			
			String sql ="select distinct(us.basicId) from `xiaoka-xydk`.user_skillinfo us where us.skillSonId IN (select t.id from `xiaoka-xydk`.user_skillclass_son t where t.className = '"+skillName.trim()+"');";
			
			System.out.println("----------print curren sql is-----:"+sql);
			
			List<Map<String,Object>> list = baseDao.getJdbcTemplate().queryForList(sql);
			
			//deal current list to String format result is id,id,id
			
			String result = dealListToStr(list);
			
			return result;
		}
		//return blank String
		return "";
	}
	
	
	
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询优选服务
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> preferrService() {
		StringBuffer sb = new StringBuffer();
		sb.append("select  p.id, p.introduce, p.serviceName, p.serviceType, p.salesVolume, p.price, p.imgUrl from "
				+ "`xiaoka-xydk`.preferred_service p where p.isDelete = '0' and p.enterTypeValue = 'jzzx'");
		return baseDao.queryForMap(sb.toString());
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询精准投放
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> preferrServiceJztf() {
		StringBuffer sb = new StringBuffer();
		sb.append("select  p.id, p.serviceName, p.serviceType, p.salesVolume, p.price, p.imgUrl, p.introduce from "
				+ "`xiaoka-xydk`.preferred_service p where p.isDelete = '0' and p.enterTypeValue = 'jztf'");
		return baseDao.queryForMap(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：添加到已选
	 * 创建人： sx
	 * 创建时间： 2017年5月5日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean addPreferr(Map<String, Object> paramsMap) {
		boolean flag = false;
		String obj = (String)paramsMap.get("obj");
		String purOpenId = (String)paramsMap.get("purOpenId");
		if(obj != null && !"".equals(obj)){
			JSONArray json = JSONArray.fromObject(obj);
			for(int i = 0;i < json.size();i++){
				StringBuffer sb = new StringBuffer();
				Map<String, Object> map = new HashMap<String, Object>();
				JSONObject job = json.getJSONObject(i); 
				String openId = job.getString("openId");
				String skillId = job.getString("skillId");
				String enterType = job.getString("enterType");
				String id = Sequence.nextId();
				map.put("id", id);
				map.put("skillId", skillId);
				map.put("openId", openId);
				map.put("purOpenId", purOpenId);
				map.put("enterType", enterType);
				map.put("isPreferred", 1);
				map.put("isDelete", 0);
				map.put("createDate", new Date());
				sb.append("insert into `xiaoka-xydk`.selectskill "
						+ "(id, skillId, openId, purOpenId, enterType, isPreferred, isDelete, createDate) "
						+ "values (:id, :skillId, :openId, :purOpenId, :enterType, :isPreferred, :isDelete, :createDate) ");
				flag = baseDao.executeNamedCommand(sb.toString(), map);
			}
		}
		return flag;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：判断已选中是否有
	 * 创建人： sx
	 * 创建时间： 2017年5月5日
	 * 标记：
	 * @param openId
	 * @param skillId
	 * @param enterType
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> isExits(String openId, String skillId,
			String enterType) {
		StringBuffer sb = new StringBuffer();
		sb.append("select * from `xiaoka-xydk`.selectskill where openId = '"+openId+
				"' and skillId = '"+skillId+"' and enterType = '"+enterType+"' ");
		return baseDao.queryForMap(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：立即咨询生成订单
	 * 创建人： sx
	 * 创建时间： 2017年5月5日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public String addOrder(Map<String, Object> paramsMap) {
		String obj = (String)paramsMap.get("obj");
		String openId = (String)paramsMap.get("openId");
		String pid = Sequence.nextId();
		if(obj != null && !"".equals(obj)){
			boolean temp = false;
			JSONArray json = JSONArray.fromObject(obj);
			for(int i = 0;i < json.size();i++){
				JSONObject job = json.getJSONObject(i);
				if(job != null && job.size() > 0){
					String orderId = Sequence.nextId();
					String sellerOpenId = job.getString("sellerOpenId");
					String schoolId = job.getString("schoolId");
					String cityId = job.getString("cityId");
					String imgUrl = job.getString("imgUrl");
					String serviceName = job.getString("serviceName");
					String serviceType = job.getString("serviceType");
					String price = job.getString("price");
					String phoneNumber = job.getString("phoneNumber");
					String nickname = job.getString("nickname");
					Date createDate = new Date();
					String headPortrait = job.getString("headPortrait");
					String salesVolume = job.getString("salesVolume");
					String introduce = job.getString("introduce");
					Date invalidTime = DateUtil.addHour(createDate,GlobalConstant.HOUR);
					StringBuffer sb = new StringBuffer();
					//订单表
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("orderId", orderId);
					map.put("pid", pid);
					map.put("openId", openId);
					map.put("sellerOpenId", sellerOpenId);
					map.put("schoolId", schoolId);
					map.put("cityId", cityId);
					map.put("payMoney", price);
					map.put("phone", phoneNumber);
					map.put("linkman", nickname);
					map.put("orderStatus", 1);
					map.put("enterType", "jzzx");
					map.put("orderType", 1);
					map.put("createDate", createDate);
					map.put("isDelete", 0);
					map.put("invalidTime", invalidTime);
					sb.append("insert into `xiaoka-xydk`.order "
							+ " (orderId, pid, openId, sellerOpenId, schoolId, cityId, createDate, "
							+ " payMoney, orderStatus, orderType, isDelete, phone, linkman, enterType, invalidTime) "
							+ " values (:orderId, :pid, :openId, :sellerOpenId, :schoolId, :cityId, "
							+ " :createDate, :payMoney, :orderStatus, :orderType, :isDelete, :phone, :linkman, :enterType, :invalidTime) ");
					boolean flag = baseDao.executeNamedCommand(sb.toString(), map);
					if(flag){
						//订单详情表
						Map<String, Object> mapDetail = new HashMap<String, Object>();
						mapDetail.put("imgUrl", imgUrl);
						mapDetail.put("skillName", serviceName);
						mapDetail.put("serviceType", serviceType);
						mapDetail.put("skillPrice", price);
						mapDetail.put("sellerHeadPortrait", headPortrait);
						mapDetail.put("sellNo", salesVolume);
						mapDetail.put("tranNumber", 1);
						mapDetail.put("sellPhone", phoneNumber);
						mapDetail.put("sellerNickname", nickname);
						mapDetail.put("skillDepict", introduce);
						mapDetail.put("dic_name", "次");
						StringBuffer ssb = new StringBuffer();
						ssb.append("insert into `xiaoka-xydk`.order_detail "
								+ " (id, orderId, typeName, typeValue, isDelete, createDate) "
								+ " values (:id, :orderId, :typeName, :typeValue, :isDelete, :createDate)");
						for (Entry<String, Object> entry : mapDetail.entrySet()) {
							Map<String, Object> inserMap = new HashMap<String, Object>();
							inserMap.put("id", Sequence.nextId());
							inserMap.put("orderId", orderId);
							inserMap.put("typeName", entry.getKey());
							inserMap.put("typeValue", entry.getValue());
							inserMap.put("isDelete", 0);
							inserMap.put("createDate", createDate);
							temp = baseDao.executeNamedCommand(ssb.toString(), inserMap);
						}
					}
				}
			}
			if(temp){
				return pid;
			}
		}
		return "";
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年5月5日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public List<OrderDetail> searchOrderDetail(Map<String, Object> map) {
		String sql = "select id,orderId,typeName,typeValue,isDelete,createDate "
				+ "from `xiaoka-xydk`.order_detail where 1=1";
		//当前传入参数为 orderId 和 isDelete 为0 以后有需要再加
		if (map.get("orderId") != null && StringUtils.isNotBlank(map.get("orderId").toString())) {
			sql = sql + " and orderId='"+map.get("orderId").toString()+"'";
		}
		if (map.get("isDelete") != null && StringUtils.isNotBlank(map.get("isDelete").toString())) {
			sql = sql + " and isDelete='"+map.get("isDelete").toString()+"'";
		}
		List<OrderDetail> list = baseDao.getJdbcTemplate().query(sql,new Object[]{},
				ParameterizedBeanPropertyRowMapper.newInstance(OrderDetail.class));
		return list;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：精准投放下单
	 * 创建人： sx
	 * 创建时间： 2017年5月9日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public String addPutOrder(Map<String, Object> paramsMap) {
		String obj = (String)paramsMap.get("obj");
		String openId = (String)paramsMap.get("openId");
		String pid = Sequence.nextId();
		if(obj != null && !"".equals(obj)){
			boolean temp = false;
			JSONArray json = JSONArray.fromObject(obj);
			for(int i = 0;i < json.size();i++){
				JSONObject job = json.getJSONObject(i);
				if(job != null && job.size() > 0 ){
					String orderId = Sequence.nextId();
					String sellerOpenId = job.getString("sellerOpenId");
					String schoolId = job.getString("schoolId");
					String cityId = job.getString("cityId");
					String imgUrl = job.getString("imgUrl");
					String serviceName = job.getString("serviceName");
					String serviceType = job.getString("serviceType");
					String phoneNumber = job.getString("phoneNumber");
					String introduce = (String)paramsMap.get("introduce");
					String headPortrait = job.getString("headPortrait");
					String salesVolume = job.getString("salesVolume");
					String totalPrice = (String)paramsMap.get("totalPrice");
					String price = (String)paramsMap.get("price");
					String name = (String)paramsMap.get("name");
					String phone = (String)paramsMap.get("phone");
					String message = (String)paramsMap.get("note");
					String title = (String)paramsMap.get("title");
					String aboutMe = (String)paramsMap.get("aboutMe");
					String link = (String)paramsMap.get("link");
					String picUrl = (String)paramsMap.get("activtyUrl");
					String timeStr = (String)paramsMap.get("time");
					Date time = DateUtil.strToDate(timeStr+":00", "yyyy-MM-dd HH:mm:ss");
					Date createDate = new Date();
					Date invalidTime = DateUtil.addHour(createDate,GlobalConstant.HOUR);
					StringBuffer sb = new StringBuffer();
					//订单表
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("orderId", orderId);
					map.put("pid", pid);
					map.put("openId", openId);
					map.put("sellerOpenId", sellerOpenId);
					map.put("schoolId", schoolId);
					map.put("cityId", cityId);
					map.put("payMoney", price);
					map.put("linkman", name);
					map.put("phone", phone);
					map.put("message", message);
					map.put("orderStatus", 1);
					map.put("orderType", 1);
					map.put("enterType", "jztf");
					map.put("createDate", createDate);
					map.put("endDate", time);
					map.put("isDelete", 0);
					map.put("dic_name", "次");
					map.put("invalidTime", invalidTime);
					sb.append("insert into `xiaoka-xydk`.order "
							+ " (orderId, pid, openId, sellerOpenId, schoolId, cityId, createDate, payMoney, "
							+ " linkman, phone, message, orderStatus, orderType, isDelete, enterType ,endDate, invalidTime) "
							+ " values (:orderId, :pid, :openId, :sellerOpenId, :schoolId, :cityId, "
							+ " :createDate, :payMoney, :linkman, :phone, :message, :orderStatus, :orderType, :isDelete, :enterType, :endDate, :invalidTime) ");
					boolean flag = baseDao.executeNamedCommand(sb.toString(), map);
					if(flag){
						//订单详情表
						Map<String, Object> mapDetail = new HashMap<String, Object>();
						mapDetail.put("sellerHeadPortrait", headPortrait);
						mapDetail.put("sellerNickname", "批量咨询");
						mapDetail.put("title", title);
						mapDetail.put("textContent", aboutMe);
						mapDetail.put("imgUrl", imgUrl);
						mapDetail.put("skillName", serviceName);
						mapDetail.put("serviceType", serviceType);
						mapDetail.put("skillPrice", price);
						mapDetail.put("aboutMe", aboutMe);
						mapDetail.put("link", link);
						mapDetail.put("sellPhone", phoneNumber);
						mapDetail.put("picUrl", picUrl);
						mapDetail.put("tranNumber", 1);
						mapDetail.put("sellNo", salesVolume);
						mapDetail.put("skillDepict", introduce);
						mapDetail.put("dic_name", "次");
						StringBuffer ssb = new StringBuffer();
						ssb.append("insert into `xiaoka-xydk`.order_detail "
								+ " (id, orderId, typeName, typeValue, isDelete, createDate) "
								+ " values (:id, :orderId, :typeName, :typeValue, :isDelete, :createDate)");
						for (Entry<String, Object> entry : mapDetail.entrySet()) {
							Map<String, Object> inserMap = new HashMap<String, Object>();
							inserMap.put("id", Sequence.nextId());
							inserMap.put("orderId", orderId);
							inserMap.put("typeName", entry.getKey());
							inserMap.put("typeValue", entry.getValue());
							inserMap.put("isDelete", 0);
							inserMap.put("createDate", createDate);
							temp = baseDao.executeNamedCommand(ssb.toString(), inserMap);
						}
					}
				}
			}
			if(temp){
				return pid;
			}
		}
		return null;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据pid获取订单(取第一个)
	 * 创建人： sx
	 * 创建时间： 2017年5月10日
	 * 标记：
	 * @param pid
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findOrderByPid(String pid) {
		Map<String, Object> map = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select orderId, pid, openId, sellerOpenId, schoolId, cityId, createDate, "
				+ " payMoney, payTime, orderStatus from `xiaoka-xydk`.order where pid = '"+pid+"'");
		List<Map<String, Object>> list = baseDao.queryForList(sb.toString());
		if(list != null && list.size() >0 ){
			map = list.get(0);
			return map;
		}
		return null;
	}
	/**
	 * 分组查询当前所有订单的订单状态
	 */
	public List<Map<String,Object>> getPublishOrder(String pid){
		
		String sql = "select t.openId,t.orderId,t.orderStatus,t.pid,t.isDelete from `xiaoka-xydk`.order t where t.pid='"+pid+"' AND t.isDelete='0' GROUP BY t.orderStatus ;";
		
		List<Map<String, Object>> list = baseDao.queryForList(sql);
		
		return list;
		
	}
	/**
	 * 查询当前订单状态下的总钱数
	 */
	public String getPubSumMoney(String pid){
		
		String sql = "select SUM(t.payMoney) AS AllMoney  from `xiaoka-xydk`.order t where t.pid='"+pid+"'";
		
		Map<String,Object> obj = baseDao.queryForMap(sql);
		
		String allMoney = obj.get("AllMoney").toString();
		
		return allMoney;
		
	}
	/**
	 * 根据订单的pid更新订单的状态
	 */
	public boolean updateOrderById(String pid, String outTradeNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pid", pid);
		map.put("partnerTradeNo", outTradeNo);
		map.put("orderStatus", 2);
		map.put("payTime", new Date());
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.order set partnerTradeNo = :partnerTradeNo, orderStatus = :orderStatus, payTime = :payTime where pid = :pid");
		return baseDao.executeNamedCommand(sb.toString(), map);
	}
	
	/**
	 * 根据当前的pid更新订单的状态
	 */
	public boolean updateOrderByPid(Map<String,Object> map){
		
		if(map.containsKey("pid")){
		
			String sql = "update `xiaoka-xydk`.order set ";
		
			if(map.containsKey("orderStatus")){
				
				sql +="orderStatus = :orderStatus ,";
			}
			if(map.containsKey("isDelete")){//更改删除订单状态
				
				sql +="isDelete = :isDelete ,";
			}
			sql.substring(0, sql.length()-1);//去掉当前的，号
			
			sql +="where pid = :pid";
			
			return baseDao.executeNamedCommand(sql, map);
		}
		
		return false;
	}
	
	/**
	 * 根据父ID查询子技能信息
	 */
	public List<Map<String,Object>> getSonSkillMessage(Map<String,Object> map){
		
		if(map.containsKey("id")){
			
			String sql = "SELECT s.id,s.className,s.classValue FROM `xiaoka-xydk`.user_skillclass_son s "
					+ "WHERE s.fatherId = '"+map.get("id")+"' AND s.`status` = '1' and isDelete = 0;";
			
			return  baseDao.getJdbcTemplate().queryForList(sql);
		}
		return null;
	}
	
	/**
	 * 处理当前list转换为String
	 * id,id,id
	 * @param list
	 * @return
	 */
	public String dealListToStr(List<Map<String,Object>> list){
		
		String result = "";
		
		if(!list.isEmpty()){

		for (Map<String, Object> map : list) {
			
			if(StringUtils.isNotBlank(map.get("basicId").toString())){
				
				result += map.get("basicId")+",";
			}
			
		}
		result = result.substring(0, result.length()-1);
		return result;
		
	}
		return null;
	}
	
	
	/**
	 * 根据传入的城市进行查询符合条件的学校 
	 */
	public List<Map<String,Object>> getSchoolLeave(String cityId){
		
		String sql ="select DISTINCT(dd.dic_name) as `level`  from `xiaoka`.tg_school_info t LEFT JOIN `xiaoka-xydk`.schoollabel s on t.id = s.schoolId "
				+ "LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = s.dicDataId  where t.isDelete = '0' and dd.isDelete = '0' and t.cityId in ("+cityId+")";
		
		return baseDao.getJdbcTemplate().queryForList(sql);
	}
	
	/**
	 * 获取当前指定城市下指定等级的学校
	 */
	public List<Map<String,Object>> getSchoolByCityAndLevel(String cityId,String level){
		
		String sql ="select t.id,t.schoolName  from `xiaoka`.tg_school_info t LEFT JOIN `xiaoka-xydk`.schoollabel s on t.id = s.schoolId "
				+ "LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = s.dicDataId  "
				+ "where t.isDelete = '0' and dd.isDelete = '0' and t.cityId in ("+cityId+") and dd.dic_name LIKE '%"+level+"%'";
		
		return baseDao.getJdbcTemplate().queryForList(sql);
		
	}
	
}

