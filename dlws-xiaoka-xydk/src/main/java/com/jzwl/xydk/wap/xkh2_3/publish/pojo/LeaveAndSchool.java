/**
 * 学校等级和学校
 */
package com.jzwl.xydk.wap.xkh2_3.publish.pojo;

import java.util.List;
import java.util.Map;

import com.jzwl.xydk.manager.school.schoollabel.pojo.SchoolInfo;

/**
 * @author Administrator
 *
 */
public class LeaveAndSchool {

	/**
	 * 学院等级
	 */
	private String leave;
	/**
	 * 学校的信息
	 */
	private List<Map<String,Object>> school;
	
	
	public String getLeave() {
		return leave;
	}


	public void setLeave(String leave) {
		this.leave = leave;
	}



	public List<Map<String, Object>> getSchool() {
		return school;
	}


	public void setSchool(List<Map<String, Object>> school) {
		this.school = school;
	}


	public LeaveAndSchool() {
		
	}

}
