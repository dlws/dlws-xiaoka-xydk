package com.jzwl.xydk.wap.xkh2_2.payment.sqdv.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.service.SkillOrderService;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.pojo.FeedBack;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.service.FeedBackService;
import com.jzwl.xydk.wap.xkh2_2.payment.sqdv.service.SqdvOrderService;

@Controller("sqdvOrder")
@RequestMapping("/sqdvOrder")
public class sqdvOrderController extends BaseWeixinController {
	@Autowired
	private SqdvOrderService sqdvOrderService;
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private PreferredServiceService preferredServiceService;
	@Autowired
	private SkillOrderService skillOrderService;
	@Autowired
	private SendMessageService sendMessage; //调用模板消息使用
	@Autowired
	private ChatMessageService chatMessageService; 
	@Autowired
	private FeedBackService feedbackservice;
	
	/**
	 * 结算 dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/subSqdvOrder")
	public String AddSelected(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		createParameterMap(request);
		 String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
//			String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8g9B";
		paramsMap.put("openId", openId);
		String orderId = sqdvOrderService.addOrder(paramsMap);
		//系统消息
		String nowOpenId = String.valueOf(paramsMap.get("sellerOpenId"));
		paramsMap.put("nowOpenId", nowOpenId);
		paramsMap.put("orderId", orderId);
		paramsMap.put("orderStatus", 1);
		chatMessageService.insertUpdateSysMeg(paramsMap);
		chatMessageService.insertBuyMeg(paramsMap);//买家端
		//社群大V消息推送增加模板消息推送
		sendMessage.submissionOrder(orderId, openId);
		
		
		return "redirect:/sqdvOrder/sqdvOrderDetail.html?orderId="+orderId+"";
	}
	//取消订单
	@RequestMapping(value = "/cancelSqdvOrder")
	public @ResponseBody boolean cancelOrder(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		boolean flag = false;
		try {
			createParameterMap(request);
			flag = sqdvOrderService.cancelOrder(paramsMap); //订单详情
			if (flag) {
				return flag;
			} else {
				model.addAttribute("msg", "取消成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "取消失败");
			return flag;
		}
		return flag;

	}
	/**
	 * 社群大v订单详情 dxf
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/sqdvOrderDetail")
	public String sqdvOrderDetail(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		
		boolean isMySelf = false;
		boolean haveFeed = true;
		try {
			createParameterMap(request);
			
			List<FeedBack> feed = feedbackservice.searchFeeaBack(paramsMap);
			
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
//			String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8g9B";
			paramsMap.put("openId", openId);
			String orderId = String.valueOf(paramsMap.get("orderId"));
			if (!"".equals(orderId)) {
				
				Map<String, Object> base = new HashMap<String, Object>(); //基本信息
				
				paramsMap.put("orderId", orderId);
				List<Map<String, Object>> settleList = sqdvOrderService.querySettleList(paramsMap);//查询子表详细信息
				model.addAttribute("settleList",settleList);
				
				
				Map<String, Object> skillInfo = new HashMap<String, Object>();
				String skillId ="";//技能id
				for (int i = 0; i < settleList.size(); i++) {
					if (settleList.size() > 0) {
						if ("payMoney".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String payMoney = String.valueOf(settleList.get(i).get("typeValue"));//付款金额
							skillInfo.put("payMoney", payMoney);
						} 
						if ("skillId".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							skillId = String.valueOf(settleList.get(i).get("typeValue"));//技能id
							Map<String, Object> selMap = skillOrderService.getSkillInfoById(skillId);
							String sellNo = String.valueOf(selMap.get("sellNo"));
							skillInfo.put("skillId", skillId);
							skillInfo.put("sellNo", sellNo);
						} 
						if ("skillName".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String skillName = String.valueOf(settleList.get(i).get("typeValue"));//技能名称
							skillInfo.put("skillName", skillName);
						} 
						if ("serviceType".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String serviceType = String.valueOf(settleList.get(i).get("typeValue"));//服务类型
							skillInfo.put("serviceType", serviceType);
						} 
						if ("skillPrice".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String skillPrice =  String.valueOf(settleList.get(i).get("typeValue"));//单价
							skillInfo.put("skillPrice", skillPrice);
						}
						if ("skillDepict".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String skillDepict =  String.valueOf(settleList.get(i).get("typeValue"));//技能描述
							skillInfo.put("skillDepict", skillDepict);
						}
						if ("imgUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String imageUrl =  String.valueOf(settleList.get(i).get("typeValue"));//技能图片更改名称
							skillInfo.put("imageUrl", imageUrl);//方便前端展示
						}
						if ("tranNumber".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String tranNumber =  String.valueOf(settleList.get(i).get("typeValue"));//购买数量
							skillInfo.put("tranNumber", tranNumber);
						}
						if ("accurateVal".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String accurateVal =  String.valueOf(settleList.get(i).get("typeValue"));//投放版位
							skillInfo.put("accurateVal", accurateVal);
						}
						if ("sellerHeadPortrait".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String sellerHeadPortrait =  String.valueOf(settleList.get(i).get("typeValue"));//卖家头像
							base.put("headPortrait", sellerHeadPortrait);
						}
						if ("sellerNickname".equals(String.valueOf(settleList.get(i).get("typeName")))) {
							String sellerNickname =  String.valueOf(settleList.get(i).get("typeValue"));//卖家昵称
							base.put("nickname", sellerNickname);
						}
					}
				}
				
				
				model.addAttribute("skillInfo", skillInfo);// 服务详情
				model.addAttribute("baseInfo", base);//卖家基本信息
				Map<String,Object> orderMap = sqdvOrderService.getOrderById(orderId); //订单详情
				
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟  
				Date now = new Date();
				String invalidTime = String.valueOf(orderMap.get("invalidTime"));
			    java.util.Date chu=sdf.parse(invalidTime); 
			    long timmer = chu.getTime();
			    String timeDiffer = subtracDateS(chu, now); 
			    if("0".equals(timeDiffer)&&"1".equals(paramsMap.get("orderStatus"))){
			    	paramsMap.put("orderStatus", 9);
			    	skillOrderService.UpOrderStatus(paramsMap);
			    }else if("0".equals(timeDiffer)&&"3".equals(paramsMap.get("orderStatus"))){
			    	paramsMap.put("orderStatus", 4);
			    	skillOrderService.UpOrderStatus(paramsMap);
			    }
			    orderMap.put("timeDiffer", timeDiffer);
			    long daoDate = chu.getTime() - now.getTime();   
			    orderMap.put("daoDate", daoDate);
				   
			    if(openId.equals(orderMap.get("openId"))){
			    	
			    	isMySelf = true;
			    }
			    FeedBack feedback = new FeedBack();
			    //提交反馈  false 表示没有 true 表示有
				if(feed.isEmpty()||feed.size()==0){
					haveFeed = false;
				}else{
					feedback = feed.get(0);
				}
			    model.addAttribute("feedBackBean", feedback);
				model.addAttribute("feedBack",haveFeed);//默认选择为
			    model.addAttribute("isMySelf",isMySelf);
				model.addAttribute("orderMap", orderMap);//订单详细信息
				model.addAttribute("timmer", timmer);//订单详细信息
				return "wap/xkh_version_2_2/orderdetaile/sqdv_orderDetail";
			} else {
				model.addAttribute("msg", "添加失败");
				return "/error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "/error";
		}
	}
	
	/**
	 * @param num1 被减数 结束时间
	 * @param num2 减数 当前时间
	 * @return 返回String 类型
	 */
	public static String subtracDateS(Date d1,Date d2){
		
		String timer = "";
		long date = d1.getTime() - d2.getTime();   
	    long day = date / (1000 * 60 * 60 * 24);    
	    long hour = (date / (1000 * 60 * 60) - day * 24);    
	    long min = ((date / (60 * 1000)) - day * 24 * 60 - hour * 60);  
	    long s = (date/1000 - day*24*60*60 - hour*60*60 - min*60);  
	    if(day<=0){
	    	timer=""+hour+"小时"+min+"分"+s+"秒";
	    }else{
	    	timer=""+day+"天"+hour+"小时"+min+"分"+s+"秒";
	    }
	    if(date<=0){
	    	timer="0";
	    }
	    
	    return timer;
	}
}
