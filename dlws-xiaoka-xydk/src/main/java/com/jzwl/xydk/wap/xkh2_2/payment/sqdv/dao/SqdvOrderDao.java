package com.jzwl.xydk.wap.xkh2_2.payment.sqdv.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.id.Sequence;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("sqdvOrderDao")
public class SqdvOrderDao {
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	/**
	 * 删除订单
	 * @param map
	 * @return
	 */
	public boolean deleteOrder(Map<String, Object> map) {

		String sql = "delete from  `xiaoka-xydk`.selectskill  where id=:id";

		return baseDAO.executeNamedCommand(sql, map);

	}

	//添加订单
	public String addOrder(Map<String, Object> map) {
		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		String orerId = Sequence.nextId();
		map.put("id",orerId);
		map.put("createDate", date);
		map.put("isDelete", 0);
		Integer day = 1;
		Date nowDate = new Date();
		nowDate = DateUtil.addHour(date, GlobalConstant.HOUR);
		map.put("nowDate", nowDate);
		double  payMoney = 0;
		if(map.get("payMoney")!=null){
			payMoney = Double.parseDouble(String.valueOf(map.get("payMoney")));
		}
		double serviceMoney = 0;
		if(map.get("serviceRatio")!=null&&!"".equals(map.get("serviceRatio"))){
			serviceMoney = Double.parseDouble(String.valueOf(map.get("serviceRatio")))*payMoney;
		}
			
		map.put("serviceMoney", serviceMoney);
		
		double diplomatMoney = 0;
		if(map.get("diplomatRatio")!=null&&!"".equals(map.get("diplomatRatio"))){
			diplomatMoney = Double.parseDouble(String.valueOf(map.get("diplomatRatio")))*payMoney;
		}
		map.put("diplomatMoney", diplomatMoney);
		
		double platformMoney =0; 
		if(map.get("platformMoney")!=null&&!"".equals(map.get("platformMoney"))){
			platformMoney = Double.parseDouble(String.valueOf(map.get("platformRatio")))*payMoney;
		}
		map.put("platformMoney", platformMoney);
		String sql = "insert into `xiaoka-xydk`.order"
				+ " (orderId,openId,sellerOpenId,schoolId,serviceRatio,diplomatRatio,platformRatio,cityId,createDate,payMoney,payTime,serviceMoney,diplomatMoney,platformMoney,realityMoney,orderStatus,"
				+ "payway,linkman,phone,message,endDate,invalidTime,isDelete,enterType) "
				+ " values "
				+ " (:id,:openId,:sellerOpenId,:schoolId,:serviceRatio,:diplomatRatio,:platformRatio,:cityId,:createDate,:payMoney,:createDate,:serviceMoney,:diplomatMoney,:platformMoney,:payMoney,1,"
				+ "0,:linkman,:phone,:message,:endDate,:nowDate,:isDelete,:enterType)";

		boolean flag = baseDAO.executeNamedCommand(sql, map);
		 
		 String DataSql = "insert into `xiaoka-xydk`.order_detail(id,orderId,typeName,typeValue,isDelete,createDate)"
					+ "values" + "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
			boolean temp = false;
			Map<String, Object> daMap = new HashMap<String, Object>();
			daMap.put("payMoney", map.get("payMoney"));
			daMap.put("skillId", map.get("skillId"));
			daMap.put("skillName", map.get("skillName"));
			daMap.put("skillDepict", map.get("skillDepict"));//技能描述
			daMap.put("imgUrl", map.get("imageUrl"));//统一存入 技能图片
			daMap.put("accurateVal", map.get("accurateVal"));//投放版位
			
			daMap.put("dic_name", map.get("dic_name"));//单位
			daMap.put("skillPrice", map.get("skillPrice"));//单价
			daMap.put("sellerHeadPortrait", map.get("sellerHeadPortrait"));//头像
			daMap.put("sellerNickname", map.get("sellerNickname"));//昵称
			daMap.put("serviceType", map.get("serviceType"));//服务类型
			
			
			daMap.put("tranNumber", map.get("tranNumber"));//购买数量
			daMap.put("sellPhone", map.get("sellPhone"));//卖家电话
			
			if (flag) {
				for (Entry<String, Object> entry : daMap.entrySet()) {
					Map<String, Object> inserMap = new HashMap<String, Object>();
					inserMap.put("id", Sequence.nextId());
					inserMap.put("pid", orerId);
					inserMap.put("typeName", entry.getKey());
					inserMap.put("typeValue", entry.getValue());
					inserMap.put("isDelete", 0);
					inserMap.put("createDate", new Date());
					temp = baseDAO.executeNamedCommand(DataSql, inserMap);
				}
				String delSql = "DELETE FROM  `xiaoka-xydk`.selectskill WHERE purOpenId='"+map.get("openId")+"' AND skillId='"+map.get("skillId")+"' "
						+ " and openId='"+map.get("sellerOpenId")+"'";
				baseDAO.executeNamedCommand(delSql, map);
			}
			return orerId;
	}
	//查询用户基本信息
	public Map getBaseInfo(Map<String,Object>map){
		String sql = "select * from `xiaoka-xydk`.user_basicinfo bas"
				+ " LEFT JOIN `xiaoka-xydk`.entertype type ON"
				+ " bas.enterId=type.id"
				+ " where bas.id='"+map.get("basiId")+"'";
		return baseDAO.queryForMap(sql, map);
	}
	
	//订单详情
	public Map getOrderById(String orderId){
		 String sql = "SELECT * FROM `xiaoka-xydk`.order WHERE orderId='"+orderId+"'";
		return baseDAO.queryForMap(sql);
	}
	
	/**
	 * dxf
	 * 查询子表信息
	 * @param map
	 * @return List
	 */
	public List querySettleList(Map<String, Object> map) {

		String sql = "SELECT id,orderId,typeName,typeValue,isDelete,createDate FROM `xiaoka-xydk`.order_detail"
				+ " WHERE orderId='" + map.get("orderId") + "' and isDelete=0";
		return baseDAO.queryForList(sql);
	}
	
	//取消订单
	public boolean cancelOrder(Map<String,Object>map){
		String sql = "update `xiaoka-xydk`.order set orderStatus=9 where orderId='"+map.get("orderId")+"'";
		return baseDAO.executeNamedCommand(sql, map);
	}
}
