package com.jzwl.xydk.wap.business.home.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.common.utils.Util;


@Controller
@RequestMapping("/prompt")
public class PromptComtroller {

	
	/**
	 * cfz
	 * 提示类用来进行提示用户
	 * 2017年3月1日19:07:10
	 * @return
	 */
	@RequestMapping(value = "/prompt")
	public String turnEnter() {
		return "/wap/xkh_version_2/cuo";
	}
	
	
}
