package com.jzwl.xydk.wap.xkh2_2.payment.delivery.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.id.Sequence;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("orderDao")
public class SkillOrderDao {
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库

	//加入已选
	public boolean joinSelected(Map<String, Object> map) {
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		map.put("id", Sequence.nextId());
		map.put("isRead", 1);//已查看：0，未查看：1
		Date date = new Date();
		map.put("date", date);
		String sql = "select count(1) num, id from `xiaoka-xydk`.selectskill where purOpenId='" + map.get("purOpenId") + "'" + " and skillId='"
				+ map.get("skillId") + "' and openId='"+map.get("openId_sel")+"'";
		String enterType = map.get("enterType").toString();
		//入驻类型为空判断  空则赋值gxst 非空 原值
		if (null == enterType || "" == enterType) {
			map.put("enterType", "gxst");
		} else {
			map.put("enterType", enterType);
		}
		String addSql = "insert into `xiaoka-xydk`.selectskill" + " (id,skillId,openId,purOpenId,enterType,buyNum,isPreferred,isDelete,createDate,isRead ) "
				+ " values " + " (:id,:skillId,:openId_sel,:purOpenId,:enterType,1,:isPreferred,0,:date,:isRead)";

		Map openMap = baseDAO.queryForMap(sql);
		String count = String.valueOf(openMap.get("num"));
		if ("0".equals(count)) {
			baseDAO.executeNamedCommand(addSql, map);
			return true;
		}else{
			openMap.put("createDate", new Date());
			updateSelectskillDate(openMap);//执行更新当前的时间操作
				
		}
		return true;
	}

	/**
	 * dxf
	 * 已选订单列表 个人服务
	 * @param map
	 * @return List
	 */
	public List<Map<String, Object>> querySelected(Map<String, Object> map) {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from (");
		sql.append("(SELECT skld.id seledId,skld.isPreferred,skld.createDate as createDate,skld.buyNum as buyNum,skld.enterType as enterType,skl.id skillId,skl.skillName,skl.skillPrice price,skl.skillDepict,");
		sql.append(" skl.serviceType,bif.id basiId,bif.userName,bif.nickname,bif.headPortrait,skl.viewNum salesVolume,");
		sql.append(" simg.imgUrl,etp.typeName,etp.typeValue,etp.id enterId,skld.openId,serviceRatio,diplomatRatio,platformRatio");
		sql.append(" FROM `xiaoka-xydk`.selectskill skld INNER JOIN `xiaoka-xydk`.user_skillinfo skl");
		sql.append(" ON skld.skillId=skl.id INNER JOIN `xiaoka-xydk`.user_basicinfo bif ON bif.id=skl.basicId");
		sql.append(" LEFT JOIN `xiaoka-xydk`.user_skill_image simg ON skl.id=simg.skillId");
		sql.append(" LEFT JOIN `xiaoka-xydk`.entertype etp ON bif.enterId=etp.id");
		sql.append(" LEFT JOIN `xiaoka-xydk`.user_skillclass_son sln ON sln.id=skl.skillSonId");
		sql.append(" WHERE skld.isDelete=0 AND skld.purOpenId='" + map.get("purOpenId") + "' GROUP BY skl.id)  ");
		sql.append(" UNION ALL");
		sql.append("(SELECT skld.id seledId,skld.isPreferred,skld.createDate as createDate,skld.buyNum as buyNum,skld.enterType as enterType,prs.id skillId,prs.serviceName skillName,prs.price,prs.introduce skillDepict,");
		sql.append("  prs.serviceType,bif.id basiId,bif.userName,bif.nickname,bif.headPortrait,salesVolume,");
		sql.append("  imgUrl,etp.typeName,prs.enterTypeValue typeValue,etp.id enterId,skld.openId,serviceRatio,diplomatRatio,platformRatio");
		sql.append("  FROM `xiaoka-xydk`.selectskill skld INNER JOIN `xiaoka-xydk`.user_basicinfo bif ON ");
		sql.append(" skld.openId=bif.openId INNER JOIN `xiaoka-xydk`.preferred_service prs ON skld.skillId=prs.id");
		sql.append(" LEFT JOIN `xiaoka-xydk`.entertype etp ON bif.enterId=etp.id");
		//bif.openId='"+map.get("openId")+"'AND
		sql.append(" WHERE skld.isPreferred='1' AND skld.isDelete=0 AND skld.purOpenId='" + map.get("purOpenId") + "'");
		sql.append(" GROUP BY skld.id)");
		sql.append(" ) t ORDER BY  t.createDate DESC");
		List po = baseDAO.queryForList(sql.toString());

		return po;
	}

	/**
	 * 删除订单
	 * @param map
	 * @return
	 */
	public boolean deleteOrder(Map<String, Object> map) {

		String sql = "delete from  `xiaoka-xydk`.selectskill  where id=:id";

		return baseDAO.executeNamedCommand(sql, map);

	}

	//添加订单
	public String addOrder(Map<String, Object> map) {
		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		String orerId = Sequence.nextId();
		map.put("id", orerId);
		map.put("createDate", date);
		map.put("isDelete", 0);
		map.put("startDate", date);
		Date nowDate = new Date();
		nowDate = DateUtil.addHour(nowDate,GlobalConstant.HOUR);
		map.put("nowDate", nowDate);
		double payMoney = Double.parseDouble(String.valueOf(map.get("payMoney")));
		double serviceMoney = Double.parseDouble(String.valueOf(map.get("serviceRatio"))) * payMoney;
		map.put("serviceMoney", serviceMoney);
		double diplomatMoney = Double.parseDouble(String.valueOf(map.get("diplomatRatio"))) * payMoney;
		map.put("diplomatMoney", diplomatMoney);
		double platformMoney = Double.parseDouble(String.valueOf(map.get("platformRatio"))) * payMoney;
		map.put("platformMoney", platformMoney);
		String sql = "insert into `xiaoka-xydk`.order"
				+ " (orderId,openId,sellerOpenId,schoolId,cityId,createDate,payMoney,serviceMoney,diplomatMoney,"
				+ "platformMoney,serviceRatio,diplomatRatio,platformRatio,realityMoney,orderStatus,"
				+ "payway,linkman,phone,message,startDate,invalidTime,endDate,isDelete,enterType) " + " values "
				+ " (:id,:openId,:sellerOpenId,:schoolId,:cityId,:createDate,:payMoney,:serviceMoney,:diplomatMoney,"
				+ ":platformMoney,:serviceRatio,:diplomatRatio,:platformRatio,:payMoney,1,"
				+ "0,:linkman,:phone,:message,:startDate,:nowDate,:endDate,:isDelete,:enterType)";

		boolean flag = baseDAO.executeNamedCommand(sql, map);

		String DataSql = "insert into `xiaoka-xydk`.order_detail(id,orderId,typeName,typeValue,isDelete,createDate)" + "values"
				+ "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
		boolean temp = false;
		
		Map<String, Object> daMap = new HashMap<String, Object>();
		daMap.put("title", map.get("title"));
		daMap.put("textContent", map.get("textContent"));
		daMap.put("linke", map.get("linke"));
		daMap.put("payMoney", map.get("payMoney"));
		daMap.put("skillId", map.get("skillId"));
		daMap.put("skillName", map.get("serviceName"));
		daMap.put("skillDepict", map.get("skillDepict"));//技能描述
		daMap.put("imgUrl", map.get("imageUrl"));//技能图片  更改当前存入图片名称便于展示
		daMap.put("picUrl", map.get("picUrl"));
		daMap.put("dic_name", map.get("dic_name"));//单位
		daMap.put("skillPrice", map.get("skillPrice"));//单价
		daMap.put("sellerHeadPortrait", map.get("sellerHeadPortrait"));//头像
		daMap.put("sellerNickname", map.get("sellerNickname"));//昵称
		daMap.put("serviceType", map.get("serviceType"));//服务类型
		
		daMap.put("tranNumber", map.get("tranNumber"));//购买数量
		daMap.put("sellPhone", map.get("sellPhone"));//卖家电话
		if (flag) {
			for (Entry<String, Object> entry : daMap.entrySet()) {
				Map<String, Object> inserMap = new HashMap<String, Object>();
				inserMap.put("id", Sequence.nextId());
				inserMap.put("pid", orerId);
				inserMap.put("typeName", entry.getKey());
				inserMap.put("typeValue", entry.getValue());
				inserMap.put("isDelete", 0);
				inserMap.put("createDate", new Date());
				temp = baseDAO.executeNamedCommand(DataSql, inserMap);
			}
			
			String delSql = "DELETE FROM  `xiaoka-xydk`.selectskill WHERE purOpenId='"+map.get("openId")+"' AND skillId='"+map.get("skillId")+"'"
			+ " and openId='"+map.get("sellerOpenId")+"'";
			baseDAO.executeNamedCommand(delSql, map);
		}
		return orerId;
	}

	//查询用户基本信息
	public Map getBaseInfo(Map<String, Object> map) {
		String sql = "select * from `xiaoka-xydk`.user_basicinfo bas" + " LEFT JOIN `xiaoka-xydk`.entertype type ON" + " bas.enterId=type.id"
				+ " where bas.id='" + map.get("basiId") + "'";
		return baseDAO.queryForMap(sql, map);
	}
	
	public Map<String, Object> getEntertype(Map<String, Object> map) {
		String sql = "select * from `xiaoka-xydk`.entertype t where t.typeValue='gxst'";
		return baseDAO.queryForMap(sql, map);
	}
	
	/**
	 * 删除当前已选中的问题数据
	 * 根据已选中的id值进行删除
	 */
	public boolean deleteSelect(List<String> condition){
		
		if(!condition.isEmpty()){
			String sql = "delete from `xiaoka-xydk`.selectskill where id in (";
			String sqlCondition = "";
			for (int i = 0; i < condition.size(); i++) {
				sqlCondition +=condition.get(i);
			}
			sql += sqlCondition+",";
			//去除当前的逗号
			sql = sql.substring(0, sql.length()-1);
			sql = sql +");";
			return baseDAO.executeCommand(sql,new Object[] {});
		}
		return false;
	}
	

	/**
	 * dxf
	 * 查询子表信息
	 * @param map
	 * @return List
	 */
	public List querySettleList(Map<String, Object> map) {

		String sql = "SELECT id,orderId,typeName,typeValue,isDelete,createDate FROM `xiaoka-xydk`.order_detail" + " WHERE orderId='"
				+ map.get("orderId") + "' and isDelete=0";
		return baseDAO.queryForList(sql);
	}

	//回显技能详细信息
	public Map getSkillInfoById(String skillId) {
		String sql = "select * from `xiaoka-xydk`.user_skillinfo ski " + " LEFT JOIN `xiaoka-xydk`.user_skillclass_son sln ON sln.id=ski.skillSonId"
				+ " LEFT JOIN `xiaoka-xydk`.user_skill_image skImg ON ski.id=skImg.skillId" + "  where ski.id='" + skillId + "' GROUP BY ski.id";
		
		Map<String,Object> map =  baseDAO.queryForMap(sql);
		if (null == map.get("sellNo")) {
			map.put("sellNo", 0);
		} else {
			map.put("sellNo", map.get("sellNo").toString());
		}
		return map;
	}

	//添加订单学校用
	public String addOrderSch(Map<String, Object> map) {
		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		String orerId = Sequence.nextId();
		map.put("id", orerId);
		map.put("createDate", date);
		map.put("isDelete", 0);
		//map.put("startDate", date);
		double payMoney = Double.parseDouble(String.valueOf(map.get("payMoney")));
		double serviceMoney = Double.parseDouble(String.valueOf(map.get("serviceRatio"))) * payMoney;
		map.put("serviceMoney", serviceMoney);
		double diplomatMoney = Double.parseDouble(String.valueOf(map.get("diplomatRatio"))) * payMoney;
		map.put("diplomatMoney", diplomatMoney);
		double platformMoney = Double.parseDouble(String.valueOf(map.get("platformRatio"))) * payMoney;
		map.put("platformMoney", platformMoney);
		String sql = "insert into `xiaoka-xydk`.order"
				+ " (orderId,openId,sellerOpenId,schoolId,cityId,createDate,payMoney,serviceMoney,diplomatMoney,"
				+ "platformMoney,serviceRatio,diplomatRatio,platformRatio,realityMoney,orderStatus,"
				+ "payway,invalidTime,linkman,phone,message,startDate,endDate,isDelete,enterType) " + " values "
				+ " (:id,:openId,:sellerOpenId,:schoolId,:cityId,:createDate,:payMoney,:serviceMoney,:diplomatMoney,"
				+ ":platformMoney,:serviceRatio,:diplomatRatio,:platformRatio,:payMoney,1,"
				+ "0,:invalidTime,:linkman,:phone,:message,:startDate,:endDate,:isDelete,:enterType)";

			boolean flag = baseDAO.executeNamedCommand(sql, map);
			 
			String delSql = "DELETE FROM  `xiaoka-xydk`.selectskill WHERE purOpenId='"+map.get("openId")+"' AND skillId='"+map.get("skillId")+"'"
					+ " and openId='"+map.get("sellerOpenId")+"'";
					baseDAO.executeNamedCommand(delSql, map);
					
			 String DataSql = "insert into `xiaoka-xydk`.order_detail(id,orderId,typeName,typeValue,isDelete,createDate)"
						+ "values" + "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
				boolean temp = false;
				//存入当前的交易数量
				Map<String, Object> daMap = new HashMap<String, Object>();
				daMap.put("tranRecord", map.get("tranRecord"));//存入交易记录
				daMap.put("begin", map.get("begin"));//存入商品种类
				daMap.put("end", map.get("end"));//存入商品种类
				daMap.put("sellerHeadPortrait", map.get("sellerHeadPortrait"));//存入交易需求
				daMap.put("sellerNickname", map.get("sellerNickname"));//存入交易需求
				daMap.put("dic_name", map.get("dic_name"));//存入交易需求
				daMap.put("tranNumber", map.get("tranNumber"));//存入交易需求
				daMap.put("sellPhone", map.get("sellPhone"));//存入交易需求
				daMap.put("tranDemand", map.get("tranDemand"));//存入交易需求
				daMap.put("singPrice", map.get("singPrice"));//存入交易需求
				daMap.put("skillDepict", map.get("skillDepict"));//存入交易需求
				daMap.put("serviceType", map.get("serviceType"));//存入交易需求
				daMap.put("skillName", map.get("skillName"));//存入交易需求
				daMap.put("imgUrl", map.get("imgUrl"));//存入交易需求
				daMap.put("tranNumber", map.get("tranNumber"));//存入交易数量
				daMap.put("skillPrice", map.get("skillPrice"));//存入交易单价
				daMap.put("skillId", map.get("skillId"));//存入商品种类
				
				if (flag) {
					for (Entry<String, Object> entry : daMap.entrySet()) {
						Map<String, Object> inserMap = new HashMap<String, Object>();
						inserMap.put("id", Sequence.nextId());
						inserMap.put("pid", orerId);
						inserMap.put("typeName", entry.getKey());
						inserMap.put("typeValue", entry.getValue());
						inserMap.put("isDelete", 0);
						inserMap.put("createDate", new Date());
						temp = baseDAO.executeNamedCommand(DataSql, inserMap);
					}
					if(temp){
						return orerId;
					}
				}
				return null;
	}

	//获取当前订单的详细信息
	public List<Map<String, Object>> getOrderDetail(String orderId) {
		String sql = "select id,orderId,typeName,typeValue,isDelete from `xiaoka-xydk`.order_detail where isDelete=0 and orderId='" + orderId + "'";
		return baseDAO.getJdbcTemplate().queryForList(sql);
	}

	//订单详情
	public Map getOrderById(String orderId) {
		String sql = "SELECT * FROM `xiaoka-xydk`.order WHERE orderId='" + orderId + "'";
		return baseDAO.queryForMap(sql);
	}

	//改变订单状态
	public boolean UpOrderStatus(Map<String, Object> map) {
		
		String sql = "update `xiaoka-xydk`.order set orderStatus=:orderStatus where orderId='" + map.get("orderId") + "'";
		//增加当前付款的支付时间
		/*if(map.containsKey("payTime")&&map.get("payTime")!=null){
			
			sql = sql + ", orderStatus=:payTime  ,";
		}
		sql.substring(0, sql.length()-1);//去逗号
		sql = sql.substring(0, sql.length() - 1);
		sql = sql + "where orderId='" + map.get("orderId") + "'";*/
		
		//方法二（判断当前更改状态为支付状态）
		if(Integer.parseInt(map.get("orderStatus").toString())==2){
			Date date = new Date();
			map.put("payTime", date);
			
			sql = "update `xiaoka-xydk`.order set orderStatus=:orderStatus,payTime=:payTime where orderId='" + map.get("orderId") + "'";
		}
		
		return baseDAO.executeNamedCommand(sql, map);
	}

	//添加订单场地用
	public String addOrderFiled(Map<String, Object> map) {
		Date date = new Date();
		//自动注入时间戳为ID 酌情修改数据库类型为bigint  int会越界
		String orerId = Sequence.nextId();
		map.put("id", orerId);
		map.put("createDate", date);
		map.put("isDelete", 0);
		//map.put("startDate", date);
		double payMoney = Double.parseDouble(String.valueOf(map.get("payMoney")));
		double serviceMoney = Double.parseDouble(String.valueOf(map.get("serviceRatio"))) * payMoney;
		map.put("serviceMoney", serviceMoney);
		double diplomatMoney = Double.parseDouble(String.valueOf(map.get("diplomatRatio"))) * payMoney;
		map.put("diplomatMoney", diplomatMoney);
		double platformMoney = Double.parseDouble(String.valueOf(map.get("platformRatio"))) * payMoney;
		map.put("platformMoney", platformMoney);
		String sql = "insert into `xiaoka-xydk`.order"
				+ " (orderId,openId,sellerOpenId,schoolId,cityId,createDate,payMoney,serviceMoney,diplomatMoney,"
				+ "platformMoney,serviceRatio,diplomatRatio,platformRatio,realityMoney,orderStatus,"
				+ "payway,invalidTime,linkman,phone,message,startDate,endDate,isDelete,enterType) " + " values "
				+ " (:id,:openId,:sellerOpenId,:schoolId,:cityId,:createDate,:payMoney,:serviceMoney,:diplomatMoney,"
				+ ":platformMoney,:serviceRatio,:diplomatRatio,:platformRatio,:payMoney,1,"
				+ "0,:invalidTime,:linkman,:phone,:message,:startDate,:endDate,:isDelete,:enterType)";

			boolean flag = baseDAO.executeNamedCommand(sql, map);
			 
			String delSql = "DELETE FROM  `xiaoka-xydk`.selectskill WHERE purOpenId='"+map.get("openId")+"' AND skillId='"+map.get("skillId")+"'"
					+ " and openId='"+map.get("sellerOpenId")+"'";
					baseDAO.executeNamedCommand(delSql, map);
					
			String DataSql = "insert into `xiaoka-xydk`.order_detail(id,orderId,typeName,typeValue,isDelete,createDate)"
						+ "values" + "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
				boolean temp = false;
				Map<String, Object> daMap = new HashMap<String, Object>();
				
				//存入当前的交易数量
				daMap.put("sellerHeadPortrait", map.get("sellerHeadPortrait"));//存入交易需求
				daMap.put("sellerNickname", map.get("sellerNickname"));//存入交易需求
				daMap.put("dic_name", map.get("dic_name"));//存入交易需求
				daMap.put("tranNumber", map.get("tranNumber"));//存入交易需求
				daMap.put("sellPhone", map.get("sellPhone"));//存入交易需求
				daMap.put("tranDemand", map.get("tranDemand"));//存入交易需求
				//daMap.put("singPrice", map.get("singPrice"));存入交易需求
				daMap.put("skillDepict", map.get("skillDepict"));//存入交易需求
				daMap.put("serviceType", map.get("serviceType"));//存入交易需求
				daMap.put("skillName", map.get("skillName"));//存入交易需求
				daMap.put("imgUrl", map.get("imgUrl"));//存入交易需求
				daMap.put("tranNumber", map.get("tranNumber"));//存入交易数量
				daMap.put("skillPrice", map.get("skillPrice"));//存入交易单价
				daMap.put("skillId", map.get("skillId"));//存入商品种类
				if (flag) {
					for (Entry<String, Object> entry : daMap.entrySet()) {
						Map<String, Object> inserMap = new HashMap<String, Object>();
						inserMap.put("id", Sequence.nextId());
						inserMap.put("pid", orerId);
						inserMap.put("typeName", entry.getKey());
						inserMap.put("typeValue", entry.getValue());
						inserMap.put("isDelete", 0);
						inserMap.put("createDate", new Date());
						temp = baseDAO.executeNamedCommand(DataSql, inserMap);
					}
					if(temp){
						return orerId;
					}
				}
				return null;
	}
	
	
	public boolean modifyBuyNum(Map<String, Object> map) {
		
		map.put("createDate", new Date());
		
		String sql = "update `xiaoka-xydk`.selectskill set buyNum=:buyNum , createDate=:createDate where id='" + map.get("seledId") + "'";
		
		return baseDAO.executeNamedCommand(sql, map);
	}			
	/**
	 * 查询当前已选列表中是否有未查看的购物车信息
	 * @param openId
	 * @return
	 */
	public boolean isReadSelect(String openId){
		
		boolean flage = false;
		
		String sql="select COUNT(*) FROM  `xiaoka-xydk`.selectskill t "
				+ "where t.purOpenId = '"+openId+"' and t.isDelete = 0 and t.isRead = 1";
		if(baseDAO.getJdbcTemplate().queryForInt(sql)>0){
			flage = true;
		}
		return flage;
	}
	
	/**
	 * 更新当前已选中的状态
	 * @param map
	 * @return
	 */
	public boolean updateSelectskill(Map<String,Object> map){
		
		boolean flage = false;
		
		String sql = "update `xiaoka-xydk`.selectskill set ";
		
		if(map.containsKey("isRead")){
			sql +=" isRead = :isRead ,";
		}
		sql = sql.substring(0, sql.length()-1);
		
		sql+="where purOpenId = :purOpenId";
		
		return baseDAO.executeNamedCommand(sql, map);
		
	}
	
	/**
	 * 查询当前已选表中的信息
	 */
	public Map<String,Object> getSelectSkill(Map<String,Object> map){
		
		try {
			
			String sql = "select sk.buyNum from `xiaoka-xydk`.selectskill sk where sk.purOpenId ='"+map.get("purOpenId")+"' and sk.skillId="+map.get("skillId");
			return baseDAO.getJdbcTemplate().queryForMap(sql);
			
		} catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 更新当前已选中的状态
	 * @param map
	 * @return
	 */
	public boolean updateSelectskillDate(Map<String,Object> map){
		
		boolean flage = false;
		
		String sql = "update `xiaoka-xydk`.selectskill set ";
		
		if(map.containsKey("createDate")){
			sql +=" createDate = :createDate ,";
		}
		sql = sql.substring(0, sql.length()-1);
		
		sql+="where id = :id";
		
		return baseDAO.executeNamedCommand(sql, map);
		
	}
	
	
	
	
}
