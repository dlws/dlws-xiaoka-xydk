package com.jzwl.xydk.wap.xkh2_2.payment.filedorder.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.skillUser.service.SkillUserManagerService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserEnterInfoService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.business.home.controller.HomeController;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.controller.SkillOrderController;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.service.SkillOrderService;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.controller.FeedBackController;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.pojo.FeedBack;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.service.FeedBackService;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;
import com.jzwl.xydk.wap.xkh2_3.skill_detail.service.SkillDetailService;


/**
 * 场地下单页面
 * @author Administrator
 * 有关于场地入驻订单controller
 */
@Controller("fileOrder")
@RequestMapping("fileOrder")
public class FileOrderController extends BaseWeixinController{
	
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	
	@Autowired
	private UserEnterInfoService userenterInfoService;
	
	@Autowired
	private SkillUserService skillUserService;
	
	@Autowired
	private SkillUserManagerService skillUserManagerService;
	
	@Autowired
	private SkillOrderService skillOrderService;
	
	@Autowired
	private EntertypeService entertypeService;
	
	@Autowired
	private SendMessageService sendMessage; //调用模板消息使用
	
	@Autowired
	private ChatMessageService chatMessageService; 
	
	@Autowired
	private HomeService homeService;
	
	@Autowired
	private SkillDetailService userSkillService;
	
	@Autowired
	private PersonInfoDetailService personInfoDetSer;
	
	@Autowired
	private OrderSeaService orderSeaService;
	
	@Autowired
	private FeedBackService feedbackservice;
	
	@Autowired
	private FeedBackController  feedBackController;
	
	@Autowired
	private SkillOrderController  skillOrderController;
	
	
	Logger log = Logger.getLogger(HomeController.class);
	
	/**
	 * 展示被购买用户的技能或是场地信息
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/toShowFieldOrder")
	public String toShowOrder(HttpServletRequest request,HttpServletResponse response, Model model){
		
		createParameterMap(request);
		//1.当前用户购买为此用户的场地租用
		if(paramsMap.containsKey("skillId")){
			
			//1.获取当前发布商品的skillId
			Map<String,Object> skillMap = new HashMap<String,Object>();
			
			skillMap = userSkillService.getSkillInfo(paramsMap.get("skillId").toString());//订单信息
			
			//获取当前技能的详细信息
			Map<String,Object> detailMap = new HashMap<String, Object>();
			detailMap.put("pid", skillMap.get("id"));
			detailMap = personInfoDetSer.getuserSkillinfoDetail(detailMap);
			detailMap.put("pid", skillMap.get("id"));
			//增加加入已选中的钱数等 2.4功能优化
			skillOrderController.getSelectSkill(request,detailMap,model);
			
			String skillImg = homeService.getImageUrl(skillMap.get("fathValue").toString(), Long.parseLong(skillMap.get("id").toString()));//技能图信息
			//2.根据当前用户的skillId获取当前用户的信息
			if(skillMap.containsKey("basicId")){
				paramsMap.put("id", skillMap.get("basicId"));
				//增加默认销量
				Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//当前用户的详细信息
				if(skillMap.containsKey("sellNo")){
					if(skillMap.get("sellNo")==null||StringUtils.isBlank(skillMap.get("sellNo").toString())){
						skillMap.put("sellNo", 0);  
					}
				}
				String unit = userSkillService.getUnitById(detailMap.get("company").toString());
				model.addAttribute("unit",unit);
				model.addAttribute("detailMap",detailMap);
				model.addAttribute("map", skillMap);
				model.addAttribute("objMap", objMap);
				model.addAttribute("skillImg", skillImg);
				return "wap/xkh_version_2_2/creatorder/filed_order";
			}
			log.error("结算展示未获取到卖家skillId");
			return "redirect:/home/index.html";//跳转到订单结算页面
		}
		return "redirect:/home/index.html";
	}
	
	/**
	 * 场地订单下单
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/beforFileOrder")
	public String beforFileOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		//获取当前用户的OpenId 买家的OpenId
		String openIdBuy = getCurrentOpenId(request); //get openId
		String openIdSell = ""; //get openId
		double money = 0;//总钱数
		double number = 0;//总份数
		createParameterMap(request);//获取当前页面的传入参数

		if(paramsMap.containsKey("message")){
			//若为空则表示未添加内容 则增加空串
			if(paramsMap.get("message")==null||StringUtils.isBlank(paramsMap.get("message").toString())){
				paramsMap.put("message", "");
			}
		}
		Map<String, Object> tranRecord = new HashMap<String, Object>();//存储交易记录 1.服务编号 2.金额
		Map<String,Object> proportion = new HashMap<String, Object>();//获取当前技能的比例
		
		//获取当前页面传入购买个数
		try {
			number = Double.parseDouble(paramsMap.get("number").toString());
		} catch (Exception e) {
			log.error("参数类型转换异常");
			return "redirect:/home/index.html";//跳转到上一提交页面
		}
		if(paramsMap.containsKey("id")){
			
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//当前用户的详细信息
			openIdSell = objMap.get("openId").toString();
			
			//获取金额分配比例
			proportion = userSkillService.getSkillInfo(paramsMap.get("skillId").toString());//订单信息
			//入驻类型
			Map<String,Object> enterMap = entertypeService.getByEnterId(objMap);
			
			Map<String,Object> detailMap = new HashMap<String, Object>();
			detailMap.put("pid", proportion.get("id"));
			detailMap = personInfoDetSer.getuserSkillinfoDetail(detailMap);
			money= Double.parseDouble(detailMap.get("skillPrice").toString());
			
			money = money*number;
			Map<String,Object> map = new HashMap<String, Object>();
			Date date = new Date();
			date = DateUtil.addHour(date,GlobalConstant.HOUR);//下单之后一个小时显示超时
			map.put("openId", openIdBuy);
			map.put("sellerOpenId", openIdSell);
			map.put("schoolId", objMap.get("schoolId"));
			map.put("cityId", objMap.get("cityId"));
			map.put("payMoney", money);//实际金额  存入字段
			map.put("serviceRatio", proportion.get("serviceRatio"));//服务器比例
			map.put("diplomatRatio", proportion.get("diplomatRatio"));//外交官比例
			map.put("platformRatio", proportion.get("platformRatio"));//平台比例
/*			map.put("orderStatus", "1");*/
/*			map.put("payway", "0");*/
			map.put("invalidTime", date);
			// 确认时间 退款时间
			map.put("linkman", paramsMap.get("linkman"));
			map.put("phone", paramsMap.get("phone"));
			map.put("message", paramsMap.get("message"));
			map.put("startDate",paramsMap.get("startDate"));
			map.put("endDate", paramsMap.get("endDate"));
			map.put("enterType", proportion.get("fathValue"));//入驻类型暂时存入enterId
			
			/***订单的私有化信息****/
			
			//头像地址
			map.put("sellerHeadPortrait",paramsMap.get("sellerHeadPortrait"));
			//用户昵称  title 也应该是存这个值
			map.put("sellerNickname",paramsMap.get("sellerNickname"));
			//技能图片
			map.put("imgUrl",paramsMap.get("imageUrl"));
			//服务名称
			map.put("skillName",paramsMap.get("skillName"));
			//服务类型
			map.put("serviceType",paramsMap.get("serviceType"));
			//技能描述
			map.put("skillDepict",paramsMap.get("skillDepict"));
			//单价
			//map.put("singPrice",paramsMap.get("singPrice"));
			
			//map.put("sellNo",paramsMap.get("sellNo"));
			//卖家用户的电话号码
			map.put("sellPhone",paramsMap.get("sellPhone"));
			//存入当前的交易需求
			map.put("tranDemand",paramsMap.get("demand"));
			//存入当前的交易数量
			map.put("tranNumber", (int)number);
			//存入skillId
			map.put("skillId",paramsMap.get("skillId"));
			//dic_name
			map.put("dic_name",paramsMap.get("dic_name"));
			//技能单价
			map.put("skillPrice",Double.parseDouble(proportion.get("skillPrice").toString()));
			
			System.out.println("********打印当前的Map值*********"+map.toString()+"*************");
			String backOrderId = skillOrderService.addOrderFiled(map);

			//系统消息
			String nowOpenId = String.valueOf(paramsMap.get("sellerOpenId"));
			paramsMap.put("nowOpenId", nowOpenId);
			paramsMap.put("orderId", backOrderId);
			paramsMap.put("orderStatus", 1);
			paramsMap.put("openId", openIdBuy);
			chatMessageService.insertUpdateSysMeg(paramsMap);//卖家端
			chatMessageService.insertBuyMeg(paramsMap);//买家端
			
			if(backOrderId!=null&&StringUtils.isNotBlank(backOrderId)){
				
				//加入订单成功增加模板消息推送  TODO
				sendMessage.submissionOrder(backOrderId, openIdBuy);
				return "redirect:/fileOrder/fileOrderDetail.html?orderId="+backOrderId;
			}
			log.error("当前用户下单未成功OpenId为"+openIdBuy);
			return "redirect:/home/index.html";
		}
		
		return "redirect:/home/index.html";
	}
	
	/**
	 * 场地下单详情页面
	 * 传入参数，订单编号
	 */
	@RequestMapping(value="fileOrderDetail")
	public String fileOrderDetail(HttpServletRequest request,HttpServletResponse response,Model model){
		//1.获取订单编号
		createParameterMap(request);//获取当前页面的传入参数
		//2.根据订单编号获取当前的订单信息
		Map<String,Object> order = new HashMap<String, Object>();
		Map<String,Object> userSkill = new HashMap<String, Object>();
		Map<String,Object> orderDetail = new HashMap<String, Object>(); 
		List<Map<String,Object>> orderSon =  new ArrayList<Map<String,Object>>();
		//3.判定是否为买家自打开订单详情
		boolean isMyself = false;//默认为非买家自点击
		//4.增加查看当前Id值是否存在反馈记录
		model = feedBackController.feedBackCommon(paramsMap,model);
	    
		if(paramsMap.containsKey("orderId")){
			
			order=skillOrderService.getOrderById(paramsMap.get("orderId").toString()); //订单信息
			orderSon = skillOrderService.getOrderDetail(paramsMap.get("orderId").toString());  //订单详情信息
			//根据子表中的skillId 查询当前订单的详细信息
			for (Map<String, Object> map : orderSon) {
				orderDetail.put(map.get("typeName").toString(), map.get("typeValue"));
			}
			//根据skillId进行信息的回显
			userSkill = skillUserService.getSkillInfoById(orderDetail.get("skillId").toString());//用户技能信息
			paramsMap.put("id", userSkill.get("basicId").toString());
			Date date = new Date();
			Date overtime = (Date) order.get("invalidTime");
			String overTimetoShow = DateUtil.subtracDateS(overtime, date);
			long timmer = overtime.getTime();
			//companyType  TODO  更改页面展示合作类型
			if(userSkill.containsKey("sellNo")){
				if(userSkill.get("sellNo")==null||StringUtils.isBlank(userSkill.get("sellNo").toString())){
					userSkill.put("sellNo", 0);  
				}
			}
			if(!paramsMap.containsKey("channel")){
				//未进行传递渠道参数则默认为 买家
				paramsMap.put("channel","buyer");
			}
			//判定当前是否为买家自点击
			String openId = getCurrentOpenId(request); //get  openId 
			if(openId.equals(order.get("openId").toString())){
				isMyself = true;//
			}
			model.addAttribute("isMyself",isMyself);//区分是否是买家自己
			model.addAttribute("order",order);//订单
			model.addAttribute("orderSon",orderSon);//订单的个性化信息
			model.addAttribute("orderDetail",orderDetail);//订单的个性化信息
			model.addAttribute("userSkill",userSkill);//关于技能信息的展示
			model.addAttribute("overTimetoShow", overTimetoShow);
			model.addAttribute("channel", paramsMap.get("channel"));
			model.addAttribute("timmer",timmer);//订单时间
			
			return "wap/xkh_version_2_2/orderdetaile/file_order_detail";//跳转到订单详情页面
		}
		
		return "redirect:/home/index.html";
	}
	
}
