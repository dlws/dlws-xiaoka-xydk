package com.jzwl.xydk.wap.xkh2_2.ordseach.pojo;

import java.util.Map;

import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;



/**
 * 此对象，包含订单信息和用户信息订单详情
 * @author Administrator
 * 
 *
 */
public class OrderAndUserInfo {

	
	private Map<String,Object> orderMessage;//订单信息
	
	private Map<String,String> orderDetil;//订单的私有化信息
	
	private UserBasicinfo userInfo;//用户信息
	
	

	public Map<String, String> getOrderDetil() {
		return orderDetil;
	}

	public void setOrderDetil(Map<String, String> orderDetil) {
		this.orderDetil = orderDetil;
	}

	public Map<String, Object> getOrderMessage() {
		return orderMessage;
	}

	public void setOrderMessage(Map<String, Object> orderMessage) {
		this.orderMessage = orderMessage;
	}

	public UserBasicinfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserBasicinfo userInfo) {
		this.userInfo = userInfo;
	}
	
	
	
}
