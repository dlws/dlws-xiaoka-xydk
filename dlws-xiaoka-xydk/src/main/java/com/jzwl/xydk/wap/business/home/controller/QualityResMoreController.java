package com.jzwl.xydk.wap.business.home.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>Title:优质资源页面（更多专题页面） </p>
 * 	<p>Description:BusinessBasicinfo </p>
 * 	<p>Company: xydk</p> 
 * 	@author about quality resouce more 
 */

@Controller
@RequestMapping(value="/quality")
public class QualityResMoreController {

	/**
	 * 此功能为点赞功能
	 * @return
	 */
	@RequestMapping(value="toThumbsUp")
	public @ResponseBody boolean toThumbsUp(HttpServletRequest request, HttpServletResponse response){
		
		//如果
		
		String is = (String) request.getSession().getAttribute("ThumbsUp");
		if(StringUtils.isNotBlank(is)){
			
		}
		
		
		
		return true;
	}
}
