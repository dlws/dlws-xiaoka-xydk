package com.jzwl.xydk.wap.xkh2_2.payment.feedback.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.payment.feedback.dao.FeedbackDao;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.pojo.FeedBack;


/**
 * the service mapping feedback 
 * @author Administrator
 *
 */

@Service
public class FeedBackService {

	@Autowired
	private FeedbackDao feedback;
	
	/**
	 * searchFeeaBack
	 */
	public List<FeedBack> searchFeeaBack(Map<String,Object> condition){
		
		return feedback.searchFeeaBack(condition);
	}
	
	/**
	 * update feedBack
	 */
	public boolean updateFeedBack(Map<String,Object> updateContent){
		
		//记录历史数据
		feedback.insertFeedBackOld(updateContent.get("orderId").toString());
		
		return feedback.updateFeedBack(updateContent);
	}
	/**
	 * insert feedback
	 */
	public boolean insertFeedBack(Map<String,Object> insertContent){
		
		return feedback.insertFeedBack(insertContent);
	}
	
}
