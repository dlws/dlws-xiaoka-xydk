package com.jzwl.xydk.wap.xkh2_3.skill_detail.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_3.skill_detail.dao.SkillDetailDao;

@Service
public class SkillDetailService {

	@Autowired 
	SkillDetailDao skillDao;
	
	public Map<String,Object> getSkillInfo(String skillId){
		
		return skillDao.getSkillInfo(skillId);
	}
	
	/**
	 * 获取当前的unitId
	 * @param unitId
	 * @return
	 */
	public String getUnitById(String unitId){
		
		return skillDao.getUnitById(unitId);
	}
	
}
