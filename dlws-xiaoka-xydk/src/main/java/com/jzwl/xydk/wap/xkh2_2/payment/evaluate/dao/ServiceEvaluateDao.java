package com.jzwl.xydk.wap.xkh2_2.payment.evaluate.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.xkh2_2.payment.evaluate.pojo.ServiceEvaluate;


@Repository
public class ServiceEvaluateDao {

	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	Logger log = Logger.getLogger(getClass());
	
	
	/**
	 * 根据Map中传入的查询条件进行查询
	 * @return
	 */
	public List<ServiceEvaluate> searchServiceEvaluate(Map<String,Object> map){
		
		
		if(map.isEmpty()){
			
			log.debug("------------当前传入的查询条件为空----------------");
			return null;
		}
		String sql = "select t.orderId,t.efficiencyGrade,t.qualityGrade,t.attitudeGrade,t.evaluate,"
				+ "t.createDate from `xiaoka-xydk`.service_evaluate t where 1=1 ";
		
		if(map.get("orderId")!=null){
			
			sql +="and orderId = "+map.get("orderId");
		}
		//condition++
		
		List<ServiceEvaluate>  list= baseDAO.getJdbcTemplate().query(sql,
				new Object[] {},
				ParameterizedBeanPropertyRowMapper
						.newInstance(ServiceEvaluate.class));
		
		return list;
		
	}
	
	/**
	 * 插入表  `xiaoka-xydk`.service_evaluate 操作
	 */
	public boolean insertServiceEvaluate(Map<String,Object> map){
		
		if(!map.isEmpty()){
			
			map.put("id", Sequence.nextId());
			map.put("createDate", new Date());
			
			String sql = "insert into  `xiaoka-xydk`.service_evaluate("
					+ " id,orderId,efficiencyGrade,qualityGrade,attitudeGrade,evaluate,createDate) values("
					+ ":id,:orderId,:efficiencyGrade,:qualityGrade,:attitudeGrade,:evaluate,:createDate)";
			
			//执行insert 语句
		    boolean flage =	baseDAO.executeNamedCommand(sql, map);
		    if(flage){
		    	log.debug("------------当前数据插入失败:--------------"+map.toString());
		    }
			return flage;
		}
		return false;
	}
	
	
	
	
	
}
