package com.jzwl.xydk.wap.xkh2_3.invoice.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_3.invoice.dao.InvoiceDao;

@Service("invoiceService")
public class InvoiceService {

	@Autowired
	private InvoiceDao invoiceDao;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据openId查询所需要开发票的订单
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> findInvoiceList(String openId, boolean flag, String status) {
		return invoiceDao.findInvoiceList(openId, flag, status);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：判断是否入驻
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> getUserBaseInfo(String openId) {
		return invoiceDao.getUserBaseInfo(openId);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取发票类型
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getInvoType() {
		return invoiceDao.getInvoType();
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取税点
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @return
	 * @version
	 */
	public Map<String, Object> getTaxPoint() {
		return invoiceDao.getTaxPoint();
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：存储信息到发票表
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public String addInvoice(Map<String, Object> paramsMap) {
		return invoiceDao.addInvoice(paramsMap);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：在支付页面点击否,删除当前数据防止产生垃圾数据
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param id
	 * @return
	 * @version
	 */
	public boolean deleteInvoice(String id) {
		return invoiceDao.deleteInvoice(id);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取发票类型
	 * 创建人： sx
	 * 创建时间： 2017年5月22日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getLogisticsTypes() {
		return invoiceDao.getLogisticsTypes();
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：专用发票税点
	 * 创建人： sx
	 * 创建时间： 2017年5月23日
	 * 标记：
	 * @return
	 * @version
	 */
	public Map<String, Object> getSpecPoint() {
		return invoiceDao.getSpecPoint();
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据发票ID查询订单ID
	 * 创建人： sx
	 * 创建时间： 2017年5月23日
	 * 标记：
	 * @param id
	 * @return
	 * @version
	 */
	public Map<String,Object> findOrderIdsById(String id) {
		return invoiceDao.findOrderIdsById(id);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据发票id修改发票状态
	 * 创建人： sx
	 * 创建时间： 2017年5月24日
	 * 标记：
	 * @param id
	 * @version
	 */
	public boolean updateInvoiceById(String id, String outTradeNo) {
		return invoiceDao.updateInvoiceById(id, outTradeNo);
	}
	/**
	 * 
	 * 描述:根据订单的Id更新订单的发票的id
	 * 作者:gyp
	 * @Date	 2017年5月27日
	 */
	public boolean updateOrderByOrderId(String invoiceId,String orderId) {
		return invoiceDao.updateOrderByOrderId(invoiceId, orderId);
	}

}
