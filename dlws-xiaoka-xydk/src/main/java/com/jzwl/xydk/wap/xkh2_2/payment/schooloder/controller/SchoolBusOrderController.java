package com.jzwl.xydk.wap.xkh2_2.payment.schooloder.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.common.utils.MapUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.skillUser.service.SkillUserManagerService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserEnterInfoService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.business.home.controller.HomeController;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.controller.SkillOrderController;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.service.SkillOrderService;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.controller.FeedBackController;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;
import com.jzwl.xydk.wap.xkh2_3.skill_detail.service.SkillDetailService;


/**
 * wap 版校内商家入驻，关于订单的信息
 * @author Administrator
 *
 */
@Controller
@RequestMapping("schoolBusOrder")
public class SchoolBusOrderController extends BaseWeixinController{
	
	Logger log = Logger.getLogger(HomeController.class);
	
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	
	@Autowired
	private UserEnterInfoService userenterInfoService;
	
	@Autowired
	private SkillUserService skillUserService;
	
	@Autowired
	private SkillOrderService skillOrderService;
	
	@Autowired
	private SkillUserManagerService skillUserManagerService;
	
	@Autowired
	private EntertypeService entertypeService;
	
	@Autowired
	private SendMessageService sendMessage; //调用模板消息使用
	
	@Autowired
	private ChatMessageService chatMessageService; //系统消息
	
	@Autowired
	private HomeService homeService;
	
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	
	@Autowired
	private SkillDetailService userSkillService;
	
	@Autowired
	private FeedBackController  feedBackController;
	
	@Autowired
	private SkillOrderController  skillOrderController;
	
	/**
	 * 准备生成订单的方法
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/toShowSchoolOrder")
	public String toShowSchoolOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		
		createParameterMap(request);
		//前端页面区分当前信息
		boolean flage = true;
		//前端页面显示价格区间 int 为整数形式的最大值
		
		//1.当前用户购买为此用户的校内商家
		if(paramsMap.get("skillId")!=null||StringUtils.isNotBlank(paramsMap.get("skillId").toString())){
			
			//1.获取当前发布商品的skillId
			Map<String,Object> map = new HashMap<String,Object>();
			//2.展示当前所有合作类型
			List<Map<String, Object>> cooperaTypeList = userBasicinfoService.getFieldtype(GlobalConstant.COMPARE_TYPE);//合作类型
			
			map = userSkillService.getSkillInfo(paramsMap.get("skillId").toString());//订单信息
			
			String skillImg = homeService.getImageUrl(map.get("fathValue").toString(), 
					Long.parseLong(map.get("id").toString()));//技能图信息
			
			String unit = homeService.setCompany(map.get("fathValue").toString(), 
					Long.parseLong(map.get("id").toString()));//获取当前单位
			
			//2.根据当前用户的skillId获取当前用户的信息
			if(map.get("basicId")!=null&&StringUtils.isNotBlank(map.get("basicId").toString())){
				paramsMap.put("id", map.get("basicId").toString());
				Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//当前用户的详细信息
				Map<String,Object> detailMap = new HashMap<String, Object>();
				detailMap.put("pid", map.get("id"));
				detailMap = personInfoDetSer.getuserSkillinfoDetail(detailMap);
				//增加加入已选中的钱数等 2.4功能优化
				detailMap.put("pid", map.get("id"));
				skillOrderController.getSelectSkill(request,detailMap,model);
				//价格区间整理 最小值 与 最大值
				JSONObject jasonObject = JSONObject.parseObject(detailMap.get("cooperaType").toString());
				Map<String, Object> cooperaTypes = (Map) jasonObject;
				//设置默认销量为0
			    String maxPrice = MapUtil.getMaxValue(cooperaTypes).toString();
			    String minPrice =  MapUtil.getMinValue(cooperaTypes).toString();
				
				if(map.containsKey("sellNo")){
					if(map.get("sellNo")==null||StringUtils.isBlank(map.get("sellNo").toString())){
						map.put("sellNo", 0);  
					}
				}
				
				model.addAttribute("map", map);//技能相关信息
				model.addAttribute("objMap", objMap);
				model.addAttribute("echoMap", detailMap);
				model.addAttribute("begin",minPrice);
				model.addAttribute("end",maxPrice);
				model.addAttribute("cooperaTypeList",cooperaTypeList); 
				model.addAttribute("cooperaTypes",cooperaTypes); //当前用户有的合作形式
				model.addAttribute("skillImg", skillImg);
				model.addAttribute("unit", unit);
				return "wap/xkh_version_2_2/creatorder/school_order";
			}
			log.error("结算展示未获取到卖家用户Id");
			return "redirect:/home/index.html";//跳转到订单结算页面
		}
		
		return "redirect:/home/index.html";//跳转到订单结算页面
	}
	/**
	 * 生成订单的方法
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/beforSchoolOrder")
	public String beforSchoolOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		
		
		
		//获取当前用户的OpenId 买家的OpenId
		String openIdBuy = getCurrentOpenId(request); //get openId
		String openIdSell = ""; //get openId
		double money = 0;//总钱数
		double number = 0;//总份数
		createParameterMap(request);//获取当前页面的传入参数
		String[] pamaters = request.getParameterValues("key");
		Map<String, Object> tranRecord = new HashMap<String, Object>();//存储交易记录 1.服务编号 2.金额
		Map<String,Object> proportion = new HashMap<String, Object>();//获取当前技能的比例
		if(paramsMap.containsKey("message")){
			//若为空则表示未添加内容 则增加空串
			if(paramsMap.get("message")==null||StringUtils.isBlank(paramsMap.get("message").toString())){
				paramsMap.put("message", "");
			}
		}
		
		//获取当前页面传入购买个数
		try {
			number = Double.parseDouble(paramsMap.get("number").toString());
		} catch (Exception e) {
			log.error("参数类型转换异常");
			return "";//跳转到上一提交页面
		}
		
		if(paramsMap.get("id")!=null&&StringUtils.isNotBlank(paramsMap.get("id").toString())){
			
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//当前用户的详细信息
			openIdSell = objMap.get("openId").toString();
			//校内商家入驻时的信息
			Map<String, Object> echoMap = new HashMap<String, Object>();
			echoMap.put("pid", paramsMap.get("skillId").toString());
			echoMap = personInfoDetSer.getuserSkillinfoDetail(echoMap);
			
			
			//计算当前价格
			JSONObject jasonObject = JSONObject.parseObject(echoMap.get("cooperaType").toString());
			Map<String, Object> cooperaTypes = (Map) jasonObject;
			
			//获取用户选中的服务类型
			for (String key: cooperaTypes.keySet()) {
				//若存在于集合中则表示被选中
				if(StringArrayContain(pamaters,key)){
					//当前选中服务的钱数
					money = money+Double.parseDouble(cooperaTypes.get(key).toString());
					//记录当前购买的产品和钱数  等等再写
					tranRecord.put(key,cooperaTypes.get(key).toString());
				}
				
			}
			//获取金额分配比例
			proportion = userSkillService.getSkillInfo(paramsMap.get("skillId").toString());
			
			money = money*number;
			Map<String,Object> map = new HashMap<String, Object>();
			Date date = new Date();
			date = DateUtil.addHour(date,GlobalConstant.HOUR);//下单之后一个小时显示超时
			map.put("openId", openIdBuy);
			map.put("sellerOpenId", openIdSell);
			map.put("schoolId", objMap.get("schoolId"));
			map.put("cityId", objMap.get("cityId"));
			map.put("payMoney", money);//实际金额  存入字段
			map.put("serviceRatio", proportion.get("serviceRatio"));//服务器比例
			map.put("diplomatRatio", proportion.get("diplomatRatio"));//外交官比例
			map.put("platformRatio", proportion.get("platformRatio"));//平台比例
			map.put("invalidTime", date);
			// 确认时间 退款时间
			map.put("linkman", paramsMap.get("linkman"));
			map.put("phone", paramsMap.get("phone"));
			map.put("message", paramsMap.get("message"));
			map.put("startDate",paramsMap.get("startDate"));
			map.put("endDate", paramsMap.get("endDate"));
			map.put("enterType", proportion.get("fathValue"));//入驻类型暂时存入enterId
			
			/***订单的私有化信息****/
			JSONObject tranRecordJ=new JSONObject(tranRecord);
			//存入当前的交易记录
			map.put("tranRecord", tranRecordJ.toJSONString());
			//存入当前的交易数量
			map.put("tranNumber", (int)number);
			//存入skillId
			map.put("skillId",paramsMap.get("skillId"));
			//存入交易单价区间
			map.put("begin",paramsMap.get("begin") );
			map.put("end",paramsMap.get("end") );
			map.put("dic_name", paramsMap.get("dic_name"));
			//头像地址
			map.put("sellerHeadPortrait",paramsMap.get("sellerHeadPortrait"));
			//用户昵称  title 也应该是存这个值
			map.put("sellerNickname",paramsMap.get("sellerNickname"));
			//技能图片
			map.put("imgUrl",paramsMap.get("imageUrl"));
			//服务名称
			map.put("skillName",paramsMap.get("skillName"));
			//服务类型
			map.put("serviceType",paramsMap.get("serviceType"));
			//技能描述
			map.put("skillDepict",paramsMap.get("skillDepict"));
			//卖家用户的电话号码
			map.put("sellPhone",paramsMap.get("sellPhone"));
			//存入当前的交易需求
			map.put("tranDemand",paramsMap.get("demand"));
			//存入skillId
			map.put("skillId",paramsMap.get("skillId"));
			System.out.println("********打印当前的Map值*********"+map.toString()+"*************");
			String backOrderId = skillOrderService.addOrderSch(map);
			//系统消息
			String nowOpenId = String.valueOf(map.get("sellerOpenId"));
			paramsMap.put("nowOpenId", nowOpenId);
			paramsMap.put("orderId", backOrderId);
			paramsMap.put("orderStatus", 1);
			paramsMap.put("openId",openIdBuy);
			chatMessageService.insertUpdateSysMeg(paramsMap);
			chatMessageService.insertBuyMeg(paramsMap);//买家端
			if(backOrderId!=null&&StringUtils.isNotBlank(backOrderId)){
				//TODO  增加下单通知
				sendMessage.submissionOrder(backOrderId, openIdBuy);
				 return "redirect:/schoolBusOrder/schoolOrderDetail.html?orderId="+backOrderId;
			}
			log.error("当前用户下单未成功OpenId为"+openIdBuy);
			return "redirect:/home/index.html";
		}
		//计算钱数
		return "redirect:/home/index.html";
	}
	
	
	/**
	 * 查看订单详情页面
	 */
	@RequestMapping("/schoolOrderDetail")
	public String schoolOrderDetail(HttpServletRequest request,HttpServletResponse response,Model model){
		
		//判定是否是购买订单的用户进行订单查看
		boolean isMySelf = false;
		//1.获取订单编号
		createParameterMap(request);//获取当前页面的传入参数
		
		//2.根据订单编号获取当前的订单信息
		Map<String,Object> order = new HashMap<String, Object>();
		Map<String,Object> userSkill = new HashMap<String, Object>();
		Map<String,Object> orderDetail = new HashMap<String, Object>(); 
		List<Map<String,Object>> orderSon =  new ArrayList<Map<String,Object>>();
		
		//4.增加查看当前Id值是否存在反馈记录
		model = feedBackController.feedBackCommon(paramsMap,model);
		
		
		if(paramsMap.containsKey("orderId")){
			
			order=skillOrderService.getOrderById(paramsMap.get("orderId").toString()); //订单信息
			orderSon = skillOrderService.getOrderDetail(paramsMap.get("orderId").toString());  //订单详情信息
			//根据子表中的skillId 查询当前订单的详细信息
			for (Map<String, Object> map : orderSon) {
				orderDetail.put(map.get("typeName").toString(), map.get("typeValue"));
			}
			//处理交易信息为map类型
			if(orderDetail.containsKey("tranRecord")){
				
				JSONObject jasonObject = JSONObject.parseObject(orderDetail.get("tranRecord").toString());
				Map<String, Object> cooperaTypes = (Map) jasonObject;
				model.addAttribute("cooperaTypes", cooperaTypes); //存入合作类型
			}
			//根据skillId进行信息的回显
			userSkill = skillUserService.getSkillInfoById(orderDetail.get("skillId").toString());//用户技能信息
			//加载当前的合作类型（字典表） filedsize
			List<Map<String, Object>> cooperaTypeList = userBasicinfoService.getFieldtype(GlobalConstant.COMPARE_TYPE);//合作类型
			Date date = new Date();
			Date overtime = (Date) order.get("invalidTime");
			String overTimetoShow = DateUtil.subtracDateS(overtime, date);
			long timmer = overtime.getTime();
			//增加渠道信息
			if(!paramsMap.containsKey("channel")){
				//未进行传递渠道参数则默认为 买家
				paramsMap.put("channel","buyer");
			}
			//销量为空，则存入0
			if(userSkill.get("sellNo")==null||StringUtils.isBlank(userSkill.get("sellNo").toString())){
				userSkill.put("sellNo", 0);
			}
			//判定当前是否为买家自点击
			String openId = getCurrentOpenId(request); //get  openId 
			if(openId.equals(order.get("openId").toString())){
				isMySelf = true;//
			}
			
			model.addAttribute("isMySelf", isMySelf);//判断是否是买家自己点击
			model.addAttribute("order",order);//订单
			model.addAttribute("orderSon",orderSon);//订单的个性化信息
			model.addAttribute("userSkill",userSkill);//关于技能信息的展示
			model.addAttribute("orderDetail",orderDetail);
			model.addAttribute("overTimetoShow", overTimetoShow);//
			model.addAttribute("cooperaTypeList", cooperaTypeList);//被商家购买的相关技能
			model.addAttribute("channel", paramsMap.get("channel"));//区分点击渠道
			model.addAttribute("timmer", timmer);//计算当前时间传入页面
			
			//model.addAttribute("order",order);
			return "wap/xkh_version_2_2/orderdetaile/school_order_detail";//跳转到订单详情页面
			
		}
		return "redirect:/home/index.html";
	}
	
	/**
	 * 比较AString【】 中是否包含B
	 * @param pamaters
	 * @param pamater
	 * @return
	 */
	public boolean StringArrayContain(String[] pamaters, String pamater){
		
		boolean flage=false;
		 for(int i = 0;i<pamaters.length;i++){        
		      if(pamaters[i].equals(pamater)){            
		           flage=true;
		      }   
		 }    
		return flage;
		
	}
	
}
