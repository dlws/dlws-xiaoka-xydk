package com.jzwl.xydk.wap.xkh2_3.changemanager.dao;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("changeManagerDao")
public class ChangeManagerDao {

	@Autowired
	private BaseDAO baseDao;

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据openId获取入驻信息
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findByOpenId(String openId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.id,t.enterId,t.userName,t.cityId,t.schoolId,t.labelId,"
				+ "	t.phoneNumber,t.email,t.wxNumber,t.openId,t.aboutMe,t.contactUser,"
				+ "	t.checkType,t.nickname,t.backPicture,t.headPortrait,t.skillStatus "
				+ "	from `xiaoka-xydk`.user_basicinfo t where isDelete=0 and openId = '"+openId+"'");
		return baseDao.queryForMap(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据微信号查询入驻信息
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param wxNumber
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findByWxNumber(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.id,t.enterId,t.userName,t.cityId,t.schoolId,t.labelId, "
				+ "	t.phoneNumber,t.email,t.wxNumber,t.openId,t.aboutMe,t.contactUser,"
				+ "	t.checkType,t.nickname,t.backPicture,t.headPortrait,t.skillStatus from `xiaoka-xydk`.user_basicinfo t "
				+ " where wxNumber = :wxNumber and nickname = :nickname and phoneNumber = :phoneNumber and email = :email and isDelete=0 ");
		return baseDao.queryForMap(sb.toString(), map);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：验证通过插入数据
	 * 创建人： sx
	 * 创建时间： 2017年5月16日
	 * 标记：
	 * @param flagMap
	 * @version
	 */
	public String addRecord(Map<String, Object> flagMap) {
		StringBuffer sb = new StringBuffer();
		String id = Sequence.nextId();
		flagMap.put("id", id);
		flagMap.put("createDate", new Date());
		sb.append("insert into `xiaoka-xydk`.change_manager_record "
				+ " (id, basicId,userId, managerName, phone, email, wx_number, createDate) "
				+ " values (:id, :basicId, :userId, :nickname, :phoneNumber, :email, :wxNumber, :createDate )");
		boolean flag = baseDao.executeNamedCommand(sb.toString(), flagMap);
		if(flag){
			return id;
		}else{
			return null;
		}
		
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询记录信息
	 * 创建人： sx
	 * 创建时间： 2017年5月16日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findChangeManager(String openId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.id,t.basicId,t.userId,t.managerName,t.phone,t.email,t.wx_number,t.isAgree,t.createDate"
				+ " from `xiaoka-xydk`.change_manager_record t left join "
				+ "	`xiaoka-xydk`.user_basicinfo u on u.id = t.basicId  where u.openId = '"+openId+"'");
		return baseDao.queryForMap(sb.toString());
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据用户的记录获取用户信息
	 * 创建人： sx
	 * 创建时间： 2017年5月16日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getBasicInfoByRecordId(String recordId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select t.id,t.enterId,t.userName,t.cityId,t.schoolId,t.labelId,"
				+ "	t.phoneNumber,t.email,t.wxNumber,t.openId,t.aboutMe,t.contactUser,"
				+ "	t.checkType,t.nickname,t.backPicture,t.headPortrait,t.skillStatus,u.isAgree"
				+ "	 from `xiaoka-xydk`.change_manager_record u "
				+ "left join `xiaoka-xydk`.user_basicinfo t on t.id = u.userId where u.id = '"+recordId+"'");
		return baseDao.queryForMap(sb.toString());
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改记录
	 * 创建人： sx
	 * 创建时间： 2017年5月16日
	 * 标记：
	 * @param insertMap
	 * @return
	 * @version
	 */
	public boolean updateChangeManage(Map<String, Object> insertMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.change_manager_record set isAgree = :isAgree where id = :id ");
		return baseDao.executeNamedCommand(sb.toString(), insertMap);
	}
	
	public boolean updateSkillinfo(Map<String, Object> insertMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.user_skillinfo set basicId = :basicId where basicId = :userId ");
		return baseDao.executeNamedCommand(sb.toString(), insertMap);
	}
	
	public boolean updateBasicinfo(Map<String, Object> insertMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.user_basicinfo set isDelete = 1 where id = :userId ");
		return baseDao.executeNamedCommand(sb.toString(), insertMap);
	}
}
