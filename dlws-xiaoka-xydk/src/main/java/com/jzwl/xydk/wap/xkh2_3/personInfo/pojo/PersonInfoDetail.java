package com.jzwl.xydk.wap.xkh2_3.personInfo.pojo;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 个人技能详情实体类
 * @author Administrator
 *
 */
public class PersonInfoDetail {

	private long id ;
	
	private long basicId ;  
	
	private long skillFatId;  //'技能分类父级的Id',
	
	private long skillSonId;//技能分类子级的Id',
	
	private String skillName; //技能名称',
	
	private String textURL; // '图文展示的url',
	
	private String otherOpus; // varchar(255) DEFAULT NULL COMMENT '其他作品的url',
	
	private String videoURL; //varchar(500) DEFAULT NULL COMMENT '视屏展示url',
	
	private String skillPrice; //double DEFAULT '0' COMMENT '技能价格',
	
	private long  company;// bigint(20) DEFAULT '0' COMMENT '服务的单位（字典表进行配置。如：次，天）',
	
	private int  serviceType;// int(2) NOT NULL DEFAULT '0' COMMENT '服务类型（1：线上服务，2：线下服务）',
	
	private String skillDepict;// varchar(1000) NOT NULL COMMENT '技能描述',
	
	private int  viewNum;// int(11) DEFAULT '0' COMMENT '用户技能的浏览量',
	
	private Date  createDate ;//datetime NOT NULL COMMENT '创建时间',
	
	private int  isDelete;//int(2) DEFAULT '0' COMMENT '是否删除（0：否，1：是）',
	
	private int  isDisplay; //int(2) NOT NULL COMMENT '是否开启(0开启，1关闭)',
	
	private int ishome; //int(2) DEFAULT '0' COMMENT '是否首页显示   0是1否',
	
	private int sellNo;//int(5) DEFAULT '0' COMMENT '销售量',
	
	private int  ord ;//int(5) DEFAULT '0' COMMENT '显示顺序',
	
	private String fathName;//一级分类
	
	private String fathValue;//以及分类缩写
	
	private String sonName;//二级分类
	
	private String sonValue;//二级分类缩写
	
	private Map<String,Object> mapDetail;//当前技能详情
	
	private List<String> closeList;//近景图片
	
	private List<String> middelList;//中景图片
	
	private List<String> allList;//远景图片
	
	private List<String> activtyList;//高校社团，技能达人公用此活动图
	
	private List<String> dairyList;
	
	private String unit;//单位
	
	
	
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public List<String> getDairyList() {
		return dairyList;
	}
	public void setDairyList(List<String> dairyList) {
		this.dairyList = dairyList;
	}
	public List<String> getActivtyList() {
		return activtyList;
	}
	public void setActivtyList(List<String> activtyList) {
		this.activtyList = activtyList;
	}
	public String getFathName() {
		return fathName;
	}
	public void setFathName(String fathName) {
		this.fathName = fathName;
	}
	public String getFathValue() {
		return fathValue;
	}
	public void setFathValue(String fathValue) {
		this.fathValue = fathValue;
	}
	public String getSonName() {
		return sonName;
	}
	public void setSonName(String sonName) {
		this.sonName = sonName;
	}
	public String getSonValue() {
		return sonValue;
	}
	public void setSonValue(String sonValue) {
		this.sonValue = sonValue;
	}
	public Map<String, Object> getMapDetail() {
		return mapDetail;
	}
	public void setMapDetail(Map<String, Object> mapDetail) {
		this.mapDetail = mapDetail;
	}
	public List<String> getCloseList() {
		return closeList;
	}
	public void setCloseList(List<String> closeList) {
		this.closeList = closeList;
	}
	public List<String> getMiddelList() {
		return middelList;
	}
	public void setMiddelList(List<String> middelList) {
		this.middelList = middelList;
	}
	public List<String> getAllList() {
		return allList;
	}
	public void setAllList(List<String> allList) {
		this.allList = allList;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getBasicId() {
		return basicId;
	}
	public void setBasicId(long basicId) {
		this.basicId = basicId;
	}
	public long getSkillFatId() {
		return skillFatId;
	}
	public void setSkillFatId(long skillFatId) {
		this.skillFatId = skillFatId;
	}
	public long getSkillSonId() {
		return skillSonId;
	}
	public void setSkillSonId(long skillSonId) {
		this.skillSonId = skillSonId;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public String getTextURL() {
		return textURL;
	}
	public void setTextURL(String textURL) {
		this.textURL = textURL;
	}
	public String getOtherOpus() {
		return otherOpus;
	}
	public void setOtherOpus(String otherOpus) {
		this.otherOpus = otherOpus;
	}
	public String getVideoURL() {
		return videoURL;
	}
	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}
	public String getSkillPrice() {
		return skillPrice;
	}
	public void setSkillPrice(String skillPrice) {
		this.skillPrice = skillPrice;
	}
	public long getCompany() {
		return company;
	}
	public void setCompany(long company) {
		this.company = company;
	}
	public int getServiceType() {
		return serviceType;
	}
	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}
	public String getSkillDepict() {
		return skillDepict;
	}
	public void setSkillDepict(String skillDepict) {
		this.skillDepict = skillDepict;
	}
	public int getViewNum() {
		return viewNum;
	}
	public void setViewNum(int viewNum) {
		this.viewNum = viewNum;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	public int getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(int isDisplay) {
		this.isDisplay = isDisplay;
	}
	public int getIshome() {
		return ishome;
	}
	public void setIshome(int ishome) {
		this.ishome = ishome;
	}
	public int getSellNo() {
		return sellNo;
	}
	public void setSellNo(int sellNo) {
		this.sellNo = sellNo;
	}
	public int getOrd() {
		return ord;
	}
	public void setOrd(int ord) {
		this.ord = ord;
	}
	
	
	
	
}
