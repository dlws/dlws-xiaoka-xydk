package com.jzwl.xydk.wap.xkh2_3.publish.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.sign.AccessCore;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderAndUserInfo;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_3.publish.dao.PublishDao;
import com.jzwl.xydk.wap.xkh2_3.publish.pojo.LeaveAndSchool;
import com.jzwl.xydk.wap.xkh2_3.publish.service.PublishService;
@Controller
@RequestMapping(value="/publish")
public class PublishXkhController extends BaseWeixinController{
	
	@Autowired
	private PublishService publishService;
	@Autowired
	private Constants contants; 
	@Autowired
	private SendMessageService sendMessageService;
	@Autowired
	private OrderSeaService orderSeaService;
	@Autowired 
	private PublishDao publishDao;
	
	/** 日志 */
	Logger log = Logger.getLogger(PublishXkhController.class);
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到朕已阅页面
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/publishInfo")
	public String toPublishInfo(HttpServletRequest request, Model model){
		
		//先判断该用户是否已经阅读过改协议  1：如果已经阅读，直接跳转至发布页面   2：如果没有阅读则跳转至朕已阅的界面
		String purOpenId = getCurrentOpenId(request);//从session中获取openId
		Map<String, Object> map_read = publishService.getBusinessRead(purOpenId);
		if(map_read.size() > 0){
			return "redirect:/publish/publishDemand.html";
		}else{
			//查询协议内容
			Map<String, Object> map = publishService.getRelease();
			map.put("openId", purOpenId);
			publishService.addBusinessRead(map);//添加读取协议记录
			model.addAttribute("map", map);
			return "/wap/xkh_version_2_3/publish_info";
		}
		
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到精准需求页面
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/publishDemand")
	public String toPublishDemand(HttpServletRequest request, Model model){
		log.info("跳转到----------------精准需求页面------");
		createParameterMap(request);
		try {
			//查询发布类型
			List<Map<String, Object>> listType = publishService.getPublishType();
			//查询banner图
			Map<String, Object> banner = publishService.getBanner("jzxq");
			//查询一级分类
			String cateList = publishService.getFirstCategory(); 
			//查询城市
			List<Map<String, Object>> cityList = publishService.getCityList();
			//查询学校标签
			List<Map<String, Object>> labelList = publishService.getLabelList();
			//985
			List<Map<String, Object>> nineList = publishService.getNineList();
			//本科院校
			List<Map<String, Object>> gradList = publishService.getGradList();
			//专科院校
			List<Map<String, Object>> specList = publishService.getSpecList();
			model.addAttribute("listType", listType);
			model.addAttribute("banner", banner);
			model.addAttribute("cateList", cateList);
			model.addAttribute("cityList", cityList);
			model.addAttribute("labelList", labelList);
			model.addAttribute("nineList", nineList);
			model.addAttribute("gradList", gradList);
			model.addAttribute("specList", specList);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "/wap/xkh_version_2_3/businesspublish/publish_demand";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到精准咨询页面
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/publishConsult")
	public String toPublishConsult(HttpServletRequest request, Model model){
		log.info("跳转到----------------精准咨询页面------");
		createParameterMap(request);
		try {
			String userName = (String)paramsMap.get("userName");
			//查询发布类型
			List<Map<String, Object>> listType = publishService.getPublishType();
			//查询banner图
			Map<String, Object> banner = publishService.getBanner("jzzx");
			//查询账号信息
			paramsMap.put("start", 0);
			paramsMap.put("pageNum", 5);
			List<Map<String, Object>> infoList = publishService.getInfoList(paramsMap);
			//查询优选服务
			Map<String, Object> serverMap = publishService.preferrService();
			//查询一级分类
			String cateList = publishService.getFirstCategory();
			model.addAttribute("listType", listType);
			model.addAttribute("banner", banner);
			model.addAttribute("infoList", infoList);
			model.addAttribute("serverMap", serverMap);
			model.addAttribute("userName", userName);
			model.addAttribute("cateList", cateList);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "/wap/xkh_version_2_3/businesspublish/publish_consult";
	}
	
	
	
	
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：精准咨询列表ajax
	 * 创建人： dxf
	 * 创建时间： 2017年6月13日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/publishConsultAjax")
	public @ResponseBody Map toPublishConsultAjax(HttpServletRequest request, HttpServletResponse response){
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		String currentPage = request.getParameter("currentPage");
		try {
			String userName = (String)paramsMap.get("userName");
			//查询发布类型
			List<Map<String, Object>> listType = publishService.getPublishType();
			//查询banner图
			Map<String, Object> banner = publishService.getBanner("jzzx");
			
			int cur = Integer.parseInt(currentPage);
			int start = (cur - 1) * 5;
			paramsMap.put("start", start);
			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			//查询账号信息
			List<Map<String, Object>> infoList = publishService.getInfoList(paramsMap);
			//查询优选服务
			Map<String, Object> serverMap = publishService.preferrService();
			//查询一级分类
			String cateList = publishService.getFirstCategory();
			
			
			map.put("listType", listType);
			map.put("banner", banner);
			map.put("infoList", infoList);
			map.put("serverMap", serverMap);
			map.put("userName", userName);
			map.put("cateList", cateList);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
		
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到精准投放页面
	 * 创建人： sx
	 * 创建时间： 2017年5月2日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/publishPut")
	public String toPublishPut(HttpServletRequest request, Model model){
		log.info("跳转到----------------精准投放页面------");
		createParameterMap(request);
		try {
			String userName = (String)paramsMap.get("userName");
			//查询发布类型
			List<Map<String, Object>> listType = publishService.getPublishType();
			//查询banner图
			Map<String, Object> banner = publishService.getBanner("jztf");
			//查询账号信息
			List<Map<String, Object>> infoList = publishService.getInfoListJztf(paramsMap);
			//查询优选服务
			Map<String, Object> serverMap = publishService.preferrServiceJztf();
			//查询一级分类
			String cateList = publishService.getFirstCategory();
			model.addAttribute("listType", listType);
			model.addAttribute("banner", banner);
			model.addAttribute("infoList", infoList);
			model.addAttribute("serverMap", serverMap);
			model.addAttribute("userName", userName);
			model.addAttribute("cateList", cateList);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "/wap/xkh_version_2_3/businesspublish/publish_put";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据一级分类查询二级分类
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/getSecondCategory")
	@ResponseBody
	public List<Map<String, Object>> getSecondCategory(HttpServletRequest request, Model model){
		createParameterMap(request);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			String id = (String)paramsMap.get("id");
			list = publishService.getSecondCategory(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：提交精准需求
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/addJzxq")
	@ResponseBody
	public boolean addJzxq(HttpServletRequest request, Model model){
		createParameterMap(request);
		boolean flag = true;
		try {
			flag = publishService.addJzxq(paramsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到提交成功页面
	 * 创建人： sx
	 * 创建时间： 2017年5月3日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/cue")
	public String cue(HttpServletRequest request, Model model){
		return "/wap/xkh_version_2_3/cue";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：优选服务加入已选
	 * 创建人： sx
	 * 创建时间： 2017年5月4日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/addPreferr")
	@ResponseBody
	public Map<String, Object> addPreferr(HttpServletRequest request, Model model){
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String purOpenId = getCurrentOpenId(request);//从session中获取openId
			//String purOpenId = "oHxmUjgTv3PnsjNmNH5HTQIgO10U";
			paramsMap.put("purOpenId", purOpenId);
			boolean flag = publishService.addPreferr(paramsMap);
			map.put("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}

	@RequestMapping(value = "/addSJOrder")
	public @ResponseBody Map<String, Object> addSJOrder(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);
		String pid = "";
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			String openId = getCurrentOpenId(request);//从session中获取openId
			paramsMap.put("openId", openId);
			pid = publishService.addOrder(paramsMap);
			
			map.put("pid", pid);
			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：支付当前订单  状态更改为2 支付成功
	 * 创建人： sx
	 * 创建时间： 2017年5月5日
	 * 标记：
	 * @param request
	 * @param model
	 * @version
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/updateOrderSelltoPay")
	public void updateOrderSelltoPay(HttpServletRequest request, HttpServletResponse response){
		createParameterMap(request);
		Map<String,Object> signMap = new HashMap<String,Object>();
		try {
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			//String openId = "oHxmUjgTv3PnsjNmNH5HTQIgO10U";
			String status = (String)paramsMap.get("status");
			String commodityName = "";
			if(status != null && !"".equals(status) && status.equals("jztf")){
				commodityName = "精准投放";
			}else{
				commodityName = "精准咨询";
			}
			String totalPrice = paramsMap.get("payMoney").toString();
			String pid = paramsMap.get("pid").toString();
			String clientFlag = "xkh";
			String sendUrl = "http://xydk.xiaoka360.com/updatePay/payOldSuccess.html";
			String keyUrlOrderId = "wxPay_xkh|http://xydk.xiaoka360.com/updatePay/payNewSuccess.html|"+pid;
			String systemFlag = "xkh";
			signMap.put("openId",openId);
			signMap.put("commodityName",commodityName);
			signMap.put("totalPrice",totalPrice);
			signMap.put("pid",pid);
			signMap.put("clientFlag",clientFlag);
			signMap.put("sendUrl",sendUrl);
			signMap.put("keyUrlOrderId",keyUrlOrderId);
			commodityName = URLEncoder.encode(commodityName);
			String sign = AccessCore.sign(signMap, contants.getWx_signKey());//拿到签名数据
			String url = "http://klgj.xiaoka360.com/wm-plat/api/jumpNewPay.html" + "?openId="+openId+"&clientFlag="+clientFlag+"&sign="+sign
					+ "&sendUrl="+sendUrl+"&keyUrlOrderId="+keyUrlOrderId+"&commodityName="+commodityName+"&totalPrice="+totalPrice+"&orderId="+pid+"&systemFlag="+systemFlag;
			response.sendRedirect(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//处理List 以key value的形式展示
	public Map<String,String> dealListForOrdDet(List<OrderDetail> orderList){
		Map<String,String> result = new HashMap<String, String>();
		for (OrderDetail orderDetail : orderList) {
			 result.put(orderDetail.getTypeName(), orderDetail.getTypeValue());
		}
		return result;
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到精准投放下订单页面
	 * 创建人： sx
	 * 创建时间： 2017年5月9日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/toPutOrder")
	public String toPutOrder(HttpServletRequest request, Model model){
		createParameterMap(request);
		try {
			//json数组
			String obj = (String)paramsMap.get("obj");
			log.info("跳转到----------------下单页面------"+obj);
			obj = obj.replaceAll("\"", "\'");
			//价格
			String price = (String)paramsMap.get("price");
			//查询精准投放
			Map<String, Object> serverMap = publishService.preferrServiceJztf();
			model.addAttribute("obj", obj);
			model.addAttribute("price", price);
			model.addAttribute("serverMap", serverMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/wap/xkh_version_2_3/publishPut_order";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：精准投放下订单
	 * 创建人： sx
	 * 创建时间： 2017年5月9日
	 * 标记：
	 * @param request
	 * @param model
	 * @version
	 */
	@RequestMapping(value="/addPutOrder")
	public String addPutOrder(HttpServletRequest request, Model model){
		createParameterMap(request);
		String pid = "";
		try {
			String openId = getCurrentOpenId(request);//从session中获取openId
			//String openId = "oHxmUjgTv3PnsjNmNH5HTQIgO10U";
			paramsMap.put("openId", openId);
			pid = publishService.addPutOrder(paramsMap);
			
			//选中的人
			List<Map<String, Object>> list = publishService.findBasicInfo(paramsMap);
			/*Date createDate = (Date)map.get("createDate");
			int orderStatus = (int)map.get("orderStatus");*/
			Date createDate = new Date();
			int orderStatus = 1;
			model.addAttribute("paramsMap", paramsMap);
			model.addAttribute("pid", pid);
			model.addAttribute("createDate", createDate);
			model.addAttribute("orderStatus", orderStatus);
			model.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "redirect:/publish/seachPubDetail.html?pid="+pid;
	}
	
	/**
	 * 
	 * 描述:跳转到发布中的精准咨询的支付页面
	 * 作者:gyp
	 * @Date	 2017年5月31日
	 */
	@RequestMapping(value="/toPay")
	@ResponseBody
	public void toPay(HttpServletRequest request, HttpServletResponse response, Model model){
		log.info("跳转到----------------发票支付页面------");
		createParameterMap(request);
		try {
			String price = (String)paramsMap.get("price");
			String pid = (String)paramsMap.get("pid");
			Map<String,Object> signMap = new HashMap<String,Object>();
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
			String commodityName = "精准咨询支付";
			//String totalPrice = price;
			String totalPrice = price;
			String id = pid;
			String clientFlag = "xkh";
			String sendUrl = "http://test.xydk.xiaoka360.com/publish/payOldSuccess.html";
			String keyUrlOrderId = "wxPay_xkh|http://test.xydk.xiaoka360.com/publish/paySuccess.html|"+id;
			String systemFlag = "xkh";
			signMap.put("openId",openId);
			signMap.put("commodityName",commodityName);
			signMap.put("totalPrice",totalPrice);
			signMap.put("orderId",id);
			signMap.put("clientFlag",clientFlag);
			signMap.put("sendUrl",sendUrl);
			signMap.put("keyUrlOrderId",keyUrlOrderId);
			commodityName = URLEncoder.encode(commodityName);
			String sign = AccessCore.sign(signMap, contants.getWx_signKey());//拿到签名数据
			String url = "http://klgj.xiaoka360.com/wm-plat/api/jumpPay.html" + "?openId="+openId+"&clientFlag="+clientFlag+"&sign="+sign
					+ "&sendUrl="+sendUrl+"&keyUrlOrderId="+keyUrlOrderId+"&commodityName="+commodityName+"&totalPrice="+totalPrice+"&orderId="+id+"&systemFlag="+systemFlag;
			response.sendRedirect(url);	
			
			
			model.addAttribute("totalPrice", totalPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * moblie:移动端
	 *	同步更新
	 * 支付成功后主动发送过来的携带：OrderId ,out_trade_no.用于更新订单状态
	 */
	@RequestMapping(value = "/paySuccess")
	public @ResponseBody void paySuccess(HttpServletRequest request, HttpServletResponse response,Model model){
		createParameterMap(request);
		//判断签名
		boolean verify = AccessCore.verify(paramsMap,contants.getWx_signKey());
		/*log.info("++++++++++++++++++++++++++++++++++++++判断签名信息，是否被更改="+verify);
		log.info("++++++++++++++++++++++++++++++++++++++同步更新订单状态");*/
		String outTradeNo = request.getParameter("out_trade_no");
		String pid = request.getParameter("orderId");
		
		log.info("++++++++++++++++++++++++++++++++++++++++++outTradeNo="+outTradeNo+"----orderId="+pid);
		//具体更新订单状态
		try {
			//更新支付的信息
			boolean flag = publishService.updateOrderById(pid, outTradeNo);//更新付款后的状态
			
			//发送模板消息到指定的用户下
			List<Map<String,Object>> list = publishDao.getPublishOrder(pid);
			//发送模板消息给买家
			String openId=list.get(0).get("openId").toString();
			String payMoney = publishDao.getPubSumMoney(pid);
			sendMessageService.payMoneySuccess(pid, openId, payMoney);;
			
			//给卖家发送模板信息
			for (Map<String, Object> map : list) {
				
				sendMessageService.remindOrderToSellerOrderSendMessage(map.get("orderId").toString());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:点击完成
	 * 作者:gyp
	 * @Date	 2017年5月27日
	 */
	@RequestMapping(value = "/payOldSuccess")
	public String payOldSuccess(HttpServletRequest request, HttpServletResponse response,Model model){
		String pid = request.getParameter("orderId");
		log.info("-----------------XKH进入跳转,点击完成或者返回"+pid);
		return  "redirect:/publish/seachPubDetail.html?pid="+pid;
		/*mov.setViewName("redirect:/groupPurchase/groupPurchaseList.html");*/
	}
	
	
	
	/**
	 * 查询当前商家发布订单的详细信息
	 */
	@RequestMapping(value="/seachPubDetail")
	public String seachPubDetail(HttpServletRequest request, Model model){
		
		createParameterMap(request);
		
		//默认此订单非本人点击观看
		boolean isMyself = false;
		
		//判定当前的订单状态
		List<String> orderStatu = new ArrayList<String>();
		
		//默认获取当前时间+1小时
		long timmer = DateUtil.addHour(new Date(), 1).getTime(); 
		
		//获取当前用户OpenId 
		String openId = getCurrentOpenId(request);
		
		//跳转地址
		String retUrl = "";
		
		if(paramsMap.containsKey("pid")){
			
			orderStatu = publishService.getPublishOrder(paramsMap.get("pid").toString());
			
			String orderStauts = StringUtil.judgeOrderStatu(orderStatu);
			
			List<OrderAndUserInfo> list =  
					orderSeaService.getAllOrderForSeller(paramsMap.get("pid").toString());
			
			if(list.isEmpty()){
				
				System.out.println("-----当前集合为空list集合订单编号为："+paramsMap.get("pid").toString());
				return "/error";
			}
			
			
			//获取当前订单的超时时间
			if(list.size()>0&&orderStauts.equals("1")){
				
				if(list.get(0).getOrderMessage().get("invalidTime")!=null&&
						StringUtils.isNotBlank(list.get(0).getOrderMessage().get("invalidTime").toString())){
					
					Date date = (Date) list.get(0).getOrderMessage().get("invalidTime");
					timmer = date.getTime();// TODO 拒绝订单时时间问题获取当前date时间
					
					Date dates = new Date(timmer);
					timmer = dates.getTime();
					System.out.println(dates);
					
				}
				
			}
			
			//确定是否为本人下单
			if(list.get(0).getOrderMessage().get("openId")!=null&&
					StringUtils.isNotBlank(list.get(0).getOrderMessage().get("openId").toString())){
				
				log.error("---------------获取当前用户的OpenId值------------"+openId);
				log.error("---------------获取当前订单的OpenId值------------"+openId);
				if(openId.equals(list.get(0).getOrderMessage().get("openId").toString())){
					isMyself = true;
				}
			}
			//若是精准投放增加标题等需要展示的信息
			if("jztf".equals(list.get(0).getOrderMessage().get("enterType").toString())){
				retUrl = "wap/xkh_version_2_3/publishPut_detail_jztf";
				
			}
			//若是精准咨询增加标题等需要展示的信息
			if("jzzx".equals(list.get(0).getOrderMessage().get("enterType").toString())){
				//增加个性化参数更改回调地址
				retUrl = "wap/xkh_version_2_3/publishPut_detail_jzzx";
				
			}
			//获取当前pid下的总钱数
			String sumMoney = publishService.getPubSumMoney(paramsMap.get("pid").toString());
			//判定是否为买家本人操作
			model.addAttribute("isMyself",isMyself);
			model.addAttribute("timmer",timmer);
			model.addAttribute("orderStatus", Integer.parseInt(orderStauts));
			model.addAttribute("list", list);
			model.addAttribute("sumMoney", sumMoney);
			System.out.println(list.get(0).getOrderDetil().toString()+"****************打印当前的参数****************");
			return retUrl;
			
		}
		System.out.println("***********当前订单状态Pid为空*************");
		return retUrl;//
	}
	
	
	/**
	 * 未进行支付的时候取消订单
	 * 传入参数pid
	 * 
	 */
	@RequestMapping(value="/cancelPubOrder")
	public String cancelPubOrder(HttpServletRequest request, Model model){
		
		//获取当前用户的pId
		createParameterMap(request);
		//判断当前pid是否为空
		if(paramsMap.containsKey("pid")){
			//更新当前订单状态 变为已关闭状态
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("pid",paramsMap.get("pid"));
			map.put("orderStatus",10);
			if(publishService.updateOrderByPid(map)){
				log.info("-----修改Pid状态成功订单关闭Pid编号：------"+paramsMap.get("pid"));
				return "redirect:/ordersea/searchOrderSell.html";
			}
			log.info("-----修改Pid状态失败订单关闭Pid编号：------"+paramsMap.get("pid"));
			//跳转到首页
			return "/error";
		}
		//跳转到首页
		return "/error";
	}
	
	/**
	 * 支付之后没有人接单取消订单
	 * 传入参数pid
	 * 
	 */
	@RequestMapping(value="/cancelPayAfterOrder")
	public String cancelPayAfterOrder(HttpServletRequest request, Model model){
		
		//获取当前用户的pId
		createParameterMap(request);
		//判断当前pid是否为空
		if(paramsMap.containsKey("pid")){
			//更新当前订单状态 变为已关闭状态
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("pid",paramsMap.get("pid"));
			map.put("orderStatus",7);
			if(publishService.updateOrderByPid(map)){
				log.info("-----修改Pid状态成功订单关闭Pid编号：------"+paramsMap.get("pid"));
				return "redirect:/ordersea/searchOrderSell.html";
			}
			log.info("-----修改Pid状态失败订单关闭Pid编号：------"+paramsMap.get("pid"));
			//跳转到首页
			return "/error";
		}
		//跳转到首页
		return "/error";
	}
	
	
	/**
	 * 更改当前订单状态为删除
	 * 传入参数pid
	 * 
	 */
	@RequestMapping(value="/deleteOrderByPid")
	public String deleteOrderByPid(HttpServletRequest request, Model model){
		
		//获取当前用户的pId
		createParameterMap(request);
		//判断当前pid是否为空
		if(paramsMap.containsKey("pid")){
			//更新当前订单状态 变为已关闭状态
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("pid",paramsMap.get("pid"));
			map.put("isDelete",1);
			if(publishService.updateOrderByPid(map)){
				log.info("-----修改Pid状态成功订单关闭Pid编号：------"+paramsMap.get("pid"));
				return "redirect:/ordersea/searchOrderSell.html";
			}
			log.info("-----修改Pid状态失败订单关闭Pid编号：------"+paramsMap.get("pid"));
			//跳转到首页
			return "/error";
		}
		//跳转到首页
		return "/error";
	}
	
	/**
	 * 获取当前被选中的省份
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/getSelectProvinceSchool")
	@ResponseBody
	public List<LeaveAndSchool> getSelectProvinceSchool( HttpServletRequest request,Model model){
		
		createParameterMap(request);
		List<LeaveAndSchool> list = new ArrayList<LeaveAndSchool>();
		
		list = publishService.getSelectProvinceSchool(paramsMap.get("prvinces").toString());
		
		return list;
	}
	
	
}

