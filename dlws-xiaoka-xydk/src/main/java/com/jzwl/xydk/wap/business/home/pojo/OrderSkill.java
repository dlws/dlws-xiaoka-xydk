package com.jzwl.xydk.wap.business.home.pojo;

import java.util.List;

import com.jzwl.system.base.pojo.BasePojo;


/**
 * 订单信息
 * @author dxf
 *
 */
public class OrderSkill extends BasePojo implements java.io.Serializable{

	private static final long serialVersionUID = 5454155825314635342L;
	
	public static final String TABLE_ALIAS = "Order";
	public static final String ALIAS_orderId = "orderId";
	public static final String ALIAS_payMoney = "付款金额";
	public static final String ALIAS_openId = "openId";
	public static final String ALIAS_sellerOpenId = "卖家openId";
	public static final String ALIAS_nickname = "用户昵称";
	public static final String ALIAS_headPortrait = "用户头像";
	public static final String ALIAS_skilId = "技能Id";
	public static final String ALIAS__skillName = "技能名称";
	public static final String ALIAS_serviceType = "服务类型（0：线上服务，1：线下服务）";
	public static final String ALIAS_skillDepict = "技能描述";
	public static final String ALIAS_tranNum = "购买数量";
	public static final String ALIAS_sellNo = "销售量";
	public static final String ALIAS_payTime = "付款时间";
	public static final String ALIAS_startDate = "服务开始时间";
	public static final String ALIAS_endDate = "服务结束时间";

	private java.lang.Long orderId;
	
	private java.lang.Double payMoney;
	
	private java.lang.String openId;
	
	private java.lang.String sellerOpenId;
	
	private java.lang.String nickname;
	
	private java.lang.String headPortrait;
	
	private List<UserSkillinfo> skill;
	
	private java.lang.Integer tranNum;
	
	private java.util.Date payTime;
	private java.util.Date startDate;
	private java.util.Date endDate;
	
	public java.lang.Integer getTranNum() {
		return tranNum;
	}
	public void setTranNum(java.lang.Integer tranNum) {
		this.tranNum = tranNum;
	}
	public List<UserSkillinfo> getSkill() {
		return skill;
	}
	public void setSkill(List<UserSkillinfo> skill) {
		this.skill = skill;
	}
	public java.lang.Long getOrderId() {
		return orderId;
	}
	public void setOrderId(java.lang.Long orderId) {
		this.orderId = orderId;
	}
	public java.lang.Double getPayMoney() {
		return payMoney;
	}
	public void setPayMoney(java.lang.Double payMoney) {
		this.payMoney = payMoney;
	}
	public java.lang.String getOpenId() {
		return openId;
	}
	public void setOpenId(java.lang.String openId) {
		this.openId = openId;
	}
	public java.lang.String getSellerOpenId() {
		return sellerOpenId;
	}
	public void setSellerOpenId(java.lang.String sellerOpenId) {
		this.sellerOpenId = sellerOpenId;
	}
	public java.lang.String getNickname() {
		return nickname;
	}
	public void setNickname(java.lang.String nickname) {
		this.nickname = nickname;
	}
	public java.lang.String getHeadPortrait() {
		return headPortrait;
	}
	public void setHeadPortrait(java.lang.String headPortrait) {
		this.headPortrait = headPortrait;
	}
	public java.util.Date getPayTime() {
		return payTime;
	}
	public void setPayTime(java.util.Date payTime) {
		this.payTime = payTime;
	}
	public java.util.Date getStartDate() {
		return startDate;
	}
	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}
	public java.util.Date getEndDate() {
		return endDate;
	}
	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public static String getTableAlias() {
		return TABLE_ALIAS;
	}
	public static String getAliasOrderid() {
		return ALIAS_orderId;
	}
	public static String getAliasPaymoney() {
		return ALIAS_payMoney;
	}
	public static String getAliasOpenid() {
		return ALIAS_openId;
	}
	public static String getAliasSelleropenid() {
		return ALIAS_sellerOpenId;
	}
	public static String getAliasNickname() {
		return ALIAS_nickname;
	}
	public static String getAliasHeadportrait() {
		return ALIAS_headPortrait;
	}
	public static String getAliasSkilid() {
		return ALIAS_skilId;
	}
	public static String getAliasSkillname() {
		return ALIAS__skillName;
	}
	public static String getAliasServicetype() {
		return ALIAS_serviceType;
	}
	public static String getAliasSkilldepict() {
		return ALIAS_skillDepict;
	}
	public static String getAliasTrannum() {
		return ALIAS_tranNum;
	}
	public static String getAliasSellno() {
		return ALIAS_sellNo;
	}
	public static String getAliasPaytime() {
		return ALIAS_payTime;
	}
	public static String getAliasStartdate() {
		return ALIAS_startDate;
	}
	public static String getAliasEnddate() {
		return ALIAS_endDate;
	}
	
}
