package com.jzwl.xydk.wap.xkh2_3.skill_detail.dao;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.system.base.dao.BaseDAO;


@Repository
public class SkillDetailDao {

	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	
	
	/**
	 * 连接father表和son表 查询出当前的skill相关信息
	 * @param skillId
	 * @return
	 */
	public Map<String,Object> getSkillInfo(String skillId){
		
		String sql = "SELECT *,uif.sellNo as sellNo,uif.id as id,usf.className as fathName,usf.classValue as fathValue,uss.className as sonName,uss.classValue as sonValue FROM `xiaoka-xydk`.user_skillinfo uif "
				+" LEFT JOIN  `xiaoka-xydk`.user_skillclass_father usf ON uif.skillFatId = usf.id "
				+" LEFT JOIN  `xiaoka-xydk`.user_skillclass_son uss ON uif.skillSonId = uss.id where uif.isDelete=0 and uif.id= ?";
		if(skillId!=null&&StringUtils.isNotBlank(skillId)){
			
			return baseDAO.getJdbcTemplate().queryForMap(sql, new Object[] {skillId});
		}
		
		return null;
	}
	
	/**
	 * 根据字典中的Id值查询当前单位并返回一个String 类型的单位
	 * 
	 */
	public String getUnitById(String id){
		
		String dicName;
		try {
			String sql ="select * from  `xiaoka`.v2_dic_data vd where  vd.id='"+id+"'";
			Map<String,Object> map = baseDAO.getJdbcTemplate().queryForMap(sql);
			dicName = map.get("dic_name").toString();
		} catch (Exception e) {
			dicName="次";
		}
		return dicName;
		
	}
	
	
	
}
