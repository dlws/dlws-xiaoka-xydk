package com.jzwl.xydk.wap.skillUser.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.json.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.log.SystemLog;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.utils.HttpClientUtil;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;

@Controller
@RequestMapping("/skillUser")
public class SkillUserController extends BaseController {
	@Autowired
	private SystemLog systemLog;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private Constants constants;
	@Autowired
	private EntertypeService entertypeService;
	@Autowired
	private PreferredServiceService preferredServiceService;
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private HomeService homeService;

	private static Logger log = LoggerFactory
			.getLogger(SkillUserController.class);

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：去添加技能用户基本信息页面 创建人： ln--wxl修改 创建时间： 2016年10月12日
	 * 标记：wap
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toAddSkillUser")
	public String toAddSkillUser(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			// 获取个人信息banner图

			Map<String, Object> bannerMap = new HashMap<String, Object>();
			List<BannerInfo> userBanner = skillUserService.getUserBanner();
			if (userBanner.size() > 0) {
				model.addAttribute("skillBannerInfo", userBanner.get(0));
			} else {
				model.addAttribute("skillBannerInfo", null);
			}

			if (userBanner.size() > 0) {
				bannerMap.put("firstPic", userBanner.get(0));
				bannerMap.put("lastPic", userBanner.get(userBanner.size() - 1));
			}
			bannerMap.put("list", bannerMap);
			// String jBanner = Json.toJson(bannerMap);//转json格式，需要时解开注释
			model.addAttribute("userBannerInfo", userBanner);

			// 获取用户基本信息
			/*
			 * request.getSession().setAttribute("dkOpenId",
			 * "oWGZPs3L5b9OtPD1dW12KOF8-oJE");
			 */
			String openId = String.valueOf(request.getSession().getAttribute(
					"dkOpenId"));
			log.info("openId=" + openId);
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> categoryList = new ArrayList<Map<String, Object>>();
			// 获取城市信息
			List<Map<String, Object>> cityList = skillUserService
					.queryCityList();
			// 不分页获取技能大类
			List<Map<String, Object>> maxList = skillUserService
					.queryMaxCategoryList();

			for (int i = 0; i < cityList.size(); i++) {
				String cityId = String.valueOf(cityList.get(i).get("id"));
				Map<String, Object> map = new HashMap<String, Object>();
				// 获取学校信息
				List<Map<String, Object>> schoolByCity = skillUserService
						.querySchoolList(cityId);
				if (schoolByCity.size() > 0) {
					map.put("text", cityList.get(i).get("cityName"));
					map.put("value", cityId);
					map.put("children", schoolByCity);
				} else {
					continue;
				}
				list.add(map);
			}
			String jsonSchool = JSONObject.toJSONString(list);
			for (int i = 0; i < maxList.size(); i++) {
				String mId = String.valueOf(maxList.get(i).get("id"));

				Map<String, Object> map = new HashMap<String, Object>();
				// 根据技能大类ID获取技能小类信息
				List<Map<String, Object>> minListByMaxId = skillUserService
						.queryMinCategoryByMaxId(mId);
				if (minListByMaxId.size() > 0) {
					map.put("text", maxList.get(i).get("className"));
					map.put("value", mId);
					map.put("children", minListByMaxId);
				} else {
					continue;
				}
				categoryList.add(map);
			}
			String jsonCategory = JSONObject.toJSONString(categoryList);
			model.addAttribute("citySchoolList", jsonSchool);
			model.addAttribute("maxMinCategory", jsonCategory);
			// 判断用户是否存在，如果存在获取用户技能信息 ，true表示存在，false表示不存在
			// String divFlag = "1";//用于页面切换
			boolean isUser = false;
			// 获取基本信息，根据openid，测试openid为null
			Map<String, Object> userInfoMap = skillUserService
					.isUserExist(openId);
			if (userInfoMap != null && userInfoMap.get("id") != null) {
				isUser = true;// 表示存在
				model.addAttribute("basicInfo", userInfoMap);
			} else {
				model.addAttribute("basicInfo", null);
			}
			return "/wap/xkh_version_2/myInfo";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：去添加技能用户基本信息页面 创建人： dxf 创建时间： 2016年2月23日 标记：wap
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/chooseStyle")
	public String chooseStyle(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			PageObject po = new PageObject();
			po = entertypeService.queryEntertypeList(paramsMap);
			model.addAttribute("enTypelist", po.getDatasource());
			return "wap/xkh_version_2/choose_style";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/home/index.html";
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：去添加技能用户基本信息页面 创建人： dxf 创建时间： 2016年2月23日 标记：wap
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toAddSkillUserV")
	public String toAddSkillUserV(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			String openId = String.valueOf(request.getSession().getAttribute(
					"dkOpenId"));
			// 获取个人信息banner图
			List<BannerInfo> userBanner = skillUserService.getUserBanner();
			model.addAttribute("userBannerInfo", userBanner);
			if (userBanner.size() > 0) {
				model.addAttribute("skillBannerInfo", userBanner.get(0));
			} else {
				model.addAttribute("skillBannerInfo", null);
			}

			log.info("openId=" + openId);
			List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表

			List<Map<String, Object>> provnceList = skillUserService
					.queryProviceListVersion2();
			for (int i = 0; i < provnceList.size(); i++) {
				String provnce = String.valueOf(provnceList.get(i).get(
						"province"));
				Map<String, Object> map = new HashMap<String, Object>();
				// 获取市信息
				List<Map<String, Object>> CityByProvnce = skillUserService
						.queryCityListVersion2(provnce);
				if (CityByProvnce != null && CityByProvnce.size() > 0) {
					map.put("value", provnce);// 市
					map.put("text", provnce);
					map.put("children", CityByProvnce);
				}
				proList.add(map);
			}
			String jsonProList = JSONObject.toJSONString(proList);
			model.addAttribute("proList", jsonProList);
			// 判断用户是否存在，如果存在获取用户技能信息 ，true表示存在，false表示不存在
			boolean isUser = false;
			Map<String, Object> userInfoMap = skillUserService
					.isUserExist(openId);// 用户基本信息
			if (userInfoMap != null && userInfoMap.get("id") != null) {
				isUser = true;// 表示存在
				model.addAttribute("basicInfo", userInfoMap);
			} else {
				model.addAttribute("basicInfo", null);
			}
			return "wap/xkh_version_2/enterInfo";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "wap/xkh_version_2/enterInfo";
	}

	/**
	 * 
	 * 获取(优选服务及服务)信息---2.1版本 预写接口
	 * <p>
	 * wxl openid
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return 两个页面
	 */
	@RequestMapping(value = "/toSkillInfoV2")
	public String toSkillInfoV2(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		// 获取发布banner图
		Map<String, Object> bannerMap = new HashMap<String, Object>();
		List<BannerInfo> skillBanner = skillUserService.getSkillBanner();
		if (skillBanner.size() > 0) {
			bannerMap.put("firstPic", skillBanner.get(0));
			bannerMap.put("lastPic", skillBanner.get(skillBanner.size() - 1));
		}
		bannerMap.put("list", bannerMap);
		if (skillBanner.size() > 0) {
			model.addAttribute("skillBannerInfo", skillBanner.get(0));
		} else {
			model.addAttribute("skillBannerInfo", null);
		}

		// 获取openId
		String openId = String.valueOf(request.getSession().getAttribute(
				"dkOpenId"));// 线上正式
		// String openId = "88";//测试使用
		Map<String, Object> userInfoMap = skillUserService.isUserExist(openId);

		// 判断用户是否存在
		boolean isUser = false;
		if (userInfoMap != null && userInfoMap.get("id") != null) {
			isUser = true;// 表示存在
		} else {
			isUser = false;// 表示不存在
		}
		if (isUser == true) {// 若存在去获取技能信息
			Map<String, Object> m1 = new HashMap<String, Object>();
			String id = String.valueOf(userInfoMap.get("id"));
			String enterId = "";
			if(null == userInfoMap.get("enterId")){
				enterId = "1489632337738001";
			}else{
				enterId = userInfoMap.get("enterId").toString();
			}
			//String enterId = userInfoMap.get("enterId").toString();
			// 获取优选服务信息
			/*
			 * PageObject po = new PageObject(); paramsMap.put("enterClass",
			 * enterId); paramsMap.remove("isDelete");
			 */

			// po =
			// preferredServiceService.queryPreferredServiceList(paramsMap);
			List<Map<String, Object>> ser_map = preferredServiceService
					.getService(enterId);
			if (ser_map.size() > 0) {
				String jsonPreferClass = Json.toJson(ser_map);// 优选服务转json
				String preferList = "{\"preferList\":" + jsonPreferClass + "}";
				model.addAttribute("preferList", preferList);
				model.addAttribute("prefersize", ser_map.size());
			} else {
				model.addAttribute("preferList", 1);
				model.addAttribute("prefersize", ser_map.size());
			}

			// 服务list
			List<Map<String, Object>> skillInfoList = skillUserService
					.querySkillInfo(id);
			if (skillInfoList.size() > 0) {
				// 存在服务信息----->使用模板显示服务信息
				for (int i = 0; i < skillInfoList.size(); i++) {// 去第一张图片到技能列表前封面

					String str = skillInfoList.get(i).get("details").toString();
					if (str.length() > 30) {
						String strdetails = str.substring(0, 30) + "...";
						skillInfoList.get(i).put("details", strdetails);
						// maxList.get(i).set("details", strdetails);
					} else {
						String strdetails = str;
						skillInfoList.get(i).put("details", strdetails);
					}

					String skillId = String.valueOf(skillInfoList.get(i).get(
							"id"));
					
					
					
					
					List<Map<String, Object>> listSkillImgInfo = skillUserService
							.getSkillImgBySkillList(skillId);
					if (listSkillImgInfo.size() > 0) {
						// 获取技能()图
						String imgStr = String.valueOf(listSkillImgInfo.get(0)
								.get("imgURL"));
						log.info("++++++++++++++++++++++++++++列表封面图片" + imgStr);
						skillInfoList.get(i).put("imageStr", imgStr);
						for (int j = 0; j < listSkillImgInfo.size(); j++) {
							String imageUrl = String.valueOf(listSkillImgInfo
									.get(j).get("imgURL"));
							imageUrl = constants
									.getXiaoka_weixin_staticresource_path()
									+ imageUrl;
						}
					}
				}

				m1.put("skillList", skillInfoList);
				String basicId = skillInfoList.get(0).get("basicId").toString();
				String isDisplay = skillInfoList.get(0).get("isDisplay")
						.toString();
				String skillStr = JSONObject.toJSONString(m1);
				String spec = "{\"classItem\":" + skillStr + "}";
				model.addAttribute("skillInfoList", skillInfoList);
				model.addAttribute("basicId", basicId);
				model.addAttribute("isDisplay", isDisplay);
				model.addAttribute("skillList", skillStr);
				model.addAttribute("spec", skillStr);
				model.addAttribute("skillsize", skillInfoList.size());
				return "wap/xkh_version_2/service";
			} else {
				// 没有服务信息
				model.addAttribute("skillStrList", false);
				model.addAttribute("isDisplay", "0");
				model.addAttribute("basicId", false);
				model.addAttribute("skillsize", skillInfoList.size());
				model.addAttribute("spec", 1);
				return "wap/xkh_version_2/service";
			}
		} else {
			return "redirect:/home/index.html";// 跳转到首页
		}

	}

	/*
	 * @RequestMapping(value = "/toSkillInfoV21") public String
	 * toSkillInfoV21(HttpServletRequest request, HttpServletResponse response,
	 * Model model) {
	 * 
	 * // 获取发布banner图 Map<String, Object> bannerMap = new HashMap<String,
	 * Object>(); List<BannerInfo> skillBanner =
	 * skillUserService.getSkillBanner(); if (skillBanner.size() > 0) {
	 * bannerMap.put("firstPic", skillBanner.get(0)); bannerMap.put("lastPic",
	 * skillBanner.get(skillBanner.size() - 1)); } bannerMap.put("list",
	 * bannerMap); if (skillBanner.size() > 0) {
	 * model.addAttribute("skillBannerInfo", skillBanner.get(0)); } else {
	 * model.addAttribute("skillBannerInfo", null); }
	 * 
	 * // 获取服务信息 String openId =
	 * String.valueOf(request.getSession().getAttribute("dkOpenId"));//线上正式 //
	 * String openId = "88";//测试使用 Map<String, Object> userInfoMap =
	 * skillUserService.isUserExist(openId); //查询基本信息
	 * 
	 * //判断用户是否存在 boolean isUser = false; if (userInfoMap != null &&
	 * userInfoMap.get("id") != null) { isUser = true;// 表示存在 } else { isUser =
	 * false;// 表示不存在 } // if (isUser == true) {// 若存在去获取技能信息 Map<String,
	 * Object> m1 = new HashMap<String, Object>(); String id =
	 * String.valueOf(userInfoMap.get("id")); String enterId =
	 * userInfoMap.get("enterId").toString(); //获取优选服务信息 PageObject po = new
	 * PageObject(); paramsMap.put("enterClass", enterId);
	 * paramsMap.remove("isDelete"); paramsMap.remove("skillName");
	 * paramsMap.remove("serviceType"); paramsMap.remove("skillName");
	 * paramsMap.remove("skillPrice"); paramsMap.remove("imageUrl");
	 * paramsMap.remove("skillSonId"); paramsMap.remove("company");
	 * paramsMap.remove("skillFatId"); paramsMap.remove("id");
	 * paramsMap.remove("createDate");
	 * 
	 * po = preferredServiceService.queryPreferredServiceList(paramsMap); if
	 * (po.getDatasource().size() > 0) { String jsonPreferClass =
	 * Json.toJson(po.getDatasource());//优选服务转json String preferList =
	 * "{\"preferList\":" + jsonPreferClass + "}";
	 * model.addAttribute("preferList", preferList); } else {
	 * model.addAttribute("preferList", 1); model.addAttribute("prefersize",
	 * po.getDatasource().size()); }
	 * 
	 * //服务list List<Map<String, Object>> skillInfoList =
	 * skillUserService.querySkillInfo(id); if (skillInfoList.size() > 0) {
	 * //存在服务信息----->使用模板显示服务信息 for (int i = 0; i < skillInfoList.size(); i++)
	 * {//去第一张图片到技能列表前封面
	 * 
	 * String str = skillInfoList.get(i).get("details").toString(); if
	 * (str.length() > 30) { String strdetails = str.substring(0, 30) + "...";
	 * skillInfoList.get(i).put("details", strdetails);
	 * //maxList.get(i).set("details", strdetails); } else { String strdetails =
	 * str; skillInfoList.get(i).put("details", strdetails); }
	 * 
	 * String skillId = String.valueOf(skillInfoList.get(i).get("id"));
	 * List<Map<String, Object>> listSkillImgInfo =
	 * skillUserService.getSkillImgBySkillList(skillId); if
	 * (listSkillImgInfo.size() > 0) { // 获取技能()图 String imgStr =
	 * String.valueOf(listSkillImgInfo.get(0).get("imgURL"));
	 * log.info("++++++++++++++++++++++++++++列表封面图片" + imgStr);
	 * skillInfoList.get(i).put("imageStr", imgStr); for (int j = 0; j <
	 * listSkillImgInfo.size(); j++) { String imageUrl =
	 * String.valueOf(listSkillImgInfo.get(j).get("imgURL")); imageUrl =
	 * constants.getXiaoka_weixin_staticresource_path() + imageUrl; } } }
	 * 
	 * m1.put("skillList", skillInfoList); String basicId =
	 * skillInfoList.get(0).get("basicId").toString(); String isDisplay =
	 * skillInfoList.get(0).get("isDisplay").toString(); String skillStr =
	 * JSONObject.toJSONString(m1); String spec = "{\"classItem\":" + skillStr +
	 * "}"; model.addAttribute("skillInfoList", skillInfoList);
	 * model.addAttribute("basicId", basicId); model.addAttribute("isDisplay",
	 * isDisplay); model.addAttribute("skillList", skillStr);
	 * model.addAttribute("spec", skillStr); model.addAttribute("skillsize",
	 * skillInfoList.size()); return "wap/xkh_version_2/service"; } else { //
	 * 没有服务信息 model.addAttribute("skillStrList", false);
	 * model.addAttribute("isDisplay", "0"); model.addAttribute("basicId",
	 * false); model.addAttribute("skillsize", skillInfoList.size());
	 * model.addAttribute("spec", 1); return "wap/xkh_version_2/service"; } }
	 * else { return "redirect:/home/index.html";//跳转到首页 }
	 * 
	 * }
	 */

	// 根据城市选择学校
	@RequestMapping(value = "/chooseSchool")
	public @ResponseBody List<Map<String, Object>> chooseSchool(
			HttpServletRequest request, HttpServletResponse response,
			Model model) {
		createParameterMap(request);

		String cityId = String.valueOf(paramsMap.get("cityId"));
		List<Map<String, Object>> schoolTypeList = new ArrayList<Map<String, Object>>();// 省市列表
		List<Map<String, Object>> schoolType = skillUserService
				.querySchollTypeVersion2(cityId);

		for (int i = 0; i < schoolType.size(); i++) {
			Map<String, Object> mapsc = new HashMap<String, Object>();
			if (schoolType.size() > 0 && schoolType != null) {
				String dcdaId = String.valueOf(schoolType.get(i).get("value"));
				List<Map<String, Object>> list = skillUserService
						.querySchollByTypeVersion2(cityId, dcdaId);
				mapsc.put("text", schoolType.get(i).get("text"));// 字典值（211/985）
				mapsc.put("value", schoolType.get(i).get("value"));// 字典id
				mapsc.put("children", list);
				schoolTypeList.add(mapsc);
			}
		}
		// String jsonSchoLList = JSONObject.toJSONString(schoolTypeList);
		return schoolTypeList;
	}

	/*
	 * @RequestMapping(value = "/getUserInfoByClassIdV2") public @ResponseBody
	 * Map<String,Object> getUserInfoByClassIdV2(HttpServletRequest
	 * request,HttpServletResponse response) {
	 * 
	 * createParameterMap(request);
	 * 
	 * List<UserSkillinfo> skills = null;//分类用户的集合 Map<String,Object> map = new
	 * HashMap<String,Object>(); String currentPage =
	 * request.getParameter("currentPage"); try {
	 * 
	 * int cur = Integer.parseInt(currentPage); int start = (cur-1)*2;
	 * paramsMap.put("start",start); skills =
	 * homeService.getUserSkillInfo(paramsMap);
	 * 
	 * map.put("msg", "获取信息成功"); map.put("ajax_status",
	 * AjaxStatusConstant.AJAX_STATUS_SUCCESS); map.put("num", skills.size());
	 * map.put("skills", skills); map.put("sid", paramsMap.get("sid"));
	 * 
	 * return map;
	 * 
	 * } catch (Exception e) { e.printStackTrace(); map.put("msg", "获取信息失败！");
	 * map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE); return
	 * map; } }
	 */

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：保存用户基本信息 创建人： dxf 创建时间： 2011年2月23日 标记：wap
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/addSkillUserBaseInfo")
	public String addSkillUser(HttpServletRequest request,
			HttpServletResponse response) {
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			String openId = String.valueOf(request.getSession().getAttribute(
					"dkOpenId"));
			log.info("openId=" + openId);
			paramsMap.put("openId", openId);
			// Map<String,Object> cityMap =
			// skillUserService.getCityCodeBySchoolId(paramsMap);
			// String cityId = String.valueOf(cityMap.get("cityId"));
			// paramsMap.put("cityId", cityId);
			String basId = String.valueOf(paramsMap.get("id"));

			boolean flag = false;
			if (paramsMap.get("id") == null || "".equals(basId)
					|| basId == null) {
				flag = skillUserService.addSkillUser(paramsMap);
			} else {
				flag = skillUserService.editBasicInfoVersion2(paramsMap);
			}

			map.put("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "用户信息异常");
			return "redirect:/home/index.html";// 跳转到首页
		}
		return "redirect:/home/getmyInform.html";
	}


	// 查询二级分类
	@RequestMapping(value = "/getClassSon")
	public @ResponseBody Map<String, Object> toGetTwoInfo(
			HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		String mId = paramsMap.get("fid").toString();
		List<Map<String, Object>> twoList = skillUserService
				.queryMinCategoryByMaxId(mId);

		String jsonclassSon = Json.toJson(twoList);
		String classSon = "{\"list\":" + jsonclassSon + "}";

		map.put("classSon", classSon);
		map.put("classList", twoList);

		return map;

	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：添加用户技能信息 创建人： ln--wxl修改 创建时间： 2016年10月18日 标记：
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/addSkillInformation")
	public String addSkillInformation(HttpServletRequest request,
			HttpServletResponse response) {
		createParameterMap(request);
		try {
			String openId = String.valueOf(request.getSession().getAttribute(
					"dkOpenId"));
			log.info("openId=" + openId);
			// 通过openid获取用户id
			Map<String, Object> userInfoMap = skillUserService
					.isUserExist(openId);
			String id = String.valueOf(userInfoMap.get("id"));
			// 用户id作为basicid
			paramsMap.put("basicId", id);
			Map<String, Object> mapMinCategory = skillUserService
					.getMinCategory(paramsMap);
			paramsMap.put("skillFatId", mapMinCategory.get("fatherId"));
			Map<String, Object> m = skillUserService
					.addSkillInformation(paramsMap);
			// 图片保存
			String imgStr = String.valueOf(paramsMap.get("imageUrl"));
			if (imgStr.contains(";")) {
				String[] split = imgStr.split(";");
				String skillId = String.valueOf(m.get("id"));
				for (int i = 0; i < split.length; i++) {
					boolean imgFlag = skillUserService.addImgBySkillId(skillId,
							split[i]);
					System.out.println("======================图片存储成功："
							+ imgFlag);
				}
			}
			return "redirect:/skillUser/toSkillInfoV2.html";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：用户Id获取技能信息列表 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/getSkillInfoUserId")
	public @ResponseBody Map getSkillInfoById(HttpServletRequest request,
			HttpServletResponse response) {
		createParameterMap(request);
		ModelAndView mov = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = String.valueOf(paramsMap.get("userId"));
			// 技能信息列表
			List<Map<String, Object>> skillInfo = skillUserService
					.querySkillInfo(id);

		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("redirect:/home/index.html");// 跳转到首页
		}
		return map;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：去修改技能信息（根据技能ID获取技能信息） 创建人： ln--wxl修改 创建时间：
	 * 2016年10月18日 标记：
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toEditSkillInfo")
	public String toEditSkillInfo(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		List<BannerInfo> skillBanner = skillUserService.getSkillBanner();

		if (skillBanner.size() > 0) {
			model.addAttribute("skillBannerInfo", skillBanner.get(0));
		} else {
			model.addAttribute("skillBannerInfo", null);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		try {
			String skillId = String.valueOf(paramsMap.get("skillId"));
			Map<String, Object> skillInfo = skillUserService
					.getSkillInfoById(skillId);

			// 获取技能大类
			List<Map<String, Object>> classList = skillUserService
					.queryMaxCategoryList();
			model.addAttribute("classList", classList);
			// 获取技能的二级分类
			String fid = skillInfo.get("skillFatId").toString();
			List<Map<String, Object>> classSonList = skillUserService
					.querySonCategoryList(fid);
			model.addAttribute("classSonList", classSonList);

			// 获取单位的集合
			List<Map<String, Object>> companyList = skillUserService
					.getDicData(GlobalConstant.DIC_TYPE_CODE_COMPANY);
			String jsoncompanyList = Json.toJson(companyList);
			model.addAttribute("company", jsoncompanyList);

			/*
			 * String str = String.valueOf(skillInfo.get("textURL"));
			 * if(str.length()==0){ skillInfo.put("textURL","0"); } String
			 * strVideo = String.valueOf(skillInfo.get("videoURL"));
			 * if(strVideo.length()==0){ skillInfo.put("videoURL","0"); }
			 */

			List<Map<String, Object>> skilList = new ArrayList<Map<String, Object>>();// 技能一级二级分类

			// List<Map<String, Object>> classList =
			// skillUserService.queryMaxCategoryList();

			for (int i = 0; i < classList.size(); i++) {
				String id = String.valueOf(classList.get(i).get("id"));
				String className = String.valueOf(classList.get(i).get(
						"className"));
				Map<String, Object> map1 = new HashMap<String, Object>();
				// 获取二级技能分类
				List<Map<String, Object>> twoList = skillUserService
						.queryMinCategoryByMaxId(id);
				if (twoList != null && twoList.size() > 0) {
					map1.put("value", id);//
					map1.put("text", className);
					map1.put("children", twoList);
					skilList.add(map1);
				}
			}
			String jsonskilList = JSONObject.toJSONString(skilList);
			model.addAttribute("skilList", jsonskilList);

			// 根据技能Id获取技能图片
			List<Map<String, Object>> listSkillImgInfo = skillUserService
					.getSkillImgBySkillList(skillId);
			/*
			 * if(listSkillImgInfo.size()>0){ for(int
			 * i=0;i<listSkillImgInfo.size();i++){ String imageUrl =
			 * String.valueOf(listSkillImgInfo.get(i).get("imgURL")); imageUrl =
			 * constants.getXiaoka_weixin_staticresource_path()+imageUrl;
			 * listSkillImgInfo.get(i).put("imgURL", imageUrl); } }
			 */
			/*
			 * map.put("skillInfo", skillInfo); map.put("listSkillImgInfo",
			 * listSkillImgInfo);
			 */
			// return map;

			model.addAttribute("skillInfo", skillInfo);
			model.addAttribute("listSkillImgInfo", listSkillImgInfo);
			return "wap/xkh_version_2/addskillEdit_2-1";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：修改技能信息 创建人： ln--wxl修改 创建时间： 2016年10月19日 标记：
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/editSkillInfo")
	public String editSkillInfo(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		try {
			Map<String, Object> mapMinCategory = skillUserService
					.getMinCategory(paramsMap);
			paramsMap.put("skillFatId", mapMinCategory.get("fatherId"));// 获取父级ID
			boolean flag = skillUserService.editSkillInfo(paramsMap);
			// 图片信息进行修改
			// 1,先删除
			boolean imgFlag = skillUserService.deleteImgBySkillId(paramsMap);
			// 2,重新添加
			String imgStr = String.valueOf(paramsMap.get("imgPath"));
			if (imgStr.contains(",")) {
				String[] split = imgStr.split(",");
				String skillId = String.valueOf(paramsMap.get("skillId"));
				for (int i = 0; i < split.length; i++) {
					boolean imgSaveFlag = skillUserService.addImgBySkillId(
							skillId, split[i]);
				}
			}
			// map.put("flag", flag);
			model.addAttribute("flag", flag);
			return "redirect:/skillUser/toSkillInfo.html";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}

	/**
	 * 修改技能信息
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/editSkillInfoV2")
	public String editSkillInfoV2(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		try {
			Map<String, Object> mapMinCategory = skillUserService
					.getMinCategory(paramsMap);
			boolean flag = skillUserService.editSkillInfo(paramsMap);

			String imgStr = String.valueOf(paramsMap.get("imageUrl"));
			if (imgStr.contains(";")) {
				String[] split = imgStr.split(";");
				String skillId = String.valueOf(paramsMap.get("id"));
				for (int i = 0; i < split.length; i++) {
					boolean imgSaveFlag = skillUserService.addImgBySkillId(
							skillId, split[i]);
					log.info("上传图片的fail   or  success+++++++++++:"
							+ imgSaveFlag);
				}
			}

			model.addAttribute("flag", flag);
			return "redirect:/skillUser/toSkillInfoV2.html";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 修改开启关闭状态
	 * <p>
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */

	@RequestMapping(value = "/updateDisplay")
	public String toUpdateDisplay(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {

			String str = paramsMap.get("isDisplay").toString();

			if (str.equals("0")) {
				// model.addAttribute("isDisplay", 1);
				paramsMap.put("isDisplay", 1);
			} else {
				// model.addAttribute("isDisplay", 0);
				paramsMap.put("isDisplay", 0);
			}
			boolean flag = skillUserService.updateDisplay(paramsMap);
			if (flag == true) {
				return "redirect:/skillUser/toSkillInfoV2.html";
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：修改基本信息 创建人： ln 创建时间： 2016年10月20日 标记：
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/editBasicInfo")
	public @ResponseBody Map editBasicInfo(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		try {
			Map<String, Object> cityMap = skillUserService
					.getCityCodeBySchoolId(paramsMap);
			String cityId = String.valueOf(cityMap.get("cityId"));
			paramsMap.put("cityId", cityId);
			boolean editBFlag = skillUserService.editBasicInfo(paramsMap);
			map.put("flag", editBFlag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;

	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：删除技能信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/deleteSkillInfo")
	public @ResponseBody Map deleteSkillInfo(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		ModelAndView mov = new ModelAndView();
		createParameterMap(request);
		try {
			boolean flag = skillUserService.deleteSkillInfo(paramsMap);
			if (null != paramsMap.get("isDelete")
					|| "" != paramsMap.get("isDelete")) {
				map.put("isDelete", paramsMap.get("isDelete"));
			}
			map.put("flag", flag);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("redirect:/home/index.html");// 跳转到首页
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：删除用户上传的技能图片 创建人： ln 创建时间： 2016年11月2日 标记：wap
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/delImg")
	public @ResponseBody Map delImg(HttpServletRequest request,
			HttpServletResponse response) {
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag = skillUserService.delImgById(paramsMap);
		map.put("flag", flag);
		return map;
	}

	/**
	 * 删除图片
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delImgV2")
	public @ResponseBody Map delImgV2(HttpServletRequest request,
			HttpServletResponse response) {
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag = skillUserService.delImgById(paramsMap);
		map.put("flag", flag);
		return map;
	}

	/**
	 * ajax方式获取js签名
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getJSConfig")
	public String getJSConfig(HttpServletRequest request, String clientUrl,
			HttpServletResponse response) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String resStr = null;
		try {
			String cUrl = clientUrl;
			cUrl = URLEncoder.encode(cUrl, "UTF-8");
			log.info("++++++++++++++++cUrl:" + cUrl);
			String url = constants.getXiaoka_weixin_xydk_toGetJsConfig();
			log.info("==================================" + clientUrl);
			resultMap = HttpClientUtil.requestByGetMethod(url + "?clientUrl="
					+ cUrl);
			log.info("<<<<<<<<<<<clientUrl=================" + clientUrl);
			resStr = (String) resultMap.get("token");
		} catch (Exception e) {
			e.printStackTrace();
			log.info("<<<<<<<<<<<ajax方式获取js签名失败<<<<<<<<<<<<");
			resultMap.put("msg", "0");
			return resStr;
		}
		return resStr;
	}

	@RequestMapping(value = "/downloadImageSelf", method = RequestMethod.POST)
	public void downloadImageSelf(HttpServletRequest request,
			HttpServletResponse response) {
		log.info("+++++++++++++++++++++进入后台去下载图片=");
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String serverId = String.valueOf(paramsMap.get("serverId"));

			if (null != serverId && serverId.length() > 0) {

				if (serverId.contains(";")) {// xkh2.0
					log.info("++++ 新版本 serverId" + serverId);

					String[] serverArr = serverId.split(";");

					String imgUrlStr = "";
					List<String> list = new ArrayList<String>();

					for (int i = 0; i < serverArr.length; i++) {

						String url = constants
								.getXiaoka_weixin_xydk_toDownloadImage()
								+ "?serverId=" + serverArr[i];
						map = HttpClientUtil.requestByGetMethod(url);
						String imgUrl = String.valueOf(map.get("token"));
						log.info("+++++++++++++++++++++imgUrl=" + imgUrl
								+ "+++++++serverId" + map);

						JSONObject json = JSONObject.parseObject(imgUrl);
						String path = String.valueOf(json.get("path"));

						imgUrlStr += imgUrl + ";";
						list.add(path);

					}

					// 返回JSON

					String json = Json.toJson(list);
					log.info("+++++++++++++++json++++++++++:" + json);

					response.setContentType("application/json;charset=UTF-8");
					response.getWriter().write(json);

				} else {// 旧版本
					log.info("++++++++++++++++++++++++++++serverId" + serverId);
					String url = constants
							.getXiaoka_weixin_xydk_toDownloadImage()
							+ "?serverId=" + serverId;
					map = HttpClientUtil.requestByGetMethod(url);
					String imgUrl = String.valueOf(map.get("token"));
					log.info("+++++++++++++++++++++imgUrl=" + imgUrl
							+ "+++++++serverId" + serverId);
					// 返回JSON
					response.setContentType("application/json;charset=UTF-8");
					response.getWriter().write(imgUrl);

				}

			} else {
				log.error("err serverId is" + serverId);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * 修改背景图片 dxf
	 */
	@RequestMapping(value = "/downloadImageBackPic", method = RequestMethod.POST)
	public void downloadImageBackPic(HttpServletRequest request,
			HttpServletResponse response) {
		log.info("+++++++++++++++++++++进入后台去下载图片=");
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String serverId = String.valueOf(paramsMap.get("serverId"));
			// 旧版本
			log.info("++++++++++++++++++++++++++++serverId" + serverId);
			String url = constants.getXiaoka_weixin_xydk_toDownloadImage()
					+ "?serverId=" + serverId;
			map = HttpClientUtil.requestByGetMethod(url);
			String imgUrl = String.valueOf(map.get("token"));

			JSONObject json = JSONObject.parseObject(imgUrl);
			String path = String.valueOf(json.get("path"));

			paramsMap.put("imgUrl", path);
//			System.out.println("imgUrl=" + path);
			skillUserService.upBackPicture(paramsMap);
			// System.out.println("baseUserId="+paramsMap.get("id"));
			log.info("+++++++++++++++++++++imgUrl=" + imgUrl
					+ "+++++++serverId" + serverId);
			// 返回JSON
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(imgUrl);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
