package com.jzwl.xydk.wap.xkh2_2.pay.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.pay.dao.UpdateOrderDao;

@Service("updateOrderService")
public class UpdateOrderService {

	private static Logger log = LoggerFactory
			.getLogger(UpdateOrderService.class);
	
	@Autowired
	private UpdateOrderDao updateOrderDao;
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更新订单状态和out_trade_No
	 * 创建人： ln
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param orderId
	 * @param outTradeNo
	 * @version
	 */
	public boolean updateOrder(String orderId, String outTradeNo) {
		return updateOrderDao.updateOrder(orderId,outTradeNo);
		
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据pid更改订单状态
	 * 创建人： sx
	 * 创建时间： 2017年5月8日
	 * 标记：
	 * @param orderId
	 * @param outTradeNo
	 * @return
	 * @version
	 */
	public boolean updateOrderByPid(String pid, String outTradeNo) {
		return updateOrderDao.updateOrder(pid,outTradeNo);
		
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据订单ID获取订单信息
	 * 创建人： ln
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param orderId
	 * @return
	 * @version
	 */
	public Map<String, Object> getOrderInfo(String orderId) {
		
		return updateOrderDao.getOrderInfo(orderId);
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据pid获取订单信息
	 * 创建人： sx
	 * 创建时间： 2017年5月8日
	 * 标记：
	 * @param pid
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getOrderInfoByPid(String pid) {
		
		return updateOrderDao.getOrderInfoByPid(pid);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据多组orderids查询订单
	 * 创建人： sx
	 * 创建时间： 2017年5月23日
	 * 标记：
	 * @param orderIds
	 * @return
	 * @version
	 */
	public List<Map<String,Object>> getOrderInfoByOrderIds(String orderIds) {
		return updateOrderDao.getOrderInfoByOrderIds(orderIds);
	}

}
