package com.jzwl.xydk.wap.xkhV2.xnsj.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.pojo.UserSkillinfoData;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserEnterInfoService;
import com.jzwl.xydk.wap.business.home.controller.HomeController;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;

/**
 * wap 版校内商家入驻
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("schoolBusi")
public class SchoolBusinessController extends BaseWeixinController {

	@Autowired
	private PreferredServiceService preferredServiceService;
	@Autowired
	private HomeService homeService;
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private UserEnterInfoService userenterInfoService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private Constants constants;

	Logger log = Logger.getLogger(HomeController.class);

	private final String BUSINTYPE = "businessType";// 校园商家类型
	private final String COMPARE_SIZE = "cooperaType";// 合作类型

	/**
	 * 跳转到添加页面，并且展示基础参数
	 * 
	 * @param response
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "toAddSchoolBusi")
	public String toAddSchoolBusi(HttpServletResponse response,
			HttpServletRequest request, Model model) {

		createParameterMap(request);
		model = toShowbasic(model);
		model.addAttribute("paramsMap", paramsMap);// enterId的传参方式更改
		return "/wap/xkh_version_2/seller_enterInfo";

	}

	/**
	 * 添加新增的用户
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addSchoolBusi")
	public String addSchoolBusin(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		Map<String, Object> map = createParameterMap(request);
		String openId = getCurrentOpenId(request); // get openId
		map.put("viewNum", "0");// 开始时默认为0
		map.put("enterId", paramsMap.get("enterId"));// 无此参数默认设置为空
		map.put("openId", openId);

		String cooperaTypeJson = ""; // 合作形式和价格
		ModelAndView mov = new ModelAndView();

		if (request.getParameterValues("cooperaType") != null
				&& request.getParameterValues("cooperaPrice") != null) {// 设置当前场地的属性

			String[] cooperaTyArray = request.getParameterValues("cooperaType");// 合作形式
			String[] cooperaPrArray = request
					.getParameterValues("cooperaPrice");// 报价

			cooperaTypeJson = StringUtil.dealArray(cooperaTyArray,
					cooperaPrArray);
		}
		map.put("cooperaType", cooperaTypeJson);

		System.out.println(cooperaTypeJson);

		String userName = String.valueOf(request.getSession().getAttribute(
				"userName"));
		if (!"".equals(userName)) {
			paramsMap.put("createUser", userName);
		} else {
			paramsMap.put("createUser", "admin");
		}
		boolean flag = userBasicinfoService.addSettleUserVersion3(map);

		if (flag) {
			return "redirect:/home/getmyInform.html";
		} else {
			return "redirect:/home/index.html";
		}

	}

	/**
	 * 当前用户去修改用户校园入驻的基本信息
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "toUpdateSchoolBusi")
	public String toUpdateSchoolBusi(HttpServletResponse response,
			HttpServletRequest request, Model model) {

		Map<String, Object> map = createParameterMap(request);
		String openId = String.valueOf(request.getSession().getAttribute(
				"dkOpenId"));
		System.out.println("***************openId**************" + openId);
		UserBasicinfo userInfo = homeService.getUserBaseInfoByOpenId(openId);
		paramsMap.put("id", userInfo.getId().toString());
		map.put("id", userInfo.getId().toString());

		Map<String, String> echoMap = new HashMap<String, String>();
		model = toShowbasic(model);
		Map<String, Object> objMap = userBasicinfoService.getById(map);// 获取当前的个人信息
		List<UserSkillinfoData> list = userenterInfoService
				.querySettleListVersion3(paramsMap);
		Map<String, Object> storageId = new HashMap<String, Object>();
		for (UserSkillinfoData userSkillinfoData : list) {
			echoMap.put(userSkillinfoData.getTypeName(),
					userSkillinfoData.getTypeValue());
			storageId.put(userSkillinfoData.getTypeName(), userSkillinfoData
					.getId().toString());
		}
		List<String> listClose = dealString(echoMap.get("closePhoto"));
		List<String> listMiddle = dealString(echoMap.get("middlePho"));
		List<String> listAll = dealString(echoMap.get("allPhoto"));
		JSONObject jasonObject = JSONObject.parseObject(echoMap
				.get("cooperaType"));
		Map<String, Object> cooperaTypes = (Map) jasonObject;
		model.addAttribute("paramsMap", paramsMap);// 公共参数
		model.addAttribute("objMap", objMap);// 个人信息加载
		model.addAttribute("listClose", listClose);// 近景
		model.addAttribute("listMiddle", listMiddle);// 中景
		model.addAttribute("listAll", listAll);// 远景
		model.addAttribute("echoMap", echoMap);// 个性化参数
		model.addAttribute("cooperaTypes", cooperaTypes);// 显示其中的场地及资金
		model.addAttribute("storageId", storageId);// 存当前个性化参数对应名称Id

		return "/wap/xkh_version_2/seller_enterInfo_alter";
	}

	/**
	 * 当前校园商家用户修改信息
	 * 
	 * @param response
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "updateSchoolBusi")
	public String updateSchoolBusi(HttpServletResponse response,
			HttpServletRequest request, Model model) {

		// 根据用户的OpenId来获取当前的Id值
		/* String openId = getCurrentOpenId(request); */
		String openId = String.valueOf(request.getSession().getAttribute(
				"dkOpenId"));
		UserBasicinfo userInfo = homeService.getUserBaseInfoByOpenId(openId);
		String cooperaTypeJson = ""; // 合作形式和价格
		// 获取传过来的用户Id 以及页面中修改的参数
		Map<String, Object> map = createParameterMap(request);
		map.put("viewNum", "0");// 无此参数默认设置为空
		map.put("openId", openId);
		map.put("id", userInfo.getId().toString());
		// 设置当前场地的属性
		if (request.getParameterValues("cooperaType") != null
				&& request.getParameterValues("cooperaPrice") != null) {// 设置当前场地的属性
			String[] cooperaTyArray = request.getParameterValues("cooperaType");// 合作形式
			String[] cooperaPrArray = request
					.getParameterValues("cooperaPrice");// 报价
			cooperaTypeJson = StringUtil.dealArray(cooperaTyArray,
					cooperaPrArray);
		}
		map.put("cooperaType", cooperaTypeJson);
		String userName = String.valueOf(request.getSession().getAttribute(
				"userName"));
		if (!"".equals(userName)) {
			paramsMap.put("createUser", userName);
		} else {
			paramsMap.put("createUser", "admin");
		}
		boolean flage = userBasicinfoService.upSettleVersion3(map);

		if (flage) {
			return "redirect:/home/getmyInform.html"; // 修改成功 成功则跳转个人页面
		} else {
			return "redirect:/home/index.html"; // 修改失败
		}

	}

	public Model toShowbasic(Model model) {

		// 获取所有的城市，从xiaoka中进行获取
		List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();
		// 获取省列表
		List<Map<String, Object>> provnceList = skillUserService
				.queryProviceListVersion2();
		// 加载当前的商家类型（字典表）fieldtype
		List<Map<String, Object>> businTypeList = userBasicinfoService
				.getFieldtype(BUSINTYPE);// 校园商家类型
		// 加载当前的合作类型（字典表） filedsize
		List<Map<String, Object>> cooperaTypeList = userBasicinfoService
				.getFieldtype(COMPARE_SIZE);// 合作类型

		List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表

		for (int i = 0; i < provnceList.size(); i++) {
			String provnce = String.valueOf(provnceList.get(i).get("province"));
			Map<String, Object> map = new HashMap<String, Object>();
			// 获取市信息
			List<Map<String, Object>> CityByProvnce = skillUserService
					.queryCityListVersion2(provnce);
			if (CityByProvnce != null && CityByProvnce.size() > 0) {
				map.put("value", provnce);// 市
				map.put("text", provnce);
				map.put("children", CityByProvnce);
			}
			proList.add(map);
		}

		String jsonProList = JSONObject.toJSONString(proList);
		List<Map<String, Object>> schoolLabels = skillUserService
				.getSchoolGrade();
		Map<String, Object> mapLabel = new HashMap<String, Object>();
		mapLabel.put("text", "全部");
		mapLabel.put("value", null);
		schoolLabels.add(0, mapLabel);
		String jsonLabelList = JSONObject.toJSONString(schoolLabels);

		model.addAttribute("labelList", jsonLabelList);// 当前的学校以json的形式进行展示
		model.addAttribute("proList", jsonProList);// 当前的省市以json的形式进行前端展示
		model.addAttribute("businTypeList", StringUtil.dealList(businTypeList));// 商家类型
		model.addAttribute("businTypeLists", businTypeList);// 商家类型 上传回显的时候显示
		model.addAttribute("cooperaTypeList", cooperaTypeList);// 合作形式

		return model;
	}

	/**
	 * 将以，号分割的STring 串以list的形式进行返回
	 * 
	 * @param str
	 * @return
	 */
	public List<String> dealString(String str) {

		List<String> list = new ArrayList<String>();
		String[] arre = str.split(",");
		for (String string : arre) {
			list.add(string);
		}
		return list;
	}

	/**
	 * 关于前台页面修改的问题 person_place.jsp 场地入驻个人空间一级页面
	 */
	@RequestMapping(value = "toShowPerSpaceSchool")
	public String toShowPersonSpace(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		createParameterMap(request);
		if (StringUtils.isBlank(paramsMap.get("id").toString())) {

			log.error("当前传入Id值为空");
			return "redirect:/home/index.html";
		}
		double end = 0;
		boolean flage = true;// 区分点击
		double begin = 2147483646; // 初始值int 的最大整形
		String openId = getCurrentOpenId(request); // get openId

		// 获取入住用户的基本信息
		UserBasicinfo userInfo = null;
		userInfo = homeService.getUserInfo(paramsMap);
		model = toShowbasic(model);// 加载修改需要的公共的基础信息
		Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);// 详细信息
		// 判断入驻类型
		String enterId = String.valueOf(objMap.get("enterId"));
		paramsMap.put("enterId", enterId);
		// 获取该类型下的优选服务
		paramsMap.put("enterClass", enterId);
		List<Map<String, Object>> po = preferredServiceService
				.getService(objMap.get("enterId").toString());

		List<UserSkillinfoData> list = userenterInfoService
				.querySettleListVersion3(paramsMap);
		Map<String, String> echoMap = new HashMap<String, String>();
		for (UserSkillinfoData userSkillinfoData : list) {
			echoMap.put(userSkillinfoData.getTypeName(),
					userSkillinfoData.getTypeValue());
		}
		List<String> listClose = dealString(echoMap.get("closePhoto"));
		List<String> listMiddle = dealString(echoMap.get("middlePho"));
		List<String> listAll = dealString(echoMap.get("allPhoto"));

		JSONObject jasonObject = JSONObject.parseObject(echoMap
				.get("cooperaType"));
		Map<String, Object> cooperaTypes = (Map) jasonObject;
		if (cooperaTypes.size() != 0) {
			for (Entry<String, Object> entry : cooperaTypes.entrySet()) {
				double curren = Double.parseDouble(entry.getValue().toString());
				if (curren < begin) {
					begin = curren;
				}
				if (curren > end) {
					end = curren;
				}
			}
		} else {
			begin = 0;
		}

		/** 区分通过对比openId关联的ID CFZ begin */
		if (userInfo != null) {
			if (openId.equals(userInfo.getOpenId())) {
				flage = false;
			}
		}
		paramsMap.put("enterId", enterId);
		Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
		String typeValue = String.valueOf(setypeMap.get("typeValue"));
		
		model.addAttribute("flage", flage);
		model.addAttribute("begin", begin);
		model.addAttribute("end", end);
		model.addAttribute("userInfo", userInfo);
		model.addAttribute("listClose", listClose);// 近景
		model.addAttribute("objMap", objMap);// 个人基础信息
		model.addAttribute("listMiddle", listMiddle);// 中景
		model.addAttribute("listAll", listAll);// 远景
		model.addAttribute("echoMap", echoMap);// 个性化参数
		model.addAttribute("po", po);// 优选服务
		model.addAttribute("cooperaTypes", cooperaTypes);// 显示其中的场地及资金
		model.addAttribute("typeValue", typeValue);// 显示其中的场地及资金
		

		return "/wap/xkh_version_2/person_school";
	}

	/**
	 * 当前用户展示个人信息页
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "toShowPerSchBusi")
	public String toShowPerSchBusi(HttpServletResponse response,
			HttpServletRequest request, Model model) {

		List<BannerInfo> gxImg = homeService.getBannerInfoListVersion3(8);// 轮播图
																			// 校内商家的信息显示
		if (gxImg.size() > 0) {
			model.addAttribute("gxImg", gxImg.get(0));
		} else {
			model.addAttribute("gxImg", null);
		}
		Map<String, Object> map = createParameterMap(request);
		String openId = getCurrentOpenId(request);
		// System.out.println("***************openId**************" + openId);
		UserBasicinfo userInfo = homeService.getUserBaseInfoByOpenId(openId);
		paramsMap.put("id", userInfo.getId().toString());
		map.put("id", userInfo.getId().toString());

		Map<String, String> echoMap = new HashMap<String, String>();
		model = toShowbasic(model);
		Map<String, Object> objMap = userBasicinfoService.getById(map);// 获取当前的个人信息
		List<UserSkillinfoData> list = userenterInfoService
				.querySettleListVersion3(paramsMap);
		Map<String, Object> storageId = new HashMap<String, Object>();
		for (UserSkillinfoData userSkillinfoData : list) {
			echoMap.put(userSkillinfoData.getTypeName(),
					userSkillinfoData.getTypeValue());
			storageId.put(userSkillinfoData.getTypeName(), userSkillinfoData
					.getId().toString());
		}
		List<String> listClose = dealString(echoMap.get("closePhoto"));
		List<String> listMiddle = dealString(echoMap.get("middlePho"));
		List<String> listAll = dealString(echoMap.get("allPhoto"));
		JSONObject jasonObject = JSONObject.parseObject(echoMap
				.get("cooperaType"));
		Map<String, Object> cooperaTypes = (Map) jasonObject;

		model.addAttribute("objMap", objMap);// 个人信息加载
		model.addAttribute("listClose", listClose);// 近景
		model.addAttribute("listMiddle", listMiddle);// 中景
		model.addAttribute("listAll", listAll);// 远景
		model.addAttribute("echoMap", echoMap);// 个性化参数
		model.addAttribute("cooperaTypes", cooperaTypes);// 显示其中的场地及资金
		model.addAttribute("storageId", storageId);// 存当前个性化参数对应名称Id

		return "/wap/xkh_version_2/seller_enterInfo_deail";
	}

}
