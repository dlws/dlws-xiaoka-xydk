/**
 * HighSchoolSkillService.java
 * com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.dao.HighSchoolSkillOrderDao;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   wxl
 * @Date	 2017年4月13日 	 
 */
@Service("highSchoolSkillOrderService")
public class HighSchoolSkillOrderService {

	@Autowired
	private HighSchoolSkillOrderDao highSchoolSkillOrderDao;

	/*
	 * 获取若干比例
	 */
	public Map<String, Object> getSkillSonMap(String skillSonId) {

		return highSchoolSkillOrderDao.getSkillSonMap(skillSonId);

	}

	/*
	 * 添加订单
	 */
	public String insertHighSchoolSkillOrder(Map<String, Object> paramsMap) {

		return highSchoolSkillOrderDao.insertHighSchoolSkillOrder(paramsMap);

	}

	/*
	 * 更新服务销售量
	 */
	public boolean updateSkillBuyNo(Map<String, Object> paramsMap) {

		return highSchoolSkillOrderDao.updateSkillBuyNo(paramsMap);

	}

	/*
	 * 查询订单
	 */
	public Map<String, Object> getOrderDetail(Map<String, Object> paramsMap) {

		return highSchoolSkillOrderDao.getOrderDetail(paramsMap);

	}

	public List<Map<String, Object>> getOrderDetailList(Map<String, Object> paramsMap) {

		// TODO Auto-generated method stub
		return highSchoolSkillOrderDao.getOrderDetailList(paramsMap);

	}

}
