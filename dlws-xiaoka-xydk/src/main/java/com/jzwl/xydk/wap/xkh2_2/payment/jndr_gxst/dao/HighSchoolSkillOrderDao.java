/**
 * HighSchoolSkillDao.java
 * HighSchoolSkillOrderController
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.id.Sequence;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * <p>
 *
 * @author   wxl
 * @Date	 2017年4月13日 	 
 */
@Repository("highSchoolSkillOrderDao")
public class HighSchoolSkillOrderDao {

	@Autowired
	private BaseDAO baseDAO;

	public String insertHighSchoolSkillOrder(Map<String, Object> paramsMap) {

		String orderid = Sequence.nextId();
		paramsMap.put("orderId", orderid);//订单id/号
		paramsMap.put("createDate", new Date());//创建时间
		paramsMap.put("payway", 0);//支付方式--微信支付
		paramsMap.put("orderStatus", 1);//订单状态--下单成功
		paramsMap.put("isDelete", 0);//是否删除 
		Date date = new Date();
		date = DateUtil.addHour(date, GlobalConstant.HOUR);
		paramsMap.put("invalidTime", date);
		//处理比例计算
		double d = Double.parseDouble(paramsMap.get("payMoney").toString());
		double dserviceRatio = Double.parseDouble(paramsMap.get("serviceRatio").toString());
		double ddiplomatRatio = Double.parseDouble(paramsMap.get("diplomatRatio").toString());
		double dplatformRatio = Double.parseDouble(paramsMap.get("platformRatio").toString());
		paramsMap.put("serviceMoney", dserviceRatio * d);
		paramsMap.put("diplomatMoney", ddiplomatRatio * d);
		paramsMap.put("platformMoney", dplatformRatio * d);
		//处理销售量计算
		/*if (null == paramsMap.get("sellNo").toString() || "" == paramsMap.get("sellNo").toString()) {
			paramsMap.put("sellNo", 0);
		}*/
		//int sellNo = Integer.parseInt(paramsMap.get("sellNo").toString());
		//paramsMap.put("sellNo", sellNo + tranNumber);
		
	
				
		int tranNumber = Integer.parseInt(paramsMap.get("tranNumber").toString());
		Map<String, Object> daMap = new HashMap<String, Object>();
		daMap = paramsMap;
		String insertOrderSql = "insert into `xiaoka-xydk`.order "
				+ " (orderId,openId,sellerOpenId,createDate,payMoney,serviceMoney,diplomatMoney,platformMoney,serviceRatio,diplomatRatio,platformRatio,realityMoney,"
				+ "orderStatus,payway,linkman,phone,isDelete,message,enterType,schoolId,cityId,startDate,endDate,invalidTime) "
				+ " values "
				+ " (:orderId,:openId,:sellerOpenId,:createDate,:payMoney,:serviceMoney,:diplomatMoney,:platformMoney,:serviceRatio,:diplomatRatio,:platformRatio,:payMoney,"
				+ ":orderStatus,:payway,:linkman,:phone,:isDelete,:message,:enterType,:schoolId,:cityId,:startDate,:endDate,:invalidTime)";
		boolean flag = baseDAO.executeNamedCommand(insertOrderSql, paramsMap);
		
		//删除已选
		String delSql = "DELETE FROM  `xiaoka-xydk`.selectskill WHERE purOpenId='"+paramsMap.get("openId")+"' AND skillId='"+paramsMap.get("skillId")+"'"
				+ " and openId='"+paramsMap.get("sellerOpenId")+"'";
				baseDAO.executeNamedCommand(delSql, paramsMap);
		daMap.remove("openId");
		daMap.remove("sellerOpenId");
		daMap.remove("createDate");
		daMap.remove("payMoney");
		daMap.remove("serviceMoney");
		daMap.remove("diplomatMoney");
		daMap.remove("platformMoney");
		daMap.remove("serviceRatio");
		daMap.remove("diplomatRatio");
		daMap.remove("platformRatio");
		daMap.remove("realityMoney");
		daMap.remove("schoolId");
		daMap.remove("cityId");
		daMap.remove("orderStatus");
		daMap.remove("payway");
		daMap.remove("linkman");
		daMap.remove("phone");
		daMap.remove("isDelete");
		daMap.remove("enterType");
		daMap.remove("message");
		daMap.remove("orderId");
		daMap.remove("startDate");
		daMap.remove("endDate");
		daMap.remove("invalidTime");

		String DataSql = "insert into `xiaoka-xydk`.order_detail(id,orderId,typeName,typeValue,isDelete,createDate)" + "values"
				+ "(:id,:orderId,:typeName,:typeValue,:isDelete,:createDate)";
		
		
		boolean temp = false;

		if (flag) {
			for (Entry<String, Object> entry : daMap.entrySet()) {
				Map<String, Object> inserMap = new HashMap<String, Object>();
				inserMap.put("id", Sequence.nextId());
				inserMap.put("orderId", orderid);
				inserMap.put("typeName", entry.getKey());
				inserMap.put("typeValue", entry.getValue());
				inserMap.put("isDelete", 0);
				inserMap.put("createDate", new Date());
				temp = baseDAO.executeNamedCommand(DataSql, inserMap);
			}
		}
		if (temp) {
			return orderid;
		} else {
			return "0";
		}

	}

	public boolean updateSkillBuyNo(Map<String, Object> paramsMap) {

		String querysql = "SELECT * FROM `xiaoka-xydk`.user_skillinfo WHERE id='" + paramsMap.get("skillId") + "' AND isDelete=0";
		Map<String, Object> map = baseDAO.queryForMap(querysql);
		if (null == map.get("sellNo")) {
			map.put("sellNo", 0);
		} else {
			map.put("sellNo", map.get("sellNo").toString());
		}
		int sn = Integer.parseInt(map.get("sellNo").toString());
		int tranNumber = Integer.parseInt(paramsMap.get("tranNumber").toString());
		String sql = "Update `xiaoka-xydk`.user_skillinfo SET sellNo='" + sn + "'+'" + tranNumber + "'  WHERE id='" + paramsMap.get("skillId") + "'";
		return baseDAO.executeNamedCommand(sql, paramsMap);
	}

	public Map<String, Object> getSkillSonMap(String skillSonId) {

		String sql = "SELECT * FROM `xiaoka-xydk`.user_skillclass_son WHERE id='" + skillSonId + "' and isDelete = 0";

		return baseDAO.queryForMap(sql);

	}

	public Map<String, Object> getOrderDetail(Map<String, Object> paramsMap) {

		String sql = " SELECT * FROM `xiaoka-xydk`.order WHERE orderId='" + paramsMap.get("orderId") + "' and isDelete=0";
		return baseDAO.queryForMap(sql);

	}

	public List<Map<String, Object>> getOrderDetailList(Map<String, Object> paramsMap) {

		String sql = " SELECT * FROM `xiaoka-xydk`.order_detail WHERE orderId='" + paramsMap.get("orderId") + "' and isDelete=0";
		return baseDAO.queryForList(sql);

	}

}
