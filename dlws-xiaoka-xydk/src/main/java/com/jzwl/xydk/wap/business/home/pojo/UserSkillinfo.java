/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.pojo;

import java.util.List;
import java.util.Map;

import com.jzwl.system.base.pojo.BasePojo;

public class UserSkillinfo extends BasePojo implements java.io.Serializable {

	private static final long serialVersionUID = 5454155825314635342L;

	//alias
	public static final String TABLE_ALIAS = "UserSkillinfo";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_BASIC_ID = "用户表的id";
	public static final String ALIAS_SKILL_FAT_ID = "技能分类父级的Id";
	public static final String ALIAS_SKILL_SON_ID = "技能分类子级的Id";
	public static final String ALIAS_SKILL_NAME = "技能名称";
	public static final String ALIAS_TEXT_URL = "图文的url";
	public static final String ALIAS_VIDEO_URL = "视屏展示url";
	public static final String ALIAS_SKILL_PRICE = "技能价格";
	public static final String ALIAS_SERVICE_TYPE = "服务类型（0：线上服务，1：线下服务）";
	public static final String ALIAS_DESCRIBE = "技能描述";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	public static final String ALIAS_IS_DELETE = "是否删除（0：否，1：是）";
	public static final String ALIAS_HEAD_PORTRAIT = "2.0头像";
	public static final String ALIAS_COMPANY = "价格单位";
	public static final String ALIAS_ENTERID = "入驻id";
	public static final String ALIAS_ENTERTYPE = "价入驻类型";
	
	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;

	//columns START
	/**
	 * id       db_column: id 
	 */
	private java.lang.Long id;
	/**
	 * 用户表的id       db_column: basicId 
	 */
	private java.lang.Long basicId;
	/**
	 * 技能分类父级的Id       db_column: skillFatId 
	 */
	private java.lang.Long skillFatId;
	/**
	 * 技能分类子级的Id       db_column: skillSonId 
	 */
	private java.lang.Long skillSonId;
	/**
	 * 技能名称       db_column: skillName 
	 */
	private java.lang.String skillName;
	/**
	 * 图文的url       db_column: textURL 
	 */
	private java.lang.String textUrl;
	/**
	 * 视屏展示url       db_column: videoURL 
	 */
	private java.lang.String videoUrl;

	/**
	 * 技能价格       db_column: skillPrice 
	 */
	private java.lang.Double skillPrice;
	/**
	 * 技能价格   字符窜需求
	 */
	private java.lang.String priceStr;
	/**
	 * 价格单位       db_column: company 
	 */
	private java.lang.Long company;
	/**
	 * 单位名称
	 */
	private java.lang.String companyStr;
	/**
	 * 单位名称  priceName；
	 */
	private java.lang.String priceName;

	private java.lang.String nickname;

	private java.lang.Long enterId;
	
	private int sellNo;
	/**
	 * 父级分类的缩写
	 */
	private java.lang.String classValueF;
	/**
	 * 入住类型   
	 */
	private java.lang.String typeValue;
	/**
	 * 服务类型（0：线上服务，1：线下服务）       db_column: serviceType 
	 */
	private java.lang.Integer serviceType;
	/**
	 * 技能描述       db_column: describe 
	 */
	private java.lang.String skillDepict;
	/**
	 * 创建时间       db_column: createDate 
	 */
	private java.util.Date createDate;
	/**
	 * 是否删除（0：否，1：是）       db_column: isDelete 
	 */
	private java.lang.Integer isDelete;

	private List<UserSkillImage> images;
	
	private String image;//技能的图片

	private String cityName;//城市名称

	private String schoolName;//学校名称

	private String userName;//用户名称

	private String otherOpus;//其他作品的url

	private int flag;

	private String className;

	private int viewNum;

	private String headImgUrl;

	private String openId;

	private List<VideoImg> videoImg;
	
	private List<VideoImg> opus;
	
	private String headPortrait;
	
	private String otherPrice;
	
	//校内商家，场地资源使用
	private List<String> closeList;//近景
	
	private List<String> middelList;//中景
	
	private List<String> allList;//全景
	
	/**
	 * 订单详情
	 */
	private Map<String,Object> mapDetail;
	
	/**
	 * 技能类型
	 */
	private String classValue;
	//columns END

	public String getClassValue() {
		return classValue;
	}

	public void setClassValue(String classValue) {
		this.classValue = classValue;
	}

	public Map<String, Object> getMapDetail() {
		return mapDetail;
	}

	public void setMapDetail(Map<String, Object> mapDetail) {
		this.mapDetail = mapDetail;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public List<VideoImg> getOpus() {
		return opus;
	}

	public void setOpus(List<VideoImg> opus) {
		this.opus = opus;
	}

	public List<VideoImg> getVideoImg() {
		return videoImg;
	}

	public void setVideoImg(List<VideoImg> videoImg) {
		this.videoImg = videoImg;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public java.lang.Long getBasicId() {
		return this.basicId;
	}

	public void setBasicId(java.lang.Long value) {
		this.basicId = value;
	}

	public java.lang.Long getSkillFatId() {
		return this.skillFatId;
	}

	public void setSkillFatId(java.lang.Long value) {
		this.skillFatId = value;
	}

	public java.lang.Long getSkillSonId() {
		return this.skillSonId;
	}

	public void setSkillSonId(java.lang.Long value) {
		this.skillSonId = value;
	}

	public java.lang.String getSkillName() {
		return skillName;
	}

	public void setSkillName(java.lang.String skillName) {
		this.skillName = skillName;
	}

	public java.lang.String getTextUrl() {
		return this.textUrl;
	}

	public void setTextUrl(java.lang.String value) {
		this.textUrl = value;
	}

	public java.lang.String getVideoUrl() {
		return this.videoUrl;
	}

	public void setVideoUrl(java.lang.String value) {
		this.videoUrl = value;
	}

	public java.lang.Double getSkillPrice() {
		return this.skillPrice;
	}

	public void setSkillPrice(java.lang.Double value) {
		this.skillPrice = value;
	}

	public java.lang.Integer getServiceType() {
		return this.serviceType;
	}

	public void setServiceType(java.lang.Integer value) {
		this.serviceType = value;
	}

	public java.lang.String getSkillDepict() {
		return skillDepict;
	}

	public void setSkillDepict(java.lang.String skillDepict) {
		this.skillDepict = skillDepict;
	}

	/*	public String getCreateDateString() {
			return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
		}

		public void setCreateDateString(String value) {
			setCreateDate(strToDate(value, java.util.Date.class,FORMAT_CREATE_DATE));
		}*/

	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}

	public List<UserSkillImage> getImages() {
		return images;
	}

	public void setImages(List<UserSkillImage> images) {
		this.images = images;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOtherOpus() {
		return otherOpus;
	}

	public void setOtherOpus(String otherOpus) {
		this.otherOpus = otherOpus;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getViewNum() {
		return viewNum;
	}

	public void setViewNum(int viewNum) {
		this.viewNum = viewNum;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public java.lang.String getPriceName() {
		return priceName;
	}

	public void setPriceName(java.lang.String priceName) {
		this.priceName = priceName;
	}

	public java.lang.String getNickname() {
		return nickname;
	}

	public void setNickname(java.lang.String nickname) {
		this.nickname = nickname;
	}

	public java.lang.Long getEnterId() {
		return enterId;
	}

	public void setEnterId(java.lang.Long enterId) {
		this.enterId = enterId;
	}

	public int getSellNo() {
		return sellNo;
	}

	public void setSellNo(int sellNo) {
		this.sellNo = sellNo;
	}
	public java.lang.String getTypeValue() {
		return typeValue;
	}

	public void setTypeValue(java.lang.String typeValue) {
		this.typeValue = typeValue;
	}

	public java.lang.Long getCompany() {
		return company;
	}

	public void setCompany(java.lang.Long company) {
		this.company = company;
	}
	
	public String getHeadPortrait() {
		return headPortrait;
	}

	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}
	
	public String getOtherPrice() {
		return otherPrice;
	}
	public void setOtherPrice(String otherPrice) {
		this.otherPrice = otherPrice;
	}

	public java.lang.String getClassValueF() {
		return classValueF;
	}

	public void setClassValueF(java.lang.String classValueF) {
		this.classValueF = classValueF;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public List<String> getCloseList() {
		return closeList;
	}

	public void setCloseList(List<String> closeList) {
		this.closeList = closeList;
	}

	public List<String> getMiddelList() {
		return middelList;
	}

	public void setMiddelList(List<String> middelList) {
		this.middelList = middelList;
	}

	public List<String> getAllList() {
		return allList;
	}

	public void setAllList(List<String> allList) {
		this.allList = allList;
	}
	public java.lang.String getPriceStr() {
		return priceStr;
	}
	public void setPriceStr(java.lang.String priceStr) {
		this.priceStr = priceStr;
	}

	public java.lang.String getCompanyStr() {
		return companyStr;
	}

	public void setCompanyStr(java.lang.String companyStr) {
		this.companyStr = companyStr;
	}
}
