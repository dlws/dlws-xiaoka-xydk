package com.jzwl.xydk.wap.xkh2_2.ordseach.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.xkh2_2.ordseach.dao.OrderCountDao;

@Service
public class OrderCountService {
	
	@Autowired
	private OrderCountDao orderCountDao;//dao基类，操作数据库 
	
	Log log = LogFactory.getLog(OrderCountService.class);
	/**
	 * 第一次创建的时候默认为0表示已读
	 * @return
	 */
	public boolean creatOrderCountByOpenId(String openId){
		
		if(getOrderCountBySellerOpenId(openId)==null){
			
			return orderCountDao.creatOrderCountByOpenId(openId);	
		}
		
		return false;
	}
	
	public boolean updateOrderCountByOpenId(Map<String,Object> map){
		
		
		return orderCountDao.updateOrderCountByOpenId(map);
	}
	
	/**
	 * 获取当前中用户下的order的信息
	 * @param sellerOpenId
	 * @return
	 */
	public Map<String,Object> getOrderCountBySellerOpenId(String openId){
		
		return orderCountDao.getOrderCountBySellerOpenId(openId);
		
	}
	
	
	/**
	 * 获取不同状态下的订单总数
	 * 获取当前用户的OpenId 返回
	 * @param openId
	 * @return
	 */
	public int getOrderCountByStatus(String openId,int orderStatus){
		
		return orderCountDao.getOrderCountByStatus(openId,orderStatus);
	}
	
	/**
	 * 根据sellerOpenId获取当前用户的总数
	 * @param sellerOpenId
	 * @return
	 */
	public int getOrderBySellerOpenId(String sellerOpenId){
		
		return orderCountDao.getOrderBySellerOpenId(sellerOpenId);
	}
	
	/**
	 * 查询当前
	 * @return
	 */
	public List<String> getSellerCountStaues(String openId){
		
		List<String> list = new ArrayList<String>();
		
		//1.获取当前openId下的count表中的状态
		
		Map<String,Object> map = getOrderCountBySellerOpenId(openId);
		
		//说明此条信息不存在
		if(map==null){
			//创建词条数据
			if(creatOrderCountByOpenId(openId)){
				map = getOrderCountBySellerOpenId(openId);
			}else{
				return list;//返回空的list
			}
		}
		
		//待付款
		int obligation = getOrderCountByStatus(openId,1);
		//待服务
		int pendingService = getOrderCountByStatus(openId,2);
		//待确认
		int noConfirmed = getOrderCountByStatus(openId,3);
		//待退款
		int refundable = getOrderCountByStatus(openId,7);
		//当前卖出技能的总数	
		int sellerCount = getOrderBySellerOpenId(openId);
		
		//对当前的订单状态进行比对
		if(Integer.parseInt(map.get("obligation").toString())<obligation){
			list.add("obligation");
		}
		if(Integer.parseInt(map.get("pendingService").toString())<pendingService){
			list.add("pendingService");
		}
		if(Integer.parseInt(map.get("noConfirmed").toString())<noConfirmed){
			list.add("noConfirmed");
		}
		if(Integer.parseInt(map.get("refundable").toString())<refundable){
			list.add("refundable");
		}
		if(Integer.parseInt(map.get("sellerCount").toString())<sellerCount){
			list.add("sellerCount");
		}
		
		return list;
	}
	
	/**
	 * 根据订单状态更新当前的orderCount表
	 */
	public boolean updateOrderCountByOpenId(String currentOpenId,int orderStatus){
		
		int count = 0;
		String mapKey = "";
		//当条件变多时考虑使用数组
		if(orderStatus==1||orderStatus==2||orderStatus==3||orderStatus==7){
			count = getOrderCountByStatus(currentOpenId,orderStatus);
		}else{
			count = getOrderBySellerOpenId(currentOpenId);
		}
		//当前的总条数
		mapKey = detailTheKey(orderStatus);
		
		Map<String,Object> map = new HashMap<String, Object>();
		
		map.put(mapKey, count);
		map.put("openId", currentOpenId);
		//更新当前count表中的状态
		return updateOrderCountByOpenId(map);
		
	}
	
	/**
	 * 处理当前用户的KEY值
	 * @return
	 */
	private String detailTheKey(int status){
		
		String orderStatus="";
		
		switch (status) {
		case 1:
			orderStatus ="obligation";
			break;
		case 2:
			orderStatus ="pendingService";
			break;
		case 3:
			orderStatus ="noConfirmed";
			break;
		case 7:
			orderStatus ="refundable";
			break;
		default:
			orderStatus ="sellerCount";
			break;
		}
		return orderStatus;
	}
	
	
public void upperCount(String OpenId,String status){
		
		int orderStatus = 0;
		//获取当前状态，转换为对用订单状态
		if(status.equals("1")){
			orderStatus=1;
		}
		else if(status.equals("2")){
			orderStatus=2;
		}
		else if(status.equals("3")){
			orderStatus=3;
		}
		else if(status.equals("4")){
			orderStatus=7;
		}else if(status.equals("seller")){
			orderStatus=21;
		}
		else{
			log.info("----------当前类型无需要同步字段-------------");
			return;
		}
		//执行更新当前orderCount表的状态
		if(updateOrderCountByOpenId(OpenId,orderStatus)){
			log.info("---------执行更新成功-----------");
		}else{
			log.info("---------执行更新失败-----------");
		}
	}

}
