package com.jzwl.xydk.wap.xkhV2.cdrz.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.pojo.UserSkillinfoData;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserEnterInfoService;
import com.jzwl.xydk.wap.business.home.controller.HomeController;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;

/**
 * 
 * @author Administrator
 * wap版  页面场地入驻
 *
 */
@Controller("fieldEnter")
@RequestMapping("/fieldEnter")
public class FieldEnterController extends BaseWeixinController {

	@Autowired
	private PreferredServiceService preferredServiceService;
	@Autowired
	private HomeService homeService;
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	@Autowired
	private UserEnterInfoService userenterInfoService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private Constants constants;

	private final String FIELD_TYPE = "fieldtype";//场地类型
	private final String FILED_SIZE = "filedsize";//场地规模
	private final String BASIC_MATERIAL = "basicmaterial";//基本物资配备
	private final String PARTIME_JOB = "partimejob";//基本物资配备
	

	Logger log = Logger.getLogger(HomeController.class);

	/**
	 * 场地入驻展示
	 * @return
	 */
	@RequestMapping(value = "toAddEnter")
	public String toAddEnter(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("******************************toAddEnter***************************");
		createParameterMap(request);//接取当前的
		model.addAttribute("paramsMap", paramsMap);//增加enterId
		try {
			model = toShowbasic(model);
			return "/wap/xkh_version_2/place_enterInfo";
		} catch (Exception e) {
			log.error("场地入驻加载基本tiaoji" + e.getMessage());
			return "redirect:/home/index.html";//跳转到上一个页面
		}

	}

	/**
	 * 添加当前的用户
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "addEnter")
	public String addEnter(HttpServletRequest request, HttpServletResponse response, Model model) {
		//增加校验规则   在后台增加校验规则
		//获取页面中的所有参数
		try {
			Map<String, Object> map = createParameterMap(request);
			String openId = getCurrentOpenId(request); //get  openId
			map.put("viewNum", "0");//开始时默认为0
			map.put("enterId", map.get("enterId"));//无此参数默认设置为空
			map.put("openId", openId);

			if (request.getParameterValues("material") != null) {//设置当前场地的属性
				map.put("material", dealArray(request.getParameterValues("material")));
			}

			String userName = String.valueOf(request.getSession().getAttribute("userName"));
			if (!"".equals(userName)) {
				paramsMap.put("createUser", userName);
			} else {
				paramsMap.put("createUser", "admin");
			}
			boolean flag = userBasicinfoService.addSettleUserVersion3(map);

		} catch (Exception e) {

			e.printStackTrace();
		}

		return "redirect:/home/getmyInform.html";

	}

	/**
	 * 修改回显当前用户的个人信息
	 * 
	 */
	@RequestMapping(value = "toUpdateEnter")
	public String toUpdateEnter(HttpServletRequest request, HttpServletResponse response, Model model) {

		createParameterMap(request);//获取当前的Id值
		String openId = getCurrentOpenId(request);
		UserBasicinfo userInfo = homeService.getUserBaseInfoByOpenId(openId);
		paramsMap.put("id", userInfo.getId().toString());//覆盖当前的Id值

		model = toShowbasic(model);//加载修改需要的公共的基础信息
		//加载个人的相关的信息

		if (StringUtils.isNotBlank(paramsMap.get("id").toString())) {
			//查询个性化参数
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息

			List<UserSkillinfoData> list = userenterInfoService.querySettleListVersion3(paramsMap);
			Map<String, String> echoMap = new HashMap<String, String>();
			Map<String, Object> storageId = new HashMap<String, Object>();
			for (UserSkillinfoData userSkillinfoData : list) {
				echoMap.put(userSkillinfoData.getTypeName(), userSkillinfoData.getTypeValue());
				storageId.put(userSkillinfoData.getTypeName(), userSkillinfoData.getId().toString());
			}
			List<String> listClose = dealString(echoMap.get("closePhoto"));
			List<String> listMiddle = dealString(echoMap.get("middlePho"));
			List<String> listAll = dealString(echoMap.get("allPhoto"));
			model.addAttribute("paramsMap", paramsMap);//公共参数
			model.addAttribute("storageId", storageId);//存当前个性化参数对应名称Id 
			model.addAttribute("listClose", listClose);//近景
			model.addAttribute("objMap", objMap);//个人基础信息
			model.addAttribute("listMiddle", listMiddle);//中景
			model.addAttribute("listAll", listAll);//远景
			model.addAttribute("echoMap", echoMap);//个性化参数
			return "/wap/xkh_version_2/place_enterInfo_alter";
		} else {
			//传递错误信息  当前Id值不能为空
			log.error("当前用户Id值为空");
			return "redirect:/home/index.html";
		}
	}

	/**
	 * 修改场地入驻用户当前的信息
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "UpdateEnter")
	public String UpdateEnter(HttpServletRequest request, HttpServletResponse response, Model model) {

		//根据用户的OpenId来获取当前的Id值
		String openId = getCurrentOpenId(request);
		UserBasicinfo userInfo = homeService.getUserBaseInfoByOpenId(openId);
		//获取传过来的用户Id 以及页面中修改的参数
		Map<String, Object> map = createParameterMap(request);
		map.put("id", userInfo.getId().toString()); //暂时屏蔽，为测试图片上传
		map.put("viewNum", "0");//默认初始为0
		map.put("openId", openId);//默认初始为0

		if (request.getParameterValues("material") != null) {//设置当前场地的属性
			map.put("material", dealArray(request.getParameterValues("material")));
		}
		String userName = String.valueOf(request.getSession().getAttribute("userName"));
		if (!"".equals(userName)) {
			paramsMap.put("createUser", userName);
		} else {
			paramsMap.put("createUser", "admin");
		}

		boolean flag = userBasicinfoService.upSettleVersion3(map);

		if (flag) {
			return "redirect:/home/getmyInform.html";//暂时定义跳转首页  正常跳转个人  home/getmyInform.html
		} else {
			log.error("修改场地入驻失败");
			return "redirect:/home/index.html";
		}
		//跳转到个人页面

	}

	/**
	 * 处理参数用，号隔开的方式进行返回String类型
	 */
	public String dealArray(String[] str) {
		StringBuffer all = new StringBuffer();
		for (int i = 0; i < str.length; i++) {

			if (i == str.length - 1) {
				all.append(str[i]);
			} else {
				all.append(str[i] + ",");
			}
		}
		return all.toString();
	}

	/**
	 * 展示基础数据，添加和修改需要的公共数据
	 */
	public Model toShowbasic(Model model) {

		
		//获取所有的城市，从xiaoka中进行获取
		List<Map<String, Object>> cityList = userBasicinfoService.getCityInfo();
		//获取省列表
		List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
		//加载当前的场地类型（字典表）fieldtype
		List<Map<String, Object>> fieldTypeList = userBasicinfoService.getFieldtype(FIELD_TYPE);
		//加载当前的场地规模（字典表） filedsize
		List<Map<String, Object>> filedSizeList = userBasicinfoService.getFieldtype(FILED_SIZE);
		//加载当前的基本物资配备（字典表）basicmaterial 
		List<Map<String, Object>> materialList = userBasicinfoService.getFieldtype(BASIC_MATERIAL);
		//加载是否兼职字典表partime_job
		List<Map<String, Object>> partimejobList = userBasicinfoService.getFieldtype(PARTIME_JOB);

		List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表

		for (int i = 0; i < provnceList.size(); i++) {
			String provnce = String.valueOf(provnceList.get(i).get("province"));
			Map<String, Object> map = new HashMap<String, Object>();
			// 获取市信息
			List<Map<String, Object>> CityByProvnce = skillUserService.queryCityListVersion2(provnce);
			if (CityByProvnce != null && CityByProvnce.size() > 0) {
				map.put("value", provnce);// 市
				map.put("text", provnce);
				map.put("children", CityByProvnce);
			}
			proList.add(map);
		}
		String jsonProList = JSONObject.toJSONString(proList);
		List<Map<String, Object>> schoolLabels = skillUserService.getSchoolGrade();
		Map<String, Object> mapLabel = new HashMap<String, Object>();
		mapLabel.put("text", "全部");
		mapLabel.put("value", null);
		schoolLabels.add(0, mapLabel);
		String jsonLabelList = JSONObject.toJSONString(schoolLabels);

		model.addAttribute("labelList", jsonLabelList);//当前的学校以json的形式进行展示
		model.addAttribute("proList", jsonProList);//当前的省市以json的形式进行前端展示
		model.addAttribute("paramsMap", paramsMap);
		model.addAttribute("fieldTypeList", StringUtil.dealList(fieldTypeList));//场地类型
		model.addAttribute("filedSizeList", StringUtil.dealList(filedSizeList));//场地规模
		model.addAttribute("partimejobList", StringUtil.dealList(partimejobList));//是否提供兼职
		model.addAttribute("fieldTypeLists", fieldTypeList);//场地类型 update 加载基础参数
		model.addAttribute("filedSizeLists", filedSizeList);//场地规模 update 加载基础参数
		model.addAttribute("partimejobLists", partimejobList);//是否提供兼职 update 加载基础参数
		model.addAttribute("materialList", materialList);//基本物资配备

		return model;
	}

	/**
	 * 将以，号分割的STring 串以list的形式进行返回
	 * @param str
	 * @return
	 */
	public List<String> dealString(String str) {

		List<String> list = new ArrayList<String>();
		String[] arre = str.split(",");
		for (String string : arre) {
			list.add(string);
		}
		return list;
	}

	/**
	 * 关于前台页面修改的问题 person_place.jsp
	 * 场地入驻个人空间一级页面
	 */
	@RequestMapping(value = "toShowPersonSpace")
	public String toShowPersonSpace(HttpServletRequest request, HttpServletResponse response, Model model) {

		createParameterMap(request);
		//若Id值为空则返回首页
		if (StringUtils.isBlank(paramsMap.get("id").toString())) {

			log.error("当前传入Id值为空");
			return "redirect:/home/index.html";
		}
		//获取入住用户的基本信息
		UserBasicinfo userInfo = null;

		String openId = getCurrentOpenId(request); //get  openId
		userInfo = homeService.getUserInfo(paramsMap);
		//PageObject po = new PageObject();
		boolean flage = true;
		/**区分通过对比openId关联的ID  CFZ begin*/
		if (userInfo != null) {
			if (openId.equals(userInfo.getOpenId())) {
				flage = false;
			}
		}
		Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息
		paramsMap.put("enterClass", objMap.get("enterId"));
		paramsMap.put("enterId", objMap.get("enterId"));
		//po = preferredServiceService.queryPreferredServiceList(paramsMap);//优选服务
		Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
		String typeValue = String.valueOf(setypeMap.get("typeValue"));
		List<Map<String,Object>>  po = preferredServiceService.getService(objMap.get("enterId").toString());
		model = toShowbasic(model);//加载修改需要的公共的基础信息
		List<UserSkillinfoData> list = userenterInfoService.querySettleListVersion3(paramsMap);
		Map<String, String> echoMap = new HashMap<String, String>();
		for (UserSkillinfoData userSkillinfoData : list) {
			echoMap.put(userSkillinfoData.getTypeName(), userSkillinfoData.getTypeValue());
		}
		List<String> listClose = dealString(echoMap.get("closePhoto"));
		List<String> listMiddle = dealString(echoMap.get("middlePho"));
		List<String> listAll = dealString(echoMap.get("allPhoto"));

		List<Map<String, Object>> materialList = userBasicinfoService.getFieldtype(BASIC_MATERIAL);

		String materialStr = "";
		//循环处理，号问题
		for (Map<String, Object> map : materialList) {
			if (echoMap.get("material").contains(map.get("dic_value").toString())) {
				materialStr += map.get("dic_name") + " ";
			}

		}
		materialStr = materialStr.trim();//去除前后空格
		materialStr = materialStr.replace(" ", "，");

		model.addAttribute("userInfo", userInfo);
		model.addAttribute("listClose", listClose);//近景
		model.addAttribute("objMap", objMap);//个人基础信息
		model.addAttribute("listMiddle", listMiddle);//中景
		model.addAttribute("listAll", listAll);//远景
		model.addAttribute("echoMap", echoMap);//个性化参数
		model.addAttribute("po", po);
		model.addAttribute("flage", flage);
		model.addAttribute("materialStr", materialStr);
		model.addAttribute("typeValue", typeValue);

		return "/wap/xkh_version_2/person_place";
	}

	/**
	 * 修改回显当前用户的个人信息
	 * 
	 */
	@RequestMapping(value = "toShowPerMessageEnter")
	public String toShowPersonMessage(HttpServletRequest request, HttpServletResponse response, Model model) {

		//获取高校社团
		List<BannerInfo> gxImg = homeService.getBannerInfoListVersion3(7);//轮播图
		if (gxImg.size() > 0) {
			model.addAttribute("gxImg", gxImg.get(0));
		} else {
			model.addAttribute("gxImg", null);
		}
		createParameterMap(request);//获取当前的Id值
		String openId = getCurrentOpenId(request);
		UserBasicinfo userInfo = homeService.getUserBaseInfoByOpenId(openId);
		paramsMap.put("id", userInfo.getId().toString());//覆盖当前的Id值

		model = toShowbasic(model);//加载修改需要的公共的基础信息
		//加载个人的相关的信息

		if (StringUtils.isNotBlank(paramsMap.get("id").toString())) {
			//查询个性化参数
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息

			List<UserSkillinfoData> list = userenterInfoService.querySettleListVersion3(paramsMap);
			Map<String, String> echoMap = new HashMap<String, String>();
			Map<String, Object> storageId = new HashMap<String, Object>();
			for (UserSkillinfoData userSkillinfoData : list) {
				echoMap.put(userSkillinfoData.getTypeName(), userSkillinfoData.getTypeValue());
				storageId.put(userSkillinfoData.getTypeName(), userSkillinfoData.getId().toString());
			}
			List<String> listClose = dealString(echoMap.get("closePhoto"));
			List<String> listMiddle = dealString(echoMap.get("middlePho"));
			List<String> listAll = dealString(echoMap.get("allPhoto"));
			model.addAttribute("storageId", storageId);//存当前个性化参数对应名称Id 
			model.addAttribute("listClose", listClose);//近景
			model.addAttribute("objMap", objMap);//个人基础信息
			model.addAttribute("listMiddle", listMiddle);//中景
			model.addAttribute("listAll", listAll);//远景
			model.addAttribute("echoMap", echoMap);//个性化参数
			return "/wap/xkh_version_2/place_enterInfo_deail";
		} else {
			//传递错误信息  当前Id值不能为空
			log.error("当前用户Id值为空");
			return "redirect:/home/index.html";
		}
	}

}
