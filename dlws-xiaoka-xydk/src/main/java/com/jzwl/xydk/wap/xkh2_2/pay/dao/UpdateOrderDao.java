package com.jzwl.xydk.wap.xkh2_2.pay.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * 订单管理使用查询当前用户的订单
 * 
 * @author Administrator
 *
 */
@Repository("updateOrderDao")
public class UpdateOrderDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	private static Logger log = LoggerFactory
			.getLogger(UpdateOrderDao.class);
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：更新订单状态，更新out_trade_no
	 * 创建人： ln
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param orderId
	 * @param outTradeNo
	 * @version
	 */
	public boolean updateOrder(String orderId, String outTradeNo) {
		try {
			String sql = " update `xiaoka-xydk`.order o set o.orderStatus=2,o.invalidTime=:invalidTime,o.payTime=:payTime,o.partnerTradeNo=:outTradeNo where o.orderId=:orderId ";
			log.info("_-----------------------------------------主动更新订单sql"+sql);
			log.info("--------------------------------------主动更新订单参数=orderId="+orderId+"--outTradeNo="+outTradeNo);
			Map map = new HashMap();
			map.put("payTime", DateUtil.formatDate(new Date()));
			map.put("orderId", orderId);
			map.put("outTradeNo", outTradeNo);
			map.put("invalidTime", DateUtil.addDay(new Date(), GlobalConstant.DAY));//付款之后更新超时时间 + 1 天 cfz
			boolean flag = baseDAO.executeNamedCommand(sql, map);
			return flag;
		} catch (Exception e) {
			log.info("---------------------------更新订单异常+orderId"+orderId+"----outTradeNo="+outTradeNo);
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据pid修改订单状态
	 * 创建人： sx
	 * 创建时间： 2017年5月8日
	 * 标记：
	 * @param pid
	 * @param outTradeNo
	 * @return
	 * @version
	 */
	public boolean updateOrderByPid(String pid, String outTradeNo) {
		try {
			String sql = " update `xiaoka-xydk`.order o set o.orderStatus=2, o.payTime=:payTime,o.partnerTradeNo=:outTradeNo where o.pid=:pid ";
			log.info("_-----------------------------------------主动更新订单sql"+sql);
			log.info("--------------------------------------主动更新订单参数=orderId="+pid+"--outTradeNo="+outTradeNo);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("payTime", DateUtil.formatDate(new Date()));
			map.put("pid", pid);
			map.put("outTradeNo", outTradeNo);
			boolean flag = baseDAO.executeNamedCommand(sql, map);
			return flag;
		} catch (Exception e) {
			log.info("---------------------------更新订单异常+pid"+pid+"----outTradeNo="+outTradeNo);
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据ID获取订单信息
	 * 创建人： ln
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param orderId
	 * @return
	 * @version
	 */
	public Map<String, Object> getOrderInfo(String orderId) {
		try {
			String sql = " select * from `xiaoka-xydk`.order o where o.orderId = "+orderId+" ";
			return  baseDAO.queryForMap(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据ID获取订单信息
	 * 创建人： ln
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param orderId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getOrderInfoByPid(String pid) {
		try {
			String sql = " select * from `xiaoka-xydk`.order o where o.pid = "+pid+" ";
			return  baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据多组orderIds查询订单
	 * 创建人： sx
	 * 创建时间： 2017年5月23日
	 * 标记：
	 * @param orderIds
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getOrderInfoByOrderIds(String orderIds) {
		StringBuffer sb = new StringBuffer();
		sb.append("select * from `xiaoka-xydk`.order where orderId in '("+orderIds+")'");
		return baseDAO.queryForList(sb.toString());
	}
}
