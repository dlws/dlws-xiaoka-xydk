/**
 * HighSchoolSkillService.java
 * com.jzwl.xydk.wap.xkhV2.highschoolskill.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkhV2.highschoolskill.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkhV2.highschoolskill.dao.HighSchoolSkillDao;

/**
 * 高校社团，技能达人service
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   wxl
 * @Date	 2017年3月15日 	 
 */
@Service("highSchoolSkillService")
public class HighSchoolSkillService {

	@Autowired
	private HighSchoolSkillDao highSchoolSkillDao;

	public Map getUseridByopenid(Map<String, Object> paramsMap) {

		// TODO Auto-generated method stub
		return highSchoolSkillDao.getUseridByopenid(paramsMap);

	}

	public Map<String, Object> getSholType(String sholId) {

		return highSchoolSkillDao.getSholType(sholId);

	}

}
