package com.jzwl.xydk.wap.xkh2_4.diplomat.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_4.diplomat.dao.DiplomatDao;
import com.jzwl.xydk.wap.xkh2_4.diplomat.pojo.CityDate;
import com.jzwl.xydk.wap.xkh2_4.diplomat.pojo.SchoolDate;


/**
 * 
 * 获取当前所有的省市
 * @author Administrator
 *
 */


@Service
public  class CityAndSchoolDateService {

	@Autowired
	private  DiplomatDao diplomatDao;
	
	private static List<CityDate> citys;
	
	/**
	 * 提供一个获取实例的公共方法
	 * 
	 * @return
	 */
	 public List<CityDate> getInstance() {   
	        if (citys == null){
	        	citys = getAllCityDate();
	        }
	       return citys;   
	 }  
	
	
	/**
	 * 加载当前 取学校的时候再取表中的id值
	 * 逐条查是因为 便于后期配置一省多城市制
	 * @return
	 */
	public List<CityDate> getAllCityDate(){
		
		System.out.println("-------------------------");
		
		List<CityDate> city = new ArrayList<CityDate>();
		
		List<Map<String,Object>> list = diplomatDao.getAllProvince();
		
		for (Map<String, Object> map : list) {
			
			CityDate citySingel = new CityDate();
			citySingel.setText(map.get("province").toString());
			citySingel.setValue(map.get("number").toString());//仅仅是为了占位
			
			List<Map<String,Object>> cach = new ArrayList<Map<String,Object>>();
			List<Map<String,Object>> childList = diplomatDao.getAllCity(map.get("province").toString());
			
			for (int i = 0; i < childList.size(); i++) {
				
				Map<String,Object> temporaryMap = new HashMap<String, Object>();
				temporaryMap.put("text",childList.get(i).get("city"));
				temporaryMap.put("value",childList.get(i).get("id"));
				cach.add(temporaryMap);
			}
			citySingel.setChildren(cach);
			city.add(citySingel);
		}
		
		return city;
	}
	
	/**
	 * 查询当前城市下的
	 * 不同等级的不同学校
	 */
	public List<SchoolDate> getAllSchoolByCityName(String cityId){
		
		List<SchoolDate> listSchoolDate = new ArrayList<SchoolDate>();
		//1.查询当前城市下的学院等级
		List<Map<String,Object>> list = diplomatDao.getLevelByCityId(cityId);
		for (Map<String, Object> map : list) {
			
			SchoolDate schoolDate = new SchoolDate();
			
			schoolDate.setText(map.get("level").toString());
			schoolDate.setValue("1001");//当前未有此字段，仅占位
			
			List<Map<String,Object>> dateList = new ArrayList<Map<String,Object>>();
			List<Map<String,Object>> sourceDate = diplomatDao.getSchoolByCityAndLevel(cityId, map.get("level").toString());
			
			for (Map<String, Object> map2 : sourceDate) {
				
				Map<String,Object> stateMent = new HashMap<String, Object>();
				stateMent.put("value",map2.get("id"));
				stateMent.put("text",map2.get("schoolName"));
				dateList.add(stateMent);
				
			}
			schoolDate.setChildren(dateList);
			listSchoolDate.add(schoolDate);
			
		}
		
		return listSchoolDate;
		
	}
	
	
	
}
