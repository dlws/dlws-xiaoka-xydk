/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class UserSkillclassS  implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
    /**
     * id       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * fatherId       db_column: fatherId 
     */ 	
	private java.lang.Long fatherId;
    /**
     * 分类名称       db_column: className 
     */ 	
	private java.lang.String className;
    /**
     * 发布状态（0：未发布，1：已发布）       db_column: status 
     */ 	
	private java.lang.Integer status;
    /**
     * 设置排列顺序       db_column: ord 
     */ 	
	private java.lang.Integer ord;
    /**
     * 创建时间       db_column: createDate 
     */ 	
	private java.util.Date createDate;
    /**
     * createUser       db_column: createUser 
     */ 	
	private java.lang.String createUser;
    /**
     * isDelete       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
	//columns END

	
	
	
	
	
	
	
	
	
	


	public java.lang.Long getFatherId() {
		return this.fatherId;
	}
	
	public java.lang.Long getId() {
		return id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public void setFatherId(java.lang.Long value) {
		this.fatherId = value;
	}
	

	public java.lang.String getClassName() {
		return this.className;
	}
	
	public void setClassName(java.lang.String value) {
		this.className = value;
	}
	

	public java.lang.Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(java.lang.Integer value) {
		this.status = value;
	}
	

	public java.lang.Integer getOrd() {
		return this.ord;
	}
	
	public void setOrd(java.lang.Integer value) {
		this.ord = value;
	}
	

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

	public java.lang.String getCreateUser() {
		return this.createUser;
	}
	
	public void setCreateUser(java.lang.String value) {
		this.createUser = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

}

