package com.jzwl.xydk.wap.xkh2_3.publish_skill.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.system.base.controller.BaseWeixinController;

/**
 * @author Administrator
 * 发布技能类
 * 1.根据技能类型展示当前的基础技能信息
 * 2.添加当前技能的基础信息
 * 3.根据不同的技能名称，对技能的信息进行修改
 */
@Controller
@RequestMapping(value="/publish_skill")
public class PublishSkillController extends BaseWeixinController{
	
	/*
	 * 增加当前类的日志
	 */
	Logger log = Logger.getLogger(PublishSkillController.class);
	
	
	/**
	 * 根据用户选择的不同技能类型进行不同技能的基础信息展示
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public String toShowBasicSkill(HttpServletRequest request,Model model) throws NoSuchMethodException, SecurityException{
		
		//获取传入参数
		Map<String,Object> map = request.getParameterMap();
		//获取前台传递过来的方法名
		String methodName = map.get("methodName").toString();
		//若传递参数不为空则进行跳转方法的反射 返回Model
		Class pubClass= PublishSkillController.class;

		pubClass.getMethod(methodName);
		
		
		return null;
	}
	
	
	
	

}
