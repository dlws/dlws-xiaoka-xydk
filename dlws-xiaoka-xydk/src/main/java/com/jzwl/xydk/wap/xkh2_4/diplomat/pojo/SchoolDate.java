package com.jzwl.xydk.wap.xkh2_4.diplomat.pojo;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * 组装json数据使用
 * 数据格式value:"1",text:"985/211",children:[{value:"11",text:"华南理工大学"}]}
 */


public class SchoolDate {

	
	/**
	 * 将当前实例初始化好
	 * 对外只提供一个初始化实例的入口
	 */
	
	private String text;
	private String value;
	private List<Map<String,Object>> children;
	
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public List<Map<String, Object>> getChildren() {
		return children;
	}
	public void setChildren(List<Map<String, Object>> children) {
		this.children = children;
	}
	
	
	
	
}
