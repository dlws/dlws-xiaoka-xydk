package com.jzwl.xydk.wap.xkh2_4.diplomat.controller;

import java.util.ArrayList;
//git.oschina.net/dlws/dlws-xiaoka-xydk.git
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.utils.DoubleArithUtil;
import com.jzwl.common.utils.HttpClientUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.business.home.dao.HomeDao;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.controller.OrderSeaController;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.Order;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderSea;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_3.changemanager.service.ChangeMangerService;
import com.jzwl.xydk.wap.xkh2_3.personInfo.pojo.PersonInfoDetail;
import com.jzwl.xydk.wap.xkh2_4.diplomat.pojo.CityDate;
import com.jzwl.xydk.wap.xkh2_4.diplomat.pojo.SchoolDate;
import com.jzwl.xydk.wap.xkh2_4.diplomat.service.CityAndSchoolDateService;
import com.jzwl.xydk.wap.xkh2_4.diplomat.service.DiplomatService;

@Controller
@RequestMapping(value="/diplomatController")
public class DiplomatController  extends BaseWeixinController {
	
	@Autowired
	private ChangeMangerService changeManagerService;
	@Autowired
	private SendMessageService sendMessage;
	@Autowired
	private DiplomatService diplomatService;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private Constants constants;
	@Autowired
	private  CityAndSchoolDateService cityAndSchoolDate;
	@Autowired
	private OrderSeaService orderSeaService;
	@Autowired
	private OrderSeaController orderController;
	@Autowired
	private HomeDao homeDao;
	
	/** 日志 */
	Logger log = Logger.getLogger(DiplomatController.class);
	
	//当前存在异常时跳转路径
	private final String FAIL = "redirect:/home/index.html";

	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到是否更换管理员页面
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/changeManager")
	public String changeManager(HttpServletRequest request, Model model){
		return "/wap/xkh_version_2_4/administrator/administrator_konw";
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击是    跳转到身份验证页面
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @return
	 * @version
	 */
	@RequestMapping(value="/checkOk")
	public String checkOk(HttpServletRequest request, Model model){
		createParameterMap(request);
		try {
			//获取openId 根据openId 查询
			String openId = getCurrentOpenId(request);//从session中获取openId  13522555332
			//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
			//根据openId 获取入驻信息
			Map<String, Object> map = diplomatService.findByOpenId(openId);
			String phoneNumber = "";
			if(map != null && map.size() > 0){
				phoneNumber = (String)map.get("phone");
			}
			String phone = phoneNumber.substring(0, 3);
			String number = phoneNumber.substring(9, phoneNumber.length());
			model.addAttribute("phoneNumber", phoneNumber);
			model.addAttribute("phone", phone);
			model.addAttribute("number", number);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/wap/xkh_version_2_4/administrator/administrator_sure";
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击确定,验证手机号
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/makeSure")
	@ResponseBody
	public Map<String, Object> makeSure(HttpServletRequest request, Model model){
		createParameterMap(request);
		Map<String, Object> flagMap = new HashMap<String, Object>();
		try {
			if(paramsMap != null && paramsMap.size() > 0){
				String phone = (String)paramsMap.get("phoneNumber");
				String openId = getCurrentOpenId(request);//从session中获取openId
				//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
				//根据openId 获取入驻信息
				Map<String, Object> map = diplomatService.findByOpenId(openId);
				if(map != null && map.size() >0){
					String phoneNumber = (String)map.get("phone");
					if(phone.equals(phoneNumber)){
						flagMap.put("flag", true);
						flagMap.put("userId", map.get("id"));//获取用户的id
					}else{
						flagMap.put("flag", false);
					}
				}else{
					flagMap.put("flag", false);
				}
			}else{
				flagMap.put("flag", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flagMap;
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到管理员信息填写页面
	 * 创建人： sx
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/changeInfo")
	public String changeInfo(HttpServletRequest request, Model model){
		log.info("跳转到---------------管理员信息填写页面------");
		createParameterMap(request);
		List<CityDate> cityDate = cityAndSchoolDate.getInstance();
		
		Gson gson = new Gson();  
	    String cityDateJson = gson.toJson(cityDate);  
		model.addAttribute("proList", cityDateJson);
		
		String userId = paramsMap.get("userId").toString();
		model.addAttribute("userId", userId);
		model.addAttribute("paramsMap", paramsMap);
		return "/wap/xkh_version_2_4/administrator/administrator_change";
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击提交验证数据
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/infoSubmit")
	@ResponseBody
	public Map<String, Object> infoSubmit(HttpServletRequest request, Model model){
		log.info("跳转到----------------验证提交数据是否正确------");
		createParameterMap(request);
		Map<String, Object> flagMap = new HashMap<String, Object>();
		Map<String, Object> returnMap = new HashMap<String, Object>();
		try {
			String openId = getCurrentOpenId(request);//从session中获取openId
			//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
			//获取当前用户入驻信息
			Map<String, Object> bascMap = diplomatService.findByOpenId(openId);
			//获取微信号
			String wxNumber = (String)paramsMap.get("wxNumber");
			String userName = (String)paramsMap.get("userName");
			String phoneNumber = (String)paramsMap.get("phoneNumber");
			String email = (String)paramsMap.get("email");
			String userId = (String)paramsMap.get("userId");
			String cityId = (String)paramsMap.get("cityId");
			String schooId = (String)paramsMap.get("schooId");
			
			//当前管理员的电话号码
			String phoneOld = String.valueOf(paramsMap.get("phoneOld"));
			
			flagMap.put("wxNumber", wxNumber);
			flagMap.put("userName", userName);
			flagMap.put("phoneNumber", phoneNumber);
			flagMap.put("email", email);
			
			flagMap.put("cityId", cityId);
			flagMap.put("schooId", schooId);
			//通过微信号查询入驻信息
			Map<String, Object> map = diplomatService.findByWxNumber(flagMap);
			if(map != null && map.size() >0){
				if(!phoneOld.equals(phoneNumber)){
					Map<String, Object> messMap = new HashMap<String, Object>();
					messMap.put("userName", bascMap.get("userName").toString());
					messMap.put("openId", map.get("openId").toString());
					//插入到change_manager_record表
					flagMap.put("basicId", map.get("id").toString());
					flagMap.put("userId", userId);
					String recordId = diplomatService.addRecord(flagMap);
					map.put("recordId", recordId);
					map.put("first", "您将成为新的外交官");
					map.put("keyword1", "点击启用");
					map.put("detail", "点击是否接受。");
					//发送消息
					sendMessage.diplomatChange(map);
					returnMap.put("msg", true);
					return returnMap;
				}
				returnMap.put("msg", "reapet");
				return returnMap;
			}else{
				returnMap.put("msg", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnMap.put("msg", false);
		}
		return returnMap;
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到提示页面
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/prompt")
	public String prompt(HttpServletRequest request, Model model){
		return "/wap/xkh_version_2_4/administrator/administrator_cue";
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：提交成功发送模板消息
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param map
	 * @version
	 */
	public void sendMessageToUser(Map<String, Object> map){
		log.info("提交成功----------------发送模板消息------");
		StringBuffer detail = new StringBuffer();
		WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
		templateMessage.setToUser(map.get("openId").toString());
		detail.append(map.get("nickname").toString() + " 邀请您成为校咖汇的新管理员,您是否接受转权?");
		templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
		Gson gson = new Gson();
		String json = gson.toJson(templateMessage);
		String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
		HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "changeManager", "xydkSystemAuth");
	}
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到提示页面
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/userConfirmation")
	public String userConfirmation(HttpServletRequest request, Model model){
		createParameterMap(request);
		String recordId = paramsMap.get("recordId").toString();
		Map<String,Object> map = diplomatService.getBasicInfoByRecordId(recordId); 
		model.addAttribute("recordId", recordId);
		model.addAttribute("isAgree", map.get("isAgree"));
		model.addAttribute("userName", map.get("userName"));
//		model.addAttribute("contactUser", map.get("contactUser"));

		return "/wap/xkh_version_2_4/administrator/administrator_new";
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击接受页面跳转
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/accept")
	public @ResponseBody Map<String,Object> accept(HttpServletRequest request, Model model){
		createParameterMap(request);
		String newOpenId = getCurrentOpenId(request);
		Map<String, Object> map_ajax = new HashMap<String, Object>();
		try {
			//修改change_manager_record表
			String recordId = paramsMap.get("recordId").toString();
			Map<String,Object> map = diplomatService.getBasicInfoByRecordId(recordId);

			Map<String, Object> insertMap = new HashMap<String, Object>();
			insertMap.put("id", recordId);
			insertMap.put("isAgree", 1);
			insertMap.put("basicId", map.get("id"));
			insertMap.put("userId", map.get("userId"));
			boolean flag = false;
			
			Map<String,Object> currentMap = new HashMap<String, Object>();
			
			currentMap.put("oldOpenId", insertMap.get("openId"));
			currentMap.put("newOpenId", newOpenId);
			//更新当前资源表中的信息 diplomatsourceinfo
			if(diplomatService.updateDiplomatsourceinfo(currentMap)){
				flag = diplomatService.updateChangeManage(insertMap);
			}
			//boolean flag2 = changeManagerService.updateSkillinfo(insertMap);
			//boolean flag3 = changeManagerService.deleteBasicInfo(insertMap);
			if(flag){
				map_ajax.put("recordId", recordId);
				map_ajax.put("first", map.get("userName")+"同意管理员授权");
				map_ajax.put("keyword1", "同意");
				map_ajax.put("detail", "同意管理员授权");
				map_ajax.put("openId", map.get("openId"));
				//发送消息
				sendMessage.numberChange(map_ajax);
				map_ajax.put("msg", "获取信息成功");
				map_ajax.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			}else{
				map_ajax.put("msg", "获取信息失败");
				map_ajax.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			}
		} catch (Exception e) {
			map_ajax.put("msg", "获取信息失败");
			map_ajax.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
		}
		return map_ajax;
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击拒绝接受
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/refuse")
	public String refuse(HttpServletRequest request, Model model){
		createParameterMap(request);
		//String openId = getCurrentOpenId(request);//从session中获取openId
		String recordId = paramsMap.get("recordId").toString();
		Map<String,Object> map = diplomatService.getBasicInfoByRecordId(recordId);
		Map<String, Object> insertMap = new HashMap<String, Object>();
		insertMap.put("id", recordId);
		insertMap.put("isAgree", 2);
		insertMap.put("recordId", recordId);
		insertMap.put("first", map.get("userName")+"拒绝管理员授权");
		insertMap.put("keyword1", "拒绝");
		insertMap.put("detail", "拒绝管理员授权");
		insertMap.put("openId", map.get("openId"));
		//发送消息
		sendMessage.numberChange(insertMap);
		diplomatService.updateChangeManage(insertMap);
		return "redirect:/home/index.html";
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去添加外交官信息或进入外交官信息页面
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：wap
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toAddOrDiplomatInfo")
	public String toAddOrDiplomatInfo(HttpServletRequest request, HttpServletResponse response,Model model) {
		String xkhOpenId = getCurrentOpenId(request);
		try {
			//1.判断外交官是否身份验证过(true:表示存在，false：表示不存在)
			boolean isflag = diplomatService.isDiplomatExistence(xkhOpenId);
			//根据有没有验证外交官信息，从而进行添加或跳转信息页面
			if(isflag){
				//存在，直接跳转信息页面
					//1.1：用户个人信息
					Map<String,Object> map = diplomatService.getUserInfo(xkhOpenId);
					//1.2：资源外交官下的资源
					List<Map<String,Object>> list = diplomatService.querySource(xkhOpenId);
					
					model.addAttribute("userInfo", map);
					model.addAttribute("sourceList", list);
					return "wap/xkh_version_2_4/diplomat/administration_center";
			}else{
				//不存在，跳转外交官身份验证页面
				//获取省市学校信息
				List<Map<String,Object>> list = diplomatService.queryProviceCitySchoolInfo();
				//组装当前省市信息并用json的形式返回 TODO
				//[{value: '110000',text: '北京市',children: [{value: "110101",text: "东城区"}]]
				
				List<CityDate> cityDate = cityAndSchoolDate.getInstance();
				
				Gson gson = new Gson();  
			    String cityDateJson = gson.toJson(cityDate);  
				
				model.addAttribute("cityDateJson", cityDateJson);
				model.addAttribute("schoolList",list);
				return "wap/xkh_version_2_4/diplomat/identity_verification";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/home/index.html";
		}
		
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：保存外交官基本信息
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/addDiplomatInfo")
	public String addDiplomatInfo(HttpServletRequest request, HttpServletResponse response,Model model) {
		//添加外交官基本信息
		//获取参数
		String openId = this.getCurrentOpenId(request);
		if(openId == null){
			log.info("----------当前用户openId为空-----------");
			return "redirect:/home/index.html";
		}
		Map<String, Object> map = createParameterMap(request);
		map.put("openId",openId);
		try {
			boolean flag = diplomatService.addDiplomatInfo(map);
			if(!flag){
				return "redirect:/home/index.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/diplomatController/toAddOrDiplomatInfo.html";
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：外交官下所有的资源
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/queryDiplomatSource")
	public String queryDiplomatSource(HttpServletRequest request, HttpServletResponse response,Model model) {
		String xkhOpenId = getCurrentOpenId(request);
		try {
			//资源列表
			List<Map<String,Object>> list = diplomatService.querySource(xkhOpenId);
			//资源个数
			int size = list.size();
			model.addAttribute("list",list);
			model.addAttribute("count",size);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "wap/xkh_version_2_4/diplomat/resource_librery";
	}
	
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去我的钱包（外交官）
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toMyWallet")
	public String toMyWallet(HttpServletRequest request, HttpServletResponse response,Model model) {
		String xkhOpenId = getCurrentOpenId(request);
		try {
			//查看钱包余额
			Map<String,Object> map = diplomatService.getWallet(xkhOpenId);
			List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			Double actualBalance = 0.0;
			if(map != null&&!map.isEmpty()){
				//交易记录
				list = diplomatService.queryDiplomatRecord(xkhOpenId);
				//根据openId查询所有未审核的记录的总金额
				Map<String,Object> sumMoney =  diplomatService.getMoneyByOpenId(xkhOpenId);
				
				//账户余额
				Double balance = Double.valueOf(map.get("balance").toString());
				//未审核金额
				Double withdrawMoney = Double.valueOf(sumMoney.get("withdrawMoney").toString());
				//扣除未审核金额
				actualBalance = DoubleArithUtil.sub(balance, withdrawMoney);
				if(actualBalance<0){
					actualBalance = 0.0;
				}
				
			}
			model.addAttribute("walletMap",map);
			model.addAttribute("balance",actualBalance);
			model.addAttribute("list",list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "wap/xkh_version_2_4/diplomat/wallet/wallet";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去提现页面(我的钱包页面点击提现按钮)
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/toWithdrawals")
	public String toWithdrawals(HttpServletRequest request, HttpServletResponse response,Model model) {
		Map<String, Object> paraMap = createParameterMap(request);
		String xkhOpenId = getCurrentOpenId(request);
		Map<String,Object> map = diplomatService.getWallet(xkhOpenId);
		
		Double actualBalance = 0d;//默认数据时0.0 测试更改为4.0
		
		if(map==null || map.isEmpty()){
			
			log.info("---------未查询到当前openId在 wallet 表中未查到数据----------");
			
			model.addAttribute("balance",actualBalance);
			
			return "wap/xkh_version_2_4/diplomat/wallet/deposit";
		}
		try {
			//查看钱包余额
			//根据openId查询所有未审核的记录的总金额
			Map<String,Object> sumMoney =  diplomatService.getMoneyByOpenId(xkhOpenId);
			//账户余额
			Double balance = Double.valueOf(map.get("balance").toString());
			//未审核金额
			Double withdrawMoney = Double.valueOf(sumMoney.get("withdrawMoney").toString());
			//扣除未审核金额
		    actualBalance = DoubleArithUtil.sub(balance, withdrawMoney);
			
			model.addAttribute("balance",actualBalance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "wap/xkh_version_2_4/diplomat/wallet/deposit";
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：提现（提现申请表插入记录）
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/withdrawals")
	public String withdrawals(HttpServletRequest request, HttpServletResponse response,Model model) {
		
		String id = "";
		boolean flag = false;
		String openId = getCurrentOpenId(request);
		Map<String, Object> paraMap = createParameterMap(request);
		try {
			paraMap.put("openId",openId);
		    id = diplomatService.withdrawals(paraMap);
		    if(StringUtils.isNotBlank(id)){
		    	flag = true;
		    }
		    model.addAttribute("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/diplomatController/withdrawalsRecord.html?id="+id;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：去提现记录详情页(查询当前的申请记录)
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/withdrawalsRecord")
	public String withdrawalsRecord(HttpServletRequest request, HttpServletResponse response,Model model) {
		String id = request.getParameter("id");
		
		try {
			Map<String,Object> map = diplomatService.getWithdrawalsRecordById(id);
			model.addAttribute("withdrawalsRecord",map);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "wap/xkh_version_2_4/diplomat/wallet/transaction_records_applicant";
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取推广链接(promotional为推广的链接)
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="getPromotionalLinks")
	public String getPromotionalLinks(HttpServletRequest request, HttpServletResponse response,Model model){
		String openId = getCurrentOpenId(request);
		try {
			model.addAttribute("openId", openId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "wap/xkh_version_2_4/diplomat/referral_link";
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：用户点击推广链接，绑定用户与外交官关系
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="promotional")
	public String promotional(HttpServletRequest request, HttpServletResponse response,Model model){
		String xkhOpenId = getCurrentOpenId(request);//用户openId
		String openId = request.getParameter("openId");//外交官openId
		try {
			//首先判断，用户是否已经入住（true 表示存在，false表示不存在）
			boolean flag = diplomatService.isSettledByOpenId(xkhOpenId);
			//如果未入住，让用户去入住，如果已经入住，绑定关系
			if(flag){
				//查询是否已经绑定其他外交官
				if(StringUtils.isBlank(diplomatService.correlatDiplomat(xkhOpenId))){
					//绑定关系
					boolean band = diplomatService.band(xkhOpenId,openId);
					//返回 绑定是否成功
					return FAIL;
				}
				log.info("-----------当前OpenId已经绑定到其他的外交官------------"+xkhOpenId);
				
			}else{
				return "redirect:/userSettled/showAgreement.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return FAIL;
	}
	
	/**
	 * ajax 回传当前省份下的所有学校返回json格式给页面
	 */
	@RequestMapping(value="getAllSchoolByCityName")
	public @ResponseBody String getAllSchoolByCityName(String cityId){
		
		Gson gson = new Gson();  
	    //String cityDateJson = gson.toJson(cityDate); 
		List<SchoolDate> schoolDateList = cityAndSchoolDate.getAllSchoolByCityName(cityId); 
	    String cityDateJson = gson.toJson(schoolDateList); 
	    
		return cityDateJson;
	}
	
	/**
	 * 查询当前的记录记录详情
	 * 查询当前表为 diplomat_divide_withdraw 
	 * 当前必传参数Id
	 */
	@RequestMapping(value="getMoneyHistoryDeatil",params={"id"})
	public String getMoneyHistoryDeatil(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		
		if(StringUtils.isNotBlank(paramsMap.get("id").toString())){
			
			//获取当前表 diplomat_divide_withdraw 当前Map的基本信息
			Map<String,Object> map = diplomatService.getSingleHistory(paramsMap.get("id").toString());
			//说明存在此数据
			if(StringUtils.isNotBlank(map.get("id").toString())){
				//state 状态为 0 分成 1 提现 0的状态展示
				if("0".equals(map.get("state").toString())){
					//获取当前的订单编号
					Order order = orderSeaService.getOrderInfo(map.get("orderId").toString());
					model.addAttribute("order",order);
					//存入当前的订单Id进行查询
					map.put("orderId", order.getOrderId().toString());
					//检查当前用户在订单表
					List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
					//获取当前order详情表中的信息
				    Map<String,String>  orderDeatil = orderController.dealListForOrdDet(orderList);
					//根据当前的OpenId获取用户的信息
				    Map<String,Object> parameters = new HashMap<String,Object>();
				    parameters.put("openId",order.getOpenId());
				    UserBasicinfo userInfo = homeDao.getUserInfo(parameters);
				    
				    model.addAttribute("order",order);
				    model.addAttribute("orderDeatil",orderDeatil);
				    model.addAttribute("userInfo",userInfo);
				}
				model.addAttribute("map", map);
				
				return "wap/xkh_version_2_4/diplomat/wallet/transaction_records";
				
			}
			
		}
		
		return FAIL;
	}
	
	
	
}
