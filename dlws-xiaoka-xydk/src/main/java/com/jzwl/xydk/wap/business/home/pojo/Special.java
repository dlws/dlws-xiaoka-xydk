/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.wap.business.home.pojo;

import com.jzwl.system.base.pojo.BasePojo;

public class Special extends BasePojo implements java.io.Serializable {

	private static final long serialVersionUID = 5454155825314635342L;

	//alias
	public static final String TABLE_ALIAS = "Special";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_TITLE = "标题";
	public static final String ALIAS_CONTENT = "内容";
	public static final String ALIAS_IMGER_URL = "图片路径";
	public static final String ALIAS_ORD = "显示顺序（数字越小显示越靠前）";
	public static final String ALIAS_IS_DISPLAY = "是否显示（0：否，1：是）";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	public static final String ALIAS_IS_DELETE = "是否删除（0：否，1：是）";

	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;

	//columns START
	/**
	 * id       db_column: id 
	 */
	private java.lang.Long id;
	/**
	 * 标题       db_column: title 
	 */
	private java.lang.String title;
	/**
	 * 内容       db_column: content 
	 */
	private java.lang.String content;
	/**
	 * 图片路径       db_column: imgerUrl 
	 */
	private java.lang.String imgerUrl;
	/**
	 * 显示顺序（数字越小显示越靠前）       db_column: ord 
	 */
	private java.lang.Integer ord;
	/**
	 * 是否显示（0：否，1：是）       db_column: isDisplay 
	 */
	private java.lang.Integer isDisplay;
	/**
	 * 创建时间       db_column: createDate 
	 */
	private java.util.Date createDate;
	/**
	 * 是否删除（0：否，1：是）       db_column: isDelete 
	 */
	private java.lang.Integer isDelete;
	/**
	 * 页面跳转链接 
	 */
	private java.lang.String  jumpLink;
	/**
	 * 点赞次数
	 */
	private java.lang.Integer isagree;
	/**
	 * 查看人数
	 */
	private java.lang.Integer viewNum;
	
	private java.lang.String getdate;
	
	//columns END

	public java.lang.Integer getIsagree() {
		return isagree;
	}

	public void setIsagree(java.lang.Integer isagree) {
		this.isagree = isagree;
	}

	public java.lang.Integer getViewNum() {
		return viewNum;
	}

	public void setViewNum(java.lang.Integer viewNum) {
		this.viewNum = viewNum;
	}

	public java.lang.String getJumpLink() {
		return jumpLink;
	}

	public void setJumpLink(java.lang.String jumpLink) {
		this.jumpLink = jumpLink;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public java.lang.String getTitle() {
		return this.title;
	}

	public void setTitle(java.lang.String value) {
		this.title = value;
	}

	public java.lang.String getContent() {
		return this.content;
	}

	public void setContent(java.lang.String value) {
		this.content = value;
	}

	public java.lang.String getImgerUrl() {
		return this.imgerUrl;
	}

	public void setImgerUrl(java.lang.String value) {
		this.imgerUrl = value;
	}

	public java.lang.Integer getOrd() {
		return this.ord;
	}

	public void setOrd(java.lang.Integer value) {
		this.ord = value;
	}

	public java.lang.Integer getIsDisplay() {
		return this.isDisplay;
	}

	public void setIsDisplay(java.lang.Integer value) {
		this.isDisplay = value;
	}

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}

	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class, FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}

	public java.lang.String getGetdate() {
		return getdate;
	}

	public void setGetdate(java.lang.String getdate) {
		this.getdate = getdate;
	}

}
