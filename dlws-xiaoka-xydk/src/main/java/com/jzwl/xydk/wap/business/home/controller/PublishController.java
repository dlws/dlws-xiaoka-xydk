/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.business.home.service.PublishService;

/**
 * BusinessBasicinfo
 * BusinessBasicinfo
 * businessBasicinfo
 * <p>Title:BusinessBasicinfoController </p>
 * 	<p>Description:BusinessBasicinfo </p>
 * 	<p>Company: </p> 
 * 	@author aotu-code 
 */
@Controller
@RequestMapping("/sub")
public class PublishController extends BaseWeixinController {
	
	@Autowired
	private PublishService pubService;
	@Autowired
	private HomeService homeService;
	
	@RequestMapping(value="/toPub")
	public ModelAndView toPub(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mov = new ModelAndView();
		mov.setViewName("/wap/businessActivity/publish");
		return mov;
	}
	
	@RequestMapping(value="/pub")
	public ModelAndView pub(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mov = new ModelAndView();
		String openId = getCurrentOpenId(request); //get  openId
		createParameterMap(request);
		
		paramsMap.put("openId", openId);
		pubService.pub(paramsMap);
		mov.setViewName("redirect:/home/index.html");
		return mov;
	}
	
	@ResponseBody
	@RequestMapping(value="/school",method=RequestMethod.POST)
	public Map<String, Object> getSchool(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		
		List<Map<String, Object>> slist = pubService.getSchool(paramsMap);
		List<Map<String, Object>> result = getJsonResult(slist,"id","schoolName");
		map.put("list", result);
		return map;
	}
	
	@ResponseBody
	@RequestMapping(value="/typeInfo",method=RequestMethod.POST)
	public Map<String, Object> getType(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		
		List<Map<String, Object>> slist = pubService.getDataType(paramsMap);
		List<Map<String, Object>> result = getJsonResult(slist,"id","dic_name");
		map.put("list", result);
		return map;
	}
	
	@ResponseBody
	@RequestMapping(value="/cityInfo",method=RequestMethod.POST)
	public Map<String, Object> getCity(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		
		List<Map<String, Object>> slist = pubService.getCity();
		List<Map<String, Object>> result = getJsonResult(slist,"id","cityName");
		map.put("list", result);
		return map;
	}
	
	
	protected List<Map<String, Object>> getJsonResult(List<Map<String, Object>> list,String valStr,String txtStr){
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		if(list == null){
			return result;
		}
		for(int i=0;i<list.size();i++){
			Map<String, Object> smap = list.get(i);
			Map<String, Object> itemMap = new HashMap<String, Object>();
			itemMap.put("value", smap.get(valStr));
			itemMap.put("text", smap.get(txtStr));
			result.add(itemMap);
		}
		return result;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////
	
	@RequestMapping(value="/toSub")
	public ModelAndView toSubBill(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mov = new ModelAndView();
		createParameterMap(request);
		
//		paramsMap.put("basicId", "1476700994529000");///测试数据
//		paramsMap.put("id", "3");///测试数据
		Map<String, Object> binfo = new HashMap<>();
		Map<String, Object> skinfo = new HashMap<>();
		skinfo = pubService.querySkillInfoPC(paramsMap);
		//获取PC端的图片
//		UserBasicinfo userBasicinfo = homeService.getUserBasicInfo(paramsMap.get("basicId")+"");
		UserBasicinfo userBasicinfo = homeService.getUserBasicInfo(skinfo.get("basicId")+"");
		if(userBasicinfo.getOpenId().equals("0")){
			Map<String,Object> mapPC = pubService.getInfo(userBasicinfo.getId());
			binfo.put("headImgUrl", mapPC.get("headImgUrl"));
			binfo.put("id",userBasicinfo.getId());
			binfo.put("userName",userBasicinfo.getUserName());
//			skinfo = pubService.querySkillInfoPC(paramsMap);
		}else{
			binfo = pubService.queryHeadImg(skinfo.get("basicId")+"");
//			skinfo = pubService.querySkillInfo(paramsMap);
		}
		
		mov.addObject("binfo", binfo);
		mov.addObject("skinfo", skinfo);
		
		mov.setViewName("/wap/businessActivity/contact");
		return mov;
	}
	
	@RequestMapping(value="/sub")
	public ModelAndView subBill(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mov = new ModelAndView();
		createParameterMap(request);
		String openId = getCurrentOpenId(request);
		paramsMap.put("openId", openId);
		paramsMap.put("businessId", 0);//不知道从哪里来，所以暂时设为0
		
		pubService.sub(paramsMap);
		
		mov.setViewName("redirect:/home/index.html");
		return mov;
	}
}

