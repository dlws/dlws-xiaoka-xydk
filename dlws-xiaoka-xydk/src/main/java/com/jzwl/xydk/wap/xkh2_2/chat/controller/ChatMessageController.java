/**
L * ChatController.java
 * com.jzwl.xydk.wap.xkh2_2.chat.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
 */

package com.jzwl.xydk.wap.xkh2_2.chat.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.utils.Util;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.controller.OrderSeaController;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.service.HighSchoolSkillOrderService;
import com.jzwl.xydk.wap.xkh2_4.diplomat.service.DiplomatService;
import com.jzwl.xydk.wap.xkhV2.highschoolskill.service.HighSchoolSkillService;

/**
 * 信息controller
 * <p>
 *
 * @author wxl
 * @Date 2017年4月7日
 */
@Controller("chatMesseage")
@RequestMapping("/chatMesseage")
public class ChatMessageController extends BaseWeixinController {

	@Autowired
	private ChatMessageService chatMessageService;
	
	@Autowired
	private OrderSeaController orderSeaController;
	
	@Autowired
	OrderSeaService orderSeaService;

	@Autowired
	private HighSchoolSkillOrderService highSchoolSkillOrderService;
	
	@Autowired
	private HighSchoolSkillService highSchoolSkillService;
	
	@Autowired
	private DiplomatService diplomatService;
	

	/**
	 * 查询 自己的 未读信息，已读信息，未读信息个数，信息来源头像，
	 * 	信息来源名称， 时间(根据最新时间排序)，信息来源信息摘要，是否删除，聊天类型
	 * <p>
	 * 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/queryChatList")
	public String queryChatList(HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, Object> objmap = new HashMap<String, Object>();
		
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		try {
			createParameterMap(request);
			paramsMap.put("openId", openId);
			List<Map<String, Object>> listmap = chatMessageService.queryChatList(paramsMap);
			
			for(int i=0;i<listmap.size();i++){
				String myOpenId = String.valueOf(listmap.get(i).get("myOpenId"));
				String otherOpenId =String.valueOf(listmap.get(i).get("otherOpenId"));
			    if(openId.equals(myOpenId)){
			    	paramsMap.put("openId", otherOpenId);
			    }else{
			    	paramsMap.put("openId", myOpenId);
			    }
			    Map<String,Object> userMap = highSchoolSkillService.getUseridByopenid(paramsMap);
		    	String nickname = String.valueOf(userMap.get("nickname"));
		    	String headPortrait = String.valueOf(userMap.get("headPortrait"));
		    	listmap.get(i).put("nickname", nickname);
		    	listmap.get(i).put("headpic", headPortrait);
			}
			
			model.addAttribute("listmap", listmap);
			//增加一个查系统消息未读 的方法
			String myOpenId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			paramsMap.put("openId", myOpenId);
			Map<String, Object> sysMap = chatMessageService.querySysMap(paramsMap);
			Map<String, Object> headMap = chatMessageService.getHeadImg(paramsMap);//头像
			model.addAttribute("headMap", headMap);//头像
			model.addAttribute("sysMap", sysMap);
			return "wap/xkh_version_2_2/news";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}
	
	/**
	 * 查询 自己的 未读信息，已读信息，未读信息个数，信息来源头像，
	 * 	信息来源名称， 时间(根据最新时间排序)，信息来源信息摘要，是否删除，聊天类型
	 * <p>
	 * 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/queryStatus")
	public @ResponseBody  Map<String, Object> queryStatus(HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, Object> objmap = new HashMap<String, Object>();
		
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		try {
			createParameterMap(request);
			paramsMap.put("openId", openId);
			List<Map<String, Object>> listmap = chatMessageService.queryChatList(paramsMap);
			
			for(int i=0;i<listmap.size();i++){
				String myOpenId = String.valueOf(listmap.get(i).get("myOpenId"));
				String otherOpenId =String.valueOf(listmap.get(i).get("otherOpenId"));
			    if(openId.equals(myOpenId)){
			    	paramsMap.put("openId", otherOpenId);
			    	Map<String,Object> userMap = highSchoolSkillService.getUseridByopenid(paramsMap);
			    	String nickname = String.valueOf(userMap.get("nickname"));
			    	listmap.get(i).put("nickname", nickname);
			    }else{
			    	paramsMap.put("openId", myOpenId);
			    	Map<String,Object> userMap = highSchoolSkillService.getUseridByopenid(paramsMap);
			    	String nickname = String.valueOf(userMap.get("nickname"));
			    	listmap.get(i).put("nickname", nickname);
			    }
			}
			
			model.addAttribute("listmap", listmap);
			//增加一个查系统消息未读 的方法
			String myOpenId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			paramsMap.put("openId", myOpenId);
			Map<String, Object> sysMap = chatMessageService.querySysMap(paramsMap);
			Map<String, Object> headMap = chatMessageService.getHeadImg(paramsMap);//头像
			model.addAttribute("headMap", headMap);//头像
			
			model.addAttribute("sysMap", sysMap);
			objmap.put("model", model);
			return objmap;
		} catch (Exception e) {
			e.printStackTrace();
			objmap.put("msg", "查询失败");
			return objmap;
		}
	}

	/**
	 * 
	 * 描述:获取聊天的内容 作者:wxl
	 * 
	 * @Date 2017年4月13日
	 */
	@RequestMapping("/getChatInfoById")
	public String getChatInfoById(HttpServletRequest request, HttpServletResponse response, Model model) {

		createParameterMap(request);
		String myOpenId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		paramsMap.put("openId", myOpenId);
		try {
			// 会话--查列表
			List<Map<String, Object>> chatInfoList = chatMessageService.getChatInfoById(paramsMap);
			// 修改未读状态
			boolean chatStatus = chatMessageService.updatechatStatus(paramsMap);
			// 查对话人信息
			Map<String, Object> otherInfo = chatMessageService.getChatTitle(paramsMap);
			String nickname = String.valueOf(otherInfo.get("nickname"));
			Map<String, Object> headMap = chatMessageService.getHeadImg(paramsMap);//头像
			//我的openId
			model.addAttribute("myOpenId", myOpenId);
			model.addAttribute("newsId", paramsMap.get("id"));
			model.addAttribute("chatInfoList", chatInfoList);
			model.addAttribute("otherInfo", otherInfo);
			model.addAttribute("headMap", headMap);
			return "wap/xkh_version_2_2/news_communicate";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}
	
	
	/**
	 * 
	 * 描述:获取聊天的内容 作者:wxl
	 * 
	 * @Date 2017年4月13日 
	 */
	@RequestMapping("/getChatClientSerInfoById")
	public String getChatClientSerInfoById(HttpServletRequest request, HttpServletResponse response, Model model) {

		createParameterMap(request);
		String myOpenId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		try {
			// 会话--查列表
			List<Map<String, Object>> chatInfoList = chatMessageService.getChatInfoById(paramsMap);
			// 修改未读状态
			boolean chatStatus = chatMessageService.updatechatStatus(paramsMap);
			// 查对话人信息
			Map<String, Object> otherInfo = chatMessageService.getChatTitle(paramsMap);
			String nickname = String.valueOf(otherInfo.get("nickname"));
			if("null".equals(nickname)||"".equals(nickname)){
				nickname = "客服";
				otherInfo.put("nickname", nickname);
			}
			//我的openId
			model.addAttribute("myOpenId", myOpenId);
			model.addAttribute("newsId", paramsMap.get("id"));
			model.addAttribute("chatInfoList", chatInfoList);
			model.addAttribute("otherInfo", otherInfo);
			return "wap/xkh_version_2_2/news_communicate";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}

	/**
	 * 
	 * 描述:获取系统消息列表 作者:wxl
	 * 
	 * @Date 2017年4月13日
	 */
	@RequestMapping("/getSysList")
	public  String getSysList(HttpServletRequest request, HttpServletResponse response, Model model) {

		createParameterMap(request);
		String myOpenId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		try {
			// 系统--查列表
			paramsMap.put("meOpenId", myOpenId);
			paramsMap.put("openId", myOpenId);
			Map<String,Object> userMap = highSchoolSkillService.getUseridByopenid(paramsMap);
			Map<String,Object> customMap = diplomatService.getUserInfo(myOpenId);
			
			Map<String, Object> headMap = chatMessageService.getHeadImg(paramsMap);//头像
			model.addAttribute("headMap", headMap);//头像
			paramsMap.put("createDate", customMap.get("createTime"));
			List<Map<String, Object>> sysList = chatMessageService.getSysList(paramsMap);
			for (Map<String, Object> m : sysList) {
				Date messageTime = (Date) m.get("createDate");
				m.put("messageTimeStr", Util.DateFormat(messageTime));
			}
			//当用户头像为空的时候使用微信头像
			if(userMap.isEmpty()){
				userMap.put("headPortrait",customMap.get("headImgUrl"));
			}
			
			// 修改未读状态
			paramsMap.put("otherOpenId", myOpenId);
		    chatMessageService.updatechatSyStatus(paramsMap);
			model.addAttribute("sysList", sysList);
			model.addAttribute("myOpenId", myOpenId);
			model.addAttribute("newsId", paramsMap.get("id"));
			model.addAttribute("userMap", userMap);
			return "wap/xkh_version_2_2/clientService";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}
	
	
	
	/**
	 * 
	 * 描述:获取系统消息列表 作者:wxl
	 * 
	 * @Date 2017年4月13日
	 */
	@RequestMapping("/clientService")
	public @ResponseBody  Map<String, Object> clientService(HttpServletRequest request, HttpServletResponse response, Model model) {

		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		String myOpenId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		try {
			// 系统--查列表
			paramsMap.put("meOpenId", myOpenId);
			paramsMap.put("openId", myOpenId);
			Map<String,Object> userMap = highSchoolSkillService.getUseridByopenid(paramsMap);
			paramsMap.put("createDate", userMap.get("createDate"));
			List<Map<String, Object>> sysList = chatMessageService.getSysList(paramsMap);
			for (Map<String, Object> m : sysList) {
				Date messageTime = (Date) m.get("createDate");
				m.put("messageTimeStr", Util.DateFormat(messageTime));
			}
			
			
			// 修改未读状态
			paramsMap.put("otherOpenId", myOpenId);
		    chatMessageService.updatechatSyStatus(paramsMap);
			model.addAttribute("sysList", sysList);
			model.addAttribute("myOpenId", myOpenId);
			model.addAttribute("sysList", sysList);
			model.addAttribute("otherOpenId", myOpenId);
			map.put("model", model);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "查询失败");
			return map;
		}
	}

	/**
	 * 
	 * 描述:获取系统消息详情 作者:wxl
	 * 
	 * 目的 更新已读未读状态
	 * 
	 * @Date 2017年4月13日
	 */
	@RequestMapping("/getSysInfo")
	public String getSysInfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		//		String myOpenId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		try {
			//订单系统信息详情--更新已读未读
			String orderId = chatMessageService.updateIsRead(paramsMap);
			if ("0" != orderId)
				//TODO 订单详情 方法
				return "redirect:/home/index.html?orderId=" + orderId;
			else
				return "redirect:/home/index.html";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}

	/**
	 * 异步请求 ---意在实现页面刷新 参数：id
	 * 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/getChatListById")
	public @ResponseBody Map<String, Object> getChatListById(HttpServletRequest request, HttpServletResponse response, Model model) {

		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> timemap = new HashMap<String, Object>();
		String myOpenId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		try {
			List<Map<String, Object>> chatInfoList = chatMessageService.getChatInfoById(paramsMap);
			Map<String, Object> otherInfo = chatMessageService.getChatTitle(paramsMap);
			Map<String, Object> headMap = chatMessageService.getHeadImg(paramsMap);//头像
			model.addAttribute("headMap", headMap);//头像
			model.addAttribute("myOpenId", myOpenId);
			model.addAttribute("newsId", paramsMap.get("id"));

			for (Map<String, Object> m : chatInfoList) {
				Date messageTime = (Date) m.get("createDate");
				m.put("messageTimeStr", Util.DateFormat(messageTime));
			}
			model.addAttribute("chatInfoList", chatInfoList);
			model.addAttribute("otherInfo", otherInfo);
			map.put("model", model);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "查询失败");
			return map;
		}
	}

	/**
	 * 发送聊天 实际参数： newsId，myOpenId，otherOpenId，contentChat
	 * 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/weChat")
	public @ResponseBody Map<String, Object> weChat(HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, Object> map = new HashMap<String, Object>();
		createParameterMap(request);
		try {
			boolean isSuccess = chatMessageService.insertChatContent(paramsMap);
			if (isSuccess) {
				map.put("msg", "添加信息成功");
				map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);

			} else {
				map.put("msg", "添加信息失败");
				map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			}
		} catch (Exception e) {
			map.put("msg", "添加信息失败");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
		}
		return map;

	}

	/**
	 * 创建对话关系
	 * myOpenid otherOpenId
	 *
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/createChatRelation")
	public String createChatRelation(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String myOpenId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		paramsMap.put("myOpenId", myOpenId);
		try {
			String isExistId = chatMessageService.getChatRelation(paramsMap);
			if ("0" != isExistId) {
				return "redirect:/chatMesseage/getChatInfoById.html?id=" + isExistId;
			} else {
				String newsId = chatMessageService.createChatRelation(paramsMap);
				if ("0" != newsId)
					return "redirect:/chatMesseage/getChatInfoById.html?id=" + newsId;
				else
					return "error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";// 跳转到首页
		}
	}

	/**
	 * 查询订单详情
	 * orderId
	 *
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/queryOrderDetail")
	public String queryOrderDetail(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String channel ="buyer";
		//增加传入渠道区分  卖家 channel=sell 和 买家 channel=buyer 如果为空，默认为买家
		if(paramsMap.containsKey("channel")){
			if(paramsMap.get("channel")!=null&&StringUtils.isNotBlank(paramsMap.get("channel").toString())){
				channel = paramsMap.get("channel").toString();
			}
		}
		
		try {
			String orderId = chatMessageService.updateIsRead(paramsMap);
			if ("0" != orderId) {
				Map<String, Object> orderMap = highSchoolSkillOrderService.getOrderDetail(paramsMap);
				String orderType = orderMap.get("enterType").toString();
				if ("jzzx".equals(orderType)) {
					return "redirect:/placeOrder/orderDetail.html?orderId=" + orderId+"&channel="+channel;
				} else if ("jztf".equals(orderType)) {
					return "redirect:/skillOrder/skillOrderDetail.html?orderId=" + orderId+"&channel="+channel;
				} else if ("jndr".equals(orderType) || "gxst".equals(orderType)) {
					return "redirect:/placeHighSchoolSkillOrder/highSchoolSkillOrderDetail.html?orderId=" + orderId+"&channel="+channel;
				} else if ("sqdv".equals(orderType)) {
					//根据当前orderId查询子表  区分：自媒体 和 其他
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("orderId", orderMap.get("orderId"));
					map.put("isDelete", 0);
					List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
					Map<String,String> mapdetail = orderSeaController.dealListForOrdDet(orderList);
					if(mapdetail.containsKey("accurateVal")){
						return "redirect:/sqdvOrder/sqdvOrderDetail.html?orderId=" + orderId+"&channel="+channel;
					}
					return "redirect:/skillOrder/skillOrderDetail.html?orderId=" + orderId+"&channel="+channel;
					
				} else if ("cdzy".equals(orderType)) {
					return "redirect:/fileOrder/fileOrderDetail.html?orderId=" + orderId+"&channel="+channel;
				} else if ("xnsj".equals(orderType)) {
					return "redirect:/schoolBusOrder/schoolOrderDetail.html?orderId=" + orderId+"&channel="+channel;
				}
			} else {
				request.setAttribute("msg", "状态更新失败！");
				return "redirect:/home/index.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";
		}
		return null;
	}
	
	
	/**
	 * 删除会话
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deletChat")
	public @ResponseBody boolean deleteBannerInfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		boolean flag = false;
		try {
			createParameterMap(request);
			flag = chatMessageService.deletChat(paramsMap);
			if (flag) {
				return flag;
			} else {
				model.addAttribute("msg", "删除失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "删除失败");
			return flag;
		}
		return flag;

	}
	
	
}
