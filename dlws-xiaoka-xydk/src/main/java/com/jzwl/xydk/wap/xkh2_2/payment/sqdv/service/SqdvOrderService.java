package com.jzwl.xydk.wap.xkh2_2.payment.sqdv.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.payment.sqdv.dao.SqdvOrderDao;

@Service("sqdvOrderService")
public class SqdvOrderService {
	@Autowired
	private SqdvOrderDao sqdvOrderDao;

	public boolean deleteOrder(Map<String, Object> map) {
		return sqdvOrderDao.deleteOrder(map);
	}

	public String addOrder(Map<String, Object> map) {
		return sqdvOrderDao.addOrder(map);

	}

	public Map getBaseInfo(Map<String, Object> map) {
		return sqdvOrderDao.getBaseInfo(map);
	}
	
	//订单详情
	public Map getOrderById(String skillId) {
		return sqdvOrderDao.getOrderById(skillId);
	}
	/**
	 * 查询子表2
	 * @param map
	 * dxf
	 * @return list
	 */
	public List querySettleList(Map<String, Object> map) {

		return sqdvOrderDao.querySettleList(map);
	}
	
	//取消订单
		public boolean cancelOrder(Map<String,Object>map) {
			return sqdvOrderDao.cancelOrder(map);
		}

}
