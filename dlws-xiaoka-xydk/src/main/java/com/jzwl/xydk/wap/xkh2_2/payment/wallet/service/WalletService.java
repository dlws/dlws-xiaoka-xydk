package com.jzwl.xydk.wap.xkh2_2.payment.wallet.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.manager.dividewithdraw.pojo.DivideDraw;
import com.jzwl.xydk.wap.xkh2_2.payment.wallet.dao.WalletDao;

@Service("walletService")
public class WalletService {

	@Autowired
	private WalletDao walletDao;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据用户openId查询钱包信息
	 * 创建人： sx
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> getWalletByOpenId(String openId) {
		return walletDao.getWalletByOpenId(openId);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据用户openId查询记录表信息
	 * 创建人： sx
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public List<DivideDraw> getDivideDrawByOpenId(String openId) {
		return walletDao.getDivideDrawByOpenId(openId);
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据openId查询未审核总金额
	 * 创建人： sx
	 * 创建时间： 2017年4月18日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> getMoneyByOpenId(String openId) {
		return walletDao.getMoneyByOpenId(openId);
	}

}
