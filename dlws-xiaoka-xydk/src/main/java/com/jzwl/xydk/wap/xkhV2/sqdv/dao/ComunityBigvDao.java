package com.jzwl.xydk.wap.xkhV2.sqdv.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;

@Repository("comunityBigvDao")
public class ComunityBigvDao {

	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	// 获取字典资源类型
	public List<Map<String, Object>> getSourceTypeByCode() {
		String sql = "select * from v2_dic_data WHERE dic_id in(select dic_id from `xiaoka`.v2_dic_type WHERE dic_code='sourceType')";
		return baseDAO.queryForList(sql);
	}

	// 获取子表
	public List<Map<String, Object>> getSourceTypeBySon() {
		String sql = "SELECT id,fatherId,className,classValue FROM  `xiaoka-xydk`.user_skillclass_son where isDelete=0";
		return baseDAO.queryForList(sql);
	}

	/*
	 * 添加入驻资源
	 * 
	 * @param map
	 * 
	 * @return flag
	 */
	public boolean addSettleUser(Map<String, Object> map) {
		Date date = new Date();
		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		String basId = Sequence.nextId();
		map.put("id", basId);
		map.put("auditState", 0);
		map.put("createDate", date);
		map.put("isRecommend", 0);// 是否推荐
		map.put("skillStatus", 0);
		map.put("isDelete", 0);
		map.put("viewNum", 0);
		Map<String, Object> daMap = new HashMap<String, Object>();
		daMap = map;

		String addBasiSql = "insert into `xiaoka-xydk`.user_basicinfo "
				+ " (id,userName,nickname,cityId,schoolId,phoneNumber,email,wxNumber,openId,aboutMe,"
				+ "headPortrait,auditState,viewNum,createDate,isRecommend,skillStatus,isDelete,enterId,contactUser) "
				+ " values "
				+ " (:id,:userName,:userName,:cityId,:schoolId,:phoneNumber,:email,:wxNumber,:openId,:aboutMe,"
				+ ":picUrl,:auditState,:viewNum,:createDate,:isRecommend,:skillStatus,:isDelete,:enterId,:contactUser)";

		boolean flag = baseDAO.executeNamedCommand(addBasiSql, map);
		daMap.remove("id");
		daMap.remove("openId");
		daMap.remove("auditState");
		daMap.remove("proId");
		daMap.remove("cityId");
		daMap.remove("cityIds");
		daMap.remove("enterId");
		daMap.remove("enterType");
		daMap.remove("wxNumber");
		daMap.remove("viewNum");
		daMap.remove("picUrl");
		daMap.remove("aboutMe");
		daMap.remove("phoneNumber");
		daMap.remove("activUrl");
		daMap.remove("contactUser");
		daMap.remove("email");
		daMap.remove("scholType");
		daMap.remove("schoolId");
		daMap.remove("userName");
		daMap.remove("keyValue");
		daMap.remove("auditState");
		daMap.remove("createDate");
		daMap.remove("isRecommend");
		daMap.remove("skillStatus");
		daMap.remove("isDelete");
		daMap.remove("nickname");
		daMap.remove("headPortrait");

		String DataSql = "insert into `xiaoka-xydk`.user_skillinfo_data(id,pid,typeName,typeValue,isDelete,createDate)"
				+ "values"
				+ "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
		boolean temp = false;
		if (flag) {
			for (Entry<String, Object> entry : daMap.entrySet()) {
				Map<String, Object> inserMap = new HashMap<String, Object>();
				inserMap.put("id", Sequence.nextId());
				inserMap.put("pid", basId);
				inserMap.put("typeName", entry.getKey());
				inserMap.put("typeValue", entry.getValue());
				inserMap.put("isDelete", 0);
				inserMap.put("createDate", new Date());
				temp = baseDAO.executeNamedCommand(DataSql, inserMap);
			}
		}

		return temp;
	}

	// 修改入驻资源
	public boolean upSettleVersion3(Map<String, Object> map) {

		String sql = "DELETE FROM `xiaoka-xydk`.user_skillinfo_data WHERE pid=:id";

		baseDAO.executeNamedCommand(sql, map);

		Date date = new Date();
		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		String basId = String.valueOf(map.get("id"));
		String myself = String.valueOf(map.get("myself"));
		map.put("createDate", date);
		if ("yes".equals(myself)) {
			map.put("auditState", 1);
		} else {
			map.put("auditState", 0);// 移动端默认未审核
		}
		map.put("isRecommend", 0);// 是否推荐
		map.put("skillStatus", 0);
		map.put("isDelete", 0);
		Map<String, Object> daMap = new HashMap<String, Object>();
		daMap = map;

		String upBasiSql = "update  `xiaoka-xydk`.user_basicinfo set userName=:userName,nickname=:userName,"
				+ " cityId=:cityId,schoolId=:schoolId,phoneNumber=:phoneNumber,email=:email,wxNumber=:wxNumber,aboutMe=:aboutMe,"
				+ "headPortrait=:picUrl,contactUser=:contactUser,createDate=:createDate,auditState=:auditState where id= '"
				+ map.get("id") + "'";
		baseDAO.executeNamedCommand(upBasiSql, map);
		daMap.remove("id");
		daMap.remove("openId");
		daMap.remove("auditState");
		daMap.remove("proId");
		daMap.remove("cityId");
		daMap.remove("cityIds");
		daMap.remove("enterId");
		daMap.remove("enterType");
		daMap.remove("wxNumber");
		daMap.remove("viewNum");
		daMap.remove("picUrl");
		daMap.remove("aboutMe");
		daMap.remove("phoneNumber");
		daMap.remove("activUrl");
		daMap.remove("contactUser");
		daMap.remove("email");
		daMap.remove("scholType");
		daMap.remove("schoolId");
		daMap.remove("userName");
		daMap.remove("keyValue");
		daMap.remove("auditState");
		daMap.remove("createDate");
		daMap.remove("isRecommend");
		daMap.remove("skillStatus");
		daMap.remove("isDelete");
		daMap.remove("province");

		String DataSql = "insert into `xiaoka-xydk`.user_skillinfo_data(id,pid,typeName,typeValue,isDelete,createDate)"
				+ "values"
				+ "(:id,:pid,:typeName,:typeValue,:isDelete,:createDate)";
		for (Entry<String, Object> entry : daMap.entrySet()) {
			Map<String, Object> inserMap = new HashMap<String, Object>();
			inserMap.put("id", Sequence.nextId());
			inserMap.put("pid", basId);
			inserMap.put("typeName", entry.getKey());
			inserMap.put("typeValue", entry.getValue());
			inserMap.put("isDelete", 0);
			inserMap.put("createDate", new Date());
			baseDAO.executeNamedCommand(DataSql, inserMap);
		}

		return true;

	}

}
