package com.jzwl.xydk.wap.xkh2_3.personInfo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.label.service.LabelService;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.wap.business.home.dao.HomeDao;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.xkh2_3.personInfo.pojo.PersonInfoDetail;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;
import com.jzwl.xydk.wap.xkh2_3.publish.controller.PublishXkhController;
import com.jzwl.xydk.wap.xkh2_3.skill_detail.controller.SkillDetailContrller;

import edu.emory.mathcs.backport.java.util.Arrays;


/**
 * @author Administrator
 * 用户详情页面 2.3 根据技能
 */

@Controller
@RequestMapping(value="/personInfoDetail")
public class PersonInfoDetailController extends BaseWeixinController{
	
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	
	@Autowired
	private HomeService homeService;
	
	@Autowired
	private HomeDao homeDao;
	
	@Autowired
	private PreferredServiceService preferredServiceService;
	
	@Autowired
	private LabelService labelService;
	
	/** 日志 */
	Logger log = Logger.getLogger(PersonInfoDetailController.class);
	
	
	/**
	 * 获取当前用户的所有技能
	 * 高校社团/社群大V时有精准投放
	 * 代码太长了有时间再整理
	 * @return
	 */
	@RequestMapping(value="/detail")
	public String detail(HttpServletRequest request,HttpServletResponse response, Model model){
		
		
		//判定入驻类型是否有高校社团或社群大V 默认为否
		boolean flage = false;
		boolean isMyself = false;//表示默认非自己
		
		createParameterMap(request);
		
		Map<String,Object> map = new HashMap<String, Object>();
		
		//分项页面展示类型
		List<PersonInfoDetail> gxstList = new ArrayList<PersonInfoDetail>();
		List<PersonInfoDetail> cdzyList = new ArrayList<PersonInfoDetail>();
		List<PersonInfoDetail> xnsjList = new ArrayList<PersonInfoDetail>();
		List<PersonInfoDetail> sqdvList = new ArrayList<PersonInfoDetail>();
		List<PersonInfoDetail> jndrList = new ArrayList<PersonInfoDetail>();
		
		//配置包含精准投放类型
		String[] arrays = {GlobalConstant.ENTER_TYPE_GXST,GlobalConstant.ENTER_TYPE_SQDV};
		
		//传递参数Mapper
		Map<String,Object> mapper = new HashMap<String, Object>();
		//获取当前用户的OpenId
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		//获取当前用户的个人信息
		map.put("id", paramsMap.get("id"));//为当前用户Id
		//获取
		UserBasicinfo userInfo = homeService.getUserInfo(map);
		if(userInfo==null){
			log.info("-------当前userInfo信息为空传入的ID值为--------"+paramsMap.get("id"));
			return "/error";
		}
		//增加为空则为匿名
		if(StringUtils.isBlank(userInfo.getNickname())){
			
			userInfo.setNickname("匿名");
			
			if(StringUtils.isNotBlank(userInfo.getUserName())){
				
				userInfo.setNickname(userInfo.getUserName());
			}
		}
		
		//获取当前用户的技能信息
		List<PersonInfoDetail> list = personInfoDetSer.getUserSkillInfo(userInfo.getId().toString());
		//关于优选服务中的信息
		List<Map<String,Object>> preferList = new ArrayList<Map<String,Object>>();
		//获取当前的大小
		List<String> labelList = getLableTransla(userInfo.getLabelId());
		
		for (int i = 0; i < list.size(); i++) {
			
			//如果包含则返回true
			if(Arrays.toString(arrays).contains(list.get(i).getFathValue())){
				flage = true;
			}
			mapper.put("pid", list.get(i).getId());
			try {
				//查询当前技能的技能详情
				Map<String,Object> detailMap = personInfoDetSer.getuserSkillinfoDetail(mapper);
				
				if(GlobalConstant.ENTER_TYPE_XNSJ.equals(list.get(i).getFathValue())){
					
					String price = SkillDetailContrller.getSectionPrice(detailMap);
					detailMap.put("skillPrice",price);
					
				}
				list.get(i).setMapDetail(detailMap);
				//查询当前的单位，并且以文字的形式在前端页面展示、
				list.get(i).setUnit("次");
				if(detailMap.containsKey("company")){
					
					Map<String,Object> pamaterMap = new HashMap<String, Object>();
					pamaterMap.put("id",detailMap.get("company"));
					//根据当前用户的ID值取他的单位
					pamaterMap = personInfoDetSer.getUnit(pamaterMap);
					if(pamaterMap!=null){
						list.get(i).setUnit(pamaterMap.get("dic_name").toString());
					}
					
				}
				//若为校内商家 或 场地资源
				if(GlobalConstant.ENTER_TYPE_CDZY.equals(list.get(i).getFathValue())||GlobalConstant.ENTER_TYPE_XNSJ.equals(list.get(i).getFathValue())){
					//将近景，中景，远景的图片放到技能的bean中到前端展示
					List<String> listClose = StringUtil.dealString(detailMap.get("closePhoto").toString());
					List<String> listMiddle =StringUtil.dealString(detailMap.get("allPhoto").toString()) ;
					List<String> listAll = StringUtil.dealString(detailMap.get("middlePho").toString());
					list.get(i).setCloseList(listClose);
					list.get(i).setMiddelList(listMiddle);
					list.get(i).setAllList(listAll);
				}
					//若为社群大V
					if(GlobalConstant.ENTER_TYPE_SQDV.equals(list.get(i).getFathValue())){
						
						//自媒体重新存储价格
						if("zmt".equals(list.get(i).getSonValue())){
							String price = detailMap.get("lessLinePrice").toString()+"~"+detailMap.get("topLinePrice");
							detailMap.put("skillPrice",price);
						}
						List<String> dairyList = StringUtil.dealString(detailMap.get("dairyTweetUrl").toString());
						list.get(i).setDairyList(dairyList);
					}
					//若为高校社团，或技能达人
					if(GlobalConstant.ENTER_TYPE_GXST.equals(list.get(i).getFathValue())||GlobalConstant.ENTER_TYPE_JNDR.equals(list.get(i).getFathValue())){
						List<String> activtyUrlList = StringUtil.dealString(detailMap.get("activtyUrl").toString());
						list.get(i).setActivtyList(activtyUrlList);
					}
					if(GlobalConstant.ENTER_TYPE_GXST.equals(list.get(i).getFathValue())){
						gxstList.add(list.get(i));
					}
					if(GlobalConstant.ENTER_TYPE_JNDR.equals(list.get(i).getFathValue())){
						jndrList.add(list.get(i));
					}
					if(GlobalConstant.ENTER_TYPE_CDZY.equals(list.get(i).getFathValue())){
						cdzyList.add(list.get(i));
					}
					if(GlobalConstant.ENTER_TYPE_XNSJ.equals(list.get(i).getFathValue())){
						xnsjList.add(list.get(i));
					}
					if(GlobalConstant.ENTER_TYPE_SQDV.equals(list.get(i).getFathValue())){
						sqdvList.add(list.get(i));
					}
				} catch (Exception e) {
				// TODO: handle exception
				log.info("-----------the current skillId is :------------"+ list.get(i).getId());
			}
			
		}
		//加载当前优选服务中的内容 1.精准咨询 
		mapper.put("enterTypeValue","jzzx");
		mapper.put("isDelete", "0");
		Map<String,Object>  jzzx = preferredServiceService.getPreferredService(mapper);	
		preferList.add(jzzx);
		
		if(flage){
			mapper.put("enterTypeValue","jztf");
			Map<String,Object>  jztf = preferredServiceService.getPreferredService(mapper);
			preferList.add(jztf);
		}
		//判定当前用户是否为自己点击
		if(openId.equals(userInfo.getOpenId())){
			//改变isMyself 状态
			isMyself = true;
		}
		
		//关于用户技能信息
		model.addAttribute("list",list);
		//关于用户信息
		model.addAttribute("userInfo", userInfo);
		//关于优选服务
		model.addAttribute("preferList",preferList);
		//跳转到当前页面
		model.addAttribute("labelList", labelList);
		//true is myself
		model.addAttribute("isMyself",isMyself);
		//分项用户技能信息
		model.addAttribute("gxstList", gxstList);
		model.addAttribute("jndrList", jndrList);
		model.addAttribute("cdzyList", cdzyList);
		model.addAttribute("xnsjList", xnsjList);
		model.addAttribute("sqdvList", sqdvList);
		//跳转到订单详情页面
		return "/wap/xkh_version_2_3/person_detail";
	}
	
	
	/**
	 * 根据传入的标签名称 查询标签名
	 * @param lables
	 * @return
	 */
	public List<String> getLableTransla(String lables){
		
		List<String> list = new  ArrayList<String>();
		if(StringUtils.isNotBlank(lables)){
			
			//减去当前最后一位 处理逗号
			if(lables.endsWith(",")){
				lables = lables.substring(0, lables.length()-1);
			}
			List<Map<String,Object>> labelArray =  labelService.queryLabelById(lables);
			
			for (Map<String, Object> map : labelArray) {
				list.add(map.get("labelName").toString());
			}
		}
		
		return list;
	}
	
	
	
	
	

}
