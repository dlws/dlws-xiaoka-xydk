package com.jzwl.xydk.wap.xkh2_2.ordseach.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.business.home.dao.HomeDao;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.xkh2_2.ordseach.dao.OrderSeaDao;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.Order;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderAndUserInfo;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderSea;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;
import com.jzwl.xydk.wap.xkh2_3.skill_detail.service.SkillDetailService;

@Service("orderSeaService")
public class OrderSeaService {

	@Autowired
	private OrderSeaDao orderSeaDao;
	
	@Autowired
	private HomeDao homeDao;
	
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	
	@Autowired
	private SkillDetailService userSkillService;

	//查询当前用户的订单(支持增加条件)
	public List<OrderSea> toSearchOrder(Map<String, Object> map) {

		return orderSeaDao.toSearchOrder(map);
	}

	//更改当前订单状态
	public boolean updateOrderState(Map<String, Object> map) {

		return orderSeaDao.updateOrderState(map);
	}

	//查询当前用户订单的详细信息detail 
	public List<OrderDetail> searchOrderDetail(Map<String, Object> map) {

		return orderSeaDao.searchOrderDetail(map);
	}
	
	//查询当前用户订单的详细信息detail 
	public List<OrderDetail> searchOrderDetailByPid(Map<String, Object> map) {

		return orderSeaDao.searchOrderDetailByPid(map);
	}
	
	//查询当前用户某一个状态的订单总数
	public int searchOrderCount(Map<String, Object> map){
		
		return orderSeaDao.searchOrderCount(map);
	}
	//获取订单的信息
	public Order getOrderInfo(String orderId){
		return orderSeaDao.getOrderInfo(orderId);
	}
	//根据订单的Id获取订单详情中的技能名称
	public Map<String,Object> getOrderDetailInfoByskillName(String orderId){
		return orderSeaDao.getOrderDetailInfoByskillName(orderId);
	}
	//添加退款信息
	public boolean addRefundInfo(Map<String, Object> map) {

		return orderSeaDao.addRefundInfo(map);
	}
	//获取要退款的金额信息
	public Map<String,Object> getExpenseAmount(){
		return orderSeaDao.getExpenseAmount();
	} 
	//获取银行的信息
	public List<Map<String,Object>> getBankInfo(){
		return orderSeaDao.getBankInfo();
	}
	//更新退款的状态
	public boolean updateRefundInfo(String orderId, String checkStatus,String orderSourceStatus) {

		return orderSeaDao.updateRefundInfo(orderId, checkStatus, orderSourceStatus);
	}
	//查询当前用户的订单(支持增加条件)
	public List<OrderSea> getSellerOrder(String openId) {

		return orderSeaDao.getSellerOrder(openId);
	}
	
	/**
	 * 查询当前关于相同pid下的订单信息和用户信息
	 * @param pid
	 * @return
	 */
	public List<OrderAndUserInfo> getAllOrderForSeller(String pid){
		
		List<Map<String,Object>> list = orderSeaDao.getAllOrderForSeller(pid);
		
		List<OrderAndUserInfo> listRepositery = new ArrayList<OrderAndUserInfo>();
		
		for (int i = 0; i < list.size(); i++) {
			
			OrderAndUserInfo message = new OrderAndUserInfo();
			String orderId = list.get(i).get("orderId").toString();
			
			//优选服务类型直接取sellerOpenId
			if("jzzx".equals(list.get(i).get("enterType").toString())||
					"jztf".equals(list.get(i).get("enterType").toString())){
				
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("orderId",orderId);
				List<OrderDetail>  orderDetail = this.searchOrderDetail(map);
				Map<String,String> mapdetail = dealListForOrdDet(orderDetail);
				
				String sellerOpenId = list.get(i).get("sellerOpenId").toString();
				Map<String,Object> mapper = new HashMap<String, Object>(); 
				mapper.put("openId", sellerOpenId);
				UserBasicinfo userInfo = homeDao.getUserInfo(mapper);
				
				message.setUserInfo(userInfo);//用户详情
				message.setOrderDetil(mapdetail);//订单信息
			}else{//根据当前用户的skillId进行查询
				
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("orderId",orderId);
				List<OrderDetail>  orderDetail = this.searchOrderDetail(map);
				Map<String,String> mapdetail = dealListForOrdDet(orderDetail);//处理之后的Map值
				//查询关于当前用户的技能的信息
				Map<String,Object> skillInfo = userSkillService.getSkillInfo(mapdetail.get("skillId").toString());
				//根据用户的basicId查询当前用户的信息
				Map<String,Object> mapper = new HashMap<String, Object>(); 
				mapper.put("basicId", skillInfo.get("basicId"));
				UserBasicinfo userInfo = homeDao.getUserInfo(mapper);
				
				message.setUserInfo(userInfo);//用户信息
				message.setOrderDetil(mapdetail);//订单详情
			}
			message.setOrderMessage(list.get(i));
			
			listRepositery.add(message);
			
		}
		return listRepositery;
	}
	/**
	 * 单独根据订单获取用户的信息
	 * @param orderList
	 * @return
	 */
	public List<Map<String,Object>> getUserMessageByOrder(String pid){
		
		List<Map<String,Object>> list = orderSeaDao.getAllOrderForSeller(pid);
		
		List<Map<String,Object>> userList = new ArrayList<Map<String,Object>>();
		
		for (int i = 0; i < list.size(); i++) {
			
			
			String orderId = list.get(i).get("orderId").toString();
			//优选服务类型直接取sellerOpenId
			if("jzzx".equals(list.get(i).get("enterType").toString())||
					"jztf".equals(list.get(i).get("enterType").toString())){
				
				String sellerOpenId = list.get(i).get("sellerOpenId").toString();
				Map<String,Object> mapper = new HashMap<String, Object>(); 
				mapper = orderSeaDao.getUserMessageByOpenId(sellerOpenId);
				userList.add(mapper);
			}else{//根据当前用户的skillId进行查询
				
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("orderId",orderId);
				List<OrderDetail>  orderDetail = this.searchOrderDetail(map);
				Map<String,String> mapdetail = dealListForOrdDet(orderDetail);//处理之后的Map值
				//查询关于当前用户的技能的信息
				Map<String,Object> skillInfo = userSkillService.getSkillInfo(mapdetail.get("skillId").toString());
				//根据用户的basicId查询当前用户的信息
				Map<String,Object> mapper = new HashMap<String, Object>(); 
				mapper = orderSeaDao.getUserMessageByBasicId(skillInfo.get("basicId").toString());
				userList.add(mapper);
			}
			
		}
		
		return userList;
	}
	
	
	
	
	
	
	//处理List 以key value的形式展示
	public Map<String,String> dealListForOrdDet(List<OrderDetail> orderList){
			
			Map<String,String> result = new HashMap<String, String>();
			for (OrderDetail orderDetail : orderList) {
				 result.put(orderDetail.getTypeName(), orderDetail.getTypeValue());
				 
			}
			return result;
	}
		
	
	
	
}
