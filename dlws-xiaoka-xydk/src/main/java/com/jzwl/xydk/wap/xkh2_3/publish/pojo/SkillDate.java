package com.jzwl.xydk.wap.xkh2_3.publish.pojo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 		查询关于技能父类标签和子类POJO
 * </p>
 * @author Administrator
 *
 */
public class SkillDate {
	
	
	private Map<String,Object> fatherSkill;
	
	private List<Map<String,Object>> sonSkill;
	
	

	public Map<String, Object> getFatherSkill() {
		return fatherSkill;
	}

	public void setFatherSkill(Map<String, Object> fatherSkill) {
		this.fatherSkill = fatherSkill;
	}

	public List<Map<String, Object>> getSonSkill() {
		return sonSkill;
	}

	public void setSonSkill(List<Map<String, Object>> sonSkill) {
		this.sonSkill = sonSkill;
	}
	
	

}
