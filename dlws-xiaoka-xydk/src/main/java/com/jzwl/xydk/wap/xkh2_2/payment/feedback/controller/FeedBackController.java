package com.jzwl.xydk.wap.xkh2_2.payment.feedback.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.controller.OrderSeaController;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_2.payment.cust_interven.pojo.CustomerIntervention;
import com.jzwl.xydk.wap.xkh2_2.payment.cust_interven.service.CustomerInterventionService;
import com.jzwl.xydk.wap.xkh2_2.payment.evaluate.pojo.ServiceEvaluate;
import com.jzwl.xydk.wap.xkh2_2.payment.evaluate.service.ServiceEvaluateService;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.pojo.FeedBack;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.service.FeedBackService;

/**
 * @author 初方正
 * 该类是反馈相关
 */

@Controller
@RequestMapping(value="/feedback")
public class FeedBackController extends BaseWeixinController{
	
	@Autowired
	private FeedBackService feedBackService;
	
	@Autowired
	private OrderSeaService orderSeaService;
	
	@Autowired
	private SendMessageService sendMessage;
	
	@Autowired
	private ServiceEvaluateService serviceEval;
	
	@Autowired
	private OrderSeaController orderSea;
	
	@Autowired
	private CustomerInterventionService customerInter;
	
	Logger log = Logger.getLogger(getClass());
	
	//当前存在异常时跳转路径
	private final String FAIL = "redirect:/home/index.html";
	
	/**
	 * 
	 * 描述:服务商修改后重新提交
	 * 作者:cfz
	 * @Date	 2017年6月22日
	 */
	@RequestMapping(value="showFeedBack")
	public String showFeedBack(HttpServletRequest request,Model model){
		
		//创建临时变量
		
		FeedBack feedback = new FeedBack();
		createParameterMap(request);
		//标示记 有或者是没有  默认没有：false
		boolean flage = false;
		//获取当前的订单编号
		if(paramsMap.containsKey("orderId")){
			//查询当前orderId中的feed表
			if(StringUtils.isNotBlank(paramsMap.get("orderId").toString())){
				//put search condition
				paramsMap.put("isDelete",0);
				List<FeedBack> list = feedBackService.searchFeeaBack(paramsMap);
				
				if(!(list.isEmpty())&&list.size()>0){
					flage = true;
					feedback = list.get(0);
				}
				model.addAttribute("feedBack",feedback);
				model.addAttribute("flage", flage);
				model.addAttribute("orderId", paramsMap.get("orderId"));
				return "wap/xkh_version_2_2/feedback/submit_feedback";
			}
			
		}
		
		return "redirect:/home/index.html";
	}
	
	
	/** 
	 * insert feedback By orderId
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="insertFeedBack")
	public String insertFeedBack(HttpServletRequest request,Model model){
		
		//insert parameter to  feedbackTable 
		createParameterMap(request);
		//校验当前传入参数的key值与value值是否合法
		if(!validate(paramsMap)){
			
			return "redirect:/home/index.html";
		}
		boolean successOrFail = feedBackService.insertFeedBack(paramsMap);
		
		if(successOrFail){
			//change order status to 3
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("orderId",paramsMap.get("orderId"));
			map.put("orderStatus", 3);
			if(orderSeaService.updateOrderState(map)){
				//send message to buyer 
				//sendMessage.skillSuccess(map);
				Map<String,Object> sendMap = setSendInfo(paramsMap,1);//推送模板消息
				sendMessage.submitFeedbackInfo(sendMap,true);
				//跳转到订单详情页面
				return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");
			}
			log.debug("-------change orderStatus filed orderIs-------"+map.get("orderId"));
			
		}
		return "redirect:/home/index.html";
	}
	/**
	 * 
	 * 描述:设置发送信息中的内容
	 * 作者:gyp
	 * @Date	 2017年6月22日
	 */
	public Map<String,Object> setSendInfo(Map<String,Object> map,int status){
		//获取服务名称
		List<OrderDetail> details = orderSeaService.searchOrderDetail(map);
		String skillName = null;
		for(int i=0;i<details.size();i++){
			if(details.get(i).getTypeName().equals("skillName")){
				skillName = details.get(i).getTypeName();
			}
		}
		Map<String,Object> mapInfo = new HashMap<String, Object>();
		if(status == 1){//服务上反馈信息模板推送
			mapInfo.put("first", "您好，您的服务“"+skillName+"”已执行完成。");
			mapInfo.put("keyword1", "服务完成提醒");
			mapInfo.put("detail", "请点击查看，满意请及时确认哦！");
		}else if(status == 2){//买家驳回修改
			mapInfo.put("first", "您好，买家请求您重新完善“"+skillName+"”的服务反馈。");
			mapInfo.put("keyword1", "重新修改");
			mapInfo.put("detail", "请点击查看。");
		}else if(status == 3){
			mapInfo.put("first", "您好，服务商拒绝了您的对“"+skillName+"”的驳回修改请求。");
			mapInfo.put("keyword1", "服务完成提醒");
			mapInfo.put("detail", "请点击查看。");
		}else if(status == 4){
			mapInfo.put("first", "您好，服务商已重新提交了“"+skillName+"”的服务反馈。");
			mapInfo.put("keyword1", "服务完成提醒");
			mapInfo.put("detail", "请点击查看。");
		}
		
		
		mapInfo.put("orderId", map.get("orderId"));
		return mapInfo;
	}
	/**
	 * update FeedBack By orderId
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="updateFeedBack")
	public String updateFeedBack(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		//校验当前传入参数的key值与value值是否合法
		if(!validate(paramsMap)){
			log.debug("-------当前存在不合法参数 跳转到首页----------");
			
			return "redirect:/home/index.html";
		}
		//修改之后标示当前为未读
		paramsMap.put("feedbackstatus",1);
		//还原当前拒绝修改状态
		paramsMap.put("sellerRefuse",0);
		//还原当前驳回状态
		paramsMap.put("buyerReject",0);
		
		boolean flage = feedBackService.updateFeedBack(paramsMap);
		
		//修改成功跳转到修改成功页面
		if(flage){
			//发送模板消息
			Map<String,Object> sendMap = setSendInfo(paramsMap,4);//推送模板消息
			sendMessage.submitFeedbackInfo(sendMap,true);
			
			
			return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");	
		}
		
		return "redirect:/home/index.html";
	}
	
	/**
	 * 校验当前传入的参数是否合法
	 * @return
	 */
	public boolean validate(Map<String,Object> map){
		
		if(map.containsKey("orderId")){
			if(StringUtils.isNotBlank(map.get("orderId").toString())){
				if(map.containsKey("title")&&map.containsKey("feedbackurl")&&map.containsKey("describe")){
					if(StringUtils.isNotBlank(map.get("title").toString())&&
							StringUtils.isNotBlank(map.get("feedbackurl").toString())&&
								StringUtils.isNotBlank(map.get("describe").toString())){
						
						return true;
					}
					log.debug("-------------当前必填字段为空 （title，feedbackurl，describe）----------------");
				}
				log.debug("-------------未包含当前必传的列 （title，feedbackurl，describe）----------------");
			}
			log.debug("-----------当前必传参数orderId为空---------------");
		}
		return false;
	}
	
	
	/**
	 * 买家端跳转到反馈页面
	 * 买家查看当前反馈
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="showFeedBackBuyer")
	public String showFeedBackBuyer(HttpServletRequest request,Model model){
		
		//创建临时变量
		FeedBack feedback = new FeedBack();
		createParameterMap(request);
		//标示记 有或者是没有  默认没有：false
		boolean flage = false;
		//获取当前的订单编号
		if(paramsMap.containsKey("orderId")){
			//查询当前orderId中的feed表
			if(StringUtils.isNotBlank(paramsMap.get("orderId").toString())){
				//put search condition
				paramsMap.put("isDelete",0);
				List<FeedBack> list = feedBackService.searchFeeaBack(paramsMap);
				//更新当前查看状态为已阅
				paramsMap.put("feedbackstatus",0);
				
				if(feedBackService.updateFeedBack(paramsMap)){
					log.debug("--------反馈记录更新已阅成功----------"+paramsMap.get("orderId"));
				}
				if(!(list.isEmpty())&&list.size()>0){
					flage = true;
					feedback = list.get(0);
				}
				model.addAttribute("feedBack",feedback);
				model.addAttribute("flage", flage);
				model.addAttribute("orderId", paramsMap.get("orderId"));
				return "wap/xkh_version_2_2/feedback/look_feedback";
			}
			log.debug("----------transmit pamater orderId is blank-----------");
		}
		log.debug("---------transmit pamater orderId is null-------------");
		
		return "redirect:/home/index.html";
	}
	
	/**
	 * 根据当前获取的OrderId进行驳回修改
	 * 展示当前驳回页面，连带参数orderId
	 */
	@RequestMapping(value="showReject")
	public String  showReject(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		boolean flage = false;
		
		FeedBack feedback = new FeedBack();
		//根据当期的orderId值查询出当前feedback中的字段
		List<FeedBack> list = feedBackService.searchFeeaBack(paramsMap);
		
		if(!(list.isEmpty())&&list.size()>0){
			flage = true;
			feedback = list.get(0);
			
			model.addAttribute("feedback",feedback);
			model.addAttribute("orderId",paramsMap.get("orderId"));
			//跳转到修改页面
			return "wap/xkh_version_2_2/feedback/write_change_buyer";
			
		}
		
		return "redirect:/home/index.html";
		
	}
	
	
	/**
	 * 买家驳回修改
	 */
	@RequestMapping(value="rejectAndAlter")
	public String rejectAndAlter(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		//校验当前传入参数是否合法
		if(paramsMap.containsKey("orderId")&&paramsMap.containsKey("suggestion")){
			
			if(StringUtils.isNotBlank(paramsMap.get("orderId").toString())&&
				StringUtils.isNotBlank(paramsMap.get("suggestion").toString())){
				
				//更新当期驳回状态
				paramsMap.put("buyerReject",1);
				boolean flage = feedBackService.updateFeedBack(paramsMap);
				
				if(flage){
					//调用消息模板链接到订单详情页面
					sendMessage.skillSuccess(paramsMap);
					//跳转回订单详情页面
					return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");	
					
				}
				log.debug("----------修改订单信息失败-------------");
				
			}
			
		}
		return "redirect:/home/index.html";
	}
	
	/**
	 * 获取当前的orderId进行表中的suggestion
	 * 的添加  必要参数  
	 * @return
	 * 备注：买家驳回修改 添加修改建议
	 */
	@RequestMapping(value="addSuggestion")
	public String addSuggestion(HttpServletRequest request,Model model){
		
		//获取当前参数
		createParameterMap(request);
		//检查当前数据是否合法
		if(paramsMap.containsKey("orderId")&&paramsMap.containsKey("suggestion")){
			
			if(StringUtils.isNotBlank(paramsMap.get("orderId").toString())&&
					StringUtils.isNotBlank(paramsMap.get("suggestion").toString())){
				//执行更新当前表的操作 标示为买家拒绝
				paramsMap.put("buyerReject",1);
				boolean flage = feedBackService.updateFeedBack(paramsMap);
				if(flage){
					//向服务商发送模板信息
					Map<String,Object> sendMap = setSendInfo(paramsMap,2);
					sendMessage.submitFeedbackInfo(sendMap,false);
					
					return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");	
				}
				log.debug("-------更新feedback表失败  当前订单编号：---------"+paramsMap.get("orderId"));
				log.debug("--------feedback更新字段 suggestion 更新内容 ------------"+paramsMap.get("suggestion").toString());
			}
		}
		return "redirect:/home/index.html";
	}
	
	/**
	 * 卖家操作拒绝修改，跳转到展示页面
	 * 方法必传参数 orderId
	 */
	@RequestMapping(value="sellerRefuseShow")
	public String sellerRefuseShow(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		if(paramsMap.containsKey("orderId")&&StringUtils.isNotBlank(paramsMap.get("orderId").toString())){
			
			//查询当前反馈记录
			
			List<FeedBack> list = feedBackService.searchFeeaBack(paramsMap);
			
			if(list.isEmpty()||list.size()<=0){
				log.debug("---------当前订单未查询到反馈记录  orderId：-------------"+paramsMap.get("orderId"));
				return FAIL;
			}
			//放入反馈记录
			model.addAttribute("feedBean",list.get(0));
			model.addAttribute("orderId",paramsMap.get("orderId"));
			
			return "wap/xkh_version_2_2/feedback/write_change_seller";
		}
		log.debug("-----------当前的必传字段为空 orderId---------------");
		//获取当前
		return "redirect:/home/index.html";
	}
	
	/**
	 * 卖家拒绝修改用户的反馈
	 * 必传字段 orderId,reason:拒绝理由
	 */
	
	@RequestMapping("addReason")
	public String addReason(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		
		//判断当前参数是否合法
		if(paramsMap.containsKey("orderId")&&paramsMap.containsKey("reseon")){
			if(paramsMap.get("orderId")!=null&&paramsMap.get("reseon")!=null){
				if(StringUtils.isNotBlank(paramsMap.get("orderId").toString())&&
					StringUtils.isNotBlank(paramsMap.get("reseon").toString())){
				  	//更新feedback 更新 reseon 
					paramsMap.put("sellerRefuse",1);
					
					boolean flage = feedBackService.updateFeedBack(paramsMap);
					if(flage){
						//向服务商发送模板信息
						Map<String,Object> sendMap = setSendInfo(paramsMap,3);
						sendMessage.submitFeedbackInfo(sendMap,true);
						
						
						return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");	
					}
				}
			}
		}
		log.error("--------当前存在不合法参数  print：----------"+paramsMap.toString());
		return "redirect:/home/index.html";
	}

	
	/**
	 * 确认收货跳转页面
	 * 传入参数 orderId
	 */
	@RequestMapping(value="orderConfirmShow")
	public String orderConfirmShow(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		
		boolean flage = false;//默认以前有评价过 为false
		
		if(paramsMap.containsKey("orderId")&&paramsMap.get("orderId")!=null){
			
			//search table service_evaluate by orderId
			
			List<ServiceEvaluate>  list = serviceEval.searchServiceEvaluate(paramsMap);
			
			if(list.isEmpty()||list.size()<0){
				
				flage = true;
			}
			
			model.addAttribute("flage",flage);
			model.addAttribute("orderId", paramsMap.get("orderId").toString());
			
			return "wap/xkh_version_2_2/feedback/confirm_receipt";
		}
		
		return FAIL;
	}
	
	/**
	 * 确认收货页面
	 * 传入参数为订单Id
	 */
	@RequestMapping(value="orderConfirm",params={"efficiencyGrade","qualityGrade","attitudeGrade"})
	@Transactional
	public String orderConfirm(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		
		//获取当前页面中的
		if(paramsMap.containsKey("orderId")&&null!=paramsMap.get("orderId")){
			
			//插入表中的评价内容
			if(serviceEval.insertServiceEvaluate(paramsMap)){
				
				//更改订单状态
				Map<String, Object> map = new HashMap<String, Object>();
				Date date = new Date();
				map.put("confirmTime", date);//存入确认收货时间
				map.put("orderStatus",4);//订单状态为已完成
				if(orderSea.updateOrderSellCommon(request,map)){
					//跳转回当前的订单详情页面
					return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");
				}
				log.debug("----------更新订单状态失败 orderId:--------------"+paramsMap.get("orderId"));
			}
			//跳转回订单详情页面
			
		}
		
		return FAIL;
	}
	
	
	
	/**
	 * 关于跳转客服介入页面
	 */
	@RequestMapping(value="jumpCustInter")
	public String jumpCustInter(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		boolean flage = false;
		CustomerIntervention custInter = new CustomerIntervention();
		if(paramsMap.get("feedbackId")!=null&&paramsMap.get("orderId")!=null){
			
			//表示查询反馈
			paramsMap.put("interveneType", 0);
			//查询当前orderId值是否存在客服介入记录
			List<CustomerIntervention>  list = customerInter.searchCustInter(paramsMap);
			
			if(list.isEmpty()){
				flage = true;
			}else{
				custInter = list.get(0);
			}
			model.addAttribute("custInter",custInter );
			model.addAttribute("flage",flage);
			model.addAttribute("orderId",paramsMap.get("orderId"));
			model.addAttribute("feedbackId",paramsMap.get("feedbackId"));
			
			return "wap/xkh_version_2_2/feedback/customer_service";
		}
		
		return FAIL;
	}
	
	/**
	 * 将值加入到客服介入表中
	 * @param reuqest
	 * @param model
	 * @return
	 */
	@RequestMapping(value="insertToCustInter")
	public String insertToCustInter(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		if(paramsMap.containsKey("Reason")){
			if(paramsMap.get("orderId")!=null&&paramsMap.get("feedbackId")!=null
					&&StringUtils.isNotBlank(paramsMap.get("feedbackId").toString())
					&&StringUtils.isNotBlank(paramsMap.get("Reason").toString())){
				 paramsMap.put("interveneType", 0);
				 if(customerInter.insertCustIntervent(paramsMap)){
					 
					 log.debug("-------客服反馈成功 --------"+paramsMap.toString());
					 return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");
				 }
				 log.debug("-------客服反馈失败 --------"+paramsMap.toString());
			}
		}
		
		return FAIL;
	}
	
	
	/**
	 * 更新当前 customer_intervention 中的内容
	 * 根据给定的feedbackId 和给定的 orderId值进行更新
	 * @return
	 */
	@RequestMapping(value="updateToCustInter",method=RequestMethod.POST,params={"feedbackId","orderId","Reason","id"})
	public String updateToCustInter(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		
		if(customerInter.updateCustInter(paramsMap)){
			
			log.debug("--------更新当前 customer_intervention 表中内容------------");
			return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");
		}
		log.debug("--------------当前缺少必传参数-------------------");
		return FAIL;
	}
	
	//关于调用公共的服务反馈的方法适用于订单详情页面
	public  Model feedBackCommon(Map<String,Object> searchCondition,Model model){
		
		boolean haveFeed = true;
		
		List<FeedBack> feed = feedBackService.searchFeeaBack(searchCondition);
		FeedBack  feedback = new FeedBack();
		 //提交反馈  false 表示没有 true 表示有
		if(feed.isEmpty()||feed.size()==0){
			haveFeed = false;
		}else{
			feedback = feed.get(0);
		}
	    model.addAttribute("feedBackBean", feedback);
	    model.addAttribute("feedBack",haveFeed);//默认选择为
		
		return model;
	}
	
	
	/**
	 * 页面跳转 到客服介入退款
	 * @return
	 */
	@RequestMapping(value="jumpCustInterRefund")
	public String jumpCustInterRefund(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		boolean flage = false;
		CustomerIntervention custInter = new CustomerIntervention();
		if(paramsMap.get("feedbackId")!=null&&paramsMap.get("orderId")!=null){
			
			//表示查询退款
			paramsMap.put("interveneType", 1);
			//查询当前orderId值是否存在客服介入记录
			List<CustomerIntervention>  list = customerInter.searchCustInter(paramsMap);
			
			if(list.isEmpty()){
				flage = true;
			}else{
				custInter = list.get(0);
			}
			model.addAttribute("custInter",custInter );
			model.addAttribute("flage",flage);
			model.addAttribute("orderId",paramsMap.get("orderId"));
			model.addAttribute("feedbackId",paramsMap.get("feedbackId"));
			
			return "wap/xkh_version_2_2/feedback/customer_service_refund";
		}
		
		return FAIL;
	}
	
	/**
	 * 将值加入到客服介入表中 退款反馈
	 * @param reuqest
	 * @param model
	 * @return
	 */
	@RequestMapping(value="insertToCustInterRefund")
	public String insertToCustInterRefund(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		if(paramsMap.containsKey("Reason")){
			if(paramsMap.get("orderId")!=null&&paramsMap.get("feedbackId")!=null
					&&StringUtils.isNotBlank(paramsMap.get("feedbackId").toString())
					&&StringUtils.isNotBlank(paramsMap.get("Reason").toString())){
				
				 paramsMap.put("interveneType", 1);
				 if(customerInter.insertCustIntervent(paramsMap)){
					 
					 log.debug("-------客服反馈成功 --------"+paramsMap.toString());
					 return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");
				 }
				 log.debug("-------客服反馈失败 --------"+paramsMap.toString());
			}
		}
		
		return FAIL;
	}
	
	
	/**
	 * 更新当前 customer_intervention 中的内容
	 * 根据给定的feedbackId 和给定的 orderId值进行更新
	 * @return
	 */
	@RequestMapping(value="updateToCustInterRefund",method=RequestMethod.POST,params={"feedbackId","orderId","Reason","id"})
	public String updateToCustInterRefund(HttpServletRequest request,Model model){
		
		createParameterMap(request);
		
		if(customerInter.updateCustInter(paramsMap)){
			
			log.debug("--------更新当前 customer_intervention 表中内容------------");
			return "redirect:/chatMesseage/queryOrderDetail.html?orderId="+paramsMap.get("orderId");
		}
		log.debug("--------------当前缺少必传参数-------------------");
		return FAIL;
	}
	
	
	
	
	
	
}
