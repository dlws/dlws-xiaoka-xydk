/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.wap.xkh2_2.ordseach.pojo;

import java.util.Map;

import com.jzwl.system.base.pojo.BasePojo;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;


/**
 * 订单的大对象
 * @author Administrator
 *
 */
public class OrderSea extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "Order";
	public static final String ALIAS_ORDER_ID = "订单的id";
	public static final String ALIAS_CREATE_DATE = "下单时间";
	public static final String ALIAS_PAY_MONEY = "付款金额";
	public static final String ALIAS_PAY_TIME = "付款时间";
	public static final String ALIAS_REALITY_MONEY = "实际付款金额";
	public static final String ALIAS_ORDER_STATUS = "1：下单成功，2：待付款、 3：30分钟未付款取消，4：支付成功（也就是待服务）、 5：待确认（已服务）， 6：已确认， 7：超时失效， 8：申请退、9：退款成功";
	public static final String ALIAS_PAYWAY = "0:微信支付、1：其他";
	public static final String ALIAS_CANCEL_TIME = "订单取消时间";
	public static final String ALIAS_INVALID_TIME = "订单超时失效时间";
	public static final String ALIAS_CONFIRM_TIME = "确认时间";
	public static final String ALIAS_REFUND_TIME = "退款时间";
	public static final String ALIAS_LINKMAN = "联系人";
	public static final String ALIAS_PHONE = "联系电话";
	public static final String ALIAS_MESSAGE = "买家留言信息";
	public static final String ALIAS_START_DATE = "开始时间";
	public static final String ALIAS_END_DATE = "结束时间";
	public static final String ALIAS_IS_DELETE = "是否删除";
	public static final String ALIAS_ENTER_TYPE = "订单的服务类型";
	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	public static final String FORMAT_PAY_TIME = DATE_FORMAT;
	public static final String FORMAT_CANCEL_TIME = DATE_FORMAT;
	public static final String FORMAT_INVALID_TIME = DATE_FORMAT;
	public static final String FORMAT_CONFIRM_TIME = DATE_FORMAT;
	public static final String FORMAT_REFUND_TIME = DATE_FORMAT;
	public static final String FORMAT_START_DATE = DATE_FORMAT;
	public static final String FORMAT_END_DATE = DATE_FORMAT;
	

	//columns START
	/**
	 * 订单的信息
	 */
	private Order order;
	
	/**
     * 差异时间
     */
	private long timmer;
	/**
	 * 卖家的用户信息
	 * @return
	 */
	private UserBasicinfo userBasicinfo;

	/**
	 * 订单的私有化信息
	 * @return
	 */
	private Map<String,String> map;
	public long getTimmer() {
		return timmer;
	}

	public void setTimmer(long timmer) {
		this.timmer = timmer;
	}

	/**
	 * 关于订单的技能的信息
	 * @return
	 */
	private Map<String,Object> userSkillMap;
	/**
	 * 关于用户技能需要展示的图片
	 * @return
	 */
	private String imageUrl;
	
	/**
	 * 单价
	 * @return
	 */
	private String singPrice;
	//columns END
	/**
	 * 销售量
	 * @return
	 */
	private String sellNo;
	
	
	
	
	public String getSellNo() {
		return sellNo;
	}

	public void setSellNo(String sellNo) {
		this.sellNo = sellNo;
	}

	public String getSingPrice() {
		return singPrice;
	}

	public void setSingPrice(String singPrice) {
		this.singPrice = singPrice;
	}

	//columns END
	
	

	public Map<String, Object> getUserSkillMap() {
		return userSkillMap;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public void setUserSkillMap(Map<String, Object> userSkillMap) {
		this.userSkillMap = userSkillMap;
	}

	public long getTimeDiffer() {
		return timmer;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	public void setTimeDiffer(long timeDiffer) {
		this.timmer = timmer;
	}


	public UserBasicinfo getUserBasicinfo() {
		return userBasicinfo;
	}

	public void setUserBasicinfo(UserBasicinfo userBasicinfo) {
		this.userBasicinfo = userBasicinfo;
	}
	
    public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}

