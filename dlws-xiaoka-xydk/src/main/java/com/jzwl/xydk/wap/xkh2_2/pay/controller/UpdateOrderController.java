package com.jzwl.xydk.wap.xkh2_2.pay.controller;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.Constants;
import com.jzwl.common.sign.AccessCore;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.pay.service.UpdateOrderService;
import com.jzwl.xydk.wap.xkh2_3.invoice.service.InvoiceService;


@Controller
@RequestMapping(value="/updatePay")
public class UpdateOrderController extends BaseWeixinController{
	
	@Autowired
	private UpdateOrderService updateOrderService;
	@Autowired
	private Constants contants; 
	@Autowired
	private SendMessageService sendMessage; //调用模板消息使用
	@Autowired
	private ChatMessageService chatMessageService;
	@Autowired
	private InvoiceService invoiceService;
	
	private static Logger log = LoggerFactory
			.getLogger(UpdateOrderController.class);
	
	
	
	/**
	 * moblie:移动端
	 *	同步更新
	 * 支付成功后主动发送过来的携带：OrderId ,out_trade_no.用于更新订单状态
	 */
	@RequestMapping(value = "/paySuccess")
	public @ResponseBody void paySuccess(HttpServletRequest request, HttpServletResponse response,Model model){
		createParameterMap(request);
		//判断签名
		boolean verify = AccessCore.verify(paramsMap,contants.getWx_signKey());
		log.info("++++++++++++++++++++++++++++++++++++++判断签名信息，是否被更改="+verify);
		log.info("++++++++++++++++++++++++++++++++++++++同步更新订单状态");
		String outTradeNo = request.getParameter("out_trade_no");
		String orderId = request.getParameter("orderId");
		
		log.info("++++++++++++++++++++++++++++++++++++++++++同步跟新订单状态参数outTradeNo="+outTradeNo+"----orderId="+orderId);
		//具体更新订单状态
		try {
			//根据订单ID更新订单状态，以及out_trade_no
			//签名校验、成功更新订单
			if(verify){
				Map<String,Object> map = updateOrderService.getOrderInfo(orderId);
				//聊天内容发送系统信息
				String openId = map.get("openId").toString();//自己的OpenId
				paramsMap.put("openId", openId);
				String nowOpenId = map.get("sellerOpenId").toString();//卖家的OpenId
				paramsMap.put("nowOpenId", nowOpenId);
				paramsMap.put("orderStatus", "2");
				boolean b = chatMessageService.insertUpdateSysMeg(paramsMap);
				chatMessageService.insertBuyMeg(paramsMap);//买家端
				
				if(b){
					boolean flag = updateOrderService.updateOrder(orderId,outTradeNo);
					if(flag){//发送支付成功的文本信息
						sendMessage.payMoneySuccess(orderId, map.get("openId").toString(), map.get("payMoney").toString());
						//judge the order is or not jztf
						if(!("jztf".equals(map.get("enterType").toString()))){
							sendMessage.remindOrderToSellerOrderSendMessage(orderId);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * moblie:移动端
	 *	同步更新
	 * 支付成功后主动发送过来的携带：OrderId ,out_trade_no.用于更新订单状态
	 */
	@RequestMapping(value = "/payNewSuccess")
	public @ResponseBody void payNewSuccess(HttpServletRequest request, HttpServletResponse response,Model model){
		createParameterMap(request);
		//判断签名
		boolean verify = AccessCore.verify(paramsMap,contants.getWx_signKey());
		log.info("++++++++++++++++++++++++++++++++++++++判断签名信息，是否被更改="+verify);
		log.info("++++++++++++++++++++++++++++++++++++++同步更新订单状态");
		String outTradeNo = request.getParameter("out_trade_no");
		String pid = request.getParameter("orderId");
		log.info("++++++++++++++++++++++++++++++++++++++++++同步跟新订单状态参数outTradeNo="+outTradeNo+"----orderId="+pid);
		//具体更新订单状态
		try {
			//根据订单ID更新订单状态，以及out_trade_no
			//签名校验、成功更新订单
			if(verify){
				updateOrderService.updateOrderByPid(pid,outTradeNo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * moblie:移动端
	 *	同步更新
	 * 支付成功后主动发送过来的携带：OrderId ,out_trade_no.用于更新订单状态 add by sunxu 2017/05/23  发票
	 */
	@RequestMapping(value = "/payNewInvoiceSuccess")
	public @ResponseBody void payNewInvoiceSuccess(HttpServletRequest request, HttpServletResponse response,Model model){
		createParameterMap(request);
		//判断签名
		boolean verify = AccessCore.verify(paramsMap,contants.getWx_signKey());
		log.info("++++++++++++++++++++++++++++++++++++++判断签名信息，是否被更改="+verify);
		log.info("++++++++++++++++++++++++++++++++++++++同步更新订单状态");
		String outTradeNo = request.getParameter("out_trade_no");
		String orderIds = request.getParameter("orderIds");//订单id(123,456,789)
		String id = request.getParameter("id");//发票id
		log.info("++++++++++++++++++++++++++++++++++++++++++同步跟新订单状态参数outTradeNo="+outTradeNo+"----orderId="+orderIds);
		//具体更新订单状态
		try {
			//根据订单ID更新订单状态，以及out_trade_no
			//签名校验、成功更新订单
			if(verify){
				//根据发票id更改发票状态
				invoiceService.updateInvoiceById(id, outTradeNo);
				List<Map<String,Object>> mapList = updateOrderService.getOrderInfoByOrderIds(orderIds);
				if(mapList != null && mapList.size() > 0){
					for(int i = 0; i< mapList.size();i++){
						Map<String, Object> map = mapList.get(i);
						updateOrderService.updateOrder(map.get("orderId").toString(),outTradeNo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：支付完成用户点击完成或返回时跳转的页面。
	 * 	携带orderId请求过来，用于显示支付成功页面所需要的参数
	 * 创建人： ln
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value = "/payOldSuccess")
	public String payOldSuccess(HttpServletRequest request, HttpServletResponse response,Model model){
		String orderId = request.getParameter("orderId");
		log.info("-----------------XKH进入跳转,点击完成或者返回");
		return  "redirect:/ordersea/searchOrderSell.html";
		/*mov.setViewName("redirect:/groupPurchase/groupPurchaseList.html");*/
	}
	
	
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：遗漏订单状态未修改，在支付系统定时回调进行更新对应订单状态,五分钟执行一次
	 * 异步更新
	 * 创建人： ln
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	@ResponseBody
	@RequestMapping(value = "/updateOrder")
	public String updateTgOrder(HttpServletRequest request,HttpServletResponse response) {
		Map<String,Object> map = new HashMap<String,Object>();
		String para = request.getParameter("outTradeNoOpenId");
		String[] par = para.split("\\|");
		Boolean flag = false;
		if(par.length>0){
			String orderId = par[0];
			String out_trade_no = par[1];
			String openId = par[2];
			log.info("++++++++++++++++++++++++++++++++++++++++++++++对应子系统接受请求进行订单状态的处理+参数= para="+para+"+out_trade_no ="+out_trade_no+"+openId="+openId);
			Map<String,Object> orderMap = updateOrderService.getOrderInfo(orderId);//获取订单
			if(null!=orderMap && null!=orderMap.get("orderId")){//判断根据传递的参数是否拿到订单
				
				 if(null!=orderMap.get("orderStatus")){//判断订单的状态
					 
					 Integer status = Integer.valueOf(orderMap.get("orderStatus").toString());
					 if(1==status){
						 
					 }else{
						flag = true;
					 }
				 }
			}
		}
		log.info("+++++++++++++++++++++++++++++++++++++flag="+flag);
		map.put("flag", flag);
		String json = com.alibaba.fastjson.JSONObject.toJSONString(map);
		return json;
	}
	
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：支付测试（其他支付根据此方法传入相应参数）
	 * 创建人： ln
	 * 创建时间： 2017年4月20日
	 * 标记：标示类参数不可修改
	 * @param request
	 * @param response
	 * @version
	 */
	@RequestMapping(value = "/goPay")
	public void goPay(HttpServletRequest request, HttpServletResponse response) {//
		createParameterMap(request);
		Map<String,Object> signMap = new HashMap<String,Object>();
		try {
			String openId = "o3P2SwabLBk80n1IdUBzkI3tnFN4";
			String commodityName = "测试商品ln";
			String totalPrice = "0.01";
			String orderId = "123456";
			String clientFlag = "xkh";
			String sendUrl = "http://xydk.xiaoka360.com/updatePay/payOldSuccess.html";
			String keyUrlOrderId = "wxPay_xkh|http://xydk.xiaoka360.com/updatePay/paySuccess.html|123456";
			String systemFlag = "xkh";
			signMap.put("openId",openId);
			signMap.put("commodityName",commodityName);
			signMap.put("totalPrice",totalPrice);
			signMap.put("orderId",orderId);
			signMap.put("clientFlag",clientFlag);
			signMap.put("sendUrl",sendUrl);
			signMap.put("keyUrlOrderId",keyUrlOrderId);
			commodityName = URLEncoder.encode(commodityName);
			String sign = AccessCore.sign(signMap, contants.getWx_signKey());//拿到签名数据
			log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++发起时签名信息="+sign);
			log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++signMap="+signMap.toString());
			String url = "http://klgj.xiaoka360.com/wm-plat/api/jumpPay.html" + "?openId="+openId+"&clientFlag="+clientFlag+"&sign="+sign
					+ "&sendUrl="+sendUrl+"&keyUrlOrderId="+keyUrlOrderId+"&commodityName="+commodityName+"&totalPrice="+totalPrice+"&orderId="+orderId+"&systemFlag="+systemFlag;
			log.info("+++++++++++++++++++++++++++++++++发送支付请求+++++++++"+url);
			response.sendRedirect(url);
				log.info("++++++++++++++++++++++++++++++++++++++++++++去支付参数问题signMap="+signMap.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	public static void main(String[] args) {
		/*Map<String,Object> map = new HashMap<String,Object>();
		map.put("orderId","1491966731403002");
		map.put("out_trade_no","12341234");
		String sign = AccessCore.sign(map, "CGgOeVe7WoyVXGbGvzHeiplAfRpQNAqW");*/
		/*String a = "123456";
		String password = AuthUtils.getPassword(a);
		System.out.println(password);*/
	}
	
	
	
	
	

}
