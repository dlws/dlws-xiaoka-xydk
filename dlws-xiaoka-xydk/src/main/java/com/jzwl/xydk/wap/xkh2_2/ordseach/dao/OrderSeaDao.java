package com.jzwl.xydk.wap.xkh2_2.ordseach.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.Order;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderSea;
import com.jzwl.xydk.wap.xkh2_3.publish.service.PublishService;

/**
 * 订单管理使用查询当前用户的订单
 * 
 * @author Administrator
 *
 */
@Repository("orderSeaDao")
public class OrderSeaDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	@Autowired
	private PublishService publishService;

	/**
	 * 根据OpenId等参数查询当前Order的基本信息
	 * 注意查询时isDelete 参数为0 (未删除)
	 * @param pamater
	 * @return
	 */
	public List<OrderSea> toSearchOrder(Map<String, Object> map) {

		// [column]为字符串拼接, {column}为使用占位符. 如username='[username]',偷懒时可以使用字符串拼接 
		// [column] 为PageRequest的属性

		String sql = "select " + getColumns() + " from `xiaoka-xydk`.order t where 1=1 ";

		if (null != map.get("openId") && StringUtils.isNotEmpty(map.get("openId").toString())) {
			sql = sql + "and t.openId = '" + map.get("openId") + "'";
		}
		if (map.containsKey("orderId") && StringUtils.isNotEmpty(map.get("orderId").toString())) {
			sql = sql + "and t.orderId = '" + map.get("orderId") + "'";
		}
		if (map.containsKey("sellerOpenId") && StringUtils.isNotEmpty(map.get("sellerOpenId").toString())) {
			sql = sql + "and t.sellerOpenId = '" + map.get("sellerOpenId") + "'";
		}
		if (null != map.get("createDateBegin") && StringUtils.isNotEmpty(map.get("createDateBegin").toString())) {
			sql = sql + " and t.createDate >= '" + map.get("createDateBegin") + "'";
		}
		if (null != map.get("createDateEnd") && StringUtils.isNotEmpty(map.get("createDateEnd").toString())) {
			sql = sql + " and t.createDate <= '" + map.get("createDateEnd") + "'";
		}

		if (null != map.get("payMoney") && StringUtils.isNotEmpty(map.get("payMoney").toString())) {
			sql = sql + " and t.payMoney  = " + map.get("payMoney") + "";
		}

		if (null != map.get("payTimeBegin") && StringUtils.isNotEmpty(map.get("payTimeBegin").toString())) {
			sql = sql + " and t.payTime >= '" + map.get("payTimeBegin") + "'";
		}
		if (null != map.get("payTimeEnd") && StringUtils.isNotEmpty(map.get("payTimeEnd").toString())) {
			sql = sql + " and t.payTime <= '" + map.get("payTimeEnd") + "'";
		}

		if (null != map.get("realityMoney") && StringUtils.isNotEmpty(map.get("realityMoney").toString())) {
			sql = sql + " and t.realityMoney  = " + map.get("realityMoney") + "";
		}

		if (null != map.get("orderStatus") && StringUtils.isNotEmpty(map.get("orderStatus").toString())) {
			sql = sql + " and t.orderStatus  = " + map.get("orderStatus") + "";
		}

		if (null != map.get("payway") && StringUtils.isNotEmpty(map.get("payway").toString())) {
			sql = sql + " and t.payway  = " + map.get("payway") + "";
		}

		if (null != map.get("cancelTimeBegin") && StringUtils.isNotEmpty(map.get("cancelTimeBegin").toString())) {
			sql = sql + " and t.cancelTime >= '" + map.get("cancelTimeBegin") + "'";
		}
		if (null != map.get("cancelTimeEnd") && StringUtils.isNotEmpty(map.get("cancelTimeEnd").toString())) {
			sql = sql + " and t.cancelTime <= '" + map.get("cancelTimeEnd") + "'";
		}

		if (null != map.get("invalidTimeBegin") && StringUtils.isNotEmpty(map.get("invalidTimeBegin").toString())) {
			sql = sql + " and t.invalidTime >= '" + map.get("invalidTimeBegin") + "'";
		}
		if (null != map.get("invalidTimeEnd") && StringUtils.isNotEmpty(map.get("invalidTimeEnd").toString())) {
			sql = sql + " and t.invalidTime <= '" + map.get("invalidTimeEnd") + "'";
		}

		if (null != map.get("confirmTimeBegin") && StringUtils.isNotEmpty(map.get("confirmTimeBegin").toString())) {
			sql = sql + " and t.confirmTime >= '" + map.get("confirmTimeBegin") + "'";
		}
		if (null != map.get("confirmTimeEnd") && StringUtils.isNotEmpty(map.get("confirmTimeEnd").toString())) {
			sql = sql + " and t.confirmTime <= '" + map.get("confirmTimeEnd") + "'";
		}

		if (null != map.get("refundTimeBegin") && StringUtils.isNotEmpty(map.get("refundTimeBegin").toString())) {
			sql = sql + " and t.refundTime >= '" + map.get("refundTimeBegin") + "'";
		}
		if (null != map.get("refundTimeEnd") && StringUtils.isNotEmpty(map.get("refundTimeEnd").toString())) {
			sql = sql + " and t.refundTime <= '" + map.get("refundTimeEnd") + "'";
		}

		if (null != map.get("linkman") && StringUtils.isNotEmpty(map.get("linkman").toString())) {
			sql = sql + " and t.linkman  = " + map.get("linkman") + "";
		}

		if (null != map.get("phone") && StringUtils.isNotEmpty(map.get("phone").toString())) {
			sql = sql + " and t.phone  = " + map.get("phone") + "";
		}

		if (null != map.get("message") && StringUtils.isNotEmpty(map.get("message").toString())) {
			sql = sql + " and t.message  = " + map.get("message") + "";
		}

		if (null != map.get("startDateBegin") && StringUtils.isNotEmpty(map.get("startDateBegin").toString())) {
			sql = sql + " and t.startDate >= '" + map.get("startDateBegin") + "'";
		}
		if (null != map.get("startDateEnd") && StringUtils.isNotEmpty(map.get("startDateEnd").toString())) {
			sql = sql + " and t.startDate <= '" + map.get("startDateEnd") + "'";
		}

		if (null != map.get("endDateBegin") && StringUtils.isNotEmpty(map.get("endDateBegin").toString())) {
			sql = sql + " and t.endDate >= '" + map.get("endDateBegin") + "'";
		}
		if (null != map.get("endDateEnd") && StringUtils.isNotEmpty(map.get("endDateEnd").toString())) {
			sql = sql + " and t.endDate <= '" + map.get("endDateEnd") + "'";
		}

		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + " and t.isDelete  = " + map.get("isDelete") + "";
		}

		if (null != map.get("enterType") && StringUtils.isNotEmpty(map.get("enterType").toString())) {
			sql = sql + " and t.enterType  = " + map.get("enterType") + "";
		}
		if(map.containsKey("havePid")){
			
			sql = sql + " and t.pid is null";
		}

		sql = sql + "  order by orderId DESC";//新增时间排序（因为orderId为时间生成） 时间越近的订单排名越靠前

		List<Order> list = baseDAO.getJdbcTemplate().query(sql, new Object[] {},
				ParameterizedBeanPropertyRowMapper.newInstance(Order.class));
		
		//放到返回订单大对象中
		return addOrderToBigOrder(list);

	}
	/**
	 * 
	 * 描述: 根据订单id,获取订单的详情信息
	 * 作者:gyp
	 * @Date	 2017年5月3日
	 */
	public Order getOrderInfo(String orderId) {
		
		String sql = "select " + getColumns() + " from `xiaoka-xydk`.order t  "
				+ " where 1=1 and t.orderId = '" +orderId + "'";
		
		List<Order> list = baseDAO.getJdbcTemplate().query(sql, new Object[] {},
				ParameterizedBeanPropertyRowMapper.newInstance(Order.class));
		if(list.size()>0){
			return list.get(0);
		}
		return null;
		
	}
	/**
	 * 
	 * 描述:根据订单的Id获取订单详情中的技能名称
	 * 作者:gyp
	 * @Date	 2017年7月5日
	 */
	public Map<String,Object> getOrderDetailInfoByskillName(String orderId) {
		
			String sql = "select id,orderId,typeName,typeValue,isDelete,createDate from `xiaoka-xydk`.order_detail t  "
					+ " where t.typeName = 'skillName' and t.orderId = '" +orderId + "'";
			
			Map<String, Object> map = baseDAO.queryForMap(sql);
			return map;
	}
	

	public String getColumns() {
		return ""
						+" orderId as orderId,"
						+" pid as pid,"
						+" openId as openId,"
						+" sellerOpenId as sellerOpenId,"
						+" schoolId as schoolId,"
						+" cityId as cityId,"
						+" createDate as createDate,"
						+" payMoney as payMoney,"
						+" payTime as payTime,"
						+" serviceMoney as serviceMoney,"
						+" diplomatMoney as diplomatMoney,"
						+" platformMoney as platformMoney,"
						+" serviceRatio as serviceRatio,"
						+" diplomatRatio as diplomatRatio,"
						+" platformRatio as platformRatio,"
						+" realityMoney as realityMoney,"
						+" orderStatus as orderStatus,"
						+" payway as payway,"
						+" cancelTime as cancelTime,"
						+" invalidTime as invalidTime,"
						+" confirmTime as confirmTime,"
						+" refundTime as refundTime,"
						+" linkman as linkman,"
						+" phone as phone,"
						+" message as message,"
						+" startDate as startDate,"
						+" endDate as endDate,"
						+" isDelete as isDelete,"
						+" enterType as enterType,"
						+" partnerTradeNo as partnerTradeNo"
				;
	}

	/**
	 * 更改订单状态 1.更改订单状态时需要在
	 * controller 层查询订单openId 值是否与当前OpenId值一致
	 * @return
	 */
	public boolean updateOrderState(Map<String, Object> map) {

		String sql = "update `xiaoka-xydk`.order set ";
		if (null != map.get("openId") && StringUtils.isNotEmpty(map.get("openId").toString())) {
			sql = sql + "			openId=:openId ,";
		}
		if (null != map.get("createDate") && StringUtils.isNotEmpty(map.get("createDate").toString())) {
			sql = sql + "			createDate=:createDate ,";
		}
		if (null != map.get("payMoney") && StringUtils.isNotEmpty(map.get("payMoney").toString())) {
			sql = sql + "			payMoney=:payMoney ,";
		}
		if (null != map.get("payTime") && StringUtils.isNotEmpty(map.get("payTime").toString())) {
			sql = sql + "			payTime=:payTime ,";
		}
		if (null != map.get("realityMoney") && StringUtils.isNotEmpty(map.get("realityMoney").toString())) {
			sql = sql + "			realityMoney=:realityMoney ,";
		}
		if (null != map.get("orderStatus") && StringUtils.isNotEmpty(map.get("orderStatus").toString())) {
			sql = sql + "			orderStatus=:orderStatus ,";
		}
		if (null != map.get("payway") && StringUtils.isNotEmpty(map.get("payway").toString())) {
			sql = sql + "			payway=:payway ,";
		}
		if (null != map.get("cancelTime") && StringUtils.isNotEmpty(map.get("cancelTime").toString())) {
			sql = sql + "			cancelTime=:cancelTime ,";
		}
		if (null != map.get("invalidTime") && StringUtils.isNotEmpty(map.get("invalidTime").toString())) {
			sql = sql + "			invalidTime=:invalidTime ,";
		}
		if (null != map.get("confirmTime") && StringUtils.isNotEmpty(map.get("confirmTime").toString())) {
			sql = sql + "			confirmTime=:confirmTime ,";
		}
		if (null != map.get("refundTime") && StringUtils.isNotEmpty(map.get("refundTime").toString())) {
			sql = sql + "			refundTime=:refundTime ,";
		}
		if (null != map.get("linkman") && StringUtils.isNotEmpty(map.get("linkman").toString())) {
			sql = sql + "			linkman=:linkman ,";
		}
		if (null != map.get("phone") && StringUtils.isNotEmpty(map.get("phone").toString())) {
			sql = sql + "			phone=:phone ,";
		}
		if (null != map.get("message") && StringUtils.isNotEmpty(map.get("message").toString())) {
			sql = sql + "			message=:message ,";
		}
		if (null != map.get("startDate") && StringUtils.isNotEmpty(map.get("startDate").toString())) {
			sql = sql + "			startDate=:startDate ,";
		}
		if (null != map.get("endDate") && StringUtils.isNotEmpty(map.get("endDate").toString())) {
			sql = sql + "			endDate=:endDate ,";
		}
		if (null != map.get("isDelete") && StringUtils.isNotEmpty(map.get("isDelete").toString())) {
			sql = sql + "			isDelete=:isDelete ,";
		}
		if (null != map.get("enterType") && StringUtils.isNotEmpty(map.get("enterType").toString())) {
			sql = sql + "			enterType=:enterType ,";
		}
		//增加接单时间
		if (null != map.get("receiveTime") && StringUtils.isNotEmpty(map.get("receiveTime").toString())) {
			sql = sql + "			receiveTime=:receiveTime ,";
		}
		//增加是否完成服务标示
		if (null != map.get("isFinish") && StringUtils.isNotEmpty(map.get("isFinish").toString())) {
			sql = sql + "			isFinish=:isFinish ,";
		}
		//增加是否完成服务完成时间
		if (null != map.get("finishTime") && StringUtils.isNotEmpty(map.get("finishTime").toString())) {
			sql = sql + "			finishTime=:finishTime ,";
		}
		//存入增加申请退款时间
		if (null != map.get("applyRefundTime") && StringUtils.isNotEmpty(map.get("applyRefundTime").toString())) {
			sql = sql + "			applyRefundTime=:applyRefundTime ,";
		}
		
		sql = sql.substring(0, sql.length() - 1);//去逗号
		sql = sql + " where orderId=:orderId";
		return baseDAO.executeNamedCommand(sql, map);
	}
	/**
	 * 
	 * 描述:添加退款信息
	 * 作者:gyp
	 * @Date	 2017年5月3日
	 */
	public boolean addRefundInfo(Map<String,Object> map){
		map.put("id",Sequence.nextId());
		String addSql = "insert into `xiaoka-xydk`.applicationrefunds "
				+ " (id,openId,refundsOrderId,refundsMoney,bank,bankCardNumber,AccountFullName,checkStatus,orderSourceStatus,createTime)"
				+ " values "
				+ " (:id,:openId,:refundsOrderId,:refundsMoney,:bank,:bankCardNumber,:AccountFullName,:checkStatus,:orderSourceStatus,:createTime)";
		boolean flag = baseDAO.executeNamedCommand(addSql, map);
		return flag;
	}

	/**
	 * 根据传入的参数，查询当前订单的私有化信息
	 */
	public List<OrderDetail> searchOrderDetail(Map<String, Object> map) {

		String sql = "select id,orderId,typeName,typeValue,isDelete,createDate "
				+ "from `xiaoka-xydk`.order_detail where 1=1";

		//当前传入参数为 orderId 和 isDelete 为0 以后有需要再加
		if (map.get("orderId") != null && StringUtils.isNotBlank(map.get("orderId").toString())) {
			sql = sql + " and orderId='"+map.get("orderId").toString()+"'";
		}
		if (map.get("isDelete") != null && StringUtils.isNotBlank(map.get("isDelete").toString())) {
			sql = sql + " and isDelete='"+map.get("isDelete").toString()+"'";
		}

		List<OrderDetail> list = baseDAO.getJdbcTemplate().query(sql,new Object[]{},
				ParameterizedBeanPropertyRowMapper.newInstance(OrderDetail.class));
		return list;
	}
	
	/**
	 * 根据传入的参数，查询当前订单的私有化信息
	 * TODO
	 */
	public List<OrderDetail> searchOrderDetailByPid(Map<String, Object> map) {

		String sql = "select id,orderId,pid,typeName,typeValue,isDelete,createDate "
				+ "from `xiaoka-xydk`.order_detail where 1=1";

		//当前传入参数为 orderId 和 isDelete 为0 以后有需要再加
		if (map.get("pid") != null && StringUtils.isNotBlank(map.get("pid").toString())) {
			sql = sql + " and pid='"+map.get("pid").toString()+"'";
		}
		if (map.get("isDelete") != null && StringUtils.isNotBlank(map.get("isDelete").toString())) {
			sql = sql + " and isDelete='"+map.get("isDelete").toString()+"'";
		}

		List<OrderDetail> list = baseDAO.getJdbcTemplate().query(sql,new Object[]{},
				ParameterizedBeanPropertyRowMapper.newInstance(OrderDetail.class));
		return list;
	}

	
	/**
	 * 处理订单对象，放到订单的集合中去
	 */
	public List<OrderSea> addOrderToBigOrder(List<Order> order){
		
		List<OrderSea> list = new ArrayList<OrderSea>();
		
		for (Order orderSing : order) {
			//若PaySum有值则赋给payMoney
			if(StringUtils.isNotBlank(orderSing.getPaySum())){
				
				orderSing.setPayMoney(Double.parseDouble(orderSing.getPaySum()));
			}
			OrderSea orderSea = new OrderSea();
			orderSea.setOrder(orderSing);
			list.add(orderSea);
		}
		return list;
		
	}
	
	/**
	 * 查询当前订单状态的总数
	 */
	@SuppressWarnings("deprecation")
	public int searchOrderCount(Map<String,Object> map){
		
		//sql
		String sql = "select count(*) from `xiaoka-xydk`.order t where 1=1 ";
		
		if(map.containsKey("openId")&&map.get("openId")!=null){
			sql = sql+"and t.openId = '" + map.get("openId") + "'";
		}
		if(map.containsKey("orderStatus")&&map.get("orderStatus")!=null){
			sql = sql+"and t.orderStatus ="+ map.get("orderStatus") +"";
		}
		return baseDAO.getJdbcTemplate().queryForInt(sql);
	}
	
	public Map<String, Object> getExpenseAmount(){
		String sql = "select t.dic_value from v2_dic_data t LEFT JOIN  v2_dic_type dt ON t.dic_id = dt.dic_id WHERE dt.dic_code = 'tkfwje'";
		Map<String, Object> map = baseDAO.queryForMap(sql);
		return map;
	}
	
	public List<Map<String, Object>> getBankInfo(){
		String sql = "select t.id as value,t.dic_name as text from v2_dic_data t LEFT JOIN  v2_dic_type dt ON t.dic_id = dt.dic_id WHERE dt.dic_code = 'bankType' and t.isDelete = 0";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	
	public boolean updateRefundInfo(String orderId, String checkStatus,String orderSourceStatus) {
		try {
			String sql = " update `xiaoka-xydk`.applicationrefunds t set t.checkStatus=:checkStatus, t.orderSourceStatus=:orderSourceStatus where t.refundsOrderId=:orderId ";
			Map map = new HashMap();
			map.put("checkStatus", checkStatus);
			map.put("orderSourceStatus", orderSourceStatus);
			map.put("orderId", orderId);
			boolean flag = baseDAO.executeNamedCommand(sql, map);
			return flag;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * 查询当前关于pid中is Not Null 的值
	 */
	public List<OrderSea> getSellerOrder(String openId){
		
		List<String> orderStatu = new ArrayList<String>();
		
		String sql ="select *, sum(payMoney) as paySum from `xiaoka-xydk`.order t  GROUP BY pid HAVING"
				+ " t.pid is not null and t.openId='"+openId+"';";
		List<Order> list = baseDAO.getJdbcTemplate().query(sql, new Object[] {},
				ParameterizedBeanPropertyRowMapper.newInstance(Order.class));
		//放到返回订单大对象中
		//处理当前订单的订单状态
		for (int i = 0; i < list.size(); i++) {
			orderStatu = publishService.getPublishOrder(list.get(i).getPid());
			String orderStatus = StringUtil.judgeOrderStatu(orderStatu);
			if(StringUtil.judgeOrderStatu(orderStatu)!=""){
				list.get(i).setOrderStatus(Integer.parseInt(orderStatus));
			}
			
		}
		return addOrderToBigOrder(list);
	}
	
	/**
	 * 查询当前关于pid下的所有的订单信息
	 */
	public List<Map<String,Object>> getAllOrderForSeller(String pId){
		
		String sql = "select * from `xiaoka-xydk`.order  where order.pid ='"+pId+"'";
		return baseDAO.getJdbcTemplate().queryForList(sql);
		
	}
	
	/**
	 * 获取当前的用户信息 
	 * 根据OpenId值
	 */
	public Map<String,Object> getUserMessageByOpenId(String openId){
		
		String sql = "select * from `xiaoka-xydk`.user_basicinfo ub where ub.openId='"+openId+"'";
		
		return baseDAO.getJdbcTemplate().queryForMap(sql);
		
	}
	
	/**
	 * 获取当前用户信息
	 * 高根据basicId值
	 */
	public Map<String,Object> getUserMessageByBasicId(String basicId){
		
		String sql = "select * from `xiaoka-xydk`.user_basicinfo ub where ub.id='"+basicId+"'";
		
		return baseDAO.getJdbcTemplate().queryForMap(sql);
	}
	
	
}
