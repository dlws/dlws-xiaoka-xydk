/**
 * ChatDao.java
 * com.jzwl.xydk.wap.xkh2_2.chat.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
 */

package com.jzwl.xydk.wap.xkh2_2.chat.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * 信息dao
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author wxl
 * @Date 2017年4月7日
 */
@Repository("chatMessageDao")
public class ChatMessageDao {

	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	/*
	 * 未读数量 信息列表
	 */
	public List<Map<String, Object>> queryChatList(Map<String, Object> paramsMap) {

		/*String sql = "SELECT ns.id as id,ns.myOpenId as myOpenId,ns.otherOpenId as otherOpenId,ns.chatType as chatType,ns.createDate as mycreateDate,ntc.contentChat as contentChat,"
				+ "ntc.createDate  as createDate,bi.headPortrait as headpic,bi.nickname as nickname,bi.userName as userName,(select COUNT(*) from `xiaoka-xydk`.newsstaffchat nst where nst.newsId = ns.id and nst.isRead=0 ) as unReadNum "
				+ "FROM `xiaoka-xydk`.newsstaff ns "
				+ "LEFT JOIN `xiaoka-xydk`.newsstaffchat ntc ON ns.id=ntc.newsId "
				+ "LEFT JOIN `xiaoka-xydk`.user_basicinfo bi ON ns.otherOpenId=bi.openId "
				+ "WHERE ns.myOpenId='"
				+ paramsMap.get("openId")
				+ "'or ns.otherOpenId = '" + paramsMap.get("openId") + "'" + "GROUP BY ntc.newsId " + "ORDER BY MAX(ntc.createDate),ntc.createDate";*/
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT 	ns.id AS id,ns.myOpenId AS myOpenId,ns.otherOpenId AS otherOpenId,ns.chatType AS chatType,");
		sql.append("   t1.contentChat,t1.createDate,bi.headPortrait AS headpic,bi.nickname AS nickname,bi.userName AS userName,");
		sql.append("   (SELECT COUNT(*) FROM `xiaoka-xydk`.newsstaffchat nst WHERE nst.newsId = ns.id AND nst.isRead = 0)AS unReadNum");
		sql.append("  FROM `xiaoka-xydk`.newsstaff ns LEFT JOIN (SELECT * FROM `xiaoka-xydk`.newsstaffchat ntc  WHERE");
		sql.append("  ( SELECT COUNT(1) FROM `xiaoka-xydk`.newsstaffchat ntc1 WHERE ntc1.createDate>ntc.createDate AND ntc.newsId=ntc1.newsId)<1)t1"); 
		sql.append(" ON ns.id=t1.newsId LEFT JOIN `xiaoka-xydk`.user_basicinfo bi ON ns.otherOpenId = bi.openId");
		sql.append("   WHERE ns.myOpenId = '" + paramsMap.get("openId") + "' OR ns.otherOpenId = '" + paramsMap.get("openId") + "'");
		sql.append(" ORDER BY createDate DESC");
		return baseDAO.queryForList(sql.toString());

	}

	/*
	 * 信息详情，聊天列表
	 */
	public List<Map<String, Object>> getChatInfoById(Map<String, Object> paramsMap) {

		String sql = "SELECT nsc.id as id,nsc.myOpenId as myOpenId,nsc.otherOpenId as otherOpenId,"
				+ " nsc.contentChat as contentChat,nsc.createDate as createDate,ub.userName as userName,"
				+ " ub.nickname as nickname,ub.headPortrait as headPortrait" + " FROM `xiaoka-xydk`.newsstaffchat nsc "
				+ " LEFT JOIN `xiaoka-xydk`.user_basicinfo ub ON nsc.myOpenId=ub.openId" + " WHERE newsId='" + paramsMap.get("id")
				+ "' AND nsc.isDelete=0 ORDER BY nsc.createDate ASC ";
		return baseDAO.queryForList(sql);

	}

	/*
	 * 更新未读为已读
	 */
	public boolean updatechatStatus(Map<String, Object> paramsMap) {

		String sql = "UPDATE `xiaoka-xydk`.newsstaffchat SET isRead=1 where newsId='" + paramsMap.get("id") + "'AND isDelete=0 ";
		return baseDAO.executeNamedCommand(sql, paramsMap);

	}
	
	/*
	 * 系统消息更新未读为已读
	 */
	public boolean updatechatSyStatus(Map<String, Object> paramsMap) {
		
		String sql = "UPDATE `xiaoka-xydk`.newsstaffchat SET isRead=1 where newsId='" + paramsMap.get("id") + "' AND isDelete=0 "
				+ "and otherOpenId='"+paramsMap.get("otherOpenId")+"' and myOpenId='0' ";
		return baseDAO.executeNamedCommand(sql, paramsMap);
		
	}

	/*
	 * 获取聊天title
	 */
	public Map<String, Object> getChatTitle(Map<String, Object> paramsMap) {

		String sql = "SELECT ub.userName userName,ub.nickname nickname,ns.myOpenId myOpenId,"
				+ " ns.otherOpenId otherOpenId FROM `xiaoka-xydk`.newsstaff ns "
				+ " LEFT JOIN `xiaoka-xydk`.user_basicinfo ub ON ns.otherOpenId = ub.openId " + " WHERE ns.id='" + paramsMap.get("id") + "'";
		return baseDAO.queryForMap(sql);

	}

	public Map<String, Object> getHeadImg(Map<String, Object> paramsMap){
		String sql = "SELECT headImgUrl FROM v2_wx_customer  WHERE openId='"+paramsMap.get("openId")+"'";
		return baseDAO.queryForMap(sql);
	}
	/*
	 * 添加内容--聊天
	 */
	public boolean insertChatContent(Map<String, Object> map) {

		map.put("id", Sequence.nextId());
		map.put("createDate", new Date());
		map.put("isRead", 0);
		map.put("isDelete", 0);
		String sql = "INSERT INTO `xiaoka-xydk`.newsstaffchat (id,newsId,myOpenId,otherOpenId,contentChat,isRead,createDate,isDelete)"
				+ "VALUES (:id,:newsId,:myOpenId,:otherOpenId,:contentChat,:isRead,:createDate,:isDelete)";

		return baseDAO.executeNamedCommand(sql, map);

	}

	public List<Map<String, Object>> getSysList(Map<String, Object> paramsMap) {


		/*String sql = "SELECT * FROM `xiaoka-xydk`.newsstaffchat WHERE newsId='0' and otherOpenId='" + paramsMap.get("meOpenId")
				+ "' AND isDelete=0 ORDER BY createDate asc";*/
		String sql = "select * from "+
					 " (SELECT nc.id,nc.newsId,nc.myOpenId,nc.otherOpenId,nc.contentChat,nc.isRead,nc.orderId,nc.createDate,nc.isDelete FROM `xiaoka-xydk`.newsstaffchat nc "
					 + " WHERE nc.newsId='0' "
					 + " and nc.otherOpenId='"+paramsMap.get("meOpenId")+"' "
					 + " AND nc.isDelete=0 "
					 + " AND nc.contentChat <> 'null'"
					 + " AND NC.CREATEDATE >'"+paramsMap.get("createDate")+"'"+ 

					 " UNION ALL "+
					 " SELECT nt.id,nt.newsId,nt.myOpenId,nt.otherOpenId,nt.contentChat,nt.isRead,nt.orderId,nt.createDate,nt.isDelete "
					 + " FROM `xiaoka-xydk`.newsstaffchat nt WHERE nt.newsId='0' and nt.otherOpenId='0' "
					 + " AND nt.otherOpenId = '0' AND nt.isDelete=0 "
					 + " AND nt.contentChat <> 'null' "
					 + " AND nt.CREATEDATE > '"+paramsMap.get("createDate")+"'"+
					 " ) t ORDER BY t.createDate asc";
		return baseDAO.queryForList(sql);

	}

	public Map<String, Object> querySysMap(Map<String, Object> paramsMap) {

		String sql = "SELECT createDate,contentChat,orderId,"
				+ "(SELECT COUNT(1) FROM	`xiaoka-xydk`.newsstaffchat "
				+" WHERE myOpenId = '0'AND otherOpenId = '"+ paramsMap.get("openId") + "' AND isDelete = 0 AND isRead = 0)sysNo "
				+ " FROM `xiaoka-xydk`.newsstaffchat WHERE myOpenId='0' AND  otherOpenId='"
				+ paramsMap.get("openId") + "' AND isDelete=0 GROUP BY myOpenId ORDER BY MAX(createDate)";
		return baseDAO.queryForMap(sql);

	}

	public String updateIsRead(Map<String, Object> paramsMap) {

		String orderId = paramsMap.get("orderId").toString();
		String sql = "UPDATE `xiaoka-xydk`.newsstaffchat set isRead=1 where orderId='" + paramsMap.get("orderId") + "'";
		boolean b = baseDAO.executeNamedCommand(sql, paramsMap);
		System.out.println("orderId="+orderId);
		if (b)
			return orderId;
		else
			return "0";
	}

	public String getChatRelation(Map<String, Object> paramsMap) {

		String sql = "SELECT id from `xiaoka-xydk`.newsstaff WHERE (myOpenId='" + paramsMap.get("myOpenId") + "' AND otherOpenId='"
				+ paramsMap.get("otherOpenId") + "') OR (myOpenId='" + paramsMap.get("otherOpenId") + "' AND otherOpenId='"
				+ paramsMap.get("myOpenId") + "')  AND isDelete=0 AND chatType=2";
		Map<String, Object> map = baseDAO.queryForMap(sql);
		if (map.size() > 0)
			return map.get("id").toString();
		else
			return "0";
	}

	public String createChatRelation(Map<String, Object> paramsMap) {

		String id = Sequence.nextId();
		paramsMap.put("id", id);
		paramsMap.put("chatType", 2);
		paramsMap.put("isDelete", 0);
		paramsMap.put("createDate", new Date());
		String sql = "INSERT INTO `xiaoka-xydk`.newsstaff (id,myOpenId,otherOpenId,chatType,createDate,isDelete) values (:id,:myOpenId,:otherOpenId,:chatType,:createDate,:isDelete)";
		boolean btrue = baseDAO.executeNamedCommand(sql, paramsMap);
		if (btrue)
			return id;
		else
			return "0";

	}

	public boolean insertOrderForBuy(Map<String, Object> paramsMap) {
		//id-序列化  newsId-0 myOpenId-0 otherOpenId-购买人的openId 
		//contentChat-   isRead-0 orderId-orderId createDate-当前时间 isDelete-0
		String buyNewsId = Sequence.nextId();
		Date buyDate = new Date();
		paramsMap.put("buyDate", buyDate);
		String buysql = "INSERT INTO `xiaoka-xydk`.newsstaffchat (id,newsId,myOpenId,otherOpenId,contentChat,isRead,orderId,createDate,isDelete) "
				+ " VALUES ('" + buyNewsId + "','" + 0 + "','" + 0 + "','" + paramsMap.get("sellOpenId") + "',1,'" + 0 + "','"
				+ paramsMap.get("isSuccess") + "',:buyDate,0)";
		return baseDAO.executeNamedCommand(buysql, paramsMap);

	}
	/**
	 * 群发信息添加列表
	 * 描述:
	 * 作者:gyp
	 * @Date	 2017年6月12日
	 */
	public boolean insertInfoByMass(Map<String, Object> paramsMap) {
		//id-序列化  newsId-0 myOpenId-0 otherOpenId-购买人的openId 
		//contentChat-   isRead-0 orderId-orderId createDate-当前时间 isDelete-0
		String id = Sequence.nextId();
		Date date = new Date();
		paramsMap.put("date", date);
		String buysql = "INSERT INTO `xiaoka-xydk`.newsstaffchat (id,newsId,myOpenId,otherOpenId,contentChat,isRead,orderId,createDate,isDelete) "
				+ " VALUES ('" + id + "','" + 0 + "','" + 0 + "','0','"+paramsMap.get("content")+"','0','0',:date,0)";
		return baseDAO.executeNamedCommand(buysql, paramsMap);

	}

	//卖家端
	public boolean insertUpdateSysMeg(Map<String, Object> paramsMap) {

		String id = Sequence.nextId();
		paramsMap.put("id", id);
		paramsMap.put("newsId", 0);
		paramsMap.put("myOpenId", 0);
		paramsMap.put("isRead", 0);
		paramsMap.put("createDate", new Date());
		paramsMap.put("isDelete", 0);
		String sql = "INSERT INTO `xiaoka-xydk`.newsstaffchat (id,newsId,myOpenId,otherOpenId,contentChat,isRead,orderId,createDate,isDelete) "
				+ " VALUES " + "(:id,:newsId,:myOpenId,'" + paramsMap.get("nowOpenId") + "','" + paramsMap.get("orderStatus")
				+ "',:isRead,:orderId,:createDate,:isDelete)";
		return baseDAO.executeNamedCommand(sql, paramsMap);
	}
	
	    //买家端
		public boolean insertBuyMeg(Map<String, Object> paramsMap) {

			String id = Sequence.nextId();
			paramsMap.put("id", id);
			paramsMap.put("newsId", 0);
			paramsMap.put("myOpenId", 0);
			paramsMap.put("isRead", 0);
			paramsMap.put("createDate", new Date());
			paramsMap.put("isDelete", 0);
			String sql = "INSERT INTO `xiaoka-xydk`.newsstaffchat (id,newsId,myOpenId,otherOpenId,contentChat,isRead,orderId,createDate,isDelete) "
					+ " VALUES " + "(:id,:newsId,:myOpenId,'" + paramsMap.get("openId") + "','" + paramsMap.get("orderStatus")
					+ "',:isRead,:orderId,:createDate,:isDelete)";
			return baseDAO.executeNamedCommand(sql, paramsMap);
		}
		/**
		 * 删除聊天
		 * @param map
		 * @return
		 */
		public boolean deletChat(Map<String, Object> map) {

			String sql = "delete from  `xiaoka-xydk`.newsstaff  where id=:id";

			return baseDAO.executeNamedCommand(sql, map);

		}
		
		
		/**
		 * 查询当前是否有新的消息
		 * @param openId
		 * @return
		 */
		public boolean haveNewMessage(String openId){
			
			boolean flage = false;
			
			String sql = "select COUNT(*) as chatCount from  `xiaoka-xydk`.newsstaffchat t WHERE t.otherOpenId = '"+openId+"' "
					+ "and t.isRead =  0 and isDelete = 0";
			if(baseDAO.getJdbcTemplate().queryForInt(sql)>0){
				
				flage = true;
			}
			return flage;
		}
		
		/**
		 * 更新当前消息为已过期不进行展示
		 * 将消息表中的消息今天之前的进行isDelete=0的操作
		 */
		public void updateMessageStatu(){
			try {
				String sql =" update `xiaoka-xydk`.newsstaffchat nchat SET  nchat.isDelete=1 where nchat.createDate < str_to_date(DATE_FORMAT(NOW(),'%Y-%m-%d'),'%Y-%m-%d %H:%i:%s')";
				baseDAO.getJdbcTemplate().execute(sql);
			} catch (Exception e) {
				System.out.println("------------当前更新消息状态异常----------------");
			}
			
			
		}
		
		
}
