/**
 * PlaceOrderDao.java
 * com.jzwl.xydk.wap.xkh2_2.payment.consultation.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.payment.consultation.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * 下单方法dao
 *
 * @author   wxl
 * @Date	 2017年4月11日 	 
 */
@Repository("placeOrderDao")
public class PlaceOrderDao {

	@Autowired
	private BaseDAO baseDAO;

	public Map<String, Object> getSkillBaseInfo(Map<String, Object> paramsMap) {

		String sql = "SELECT us.*" + " FROM `xiaoka-xydk`.preferred_service us " + " WHERE us.id='" + paramsMap.get("skillId")
				+ "' AND us.isDelete=0 ";

		return baseDAO.queryForMap(sql);
	}

	/*
	 * 添加到订单
	 */
	public String insertOrder(Map<String, Object> paramsMap) {

		String orderid = Sequence.nextId();
		paramsMap.put("orderId", orderid);//订单id/号
		paramsMap.put("createDate", new Date());//创建时间
		paramsMap.put("payway", 0);//支付方式--微信支付
		paramsMap.put("orderStatus", 1);//订单状态--下单成功
		paramsMap.put("isDelete", 0);//是否删除
		Date date = new Date();
		Integer day = 1;
		date = DateUtil.addDay(date, day);
		paramsMap.put("invalidTime", date);

		Map<String, Object> daMap = new HashMap<String, Object>();
		daMap = paramsMap;

		String insertOrderSql = "insert into `xiaoka-xydk`.order "
				+ " (orderId,openId,sellerOpenId,createDate,payMoney,serviceMoney,diplomatMoney,platformMoney,serviceRatio,diplomatRatio,platformRatio,realityMoney,"
				+ "orderStatus,payway,linkman,phone,isDelete,enterType,schoolId,cityId,invalidTime) "
				+ " values "
				+ " (:orderId,:openId,:sellerOpenId,:createDate,:payMoney,:serviceMoney,:diplomatMoney,:platformMoney,:serviceRatio,:diplomatRatio,:platformRatio,:realityMoney,"
				+ ":orderStatus,:payway,:linkman,:phone,:isDelete,:enterType,:schoolId,:cityId,:invalidTime)";
		boolean flag = baseDAO.executeNamedCommand(insertOrderSql, paramsMap);
		
		String delSql = "DELETE FROM  `xiaoka-xydk`.selectskill WHERE purOpenId='"+paramsMap.get("openId")+"' AND skillId='"+paramsMap.get("skillId")+"'"
				+ " and openId='"+paramsMap.get("sellerOpenId")+"'";
				baseDAO.executeNamedCommand(delSql, paramsMap);
		daMap.remove("openId");
		daMap.remove("sellerOpenId");
		daMap.remove("createDate");
		daMap.remove("payMoney");
		daMap.remove("serviceMoney");
		daMap.remove("diplomatMoney");
		daMap.remove("platformMoney");
		daMap.remove("serviceRatio");
		daMap.remove("diplomatRatio");
		daMap.remove("platformRatio");
		daMap.remove("realityMoney");
		daMap.remove("schoolId");
		daMap.remove("cityId");
		daMap.remove("orderStatus");
		daMap.remove("payway");
		daMap.remove("linkman");
		daMap.remove("phone");
		daMap.remove("isDelete");
		daMap.remove("enterType");
		daMap.remove("orderId");
		daMap.remove("invalidTime");
		String DataSql = "insert into `xiaoka-xydk`.order_detail(id,orderId,typeName,typeValue,isDelete,createDate)" + "values"
				+ "(:id,:orderId,:typeName,:typeValue,:isDelete,:createDate)";
		boolean temp = false;

		if (flag) {
			for (Entry<String, Object> entry : daMap.entrySet()) {
				Map<String, Object> inserMap = new HashMap<String, Object>();
				inserMap.put("id", Sequence.nextId());
				inserMap.put("orderId", orderid);
				inserMap.put("typeName", entry.getKey());
				inserMap.put("typeValue", entry.getValue());
				inserMap.put("isDelete", 0);
				inserMap.put("createDate", new Date());
				temp = baseDAO.executeNamedCommand(DataSql, inserMap);
			}
		}
		if (temp) {
			return orderid;
		} else {
			return "0";
		}

	}

	public Map<String, Object> getSelBaseInfo(Map<String, Object> paramsMap) {

		String sql = " SELECT b.*,si.schoolName,ct.cityName FROM `xiaoka-xydk`.user_basicinfo  b LEFT JOIN `xiaoka`.tg_school_info si on si.id=b.schoolId "
				+ " LEFT JOIN tg_city ct ON ct.id=b.cityId" + "   WHERE b.isDelete = 0 and b.openId = '" + paramsMap.get("selOpenId") + "' ";
		return baseDAO.queryForMap(sql);

	}

	public Map<String, Object> getOrderDetail(Map<String, Object> paramsMap) {

		String sql = " SELECT * FROM `xiaoka-xydk`.order WHERE orderId='" + paramsMap.get("orderId") + "' and isDelete=0";
		return baseDAO.queryForMap(sql);

	}

	public List<Map<String, Object>> getOrderDetailData(Map<String, Object> paramsMap) {

		String sql = " SELECT * FROM `xiaoka-xydk`.order_detail WHERE orderId='" + paramsMap.get("orderId") + "' and isDelete=0";
		return baseDAO.queryForList(sql);

	}

	public String updateOrderStatus(Map<String, Object> paramsMap) {
		String orderId = paramsMap.get("orderId").toString();
		paramsMap.put("cancelTime", new Date());//取消订单的时间
		if ("10".equals(paramsMap.get("orderStatus"))) {
			String sql = "UPDATE `xiaoka-xydk`.order SET cancelTime=:cancelTime,orderStatus='" + paramsMap.get("orderStatus") + "'  WHERE orderId='"
					+ paramsMap.get("orderId") + "'";
			boolean flag = baseDAO.executeNamedCommand(sql, paramsMap);
			if (flag)
				return orderId;
			else
				return "0";
		} else {
			String sql = "UPDATE `xiaoka-xydk`.order SET orderStatus='" + paramsMap.get("orderStatus") + "'  WHERE orderId='"
					+ paramsMap.get("orderId") + "'";
			boolean flag = baseDAO.executeNamedCommand(sql, paramsMap);
			if (flag)
				return orderId;
			else
				return "0";
		}
	}
	/**]
	 * 
	 * 描述:更新用户的订单
	 * 作者:gyp
	 * @Date	 2017年4月26日
	 */
	public String updateOrder(Map<String, Object> map) {
		String orderId = map.get("orderId").toString();
		String sql = "UPDATE `xiaoka-xydk`.order SET orderStatus='" + map.get("orderStatus") + "'  WHERE orderId='"
				+ map.get("orderId") + "'";
		boolean flag = baseDAO.executeNamedCommand(sql, map);
		if (flag){
			return orderId;
		}else{
			return "0";
		}
	}
	
	
	

	public boolean addBuyNum(Map<String, Object> paramsMap) {

		String sql = "UPDATE `xiaoka-xydk`.preferred_service set salesVolume=salesVolume+1 where id='" + paramsMap.get("id") + "'";
		return baseDAO.executeNamedCommand(sql, paramsMap);
	}

}
