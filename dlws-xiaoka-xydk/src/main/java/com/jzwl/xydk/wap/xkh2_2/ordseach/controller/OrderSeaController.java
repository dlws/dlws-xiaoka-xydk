package com.jzwl.xydk.wap.xkh2_2.ordseach.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.sign.AccessCore;
import com.jzwl.common.utils.DataUtil;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.dividewithdraw.service.DivideDrawService;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.refunds.service.RefundsService;
import com.jzwl.xydk.manager.skillUser.service.SkillUserManagerService;
import com.jzwl.xydk.manager.special.service.SpecialService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.business.home.controller.HomeController;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.ComparatorOrder;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.Order;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderSea;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderCountService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_2.payment.wallet.service.WalletService;
import com.jzwl.xydk.wap.xkh2_4.diplomat.service.DiplomatService;


/**
 * 订单管理中买家端
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value="/ordersea")
public class OrderSeaController extends BaseWeixinController{
	
	@Autowired
	OrderSeaService orderSeaService;
	
	@Autowired
	private HomeService homeService;

	@Autowired
	private SkillUserService skillUserService;
	
	@Autowired
	private SkillUserManagerService skillUserManagerService;
	
	@Autowired
	private SpecialService specialService;
	
	@Autowired
	private PreferredServiceService preferredServiceService;
	
	@Autowired
	private ChatMessageService chatMessageService;
	
	@Autowired
	private SendMessageService sendMessage; //调用模板消息使用
	
	@Autowired
	private Constants contants; 
	
	@Autowired
	private OrderCountService orderCountService;
	
	//用于存储当前订单总数
	private int orderStatusCount;
	@Autowired
	private WalletService walletService;
	@Autowired
	private DivideDrawService divideDrawService;
	@Autowired
	private RefundsService refundsService;
	
	@Autowired
	private DiplomatService diplomate;
	
	Logger log = Logger.getLogger(HomeController.class);
	
	//订单管理卖家端订单展示
	@RequestMapping(value="/searchOrderSell")
	public String searchOrderSell(HttpServletRequest request,HttpServletRequest response,Model mov){
		
		
		//用来计算回显的tab
		if(!paramsMap.containsKey("tabNum")||paramsMap.get("tabNum")==null){
			paramsMap.put("tabNum", 0);
		}
		//获取当前自定义查询条件
		createParameterMap(request);
		//装入查询条件
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String, Object>> skillImg = new ArrayList<Map<String,Object>>();//存入当前技能图片
		List<OrderSea> ordList = new ArrayList<OrderSea>();
		List<OrderSea> sellerList = new ArrayList<OrderSea>();
		//1.校验OpenId值
		String openId = getCurrentOpenId(request); //get  openId
		
		//更新当前orderCount表中的统计值
		orderCountService.upperCount(openId,paramsMap.get("tabNum").toString());
		
		if("null"==openId&&StringUtils.isBlank(openId)){
			request.setAttribute("msg", "查询失败！");
			log.error("当前用户OpenId值为空");
			return"/wap/xkh_version_2/index_2-1";
		}
		//2.用当前用户的OpenId值在对应的订单表中寻找
		if(paramsMap.get("orderStatus")!=null&&StringUtils.isNotBlank(paramsMap.get("orderStatus").toString())){
			map.put("orderStatus",paramsMap.get("orderStatus").toString());
		}
		map.put("openId",openId);
		map.put("isDelete","0");
		map.put("havePid", "");
		ordList = orderSeaService.toSearchOrder(map);
		sellerList = orderSeaService.getSellerOrder(openId);
		ordList.addAll(sellerList);//增加商家列表
		Date date = new Date();
		//3.获取表中的数据状态 提供剩余时间
		for (OrderSea orderSea : ordList) {
			UserBasicinfo  userBasicinfo = new UserBasicinfo();
			
			//存入当前的订单Id进行查询
			map.put("orderId", orderSea.getOrder().getOrderId().toString());
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			
			Map<String,String> mapdetail = dealListForOrdDet(orderList);
			
			//处理当前的订单单价进行展示 存入订单单价
			if(mapdetail.containsKey("skillPrice")){
				orderSea.setSingPrice(mapdetail.get("skillPrice"));
			}
			if(mapdetail.containsKey("begin")&&mapdetail.containsKey("end")){
				String singlePrice = mapdetail.get("begin")+"~"+mapdetail.get("end");
				orderSea.setSingPrice(singlePrice);
			}
			//查询当前产品的销售数量
			if(mapdetail.containsKey("skillId")){
				
				Map<String, Object>  skillMap = skillUserService.getSkillInfoById(mapdetail.get("skillId").toString());//用户技能信息
				paramsMap.put("id", mapdetail.get("skillId").toString());
				Map<String, Object> preferredService = preferredServiceService.getById(paramsMap);
				if(!skillMap.isEmpty()){
					//存入销售技能中的销售数量
					if(skillMap.get("sellNo")==null){
						skillMap.put("sellNo", "0");
					}
					orderSea.setSellNo(skillMap.get("sellNo").toString());
				}else if(!preferredService.isEmpty()){
					
					if(preferredService.get("salesVolume")==null){
						skillMap.put("salesVolume", "0");
					}
					orderSea.setSellNo(preferredService.get("salesVolume").toString());
				}else{
					orderSea.setSellNo("0");
					System.out.println("暂时未查询到当前订单的ID值用0 进行代替");
				}
				
			}
			
			//处理当前List以Key value 形式展示
			orderSea.setMap(mapdetail);	
			
			//检查订单状态，存入差异时间
			if(checkPama(orderSea.getOrder().getOrderStatus().toString())){
				
			    String differTime =	subtracDateS(orderSea.getOrder().getInvalidTime(), date);
			    orderSea.getOrder().setTimeDiffer(differTime);
			    orderSea.setTimmer(orderSea.getOrder().getInvalidTime().getTime());// TODO 拒绝订单时时间问题获取当前date时间
			}
			
			//存放卖家用户信息
			orderSea.setUserBasicinfo(userBasicinfo);
		}
		//获取退款的金额
		Map<String,Object> amountInfo = orderSeaService.getExpenseAmount();
		//处理List排序
		ComparatorOrder comparatorOrder = new ComparatorOrder();
		Collections.sort(ordList, comparatorOrder);
		//返回参数展示
		mov.addAttribute("ordList",ordList);
		mov.addAttribute("amount",amountInfo.get("dic_value"));
		
		mov.addAttribute("tabNum",paramsMap.get("tabNum"));//用来区分当前Tab页
		
		return "wap/xkh_version_2_2/buyer_list";//跳转
	}
	
	/*** 订单管理 买家端  ------ 待付款   */
	//TODO  正式环境下删除该方法
	/*@RequestMapping(value="/updateOrderSelltoPay")
	public @ResponseBody boolean updateOrderSelltoPay(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus",2);
		
		
		if(updateOrderSellCommon(request,map)){
			//成功则推送模板消息
			createParameterMap(request);
			Map<String,Object> sendMap = new HashMap<String, Object>();
			
			sendMap.put("orderId",paramsMap.get("orderId"));
			sendMap.put("isDelete", 0);
			List<OrderSea> ordList = new ArrayList<OrderSea>();
			ordList = orderSeaService.toSearchOrder(sendMap);
			//获取当前买家的OpenId进行推送
			//String orderId,String openId,String payMoney
			String money = ordList.get(0).getOrder().getPayMoney().toString();
			System.out.println("开始发送模板信息"
					+ "===orderID:"+paramsMap.get("orderId").toString()+"+++++opeId:"+ordList.get(0).getOrder().getOpenId()+"++++money:"+money);
			sendMessage.payMoneySuccess(paramsMap.get("orderId").toString(),ordList.get(0).getOrder().getOpenId(),money);
			return true;
		}
		return false;
	}*/
	//支付当前订单  状态更改为2 支付成功
	@RequestMapping(value="/updateOrderSelltoPay")
	public void updateOrderSelltoPay(HttpServletRequest request,HttpServletResponse response){
		//System.out.println("**************************进入付款*******************");

		createParameterMap(request);
		Map<String,Object> signMap = new HashMap<String,Object>();
		try {
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(paramsMap);
			Map<String,String> skillInfo =  dealListForOrdDet(orderList);
			//String openId = "o3P2SwabLBk80n1IdUBzkI3tnFN4";
			String commodityName = skillInfo.get("skillName").toString();
			String totalPrice = paramsMap.get("payMoney").toString();
			String orderId = paramsMap.get("orderId").toString();
			String clientFlag = "xkh";
			String sendUrl = "http://xydk.xiaoka360.com/updatePay/payOldSuccess.html";
			String keyUrlOrderId = "wxPay_xkh|http://xydk.xiaoka360.com/updatePay/paySuccess.html|"+orderId;
			String systemFlag = "xkh";
			signMap.put("openId",openId);
			signMap.put("commodityName",commodityName);
			signMap.put("totalPrice",totalPrice);
			signMap.put("orderId",orderId);
			signMap.put("clientFlag",clientFlag);
			signMap.put("sendUrl",sendUrl);
			signMap.put("keyUrlOrderId",keyUrlOrderId);
			commodityName = URLEncoder.encode(commodityName);
			String sign = AccessCore.sign(signMap, contants.getWx_signKey());//拿到签名数据
			String url = "http://klgj.xiaoka360.com/wm-plat/api/jumpPay.html" + "?openId="+openId+"&clientFlag="+clientFlag+"&sign="+sign
					+ "&sendUrl="+sendUrl+"&keyUrlOrderId="+keyUrlOrderId+"&commodityName="+commodityName+"&totalPrice="+totalPrice+"&orderId="+orderId+"&systemFlag="+systemFlag;
			response.sendRedirect(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*if(updateOrderSellCommon(request,map)){
			//成功则推送模板消息
			createParameterMap(request);
			Map<String,Object> sendMap = new HashMap<String, Object>();
			
			sendMap.put("orderId",paramsMap.get("orderId"));
			sendMap.put("isDelete", 0);
			List<OrderSea> ordList = new ArrayList<OrderSea>();
			ordList = orderSeaService.toSearchOrder(sendMap);
			//获取当前买家的OpenId进行推送
			//String orderId,String openId,String payMoney
			String money = ordList.get(0).getOrder().getPayMoney().toString();
			sendMessage.payMoneySuccess(paramsMap.get("orderId").toString(),ordList.get(0).getOrder().getOpenId(),money);//取消订单模板消息推送
			return true;
		}
		return false;*/
	}
	//待付款中的取消订单  默认为关闭该订单
	@RequestMapping(value="/updateOrderSellQxdd")
	public @ResponseBody boolean updateOrderSellQxdd(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus",10);
		if(updateOrderSellCommon(request,map)){
			//成功则推送模板消息
			createParameterMap(request);
			Map<String,Object> sendMap = new HashMap<String, Object>();
			
			sendMap.put("orderId",paramsMap.get("orderId"));
			sendMap.put("isDelete", 0);
			List<OrderSea> ordList = new ArrayList<OrderSea>();
			ordList = orderSeaService.toSearchOrder(sendMap);
			//获取当前买家的OpenId进行推送
			sendMap.put("openId",ordList.get(0).getOrder().getOpenId());
			sendMessage.cancelOrderNotPayOrderSendMessage(sendMap);//取消订单模板消息推送
			return true;
		}
		return false;
	}
	
	//更改当前的订单状态
	@RequestMapping(value="/updateOrderSell")
	public @ResponseBody boolean updateOrderSell(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus",3);
		return updateOrderSellCommon(request,map);
	}
	
	/*** 订单管理 买家端  ------ 待服务 */
	//取消订单状态更改为待退款
	@RequestMapping(value="/updateOrderSellDfuRefuse")
	public @ResponseBody boolean updateOrderSellDfuRefuse(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus",7);//卖家未接单   更改状态为申请退款  或者状态为 9 取消订单
		return updateOrderSellCommon(request,map);
	}
	/*** 订单管理   ------ 结束 */
	
	/*** 订单管理 买家端  ------ 待确认 */
	//申请退款
	@RequestMapping(value="/updateOrderSellRefund")
	public @ResponseBody boolean updateOrderSellRefund(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus",6);//申请退款按钮需要更改为待退款 状态为6或7
		map.put("applyRefundTime",new Date());//申请退款按钮需要更改为待退款 状态为6或7
		map.put("invalidTime",DateUtil.addDay(new Date(), GlobalConstant.WEEK));//申请退款时增加退款时间 
		return updateOrderSellCommon(request,map);
	}
	//确认收货
	@RequestMapping(value="/updateOrderSellAccept")
	public @ResponseBody boolean updateOrderSellAccept(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		Date date = new Date();
		map.put("confirmTime", date);//存入确认收货时间
		map.put("orderStatus",4);//订单状态为已完成
		return updateOrderSellCommon(request,map);
	}
	
	/***订单管理 买家端  ------ 待退款*/
	//查询当前订单中数据状态  此逻辑只在买家端使用
	@RequestMapping(value="/toSerchAccept")
	public @ResponseBody boolean toSerchAccept(HttpServletRequest request,HttpServletResponse response){
		
		try {
			Thread.sleep(3000); //睡3秒 害怕请求太多
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//1.获取当前用户的OpenId
		String openId = getCurrentOpenId(request); //get  openId
		//根据OpenId获取当前未服务的订单
		Map<String,Object> map = new HashMap<String, Object>();
		//存入当前查询条件
		List<OrderSea> ordList = new ArrayList<OrderSea>();
		map.put("openId",openId);
		map.put("orderStatus",2);//当前订单状态为待接单的
		ordList = orderSeaService.toSearchOrder(map);
		map.put("orderStatus",3);//当前订单状态为服务完成的
		int finishSize = orderSeaService.toSearchOrder(map).size();//当前订单为服务完成的订单
		
		//如果当前对象中的值为空则放入当前订单大小
		if(request.getSession().getAttribute("OrderCount")==null){
			request.getSession().setAttribute("OrderCount",ordList.size());
		}
		if(request.getSession().getAttribute("finishSize")==null){
			request.getSession().setAttribute("finishSize",finishSize);
		}
		
		//若请求为0 则为第一次请求 不刷新 并将条数放入到订单列表中
		//System.out.println("********当前存储订单个数 实时*********"+Integer.parseInt(request.getSession().getAttribute("OrderCount").toString()));
		//System.out.println("********当前查询出的订单个数 实时*********"+ordList.size());
		//若不相等则执行刷新页面操作  
		if(Integer.parseInt(request.getSession().getAttribute("OrderCount").toString())!=ordList.size()){
			request.getSession().setAttribute("OrderCount",ordList.size());
			System.out.println("-----用户编号--"+openId+"------的订单已经被接收----------------");
			return true; 
		}
		if(Integer.parseInt(request.getSession().getAttribute("finishSize").toString())!=finishSize){
			request.getSession().setAttribute("finishSize",finishSize);
			System.out.println("-----用户编号+"+openId+"+-------的服务已完成--------------");
			return true; 
		}
		
		return false;
		
	}
	
	
	
	/**
	 * @param request
	 * @param status 更新表的状态值(订单状态)
	 * @return
	 */
	public  boolean updateOrderSellCommon(HttpServletRequest request,Map<String,Object> map){
		
		List<OrderSea> ordList = new ArrayList<OrderSea>();
		
		//1.获取当前用户的OpenId值
		String openId = getCurrentOpenId(request); //正式版
		//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8g9B";//测试版
		//2.获取订单编号
		createParameterMap(request);
		
		//申请退款时放入orderId 
		if(request.getAttribute("orderId")!=null){
			paramsMap.put("orderId",request.getAttribute("orderId").toString());
		}
		paramsMap.put("openId", openId);
		if("null"==openId&&StringUtils.isBlank(openId)){
			request.setAttribute("msg", "用户OpenId异常！");
			log.error("当前用户OpenId值为空");
			return false;
		}
		if(!paramsMap.containsKey("orderId")){
			request.setAttribute("msg", "订单编号异常！");
			log.error("当前用户订单编号值为空");
			return false;
		}
		//3.查询当前订单的OpenId值
		// map.put("orderId",paramsMap.get("orderId").toString());
		Map<String,Object> temporary = new HashMap<String, Object>();
		 temporary.put("orderId",paramsMap.get("orderId").toString());
		 ordList = orderSeaService.toSearchOrder(temporary);
		//4.判断对应则允许操作，不对应则记录日志跳转回首页
		if(ordList!=null&&ordList.size()>0){
			String openIdOrder = ordList.get(0).getOrder().getOpenId();
			if(openId.equals(openIdOrder)){
				// wxl z增加订单状态更改记录代码
				String nowOpenId = ordList.get(0).getOrder().getSellerOpenId();//买家的OpenId
				paramsMap.put("nowOpenId", nowOpenId);
				paramsMap.put("orderStatus", map.get("orderStatus"));
				boolean b = chatMessageService.insertUpdateSysMeg(paramsMap);//卖家端
				chatMessageService.insertBuyMeg(paramsMap);//买家端
				if (b) {
					map.put("orderId", paramsMap.get("orderId").toString());
					// map.put("payTime", new Date()); 将此方法暂时移植到调用方法的入口
					boolean c = orderSeaService.updateOrderState(map);
					if(map.containsKey("orderStatus")){//没有这个状态表示为删除订单
					if(c && map.get("orderStatus").toString().equals("4")){
						Order order = orderSeaService.getOrderInfo(paramsMap.get("orderId").toString());
						//订单金额
						Double payMoney = order.getPayMoney();
						//分成比例
						Double serviceRatio = order.getServiceRatio();
						//分成金额
						Double money = payMoney*serviceRatio;
						//商家openId
						String sellerOpenId = order.getSellerOpenId();
						//获取商家钱包信息
						Map<String, Object> walletMap = walletService.getWalletByOpenId(sellerOpenId);
						Double balance = 0d;
						if(walletMap != null && walletMap.size() > 0 ){
							balance = (Double)walletMap.get("balance");
						}
						//账户余额
						Double balanceMoney = money+balance;
						//记录表插入记录
						Map<String, Object> recordMap = new HashMap<String, Object>();
						recordMap.put("openId", sellerOpenId);
						recordMap.put("divideOrWithdraw", money);
						recordMap.put("balanceMoney", balanceMoney);
						recordMap.put("state", 0);
						recordMap.put("divideOrWithdrawTime", new Date());
						divideDrawService.addDraw(recordMap);
						//更新钱包
						Map<String, Object> moneyMap = new HashMap<String, Object>();
						moneyMap.put("openId", sellerOpenId);
						moneyMap.put("balanceMoney", balance);
						refundsService.updateWallet(walletMap, moneyMap);
						//增加外交官分账
						diplomate.increaseDiplomatMoney(sellerOpenId, order.getDiplomatMoney(), 
								"0", order.getOrderId().toString());
					}
					return c;
				} else {
					log.error("当前方法出现异常！！");
				}
				}
				
			}
			request.setAttribute("msg", "当前用户异常OpenId！");
			log.error("当前用户OpenId与订单不对应");
		}
		
		return false;
	}
	/**
	 * @param status
	 * @return
	 * 删除当前用户的订单
	 */
	//更改当前的订单状态
	@RequestMapping(value="/deleteOrderSell")
	public @ResponseBody boolean deleteOrderSell(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isDelete",1);//删除当前订单
		return updateOrderSellCommon(request,map);
	}
	
	
	//检查当前订单状态是否属于 超时
	public boolean checkPama(String status){
		
		String[] clamun = {GlobalConstant.ORDER_SUC,GlobalConstant.ORDER_DFK,GlobalConstant.ORDER_QX,
				GlobalConstant.ORDER_DQR,GlobalConstant.ORDER_SXCS,GlobalConstant.ORDER_YQR,GlobalConstant.ORDER_JJTK};
		for (int i = 0; i < clamun.length; i++) {
			
			String string = clamun[i];
			if(string.equals(status)){
				return true;
			}
			
		}
		return false;
	}
	
	//处理List 以key value的形式展示
	public Map<String,String> dealListForOrdDet(List<OrderDetail> orderList){
		
		Map<String,String> result = new HashMap<String, String>();
		for (OrderDetail orderDetail : orderList) {
			 result.put(orderDetail.getTypeName(), orderDetail.getTypeValue());
			 
		}
		return result;
	}
	
	/**
	 * 用于ajax的方式进行区分请求 
	 * 待付款，待确认，待服务，待退款
	 * @param request
	 * @param map
	 * @param mov
	 * @return
	 */
	public List<OrderSea> selectOrderSellCommon(HttpServletRequest request,Map<String,Object> map){
		
		//装入查询条件
		List<OrderSea> ordList = new ArrayList<OrderSea>();
		//1.校验OpenId值
		String openId = getCurrentOpenId(request); //get  openId
		if("null"==openId&&StringUtils.isBlank(openId)){
			log.error("当前用户OpenId值为空");
			return ordList;
		}
		//2.用当前用户的OpenId值在对应的订单表中寻找
		map.put("openId",openId);
		map.put("isDelete","0");
		ordList = orderSeaService.toSearchOrder(map);
		Date date = new Date();
		//3.获取表中的数据状态 提供剩余时间
		for (OrderSea orderSea : ordList) {
			
			//存入当前的订单Id进行查询
			map.put("orderId", orderSea.getOrder().getOrderId().toString());
			//检查当前用户在订单表
			UserBasicinfo  userBasicinfo = homeService.getUserBaseInfoByOpenId(orderSea.getOrder().getSellerOpenId());
			//checkOut 当前订单的相信信息表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			//检查订单状态，存入差异时间
			if(checkPama(orderSea.getOrder().getOrderStatus().toString())){
	
			    String differTime =	DataUtil.subtracDateS(orderSea.getOrder().getInvalidTime(), date);
			    //orderSea.setTimmer(differTime);
			}
			//处理当前List以Key value 形式展示
			orderSea.setMap(dealListForOrdDet(orderList));
			//存放卖家用户信息
			orderSea.setUserBasicinfo(userBasicinfo);
		}
		
		return ordList;
	}
	/**
	 * @param num1 被减数 结束时间
	 * @param num2 减数 当前时间
	 * @return 返回String 类型
	 */
	public static String subtracDateS(Date d1,Date d2){
		
		String timer = "";
		long date = d1.getTime() - d2.getTime();   
	    long day = date / (1000 * 60 * 60 * 24);    
	    long hour = (date / (1000 * 60 * 60) - day * 24);    
	    long min = ((date / (60 * 1000)) - day * 24 * 60 - hour * 60);  
	    long s = (date/1000 - day*24*60*60 - hour*60*60 - min*60);  
	    if(day<=0){
	    	timer=""+hour+"小时"+min+"分"+s+"秒";
	    }else{
	    	timer=""+day+"天"+hour+"小时"+min+"分"+s+"秒";
	    }
	    if(date<=0){
	    	timer="0";
	    }
	    
	    return timer;
	}

	
	
	public int getOrderStatusCount() {
		return orderStatusCount;
	}
	public void setOrderStatusCount(int orderStatusCount) {
		this.orderStatusCount = orderStatusCount;
	}
	
	@RequestMapping(value = "/toRefund")
	public String toRefund(HttpServletRequest request,HttpServletResponse response, Model model) {
		createParameterMap(request);
		String orderId = paramsMap.get("orderId").toString();
		
		//发送模板信息
		paramsMap.put("isDelete", 0);
		sendMessage.applyRefundToSeller(paramsMap);
		sendMessage.applyRefundToBuyers(paramsMap);
		
		String checkStatus = paramsMap.get("checkStatus").toString();
		//增加请求的url路径带参数
		String url = paramsMap.get("url").toString();
				
		Order order = orderSeaService.getOrderInfo(orderId);
		Map<String,Object> map = orderSeaService.getExpenseAmount();
		//获取扣款后的金额;
		double amount = Double.parseDouble(map.get("dic_value").toString());
		List<Map<String,Object>> list = orderSeaService.getBankInfo();
		double refundMoney = order.getPayMoney() - amount;//获取退款的金额
		String jsonList = JSONObject.toJSONString(list);
		
		model.addAttribute("refundMoney", refundMoney);
		model.addAttribute("orderId", orderId);
		model.addAttribute("checkStatus", checkStatus);
		model.addAttribute("jsonList", jsonList);
		model.addAttribute("url", url);
		
		return "wap/xkh_version_2_2/refundInfo";//跳转
	

	}
	
	@Transactional
	@RequestMapping(value = "/addRefundInfo")
	public String addRefundInfo(HttpServletRequest request,HttpServletResponse response, Model model) {
		createParameterMap(request);
		String openId = getCurrentOpenId(request); 		
		//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
		Date date = new Date();
		paramsMap.put("openId", openId);
		paramsMap.put("createTime", date);
		paramsMap.put("orderSourceStatus", "1");
		boolean flag = orderSeaService.addRefundInfo(paramsMap);
		if(flag){//存入订单状态表成功
			
			//根据checkstatus更新当前订单表的状态  方法头应该增加事物
			if(paramsMap.containsKey("checkStatus")){
				
				//当前状态为0表示 未服务时申请退款
				if(paramsMap.get("checkStatus").toString().equals("0")){
					
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("orderStatus",7);//卖家未接单   更改状态为申请退款  或者状态为 9 取消订单
					map.put("applyRefundTime",new Date());//申请退款按钮需要更改为待退款 状态为6或7
					map.put("invalidTime",DateUtil.addDay(new Date(), GlobalConstant.WEEK));//申请退款时增加退款时间
					request.setAttribute("orderId",paramsMap.get("refundsOrderId"));
					if(updateOrderSellCommon(request,map)){
						log.error("订单编号为："+ paramsMap.get("refundsOrderId")+"申请退款未服务");
					}
				}
				//当前状态为4表示 已服务或者服务中申请退款
				if(paramsMap.get("checkStatus").toString().equals("4")){
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("orderStatus",6);//申请退款按钮需要更改为待退款 状态为6或7
					map.put("applyRefundTime",new Date());//申请退款按钮需要更改为待退款 状态为6或7
					map.put("invalidTime",DateUtil.addDay(new Date(), GlobalConstant.WEEK));//申请退款时增加退款时间 
					request.setAttribute("orderId",paramsMap.get("refundsOrderId"));
					if(updateOrderSellCommon(request,map)){
						log.error("订单编号为："+ paramsMap.get("refundsOrderId")+"申请退款已服务或已接单");
					}
				}
				
			}
			
		}
		
		String url = paramsMap.get("url").toString();
		return "redirect:"+url;//跳转

	}
	
	
	
	
	
	
}
