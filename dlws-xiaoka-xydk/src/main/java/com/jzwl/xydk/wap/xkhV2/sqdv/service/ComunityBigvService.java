package com.jzwl.xydk.wap.xkhV2.sqdv.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkhV2.sqdv.dao.ComunityBigvDao;

@Service("comunityBigvService")
public class ComunityBigvService {
	@Autowired
	private ComunityBigvDao comunityBigvDao;
	
	//获取字典资源类型
	public List<Map<String,Object>> getSourceTypeByCode(){
		List<Map<String,Object>> list = comunityBigvDao.getSourceTypeByCode();
		return list;
	}	
	
	//获取字表资源类型
	public List<Map<String,Object>> getSourceTypeBySon(){
		List<Map<String,Object>> list = comunityBigvDao.getSourceTypeBySon();
		return list;
	}	
	
	/*
	 * 添加入驻资源
	 * dxf
	 */
	public boolean addSettleUserVersion3(Map<String, Object> map) {

		return comunityBigvDao.addSettleUser(map);

	}
	
	/*
	 * 修改入驻资源
	 * dxf
	 */
	public boolean upSettleVersion3(Map<String, Object> map) {

		return comunityBigvDao.upSettleVersion3(map);

	}
}
