package com.jzwl.xydk.wap.skillUser.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.dao.SkillUserDao;

@Service("skillUserService")
public class SkillUserService {
	@Autowired
	private SkillUserDao skillUserDao;
	@Autowired
	private HomeService homeService;

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取城市信息 标记：wap 创建人： ln 创建时间： 2016年10月12日
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryCityList() {
		return skillUserDao.queryCityList();
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取省列表信息 创建人： dxf 创建时间： 2017年2月23日 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryProviceListVersion2() {
		return skillUserDao.queryProviceListVersion2();
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获城市列表信息 创建人： dxf 创建时间： 2017年2月23日 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryCityListVersion2(String provnceId) {
		return skillUserDao.queryCityListVersion2(provnceId);
	}

	public List<Map<String, Object>> getSchoolGrade() {
		return skillUserDao
				.getSchoolGrade(GlobalConstant.DIC_TYPE_CODE_SCHOOLLABEL);
	}

	/**
	 * 根据城市获取学校类型列表 dxf
	 * 
	 * @param cityId
	 * @return list
	 */
	public List<Map<String, Object>> querySchollTypeVersion2(String cityId) {
		return skillUserDao.querySchollTypeVersion2(cityId);
	}

	/**
	 * 根据城市获取学校 dxf
	 * 
	 * @param cityId
	 * @return list
	 */
	public List<Map<String, Object>> querySchollByTypeVersion2(String cityId,
			String dcdaId) {
		return skillUserDao.querySchollByTypeVersion2(cityId, dcdaId);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取学校信息 创建人： ln 创建时间： 2016年10月12日 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySchoolList(String cityId) {
		return skillUserDao.querySchoolList(cityId);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：保存技能用户基本信息 创建人： ln 创建时间： 2016年10月12日 标记：wap
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean addSkillUser(Map<String, Object> paramsMap) {

		return skillUserDao.addSkillUser(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据学校ID获取所在城市的ID 创建人： ln 创建时间： 2016年10月17日 标记：wap
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> getCityCodeBySchoolId(
			Map<String, Object> paramsMap) {

		return skillUserDao.getCityCodeBySchoolId(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：保存用户技能信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> addSkillInformation(Map<String, Object> paramsMap) {
		return skillUserDao.addSkillInformation(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：不分页查询技能大类信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryMaxCategoryList() {
		return skillUserDao.queryMaxCategoryList();
	}

	/**
	 * 根据一级分类的id或取二级分类
	 * 
	 * @param fid
	 * @return
	 */
	public List<Map<String, Object>> querySonCategoryList(String fid) {
		return skillUserDao.querySonCategoryList(fid);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：不分页查询技能小类信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryMinCategoryList() {
		return skillUserDao.queryMinCategoryList();
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据技能大类ID获取技能小类列表 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param mId
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryMinCategoryByMaxId(String mId) {
		return skillUserDao.queryMinCategoryByMaxId(mId);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：判断用户是否已经存在 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> isUserExist(String openId) {
		Map<String, Object> map = skillUserDao.isUserExist(openId);
		return map;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取用户技能信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySkillInfo(String id) {
		List<Map<String, Object>> skills = skillUserDao.querySkillInfo(id);
		for(Map<String, Object> info:skills){
			String image = homeService.getImageUrl(String.valueOf(info.get("classValue")),Long.valueOf(String.valueOf(info.get("id"))));
			String price = homeService.getPrice(String.valueOf(info.get("classValue")),Long.valueOf(String.valueOf(info.get("id"))));
			String priceName = homeService.setCompany(String.valueOf(info.get("classValue")),Long.valueOf(String.valueOf(info.get("id"))));
			if(StringUtils.isBlank(priceName)||"null".equals(priceName)){//增加默认值次
				priceName = "次";
			}
			info.put("imageStr", image);
			info.put("price", price);
			info.put("priceName", priceName);
		}
		return skills;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据ID获取子级分类 创建人： ln 创建时间： 2016年10月18日 标记：
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> getMinCategory(Map<String, Object> paramsMap) {

		return skillUserDao.getMinCategory(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：添加图片，技能ID 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param skillId
	 * @param string
	 * @return
	 * @version
	 */
	public boolean addImgBySkillId(String skillId, String img) {

		return skillUserDao.addImgBySkillId(skillId, img);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据技能ID获取技能信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @return
	 * @version
	 */
	public Map<String, Object> getSkillInfoById(String skillId) {
		return skillUserDao.getSkillInfoById(skillId);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据技能ID获取技能图片 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param skillId
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getSkillImgBySkillList(String skillId) {

		return skillUserDao.getSkillImgBySkillList(skillId);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：删除技能信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean deleteSkillInfo(Map<String, Object> paramsMap) {

		return skillUserDao.deleteSkillInfo(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：修改技能信息 创建人： ln 创建时间： 2016年10月19日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean editSkillInfo(Map<String, Object> paramsMap) {

		return skillUserDao.editSkillInfo(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据技能ID删除图片ID 创建人： ln 创建时间： 2016年10月19日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean deleteImgBySkillId(Map<String, Object> paramsMap) {
		return skillUserDao.deleteImgBySkillId(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：修改基本信息 创建人： ln 创建时间： 2016年10月20日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean editBasicInfo(Map<String, Object> paramsMap) {
		return skillUserDao.editBasicInfo(paramsMap);
	}

	public boolean editBasicInfoVersion2(Map<String, Object> paramsMap) {
		return skillUserDao.editBasicInfoVersion2(paramsMap);
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：删除用户上传的技能图片 创建人： ln 创建时间： 2016年11月2日 标记：wap
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	public boolean delImgById(Map<String, Object> paramsMap) {

		return skillUserDao.delImgById(paramsMap);
	}

	/**
	 * 
	 * 获取发布banner图
	 * <p>
	 * 无参数
	 */
	public List<BannerInfo> getSkillBanner() {

		return skillUserDao.getSkillBanner();

	}

	/**
	 * 
	 * 个人信息页面banner图
	 * <p>
	 * 无参数
	 */
	public List<BannerInfo> getUserBanner() {

		return skillUserDao.getUserBanner();

	}

	/**
	 * 
	 * 技能开启，关闭
	 * <p>
	 * 技能id
	 */
	public boolean updateDisplay(Map<String, Object> paramsMap) {

		return skillUserDao.updateDisplay(paramsMap);

	}

	public List<Map<String, Object>> getDicData(String dicType) {
		return skillUserDao.getDicData(dicType);
	}

	public boolean upBackPicture(Map<String, Object> paramsMap) {

		return skillUserDao.upBackPicture(paramsMap);

	}

	public Map<String, Object> getCompanyId(String company) {
		return skillUserDao.getCompanyId(company);
	}

	/**
	 * 
	 * 描述:获取手机端用户的入住信息 作者:gyp
	 * 
	 * @Date 2017年4月17日
	 */
	public List<Map<String, Object>> getUserBasicInfoList() {
		return skillUserDao.getUserBasicInfoList();
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取标签列表信息 创建人： dxf 
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryLabeList() {
		return skillUserDao.queryLabeList();
	}
}
