package com.jzwl.xydk.wap.xkh2_2.ordseach.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.DataUtil;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.skillUser.service.SkillUserManagerService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.business.home.controller.HomeController;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderSea;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderCountService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.service.HighSchoolSkillOrderService;

/**
 * 卖家端订单管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/orderseaSell")
public class OrderSeaSellController extends BaseWeixinController {

	@Autowired
	OrderSeaService orderSeaService;

	@Autowired
	private HomeService homeService;

	@Autowired
	private SkillUserService skillUserService;

	@Autowired
	private SkillUserManagerService skillUserManagerService;

	@Autowired
	private PreferredServiceService preferredServiceService;

	@Autowired
	private HighSchoolSkillOrderService highSchoolSkillOrderService;

	@Autowired
	private ChatMessageService chatMessageService;
	
	@Autowired
	private SendMessageService sendMessage;
	
	@Autowired
	private OrderCountService orderCountService;
	

	Logger log = Logger.getLogger(HomeController.class);

	//订单管理卖家端订单展示
	@RequestMapping(value = "/searchOrderSell")
	public String searchOrderSell(HttpServletRequest request, HttpServletRequest response, Model mov) {

		//获取当前自定义查询条件
		createParameterMap(request);

		//用来计算回显的tab
		if (!paramsMap.containsKey("tabNum") || paramsMap.get("tabNum") == null) {
			paramsMap.put("tabNum", 0);
		}
		//装入查询条件
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> skillImg = new ArrayList<Map<String, Object>>();//存入当前技能图片
		List<OrderSea> ordList = new ArrayList<OrderSea>();
		//1.校验OpenId值
		String openId = getCurrentOpenId(request); //get  openId
		
		orderCountService.upperCount(openId,"seller");
		//String openId = "oHxmUjgSIW36wQbYa6rI3T9SlsW8"; //测试版OpenId
		if ("null" == openId && StringUtils.isBlank(openId)) {
			request.setAttribute("msg", "查询失败！");
			log.error("当前用户OpenId值为空");
			return "/wap/xkh_version_2/index_2-1";
		}
		//2.用当前用户的OpenId值在对应的订单表中寻找
		if (paramsMap.get("orderStatus") != null && StringUtils.isNotBlank(paramsMap.get("orderStatus").toString())) {
			map.put("orderStatus", paramsMap.get("orderStatus").toString());
		}
		map.put("sellerOpenId", openId);
		map.put("isDelete", 0);
		ordList = orderSeaService.toSearchOrder(map);
		Date date = new Date();
		//3.获取表中的数据状态 提供剩余时间
		//list 记录当前索引值
		
		List<Integer> inteList = new ArrayList<Integer>();
		
		int i = 0;
		for (OrderSea orderSea : ordList) {
			
			i++;
			UserBasicinfo userBasicinfo = new UserBasicinfo();
			//System.out.println("当前问题数据OrderID" + orderSea.getOrder().getOrderId());
			
			//标记当前要移除的ID值
			
			
			if(orderSea.getOrder().getEnterType().equals("jztf")){
				// ordList.remove(i);
				inteList.add(i);
				continue;//跳出当前执行下一循环
			}
			
			//存入当前的订单Id进行查询
			map.put("orderId", orderSea.getOrder().getOrderId().toString());
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);

			Map<String, String> mapdetail = dealListForOrdDet(orderList);
			//处理当前List以Key value 形式展示
			orderSea.setMap(mapdetail);

			//查询当前产品的销售数量
			if (mapdetail.containsKey("skillId")) {

				Map<String, Object> skillMap = skillUserService.getSkillInfoById(mapdetail.get("skillId").toString());//用户技能信息
				paramsMap.put("id", mapdetail.get("skillId").toString());
				Map<String, Object> preferredService = preferredServiceService.getById(paramsMap);
				if (!skillMap.isEmpty()) {
					//存入销售技能中的销售数量
					if (skillMap.get("sellNo") == null) {
						skillMap.put("sellNo", "0");
					}
					orderSea.setSellNo(skillMap.get("sellNo").toString());
				} else if (!preferredService.isEmpty()) {

					if (preferredService.get("salesVolume") == null) {
						skillMap.put("salesVolume", "0");
					}
					orderSea.setSellNo(preferredService.get("salesVolume").toString());
				} else {
					orderSea.setSellNo("0");
					System.out.println("暂时未查询到当前订单的ID值用0 进行代替");
				}

			}

			//处理当前的订单单价进行展示 存入订单单价
			if (mapdetail.containsKey("skillPrice")) {
				orderSea.setSingPrice(mapdetail.get("skillPrice"));
			}
			if (mapdetail.containsKey("begin") && mapdetail.containsKey("end")) {
				String singlePrice = mapdetail.get("begin") + "~" + mapdetail.get("end");
				orderSea.setSingPrice(singlePrice);
			}

			//checkOut 当前订单的相信信息表
			//检查订单状态，存入差异时间
			if (checkPama(orderSea.getOrder().getOrderStatus().toString())) {

				String differTime = subtracDateS(orderSea.getOrder().getInvalidTime(), date);
				orderSea.getOrder().setTimeDiffer(differTime);//存入差异时间
				orderSea.setTimmer(orderSea.getOrder().getInvalidTime().getTime());//获取当前date时间
			}

			//存放卖家用户信息
			orderSea.setUserBasicinfo(userBasicinfo);
			
			//1.获取当前卖家用户的OpenId
			String openIdCurrent = orderSea.getOrder().getOpenId();
			//2.查询出当前用户的头像和昵称信息
			Map<String,Object> userInfo =  homeService.getPersonInform(openIdCurrent);
			
			String headImgUrl ="http://xiaoka2016.oss-cn-qingdao.aliyuncs.com/201703231957014158228eb1d0744a7bcfa3f96a0aa87cc0113f.jpeg";//微信头像（默认）
			String wxname = "猛虎";//微信昵称（默认）
			
			//3.放到封装中的对象当中
			//微信头像
			if(userInfo.containsKey("headImgUrl")){
				if(userInfo.get("headImgUrl")!=null&&StringUtils.isNotBlank(userInfo.get("headImgUrl").toString())){
					headImgUrl = userInfo.get("headImgUrl").toString();//微信头像 
				}
			}
			//微信昵称
			if(userInfo.containsKey("headImgUrl")){
				if(userInfo.get("headImgUrl")!=null&&StringUtils.isNotBlank(userInfo.get("headImgUrl").toString())){
					 wxname = userInfo.get("wxname").toString(); //微信昵称
				}
			}
			
			//4.覆盖当前要展示的map中的值
			mapdetail.put("sellerHeadPortrait", headImgUrl);
			mapdetail.put("sellerNickname", wxname);
			
			
		}
		//处理当前的ordList
		for (Integer integer : inteList) {
			ordList.remove(integer);//移除精准咨询的list
		}
		//返回参数展示
		mov.addAttribute("ordList", ordList);
		mov.addAttribute("tabNum", paramsMap.get("tabNum"));//用来区分当前Tab页
		return "wap/xkh_version_2_2/seller_list";//跳转
	}

	//更改当前的订单状态
	@RequestMapping(value = "/updateOrderSell")
	public @ResponseBody boolean updateOrderSell(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus", 3);
		return updateOrderSellCommon(request, map);
	}

	/*** 订单管理 卖家端  ------ 待服务 */
	//拒绝订单
	@RequestMapping(value = "/updateOrderSellDfuRefuse")
	public @ResponseBody boolean updateOrderSellDfuRefuse(HttpServletRequest request, HttpServletResponse response) {
		
		createParameterMap(request);
		boolean flage = false;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus",11);//拒绝订单 
		if(updateOrderSellCommon(request, map)){
			//更新退款表的状态  checkStatus 更改为0同意退款  来源为0 卖家拒绝订单
			//flage = orderSeaService.updateRefundInfo(paramsMap.get("orderId").toString(), "0", "0");
			if(flage){
				paramsMap.put("isDelete", 0);
				sendMessage.refuseAcceptOrder(paramsMap);
			}
		}
		return flage;
	}

	//接收订单
	@RequestMapping(value = "/updateOrderSellAccept")
	public @ResponseBody boolean updateOrderSellAccept(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		Date now = new Date();
		map.put("orderStatus", 0);//已接单
		map.put("receiveTime", now);//存入当前接单时间
		if(updateOrderSellCommon(request, map)){
			//成功则推送模板消息
			createParameterMap(request);
			Map<String,Object> sendMap = new HashMap<String, Object>();
			
			sendMap.put("orderId",paramsMap.get("orderId"));
			sendMap.put("isDelete", 0);
			List<OrderSea> ordList = new ArrayList<OrderSea>();
			ordList = orderSeaService.toSearchOrder(sendMap);
			//获取当前买家的OpenId进行推送
			sendMap.put("openId",ordList.get(0).getOrder().getOpenId());
			sendMessage.acceptOrder(sendMap);
			return true;
		}
		return false;
	}
	/*** 订单管理 卖家端  ------ 待确认 */
	//完成服务
	@RequestMapping(value = "/updateOrderFinish")
	public @ResponseBody boolean updateOrderFinish(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus", 3);//订单状态更改为已完成确认买家收货
		map.put("isFinish", 1);//更改当前订单状态为完成
		Date date = new Date();
		map.put("finishTime",date);
		map.put("invalidTime",DateUtil.addDay(date, GlobalConstant.WEEK));
		return updateOrderSellCommon(request, map);
	}
	
	/*** 订单管理 卖家端  ------ 待退款 */
	//拒绝退款
	@RequestMapping(value = "/updateOrderSellReject")
	public @ResponseBody boolean updateOrderSellReject(HttpServletRequest request, HttpServletResponse response) {
		createParameterMap(request);
		boolean flage = false;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus", 12);
		map.put("invalidTime", DateUtil.addDay(new Date(), GlobalConstant.WEEK));//更新订单超时时间
		//0：表示拒绝接单的退款，1：表示用户申请退款的
		if(updateOrderSellCommon(request, map)){
			//更新退款表的状态  checkStatus 更改为3卖家拒绝  来源为1
			flage = orderSeaService.updateRefundInfo(paramsMap.get("orderId").toString(), "3", "1");
			if(flage){
				paramsMap.put("isDelete", "0");
				sendMessage.refuseRefundToBuyers(paramsMap);//同意退款发送模板信息
			}
			
		}
		
		return flage;
	}

	//同意退款
	@RequestMapping(value = "/updateOrderSellAgree")
	public @ResponseBody boolean updateOrderSellAgree(HttpServletRequest request, HttpServletResponse response) {createParameterMap(request);
		boolean flage = false;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orderStatus", 7);//订单状态更改为待退款
		if(updateOrderSellCommon(request, map)){
			//更新退款表的状态  checkStatus 更改为0卖家同意退款  来源为1
			flage = orderSeaService.updateRefundInfo(paramsMap.get("orderId").toString(), "0", "1");
			if(flage){
				paramsMap.put("isDelete", "0");
				sendMessage.refuseRefundToBuyers(paramsMap);//拒绝退款发送模板信息
			}
		}
		
		return flage;
	}
	
	
	
	/**
	 * @param request
	 * @param status 更新表的状态值(订单状态)
	 * @return
	 */
	public synchronized boolean updateOrderSellCommon(HttpServletRequest request, Map<String, Object> map) {

		List<OrderSea> ordList = new ArrayList<OrderSea>();

		//1.获取当前用户的OpenId值
		String openId = getCurrentOpenId(request); //正式版
		//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8g9B";//测试版
		//2.获取订单编号
		createParameterMap(request);
		if ("null" == openId && StringUtils.isBlank(openId)) {
			request.setAttribute("msg", "用户OpenId异常！");
			log.error("当前用户OpenId值为空");
			return false;
		}
		if (!paramsMap.containsKey("orderId")) {
			request.setAttribute("msg", "订单编号异常！");
			log.error("当前用户订单编号值为空");
			return false;
		}
		//3.查询当前订单的OpenId值
		// map.put("sellerOpenId",paramsMap.get("sellerOpenId").toString());
		Map<String, Object> temporary = new HashMap<String, Object>();
		temporary.put("orderId", paramsMap.get("orderId").toString());
		ordList = orderSeaService.toSearchOrder(temporary);

		//4.判断对应则允许操作，不对应则记录日志跳转回首页
		if (ordList != null && ordList.size() > 0) {
			String openIdOrder = ordList.get(0).getOrder().getSellerOpenId();
			if (openId.equals(openIdOrder)) {
				//执行更新状态
				//wxl-补充-将状态修改的信息存放在:newsstaffchat 系统消息    根据orderId 查系统信息接受人(买家)的openId
				String nowOpenId = ordList.get(0).getOrder().getOpenId();
				paramsMap.put("nowOpenId", nowOpenId);
				paramsMap.put("orderStatus", map.get("orderStatus"));
				boolean b = chatMessageService.insertUpdateSysMeg(paramsMap);
				if (b) {
					map.put("orderId", paramsMap.get("orderId").toString());
					return orderSeaService.updateOrderState(map);
				} else {
					System.out.println("出错了！！！！！！！！！！！！");
				}
			}
			request.setAttribute("msg", "当前用户异常OpenId！");
			log.error("当前用户OpenId与订单不对应");
		}

		return false;
	}

	/**
	 * @param status
	 * @return
	 * 删除当前用户的订单
	 */
	//更改当前的订单状态
	@RequestMapping(value = "/deleteOrderSell")
	public @ResponseBody boolean deleteOrderSell(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isDelete", 1);//删除当前订单
		return updateOrderSellCommon(request, map);
	}

	//检查当前订单状态是否属于 超时
	public boolean checkPama(String status) {

		String[] clamun = { GlobalConstant.ORDER_SUC, GlobalConstant.ORDER_DFK, GlobalConstant.ORDER_QX, GlobalConstant.ORDER_DQR,
				GlobalConstant.ORDER_SXCS, GlobalConstant.ORDER_YQR,GlobalConstant.ORDER_JJTK};
		for (int i = 0; i < clamun.length; i++) {

			String string = clamun[i];
			if (string.equals(status)) {
				return true;
			}

		}
		return false;
	}

	//处理List 以key value的形式展示
	public Map<String, String> dealListForOrdDet(List<OrderDetail> orderList) {

		Map<String, String> result = new HashMap<String, String>();
		for (OrderDetail orderDetail : orderList) {
			result.put(orderDetail.getTypeName(), orderDetail.getTypeValue());

		}
		return result;
	}

	/**
	 * 用于ajax的方式进行区分请求 
	 * 待付款，待确认，待服务，待退款
	 * @param request
	 * @param map
	 * @param mov
	 * @return
	 */
	public List<OrderSea> selectOrderSellCommon(HttpServletRequest request, Map<String, Object> map) {

		//装入查询条件
		List<OrderSea> ordList = new ArrayList<OrderSea>();
		//1.校验OpenId值
		String openId = getCurrentOpenId(request); //get  openId
		if ("null" == openId && StringUtils.isBlank(openId)) {
			log.error("当前用户OpenId值为空");
			return ordList;
		}
		//2.用当前用户的OpenId值在对应的订单表中寻找
		map.put("openId", openId);
		map.put("isDelete", "0");
		ordList = orderSeaService.toSearchOrder(map);
		Date date = new Date();
		//3.获取表中的数据状态 提供剩余时间
		for (OrderSea orderSea : ordList) {

			//存入当前的订单Id进行查询
			map.put("orderId", orderSea.getOrder().getOrderId().toString());
			//检查当前用户在订单表
			UserBasicinfo userBasicinfo = homeService.getUserBaseInfoByOpenId(orderSea.getOrder().getSellerOpenId());
			//checkOut 当前订单的相信信息表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			//检查订单状态，存入差异时间
			if (checkPama(orderSea.getOrder().getOrderStatus().toString())) {

				String differTime = DataUtil.subtracDateS(orderSea.getOrder().getInvalidTime(), date);
				//orderSea.setTimeDiffer(differTime);
			}
			//处理当前List以Key value 形式展示
			orderSea.setMap(dealListForOrdDet(orderList));
			//存放卖家用户信息
			orderSea.setUserBasicinfo(userBasicinfo);
		}

		return ordList;
	}

	/**
	 * @param num1 被减数 结束时间
	 * @param num2 减数 当前时间
	 * @return 返回String 类型
	 */
	public String subtracDateS(Date d1, Date d2) {

		String timer = "";
		long date = d1.getTime() - d2.getTime();
		long day = date / (1000 * 60 * 60 * 24);
		long hour = (date / (1000 * 60 * 60) - day * 24);
		long min = ((date / (60 * 1000)) - day * 24 * 60 - hour * 60);
		long s = (date / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		System.out.println("" + day + "天" + hour + "小时" + min + "分" + s + "秒");
		timer = "" + day + "天" + hour + "小时" + min + "分"+ s + "秒";

		return timer;
	}

}


