/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.utils.MapUtil;
import com.jzwl.xydk.wap.business.home.dao.HomeDao;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.pojo.OrderSkill;
import com.jzwl.xydk.wap.business.home.pojo.Special;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillImage;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassF;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassS;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillinfo;
import com.jzwl.xydk.wap.business.home.pojo.VideoImg;
import com.jzwl.xydk.wap.xkh2_2.pay.controller.UpdateOrderController;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;


@Service("homeService")
public class HomeService {
	
	@Autowired
	private HomeDao homeDao;
	
	@Autowired
	private Constants constants;
	
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	
	private static Logger log = LoggerFactory
			.getLogger(UpdateOrderController.class);
	
	public List<BannerInfo> getBannerInfoList(){
		
		List<BannerInfo> list = homeDao.getBannerInfoList();
		
		return list;
	}
	
	public List<BannerInfo> getBannerInfoListVersion3(int type){
		
		List<BannerInfo> list = homeDao.getBannerInfoListVersion3(type);
		
		return list;
	}
	//获取一级分类的接口
	public List<UserSkillclassF> findUserSkillclassF(){
		
		List<UserSkillclassF> list = homeDao.findUserSkillclassF();
		
		return list;
	}
	//获取入住用户的总数
	public Map<String,Object> getUserAllNum(){
		
		Map<String,Object> map = homeDao.getUserAllNum();
		
		return map;
	}
	/**
	 * 
	 * 描述:获取用户的发布的所有技能的总数
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public Map<String,Object> getUserSkillAllNum(){
		
		Map<String,Object> map = homeDao.getUserSkillAllNum();
		
		return map;
	}
	
	
	//获取最新的入住用户，
	public List<UserBasicinfo> findNewUserBasicinfo(){
		
		List<UserSkillinfo> skills = null;//入主用户技能的集合
		
		Map<String,Object> map = homeDao.getDataValueByType(GlobalConstant.DIC_TYPE_CODE_NEWNUM);
		String num = map.get("dicValue").toString(); 
		
		List<UserBasicinfo> list = homeDao.findNewUserBasicinfo(num);
		
		/*for(UserBasicinfo userInfo:list){
			//获取入主用户的技能的集合
			List<UserSkillImage> images = null;
			if(null != list){
				skills = homeDao.getUserSkillInfos(userInfo.getId().toString());
				images = homeDao.getSkillImages(skills.get(0).getId().toString());
			}	
			if(userInfo.getOpenId().equals("0")){//pc端添加
				userInfo.setImageUrl(images.get(0).getImgUrl());
			}else{
				userInfo.setImageUrl(images.get(0).getImgUrl());
			}
		}*/
		
		
		return list;
	}
	
	//获取优质资源的接口
	public List<Special> getSpecial(){
		
		Map<String,Object> map = homeDao.getSpecialNum();
		String num = map.get("dicValue").toString(); 
		List<Special> list = homeDao.getSpecial(num);
		
		return list;
	}
	
	//获取优质资源的接口
	public List<Map<String,Object>> getDataByTypeCode(String str){
		
		List<Map<String,Object>> list = homeDao.getDataByTypeCode(str);
		return list;
	}	
	
	//获取入住用户
	public List<UserBasicinfo> findUserBasicinfo(Map<String,Object> map){
		
		List<UserBasicinfo> list = homeDao.findUserBasicinfo(map);
		
		return list;
	}
	
	
	
	public UserBasicinfo getUserBasicInfo(Map<String,Object> map){
		
		UserBasicinfo userInfo = homeDao.getUserInfo(map);
		return userInfo;
	}
	
	public UserBasicinfo getUserBasicInfo(String id){
		
		UserBasicinfo userInfo = homeDao.getUserInfo(id);
		return userInfo;
	}
	/**
	 * 获取用户的信息的二级页面
	 * @param map
	 * @return
	 */
	public UserBasicinfo getUserInfo(Map<String,Object> map){
		
		List<UserSkillinfo> skills = null;//入主用户技能的集合
		
		UserBasicinfo userInfo = homeDao.getUserInfo(map);
		
		
		if(null != userInfo && userInfo.getOpenId().equals("0")){
			//获取用户的头像
			Map<String,Object> user = homeDao.getInfo(userInfo.getId());
			//userInfo.setHeadImgUrl(user.get("headImgUrl").toString());
			skills = homeDao.getUserSkillInfos(userInfo.getId().toString());
			for(UserSkillinfo skill:skills){//获取技能的高清图片的集合
				List<UserSkillImage> images = homeDao.getSkillImages(skill.getId().toString());
				for(UserSkillImage image:images){
					image.setImgUrl(image.getImgUrl());
				}
				skill.setImages(images);
			
			}
		}else{
			//获取入主用户的技能的集合
			if(null != userInfo){
				skills = homeDao.getUserSkillInfos(userInfo.getId().toString());
				for(UserSkillinfo skill:skills){//获取技能的高清图片的集合
					List<UserSkillImage> images = homeDao.getSkillImages(skill.getId().toString());
					for(UserSkillImage image:images){
						image.setImgUrl(image.getImgUrl());
					}
					skill.setImages(images);
				}
			}
		}
		if(null != userInfo){
			
			userInfo.setSkillInfos(skills);
			//用户的浏览量+1
			Map<String,Object> m = new HashMap<String,Object>();
			m.put("viewNum", userInfo.getViewNum()+1);
			m.put("id", userInfo.getId());
			homeDao.updateViewNum(m);
		}
		return userInfo;
	}
	/*
	 * dxf
	 * 用户个人信息 自己查自己
	 * @return UserBasicinfo
	 * @param map openid
	 */
	public UserBasicinfo getUserInfoselfVersion2(Map<String,Object> map){
		UserBasicinfo userInfo = homeDao.getUserInfoselfVersion2(map);
		return userInfo;
	}
	
	/**
	 * get分类的子级
	 * @param fid
	 * @return
	 */
	public List<UserSkillclassS> getSkillClassS(Map<String,Object> map){
		
		List<UserSkillclassS> list = homeDao.getSkillClassS(map);
		
		return list;
	}	
	
	/**
	 * version2
	 * dxf
	 * 二级分类
	 * @param fid
	 * @return
	 */
	public List<UserSkillclassS> getSkillClassSVersion2(Map<String,Object> map){
		
		List<UserSkillclassS> list = homeDao.getSkillClassS(map);
		
		return list;
	}	
	
	/*
	 * 根据字典查出线上线下服务
	 * dxf
	 * @return list
	 */
	public List<Map<String,Object>> getServiceTypeVersion2(){
		
		List<Map<String,Object>> list = homeDao.getServiceTypeVersion2();
		
		return list;
	}
	/*
	 * 热搜词列表
	 * @dxf
	 * @return list
	 * 
	 */
	public List<Map<String,Object>> getHotWordsTypeVersion2(){
		
		List<Map<String,Object>> list = homeDao.getHotWordVersion2();
		
		return list;
	}
	
	/**
	 * 211,985 院校查询
	 * dxf
	 */
	public List<Map<String,Object>> getScholTypeListVersion2(){
			
			List<Map<String,Object>> list = homeDao.getScholTypeListVersion2();
			
			return list;
	}
	/*
	 * 优化资源列表
	 * dxf
	 * @return List
	 */
	public PageObject queryOptimizaVersion2(Map<String, Object> map) {
		return homeDao.queryOptimizaVersion2(map);

	}
	/**
	 * 创建人：gyp
	 * 创建时间：2017年3月20日
	 * 描述：获取土建到首页的技能信息列表
	 * @param map
	 * @return
	 */
	public List<UserSkillinfo> getPromoteSkillInfoList(Map<String, Object> map){
		
		List<UserSkillinfo> skills = homeDao.getPromoteSkillInfoList(map);
		for(UserSkillinfo info:skills){
			String image = getImageUrl(info.getClassValueF(),info.getId());
			String setPrice = getPrice(info.getClassValueF(),info.getId());
			String setCom = setCompany(info.getClassValueF(),info.getId());
			info.setImage(image);
			info.setPriceStr(setPrice);
			info.setCompanyStr(setCom);
		}
		return skills;
	}
	
	/**
	 * 搜索执行
	 * 获取入住用户----分页
	 * @param map
	 * @return
	 */
	public List<UserSkillinfo> getUserSkillInfo(Map<String, Object> map) {
		
		List<UserSkillinfo> skills = homeDao.getUserSkillInfoList(map);
		for(UserSkillinfo info:skills){
			String image = getImageUrl(info.getClassValueF(),info.getId());
			String setPrice = getPrice(info.getClassValueF(),info.getId());
			String setCom = setCompany(info.getClassValueF(),info.getId());
			info.setImage(image);
			info.setPriceStr(setPrice);
			info.setCompanyStr(setCom);
		}
		return skills;
	}
	
	/**
	 * 
	 * 描述:获取用户的信息，用来做搜索------实现搜大咖的功能
	 * 作者:gyp
	 * @Date	 2017年6月1日
	 */
	public List<UserBasicinfo> getBasicUserInfo(Map<String, Object> map) {
		List<UserBasicinfo> users = homeDao.getBasicUserInfo(map);
		return users;
	}
	
	/**
	 * 
	 * 描述:得带单位 
	 * 作者:gyp
	 * @Date	 2017年5月20日
	 */
	public String setCompany(String classValue,Long skillId){
		Map<String,Object> map = null;
		String str = "";
		if(classValue.equals(GlobalConstant.ENTER_TYPE_XNSJ)){
			return "次";
		}else{
			map = homeDao.getCompanyValue(skillId, "company");
			str = String.valueOf(map.get("dic_name"));
			return str;
		}
	}
	
	/**
	 * 
	 * 描述:获取技能的价格
	 * 作者:gyp
	 * @Date	 2017年5月17日
	 */
	public String getPrice(String classValue,Long skillId){
		Map<String,Object> map = null;
		String str = "";
		if(classValue.equals(GlobalConstant.ENTER_TYPE_XNSJ)){
			map = homeDao.getTypeValue(skillId, "cooperaType");
			str = map.get("typeValue").toString();
			Map<String,Object> maps = (Map<String,Object>)JSON.parse(str);  
			String min = MapUtil.getMinValue(maps)+"";
			String max = MapUtil.getMaxValue(maps)+"";
			return min+"~"+max;
		}else if(classValue.equals(GlobalConstant.ENTER_TYPE_SQDV)){
			//其他
			Map<String,Object> pamater = new HashMap<String, Object>();
			pamater.put("pid",skillId);
			pamater.put("isDelete", 0);
			pamater = personInfoDetSer.getuserSkillinfoDetail(pamater);
			//如果为其他则返回skillPrice 价格
			if(!pamater.containsKey("lessLinePrice")||StringUtils.isBlank(pamater.get("lessLinePrice").toString())){
				return pamater.get("skillPrice").toString();
			}
			//当前为社群大V自媒体
			Map<String,Object> map_less = homeDao.getTypeValue(skillId, "lessLinePrice");//获取次条价格
			Map<String,Object> map_top = homeDao.getTypeValue(skillId, "topLinePrice");//获取头条价格
			String str_less = map_less.get("typeValue") == null?"null":map_less.get("typeValue").toString();
			String str_top = map_top.get("typeValue") == null?"null": map_top.get("typeValue").toString();
			return str_less+"~"+str_top;
		}else{
			map = homeDao.getTypeValue(skillId, "skillPrice");
			str = map.get("typeValue").toString();
			return str;
		}
	}
	/**
	 * 
	 * 描述:返回图片
	 * 作者:gyp
	 * @Date	 2017年5月16日
	 */
	public String getImageUrl(String classValue,Long skillId){
		String[] images = null;
		Map<String,Object> map = null;
		String imgUrl = "";
		if(classValue.equals(GlobalConstant.ENTER_TYPE_GXST)){
			map = homeDao.getTypeValue(skillId, "activtyUrl");
			if(null == map.get("typeValue")){
				imgUrl = "http://xiaoka2016.oss-cn-qingdao.aliyuncs.com/20170327111947248903tf.jpg";
			}else{
				imgUrl = map.get("typeValue").toString();
			}
			if(null == imgUrl){
				log.info("******************************二级分类的照片为空GXST*********************************");
			}else{
				images = imgUrl.split(",");
			}
		}else if(classValue.equals(GlobalConstant.ENTER_TYPE_JNDR)){
			map = homeDao.getTypeValue(skillId, "activtyUrl");
			if(null == map.get("typeValue")){
				imgUrl = "http://xiaoka2016.oss-cn-qingdao.aliyuncs.com/20170327111947248903tf.jpg";
			}else{
				imgUrl = map.get("typeValue").toString();
			}
			if(null == imgUrl){
				log.info("******************************二级分类的照片为空JNDR*********************************");
			}else{
				images = imgUrl.split(",");
			}
		}else if(classValue.equals(GlobalConstant.ENTER_TYPE_CDZY)){
			map = homeDao.getTypeValue(skillId, "allPhoto");
			if(null == map.get("typeValue")){
				imgUrl = "http://xiaoka2016.oss-cn-qingdao.aliyuncs.com/20170327111947248903tf.jpg";
			}else{
				imgUrl = map.get("typeValue").toString();
			}
			if(null == imgUrl){
				log.info("******************************二级分类的照片为空CDZY*********************************");
			}else{
				images = imgUrl.split(",");
			}
			
		}else if(classValue.equals(GlobalConstant.ENTER_TYPE_XNSJ)){
			map = homeDao.getTypeValue(skillId, "allPhoto");
			if(null == map.get("typeValue")){
				imgUrl = "http://xiaoka2016.oss-cn-qingdao.aliyuncs.com/20170327111947248903tf.jpg";
			}else{
				imgUrl = map.get("typeValue").toString();
			}
			if(null == imgUrl){
				log.info("******************************二级分类的照片为空XNSJ*********************************");
			}else{
				images = imgUrl.split(",");
			}
			
		}else if(classValue.equals(GlobalConstant.ENTER_TYPE_SQDV)){
			map = homeDao.getTypeValue(skillId, "activtyUrl");
			
			if(null == map.get("typeValue")){
				imgUrl = "http://xiaoka2016.oss-cn-qingdao.aliyuncs.com/20170327111947248903tf.jpg";
			}else{
				imgUrl = map.get("typeValue").toString();
			}
			if(null == imgUrl){
				log.info("******************************二级分类的照片为空SQDV*********************************");
			}else{
				images = imgUrl.split(",");
			}
		}else{
			log.info("++++++++++++++++++++++++++++++没有该入住的类型+++++++++++++++++++++++++++++++++++++++++");
		}
		
		return images[0];
	}
	
	
	
	//优质服务详情
	public Map<String,Object> getPreInfoById(Map<String,Object> map){
		return homeDao.getPreInfoById(map);
	}
	//获取技能接口   ----  
	public UserSkillinfo getSkillInfo(Map<String,Object> map){
		
		UserSkillinfo info = homeDao.getSkillInfo(map);
		//List<UserSkillImage> images = null;
		List<VideoImg> videos = new ArrayList<VideoImg>();
		List<VideoImg> opus = new ArrayList<VideoImg>();
		
		/*if(info.getOpenId().equals("0")){
			
			Map<String,Object> mapPC = homeDao.getInfo(info.getBasicId());
			info.setHeadImgUrl(mapPC.get("headImgUrl").toString());
			
			images = homeDao.getSkillImages(info.getId().toString());
			for(UserSkillImage image:images){
				image.setImgUrl(image.getImgUrl());
			}
		}else{
			images = homeDao.getSkillImages(info.getId().toString());
			for(UserSkillImage image:images){
				image.setImgUrl(image.getImgUrl());
			}
		}*/
		
		List<UserSkillImage> images = homeDao.getSkillImages(info.getId().toString());
		for(UserSkillImage image:images){
			image.setImgUrl(image.getImgUrl());
		}
		
		info.setImages(images);	
		
		
		//用户的浏览量+1
		Map<String,Object> m = new HashMap<String,Object>();
		m.put("viewNum", info.getViewNum()+1);
		m.put("id", info.getId());
		homeDao.updateSkillViewNum(m);
		
		
		//判断其他是否有其他作品
		if(StringUtils.isNotEmpty(info.getTextUrl())){//有其他作品
			//根据“,”号进行分割
			String[] otherOpus = info.getTextUrl().split(",");
			for(int i=0;i<otherOpus.length;i++){
				VideoImg vi = new VideoImg();
				vi.setOtherUrl(otherOpus[i]);
				String s= com.jzwl.common.utils.StringUtil.numToUpper(i+1);
				vi.setOpusName(s);
				opus.add(vi);
			}
			info.setOpus(opus);
		}
		
		if(StringUtils.isNotEmpty(info.getVideoUrl())){//有视频
			
			//根据“,”号进行分割
			String[] urls = info.getVideoUrl().split(",");
			for(int i=0;i<urls.length;i++){
				VideoImg vi = new VideoImg();
				vi.setImgUrl(info.getImages().get(i).getImgUrl());
				vi.setVideoUrl(urls[i]);
				videos.add(vi);
			}
			info.setVideoImg(videos);
			info.setFlag(1);
			return info;
		}else{//显示图片
			info.setFlag(3);
			return info;
		}
	}
	
	/**
	 * 根据OpenId获取用户的头像和
	 * 是否入驻的基本信息
	 * @param openId
	 * @return
	 */
	public Map<String,Object> getCustomerByOpenId(String openId){
		
		return homeDao.getHeadimgurl(openId);
	}
	
	/**
	 * 根据OpenId获取用户的头像和昵称
	 * @param openId
	 * @return
	 */
	public Map<String,Object> getPersonInform(String openId){
		
		return homeDao.getPersonInform(openId);
	}
	
	
	/**
	 * 根据OpenId获取当前的用户在
	 * 个人空间一级页面预览页中的信息
	 * @param openId
	 * @return
	 */
	public UserBasicinfo getUserBaseInfoByOpenId(String openId){
		
		return homeDao.getUserinfoByOpenId(openId);
	}
	public Map<String,Object> getByopenId(Map<String, Object> map){
		return homeDao.getByopenId(map);
	}
	public boolean upPersonInform(Map<String, Object> map) {
		return homeDao.upPersonInform(map);
	}
	
	/**
	 * 创建人：cfz
	 * 时间：2017年2月22日14:40:52
	 * @param id 用户ID
	 * @return
	 */
	public String getSkillIdById(String id){
		
		return homeDao.getSkillIdById(id);
	}
	/**
	 * 创建人：cfz
	 * 获取更多专题资源分页查询
	 * 时间：2017年2月22日17:26:39
	 * @param begin 初始条数
	 * @param end	中止条数
	 * @return
	 */
	public List<Special> getMoreSpecial(Map<String,Object>map){
		
		return homeDao.getMoreSpecial(map);
	}
	
	/**
	 * 创建人：cfz
	 * 获取当前表的总条数
	 * 时间：2017年2月22日17:43:41
	 * @param tableName 表名
	 * @return
	 */
	public int getTableCount(String tableName){
		
		return homeDao.getTableCount(tableName);
	}
	
	/**
	 * 创建人：cfz
	 * 获取更多大咖的信息
	 * 时间：2017年2月22日20:29:01
	 * @return
	 */
	public List<UserBasicinfo> findMorePeople(Map<String,Object> map){
		
		List<UserBasicinfo> list = homeDao.findMorePeople(map);
		/*for(UserBasicinfo userInfo:list){
			//获取入主用户的技能的集合
			List<UserSkillImage> images = null;
			List<UserSkillinfo> skills = null;
			if(null != list){
				skills = homeDao.getUserSkillInfos(userInfo.getId().toString());
				if(skills.size()>0){
					images = homeDao.getSkillImages(skills.get(0).getId().toString());
					*//**将SkillInfos 和 images重新set回原来的地址*//*
					userInfo.setImages(images);
					userInfo.setSkillInfos(skills);
					
					if(userInfo.getOpenId().equals("0")){//pc端添加
						userInfo.setImageUrl(images.get(0).getImgUrl());
					}else{
						
						userInfo.setImageUrl(images.get(0).getImgUrl());
					}
				}
				
			}
		}*/
		return list;
	}
	
	/**
	 * 创建人：cfz
	 * 获取复合最新大咖的总的人数
	 * 时间：2017年2月22日20:29:01
	 * @return
	 */
	public int getCountPeople(){
		
		return homeDao.getCountPeople();
	}
	
	/**
	 * 创建人：cfz
	 * 判断当前用户是否已经入驻 链表 userBasicinfo 和 userskillinfo
	 * 时间：2017年2月22日20:29:01
	 * @return
	 */
	public boolean getIsExist(String basicId){
		
		return homeDao.getIsExist(basicId);
	}
	
	/**
	 * 创建人：cfz
	 * 更改点赞的状态
	 * 时间：2017年2月27日15:44:49
	 * @return
	 */
	public boolean getIsagree(String basicId){
		
		return homeDao.getIsExist(basicId);
	}
	
	/**
	 * 创建人：cfz
	 * 更改观看记录的状态
	 * 时间：2017年2月27日15:44:49
	 * @return
	 */
	public boolean toAddViewNum(String id){
		
		return homeDao.toAddViewNum(id);
	}
	
	/**
	 * 创建人：cfz
	 * 更改观看记录的状态
	 * 时间：2017年2月27日15:44:49
	 * @return
	 */
	public String toShowJump(String id){
		return homeDao.toShowJump(id);
	}
	
	/**
	 * 
	 * @param flage true 表示点赞 ，false 表示取消赞
	 * @param id 表示资源的ID值
	 * @return
	 */
	public boolean toAddIsagree(boolean flage ,String id){
		return homeDao.toAddIsagree(flage,id);
	}
	
	/**
	 * 搜索执行
	 * @param map
	 * @return
	 */
	public List<UserSkillinfo> serchSkill(Map<String, Object> map) {
		
		List<UserSkillinfo> skills = homeDao.getUserSkillInfoList(map);
		for(UserSkillinfo info:skills){
			
			List<UserSkillImage> images = homeDao.getSkillImages(info.getId().toString());
			
			if(info.getOpenId().equals("0")){
				
				
				Map<String,Object> mapPC = homeDao.getInfo(info.getBasicId());
				info.setHeadImgUrl(mapPC.get("headImgUrl").toString());
				
				for(UserSkillImage image:images){
					image.setImgUrl(image.getImgUrl());
				}
				info.setImages(images);	
			}else{
				for(UserSkillImage image:images){
					image.setImgUrl(image.getImgUrl());
				}
				info.setImages(images);	
			}
		}
		return skills;
	}
	/**
	 * 订单列表
	 * dxf
	 * return List
	 */
	public List<OrderSkill> orderList(Map<String,Object> map){
		List<OrderSkill> orderList = homeDao.orderList(map);
		
		for(OrderSkill info:orderList){
			List<UserSkillinfo> skill = homeDao.getSkillList(info.getId().toString());
			info.setSkill(skill);
		}
		return orderList;
		
	}
	
}
