/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2017
 */

package com.jzwl.xydk.wap.xkh2_2.ordseach.pojo;

import com.jzwl.system.base.pojo.BasePojo;


public class Order extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "Order";
	public static final String ALIAS_ORDER_ID = "订单的id";
	public static final String ALIAS_OPEN_ID = "下单人的openId";
	public static final String ALIAS_SELLER_OPEN_ID = "卖家的openId";
	public static final String ALIAS_SCHOOL_ID = "学校的id";
	public static final String ALIAS_CITY_ID = "城市的id";
	public static final String ALIAS_CREATE_DATE = "下单时间";
	public static final String ALIAS_PAY_MONEY = "付款金额";
	public static final String ALIAS_PAY_TIME = "付款时间";
	public static final String ALIAS_SERVICE_MONEY = "服务端应得金额";
	public static final String ALIAS_DIPLOMAT_MONEY = "外交官应得比例";
	public static final String ALIAS_PLATFORM_MONEY = "平台应付金额";
	public static final String ALIAS_SERVICE_RATIO = "服务端的分成比例";
	public static final String ALIAS_DIPLOMAT_RATIO = "外交官的分成比例";
	public static final String ALIAS_PLATFORM_RATIO = "平台的分成比例";
	public static final String ALIAS_REALITY_MONEY = "实际付款金额";
	public static final String ALIAS_ORDER_STATUS = "1：下单成功，2：支付成功（也就是待服务）、0：已接单、11：拒绝接单、 3：待确认（已服务）、 4：已确认（已完成），5：超时失效， 6：申请退款、7：待退款、8：退款成功 9 取消订单、10已关闭";
	public static final String ALIAS_PAYWAY = "0:微信支付、1：其他";
	public static final String ALIAS_CANCEL_TIME = "订单取消时间";
	public static final String ALIAS_INVALID_TIME = "订单超时失效时间";
	public static final String ALIAS_CONFIRM_TIME = "确认时间";
	public static final String ALIAS_REFUND_TIME = "退款时间";
	public static final String ALIAS_LINKMAN = "联系人";
	public static final String ALIAS_PHONE = "联系电话";
	public static final String ALIAS_MESSAGE = "买家留言信息";
	public static final String ALIAS_START_DATE = "开始时间";
	public static final String ALIAS_END_DATE = "结束时间";
	public static final String ALIAS_IS_DELETE = "是否删除";
	public static final String ALIAS_ENTER_TYPE = "订单的服务类型";
	public static final String ALIAS_PARTNER_TRADE_NO = "partnerTradeNo";
	
	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	public static final String FORMAT_PAY_TIME = DATE_FORMAT;
	public static final String FORMAT_CANCEL_TIME = DATE_FORMAT;
	public static final String FORMAT_INVALID_TIME = DATE_FORMAT;
	public static final String FORMAT_CONFIRM_TIME = DATE_FORMAT;
	public static final String FORMAT_REFUND_TIME = DATE_FORMAT;
	public static final String FORMAT_START_DATE = DATE_FORMAT;
	public static final String FORMAT_END_DATE = DATE_FORMAT;
	

	//columns START
    /**
     * 订单的id       db_column: orderId 
     */ 	
	private java.lang.Long orderId;
    /**
     * 下单人的openId       db_column: openId 
     */ 	
	private String pid;
	/**
	 * 商家下单时的父Id
	 */
	private java.lang.String openId;
    /**
     * 卖家的openId       db_column: sellerOpenId 
     */ 	
	private java.lang.String sellerOpenId;
    /**
     * 学校的id       db_column: schoolId 
     */ 	
	private java.lang.Long schoolId;
    /**
     * 城市的id       db_column: cityId 
     */ 	
	private java.lang.Long cityId;
    /**
     * 下单时间       db_column: createDate 
     */ 	
	private java.util.Date createDate;
    /**
     * 付款金额       db_column: payMoney 
     */ 	
	private Double payMoney;
    /**
     * 付款时间       db_column: payTime 
     */ 	
	private java.util.Date payTime;
    /**
     * 服务端应得金额       db_column: serviceMoney 
     */ 	
	private Double serviceMoney;
    /**
     * 外交官应得比例       db_column: diplomatMoney 
     */ 	
	private Double diplomatMoney;
    /**
     * 平台应付金额       db_column: platformMoney 
     */ 	
	private Double platformMoney;
    /**
     * 服务端的分成比例       db_column: serviceRatio 
     */ 	
	private Double serviceRatio;
    /**
     * 外交官的分成比例       db_column: diplomatRatio 
     */ 	
	private Double diplomatRatio;
    /**
     * 平台的分成比例       db_column: platformRatio 
     */ 	
	private Double platformRatio;
    /**
     * 实际付款金额       db_column: realityMoney 
     */ 	
	private Double realityMoney;
    /**
     * 1：下单成功，2：支付成功（也就是待服务）、0：已接单、11：拒绝接单、 3：待确认（已服务）、 4：已确认（已完成），5：超时失效， 6：申请退款、7：待退款、8：退款成功 9 取消订单、10已关闭       db_column: orderStatus 
     */ 	
	private java.lang.Integer orderStatus;
    /**
     * 0:微信支付、1：其他       db_column: payway 
     */ 	
	private java.lang.Integer payway;
    /**
     * 订单取消时间       db_column: cancelTime 
     */ 	
	private java.util.Date cancelTime;
    /**
     * 订单超时失效时间       db_column: invalidTime 
     */ 	
	private java.util.Date invalidTime;
    /**
     * 确认时间       db_column: confirmTime 
     */ 	
	private java.util.Date confirmTime;
    /**
     * 退款时间       db_column: refundTime 
     */ 	
	private java.util.Date refundTime;
    /**
     * 联系人       db_column: linkman 
     */ 	
	private java.lang.String linkman;
    /**
     * 联系电话       db_column: phone 
     */ 	
	private java.lang.String phone;
    /**
     * 买家留言信息       db_column: message 
     */ 	
	private java.lang.String message;
    /**
     * 开始时间       db_column: startDate 
     */ 	
	private java.util.Date startDate;
    /**
     * 结束时间       db_column: endDate 
     */ 	
	private java.util.Date endDate;
    /**
     * 是否删除       db_column: isDelete 
     */ 	
	private java.lang.Integer isDelete;
    /**
     * 订单的服务类型       db_column: enterType 
     */ 	
	private java.lang.String enterType;
    /**
     * partnerTradeNo       db_column: partnerTradeNo 
     */ 	
	private java.lang.String partnerTradeNo;
	/**
	 * 配合当前的订单总额钱数
	 */
	private String paySum;
	
	/**
	 * 根据不同时间的时间差值
	 * @param value
	 */
	private String timeDiffer;
	
	
	
	public String getPaySum() {
		return paySum;
	}

	public void setPaySum(String paySum) {
		this.paySum = paySum;
	}

	public String getTimeDiffer() {
		return timeDiffer;
	}

	public void setTimeDiffer(String timeDiffer) {
		this.timeDiffer = timeDiffer;
	}

	public void setOrderId(java.lang.Long value) {
		this.orderId = value;
	}
	
	public java.lang.Long getOrderId() {
		return this.orderId;
	}
	

	public java.lang.String getOpenId() {
		return this.openId;
	}
	
	public void setOpenId(java.lang.String value) {
		this.openId = value;
	}
	

	public java.lang.String getSellerOpenId() {
		return this.sellerOpenId;
	}
	
	public void setSellerOpenId(java.lang.String value) {
		this.sellerOpenId = value;
	}
	

	public java.lang.Long getSchoolId() {
		return this.schoolId;
	}
	
	public void setSchoolId(java.lang.Long value) {
		this.schoolId = value;
	}
	

	public java.lang.Long getCityId() {
		return this.cityId;
	}
	
	public void setCityId(java.lang.Long value) {
		this.cityId = value;
	}
	

	public String getCreateDateString() {
		return dateToStr(getCreateDate(), FORMAT_CREATE_DATE);
	}
	public void setCreateDateString(String value) {
		setCreateDate(strToDate(value, java.util.Date.class,FORMAT_CREATE_DATE));
	}

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

	public Double getPayMoney() {
		return this.payMoney;
	}
	
	public void setPayMoney(Double value) {
		this.payMoney = value;
	}
	

	public String getPayTimeString() {
		return dateToStr(getPayTime(), FORMAT_PAY_TIME);
	}
	public void setPayTimeString(String value) {
		setPayTime(strToDate(value, java.util.Date.class,FORMAT_PAY_TIME));
	}

	public java.util.Date getPayTime() {
		return this.payTime;
	}
	
	public void setPayTime(java.util.Date value) {
		this.payTime = value;
	}
	

	public Double getServiceMoney() {
		return this.serviceMoney;
	}
	
	public void setServiceMoney(Double value) {
		this.serviceMoney = value;
	}
	

	public Double getDiplomatMoney() {
		return this.diplomatMoney;
	}
	
	public void setDiplomatMoney(Double value) {
		this.diplomatMoney = value;
	}
	

	public Double getPlatformMoney() {
		return this.platformMoney;
	}
	
	public void setPlatformMoney(Double value) {
		this.platformMoney = value;
	}
	

	public Double getServiceRatio() {
		return this.serviceRatio;
	}
	
	public void setServiceRatio(Double value) {
		this.serviceRatio = value;
	}
	

	public Double getDiplomatRatio() {
		return this.diplomatRatio;
	}
	
	public void setDiplomatRatio(Double value) {
		this.diplomatRatio = value;
	}
	

	public Double getPlatformRatio() {
		return this.platformRatio;
	}
	
	public void setPlatformRatio(Double value) {
		this.platformRatio = value;
	}
	

	public Double getRealityMoney() {
		return this.realityMoney;
	}
	
	public void setRealityMoney(Double value) {
		this.realityMoney = value;
	}
	

	public java.lang.Integer getOrderStatus() {
		return this.orderStatus;
	}
	
	public void setOrderStatus(java.lang.Integer value) {
		this.orderStatus = value;
	}
	

	public java.lang.Integer getPayway() {
		return this.payway;
	}
	
	public void setPayway(java.lang.Integer value) {
		this.payway = value;
	}
	

	public String getCancelTimeString() {
		return dateToStr(getCancelTime(), FORMAT_CANCEL_TIME);
	}
	public void setCancelTimeString(String value) {
		setCancelTime(strToDate(value, java.util.Date.class,FORMAT_CANCEL_TIME));
	}

	public java.util.Date getCancelTime() {
		return this.cancelTime;
	}
	
	public void setCancelTime(java.util.Date value) {
		this.cancelTime = value;
	}
	

	public String getInvalidTimeString() {
		return dateToStr(getInvalidTime(), FORMAT_INVALID_TIME);
	}
	public void setInvalidTimeString(String value) {
		setInvalidTime(strToDate(value, java.util.Date.class,FORMAT_INVALID_TIME));
	}

	public java.util.Date getInvalidTime() {
		return this.invalidTime;
	}
	
	public void setInvalidTime(java.util.Date value) {
		this.invalidTime = value;
	}
	

	public String getConfirmTimeString() {
		return dateToStr(getConfirmTime(), FORMAT_CONFIRM_TIME);
	}
	public void setConfirmTimeString(String value) {
		setConfirmTime(strToDate(value, java.util.Date.class,FORMAT_CONFIRM_TIME));
	}

	public java.util.Date getConfirmTime() {
		return this.confirmTime;
	}
	
	public void setConfirmTime(java.util.Date value) {
		this.confirmTime = value;
	}
	

	public String getRefundTimeString() {
		return dateToStr(getRefundTime(), FORMAT_REFUND_TIME);
	}
	public void setRefundTimeString(String value) {
		setRefundTime(strToDate(value, java.util.Date.class,FORMAT_REFUND_TIME));
	}

	public java.util.Date getRefundTime() {
		return this.refundTime;
	}
	
	public void setRefundTime(java.util.Date value) {
		this.refundTime = value;
	}
	

	public java.lang.String getLinkman() {
		return this.linkman;
	}
	
	public void setLinkman(java.lang.String value) {
		this.linkman = value;
	}
	

	public java.lang.String getPhone() {
		return this.phone;
	}
	
	public void setPhone(java.lang.String value) {
		this.phone = value;
	}
	

	public java.lang.String getMessage() {
		return this.message;
	}
	
	public void setMessage(java.lang.String value) {
		this.message = value;
	}
	

	public String getStartDateString() {
		return dateToStr(getStartDate(), FORMAT_START_DATE);
	}
	public void setStartDateString(String value) {
		setStartDate(strToDate(value, java.util.Date.class,FORMAT_START_DATE));
	}

	public java.util.Date getStartDate() {
		return this.startDate;
	}
	
	public void setStartDate(java.util.Date value) {
		this.startDate = value;
	}
	

	public String getEndDateString() {
		return dateToStr(getEndDate(), FORMAT_END_DATE);
	}
	public void setEndDateString(String value) {
		setEndDate(strToDate(value, java.util.Date.class,FORMAT_END_DATE));
	}

	public java.util.Date getEndDate() {
		return this.endDate;
	}
	
	public void setEndDate(java.util.Date value) {
		this.endDate = value;
	}
	

	public java.lang.Integer getIsDelete() {
		return this.isDelete;
	}
	
	public void setIsDelete(java.lang.Integer value) {
		this.isDelete = value;
	}
	

	public java.lang.String getEnterType() {
		return this.enterType;
	}
	
	public void setEnterType(java.lang.String value) {
		this.enterType = value;
	}
	

	public java.lang.String getPartnerTradeNo() {
		return this.partnerTradeNo;
	}
	
	public void setPartnerTradeNo(java.lang.String value) {
		this.partnerTradeNo = value;
	}
	
	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

}

