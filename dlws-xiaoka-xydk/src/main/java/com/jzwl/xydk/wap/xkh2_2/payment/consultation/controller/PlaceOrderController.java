/**
 * PlaceOrder.java
 * com.jzwl.xydk.wap.xkh2_2.payment.consultation.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.payment.consultation.controller;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.sign.AccessCore;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_2.payment.consultation.service.PlaceOrderService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.service.SkillOrderService;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.controller.FeedBackController;

/**
 * 下单方法
 * @author   wxl
 * @Date	 2017年4月11日 	 
 */
@Controller("placeOrder")
@RequestMapping("/placeOrder")
public class PlaceOrderController extends BaseWeixinController {

	@Autowired
	private PlaceOrderService placeOrderService;

	@Autowired
	private SkillUserService skillUserService;

	@Autowired
	private PreferredServiceService preferredServiceService;

	@Autowired
	private UserBasicinfoService userBasicinfoService;

	@Autowired
	private SkillOrderService skillOrderService;
	
	@Autowired
	OrderSeaService orderSeaService;
	
	@Autowired
	private SendMessageService sendMessage;
	
	@Autowired
	private Constants contants; 
	
	@Autowired
	private FeedBackController  feedBackController;

	/**
	 * 由页面拿到优选服务所属的openid   和 优选服务id   
	 * 
	 * 去支付页面
	 * 分别可以从优选服务--立即咨询
	 * 			已选列表--结算进入
	 *	实际参数：skillId--期间用到卖家和买家的openid
	 *	实现添加需要的参数较多
	 *	将 需要的参数1.skillId  2.myOpenId
	 *		放到paramsMap里面---放置页面是hidden域
	 *		
	 * @param request
	 * @param response
	 * @param model
	 * 
	 * 点击立即支付
	 * 1，去支付（先不做）
	 * 2，根据返回值（判断存储）
	 * 3，根据存储的返回值（判断进行查询）
	 * 
	 * 查出来需要的信息直接添加 到订单
	 * 
	 */
	@RequestMapping("/toPlaceOrder")
	public void toPlaceOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";//用户自己的openid
		
		System.out.println("============精准咨询立即付款============openId==========="+openId);
		Map<String, Object> skillBaseInfo = new HashMap<String, Object>();
		Map<String, Object> myBaseInfo = new HashMap<String, Object>();
		Map<String, Object> selBaseInfo = new HashMap<String, Object>();
		Map<String,Object> signMap = new HashMap<String,Object>();
		try {
			//买家基本信息
			myBaseInfo = skillUserService.isUserExist(openId);// 用户基本信息
			//精准咨询--优选服务基本信息
			paramsMap.put("id", paramsMap.get("skillId"));
			paramsMap.put("enterId", myBaseInfo.get("enterId"));
			skillBaseInfo = preferredServiceService.getById(paramsMap);
			//Map<String, Object> enterType = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);

			//卖家基本信息
			paramsMap.put("selOpenId", paramsMap.get("sellerOpenId"));//页面获取卖家的openId
			selBaseInfo = placeOrderService.getSelBaseInfo(paramsMap);
			//添加订单信息
	
			//if (null != enterType && null != skillBaseInfo && null != myBaseInfo && null != selBaseInfo) {
			if (null != skillBaseInfo && null != myBaseInfo && null != selBaseInfo) {
				Map<String, Object> mapParams = paramMap(skillBaseInfo, myBaseInfo, selBaseInfo,openId);
				
				//保留参数
				String money = mapParams.get("payMoney").toString();
				
				//添加订单
				String isSuccess = placeOrderService.insertOrder(mapParams);
				//将服务表已售数量加一
				boolean isRight = placeOrderService.addBuyNum(paramsMap);
				
				if ("0" != isSuccess && isRight){
					
					model.addAttribute("msg", "下单成功");
					paramsMap.put("orderId", isSuccess);
					paramsMap.put("isDelete", "0");
					List<OrderDetail> orderList = orderSeaService.searchOrderDetail(paramsMap);
					Map<String,String> skillInfo =  dealListForOrdDet(orderList);
					//String openId = "o3P2SwabLBk80n1IdUBzkI3tnFN4";
					String commodityName = skillInfo.get("skillName").toString();
					String totalPrice = money;
					String orderId = isSuccess;
					String clientFlag = "xkh";
					String sendUrl = "http://xydk.xiaoka360.com/updatePay/payOldSuccess.html";
					String keyUrlOrderId = "wxPay_xkh|http://xydk.xiaoka360.com/updatePay/paySuccess.html|"+orderId;
					String systemFlag = "xkh";
					signMap.put("openId",openId);
					signMap.put("commodityName",commodityName);
					signMap.put("totalPrice",totalPrice);
					signMap.put("orderId",orderId);
					signMap.put("clientFlag",clientFlag);
					signMap.put("sendUrl",sendUrl);
					signMap.put("keyUrlOrderId",keyUrlOrderId);
					commodityName = URLEncoder.encode(commodityName);
					String sign = AccessCore.sign(signMap, contants.getWx_signKey());//拿到签名数据
					String url = "http://klgj.xiaoka360.com/wm-plat/api/jumpPay.html" + "?openId="+openId+"&clientFlag="+clientFlag+"&sign="+sign
							+ "&sendUrl="+sendUrl+"&keyUrlOrderId="+keyUrlOrderId+"&commodityName="+commodityName+"&totalPrice="+totalPrice+"&orderId="+orderId+"&systemFlag="+systemFlag;
					response.sendRedirect(url);
					//不用发送模板信息
					//sendMessage.payMoneySuccess(isSuccess,openId,money);
				}else{
					model.addAttribute("msg", "下单失败");
				}
				//return "redirect:/placeOrder/orderDetail.html?orderId=" + isSuccess;//跳到订单详情页面
			} else {
				//return "有map是空的，请核对一下！";
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			//return "redirect:/home/index.html";//跳转到首页 
		}
	}
	
	//处理List 以key value的形式展示
	public Map<String,String> dealListForOrdDet(List<OrderDetail> orderList){
			
			Map<String,String> result = new HashMap<String, String>();
			for (OrderDetail orderDetail : orderList) {
				 result.put(orderDetail.getTypeName(), orderDetail.getTypeValue());
				 
			}
			return result;
	}

	public Map<String, Object> paramMap(Map<String, Object> skillBaseInfo, Map<String, Object> myBaseInfo,
			Map<String, Object> selBaseInfo,String openId) {

		Map<String, Object> myParamsMap = new HashMap<String, Object>();
		try {
			//入驻类型
			//myParamsMap.put("enterType", enterType.get("typeValue"));
			myParamsMap.put("enterType", GlobalConstant.ENTER_TYPE_JZZX);
			//优选服务信息
			myParamsMap.put("skillId", skillBaseInfo.get("id"));
			myParamsMap.put("imgUrl", skillBaseInfo.get("imgUrl"));
			myParamsMap.put("skillName", skillBaseInfo.get("serviceName"));
			myParamsMap.put("serviceType", skillBaseInfo.get("serviceType"));
			myParamsMap.put("skillDepict", skillBaseInfo.get("introduce"));
			myParamsMap.put("payMoney", skillBaseInfo.get("price"));
			/**代码订单管理使用传参*/
			//myParamsMap.put("dic_name", "");//单位当前页面中传入的参数
			myParamsMap.put("skillPrice", skillBaseInfo.get("price"));
			myParamsMap.put("tranNumber", 1);
			myParamsMap.put("sellPhone", myBaseInfo.get("phoneNumber"));//卖家电话号码
			myParamsMap.put("dic_name", "次");//确定单位类型
			
			
			
			//	myParamsMap.put("salesVolume", (int) skillBaseInfo.get("salesVolume") + 1);
			myParamsMap.put("serviceMoney", (double) skillBaseInfo.get("price") * (double) skillBaseInfo.get("serviceRatio"));
			myParamsMap.put("diplomatMoney", (double) skillBaseInfo.get("price") * (double) skillBaseInfo.get("diplomatRatio"));
			myParamsMap.put("platformMoney", (double) skillBaseInfo.get("price") * (double) skillBaseInfo.get("platformRatio"));
			myParamsMap.put("serviceRatio", skillBaseInfo.get("serviceRatio"));
			myParamsMap.put("diplomatRatio", skillBaseInfo.get("diplomatRatio"));
			myParamsMap.put("platformRatio", skillBaseInfo.get("platformRatio"));
			myParamsMap.put("realityMoney", skillBaseInfo.get("price"));
			//买家信息
//			myParamsMap.put("openId", myBaseInfo.get("openId"));
//			myParamsMap.put("linkman", myBaseInfo.get("nickname"));
//			myParamsMap.put("phone", myBaseInfo.get("phoneNumber"));
			myParamsMap.put("openId", openId);
			myParamsMap.put("linkman", selBaseInfo.get("nickname"));
			myParamsMap.put("phone", selBaseInfo.get("phoneNumber"));
			//卖家信息
			myParamsMap.put("sellerOpenId", selBaseInfo.get("openId"));
			myParamsMap.put("sellerHeadPortrait", selBaseInfo.get("headPortrait"));
			myParamsMap.put("sellerNickname", selBaseInfo.get("nickname"));
			myParamsMap.put("schoolId", selBaseInfo.get("schoolId"));
			myParamsMap.put("cityId", selBaseInfo.get("cityId"));
		} catch (Exception e) {
			System.out.println("有个map是空的！！！！！");
			e.printStackTrace();
		}

		return myParamsMap;
	}

	/**
	 * 
	 * 支付方法
	 * 理论需要两部，1.调用微信支付接口，2.实现在订单表进行插入数据
	 * 实际参数见   sql
	 * @param request
	 * @param response
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping("/insertOrder")
	public String insertOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			String isSuccess = placeOrderService.insertOrder(paramsMap);
			if ("0" != isSuccess)
				model.addAttribute("msg", "下单成功");
			else
				model.addAttribute("msg", "下单失败");
			return "redirect:/placeOrder/orderDetail.html?orderId=" + isSuccess;//跳到订单详情页面
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";//跳转到首页 
		}
	}

	/**
	 * 
	 * 支付成功以后跳转 订单详情
	 *	orderId
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping("/orderDetail")
	public String orderDetail(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		
		boolean isMySelf = false;
		String currentOpenId = getCurrentOpenId(request);
		
		//4.增加查看当前Id值是否存在反馈记录
		model = feedBackController.feedBackCommon(paramsMap,model);
		
		Map<String, Object> orderDetail = new HashMap<String, Object>();
		try {
			orderDetail = placeOrderService.getOrderDetail(paramsMap);
			if (null != orderDetail) {
				List<Map<String, Object>> orderData = placeOrderService.getOrderDetailData(paramsMap);
				for (int i = 0; i < orderData.size(); i++) {
					String skillid = orderData.get(i).get("typeName").toString();
					if ("skillId".equals(skillid)) {
						String skillId = orderData.get(i).get("typeValue").toString();
						paramsMap.put("id", skillId);
						Map<String, Object> preferredService = preferredServiceService.getById(paramsMap);
						model.addAttribute("preferredService", preferredService);
					}
				}
				if(currentOpenId.equals(orderDetail.get("openId"))){
					isMySelf = true;
				}
				Date invalidTime = (Date) orderDetail.get("invalidTime");
				long timmer = invalidTime.getTime();
				DateUtil du = new DateUtil();
				String invalid = du.subtracDateS(invalidTime, new Date());
				
				//区分用户点击
				model.addAttribute("isMySelf", isMySelf);
				model.addAttribute("invalidTime", invalid);
				model.addAttribute("orderDetail", orderDetail);
				model.addAttribute("orderData", orderData);
				model.addAttribute("phone", orderDetail.get("phone"));
				model.addAttribute("timmer", timmer);
			} else {
				request.setAttribute("msg", "查询失败！");
				model.addAttribute("orderDetail", null);
			}
			return "wap/xkh_version_2_2/orderdetaile/jzzx_order";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";//跳转到首页 
		}

	}

	/**
	 * 
	 * 根具orderId改订单状态
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping("/giveUpOrder")
	public String giveUpOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			String isSuccess = placeOrderService.updateOrderStatus(paramsMap);
			if ("0" != isSuccess) {
				model.addAttribute("msg", "取消成功");
			} else {
				model.addAttribute("msg", "取消失败!");
			}
			return "redirect:/placeOrder/orderDetail.html?orderId=" + isSuccess;
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";//跳转到首页 
		}

	}
}
