/**
 * HighSchoolSkillCcontroller.java
 * com.jzwl.xydk.wap.xkhV2.highschoolskill.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/
package com.jzwl.xydk.wap.xkhV2.highschoolskill.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.json.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.page.PageObject;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkhV2.highschoolskill.service.HighSchoolSkillService;

/**
 * 高校社团，技能达人controller
 * <p>
 * @author   wxl
 * @Date	 2017年3月15日 	 
 */
@Controller("highSchoolSkill")
@RequestMapping("/highSchoolSkill")
public class HighSchoolSkillCcontroller extends BaseWeixinController {

	@Autowired
	private HighSchoolSkillService highSchoolSkillService;

	@Autowired
	private UserBasicinfoService userBasicinfoService;

	@Autowired
	private PreferredServiceService preferredServiceService;

	@Autowired
	private SkillUserService skillUserService;

	@Autowired
	private HomeService homeService;

	@RequestMapping("/getBasicInfo")
	public ModelAndView getHighSchoolSkillInfo(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();
		createParameterMap(request);

		try {
			String id = String.valueOf(paramsMap.get("id"));//基本信息id
			paramsMap.put("id", id);
			//查询基本信息
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息
			mov.addObject("objMap", objMap);

			boolean flage = true;
			String openId = getCurrentOpenId(request);
			UserBasicinfo userInfo = homeService.getUserInfo(paramsMap);
			/**区分通过对比openId关联的ID  CFZ begin*/
			if (userInfo != null) {
				if (openId.equals(userInfo.getOpenId())) {
					flage = false;
				}
			}
			mov.addObject("flage", flage);
			mov.addObject("userInfo", userInfo);

			//查询所属城市
			String cityId = String.valueOf(objMap.get("cityId"));
			Map<String, Object> cityMap = userBasicinfoService.getCity(cityId);//查询所属省
			mov.addObject("cityMap", cityMap);
			//查询所属学校
			String sholId = String.valueOf(objMap.get("schoolId"));
			Map<String, Object> sholMap = userBasicinfoService.getSholName(sholId);
			mov.addObject("sholMap", sholMap);
			//查询子表list
			List<Map<String, Object>> settleList = userBasicinfoService.querySettleListVersion3(paramsMap);

			String imgString = "";//活动图片
			for (int i = 0; i < settleList.size(); i++) {
				if (settleList.size() > 0) {
					if ("activtyUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						imgString = String.valueOf(settleList.get(i).get("typeValue"));
						objMap.put("imgString", imgString);
					}
				}
			}
			//活动图片
			if (imgString != null && !"".equals(imgString)) {
				String[] imgArr = imgString.split(",");
				mov.addObject("imgArr", imgArr);
			}
			mov.addObject("settleList", settleList);
			//判断入驻类型
			String enterId = String.valueOf(objMap.get("enterId"));
			if (enterId == null || "".equals(enterId) || "null".equals(enterId)) {
				enterId = String.valueOf(paramsMap.get("enterId"));
				paramsMap.put("enterId", enterId);
			} else {
				paramsMap.put("enterId", enterId);
			}
			Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
			String typeValue = String.valueOf(setypeMap.get("typeValue"));
			mov.addObject("typeValue", typeValue);
			//查询优选服务
			PageObject preferrServer = new PageObject();
			paramsMap.put("enterClass", enterId);
			preferrServer = preferredServiceService.queryPreferredServiceList(paramsMap);
			if (preferrServer.getDatasource().size() > 0) {
				String jsonPreferClass = Json.toJson(preferrServer.getDatasource());//优选服务转json
				String preferList = "{\"preferList\":" + jsonPreferClass + "}";
				mov.addObject("preferList", preferList);
			} else {
				mov.addObject("preferSize", preferrServer.getDatasource().size());
				mov.addObject("preferList", 1);
			}

			//服务/技能列表
			Map<String, Object> m1 = new HashMap<String, Object>();
			List<Map<String, Object>> skillInfoList = skillUserService.querySkillInfo(id);
			if (skillInfoList.size() > 0) {
				//存在服务信息----->使用模板显示服务信息
				for (int i = 0; i < skillInfoList.size(); i++) {

					String str = skillInfoList.get(i).get("details").toString();
					if (str.length() > 30) {
						String strdetails = str.substring(0, 30) + "...";
						skillInfoList.get(i).put("details", strdetails);
					} else {
						String strdetails = str;
						skillInfoList.get(i).put("details", strdetails);
					}
					String skillId = String.valueOf(skillInfoList.get(i).get("id"));
					List<Map<String, Object>> listSkillImgInfo = skillUserService.getSkillImgBySkillList(skillId);
					if (listSkillImgInfo.size() > 0) {
						// 获取技能轮播图
						skillInfoList.get(i).put("SkillImgsLength", listSkillImgInfo.size());
						skillInfoList.get(i).put("SkillImgs", listSkillImgInfo);
					}
				}
				m1.put("skillsList", skillInfoList);
			} else {
				mov.addObject("skillsSize", skillInfoList.size());
				mov.addObject("skillsList", 1);
			}
			String jsonskillInfoList = Json.toJson(m1);
			//String skillsList = "{\"skillsList\":" + jsonskillInfoList + "}";
			mov.addObject("skillsList", jsonskillInfoList);
			mov.setViewName("/wap/xkh_version_2/person_skill");//跳转到个人空间一级页面
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("redirect:/home/index.html");//跳转到首页  
		}

		return mov;

	}

	/**
	 * 添加UserBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addSettleUser")
	public String addSettleUser(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			boolean flag = userBasicinfoService.addSettleUserVersion3(paramsMap);
			if (flag) {
				model.addAttribute("msg", "添加成功");
				return "redirect:/home/getmyInform.html";
			} else {
				model.addAttribute("msg", "添加失败");
				return "redirect:/home/index.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "添加失败");
			return "redirect:/home/index.html";

		}

	}

	//回显

	/**
	 * 跳转到修改页面
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/toUpdateSettledUser")
	public String toUpdateSettledUser(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			//获取banner图
			//获取技能达人
			List<BannerInfo> jnImg = homeService.getBannerInfoListVersion3(5);//轮播图
			if (jnImg.size() > 0) {
				model.addAttribute("jnImg", jnImg.get(0));
			} else {
				model.addAttribute("jnImg", null);
			}
			//获取高校社团
			List<BannerInfo> gxImg = homeService.getBannerInfoListVersion3(6);//轮播图
			if (gxImg.size() > 0) {
				model.addAttribute("gxImg", gxImg.get(0));
			} else {
				model.addAttribute("gxImg", null);
			}

			//获取省列表
			List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
			model.addAttribute("provnceList", provnceList);
			Map<String, Object> objMap = userBasicinfoService.getById(paramsMap);//详细信息
			model.addAttribute("objMap", objMap);
			String cityId = String.valueOf(objMap.get("cityId"));
			Map<String, Object> proMap = userBasicinfoService.getProvence(cityId);//查询所属省
			model.addAttribute("proMap", proMap);

			String province = String.valueOf(proMap.get("province"));
			List<Map<String, Object>> cityList = userBasicinfoService.queryCityByPro(province);//根据省查出市
			model.addAttribute("cityList", cityList);

			List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表

			for (int i = 0; i < provnceList.size(); i++) {
				String provnce = String.valueOf(provnceList.get(i).get("province"));
				Map<String, Object> map = new HashMap<String, Object>();
				// 获取市信息
				List<Map<String, Object>> CityByProvnce = skillUserService.queryCityListVersion2(provnce);
				if (CityByProvnce != null && CityByProvnce.size() > 0) {
					map.put("value", provnce);// 市
					map.put("text", provnce);
					map.put("children", CityByProvnce);
				}
				proList.add(map);
			}
			String jsonProList = JSONObject.toJSONString(proList);
			model.addAttribute("proList", jsonProList);

			String sholId = String.valueOf(objMap.get("schoolId"));
			Map<String, Object> sholMap = userBasicinfoService.getSholName(sholId);//查询所属学校

			model.addAttribute("sholMap", sholMap);
			//获取学校类型
			Map<String, Object> sholType = highSchoolSkillService.getSholType(sholId);
			model.addAttribute("sholType", sholType);
			List<Map<String, Object>> settleList = userBasicinfoService.querySettleListVersion3(paramsMap);//查询子表list

			String imgString = "";//活动图片
			String imgStringId = "";
			for (int i = 0; i < settleList.size(); i++) {
				if (settleList.size() > 0) {
					if ("activtyUrl".equals(String.valueOf(settleList.get(i).get("typeName")))) {
						imgString = String.valueOf(settleList.get(i).get("typeValue"));
						objMap.put("imgString", imgString);
						imgStringId = settleList.get(i).get("id").toString();
						objMap.put("xkh_imgId", imgStringId);
					}
				}
			}
			//活动图片
			if (imgString != null && !"".equals(imgString)) {
				String[] imgArr = imgString.split(",");
				model.addAttribute("imgArr", imgArr);
			}

			model.addAttribute("settleList", settleList);
			//判断入驻类型
			String enterId = String.valueOf(objMap.get("enterId"));
			paramsMap.put("enterId", enterId);
			Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
			String typeValue = String.valueOf(setypeMap.get("typeValue"));
			paramsMap.put("typeValue", typeValue);
			model.addAttribute("typeValue", typeValue);
			model.addAttribute("paramsMap", paramsMap);
			if ("jndr".equals(typeValue) || "gxst".equals(typeValue)) {
				return "/wap/xkh_version_2/school_enterInfoEdit";
			} else {
				return "redirect:/home/index.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";

		}

	}

	/**
	 * 修改userBasicinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateSettleUser")
	public String updateSettleUser(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			boolean flag = userBasicinfoService.upSettleVersion3(paramsMap);
			if (flag) {
				return "redirect:/home/getmyInform.html";
			} else {
				model.addAttribute("msg", "更新失败");
				return "redirect:/home/index.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "更新失败");
			return "redirect:/home/index.html";
		}
	}

	/**
	 * 删除图片   先取出后更新
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delImage")
	public @ResponseBody Map delImage(HttpServletRequest request, HttpServletResponse response) {
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();

		Map<String, Object> image = userBasicinfoService.getSkillinfoData(paramsMap);//获取子表中的信息
		String imgString = image.get("typeValue").toString();//活动路径
		String imge = paramsMap.get("imageUrl") + ",";

		imgString.replaceAll(imge, "");
		map.put("typeValue", imgString.replaceAll(imge, ""));
		map.put("imgeId", paramsMap.get("imgeId"));
		boolean flag = userBasicinfoService.updateSkillinfoData(map);
		map.put("flag", flag);

		return map;
	}

	/**
	 * 回显公共接口  
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toAllUpdateUser")
	public String toAllUpdateUser(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			//使用openId 去查需要的参数   用户id  入驻类型
			Map<String, Object> userInfoMap = skillUserService.isUserExist(openId);// 用户基本信息
			String enterId="";
			if(userInfoMap!=null&&!"".equals(userInfoMap)){
				enterId =String.valueOf(userInfoMap.get("enterId"));
			}
			paramsMap.put("enterId", enterId);
			Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
			//String enterId = userInfoMap.get("enterId").toString();
			//String enterId = setypeMap.get("id").toString();
			String id = userInfoMap.get("id").toString();
			//paramsMap.put("enterId", enterId);
			
			String typeValue = String.valueOf(setypeMap.get("typeValue"));
		//	System.out.println("王小雷的typeValue==========" + typeValue);
			paramsMap.put("typeValue", typeValue);
			if (GlobalConstant.ENTER_TYPE_JNDR.equals(typeValue) || GlobalConstant.ENTER_TYPE_GXST.equals(typeValue)) {
				return "redirect:/highSchoolSkill/toUpdateSettledUser.html?id=" + id + "&myself="
						+ paramsMap.get("myself");
			} else if (GlobalConstant.ENTER_TYPE_SQDV.equals(typeValue)) {
				return "redirect:/comunityBigV/toAddSettle.html?id=" + id + "&enterId=" + enterId + "&myself="
						+ paramsMap.get("myself");
			} else if (GlobalConstant.ENTER_TYPE_CDZY.equals(typeValue)) {
				return "redirect:/fieldEnter/toUpdateEnter.html?id=" + id + "&myself=" + paramsMap.get("myself");
			} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(typeValue)) {
				return "redirect:/schoolBusi/toUpdateSchoolBusi.html?id=" + id + "&myself=" + paramsMap.get("myself");
			} else {
				model.addAttribute("msg", "查询失败");
				return "redirect:/home/index.html";
			}

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "查询失败");
			return "redirect:/home/index.html";
		}
	}
	
	@RequestMapping(value = "/toRefundInfo")
	public String toRefundInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		try {
			createParameterMap(request);
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));//用户的openId
			String orderId = request.getParameter("orderId");
			Map<String, Object> setypeMap = userBasicinfoService.getEnterTypeValueVersion3(paramsMap);
		
			return null;
		

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "查询失败");
			return "redirect:/home/index.html";
		}
	}
	
	
	
	

}
