package com.jzwl.xydk.wap.xkh2_3.invoice.controller;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.sign.AccessCore;
import com.jzwl.common.utils.DateUtil;
import com.jzwl.common.utils.DoubleArithUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.xkh2_3.invoice.service.InvoiceService;
/**
 * @author Administrator
 *发票管理
 */
@Controller
@RequestMapping(value="/invoice")
public class InvoiceController extends BaseWeixinController{
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private Constants contants; 
	@Autowired
	private SendMessageService sendMessage;
	/** 日志 */
	Logger log = Logger.getLogger(InvoiceController.class);
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询已开/未开发票列表
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/invoice")
	public String findInvoiceList(HttpServletRequest request, Model model){
		log.info("跳转到----------------发票列表页面------");
		createParameterMap(request);
		try {
			//获取openId 根据openId 查询
			String openId = getCurrentOpenId(request);//从session中获取openId
			//String openId = "oHxmUjgTv3PnsjNmNH5HTQIgO10U";
			//判断是否入驻
			boolean flag = false;
			Map<String, Object> map = invoiceService.getUserBaseInfo(openId);
			if(map != null && map.size() > 0 ){
				flag = true;
			}
			//未开发票的订单
			List<Map<String, Object>> unInvoicedList = invoiceService.findInvoiceList(openId, flag, "0");
			//已开发票的订单
			List<Map<String, Object>> invoicedList = invoiceService.findInvoiceList(openId, flag, "1");
			Map<String, List<Map<String, Object>>> cache = new HashMap<String, List<Map<String, Object>>>();
			for (Map<String, Object> db : invoicedList)  {
			    Date keyDate = (Date)db.get("createDate");
			    String key = DateUtil.dateToStr(keyDate, "yyyy-MM-dd HH:mm:ss");
			    List<Map<String, Object>> cacheList = cache.get(key);
			    if (null == cacheList || cacheList.size() == 0){
			         List<Map<String, Object>> newCacheList = new LinkedList<Map<String, Object>>();
			         newCacheList.add(db);
			         cache.put(key, newCacheList);
			    } else {
			         cacheList.add(db);
			    }
			}
			model.addAttribute("invoicedList", cache);
			model.addAttribute("unInvoicedList", unInvoicedList);
			model.addAttribute("openId", openId);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "/wap/xkh_version_2_3/invoice/open_invoice";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击开具发票 到发票详情页面
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/openInvoice")
	public String openInvoice(HttpServletRequest request, Model model){
		log.info("跳转到----------------发票信息详情页面------");
		createParameterMap(request);
		try {
			String obj = (String)paramsMap.get("obj");
			obj = obj.replaceAll("\"", "\'");
			String totalPrice = (String)paramsMap.get("price");
			//获取发票类型
			List<Map<String, Object>> typeList = invoiceService.getInvoType();
			//获取物流类型
			List<Map<String, Object>> logisticsList = invoiceService.getLogisticsTypes();
			Double type = 0d;
			//获取普通税点
			Map<String, Object> pointMap = invoiceService.getTaxPoint();
			//获取专用税点
			Map<String, Object> specPointMap = invoiceService.getSpecPoint();
			String specDic_name = (String)specPointMap.get("dic_name");
			//专用税点
			Double spec = 0d;
			if(specDic_name != null && !"".equals(specDic_name)){
				spec = Double.parseDouble(specDic_name);
			}
			String specd = spec*100+"%";
			String dic_name = (String)pointMap.get("dic_name");
			//普通税点
			Double d = 0d;
			if(dic_name != null && !"".equals(dic_name)){
				d = Double.parseDouble(dic_name);
			}
			String point = d*100+"%";
			//计算合计金额
			Double price = 0d;
			if(totalPrice != null && !"".equals(totalPrice)){
				price = Double.parseDouble(totalPrice);
			}
			if(logisticsList != null && logisticsList.size() > 0){
				Map<String, Object> map = logisticsList.get(0);
				String dic_value = (String)map.get("dic_value");
				if(dic_value != null && !"".equals(dic_value)){
					type = Double.parseDouble(dic_value);
				}
			}
			//普通合计金额
			Double money = DoubleArithUtil.mul(price, DoubleArithUtil.add(1, d));
			Double allMoney = DoubleArithUtil.add(money, type);
			//专用合计金额
			Double sMoney = DoubleArithUtil.mul(price, DoubleArithUtil.add(1, spec));
			Double specMoney = DoubleArithUtil.add(sMoney, type);
			//普通发票税点后的价值
			Double ptfpMoney = DoubleArithUtil.mul(price, d);
			//专用发票税点后价值
			Double zyfpMoney = DoubleArithUtil.mul(price, spec);
			model.addAttribute("obj", obj);
			model.addAttribute("totalPrice", totalPrice);
			model.addAttribute("typeList", typeList);
			model.addAttribute("logisticsList", logisticsList);
			model.addAttribute("point", point);
			model.addAttribute("pointMap", pointMap);
			model.addAttribute("ptfpMoney", ptfpMoney);
			model.addAttribute("specd", specd);
			model.addAttribute("specPointMap", specPointMap);
			model.addAttribute("zyfpMoney", zyfpMoney);
			model.addAttribute("allMoney", allMoney);
			model.addAttribute("specMoney", specMoney);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/wap/xkh_version_2_3/invoice/invoice";
	}
	
	
	
	
	
	/**
	 * 
	 * 描述:ajax 存储记录
	 * 作者:gyp
	 * @Date	 2017年5月27日
	 */
	@RequestMapping(value = "/addInvoice")
	public @ResponseBody Map<String, Object> addInvoice(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);

		String openId = getCurrentOpenId(request);//从session中获取openId
		Map<String, Object> map = new HashMap<String, Object>();
		//获取用户的集合，根据排列顺序进行显示
		try {
			paramsMap.put("openId", openId);
			String price = (String)paramsMap.get("totalPrice");
			//存储信息到发票表
			String id = invoiceService.addInvoice(paramsMap);
			
			map.put("price", price);
			map.put("id", id);
			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到发票支付页
	 * 创建人： sx
	 * 创建时间： 2017年5月12日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/toPay")
	@ResponseBody
	public void toPay(HttpServletRequest request, HttpServletResponse response, Model model){
		log.info("跳转到----------------发票支付页面------");
		createParameterMap(request);
		try {
			String price = (String)paramsMap.get("price");
			String id = (String)paramsMap.get("invoiceId");
			Map<String,Object> signMap = new HashMap<String,Object>();
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
			String commodityName = "发票支付";
			//String totalPrice = price;
			String totalPrice = price;
			String invoiceId = id;
			String clientFlag = "xkh";
			String sendUrl = "http://xydk.xiaoka360.com/invoice/successToIndex.html";
			String keyUrlOrderId = "wxPay_xkh|http://xydk.xiaoka360.com/invoice/invoicePaySuccess.html|"+invoiceId;
			String systemFlag = "xkh";
			signMap.put("openId",openId);
			signMap.put("commodityName",commodityName);
			signMap.put("totalPrice",totalPrice);
			signMap.put("orderId",invoiceId);
			signMap.put("clientFlag",clientFlag);
			signMap.put("sendUrl",sendUrl);
			signMap.put("keyUrlOrderId",keyUrlOrderId);
			commodityName = URLEncoder.encode(commodityName);
			String sign = AccessCore.sign(signMap, contants.getWx_signKey());//拿到签名数据
			String url = "http://klgj.xiaoka360.com/wm-plat/api/jumpPay.html" + "?openId="+openId+"&clientFlag="+clientFlag+"&sign="+sign
					+ "&sendUrl="+sendUrl+"&keyUrlOrderId="+keyUrlOrderId+"&commodityName="+commodityName+"&totalPrice="+totalPrice+"&orderId="+invoiceId+"&systemFlag="+systemFlag;
			response.sendRedirect(url);	
			
			
			model.addAttribute("totalPrice", totalPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * moblie:移动端
	 *	同步更新
	 * 支付成功后主动发送过来的携带：OrderId ,out_trade_no.用于更新订单状态
	 */
	@RequestMapping(value = "/invoicePaySuccess")
	public @ResponseBody void invoicePaySuccess(HttpServletRequest request, HttpServletResponse response,Model model){
		createParameterMap(request);
		//判断签名
		boolean verify = AccessCore.verify(paramsMap,contants.getWx_signKey());
		String outTradeNo = request.getParameter("out_trade_no");
		String invoiceId = request.getParameter("orderId");
		
		log.info("++++++++++++++++++++++++++++++++++++++++++同步跟新订单状态参数outTradeNo="+outTradeNo+"----orderId="+invoiceId);
		//具体更新订单状态
		try {
			//更新支付的信息
			boolean flag = invoiceService.updateInvoiceById(invoiceId, outTradeNo);//更新付款后的状态
			//根据发票id查询订单id
			Map<String, Object> map = invoiceService.findOrderIdsById(invoiceId);
			String orderIds = map.get("orderIds").toString();
			String openId = map.get("openId").toString();
			String payMoney = map.get("payMoney").toString();
			if(flag){
				sendMessage.invoicePayMoneySuccess(invoiceId, openId, payMoney);
			}
			if(orderIds.contains(",")){//含有说明是多个
				String str[] = orderIds.split(",");
				for (int i = 0; i < str.length; i++) {
					invoiceService.updateOrderByOrderId(invoiceId,str[i]);
				}
			}else{
				invoiceService.updateOrderByOrderId(invoiceId,orderIds);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述:点击完成
	 * 作者:gyp
	 * @Date	 2017年5月27日
	 */
	@RequestMapping(value = "/successToIndex")
	public String payOldSuccess(HttpServletRequest request, HttpServletResponse response,Model model){
		String orderId = request.getParameter("orderId");
		log.info("-----------------XKH进入跳转,点击完成或者返回");
		return  "redirect:/home/index.html";
		/*mov.setViewName("redirect:/groupPurchase/groupPurchaseList.html");*/
	}
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：在支付页面点击否,删除当前数据防止产生垃圾数据
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/checkNo")
	public String checkNo(HttpServletRequest request, Model model){
		log.info("在支付页面点击否删除当前发票信息防止产生垃圾数据------");
		createParameterMap(request);
		boolean flag = false;
		try {
			String id = (String)paramsMap.get("id");
			flag = invoiceService.deleteInvoice(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(flag){//删除成功跳转到相应页面
			return "";
		}
		return "/error";
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/updateOrderSelltoPay")
	public void updateOrderSelltoPay(HttpServletRequest request, HttpServletResponse response){
		createParameterMap(request);
		Map<String,Object> signMap = new HashMap<String,Object>();
		
		try {
			String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
			//String openId = "oHxmUjgTv3PnsjNmNH5HTQIgO10U";
			String commodityName = "我的发票";
			String totalPrice = paramsMap.get("payMoney").toString();
			String id = (String)paramsMap.get("id");
			Map<String,Object> map = invoiceService.findOrderIdsById(id);
			//根据发票id查询订单id
			String orderIds = map.get("orderIds").toString();
			String clientFlag = "xkhinv";
			String sendUrl = "http://xydk.xiaoka360.com/invoice/payOldSuccess.html";
			String keyUrlOrderId = "wxPay_xkh|http://xydk.xiaoka360.com/invoice/paySuccess.html|"+orderIds;
			String systemFlag = "xkh";
			signMap.put("openId",openId);
			signMap.put("commodityName",commodityName);
			signMap.put("totalPrice",totalPrice);
			signMap.put("orderIds",orderIds);
			signMap.put("clientFlag",clientFlag);
			signMap.put("sendUrl",sendUrl);
			signMap.put("keyUrlOrderId",keyUrlOrderId);
			commodityName = URLEncoder.encode(commodityName);
			String sign = AccessCore.sign(signMap, contants.getWx_signKey());//拿到签名数据
			String url = "http://klgj.xiaoka360.com/wm-plat/api/jumpNewPay.html" + "?openId="+openId+"&clientFlag="+clientFlag+"&sign="+sign+"&id"+id
					+ "&sendUrl="+sendUrl+"&keyUrlOrderId="+keyUrlOrderId+"&commodityName="+commodityName+"&totalPrice="+totalPrice+"&orderId="+orderIds+"&systemFlag="+systemFlag;
			response.sendRedirect(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
