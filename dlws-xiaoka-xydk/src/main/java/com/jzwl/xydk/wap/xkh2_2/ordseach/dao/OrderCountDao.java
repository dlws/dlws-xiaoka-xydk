/**
 * 关于订单统计表
 */
package com.jzwl.xydk.wap.xkh2_2.ordseach.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.system.base.dao.BaseDAO;

/**
 * @author Administrator
 *
 */

@Repository(value="orderCountDao")
public class OrderCountDao {

	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库 
	
	/**
	 * 第一次创建的时候默认为0表示已读
	 * @return
	 */
	public boolean creatOrderCountByOpenId(String openId){
		
		//待付款
		int obligation = getOrderCountByStatus(openId,1);
		//待服务
		int pendingService = getOrderCountByStatus(openId,2);
		//待确认
		int noConfirmed = getOrderCountByStatus(openId,3);
		//待退款
		int refundable = getOrderCountByStatus(openId,7);
		
		int sellerCount = getOrderBySellerOpenId(openId);
		
		Map<String,Object> conditionMap = new HashMap<String, Object>();
		
		conditionMap.put("openId", openId);
		conditionMap.put("obligation", obligation);
		conditionMap.put("pendingService", pendingService);
		conditionMap.put("noConfirmed", noConfirmed);
		conditionMap.put("refundable", refundable);
		conditionMap.put("sellerCount", sellerCount);
		conditionMap.put("createDate", new Date());

		String sql = "insert into `xiaoka-xydk`.order_count(openId,sellerCount,obligation,pendingService,noConfirmed,refundable,createDate)"
				+ " values (:openId,:sellerCount,:obligation,:pendingService,:noConfirmed,:refundable,:createDate)";
		
		baseDAO.executeNamedCommand(sql, conditionMap);
		
		return false;
	}
	
	public boolean updateOrderCountByOpenId(Map<String,Object> map){
		
		try {
			String sql="update `xiaoka-xydk`.order_count set ";
			if(map.containsKey("sellerCount")){
				sql+="sellerCount = :sellerCount ,";
			}
			if(map.containsKey("obligation")){
				sql+="obligation = :obligation ,";
			}
			if(map.containsKey("pendingService")){
				sql+="pendingService = :pendingService ,";
			}
			if(map.containsKey("noConfirmed")){
				sql+="noConfirmed = :noConfirmed ,";
			}
			if(map.containsKey("refundable")){
				sql+="refundable = :refundable ,";
			}
			sql = sql.substring(0, sql.length()-1);
			sql +="where openId = :openId";
			return baseDAO.executeNamedCommand(sql, map);
		
		} catch (Exception e) {
			
			return false;
		}
		
	}
	
	/**
	 * 获取当前中用户下的order的信息
	 * @param sellerOpenId
	 * @return
	 */
	public Map<String,Object> getOrderCountBySellerOpenId(String openId){
		
		try {
			String sql="select openId,sellerCount,obligation,pendingService,"
					+ "noConfirmed,refundable from `xiaoka-xydk`.order_count where openId = ?";
			return baseDAO.getJdbcTemplate().queryForMap(sql,openId);
			
		} catch (Exception e) {
			return null;
		}
	}
	
	
	/**
	 * 获取不同状态下的订单总数
	 * 获取当前用户的OpenId 返回
	 * @param openId
	 * @return
	 */
	public int getOrderCountByStatus(String openId,int orderStatus){
		
		String sql = " select count(*) as obligation from `xiaoka-xydk`.`order` "
				+ "ord where ord.openId='"+openId+"' and ord.orderStatus ="+orderStatus;
		int count  = baseDAO.getJdbcTemplate().queryForInt(sql);
		
		return count;
	}
	
	
	public int getOrderBySellerOpenId(String sellerOpenId){
		
		String sql = "select count(*) from `xiaoka-xydk`.`order` ord where ord.sellerOpenId='"+sellerOpenId+"'";
		
		int count  = baseDAO.getJdbcTemplate().queryForInt(sql);
		
		return count;
	}
	
	
}
