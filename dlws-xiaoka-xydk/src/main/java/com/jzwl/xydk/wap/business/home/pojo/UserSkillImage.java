/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.pojo;

import com.jzwl.system.base.pojo.BasePojo;



public class UserSkillImage extends BasePojo implements java.io.Serializable{
	
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "UserSkillImage";
	public static final String ALIAS_ID = "id";
	public static final String ALIAS_SKILL_ID = "技能的id";
	public static final String ALIAS_IMG_URL = "图片地址";
	public static final String ALIAS_IMG_TYPE = "技能图片类型（0：封面图，1：高清照片）";
	public static final String ALIAS_CREATE_DATE = "创建时间";
	
	//date formats
	public static final String FORMAT_CREATE_DATE = DATE_FORMAT;
	

	//columns START
    /**
     * id       db_column: id 
     */ 	
	private java.lang.Long id;
    /**
     * 技能的id       db_column: skillId 
     */ 	
	private java.lang.Long skillId;
    /**
     * 图片地址       db_column: imgURL 
     */ 	
	private java.lang.String imgUrl;
    /**
     * 技能图片类型（0：封面图，1：高清照片）       db_column: imgType 
     */ 	
	private java.lang.Integer imgType;
    /**
     * 创建时间       db_column: createDate 
     */ 	
	private java.util.Date createDate;
	//columns END

	
	
	
	
	
	


	public java.lang.Long getId() {
		return this.id;
	}
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	

	public java.lang.Long getSkillId() {
		return this.skillId;
	}
	
	public void setSkillId(java.lang.Long value) {
		this.skillId = value;
	}
	

	public java.lang.String getImgUrl() {
		return this.imgUrl;
	}
	
	public void setImgUrl(java.lang.String value) {
		this.imgUrl = value;
	}
	

	public java.lang.Integer getImgType() {
		return this.imgType;
	}
	
	public void setImgType(java.lang.Integer value) {
		this.imgType = value;
	}
	

	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	

}

