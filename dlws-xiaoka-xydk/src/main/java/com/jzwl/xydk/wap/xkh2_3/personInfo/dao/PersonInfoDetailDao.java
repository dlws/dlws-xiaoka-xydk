package com.jzwl.xydk.wap.xkh2_3.personInfo.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillinfo;
import com.jzwl.xydk.wap.xkh2_3.personInfo.pojo.PersonInfoDetail;

@Repository
public class PersonInfoDetailDao {

	@Autowired
	private BaseDAO baseDao;
	
	
	/**
	 * 获取当前用户的技能信息
	 * pid 对应user_skillinfo 中的Id
	 * @return
	 */
	public Map<String,Object> getuserSkillinfoDetail(Map<String,Object> map){
		
		String sql = "select id,pid,typeName,typeValue from `xiaoka-xydk`.user_skillinfo_detail where 1=1 ";
		
		if(map.containsKey("pid")){
			sql = sql + "  and pid = "+map.get("pid").toString();
		}
		if(map.containsKey("isDelete")){
			
			sql = sql + "  and isDelete ="+map.get("isDelete").toString();
		}
		
		List<Map<String,Object>> list = baseDao.getJdbcTemplate().queryForList(sql);
		return dealList(list);
	}
	
	/**
	 * 获取当前用户的技能信息
	 */
	public  List<PersonInfoDetail> getUserSkillInfo(String basicInfo){
		
		if(StringUtils.isNotBlank(basicInfo)){
			
			String sql ="select *,us.id as id,usf.className as fathName,usf.classValue as fathValue,uss.className as sonName,uss.classValue as sonValue  from `xiaoka-xydk`.user_skillinfo us LEFT JOIN  "
					+ "`xiaoka-xydk`.user_skillclass_father usf on us.skillFatId = usf.id LEFT JOIN "
					+ "`xiaoka-xydk`.user_skillclass_son uss on uss.id = us.skillSonId where us.isDelete = 0 and us.basicId='"+basicInfo+"'";
			
			List<PersonInfoDetail> kills = baseDao.getJdbcTemplate().query(sql,new Object[] {},
					ParameterizedBeanPropertyRowMapper
							.newInstance(PersonInfoDetail.class));
			
			return kills;
		}
		
		return null;
		
	}
	
	/**
	 * 根据ID值获取当前的单位 汉化
	 */
	public Map<String,Object> getUnit(Map<String,Object> map){
		
		String sql = "select * from `xiaoka`.v2_dic_data where 1=1 ";
		
		if(map.containsKey("id")){
			sql = sql + "and id='"+map.get("id")+"' ";
		}
		if(map.containsKey("parentId")){
			sql = sql + "and parentId ='"+map.get("parentId")+"' ";
		}
		try {
			return baseDao.getJdbcTemplate().queryForMap(sql);
		} catch (Exception e) {
			return null;
		}
		
		
	}
	
	
	
	/**
	 * 处理当前查询出来的参数 
	 * 将单条的
	 * 
	 */
	public Map<String,Object>  dealList(List<Map<String,Object>> list){
		
		Map<String,Object> dealResult = new HashMap<String, Object>();
		
		for (int i = 0; i < list.size(); i++) {
			
			String key = list.get(i).get("typeName").toString();
			dealResult.put(key,list.get(i).get("typeValue"));
			
		}
		return dealResult;
	}
	
}
