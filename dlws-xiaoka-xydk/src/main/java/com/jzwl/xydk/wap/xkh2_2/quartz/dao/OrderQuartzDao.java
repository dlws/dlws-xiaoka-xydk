/**
 * QuartzDao.java
 * com.jzwl.xydk.wap.xkh2_2.quartz.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.quartz.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   gyp
 * @Date 2017年4月20日
 * @Time 下午6:16:05
 */
@Repository("orderQuartzDao")
public class OrderQuartzDao {
	
	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库
	
	/**
	 * 描述:下单超过1小时未付款
	 * 作者:gyp
	 * @Date	 2017年4月20日
	 */
	public boolean cancelTimeOutNotPayOrder(String currTime, String beforeTime,String orderId) {
//		finishflag timeoutflag cancelflag payflag remark cancleTime
		String sql = " update `xiaoka-xydk`.`order` set orderStatus = 5, cancelTime=:cancelTime where orderId=:orderId";
		Map map = new HashMap();
		map.put("cancelTime", currTime);
		map.put("remark", "下单后1小时未付款,系统自动关闭订单！");
		map.put("beforeTime", beforeTime);
		map.put("orderId", orderId);
		return baseDAO.executeNamedCommand(sql, map);
	}
	/**
	 * 
	 * 描述:发布更新订单状态
	 * 作者:gyp
	 * @Date	 2017年6月9日
	 */
	public boolean cancelTimeOutNotPayOrder_fabu(String currTime, String beforeTime,String pid) {
//		finishflag timeoutflag cancelflag payflag remark cancleTime
		String sql = " update `xiaoka-xydk`.`order` set orderStatus = 5, cancelTime=:cancelTime where pid=:pid";
		Map map = new HashMap();
		map.put("cancelTime", currTime);
		map.put("remark", "下单后1小时未付款,系统自动关闭订单！");
		map.put("beforeTime", beforeTime);
		map.put("pid", pid);
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	/**
	 * 
	 * 描述:查询出超时未付款的订单
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public List<Map<String, Object>> findTimeOutNotPayOrderList(String beforeTime) {
		String sql = " SELECT openId,orderId FROM `xiaoka-xydk`.`order` t where t.orderStatus = 1 and '" + beforeTime + "' >= createDate and orderType = 0";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	/**
	 * 
	 * 描述:发布用户查询订单
	 * 作者:gyp
	 * @Date	 2017年6月9日
	 */
	public List<Map<String, Object>> findTimeOutNotPayOrderList_fabu(String beforeTime) {
		String sql = " SELECT openId,orderId,pid FROM `xiaoka-xydk`.`order` t where t.orderStatus = 1 and '" + beforeTime + "' >= createDate and orderType = 1 GROUP BY t.pid";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	/**
	 * 
	 * 描述:精准咨询已接单，咨询到期的订单
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public List<Map<String, Object>> findExpiresOrderList_jzzx(String expiresTime) {
		String sql = " SELECT openId,orderId FROM `xiaoka-xydk`.`order` t where t.enterType = 'jzzx' and t.orderStatus = 0 and '" + expiresTime + "' >= receiveTime";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	//非精准投放
	public List<Map<String, Object>> orderSuccess(String expiresTime) {
		
		String sql = " SELECT openId,orderId,payMoney,sellerOpenId, schoolId, cityId, createDate, payMoney, "
				+ " payTime, serviceMoney,diplomatMoney,platformMoney, serviceRatio, diplomatRatio, "
				+ " platformRatio, realityMoney, orderStatus,payway, cancelTime, invalidTime,refundTime, "
				+ " linkman, phone, message, startDate,finishTime,endDate FROM `xiaoka-xydk`.`order` t "
				+ " where ((t.isFinish = 1 and t.orderStatus= 12) or (t.isFinish = 1 and t.orderStatus= 3)) and '" + expiresTime + "' >= t.finishTime and t.enterType <>'"+GlobalConstant.ENTER_TYPE_JZTF+"'";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	/**
	 * 
	 * 描述:精准投放     投放内容审核成功后的列表
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public List<Map<String, Object>> jztfGetOrderList(String beforeTime) {
		String sql = " SELECT openId,orderId,payMoney,sellerOpenId, schoolId, cityId, createDate, payMoney, "
				+ " payTime, serviceMoney,diplomatMoney,platformMoney, serviceRatio, diplomatRatio, "
				+ " platformRatio, realityMoney, orderStatus,payway, cancelTime, invalidTime,refundTime, "
				+ " linkman, phone, message, startDate,finishTime,endDate FROM `xiaoka-xydk`.`order` t "
				+ " where t.orderStatus = 3 and t.enterType ='"+GlobalConstant.ENTER_TYPE_JZTF+"' and '" + beforeTime + "' >= endDate";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	/**
	 * 
	 * 描述:获取支付后超时未接单的订单
	 * 作者:gyp
	 * @Date	 2017年5月10日
	 */
	public List<Map<String, Object>> getPaySuccessOrders(String expiresTime) {
		String sql = " SELECT openId,orderId FROM `xiaoka-xydk`.`order` t where t.orderStatus = 2 and '" + expiresTime + "' >= t.payTime";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	/**
	 * 
	 * 描述:根据订单的orderId更新订单的状态
	 * 作者:gyp
	 * @Date	 2017年5月10日
	 */
	public boolean updateOrder(Map<String,Object> map) {
		String sql = " update `xiaoka-xydk`.`order` set orderStatus = :orderStatus where orderId = :orderId";
		return baseDAO.executeNamedCommand(sql, map);
	}
	/**
	 * 
	 * 描述:定时任务将订单状态更新成完成
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public boolean jztfUpdateOrderList(Map<String,Object> map) {
		String sql = " update `xiaoka-xydk`.`order` set orderStatus = 4 where orderId = :orderId";
		return baseDAO.executeNamedCommand(sql, map);
	}
	/**
	 * 精准咨询更新咨询到期的订单
	 * 描述:
	 * 作者:gyp
	 * @Date	 2017年4月24日
	 */
	public boolean jzzxUpdateExpiresOrderList(Map<String,Object> map) {
		String sql = " update `xiaoka-xydk`.`order` set orderStatus = 4 where orderId=:orderId ";
		return baseDAO.executeNamedCommand(sql, map);
	}
	
	/**
	 * 申请退款的到期未执行退款的订单
	 */
	public List<Map<String, Object>> orderAgreen(){
		String sql = "select * from `xiaoka-xydk`.`order` b where  timestampadd(day, 7, b.applyRefundTime) <= NOW() and b.orderStatus=6";
		List<Map<String, Object>> list = baseDAO.queryForList(sql);
		return list;
	}
	
	/**
	 * 将订单状态更改为7待退款状态
	 */
	public boolean toChangeOrderStatus(Map<String,Object> map) {
		String sql = " update `xiaoka-xydk`.`order` set orderStatus = 7 where orderId=:orderId ";
		return baseDAO.executeNamedCommand(sql, map);
	}

}

