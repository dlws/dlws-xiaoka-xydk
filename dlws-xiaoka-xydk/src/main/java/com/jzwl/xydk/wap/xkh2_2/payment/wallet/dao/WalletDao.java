package com.jzwl.xydk.wap.xkh2_2.payment.wallet.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.manager.dividewithdraw.pojo.DivideDraw;

@Repository("walletDao")
public class WalletDao {

	@Autowired
	private BaseDAO baseDao;
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据用户openId查询钱包信息
	 * 创建人： sx
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getWalletByOpenId(String openId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, openId, balance, createTime from `xiaoka-xydk`.wallet where openId = '"+openId+"'");
		return baseDao.queryForMap(sb.toString());
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据用户openId查询记录表信息
	 * 创建人： sx
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public List<DivideDraw> getDivideDrawByOpenId(String openId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select id, openId, divideOrWithdraw, balanceMoney, state, divideOrWithdrawTime,orderId "
				+ "from `xiaoka-xydk`.divideorwithdrawrecord where openId = '"+openId+"' order by divideOrWithdrawTime desc");
		return baseDao.queryForList(sb.toString());
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月18日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getMoneyByOpenId(String openId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select IFNULL(sum(withdrawMoney),0) as withdrawMoney from `xiaoka-xydk`.application_withdraw "
				+ " where applicationStatus = '0' and openId = '"+openId+"'");
		return baseDao.queryForMap(sb.toString());
	}
	
	/**
	 * 新增记录到 wallet中
	 * @param openId 当前新增钱包的OpenId
	 * @param balance 新增初始化的余额
	 * @return
	 */
	public boolean insertWallet(String openId,double balance,int isDiplomat){
		
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("id", Sequence.nextId());
		map.put("openId", openId);
		map.put("balance",balance);
		map.put("isDiplomat",1);//1.表示外交官
		map.put("createTime",new Date());
		String sql = "insert into `xiaoka-xydk`.wallet (id,openId,balance,isDiplomat,createTime) "
				+ "values (:id,:openId,:balance,:isDiplomat,:createTime )";
		
		return baseDao.executeNamedCommand(sql, map);
	}
	
	
	public boolean updateWallet(Map<String,Object> map){
		
		if(map.get("balance")==null||map.get("openId")==null||
			StringUtils.isBlank(map.get("balance").toString())){
			return false;
		}
		String sql = "update `xiaoka-xydk`.wallet set ";
		
		if(map.containsKey("balance")){
			sql += "balance = :balance ,";
		}
		if(map.containsKey("isDiplomat")){
			sql += "isDiplomat = :isDiplomat ,";
		}
		sql = sql.substring(0, sql.length()-1);
		sql +="  where openId=:openId";
		
		return baseDao.executeNamedCommand(sql, map);
	}
	
	
}
