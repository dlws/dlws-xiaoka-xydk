/**
 * ChatService.java
 * com.jzwl.xydk.wap.xkh2_2.chat.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.chat.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.chat.dao.ChatMessageDao;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   wxl
 * @Date	 2017年4月7日 	 
 */
@Service("chatMessageService")
public class ChatMessageService {

	@Autowired
	private ChatMessageDao chatMessageDao;

	/*
	 * 消息列表
	 */
	public List<Map<String, Object>> queryChatList(Map<String, Object> paramsMap) {

		return chatMessageDao.queryChatList(paramsMap);

	}

	/*
	 * 消息详情---即聊天列表
	 */
	public List<Map<String, Object>> getChatInfoById(Map<String, Object> paramsMap) {

		return chatMessageDao.getChatInfoById(paramsMap);

	}

	/*
	 * 更新未读为已读
	 */
	public boolean updatechatStatus(Map<String, Object> paramsMap) {

		return chatMessageDao.updatechatStatus(paramsMap);

	}
	
	/*
	 * 系统消息更新未读为已读
	 */
	public boolean updatechatSyStatus(Map<String, Object> paramsMap) {
		
		return chatMessageDao.updatechatSyStatus(paramsMap);
		
	}

	/*
	 * 获取聊天title
	 */
	public Map<String, Object> getChatTitle(Map<String, Object> paramsMap) {

		return chatMessageDao.getChatTitle(paramsMap);

	}
	/**
	 * 获得微信头像
	 */
	public Map<String, Object>  getHeadImg(Map<String,Object> map){
		return chatMessageDao.getHeadImg(map);
	}
	/*
	 * 发送内容
	 */
	public boolean insertChatContent(Map<String, Object> map) {

		return chatMessageDao.insertChatContent(map);

	}
	/*
	 * 群发信息添加列表
	 */
	public boolean insertInfoByMass(Map<String, Object> map) {

		return chatMessageDao.insertInfoByMass(map);

	}

	/*
	 * 系统消息
	 */
	public List<Map<String, Object>> getSysList(Map<String, Object> paramsMap) {

		return chatMessageDao.getSysList(paramsMap);

	}

	/*
	 * 列表关于系统消息的信息--未读条数，最新系统消息时间，系统头像
	 */
	public Map<String, Object> querySysMap(Map<String, Object> paramsMap) {

		return chatMessageDao.querySysMap(paramsMap);

	}

	/*
	 * 更新状态
	 */

	public String updateIsRead(Map<String, Object> paramsMap) {

		return chatMessageDao.updateIsRead(paramsMap);

	}

	/*
	 * 判断会话是否存在
	 */
	public String getChatRelation(Map<String, Object> paramsMap) {

		return chatMessageDao.getChatRelation(paramsMap);

	}

	/*
	 * 创建会话 并返回会话的id
	 */
	public String createChatRelation(Map<String, Object> paramsMap) {

		return chatMessageDao.createChatRelation(paramsMap);

	}

	/*
	 * 插入关于订单系统信息
	 */
	public boolean insertOrderForBuy(Map<String, Object> paramsMap) {

		return chatMessageDao.insertOrderForBuy(paramsMap);

	}

	/*
	 * 补充插入关于订单系统信息
	 */
	public boolean insertUpdateSysMeg(Map<String, Object> paramsMap) {

		return chatMessageDao.insertUpdateSysMeg(paramsMap);

	}
	
	/*
	 * 买家订单系统信息
	 */
	public boolean insertBuyMeg(Map<String, Object> paramsMap) {
		
		return chatMessageDao.insertBuyMeg(paramsMap);
		
	}

	public boolean deletChat(Map<String, Object> map) {
		return chatMessageDao.deletChat(map);
	}
	
	/**
	 * 查询当前OpenId下有无新的消息
	 * 有则返回 true 无则 false
	 * @return
	 */
	public boolean haveNewMessage(String openId){
		
		return chatMessageDao.haveNewMessage(openId);
	}
	
	
	
	
	
	
}
