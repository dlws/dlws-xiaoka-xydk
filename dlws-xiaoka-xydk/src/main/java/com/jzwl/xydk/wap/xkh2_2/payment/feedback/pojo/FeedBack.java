package com.jzwl.xydk.wap.xkh2_2.payment.feedback.pojo;

import java.util.Date;

/**
 * 
 * this pojo content feedbackold table
 * @author Administrator
 * 
 */

public class FeedBack {

	 private long  id; //'反馈内容的id',
	 private long  orderId; //'关联当前订单表id',
	 private String title; //'标题',
	 private String feedbackurl; //'反馈链接',
	 private String describe; //'反馈描述',
	 private String suggestion; //'反馈描述',
	 private String reseon; //'拒绝理由',
	 private Date createTime; //'创建时间也是反馈排序时间',
	 private int isDelete; //'是否删除',
	 private int sellerRefuse;//卖家拒绝状态 0: 未拒绝1：已拒绝
	 private int buyerReject;//买家驳回状态：0 为未驳回 1 为已驳回
	 private int feedbackstatus;//标识当前反馈状态是否最新 0 老的,1 最新
	 
	

	public FeedBack() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public FeedBack(long id, long orderId, String title, String feedbackurl,
			String describe, String suggestion, String reseon, Date createTime,
			int isDelete) {
		super();
		this.id = id;
		this.orderId = orderId;
		this.title = title;
		this.feedbackurl = feedbackurl;
		this.describe = describe;
		this.suggestion = suggestion;
		this.reseon = reseon;
		this.createTime = createTime;
		this.isDelete = isDelete;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFeedbackurl() {
		return feedbackurl;
	}
	public void setFeedbackurl(String feedbackurl) {
		this.feedbackurl = feedbackurl;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getSuggestion() {
		return suggestion;
	}
	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}
	public String getReseon() {
		return reseon;
	}
	public void setReseon(String reseon) {
		this.reseon = reseon;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	public int getSellerRefuse() {
		return sellerRefuse;
	}

	public void setSellerRefuse(int sellerRefuse) {
		this.sellerRefuse = sellerRefuse;
	}

	public int getBuyerReject() {
		return buyerReject;
	}

	public void setBuyerReject(int buyerReject) {
		this.buyerReject = buyerReject;
	}

	public int getFeedbackstatus() {
		return feedbackstatus;
	}

	public void setFeedbackstatus(int feedbackstatus) {
		this.feedbackstatus = feedbackstatus;
	}
	
	
}
