package com.jzwl.xydk.wap.xkh2_4.diplomat.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;
@Repository("diplomatDao")
public class DiplomatDao {
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据openId获取外交官是否存在
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public Map<String, Object> isDiplomatExistence(String xkhOpenId) {
		try {
			StringBuffer sb = new StringBuffer();
			sb.append(" select id,schoolId,schoolName,openId,cityName,phone,wxNumber from `xiaoka-xydk`.diplomatInfo where openId='"+xkhOpenId+"'");
			String sql = sb.toString();
			Map map = baseDAO.queryForMap(sql);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取省市学习信息
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryProviceCitySchoolInfo() {
		try {
			StringBuffer sb = new StringBuffer();
			sb.append(" select id,provice,school,city,`level` from `xiaoka-xydk`.`xk-city-school-info` ");
			String sql = sb.toString();
			List list = baseDAO.queryForList(sql);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取个人信息
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public Map<String, Object> getUserInfo(String xkhOpenId) {
		try {
			String sql = " select wxname,headImgUrl,createTime from v2_wx_customer where openId='"+xkhOpenId+"'";
			Map map = baseDAO.queryForMap(sql);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取外交官下的资源信息
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySource(String xkhOpenId) {
		try {
			String sql = " select ubi.*,si.* from `xiaoka-xydk`.diplomatInfo di left join `xiaoka-xydk`.diplomatSourceInfo dsi on di.openId=dsi.diplomatOpenId "
					+ "	left join `xiaoka-xydk`.user_basicinfo ubi on dsi.sourceUserOpenId = ubi.openId "
					+ "LEFT JOIN `xiaoka`.tg_school_info si ON si.id = ubi.schoolId where di.openId = '"+xkhOpenId+"'";
			List<Map<String, Object>> list = baseDAO.queryForList(sql);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param map
	 * @return
	 * @version
	 */
	public boolean addDiplomatInfo(Map<String, Object> map) {
		map.put("id",  Sequence.nextId());
		map.put("createTime", new Date());
		try {
			String sql = " insert into `xiaoka-xydk`.diplomatInfo (id,userName,schoolId,schoolName,openId,cityName,phone,wxNumber,createTime,cityId) "
					+ "values (:id,:userName,:schoolId,:schoolName,:openId,:cityName,:phone,:wxNumber,:createTime,:cityId) ";
			boolean flag = baseDAO.executeNamedCommand(sql, map);
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取外交官钱包金额
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public Map<String, Object> getWallet(String xkhOpenId) {
		try {
			String sql = " select id, openId, balance, createTime from `xiaoka-xydk`.wallet where openId = '"+xkhOpenId+"' and isDiplomat=1 ";
			return baseDAO.queryForMap(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：查询外交官提现或分成记录
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryDiplomatRecord(String xkhOpenId) {
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("select id, openId, divideOrWithdraw, balanceMoney, state, divideOrWithdrawTime,orderId "
					+ "from `xiaoka-xydk`.diplomat_divide_withdraw where openId = '"+xkhOpenId+"' order by divideOrWithdrawTime desc");
			return baseDAO.queryForList(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取未审核总金额
	 * 创建人： ln
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public Map<String, Object> getMoneyByOpenId(String xkhOpenId) {
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("select IFNULL(sum(withdrawMoney),0) as withdrawMoney from `xiaoka-xydk`.diplomat_Withdrawals "
					+ " where applicationStatus = '0' and openId = '"+xkhOpenId+"'");
			return baseDAO.queryForMap(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：插入提现记录（外交官）
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param paraMap
	 * @return
	 * @version
	 */
	public String withdrawals(Map<String, Object> map) {
		map.put("id",  Sequence.nextId());
		map.put("createTime", new Date());
		map.put("applicationTime", new Date());
		try {
			String sql = " insert into `xiaoka-xydk`.diplomat_Withdrawals (id,openId,withdrawMoney,applicationTime,applicationStatus,createTime) values (:id,:openId,:withdrawMoney,:applicationTime,0,:createTime) ";
			boolean flag = baseDAO.executeNamedCommand(sql, map);
			if(flag){
				return map.get("id").toString();
			}
			System.out.println("------------当前插入记录失败  method name is withdrawals--------------");
			return "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据ID获取提现记录
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param id
	 * @return
	 * @version
	 */
	public Map<String, Object> getWithdrawalsRecordById(String id) {
		try {
			String sql = " select id,openId,withdrawMoney,applicationTime,applicationStatus,checkPeople,refuseReason,checkTime,createTime from `xiaoka-xydk`.diplomat_Withdrawals where id = "+id+" ";
			Map map = baseDAO.queryForMap(sql);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据openId获取用户，用于判断用户是否已经入驻
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param xkhOpenId
	 * @return
	 * @version
	 */
	public Map<String, Object> isSettledByOpenId(String xkhOpenId) {
		try {
			String sql = " select * from  `xiaoka-xydk`.user_basicinfo where openId='"+xkhOpenId+"' ";
			Map<String,Object> map = baseDAO.queryForMap(sql);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：绑定用户与外交官关系
	 * 创建人： ln
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param xkhOpenId  用户openId
	 * @param openId	外交官openId
	 * @return
	 * @version
	 */
	public boolean band(String xkhOpenId, String openId) {
		Map<String,Object> map= new HashMap<String, Object>();
  		map.put("id",  Sequence.nextId());
  		map.put("sourceUserOpenId", xkhOpenId);
  		map.put("diplomatOpenId", openId);
  		map.put("createTime", new Date());
		try {
			String sql = " insert into `xiaoka-xydk`.diplomatSourceInfo (id,diplomatOpenId,sourceUserOpenId,createTime) values (:id,:diplomatOpenId,:sourceUserOpenId,:createTime) ";
			boolean flag = baseDAO.executeNamedCommand(sql, map);
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据微信号查询入驻信息
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param wxNumber
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findByWxNumber(Map<String, Object> map) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM `xiaoka-xydk`.diplomatinfo"
				+ " where wxNumber = :wxNumber and userName = :userName and phone = :phoneNumber "
				+ " and cityId=:cityId and schoolId=:schooId and isDelete=0 ");
		return baseDAO.queryForMap(sb.toString(), map);
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：验证通过插入数据
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param flagMap
	 * @version
	 */
	public String addRecord(Map<String, Object> flagMap) {
		StringBuffer sb = new StringBuffer();
		String id = Sequence.nextId();
		flagMap.put("id", id);
		flagMap.put("createDate", new Date());
		sb.append("insert into `xiaoka-xydk`.diplomatinfo_change_record "
				+ " (id, diplomatinfoId,replacedId, isAgree, createDate) "
				+ " values (:id, :basicId, :userId,0, :createDate )");
		boolean flag = baseDAO.executeNamedCommand(sb.toString(), flagMap);
		if(flag){
			return id;
		}else{
			return null;
		}
	}
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据用户的记录获取用户信息
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getBasicInfoByRecordId(String recordId) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT dio.id,dio.userName,dir.isAgree,dio.openId FROM `xiaoka-xydk`.diplomatinfo dio"
				+" LEFT JOIN `xiaoka-xydk`.diplomatinfo_change_record dir ON dio.id=dir.replacedId  where dio.id = '"+recordId+"'");
		return baseDAO.queryForMap(sb.toString());
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：修改记录
	 * 创建人： dxf
	 * 创建时间： 2017年6月15日
	 * 标记：
	 * @param insertMap
	 * @return
	 * @version
	 */
	public boolean updateChangeManage(Map<String, Object> insertMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("update `xiaoka-xydk`.diplomatinfo_change_record set isAgree = :isAgree where id = :id");
		String isAgree = String.valueOf(insertMap.get("isAgree"));
		if("1".equals(isAgree)){
			String delsql = "delete from `xiaoka-xydk`.diplomatinfo where id = :id";
			baseDAO.executeNamedCommand(delsql, insertMap);
		}
		return baseDAO.executeNamedCommand(sb.toString(), insertMap);
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：根据openId获取入驻信息
	 * 创建人： dxf
	 * 创建时间： 2017年6月16日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findByOpenId(String openId) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM `xiaoka-xydk`.diplomatinfo dio WHERE  openId = '"+openId+"'");
		return baseDAO.queryForMap(sb.toString());
	}
	
	
	
	/**
	 * 返回当前所有省的名称
	 * @return
	 */
	public List<Map<String,Object>> getAllProvince(){
		
		String sql ="select cy.province,cy.number from `xiaoka`.tg_city cy where isDelete = 0 and cy.province is not NULL;";
		
		return baseDAO.getJdbcTemplate().queryForList(sql);
	}
	
	
	/**
	 * 根据当前的省名查询下面市的名称
	 * @author cfz
	 */
	public List<Map<String,Object>> getAllCity(String province){
		
		
		String sql ="select cy.id,cy.cityName as city from xiaoka.tg_city cy where isDelete = 0 and cy.province LIKE '%"+province+"%'";
		//String sql = "select DISTINCT(TRIM(t.city)) as city from `xiaoka-xydk`.`xk-city-school-info` t where t.provice LIKE '%"+province+"%';";
		return baseDAO.getJdbcTemplate().queryForList(sql);
	}
	
	/**
	 * 获取当前城市下的level 211.985 专科 本科等
	 */
	public List<Map<String,Object>> getLevelByCityId(String cityId){
		
		String sql ="select DISTINCT(dd.dic_name) as `level`  from `xiaoka`.tg_school_info t LEFT JOIN `xiaoka-xydk`.schoollabel s on t.id = s.schoolId "
				+ "LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = s.dicDataId  where t.isDelete = '0' and dd.isDelete = '0' and t.cityId = '"+cityId+"'";
		//String sql = "select DISTINCT(t.`level`) from `xiaoka-xydk`.`xk-city-school-info` t where t.city = '"+cityName.trim()+"'";
		return baseDAO.getJdbcTemplate().queryForList(sql);
	}
	
	/**
	 * 获取当前指定城市下指定等级的学校
	 */
	public List<Map<String,Object>> getSchoolByCityAndLevel(String cityId,String level){
		
		String sql ="select t.id,t.schoolName  from `xiaoka`.tg_school_info t LEFT JOIN `xiaoka-xydk`.schoollabel s on t.id = s.schoolId "
				+ "LEFT JOIN `xiaoka`.v2_dic_data dd on dd.id = s.dicDataId  "
				+ "where t.isDelete = '0' and dd.isDelete = '0' and t.cityId = '"+cityId+"' and dd.dic_name LIKE '%"+level+"%'";
		
		return baseDAO.getJdbcTemplate().queryForList(sql);
		
	}
	
	/**
	 * 根据当前的Id值获取单条交易记录的信息
	 */
	public Map<String,Object> getSingleHistory(String id) {
		
		String sql = "select ts.id,ts.divideOrWithdraw,ts.divideOrWithdrawTime,ts.openId,ts.orderId,ts.state from "
				+ "`xiaoka-xydk`.diplomat_divide_withdraw ts where ts.id='"+id+"'";
		
		Map<String,Object>  object = baseDAO.getJdbcTemplate().queryForMap(sql);
		
		return object;
	}
	
	
	/**
	 * 插入记录到 
	 * diplomat_divide_withdraw 外交官提现或分成记录表中
	 * @return
	 */
	public boolean insertDiplomatWithdraw(Map<String,Object> map){
		
		map.put("id", Sequence.nextId());
		map.put("divideOrWithdrawTime", new Date());
		String sql = "insert into  `xiaoka-xydk`.diplomat_divide_withdraw (id,openId,divideOrWithdraw,balanceMoney,state,divideOrWithdrawTime,orderId) values (:id,:openId,:divideOrWithdraw,:balanceMoney,:state,:divideOrWithdrawTime,:orderId)";
		boolean flag = baseDAO.executeNamedCommand(sql, map);
		
		return flag;
	}
	
	/**
	 * 查询当前提供服务方的OpenId是否
	 * 存在 相关联的外交官 有则返回外交官的OpenId
	 * 没有则返回"";
	 * @return
	 */
	public String correlatDiplomat(String sellerOpenId){
		
		try {
			String sql = "select t.diplomatOpenId from `xiaoka-xydk`.diplomatsourceinfo t where t.sourceUserOpenId='"+sellerOpenId+"'; ";
			Map<String,Object> map = baseDAO.getJdbcTemplate().queryForMap(sql);
			return map.get("diplomatOpenId").toString();
		} catch (Exception e) {
			//查不到的异常返回空的字符串
			return "";
		}
		
	}
	
	public boolean updateDiplomatsourceinfo(Map<String,Object> map){
		
		try {
			if(StringUtils.isBlank(map.get("newOpenId").toString())
					&&StringUtils.isBlank(map.get("oldOpenId").toString())){
				return false;
			}
			String sql ="update diplomatsourceinfo set  diplomatOpenId = :newOpenId  where diplomatOpenId = :oldOpenId";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			return false;
		}
		
	}
	
	
	
}
