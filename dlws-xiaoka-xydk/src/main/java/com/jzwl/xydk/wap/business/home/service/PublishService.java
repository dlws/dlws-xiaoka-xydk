/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.common.constant.Constants;
import com.jzwl.xydk.wap.business.home.dao.HomeDao;
import com.jzwl.xydk.wap.business.home.dao.PublishDao;


@Service("pubService")
public class PublishService {
	
	@Autowired
	private PublishDao pubDao;
	@Autowired
	private HomeDao homeDao;
	
	@Autowired
	private Constants constants;
	
	public void pub(Map<String,Object> map){
		pubDao.addPubInfo(map);
	}
	
	public List<Map<String,Object>> getDataType(Map<String,Object> map){
		return pubDao.queryActionType(map);
	}
	
	public List<Map<String,Object>> getCity(){
		return pubDao.queryCity();
	}
	
	public List<Map<String,Object>> getSchool(Map<String,Object> map){
		return pubDao.querySchool(map);
	}
	
	///////////////
	public void sub(Map<String,Object> map){
		pubDao.addOrderInfo(map);
	}
	
	public Map<String, Object> getInfo(Long basicId) {
		return homeDao.getInfo(basicId);
	}
	
	public Map<String, Object> queryHeadImg(String basicId) {
		return pubDao.queryHeadImg(basicId);
	}
	
	public Map<String, Object> querySkillInfo(Map<String, Object> map) {
		
		Map<String, Object> skillInfo = pubDao.querySkillInfo(map);
		skillInfo.put("imgURL",skillInfo.get("imgURL"));
		return skillInfo;
	}
	
	public Map<String, Object> querySkillInfoPC(Map<String, Object> map) {
		
		Map<String, Object> skillInfo = pubDao.querySkillInfo(map);
		skillInfo.put("imgURL",skillInfo.get("imgURL"));
		return skillInfo;
	}
}
