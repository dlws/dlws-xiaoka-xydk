/*
 * Powered By [rapid-framework]
 * Web Site: http://www.rapid-framework.org.cn
 * Google Code: http://code.google.com/p/rapid-framework/
 * Since 2008 - 2016
 */

package com.jzwl.xydk.wap.business.home.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nutz.json.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.cache.RedisService;
import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.page.PageObject;
import com.jzwl.common.utils.ListUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.preferService.service.PreferredServiceService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;
import com.jzwl.xydk.wap.business.home.pojo.Special;
import com.jzwl.xydk.wap.business.home.pojo.UserBasicinfo;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassF;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillclassS;
import com.jzwl.xydk.wap.business.home.pojo.UserSkillinfo;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.service.SkillOrderService;

/**
 * 
 * 描述:首页相关
 * @author    gyp
 * @Date	 2017年6月5日
 */

@Controller("home")
@RequestMapping("/home")
public class HomeController extends BaseWeixinController {

	@Autowired
	private HomeService homeService;
	@Autowired
	private Constants constants;
	@Autowired
	private SkillUserService skillUserService;
	@Autowired
	private SendMessageService sendMessage;
	@Autowired
	private PreferredServiceService preferredServiceService;
	@Autowired
	private SkillOrderService skillOrderService;
	@Autowired
	private EntertypeService entertypeService;
	@Autowired
	private RedisService redisService;

	Logger log = Logger.getLogger(HomeController.class);

	//定义静态变量配置当前页数
	private static final int PAGE_SIZE = 10;
	private static final int PAGE_SIZE_PEOPLE = 10;
	

	/**s
	 * 进入首页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();
		
		//sendMessage.payMoneySuccess("1493111298479000","o3P2SwZCegQ-M6AG4-Nukz5R8gN8","20");

		//String openId = getCurrentOpenId(request); //get  openId
		createParameterMap(request);

		//获取父级分类的集合
		List<UserSkillclassF> userSkillFList = null;
		//获取轮播图的集合
		List<BannerInfo> bannerInfos = null;
		//获取热搜词
		List<Map<String, Object>> hotWords = null;
		//获取推荐到首页服务的集合
		List<UserSkillinfo> skills = null;

		try {
			//轮播图
			bannerInfos = homeService.getBannerInfoList();
			String bannerJson = Json.toJson(bannerInfos);
			//获取一级分类
			userSkillFList = homeService.findUserSkillclassF();
			//String ss = ListUtil.dealBySubList(userSkillFList, 10);
			String jsonSkillClass = Json.toJson(userSkillFList);
			String cla = "{\"classItem\":" + jsonSkillClass + "}";
			//优质资源
			paramsMap.put("ishome", GlobalConstant.TYPE_YES);
			skills = homeService.getPromoteSkillInfoList(paramsMap);
			String jsonSkills = Json.toJson(skills);
			String promote = "{\"promoteSkills\":" + jsonSkills + "}";
			paramsMap.put("start", 0);
			//热搜词管理
			String dicTypeCode = GlobalConstant.DIC_TYPE_CODE;
			hotWords = homeService.getDataByTypeCode(dicTypeCode);

			String homeCount = "";
			Map<String, Object> map = homeService.getUserAllNum();//获取用户总数
			Map<String, Object> map_skill_num = homeService.getUserSkillAllNum();//获取所有技能的总数
			DecimalFormat df = new DecimalFormat("0000");
			homeCount = df.format(Integer.parseInt(map.get("allNum").toString())+Integer.parseInt(map_skill_num.get("allNum").toString()));

			mov.addObject("allNum", homeCount);//入住人数
			mov.addObject("bannerInfo", bannerJson);//轮播图
			mov.addObject("skillClass", cla);//技能一级分类
			mov.addObject("bannerInfos", bannerInfos);
			mov.addObject("hotWords", hotWords);//热搜词
			mov.addObject("promoteSkills", promote);//推荐服务
			mov.setViewName("/wap/xkh_version_2/index_2-1");//跳转到首页
			return mov;

		} catch (Exception e) {
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("/wap/xkh_version_2/index_2-1");
			e.printStackTrace();
			return mov;
		}
	}

	/**
	 * 点击加载更多
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getUserBasicinfo")
	public @ResponseBody Map<String, Object> getUserBasicinfo(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);

		Map<String, Object> map = new HashMap<String, Object>();
		String currentPage = request.getParameter("currentPage");
		//获取用户的集合，根据排列顺序进行显示
		List<UserBasicinfo> usersByOrd = null;
		try {

			int cur = Integer.parseInt(currentPage);
			int start = (cur - 1) * 25;
			paramsMap.put("start", start);
			usersByOrd = homeService.findUserBasicinfo(paramsMap);
			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("list", usersByOrd);
			map.put("num", usersByOrd.size());
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	/**
	 * 获取用户的信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getUserInfo")
	public ModelAndView getUserInfo(HttpServletRequest request, HttpServletResponse response) {

		boolean flage = true;//true is other false is own

		ModelAndView mov = new ModelAndView();

		String openId = getCurrentOpenId(request); //get  openId

		createParameterMap(request);
		//获取轮播图的集合
		List<BannerInfo> bannerInfos = null;
		//获取入住用户的基本信息
		UserBasicinfo userInfo = null;

		try {
			bannerInfos = homeService.getBannerInfoList();//轮播图
			String picUrl = "";
			userInfo = homeService.getUserInfo(paramsMap);

			/*System.out.println("id="+paramsMap.get("id"));
			System.out.println("昵称="+userInfo.getNickname());*/
			if (bannerInfos.size() > 0) {
				int ranNum = (int) (bannerInfos.size() * Math.random());
				picUrl = bannerInfos.get(ranNum).getPicUrl();
			}
			/**区分通过对比openId关联的ID  CFZ begin*/
			if (userInfo != null) {
				if (openId.equals(userInfo.getOpenId())) {
					flage = false;
				}
			}
			mov.addObject("flage", flage);
			//userInfo.setSkillInfos(null);
			/**区分通过个人还是他人进行点击  CFZ end*/
			/**test scene */
			mov.addObject("userInfo", userInfo);
			mov.addObject("id", userInfo.getId());

			if (userInfo.getSkillInfos().size() > 0) {
				mov.addObject("skillId", userInfo.getSkillInfos().get(0).getId());
			}

			if (flage == false && userInfo.getSkillInfos().size() < 1) {
				mov.addObject("picUrl", picUrl);
				mov.setViewName("/wap/xkh_version_2/personNoPub");//跳转到首页
				return mov;
			} else {

				mov.addObject("picUrl", picUrl);
				mov.setViewName("/wap/xkh_version_2/person");//跳转到首页
				return mov;
			}

		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("redirect:/home/index.html");//跳转到首页  
			return mov;
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getClassList")
	public ModelAndView getClassList(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);
		List<UserSkillclassS> skills = null;//子分类的集合
		List<UserSkillinfo> userList = null;//分类用户的集合
		//获取热搜词
		List<Map<String, Object>> hotWords = null;

		try {

			paramsMap.put("start", 0);
			paramsMap.put("pageSize", GlobalConstant.PAGESIZE);

			skills = homeService.getSkillClassS(paramsMap);

			UserSkillclassS skill = new UserSkillclassS();
			skill.setId(Long.parseLong("0"));
			skill.setClassName("全部");

			skills.add(0, skill);

			String jsonSkillClass = Json.toJson(skills);//获取分类下面的子分类
			String skillClass = "{\"navItem\":" + jsonSkillClass + "}";

			String ss = ListUtil.dealSonList(skills, 4);

			userList = homeService.getUserSkillInfo(paramsMap);
			String jsonUserList = Json.toJson(userList);

			//			mov.addObject("skillClass", skillClass);
			String userInfoList = "{\"list\":" + jsonUserList + "}";

			mov.addObject("skillClass", skillClass);
			//mov.addObject("userInfoList", userInfoList);
			mov.addObject("searchValue", paramsMap.get("topValue"));
			if (paramsMap.get("searchValue") != null) {
				mov.addObject("searchValue", paramsMap.get("searchValue"));
			}

			if (paramsMap.get("topValue") != null || paramsMap.get("searchValue") != null) {
				mov.addObject("isPage", "0");
			}
			List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表
			List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
			for (int i = 0; i < provnceList.size(); i++) {
				String provnce = String.valueOf(provnceList.get(i).get("province"));
				Map<String, Object> map = new HashMap<String, Object>();
				// 获取市信息
				List<Map<String, Object>> CityByProvnce = skillUserService.queryCityListVersion2(provnce);
				if (CityByProvnce != null && CityByProvnce.size() > 0) {
					map.put("value", provnce);// 市
					map.put("text", provnce);
					map.put("children", CityByProvnce);
				}
				proList.add(map);
			}
			String jsonProList = JSONObject.toJSONString(proList);
			List<Map<String, Object>> schoolLabels = skillUserService.getSchoolGrade();

			//热搜词管理
			String dicTypeCode = GlobalConstant.DIC_TYPE_CODE;
			hotWords = homeService.getDataByTypeCode(dicTypeCode);

			mov.addObject("proList", jsonProList);

			mov.addObject("labelList", schoolLabels);
			mov.addObject("fid", paramsMap.get("fid"));
			mov.addObject("fid2", paramsMap.get("fid"));

			mov.addObject("ss", ss);
			mov.addObject("skillClass", skills);
			mov.addObject("hotWords", hotWords);
			//跳转到二级分类
			mov.setViewName("/wap/xkh_version_2/class_list");
			//mov.setViewName("/wap/xkh_version_2_3/searchSecond");
			return mov;

		} catch (Exception e) {
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("redirect:/home/index.html");
			e.printStackTrace();
			return mov;
		}
	}

	/**
	 * 根据分类的id获取入主用户的信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/claass_list")
	public @ResponseBody Map<String, Object> claass_list(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);

			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	/**
	 * 根据分类的id获取入主用户的信息2.0
	 * @param request
	 * @param response
	 * @return
	 */

	@RequestMapping(value = "/getUserInfoByClassIdV2")
	public @ResponseBody Map<String, Object> getUserInfoByClassIdV2(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);

		List<UserSkillinfo> skills = null;//分类用户的集合
		Map<String, Object> map = new HashMap<String, Object>();
		String currentPage = request.getParameter("currentPage");
		try {

			int cur = Integer.parseInt(currentPage);
			//int pageSize = GlobalConstant.PAGESIZE;
			int pageSize = Integer.parseInt(paramsMap.get("pageNum").toString());
			int start = (cur - 1) * pageSize;
			paramsMap.put("start", start);
			paramsMap.put("pageSize", pageSize);
			skills = homeService.getUserSkillInfo(paramsMap);

			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("num", skills.size());
			map.put("skills", skills);
			map.put("sid", paramsMap.get("sid"));

			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	/**
	 * 根据分类的id获取入主用户的信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getUserInfoByClassId")
	public @ResponseBody Map<String, Object> getUserInfoByClassId(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);

		List<UserSkillinfo> skills = null;//分类用户的集合
		Map<String, Object> map = new HashMap<String, Object>();
		String currentPage = request.getParameter("currentPage");
		try {

			int cur = Integer.parseInt(currentPage);
			int start = (cur - 1) * 2;
			paramsMap.put("start", start);
			skills = homeService.getUserSkillInfo(paramsMap);
			String jsonUserList = Json.toJson(skills);
			String userInfoList = "{\"list\":" + jsonUserList + "}";

			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("userInfoList", userInfoList);
			map.put("num", skills.size());
			map.put("sid", paramsMap.get("sid"));

			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	/**
	 * 
	 * 二级页面
	 * <p>
	 * 参数-id
	 * @param request
	 * @param response
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	@RequestMapping(value = "/getSkillInfo")
	public ModelAndView getSkillInfo(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();
		boolean flage = true;//true 表示他人查阅  false 表示本人查阅
		String openId = getCurrentOpenId(request); //get  openId
		createParameterMap(request);

		UserSkillinfo skillInfo = null;//分类用户的集合

		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> v_map = new HashMap<String, Object>();
		try {

			String isPreferred = String.valueOf(paramsMap.get("isPreferred"));
			if("1".equals(isPreferred)){
				Map<String, Object> preferredService = homeService.getPreInfoById(paramsMap);
				UserBasicinfo base = homeService.getUserBasicInfo(paramsMap);
				mov.addObject("skillInfo",preferredService);//优质服务详情
				mov.addObject("baseInfo",base);//个人基本信息
				mov.setViewName("/wap/xkh_version_2/peferServ_detail");//跳转到优选服务详情
				return mov;
			}
			skillInfo = homeService.getSkillInfo(paramsMap);
			if (skillInfo.getImages().size() > 0) {
				map.put("firstPic", skillInfo.getImages().get(0));
				map.put("lastPic", skillInfo.getImages().get(skillInfo.getImages().size() - 1));
				//	mov.addObject("imgUrl", skillInfo.getImages().get(0).getImgUrl());//轮播图

			}

			if (StringUtils.isNotEmpty(skillInfo.getVideoUrl())) {//有视频,对视屏的轮播进行设置
				v_map.put("v_firstPic", skillInfo.getVideoImg().get(0));
				v_map.put("v_lastPic", skillInfo.getVideoImg().get(skillInfo.getVideoImg().size() - 1));
				//mov.addObject("v_imgUrl", skillInfo.getVideoImg().get(0).getImgUrl());//轮播图
			}
			//1.区分是否是用户本人的技能
			
			//获取当前用户ID
			UserBasicinfo userInfo = homeService.getUserBasicInfo(skillInfo.getBasicId().toString());
			
			if(openId.equals(userInfo.getOpenId().toString())){
				
				flage = false;//更改为本人查阅
			}
			//2.增加加入已选和立即下单功能
		
			Map<String,Object> mapper = new HashMap<String, Object>();
			if(userInfo.getEnterId()==null||StringUtils.isBlank(userInfo.getEnterId().toString())){//如果入驻类型为空，默认高校社团的enterID
				
				Map<String,Object> mapCurren = new HashMap<String, Object>();
				mapCurren.put("typeValue","gxst");
				String enterId = entertypeService.getByEnterValue(mapCurren).get("id").toString();//获取高校社团的enterId
				userInfo.setEnterId(Long.parseLong(enterId));
			}
			mapper.put("enterId", userInfo.getEnterId());
			Map<String,Object> enterMap = entertypeService.getByEnterId(mapper);  //查询enter的信息
			map.put("list", skillInfo.getImages());
			v_map.put("v_list", skillInfo.getVideoImg());
			String jBanner = Json.toJson(map);
			String v_imgs = Json.toJson(v_map);
			mov.addObject("bannerInfo", skillInfo.getImages());//轮播图
			mov.addObject("count", skillInfo.getImages().size());
			mov.addObject("skillInfo", skillInfo);
			mov.addObject("v_imgs", v_imgs);
			mov.addObject("flage", flage);//表示为本人进行查阅
			mov.addObject("typeValue",enterMap.get("typeValue"));//回传当前的typeValue加入已选使用
			mov.addObject("userInfo",userInfo);
			mov.setViewName("/wap/xkh_version_2/personal_details");//跳转到首页    businessActivity
			return mov;

		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("redirect:/home/index.html");//跳转到首页 
			return mov;
		}
	}

	/**
	 * 获取首页的商品xinx
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getUserSkillInfo")
	public @ResponseBody Map<String, Object> getUserSkillInfo(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> map = new HashMap<String, Object>();
		String openId = getCurrentOpenId(request); //get  openId

		String currentPage = request.getParameter("currentPage");
		int cur = Integer.parseInt(currentPage);

		List<UserSkillinfo> userList = null;
		try {
			paramsMap.put("isRecommend", 1);
			paramsMap.put("start", GlobalConstant.Global_PAGESIZE * (cur - 1));

			userList = homeService.getUserSkillInfo(paramsMap);
			String jsongood = Json.toJson(userList);
			String goodString = "{\"list\":" + jsongood + "}";
			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("goodString", goodString);
			map.put("num", userList.size());

			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	/**
	 * 
	 * 获取用户的微信头像openId
	 * 及是否入驻信息
	 * 返回到我的页面 
	 * @return 
	 *  
	 */
	@RequestMapping(value = "/getmyInform")
	public ModelAndView getmyInform(HttpServletRequest request, HttpServletResponse response) {

		
		String fromURL = request.getHeader("Referer");        
		System.out.println("-------------来源于：--------------"+fromURL);  
		
		if(fromURL.contains("dlws-xiaoka-xydk")){
			
			System.out.println("---------当前项目已调用此方法------");
		}
		
		ModelAndView mov = new ModelAndView();
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		//		String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
		try {
			if (!"".equals(openId) && !"null".equals(openId) && openId != null) {
				Map<String, Object> map = homeService.getCustomerByOpenId(openId);
				Map<String, Object> reduMap = homeService.getPersonInform(openId);
				boolean exist = true;
				exist = homeService.getIsExist(openId);
				if (exist == false) {
					map.put("auditState", "3");
				}
				mov.addObject("map", map);
				mov.addObject("reduMap", reduMap);
				mov.setViewName("/wap/xkh_version_2_2/buyer");
				return mov;
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "当前用户授权异常！");
			mov.setViewName("redirect:/home/index.html");//跳转到首页 
		}
		return mov;

	}

	/**
	 * 已通过审核
	 * 个人空间-一级页面-已发布的服务
	 * 以及未发布的服务
	 * @return
	 */
	@RequestMapping(value = "/getPerService")
	public ModelAndView getPerService(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();
		Map<String, Object> map = null;
		List<String> list = new ArrayList<String>();

		String openId = getCurrentOpenId(request);
		UserBasicinfo userInfo = homeService.getUserBaseInfoByOpenId(openId);
		if (userInfo != null) {

			String Userid = userInfo.getId().toString();
			String skillId = homeService.getSkillIdById(Userid);
			map = skillUserService.getSkillInfoById(skillId);
			List<Map<String, Object>> imageList = skillUserService.getSkillImgBySkillList(skillId);
			mov.addObject("map", map);
			mov.addObject("userInfo", userInfo);
			mov.addObject("imageList", imageList);
			mov.setViewName("/wap/xkh_version_2/person_space_one.jsp");
			return mov;
		}
		//未查询到当前用户信息
		request.setAttribute("msg", "未查询到当前用户信息！");
		mov.setViewName("/home/index.html");
		return mov;
	}

	/**
	 * 去往修改个人信息页面
	 * 作为用户回显的时候使用
	 * @param paramsMap
	 * @return map
	 * dxf
	 */
	@RequestMapping(value = "/editor")
	public ModelAndView getByopenId(HttpServletRequest request, HttpServletResponse response) {
		createParameterMap(request);
		ModelAndView mov = new ModelAndView();
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		Map<String, Object> map = new HashMap<String, Object>();
		paramsMap.put("openId", openId);
		try {
			map = homeService.getByopenId(paramsMap);
			mov.addObject("map", map);
			mov.setViewName("/wap/xkh_version_2/editor");
			return mov;
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("/home/index.html");
			e.printStackTrace();
			return mov;
		}
	}

	/**
	 * 修改个人基本信息
	 * dxf
	 * @param map
	 * @return flag
	 */
	@RequestMapping(value = "/updatePersonInform")
	public ModelAndView updateSpecial(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();
		createParameterMap(request);
		String openId = String.valueOf(request.getSession().getAttribute("dkOpenId"));
		paramsMap.put("openId", openId);

		try {
			boolean flag = homeService.upPersonInform(paramsMap);
			if (!flag) {
				mov.addObject("msg", "修改失败!");
				mov.setViewName("/home/index.html");
			} else {
				mov.setViewName("redirect:/home/getmyInform.html");//跳转到首页    businessActivity
			}
		} catch (Exception e) {
			e.printStackTrace();
			mov.addObject("msg", "修改失败!");
			mov.setViewName("/home/index.html");
		}

		return mov;
	}

	/**
	 * TODO 落实拆分方法
	 * 更多优质资源  第一次进行展示固定展10条数据
	 * 创建时间： 2017年2月22日16:39:10
	 * 创建人：cfz
	 * @param end：标记当前页
	 * @return
	 */
	@RequestMapping(value = "/moreResources")
	public ModelAndView getMoreResource(HttpServletRequest request, HttpServletResponse response) {
		PageObject po = new PageObject();
		ModelAndView mov = new ModelAndView();
		try {
			mov.setViewName("/wap/xkh_version_2/more");
		} catch (Exception e) {

			log.error("查询更多" + e.getMessage());
			mov.addObject("msg", "查询更多失败");
			mov.setViewName("/home/index.html");
		}

		return mov;
	}

	/**
	 * 更多大咖
	 * 创建时间：2017年2月22日20:16:50
	 * 创建人：cfz
	 * @return
	 */
	@RequestMapping(value = "/morePeople")
	public ModelAndView getMorePeople(HttpServletRequest request, HttpServletResponse response) {
		int start = 0;
		ModelAndView mov = new ModelAndView();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("start", start);
			map.put("end", PAGE_SIZE_PEOPLE);
			List<UserBasicinfo> users = homeService.findMorePeople(map);//展示一页的内容
			int pageCount = homeService.getCountPeople();
			if (pageCount % PAGE_SIZE > 0) {
				pageCount = (pageCount / PAGE_SIZE) + 1;
			}
			mov.addObject("users", users);
			mov.addObject("pageCount", pageCount);//将总页数回传至前台
			mov.setViewName("/wap/xkh_version_2/more_new_two");
		} catch (Exception e) {
			log.error("查询最新大咖失败" + e.getMessage());
			e.printStackTrace();
			mov.addObject("msg", "服务异常");
			mov.setViewName("/home/index.html");
		}
		return mov;
	}

	/**
	 * 我的-校园资源入驻
	 * 创建人：cfz
	 * 创建时间：2017年2月23日19:04:43
	 * 作为中间跳转页面用无逻辑进行处理
	 */
	@RequestMapping(value = "/transit")
	public String turnEnter() {

		return "/wap/xkh_version_2/transit";
	}

	/**
	 * TODO 落实拆分方法
	 * 更多优质资源  
	 * 创建时间： 2017年2月22日16:39:10
	 * 创建人：cfz
	 * @param end：标记当前页
	 * @return
	 */
	@RequestMapping(value = "/moreResourceAjax")
	public @ResponseBody Map<String, Object> getResourceAjax(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> map = new HashMap<String, Object>();

		try {
			int pageCurrent = 0;
			if (request.getParameter("pageCurrent") != null) {
				pageCurrent = Integer.valueOf(request.getParameter("pageCurrent"));
			}
			createParameterMap(request);
			paramsMap.put("start", pageCurrent * PAGE_SIZE);//开始条数
			paramsMap.put("end", PAGE_SIZE);//结束条数

			List<Special> specials = homeService.getMoreSpecial(paramsMap);

			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("num", specials.size());
			map.put("specials", specials);

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}

		return map;
	}

	/**
	 * 更多大咖
	 * AJAX 实行页面刷新
	 * 创建时间：2017年2月22日20:16:50
	 * 创建人：cfz
	 * @return
	 */
	@RequestMapping(value = "/morePeopleAjax")
	public @ResponseBody PageObject getPeopleAjax(HttpServletRequest request, HttpServletResponse response) {

		int pageCurrent = 0;
		Map map = new HashMap<String, String>();
		if (request.getParameter("pageCurrent") != null) {
			pageCurrent = Integer.valueOf(request.getParameter("pageCurrent"));
		}
		System.out.println("当前页面：" + pageCurrent);
		map.put("start", pageCurrent * PAGE_SIZE_PEOPLE);
		map.put("end", PAGE_SIZE_PEOPLE);
		int pageCount = homeService.getCountPeople();
		if (pageCount % PAGE_SIZE > 0) {
			pageCount = (pageCount / PAGE_SIZE) + 1;
		}
		System.out.println("-----pageCount:-----" + pageCount);
		PageObject pog = new PageObject();
		if (pageCurrent < pageCount) {
			List<UserBasicinfo> users = homeService.findMorePeople(map);//展示
			List<Map<String, String>> lists = new ArrayList<Map<String, String>>();
			//由于UserBasicinfo此对象问题转换json失败
			for (UserBasicinfo userBasicinfo : users) {
				Map<String, String> maps = new HashMap<String, String>();
				maps.put("id", userBasicinfo.getId().toString());
				maps.put("headPortrait", userBasicinfo.getHeadPortrait());
				maps.put("userName", userBasicinfo.getUserName());
				maps.put("schoolName", userBasicinfo.getSchoolName());
				lists.add(maps);
			}

			pog.setAbsolutePage(pageCount);
			pog.setCurrentPage(pageCurrent);
			pog.setDatasource(lists);
		}
		System.out.println(pog.getDatasource());

		return pog;
	}

	/**
	 * 根据分类的id获取入主用户的信息2.0
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/moreNewPeopleAjax")
	public @ResponseBody Map<String, Object> moreNewPeopleAjax(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			/*int cur = Integer.parseInt(currentPage);
			int pageSize = GlobalConstant.PAGESIZE;
			int start = (cur - 1) * pageSize;
			paramsMap.put("start", start);
			paramsMap.put("pageSize", pageSize);*/

			int pageCurrent = 0;
			if (request.getParameter("pageCurrent") != null) {
				pageCurrent = Integer.valueOf(request.getParameter("pageCurrent"));
			}
			//			System.out.println("当前页面："+pageCurrent);
			map.put("start", pageCurrent * PAGE_SIZE_PEOPLE);
			map.put("end", PAGE_SIZE_PEOPLE);
			int pageCount = homeService.getCountPeople();
			if (pageCount % PAGE_SIZE > 0) {
				pageCount = (pageCount / PAGE_SIZE) + 1;
			}
			//			System.out.println("-----pageCount:-----" + pageCount);

			List<UserBasicinfo> users = homeService.findMorePeople(map);//展示

			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("num", users.size());
			map.put("users", users);

			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	/***
	 * 以AJAX的方式展示用户查看的数量
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toViewNum")
	public @ResponseBody String toViewNum(HttpServletRequest request, HttpServletResponse response) {
		String id = "";
		if (StringUtils.isNotEmpty(request.getParameter("id"))) {
			id = request.getParameter("id").toString();
		}
		homeService.toAddViewNum(id);
		return homeService.toShowJump(id);
	}

	/**
	 * 根据分类的id获取入主用户的信息2.0
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/searchSkill")
	public @ResponseBody Map<String, Object> searchSkill(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);

		List<UserSkillinfo> skills = null;//分类用户的集合
		Map<String, Object> map = new HashMap<String, Object>();
		String currentPage = request.getParameter("currentPage");
		try {

			int cur = Integer.parseInt(currentPage);
			int pageSize = Integer.valueOf(request.getParameter("pageNum"));
			int start = (cur - 1) * pageSize;
			paramsMap.put("start", start);
			paramsMap.put("pageSize", pageSize);
			skills = homeService.getUserSkillInfo(paramsMap);

			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("num", skills.size());
			map.put("skills", skills);
			map.put("sid", paramsMap.get("sid"));

			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}

	/**
	 * 首页点击搜索进入搜索页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getClassListSeach")
	public ModelAndView getClassListSeach(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mov = new ModelAndView();

		createParameterMap(request);
		List<UserSkillinfo> userList = null;//分类用户的集合
		//获取热搜词
		List<Map<String, Object>> hotWords = null;

		try {

			paramsMap.put("start", 0);
			paramsMap.put("pageSize", GlobalConstant.PAGESIZE);

			userList = homeService.getUserSkillInfo(paramsMap);
			mov.addObject("userList",userList);
			String jsonUserList = Json.toJson(userList);

			String userInfoList = "{\"list\":" + jsonUserList + "}";

			mov.addObject("userInfoList", userInfoList);
			mov.addObject("searchValue", paramsMap.get("topValue"));
			if (paramsMap.get("searchValue") != null) {
				mov.addObject("searchValue", paramsMap.get("searchValue"));
			}

			if (paramsMap.get("topValue") != null || paramsMap.get("searchValue") != null) {
				mov.addObject("isPage", "0");
			}
			List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();// 省市列表
			List<Map<String, Object>> provnceList = skillUserService.queryProviceListVersion2();
			for (int i = 0; i < provnceList.size(); i++) {
				String provnce = String.valueOf(provnceList.get(i).get("province"));
				Map<String, Object> map = new HashMap<String, Object>();
				// 获取市信息
				List<Map<String, Object>> CityByProvnce = skillUserService.queryCityListVersion2(provnce);
				if (CityByProvnce != null && CityByProvnce.size() > 0) {
					map.put("value", provnce);// 市
					map.put("text", provnce);
					map.put("children", CityByProvnce);
				}
				proList.add(map);
			}

			String jsonProList = JSONObject.toJSONString(proList);

			List<Map<String, Object>> schoolLabels = skillUserService.getSchoolGrade();
			Map<String, Object> mapLabel = new HashMap<String, Object>();
			mapLabel.put("text", "全部");
			mapLabel.put("value", null);
			schoolLabels.add(0, mapLabel);
			String jsonLabelList = JSONObject.toJSONString(schoolLabels);

			//热搜词管理
			String dicTypeCode = GlobalConstant.DIC_TYPE_CODE;
			hotWords = homeService.getDataByTypeCode(dicTypeCode);

			mov.addObject("proList", jsonProList);

			mov.addObject("labelList", jsonLabelList);
			mov.addObject("fid", paramsMap.get("fid"));
			mov.addObject("fid2", paramsMap.get("fid"));

			mov.addObject("hotWords", hotWords);
			String s = String.valueOf(paramsMap.get("seachValue"));
			if (s.length() == 0) {
				mov.addObject("seachValue", "null");
			} else {
				mov.addObject("map", paramsMap);
			}

			//mov.setViewName("/wap/xkh_version_2/class_list_search");
			mov.setViewName("/wap/xkh_version_2_3/search");
			return mov;

		} catch (Exception e) {
			request.setAttribute("msg", "查询失败！");
			mov.setViewName("redirect:/home/index.html");
			e.printStackTrace();
			return mov;
		}
	}
	
	/**
	 * 根据分类的id获取入主用户的信息2.0
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/searchBasicUser")
	public @ResponseBody Map<String, Object> searchBasicUser(HttpServletRequest request, HttpServletResponse response) {

		createParameterMap(request);

		List<UserBasicinfo> users = null;//获取用户的集合
		Map<String, Object> map = new HashMap<String, Object>();
		String currentPage = request.getParameter("currentPage");
		try {

			int cur = Integer.parseInt(currentPage);
			int pageSize = Integer.valueOf(request.getParameter("pageNum"));
			int start = (cur - 1) * pageSize;
			paramsMap.put("start", start);
			paramsMap.put("pageSize", pageSize);
			users = homeService.getBasicUserInfo(paramsMap);

			map.put("msg", "获取信息成功");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			map.put("num", users.size());
			map.put("users", users);
			map.put("seachValue", map.get("seachValue"));
			map.put("sid", paramsMap.get("sid"));

			return map;

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "获取信息失败！");
			map.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			return map;
		}
	}
	
	
	
	

}
