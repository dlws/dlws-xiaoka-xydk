/**
 * UserSettledService.java
 * com.jzwl.xydk.wap.xkh2_3.user.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_3.user.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.xkh2_2.ordseach.dao.OrderSeaDao;
import com.jzwl.xydk.wap.xkh2_3.user.dao.UserSettledDao;

/**
 * TODO(这里用一句话描述这个类的作用)
 * @author   gyp
 * @Date 2017年5月9日
 * @Time 下午7:04:43
 */
@Service("userSettledService")
public class UserSettledService {
	
	@Autowired
	private UserSettledDao  userSettledDao;
	
	/**
	 * 描述:判断用户是否已经入驻
	 * 作者:gyp
	 * @Date	 2017年5月9日
	 */
	public Map<String,Object> getUserBaseInfo(String openId) {
		
		Map<String,Object> map = userSettledDao.getUserBaseInfo(openId);
		return map;
	}
	/**
	 * 描述:添加用户接口
	 * 作者:gyp
	 * @Date	 2017年5月10日
	 */
	public boolean addUserBaseInfo(Map<String,Object> map){
		boolean flag = userSettledDao.addUserBaseInfo(map);
		return flag;
	}
	
	/**
	 * 描述:添加用户接口
	 * 作者:gyp
	 * @Date	 2017年5月10日
	 */
	public boolean editUserBaseInfo(Map<String,Object> map){
		boolean flag = userSettledDao.editUserBaseInfo(map);
		return flag;
	}

}

