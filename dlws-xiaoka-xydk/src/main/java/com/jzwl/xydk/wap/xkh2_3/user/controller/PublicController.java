/**
 * PublicController.java
 * com.jzwl.xydk.wap.xkh2_3.user.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_3.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.jzwl.common.constant.GlobalConstant;
import com.jzwl.common.utils.StringUtil;
import com.jzwl.system.base.controller.BaseController;
import com.jzwl.xydk.manager.baseInfoSkill.controller.BaseInfoSkillController;
import com.jzwl.xydk.manager.baseInfoSkill.service.BaseInfoSkillService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.wap.xkh2_3.user.service.PublicService;
import com.jzwl.xydk.wap.xkhV2.cdrz.controller.FieldEnterController;

/**
 * TODO这个方法写公共的方法，比如：根据这个跳转到不同的服务详情。有公共的也在这里面进行显示
 *
 * @author   gyp
 * @Date 2017年5月9日
 * @Time 下午6:22:30
 */
@Controller
@RequestMapping(value="/pubSkill")
public class PublicController  extends BaseController{

	@Autowired
	private BaseInfoSkillService baseInfoSkillService;
	
	@Autowired
	private PublicService publicService;
	
	@Autowired
	private FieldEnterController pieldEnterController;
	
	@Autowired
	private UserBasicinfoService userBasicinfoService;
	
	@Autowired
	private BaseInfoSkillController baseInfoSkillController;
	
	private final String COMPARE_SIZE = "cooperaType";// 合作类型
	
	private final String FIELD_TYPE = "fieldtype";//场地类型
	private final String FILED_SIZE = "filedsize";//场地规模
	private final String BASIC_MATERIAL = "basicmaterial";//基本物资配备
	private final String PARTIME_JOB = "partimejob";//基本物资配备
	
	/**
	 * 去高校社团发布技能页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toPubSkillMan")
	public String toAddUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		List<Map<String, Object>> classF = baseInfoSkillService.getClassF();
		String sid = "";
		for(int i=0;i<classF.size();i++){
			String classValue = String.valueOf(classF.get(i).get("classValue"));
				if("gxst".equals(classValue)){
					sid =  String.valueOf(classF.get(i).get("id"));
			}
		}
		paramsMap.put("sid", sid);
		List<Map<String,Object>> sonList = publicService.getClassSBySid(paramsMap);//服务类型
		//获取单位字典表项
		List<Map<String, Object>> canpanyLists = publicService.getCanpanyMap();
		String canpanyList = JSONObject.toJSONString(canpanyLists);
		
		model.addAttribute("canpanyList",  canpanyList);
		model.addAttribute("classList", classF);
		model.addAttribute("sonList", JSONObject.toJSONString(sonList));
		model.addAttribute("paramsMap", paramsMap);
		return "/wap/xkh_version_2_3/publishskill/publishService_community";

	}
	

	
	//根据发布类型跳转到不同页面
	@RequestMapping(value = "/chosePubSkill")
	public String chosePubSkill(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		List<Map<String, Object>> classF = baseInfoSkillService.getClassF();//发布类型
		
		List<Map<String,Object>> sonList = publicService.getClassSBySid(paramsMap);//服务类型
		model.addAttribute("sonEditList", sonList);
//		Map<String, Object> objMap = baseInfoSkillService.querySkillById(paramsMap);//查询技能主表信息
		
		if(paramsMap.get("skillId")!=""&&paramsMap.get("skillId")!="null"){
			model = baseInfoSkillController.skillDetail(model,paramsMap);
		}
		//获取单位字典表项
		List<Map<String, Object>> canpanyLists = publicService.getCanpanyMap();
		model.addAttribute("canpanyEdit", canpanyLists);//单位
		String canpanyList = JSONObject.toJSONString(canpanyLists);
		//model = pieldEnterController.toShowbasic(model);//加载修改需要的公共的基础信息
		
		
		//加载当前的场地类型（字典表）fieldtype
		List<Map<String, Object>> fieldTypeList = userBasicinfoService.getFieldtype(FIELD_TYPE);
		//加载当前的场地规模（字典表） filedsize
		List<Map<String, Object>> filedSizeList = userBasicinfoService.getFieldtype(FILED_SIZE);
		model.addAttribute("filedSizeEdList", filedSizeList);
		//加载当前的基本物资配备（字典表）basicmaterial 
		List<Map<String, Object>> materialList = userBasicinfoService.getFieldtype(BASIC_MATERIAL);
		
		//加载是否兼职字典表partime_job
		List<Map<String, Object>> partimejobList = userBasicinfoService.getFieldtype(PARTIME_JOB);
		model.addAttribute("fieldTypeList", StringUtil.dealList(fieldTypeList));//场地类型
		model.addAttribute("filedSizeList", StringUtil.dealList(filedSizeList));//场地规模
		model.addAttribute("partimejobList", StringUtil.dealList(partimejobList));//是否提供兼职
		model.addAttribute("fieldTypeLists", fieldTypeList);//场地类型 update 加载基础参数
		model.addAttribute("filedSizeLists", filedSizeList);//场地规模 update 加载基础参数
		model.addAttribute("partimejobLists", partimejobList);//是否提供兼职 update 加载基础参数
		model.addAttribute("materialList", materialList);//基本物资配备
		List<Map<String, Object>> cooperaTypeList = userBasicinfoService
				.getFieldtype(COMPARE_SIZE);// 合作类型
		model.addAttribute("classList", classF);
		model.addAttribute("sonList", JSONObject.toJSONString(sonList));
		model.addAttribute("canpanyList", canpanyList);
		model.addAttribute("cooperaTypeList", cooperaTypeList);//合作形式
		model.addAttribute("paramsMap", paramsMap);
//		model.addAttribute("objMap", objMap);
		
		String enterTypeName = String.valueOf(paramsMap.get("classValue"));
		model.addAttribute("enterTypeName", enterTypeName);
		if (GlobalConstant.ENTER_TYPE_JNDR.equals(enterTypeName)) {
			return "/wap/xkh_version_2_3/publishskill/publishService_acitive";//技能达人
		}else if(GlobalConstant.ENTER_TYPE_GXST.equals(enterTypeName)){
			return "/wap/xkh_version_2_3/publishskill/publishService_community";//高校社团
		}else if (GlobalConstant.ENTER_TYPE_CDZY.equals(enterTypeName)) {
			return "/wap/xkh_version_2_3/publishskill/publishService_place";//场地
		} else if (GlobalConstant.ENTER_TYPE_XNSJ.equals(enterTypeName)) {
			return "/wap/xkh_version_2_3/publishskill/publishService_school";//校内商家
		} else if (GlobalConstant.ENTER_TYPE_SQDV.equals(enterTypeName)) {
			return "/wap/xkh_version_2_3/publishskill/publishService_association";//社群大V技能设置
		}
		return "/wap/xkh_version_2/index_2-1";

	}
	
	/**
	 * 添加UserSkillinfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add")
	public String addUserSkillinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		String cooperaTypeJson="";  //合作形式和价格
		String cooperaCompJson="";  //合作形式和单位
		Map<String,Object> map =  createParameterMap(request);
		//处理合作形式和价格
		if(request.getParameterValues("cooperaType")!=null&&request.getParameterValues("cooperaPrice")!=null){//设置当前场地的属性
			
			String [] cooperaTyArray = request.getParameterValues("cooperaType");//合作形式
			String [] cooperaPrArray = request.getParameterValues("cooperaPrice");//报价
			
		    cooperaTypeJson = StringUtil.dealArray(cooperaTyArray,cooperaPrArray);
		    map.put("cooperaType",cooperaTypeJson);//封装合作形式和价格
		}
		//处理合作形式和单位
		if(request.getParameterValues("cooperaType")!=null&&request.getParameterValues("cooperaCompany")!=null){
			
			String [] cooperaTyArray = request.getParameterValues("cooperaType");//合作形式
			String [] cooperaCompArray = request.getParameterValues("cooperaCompany");//单位
			
			cooperaCompJson = StringUtil.dealArray(cooperaTyArray,cooperaCompArray);
			map.put("cooperaCompany",cooperaCompJson);//封装合作形式和单位
		}
		String skillId = String.valueOf(paramsMap.get("skillId"));
		if(skillId!=""&&skillId!=null&&skillId!="null"){
			baseInfoSkillService.updateUserSkillinfo(paramsMap);
		}else{
			baseInfoSkillService.addUserSkillinfo(paramsMap);
		}
		
		return "redirect:/skillUser/toSkillInfoV2.html";
	}
	
	/**
	 * 删除图片   先取出后更新
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delImage")
	public @ResponseBody Map delImage(HttpServletRequest request, HttpServletResponse response) {
		createParameterMap(request);
		Map<String, Object> map = new HashMap<String, Object>();

		Map<String, Object> image = publicService.getSkillDetail(paramsMap);//获取子表中的信息
		String imgString = image.get("typeValue").toString();//活动路径
		String imge = paramsMap.get("imageUrl") + ",";

		imgString.replaceAll(imge, "");
		map.put("typeValue", imgString.replaceAll(imge, ""));
		map.put("imgeId", paramsMap.get("imgeId"));
		boolean flag = publicService.updateSkillDetail(map);
		map.put("flag", flag);
		return map;
	}

}

