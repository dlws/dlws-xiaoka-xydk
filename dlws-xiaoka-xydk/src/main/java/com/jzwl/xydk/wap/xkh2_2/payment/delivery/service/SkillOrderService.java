package com.jzwl.xydk.wap.xkh2_2.payment.delivery.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.dao.SkillOrderDao;

@Service("SkillOrderService")
public class SkillOrderService {
	@Autowired
	private SkillOrderDao orderDao;
	@Autowired
	private HomeService homeService;
	
	public boolean joinSelected(Map<String, Object> map) {
		return orderDao.joinSelected(map);

	}
	/**
	 * 
	 * 描述:修改加入已选的数量
	 * 作者:gyp
	 * @Date	 2017年6月29日
	 */
	public boolean modifyBuyNum(Map<String, Object> map) {
		return orderDao.modifyBuyNum(map);

	}
	
	public List<Map<String,Object>> getSeclectSkills(Map<String, Object> map) {
		
			List<Map<String,Object>> list = orderDao.querySelected(map);
			return list;
	}

	public List<Map<String,Object>> querySelected(Map<String, Object> map) {
			int index = 0;
			List<Map<String,Object>> list = orderDao.querySelected(map);
			Map<String,Object> enter_map = orderDao.getEntertype(map);
			List<Integer> deleteIndex = new ArrayList<Integer>();
			List<String> deleteCondition = new ArrayList<String>();
			
			for(Map<String,Object> m : list){
				//System.out.println("***************************************enterType："+m.get("enterType"));
				
				//获取价格和当前图片信息
				if(m.get("skillId")!=null&&"0".equals(m.get("isPreferred").toString())){
					
					//记录当前需要处理的问题数据
					if(StringUtils.isBlank(String.valueOf(m.get("enterType")))||
							StringUtils.isBlank(String.valueOf(m.get("skillId")))||
							"null".equals(String.valueOf(m.get("enterType")))){
						deleteIndex.add(index);
						deleteCondition.add(m.get("seledId").toString());
						continue;
					}
					try {
						String imgUrl = homeService.getImageUrl(m.get("enterType").toString(),
								Long.parseLong(m.get("skillId").toString()));
						String price = homeService.getPrice(m.get("enterType").toString(),
								Long.parseLong(m.get("skillId").toString()));
						m.put("price", price);
						m.put("imgUrl", imgUrl);
						
					} catch (Exception e) {
						System.out.println("-------问题数据skillId为：----------"+m.get("skillId").toString());
						System.out.println("-------问题数据当前类型为：----------"+m.get("enterType").toString());
						deleteCondition.add(m.get("seledId").toString());
					}
					 
				}
				
				if("jztf".equals(m.get("enterType"))){
					break;
				}else if("jzzx".equals(m.get("enterType"))){
					break;
				}else{
					if(null == m.get("enterId") || "" == m.get("enterId") || " " == m.get("enterId")){//如果没有入住类型，则该入住类型默认为高校社团
						m.put("enterId", enter_map.get("id"));
						m.put("typeValue", enter_map.get("typeValue"));
						m.put("typeName", enter_map.get("typeName"));
					}
				}
				index++;
			}
			for (Integer integer : deleteIndex) {
				list.get(index).get("seledId");
				list.remove(integer);
			}
			
			return list;
	}

	public boolean deleteOrder(Map<String, Object> map) {
		return orderDao.deleteOrder(map);
	}

	public String addOrder(Map<String, Object> map) {
		return orderDao.addOrder(map);

	}
	public String addOrderSch(Map<String, Object> map) {
		return orderDao.addOrderSch(map);

	}
	public String addOrderFiled(Map<String, Object> map) {
		return orderDao.addOrderFiled(map);

	}
	/**
	 * 删除当前已选中的错误数据
	 * @param condition
	 * @return
	 */
	public boolean deleteSelect(List<String> condition){
		
		return orderDao.deleteSelect(condition); 
	}
	public Map getBaseInfo(Map<String, Object> map) {
		return orderDao.getBaseInfo(map);
	}
	/**
	 * 查询子表2
	 * @param map
	 * dxf
	 * @return list
	 */
	public List querySettleList(Map<String, Object> map) {

		return orderDao.querySettleList(map);
	}
	//技能详情
	public Map getSkillInfoById(String skillId) {
		return orderDao.getSkillInfoById(skillId);
	}
	//订单子表
	public List<Map<String,Object>> getOrderDetail(String orderId){
		return orderDao.getOrderDetail(orderId);
	}
	
	//订单详情
	public Map getOrderById(String skillId) {
		return orderDao.getOrderById(skillId);
	}
	//改变订单状态
	public boolean UpOrderStatus(Map<String,Object>map) {
		return orderDao.UpOrderStatus(map);
	}
	
	/**
	 * 查询当前已选列表中是否有未查看的项
	 * @param openId
	 * @return
	 */
	public boolean isReadSelect(String openId){
		
		return orderDao.isReadSelect(openId);
	}
	
	
	/**
	 * 更新当前已选中的状态
	 * @param map
	 * @return
	 */
	public boolean updateSelectskill(Map<String,Object> map){
		
		return orderDao.updateSelectskill(map);
	}
	
	/**
	 * 查询当前已选表中的信息
	 */
	public Map<String,Object> getSelectSkill(Map<String,Object> map){
		
		return orderDao.getSelectSkill(map);
	}
	
}
