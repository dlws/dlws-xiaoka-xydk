package com.jzwl.xydk.wap.xkh2_2.payment.wallet.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jzwl.common.utils.DateUtil;
import com.jzwl.common.utils.DoubleArithUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.dividewithdraw.pojo.DivideDraw;
import com.jzwl.xydk.manager.draw.service.DrawService;
import com.jzwl.xydk.wap.xkh2_2.ordseach.pojo.OrderDetail;
import com.jzwl.xydk.wap.xkh2_2.ordseach.service.OrderSeaService;
import com.jzwl.xydk.wap.xkh2_2.payment.wallet.service.WalletService;

@Controller
@RequestMapping(value="/wallet")
public class WalletController extends BaseWeixinController{
	/** 日志 */
	Logger log = Logger.getLogger(WalletController.class);
	
	@Autowired
	private WalletService walletService;
	@Autowired
	private DrawService drawService;
	@Autowired
	private OrderSeaService orderSeaService;
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：我的钱包信息
	 * 创建人： sx
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value="/myWallet")
	public String myWallet(HttpServletRequest request, HttpServletResponse response,Model mov){
		log.info("查看我的钱包");
		createParameterMap(request);
		try {
			//获取用户openId
			//String openId = (String)paramsMap.get("openId");
			String openId = getCurrentOpenId(request);//从session中获取openId
			 log.info("1+++++++++++++++++++++++++++++++++++openId="+openId);
			//根据用户openId查找钱包余额
			Map<String, Object> walletMap = getWallet(openId);
			log.info("2+++++++++++++++++++++++++++++++++++walletMap="+walletMap);
			Double vBalance = 0d;
			Double balance = 0d;
			if(walletMap != null && walletMap.size() > 0){
				balance = (Double)walletMap.get("balance");
			}
			log.info("3+++++++++++++++++++++++++++++++++++balance="+balance);
			//根据用户openId查询记录表
			List<DivideDraw> divideDrawList = walletService.getDivideDrawByOpenId(openId);
			//根据openId查询所有未审核的记录的总金额
			Map<String, Object> moneyMap = walletService.getMoneyByOpenId(openId);
			log.info("4+++++++++++++++++++++++++++++++++++moneyMap="+moneyMap);
			Double withdrawMoney = 0d;
			if(moneyMap != null && moneyMap.size() >0){
				withdrawMoney = (Double)moneyMap.get("withdrawMoney");
			}
			log.info("5+++++++++++++++++++++++++++++++++++withdrawMoney="+withdrawMoney);
			vBalance = DoubleArithUtil.sub(balance,withdrawMoney);
			log.info("6+++++++++++++++++++++++++++++++++++vBalance="+vBalance);
			if(vBalance < 0){
				vBalance = 0d;
			}
			mov.addAttribute("vBalance", vBalance);
			mov.addAttribute("walletMap", walletMap);
			mov.addAttribute("divideDrawList", divideDrawList);
			mov.addAttribute("openId", openId);
			mov.addAttribute("withdrawMoney", withdrawMoney);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取数据错误");
			return "/error";
		}
		return "/wap/xkh_version_2_2/wallet";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：提现操作页面
	 * 创建人： sx
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value="/deposit")
	public String deposit(HttpServletRequest request, HttpServletResponse response,Model mov){
		log.info("提现页面...");
		createParameterMap(request);
		try {
			String openId = (String)paramsMap.get("openId");
			//获取钱包信息
			Map<String, Object> walletMap = getWallet(openId);
			Double balance = 0d;
			if(walletMap != null && walletMap.size() > 0 ){
				balance = (Double)walletMap.get("balance");
			}
			//获取记录表未审核金额
			Map<String, Object> moneyMap = walletService.getMoneyByOpenId(openId);
			Double withdrawMoney = 0d;
			if(moneyMap != null && moneyMap.size() > 0){
				withdrawMoney = (Double)moneyMap.get("withdrawMoney");
			}
			Double vBalance = balance - withdrawMoney;
			if(vBalance < 0){
				vBalance = 0d;
			}
			mov.addAttribute("walletMap", walletMap);
			mov.addAttribute("openId", openId);
			mov.addAttribute("vBalance", vBalance);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取数据错误...");
		}
		return "/wap/xkh_version_2_2/deposit";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：获取openId
	 * 创建人： sx
	 * 创建时间： 2017年4月17日
	 * 标记：
	 * @param openId
	 * @return
	 * @version
	 */
	private Map<String, Object> getWallet(String openId){
		Map<String, Object> map = null;
		try {
			map = walletService.getWalletByOpenId(openId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月18日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value="/depositDetail")
	public String depositDetail(HttpServletRequest request, HttpServletResponse response,Model mov){
		log.info("提现成功页面跳转...");
		createParameterMap(request);
		try {
			Map<String, Object> drawMap = new HashMap<String, Object>();
			//openId
			String openId = (String)paramsMap.get("openId");
			//提现金额
			String withdrawMoney = (String)paramsMap.get("balance");
			//状态
			String state = (String)paramsMap.get("state");
			//开户银行
			String bank = (String)paramsMap.get("bank");
			//银行卡号
			String bankCardNumber = (String)paramsMap.get("bankCardNumber");
			//开户支行全称
			String accountFullName = (String)paramsMap.get("accountFullName");
			drawMap.put("openId", openId);
			drawMap.put("withdrawMoney", withdrawMoney);
			drawMap.put("bank", bank);
			drawMap.put("bankCardNumber", bankCardNumber);
			drawMap.put("accountFullName", accountFullName);
			drawService.addDraw(drawMap);
			mov.addAttribute("openId", openId);
			mov.addAttribute("withdrawMoney", withdrawMoney);
			mov.addAttribute("nowDate", DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
			mov.addAttribute("state", state);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "/wap/xkh_version_2_2/deposit_detail";
	}
	
	
	
	/**
	 * 
	 * 描述:list ---->>>>>>>> map
	 * 作者:gyp
	 * @Date	 2017年4月21日
	 */
	public Map<String,Object> detailListToMap(List<OrderDetail> orderList){
		Map<String,Object> result = new HashMap<String, Object>();
		for (OrderDetail orderDetail : orderList) {
			 result.put(orderDetail.getTypeName(), orderDetail.getTypeValue());
		}
		return result;	
			
	}
	
	/**
	 * 
	 * 描述:提现详情
	 * 作者:gyp
	 * @Date	 2017年6月12日
	 */
	@RequestMapping(value="/orderSharingDetail")
	public String orderSharingDetail(HttpServletRequest request, HttpServletResponse response,Model mov){
		createParameterMap(request);
		try {
			Map<String, Object> drawMap = new HashMap<String, Object>();
			//openId
			String openId = (String)paramsMap.get("openId");
			//提现金额
			String withdrawMoney = (String)paramsMap.get("balance");
			//状态
			String state = (String)paramsMap.get("state");
			//开户银行
			String bank = (String)paramsMap.get("bank");
			//银行卡号
			String bankCardNumber = (String)paramsMap.get("bankCardNumber");
			//开户支行全称
			String accountFullName = (String)paramsMap.get("accountFullName");
			//获取订单的Id
			String orderId = (String)paramsMap.get("orderId");
		
			drawMap.put("openId", openId);
			drawMap.put("withdrawMoney", withdrawMoney);
			drawMap.put("bank", bank);
			drawMap.put("bankCardNumber", bankCardNumber);
			drawMap.put("accountFullName", accountFullName);
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("orderId",orderId);
			//检查当前用户在订单表
			List<OrderDetail> orderList = orderSeaService.searchOrderDetail(map);
			Map<String,Object> skillInfo =  detailListToMap(orderList);
			mov.addAttribute("openId", openId);
			mov.addAttribute("withdrawMoney", withdrawMoney);
			mov.addAttribute("nowDate", DateUtil.dateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
			mov.addAttribute("state", state);
			mov.addAttribute("skillInfo", skillInfo);
			mov.addAttribute("orderId", orderId);
		} catch (Exception e) {
			e.printStackTrace();
			return "/error";
		}
		return "/wap/xkh_version_2_2/orderSharing_detail";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：
	 * 创建人： sx
	 * 创建时间： 2017年4月18日
	 * 标记：
	 * @param request
	 * @param response
	 * @param mov
	 * @return
	 * @version
	 */
	@RequestMapping(value="/getByOpenIdAndDrawMoney")
	@ResponseBody
	public Map<String, Object> getByOpenIdAndDrawMoney(HttpServletRequest request, HttpServletResponse response,Model mov){
		log.info("提现判断...");
		Map<String, Object> map = new HashMap<String,Object>();
		createParameterMap(request);
		try {
			String openId = (String)paramsMap.get("openId");
			//页面提现输入的金额
			String drawMoney = (String)paramsMap.get("drawMoney");
			//获取钱包信息
			Map<String, Object> walletMap = getWallet(openId);
			Double balance = 0d;
			if(walletMap != null && walletMap.size() > 0 ){
				balance = (Double)walletMap.get("balance");
			}
			//获取记录表未审核金额
			Map<String, Object> moneyMap = walletService.getMoneyByOpenId(openId);
			Double withdrawMoney = 0d;
			if(moneyMap != null && moneyMap.size() > 0){
				withdrawMoney = (Double)moneyMap.get("withdrawMoney");
			}
			Double vBalance = balance - withdrawMoney;
			if(vBalance < 0){
				vBalance = 0d;
			}
			if(Double.valueOf(drawMoney) <= vBalance ){
				map.put("flag", true);
			}else{
				map.put("flag", false);
			}
			map.put("vBalance", vBalance);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("getByOpenIdAndDrawMoney...提现判断异常");
		}
		return map;
	}
}
