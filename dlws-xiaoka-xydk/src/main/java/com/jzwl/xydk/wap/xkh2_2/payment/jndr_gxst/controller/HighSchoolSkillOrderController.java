/**
 * HighSchoolSkillOrderController.java
 * com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.controller
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jzwl.common.utils.Util;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.manager.Entertype.service.EntertypeService;
import com.jzwl.xydk.manager.skillUser.service.SkillUserManagerService;
import com.jzwl.xydk.manager.user.userBasicinfo.service.UserBasicinfoService;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.business.home.service.HomeService;
import com.jzwl.xydk.wap.skillUser.service.SkillUserService;
import com.jzwl.xydk.wap.xkh2_2.chat.service.ChatMessageService;
import com.jzwl.xydk.wap.xkh2_2.payment.consultation.service.PlaceOrderService;
import com.jzwl.xydk.wap.xkh2_2.payment.delivery.controller.SkillOrderController;
import com.jzwl.xydk.wap.xkh2_2.payment.feedback.controller.FeedBackController;
import com.jzwl.xydk.wap.xkh2_2.payment.jndr_gxst.service.HighSchoolSkillOrderService;
import com.jzwl.xydk.wap.xkh2_3.personInfo.service.PersonInfoDetailService;
import com.jzwl.xydk.wap.xkh2_3.skill_detail.service.SkillDetailService;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   wxl
 * @Date	 2017年4月13日 	 
 */
@Controller("placeHighSchoolSkillOrder")
@RequestMapping("/placeHighSchoolSkillOrder")
public class HighSchoolSkillOrderController extends BaseWeixinController {

	@Autowired
	private HighSchoolSkillOrderService highSchoolSkillOrderService;

	@Autowired
	private SkillUserService skillUserService;

	@Autowired
	private PlaceOrderService placeOrderService;

	@Autowired
	private UserBasicinfoService userBasicinfoService;

	@Autowired
	private ChatMessageService chatMessageService;
	
	@Autowired
	private SkillUserManagerService skillUserManagerService;
	
	@Autowired
	private SendMessageService sendMessage;

	@Autowired
	private EntertypeService entertypeService;
	
	@Autowired
	private HomeService homeService;
	
	@Autowired
	private SkillDetailService userSkillService;
	
	@Autowired
	PersonInfoDetailService personInfoDetSer;
	
	@Autowired
	private FeedBackController  feedBackController;
	
	@Autowired
	private SkillOrderController  skillOrderController;
	//jndr 
	/**
	 * enterType仅限jndr gxst
	 * 
	 * 根据若干参数  到提交 去提交订单页面
	 * <p>
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/toPlaceHighSchoolSkill")
	public String toPlaceHighSchoolSkillOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		Map<String, Object> skillSonMap = new HashMap<String, Object>();
		Map<String,Object> detailMap = new HashMap<String, Object>();//技能详情
		//获取当前的技能图片
		String skillImg;
		String unit;
		
		try {
			String skillId = paramsMap.get("skillId").toString();
			detailMap.put("pid", skillId);
			//若干比例
			skillSonMap = userSkillService.getSkillInfo(paramsMap.get("skillId").toString());//订单信息
			skillImg = homeService.getImageUrl(skillSonMap.get("fathValue").toString(), Long.parseLong(skillId));
			unit = homeService.setCompany("company", Long.parseLong(skillId));
			detailMap = personInfoDetSer.getuserSkillinfoDetail(detailMap);
			//增加加入已选中的钱数等 2.4功能优化
			detailMap.put("pid", skillId);
			skillOrderController.getSelectSkill(request,detailMap,model);
			
			paramsMap.put("id",skillSonMap.get("basicId"));//根据前台传入的skillId进行
			//查询方式为ID查询
			Map<String, Object> selUserInfo = userBasicinfoService.getById(paramsMap);//当前用户的详细信息
			
			//判定当前enterId为空，或者为null则默认给高校社团
			if(selUserInfo.get("enterId")==null||StringUtils.isBlank(selUserInfo.get("enterId").toString())){
				
				Map<String,Object> currenMap = new HashMap<String, Object>();
				currenMap.put("typeValue","gxst");
				currenMap = entertypeService.getByEnterValue(currenMap);
				selUserInfo.put("enterId",currenMap.get("enterId"));
			}
			
			//若当前用户的nick name为空则取用userName
			if(selUserInfo.get("nickname")==null||StringUtils.isBlank(selUserInfo.get("nickname").toString())){
				
				if(selUserInfo.get("userName")!=null||StringUtils.isNotBlank(selUserInfo.get("userName").toString())){
					selUserInfo.put("nickname", selUserInfo.get("userName").toString());
				}
			}
			//获取dic_name
			model.addAttribute("unit",unit);
			model.addAttribute("skillImg",skillImg);
			model.addAttribute("skillSonMap", skillSonMap);
			model.addAttribute("selUserInfo", selUserInfo);
			model.addAttribute("skillImg", skillImg);
			model.addAttribute("detailMap", detailMap);
			
			return "wap/xkh_version_2_2/creatorder/placeOrder_jndr_gxst";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";
		}
	}

	/**
	 * 
	 * 添加到订单
	 *
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/insertHighSchoolSkillOrder")
	public String insertHighSchoolSkillOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			//下单成功就在添加系统信息
			//获取当前用户的OpenId
			String openIdBuy = String.valueOf(request.getSession().getAttribute("dkOpenId")); //get openId
			paramsMap.put("openId",openIdBuy);//存入当前买家的OpenId
			String sellOpenId = paramsMap.get("sellerOpenId").toString();
			String orderId = highSchoolSkillOrderService.insertHighSchoolSkillOrder(paramsMap);
			//系统消息
			paramsMap.put("openId",openIdBuy);//存入当前买家的OpenId
			paramsMap.put("nowOpenId", sellOpenId);
			paramsMap.put("orderId", orderId);
			paramsMap.put("orderStatus", 1);
			paramsMap.put("isSuccess", orderId);
			chatMessageService.insertUpdateSysMeg(paramsMap);//卖家端
			//chatMessageService.insertBuyMeg(paramsMap);
			//TODO 有支付了 放到支付里面-状态该为支付的状态添加到信息表
			boolean isAddSysMsg = chatMessageService.insertBuyMeg(paramsMap);//买家端
			boolean isTrue = highSchoolSkillOrderService.updateSkillBuyNo(paramsMap);
			if ("0" != orderId && isTrue && isAddSysMsg) {
				model.addAttribute("msg", true);
				//发送模板信息
				sendMessage.submissionOrder(orderId,openIdBuy);
				return "redirect:/placeHighSchoolSkillOrder/highSchoolSkillOrderDetail.html?orderId=" + orderId;
			} else {
				model.addAttribute("msg", false);
				request.setAttribute("msg", "添加失败！");
				return "redirect:/home/index.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "添加失败！");
			return "redirect:/home/index.html";
		}

	}
	/**
	 * 订单详情
	 * orderId
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/highSchoolSkillOrderDetail")
	public String highSchoolSkillOrderDetail(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		Map<String, Object> orderDetail = new HashMap<String, Object>();
		try {
			
			boolean isMySelf = false;//默认非自己点击
			String  currentOpenId = getCurrentOpenId(request);
			
			//4.增加查看当前Id值是否存在反馈记录
			model = feedBackController.feedBackCommon(paramsMap,model);
			
			orderDetail = highSchoolSkillOrderService.getOrderDetail(paramsMap);
			Date startTime = (Date) orderDetail.get("startDate");
			Date endTime = (Date) orderDetail.get("endDate");
			orderDetail.put("startTime", Util.DateFormat(startTime));
			orderDetail.put("endTime", Util.DateFormat(endTime));
			if (null != orderDetail) {
				List<Map<String, Object>> orderDetailList = highSchoolSkillOrderService.getOrderDetailList(paramsMap);
				if (orderDetailList.size() > 0) {
					for (int i = 0; i < orderDetailList.size(); i++) {
						String skillid = orderDetailList.get(i).get("typeName").toString();
						if ("skillId".equals(skillid)) {
							String skillId = orderDetailList.get(i).get("typeValue").toString();
							Map<String, Object> skillInfo = skillUserService.getSkillInfoById(skillId);
							model.addAttribute("skillInfo", skillInfo);
						}
					}
					Date invalidTime = (Date) orderDetail.get("invalidTime");
					SkillOrderController skOder = new SkillOrderController();
					String invalid = skOder.subtracDateS(invalidTime, new Date());
					long timmer = invalidTime.getTime();
					
					if(currentOpenId.equals(orderDetail.get("openId"))){
						isMySelf = true;
					}
					
					model.addAttribute("isMySelf", isMySelf);
					model.addAttribute("invalidTime", invalid);
					model.addAttribute("orderDetail", orderDetail);
					model.addAttribute("orderDetailList", orderDetailList);
					model.addAttribute("timmer", timmer);//增加timmer时间
					return "wap/xkh_version_2_2/orderdetaile/gxst_order";
				} else {
					System.out.println("副表为空");
					request.setAttribute("msg", "查询失败！");
					return "redirect:/home/index.html";
				}
			} else {
				System.out.println("主表为空");
				request.setAttribute("msg", "查询失败！");
				return "redirect:/home/index.html";
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";
		}
	}

	/**
	 * 
	 * 	1：下单成功，2：支付成功（也就是待服务）、0：已接单、11：拒绝接单、 
	 * 	3：待确认（已服务）、 4：已确认（已完成），5：超时失效， 6：申请退款、
	 * 	7：待退款、8：退款成功 9 取消订单、10已关闭
	 * 
	 * 根具orderId改订单状态---和参数--x
	 * 			取消订单-9
	 * <p>		拒绝--11
	 * 			接受--0
	 * 			
	 * 			待退款-7
	 * 			退款成功-8
	 *			取消退款-90
	 * @param request
	 * @param response
	 * @param model
	 *
	 * 买家发给卖家
	 */
	@RequestMapping("/giveBuyOrder")
	public String giveBuyOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			
			String orderStatus = paramsMap.get("paramsMap").toString();
			String orderId = null;
			if(orderStatus.equals("7")){//付款成功  卖家未接单
				orderId = placeOrderService.updateOrder(paramsMap);
			}else{
				orderId = placeOrderService.updateOrderStatus(paramsMap);
			}
			
			//操作人操作以后  发给另一个人
			Map<String, Object> orderMap = highSchoolSkillOrderService.getOrderDetail(paramsMap);
			String nowOpenId = orderMap.get("sellerOpenId").toString();//操作人的openId
			paramsMap.put("nowOpenId", nowOpenId);
			boolean b = chatMessageService.insertUpdateSysMeg(paramsMap);
			if ("0" != orderId && b) {
				model.addAttribute("msg", "操作成功");
			} else {
				model.addAttribute("msg", "操作失败!");
			}
			return "redirect:/chatMesseage/queryOrderDetail.html?orderId=" + orderId;
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";
		}
	}

	//卖家发给买家
	@RequestMapping("/giveSellOrder")
	public String giveSellOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		createParameterMap(request);
		try {
			String isSuccess = placeOrderService.updateOrderStatus(paramsMap);
			//操作人操作以后  发给另一个人
			Map<String, Object> orderMap = highSchoolSkillOrderService.getOrderDetail(paramsMap);
			String nowOpenId = orderMap.get("openId").toString();//操作人的openId
			paramsMap.put("nowOpenId", nowOpenId);
			boolean b = chatMessageService.insertUpdateSysMeg(paramsMap);
			if ("0" != isSuccess && b) {
				model.addAttribute("msg", "操作成功");
			} else {
				model.addAttribute("msg", "操作失败!");
			}
			return "redirect:/chatMesseage/queryOrderDetail.html?orderId=" + isSuccess;
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败！");
			return "redirect:/home/index.html";
		}
	}

}
