/**
 * UserSettledDao.java
 * com.jzwl.xydk.wap.xkh2_3.user.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_3.user.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   gyp
 * @Date 2017年5月9日
 * @Time 下午7:07:01
 */
@Repository("publicDao")
public class PublicDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	/**
	 * 根据类型获取轮播图
	 * 描述:
	 * 作者:gyp
	 * @Date	 2017年5月9日
	 */
	public List<BannerInfo> getSkillBanner(String bannerType) {

		String sql = "select t.* from `xiaoka-xydk`.banner_info t where bannerType = '"+bannerType+"' and  isDelete = 0 and isUse = 1 order by t.ord asc";
		List<BannerInfo> skillBanner = baseDAO.getJdbcTemplate().query(
				sql,
				new Object[] {},
				ParameterizedBeanPropertyRowMapper
						.newInstance(BannerInfo.class));
		return skillBanner;
	}
	
	/**
	 * 
	 * 价格单位字典查询
	 * <p>
	 */
	public List<Map<String, Object>> getCanpanyMap() {

		String sql = "select id as VALUE ,dic_name as text from v2_dic_data d ";
		sql = sql+ " where d.isDelete=0 and d.dic_id in (select dic_id from v2_dic_type where isDelete = 0 and dic_code='companyType')";
		sql = sql + " order by id ";

		return baseDAO.queryForList(sql);
	}

	/**
	 * 获取子表
	 * @param map
	 * @return
	 */
	public List getClassSBySid(Map<String, Object> map) {
		String sql = "select id as VALUE,className as text "
				+ "from `xiaoka-xydk`.user_skillclass_son t where t.fatherId='" + map.get("sid") + "' and isDelete=0";
		return baseDAO.queryForList(sql);
	}
	
	/**
	 * 根据id获取用户的子集信息
	 * 创建人：dxf
	 * 创建时间：2017年5月26日
	 * 描述：
	 * @param map
	 * @return
	 */
	public Map<String, Object> getSkillDetail(Map<String, Object> map) {

		String sql = "SELECT id,pid,typeName,typeValue,isDelete,createDate FROM `xiaoka-xydk`.user_skillinfo_detail"
				+ " WHERE id='" + map.get("imgeId") + "'";
		return baseDAO.queryForMap(sql);
	}
	
	public boolean updateSkillDetail(Map<String, Object> map) {
		String sql = "update  `xiaoka-xydk`.user_skillinfo_detail set typeValue=:typeValue where id= '"
				+ map.get("imgeId") + "'";
		return baseDAO.executeNamedCommand(sql, map);
	}
}

