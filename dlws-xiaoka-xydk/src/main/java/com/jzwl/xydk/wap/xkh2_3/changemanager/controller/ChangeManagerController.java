package com.jzwl.xydk.wap.xkh2_3.changemanager.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.jzwl.common.constant.AjaxStatusConstant;
import com.jzwl.common.constant.Constants;
import com.jzwl.common.utils.HttpClientUtil;
import com.jzwl.system.base.controller.BaseWeixinController;
import com.jzwl.xydk.sendmessage.SendMessageService;
import com.jzwl.xydk.wap.xkh2_3.changemanager.service.ChangeMangerService;

@Controller
@RequestMapping(value="/change")
public class ChangeManagerController extends BaseWeixinController{

	@Autowired
	private ChangeMangerService changeManagerService;
	@Autowired
	private SendMessageService sendMessage;
	@Autowired
	private Constants constants;
	/** 日志 */
	Logger log = Logger.getLogger(ChangeManagerController.class);
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到是否更换管理员页面
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/changeManager")
	public String changeManager(HttpServletRequest request, Model model){
		log.info("跳转到----------------是否更改管理员页面------");
		return "/wap/xkh_version_2_3/administrator/administrator_konw";
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击是     ,,,,,,,,,,,跳转到身份验证页面
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @return
	 * @version
	 */
	@RequestMapping(value="/checkOk")
	public String checkOk(HttpServletRequest request, Model model){
		log.info("跳转到----------------身份认证页面------");
		createParameterMap(request);
		try {
			//获取openId 根据openId 查询
			String openId = getCurrentOpenId(request);//从session中获取openId  13522555332
			//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
			//根据openId 获取入驻信息
			Map<String, Object> map = changeManagerService.findByOpenId(openId);
			String phoneNumber = "";
			if(map != null && map.size() > 0){
				phoneNumber = (String)map.get("phoneNumber");
			}
			String phone = phoneNumber.substring(0, 3);
			String number = phoneNumber.substring(9, phoneNumber.length());
			model.addAttribute("phoneNumber", phoneNumber);
			model.addAttribute("phone", phone);
			model.addAttribute("number", number);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/wap/xkh_version_2_3/administrator/administrator_sure";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击确定,验证手机号
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/makeSure")
	@ResponseBody
	public Map<String, Object> makeSure(HttpServletRequest request, Model model){
		log.info("跳转到----------------验证手机号------");
		createParameterMap(request);
		Map<String, Object> flagMap = new HashMap<String, Object>();
		try {
			if(paramsMap != null && paramsMap.size() > 0){
				String phone = (String)paramsMap.get("phoneNumber");
				String openId = getCurrentOpenId(request);//从session中获取openId
				//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
				//根据openId 获取入驻信息
				Map<String, Object> map = changeManagerService.findByOpenId(openId);
				if(map != null && map.size() >0){
					String phoneNumber = (String)map.get("phoneNumber");
					if(phone.equals(phoneNumber)){
						flagMap.put("flag", true);
						flagMap.put("userId", map.get("id"));//获取用户的id
					}else{
						flagMap.put("flag", false);
					}
				}else{
					flagMap.put("flag", false);
				}
			}else{
				flagMap.put("flag", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flagMap;
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到管理员信息填写页面
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/changeInfo")
	public String changeInfo(HttpServletRequest request, Model model){
		log.info("跳转到---------------管理员信息填写页面------");
		createParameterMap(request);
		String userId = paramsMap.get("userId").toString();
		model.addAttribute("userId", userId);
		return "/wap/xkh_version_2_3/administrator/administrator_change";
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击提交验证数据
	 * 创建人： sx
	 * 创建时间： 2017年5月15日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/infoSubmit")
	@ResponseBody
	public Map<String, Object> infoSubmit(HttpServletRequest request, Model model){
		log.info("跳转到----------------验证提交数据是否正确------");
		createParameterMap(request);
		Map<String, Object> flagMap = new HashMap<String, Object>();
		Map<String, Object> returnMap = new HashMap<String, Object>();
		try {
			String openId = getCurrentOpenId(request);//从session中获取openId
			//String openId = "o3P2SwZCegQ-M6AG4-Nukz5R8gN8";
			//获取当前用户入驻信息
			Map<String, Object> bascMap = changeManagerService.findByOpenId(openId);
			//获取微信号
			String wxNumber = (String)paramsMap.get("wxNumber");
			String nickname = (String)paramsMap.get("nickname");
			String phoneNumber = (String)paramsMap.get("phoneNumber");
			String email = (String)paramsMap.get("email");
			String userId = (String)paramsMap.get("userId");
			flagMap.put("wxNumber", wxNumber);
			flagMap.put("nickname", nickname);
			flagMap.put("phoneNumber", phoneNumber);
			flagMap.put("email", email);
			//通过微信号查询入驻信息
			Map<String, Object> map = changeManagerService.findByWxNumber(flagMap);
			if(map != null && map.size() >0){
				Map<String, Object> messMap = new HashMap<String, Object>();
				messMap.put("nickname", bascMap.get("nickname").toString());
				messMap.put("openId", map.get("openId").toString());
				//插入到change_manager_record表
				flagMap.put("basicId", map.get("id").toString());
				flagMap.put("userId", userId);
				String recordId = changeManagerService.addRecord(flagMap);
				map.put("recordId", recordId);
				map.put("first", "您将成为新的管理员");
				map.put("keyword1", "启用");
				map.put("detail", "点击是否接受。");
				map.put("detail", "点击是否接受。");
				//发送消息
				sendMessage.numberChange(map);
				//sendMessageToUser(messMap);
				returnMap.put("msg", true);
			}else{
				returnMap.put("msg", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnMap.put("msg", false);
		}
		return returnMap;
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到提示页面
	 * 创建人： sx
	 * 创建时间： 2017年5月24日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/prompt")
	public String prompt(HttpServletRequest request, Model model){
		return "/wap/xkh_version_2_3/administrator/administrator_cue";
	}
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：提交成功发送模板消息
	 * 创建人： sx
	 * 创建时间： 2017年5月16日
	 * 标记：
	 * @param map
	 * @version
	 */
	public void sendMessageToUser(Map<String, Object> map){
		log.info("提交成功----------------发送模板消息------");
		StringBuffer detail = new StringBuffer();
		WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
		templateMessage.setToUser(map.get("openId").toString());
		detail.append(map.get("nickname").toString() + " 邀请您成为校咖汇的新管理员,您是否接受转权?");
		templateMessage.getDatas().add(new WxMpTemplateData("remark", detail.toString()));
		Gson gson = new Gson();
		String json = gson.toJson(templateMessage);
		String urlString = constants.getXiaoka_weixin_wm_sendTemplateByOpenId();
		HttpClientUtil.requestByPostMethod(urlString, json,"templateMessage", "changeManager", "xydkSystemAuth");
	}
	
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：跳转到提示页面
	 * 创建人： sx
	 * 创建时间： 2017年5月24日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/userConfirmation")
	public String userConfirmation(HttpServletRequest request, Model model){
		createParameterMap(request);
		String recordId = paramsMap.get("recordId").toString();
		Map<String,Object> map = changeManagerService.getBasicInfoByRecordId(recordId);
		model.addAttribute("recordId", recordId);
		model.addAttribute("isAgree", map.get("isAgree"));
		model.addAttribute("nickname", map.get("nickname"));
		model.addAttribute("contactUser", map.get("contactUser"));

		return "/wap/xkh_version_2_3/administrator/administrator_new";
	}
	
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击接受页面跳转
	 * 创建人： sx
	 * 创建时间： 2017年5月16日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/accept")
	public @ResponseBody Map<String,Object> accept(HttpServletRequest request, Model model){
		createParameterMap(request);
		Map<String, Object> map_ajax = new HashMap<String, Object>();
		try {
			//修改change_manager_record表
			String recordId = paramsMap.get("recordId").toString();
			Map<String,Object> map = changeManagerService.getBasicInfoByRecordId(recordId);

			Map<String, Object> insertMap = new HashMap<String, Object>();
			insertMap.put("id", recordId);
			insertMap.put("isAgree", 1);
			insertMap.put("basicId", map.get("basicId"));
			insertMap.put("userId", map.get("userId"));
			boolean flag = changeManagerService.updateChangeManage(insertMap);
			//boolean flag2 = changeManagerService.updateSkillinfo(insertMap);
			//boolean flag3 = changeManagerService.deleteBasicInfo(insertMap);
			if(flag){
				map_ajax.put("recordId", recordId);
				map_ajax.put("first", map.get("userName")+"同意管理员授权");
				map_ajax.put("keyword1", "同意");
				map_ajax.put("detail", "同意管理员授权");
				map_ajax.put("openId", map.get("openId"));
				//发送消息
				sendMessage.numberChange(map_ajax);
				map_ajax.put("msg", "获取信息成功");
				map_ajax.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_SUCCESS);
			}else{
				map_ajax.put("msg", "获取信息失败");
				map_ajax.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
			}
		} catch (Exception e) {
			map_ajax.put("msg", "获取信息失败");
			map_ajax.put("ajax_status", AjaxStatusConstant.AJAX_STATUS_FAILURE);
		}
		return map_ajax;
	}
	/**
	 * 项目名称：dlws-xiaoka-xydk
	 * 描述：点击拒绝接受
	 * 创建人： sx
	 * 创建时间： 2017年5月16日
	 * 标记：
	 * @param request
	 * @param model
	 * @return
	 * @version
	 */
	@RequestMapping(value="/refuse")
	public String refuse(HttpServletRequest request, Model model){
		createParameterMap(request);
		//String openId = getCurrentOpenId(request);//从session中获取openId
		String recordId = paramsMap.get("recordId").toString();
		Map<String,Object> map = changeManagerService.getBasicInfoByRecordId(recordId);
		Map<String, Object> insertMap = new HashMap<String, Object>();
		insertMap.put("id", recordId);
		insertMap.put("isAgree", 2);
		insertMap.put("recordId", recordId);
		insertMap.put("first", map.get("userName")+"拒绝管理员授权");
		insertMap.put("keyword1", "拒绝");
		insertMap.put("detail", "拒绝管理员授权");
		insertMap.put("openId", map.get("openId"));
		//发送消息
		sendMessage.numberChange(insertMap);
		boolean flag = changeManagerService.updateChangeManage(insertMap);
		
			
		return "redirect:/home/index.html";
	}
	
	
}
