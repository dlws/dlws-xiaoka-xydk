/**
 * UserSettledDao.java
 * com.jzwl.xydk.wap.xkh2_3.user.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.jzwl.xydk.wap.xkh2_3.user.dao;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   gyp
 * @Date 2017年5月9日
 * @Time 下午7:07:01
 */
@Repository("userSettledDao")
public class UserSettledDao {
	
	@Autowired
	private BaseDAO baseDAO;//dao基类，操作数据库
	
	/**
	 * 描述: 根据用户的openId获取用户的详情
	 * 作者:gyp
	 * @Date	 2017年5月9日
	 */
	public Map<String,Object> getUserBaseInfo(String openId){
		String sql = "select bas.openId as openId,bas.id as id  from `xiaoka-xydk`.user_basicinfo bas  where bas.openId='"+openId+"'";
		return baseDAO.queryForMap(sql);
	}
	/**
	 * 
	 * 描述:添加基本用户信息
	 * 作者:gyp
	 * @Date	 2017年5月10日
	 */
	public boolean addUserBaseInfo(Map<String, Object> map) {
		Date date = new Date();
		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		map.put("id", Sequence.nextId());
		map.put("auditState", 0);
		map.put("createDate", date);
		map.put("isRecommend", 0);// 是否推荐
		map.put("skillStatus", 0);
		map.put("isDelete", 0);
		map.put("viewNum", 0);

		String addBasiSql = "insert into `xiaoka-xydk`.user_basicinfo "
				+ " (id,userName,nickname,cityId,schoolId,labelId,phoneNumber,email,wxNumber,openId,aboutMe,"
				+ "headPortrait,auditState,viewNum,createDate,isRecommend,skillStatus,isDelete,contactUser) "
				+ " values "
				+ " (:id,:userName,:userName,:cityId,:schoolId,:labelId,:phoneNumber,:email,:wxNumber,:openId,:aboutMe,"
				+ ":picUrl,:auditState,:viewNum,:createDate,:isRecommend,:skillStatus,:isDelete,:contactUser)";

		boolean flag = baseDAO.executeNamedCommand(addBasiSql, map);
		
		return flag;
	}
	
	/**
	 * 
	 * 描述:添加基本用户信息
	 * 作者:gyp
	 * @Date	 2017年5月10日
	 */
	public boolean editUserBaseInfo(Map<String, Object> map) {
		Date date = new Date();
		// 自动注入时间戳为ID 酌情修改数据库类型为bigint int会越界
		String addBasiSql = "update `xiaoka-xydk`.user_basicinfo set "
				+ " userName=:userName,nickname=:userName,cityId=:cityId,schoolId=:schoolId,labelId=:labelId,phoneNumber=:phoneNumber,"
				+ "email=:email,wxNumber=:wxNumber,aboutMe=:aboutMe,"
				+ "headPortrait=:picUrl,contactUser=:contactUser where id=:baseId";
		boolean flag = baseDAO.executeNamedCommand(addBasiSql, map);
		
		return flag;
	}

}

