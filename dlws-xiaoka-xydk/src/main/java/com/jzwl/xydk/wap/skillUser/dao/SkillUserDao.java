package com.jzwl.xydk.wap.skillUser.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.chanjar.weixin.common.util.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.jzwl.common.id.Sequence;
import com.jzwl.system.base.dao.BaseDAO;
import com.jzwl.xydk.wap.business.home.pojo.BannerInfo;

@Repository("skillUserDao")
public class SkillUserDao {
	@Autowired
	private BaseDAO baseDAO;// dao基类，操作数据库

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取城市列表信息 创建人： ln 创建时间： 2016年10月12日 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryCityList() {
		try {
			String sql = " SELECT id,cityName FROM tg_city  where isDelete = 0 ";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取省列表信息 创建人： dxf 创建时间： 2017年2月23日 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryProviceListVersion2() {
		try {
			String sql = "SELECT id,cityName,province,number,cityOperateManager,phone,longitude,latitude,remark "
					+ "FROM tg_city WHERE isDelete=0 AND province IS NOT NULL  GROUP BY   province ";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取城市列表信息 创建人： dxf 创建时间： 2017年2月23日 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryCityListVersion2(String provnceId) {
		try {
			String sql = "SELECT id as value,cityName as text "
					+ " FROM tg_city  WHERE isDelete=0 and province='"
					+ provnceId + "'";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Map<String, Object>> getSchoolGrade(String type) {
		try {
			String sql = "select dd.dic_name as text,dd.id as value from `xiaoka`.v2_dic_type t LEFT JOIN `xiaoka`.v2_dic_data dd ON t.dic_id = dd.dic_id where t.isDelete = 0 and t.dic_code = '"
					+ type + "'";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取院校类型列表信息 创建人： dxf 创建时间： 2017年2月23日 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySchollTypeVersion2(String cityId) {
		try {
			String sql = "SELECT dcda.id as value,dcda.dic_name as text "
					+ " FROM `xiaoka-xydk`.schoolLabel sl INNER JOIN `xiaoka`.v2_dic_data dcda ON dcda.id=sl.dicDataId "
					+ " INNER JOIN tg_school_info shol ON sl.schoolId=shol.id where shol.cityId="
					+ cityId + " group by dic_name "
					+ " ORDER BY dic_name desc limit 0,50";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据城市和院校类型获取院校列表信息 创建人： dxf 创建时间： 2017年2月23日
	 * 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySchollByTypeVersion2(String cityId,
			String dcdaId) {
		try {
			String sql = "SELECT shol.schoolName as text,shol.id as value"
					+ " FROM `xiaoka-xydk`.schoolLabel sl INNER JOIN `xiaoka`.v2_dic_data dcda ON dcda.id=sl.dicDataId "
					+ " INNER JOIN tg_school_info shol ON sl.schoolId=shol.id where shol.cityId="
					+ cityId + " and dcda.id=" + dcdaId
					+ " ORDER BY dic_name desc limit 0,1000";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取学校信息 创建人： ln 创建时间： 2016年10月12日 标记：wap
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySchoolList(String cityId) {
		try {
			String sql = " SELECT id as value,cityId,schoolName as text FROM tg_school_info where isDelete = 0 and cityId="
					+ cityId + " ";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：保存技能用户基本信息 创建人： ln 创建时间： 2016年10月12日 标记：wap
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean addSkillUser(Map<String, Object> map) {
		try {
			map.put("id", Sequence.nextId());
			map.put("createDate", new Date());
			String headPortrait = String.valueOf(map.get("headPortrait"));
			String sql = " insert into `xiaoka-xydk`.user_basicinfo"
					+ "    (id,userName,wxNumber,cityId,schoolId,nickname,headPortrait,checkType,phoneNumber,email,openId,aboutMe,auditState,createDate,createUser,isDelete,isRecommend,skillStatus) "
					+ "    values "
					+ "    (:id,:userName,:wxNumber,:cityId,:schoolId,:nickname,:headPortrait,:checkType,:phoneNumber,:email,:openId,:aboutMe,0,:createDate,0,0,0,0)";

			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据学校ID获取城市ID 创建人： ln 创建时间： 2016年10月17日 标记：wap
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> getCityCodeBySchoolId(
			Map<String, Object> paramsMap) {
		try {
			String sql = " select * from tg_school_info where id = '"
					+ paramsMap.get("schoolId") + "' ";
			return baseDAO.queryForMap(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：保存用户技能信息 创建人： ln 创建时间： 2016年10月18日 标记： 补充:{
	 * 服务价格单位company， }
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> addSkillInformation(Map<String, Object> map) {
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			map.put("id", Sequence.nextId());
			map.put("createDate", new Date());
			map.put("textURL", "");
			map.put("videoURL", "");
			String sql = " insert into `xiaoka-xydk`.user_skillinfo"
					+ "    (id,basicId,skillFatId,skillSonId,skillName,textURL,videoURL,skillPrice,serviceType,skillDepict,createDate,isDelete,company,isDisplay) "
					+ "    values "
					+ "    (:id,:basicId,:skillFatId,:skillSonId,:skillName,:textURL,:videoURL,:skillPrice,:serviceType,:skillDepict,:createDate,0,:company,0)";
			boolean flag = baseDAO.executeNamedCommand(sql, map);
			m.put("id", map.get("id"));
			m.put("flag", flag);
			return m;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：不分页获取技能大类 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryMaxCategoryList() {
		try {
			String sql = " SELECT * from `xiaoka-xydk`.user_skillclass_father where isDelete = 0 and status = 1";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 根据一级分类的id或取二级分类
	 * 
	 * @param fid
	 * @return
	 */
	public List<Map<String, Object>> querySonCategoryList(String fid) {
		try {
			String sql = " SELECT t.* from `xiaoka-xydk`.user_skillclass_son t where t.isDelete = 0 and t.status = 1 and t.fatherId="
					+ fid;
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：不分页获取技能小类 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryMinCategoryList() {
		try {
			String sql = " SELECT * from `xiaoka-xydk`.user_skillclass_son where isDelete = 0 and status = 1";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据技能大类ID获取技能小类列表 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param mId
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryMinCategoryByMaxId(String mId) {
		try {

			String sql = "SELECT id as value,className as text from `xiaoka-xydk`.user_skillclass_son where status = 1 and isDelete = 0 and fatherId="
					+ mId + "";// fatherId

			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据openId获取用户基本信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param openId
	 * @return
	 * @version
	 */
	public Map<String, Object> isUserExist(String openId) {
		try {
			String sql = " SELECT b.*,si.schoolName,ct.cityName FROM `xiaoka-xydk`.user_basicinfo  b LEFT JOIN `xiaoka`.tg_school_info si on si.id=b.schoolId "
					+ " LEFT JOIN tg_city ct ON ct.id=b.cityId"
					+ "   WHERE b.isDelete = 0 and b.openId = '"
					+ openId
					+ "' ";
			return baseDAO.queryForMap(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取用户技能信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param id
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> querySkillInfo(String id) {
		try {
			String sql = " select s.id as id,basicId,skillFatId,skillSonId,skillName as subTitle,skillDepict as details,skillPrice as price,serviceType,"
					+ "isDisplay as isDisplay,ss.className as className,s.sellNo as sellNo,ss.id as sid,dd.dic_name as priceName,b.headPortrait as headPortrait,fat.classValue  "
					+ "  from `xiaoka-xydk`.user_skillinfo s"
					+ " LEFT JOIN `xiaoka-xydk`.user_skillclass_son ss ON ss.id=s.skillSonId "
					+ " LEFT JOIN `xiaoka-xydk`.user_basicinfo b ON b.id=s.basicId "
					+ " LEFT JOIN v2_dic_data dd ON s.company=dd.id  "
					+ " LEFT JOIN `xiaoka-xydk`.user_skillclass_father fat ON s.skillFatId=fat.id"
					+ "  where s.isDelete = 0 and s.basicId = "
					+ id
					+ ""
					+ " order by s.createDate desc LIMIT 0,99";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据分类ID获取子集分类 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public Map<String, Object> getMinCategory(Map<String, Object> paramsMap) {

		try {
			String sql = " select * from `xiaoka-xydk`.user_skillclass_son where isDelete = 0 and id = "
					+ paramsMap.get("skillSonId") + " ";
			return baseDAO.queryForMap(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：添加图片，添加skillId 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param skillId
	 * @param img
	 * @return
	 * @version
	 */
	public boolean addImgBySkillId(String skillId, String img) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("skillId", skillId);
		map.put("imgURL", img);
		try {
			map.put("id", Sequence.nextId());
			map.put("createDate", new Date());
			String sql = " insert into `xiaoka-xydk`.user_skill_image"
					+ "    (id,skillId,imgURL,imgType,createDate) "
					+ "    values "
					+ "    (:id,:skillId,:imgURL,1,:createDate)";
			boolean flag = baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据技能ID获取技能信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param skillId
	 * @return
	 * @version
	 */
	public Map<String, Object> getSkillInfoById(String skillId) {
		try {
			String sql = " SELECT s.id, s.basicId, s.skillFatId,s.sellNo, s.skillSonId, s.skillName, s.textURL, s.videoURL, s.skillPrice, s.serviceType, s.skillDepict, s.createDate, s.isDelete, s.company, s.isDisplay,su.imageURL,ss.serviceRatio,ss.diplomatRatio,ss.platformRatio, ss.className,dd.dic_name "
					+ " FROM `xiaoka-xydk`.user_skillinfo s "
					+ " LEFT JOIN `xiaoka-xydk`.user_skillclass_son ss on ss.id = s.skillSonId "
					+ " LEFT JOIN `xiaoka-xydk`.user_skillclass_father su on su.id = s.skillFatId "
					+ " LEFT JOIN `xiaoka`.v2_dic_data as dd ON dd.id = s.company"
					+ "	 where s.isDelete = 0 and s.id = " + skillId + "";
			return baseDAO.queryForMap(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据技能ID获取技能图片 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param skillId
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> getSkillImgBySkillList(String skillId) {
		try {
			String sql = " select id,skillId,imgURL,imgType,createDate from `xiaoka-xydk`.user_skill_image where skillId = "
					+ skillId + " ";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：删除技能信息 创建人： ln 创建时间： 2016年10月18日 标记：
	 * 
	 * @param class1
	 * @return
	 * @version
	 */
	public boolean deleteSkillInfo(Map<String, Object> map) {
		try {
			map.put("isDelete", 1);
			String sql = "update `xiaoka-xydk`.user_skillinfo set "
					+ " isDelete=:isDelete" + " where id=:skillId";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：修改技能信息 创建人： ln 创建时间： 2016年10月19日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean editSkillInfo(Map<String, Object> map) {
		map.put("createDate", new Date());

		try {
			// String sql = "update `xiaoka-xydk`.user_skillinfo set ";
			// if (null != map.get("skillFatId") &&
			// StringUtils.isNotEmpty(map.get("skillFatId").toString())) {
			// sql = sql + "			skillFatId=:skillFatId,";
			// }
			// if (null != map.get("skillSonId") &&
			// StringUtils.isNotEmpty(map.get("skillSonId").toString())) {
			// sql = sql + "			skillSonId=:skillSonId,";
			// }
			// if (null != map.get("skillName") &&
			// StringUtils.isNotEmpty(map.get("skillName").toString())) {
			// sql = sql + "			skillName=:skillName,";
			// }
			// if (null != map.get("textURL") &&
			// StringUtils.isNotEmpty(map.get("textURL").toString())) {
			// sql = sql + "			textURL=:textURL,";
			// }
			// if (null != map.get("videoURL") &&
			// StringUtils.isNotEmpty(map.get("videoURL").toString())) {
			// sql = sql + "			videoURL=:videoURL,";
			// }
			// if (null != map.get("skillPrice") &&
			// StringUtils.isNotEmpty(map.get("skillPrice").toString())) {
			// sql = sql + "			skillPrice=:skillPrice,";
			// }
			// if (null != map.get("serviceType") &&
			// StringUtils.isNotEmpty(map.get("serviceType").toString())) {
			// sql = sql + "			serviceType=:serviceType,";
			// }
			// if (null != map.get("skillDepict") &&
			// StringUtils.isNotEmpty(map.get("skillDepict").toString())) {
			// sql = sql + "			skillDepict=:skillDepict,";
			// }
			// if (null != map.get("isDisplay") &&
			// StringUtils.isNotEmpty(map.get("isDisplay").toString())) {
			// sql = sql + "			isDisplay=:isDisplay,";
			// }
			// if (null != map.get("createDate") &&
			// StringUtils.isNotEmpty(map.get("createDate").toString())) {
			// sql = sql + "			createDate=:createDate";
			// }
			// if (null != map.get("company") &&
			// StringUtils.isNotEmpty(map.get("company").toString())) {
			// sql = sql + "			company=:company";
			// }

			String sql = "update `xiaoka-xydk`.user_skillinfo set skillFatId=:skillFatId,company=:company,skillSonId=:skillSonId,skillName=:skillName,skillPrice=:skillPrice,serviceType=:serviceType,skillDepict=:skillDepict";
			sql = sql + " where id=:id";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：根据技能ID删除技能图片 创建人： ln 创建时间： 2016年10月19日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean deleteImgBySkillId(Map<String, Object> map) {
		try {

			String sql = "delete from `xiaoka-xydk`.user_skill_image where skillId = "
					+ map.get("skillId") + " ";

			return baseDAO.executeNamedCommand(sql, map);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：修改基本信息 创建人： ln 创建时间： 2016年10月20日 标记：
	 * 
	 * @param paramsMap
	 * @return
	 * @version
	 */
	public boolean editBasicInfo(Map<String, Object> map) {
		try {
			String sql = "update `xiaoka-xydk`.user_basicinfo set "
					+ " userName=:userName,cityId=:cityId,schoolId=:schoolId,phoneNumber=:phoneNumber,email=:email,wxNumber=:wxNumber,aboutMe=:aboutMe "
					+ " where id=:basicId";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * 修改个人基本信息 dxf return flag
	 */
	public boolean editBasicInfoVersion2(Map<String, Object> map) {
		try {
			String sql = "update `xiaoka-xydk`.user_basicinfo set "
					+ " userName=:userName,auditState=0,";
			if (null != map.get("cityId")
					&& StringUtils.isNotEmpty(map.get("cityId").toString())
					&& !"null".equals(map.get("cityId"))) {
				sql = sql + " cityId=:cityId,";
			}
			if (null != map.get("schoolId")
					&& StringUtils.isNotEmpty(map.get("schoolId").toString())
					&& !"null".equals(map.get("schoolId"))) {

				sql = sql + " schoolId=:schoolId,";
			}
			if (null != map.get("checkType")
					&& StringUtils.isNotEmpty(map.get("checkType").toString())) {
				sql = sql + " checkType=:checkType,";
			}
			if (null != map.get("nickname")
					&& StringUtils.isNotEmpty(map.get("nickname").toString())) {
				sql = sql + " nickname=:nickname,";
			}
			if (null != map.get("headPortrait")
					&& StringUtils.isNotEmpty(map.get("headPortrait")
							.toString())) {
				sql = sql + " headPortrait=:headPortrait,";
			}
			sql = sql
					+ " phoneNumber=:phoneNumber,email=:email,wxNumber=:wxNumber,aboutMe=:aboutMe "
					+ " where id=:id";
			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：删除用户上传的技能图片 创建人： ln 创建时间： 2016年11月2日 标记：wap
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @version
	 */
	public boolean delImgById(Map<String, Object> map) {
		try {
			String sql = "delete from `xiaoka-xydk`.user_skill_image where id = "
					+ map.get("id") + " ";

			return baseDAO.executeNamedCommand(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * 获取发布服务banner图
	 * <p>
	 * TODO(这里描述这个方法详情– 可选) 无参数 类型区分 bannerType = 2
	 */

	public List<BannerInfo> getSkillBanner() {

		String sql = "select t.* from `xiaoka-xydk`.banner_info t where bannerType = 2 and  isDelete = 0 and isUse = 1 order by t.ord asc";
		List<BannerInfo> skillBanner = baseDAO.getJdbcTemplate().query(
				sql,
				new Object[] {},
				ParameterizedBeanPropertyRowMapper
						.newInstance(BannerInfo.class));
		return skillBanner;

	}

	/**
	 * 
	 * 获取个人信息服务banner图
	 * <p>
	 * 无参数 类型区分 bannerType = 3
	 */
	public List<BannerInfo> getUserBanner() {

		String sql = "select t.* from `xiaoka-xydk`.banner_info t where bannerType = 3 and  isDelete = 0 and isUse = 1 order by t.ord asc";
		List<BannerInfo> userBanner = baseDAO.getJdbcTemplate().query(
				sql,
				new Object[] {},
				ParameterizedBeanPropertyRowMapper
						.newInstance(BannerInfo.class));
		return userBanner;

	}

	/**
	 * 
	 * 技能开启，关闭
	 * <p>
	 * 技能id
	 */
	public boolean updateDisplay(Map<String, Object> paramsMap) {
		String qySql = "select skillStatus from `xiaoka-xydk`.user_basicinfo where id=:basicId";
		Map<String,Object> map =baseDAO.queryForMap(qySql, paramsMap);
		String skillStatus = String.valueOf(map.get("skillStatus"));
		if("0".equals(skillStatus)){
			paramsMap.put("isDisplay","1");			
		}else{
			paramsMap.put("isDisplay","0");			
		}
		
		String sql = "update `xiaoka-xydk`.user_skillinfo set isDisplay=:isDisplay  where basicId=:basicId";
		String baSql = "update `xiaoka-xydk`.user_basicinfo set skillStatus= :isDisplay ";
			   baSql+=" WHERE id=:basicId";
			
			baseDAO.executeNamedCommand(baSql, paramsMap);
		return baseDAO.executeNamedCommand(sql, paramsMap);
	}

	/**
	 * ] 根据dicType或取dicData
	 * 
	 * @param dicType
	 * @return
	 */
	public List<Map<String, Object>> getDicData(String dicType) {
		String sql = "select dd.id as value,dd.dic_name as text from `xiaoka`.v2_dic_type t LEFT JOIN `xiaoka`.v2_dic_data dd ON t.dic_id = dd.dic_id where t.isDelete = 0 and t.dic_code = '"
				+ dicType + "'";
		return baseDAO.queryForList(sql);
	}

	/*
	 * 修改背景图片 dxf
	 */
	public boolean upBackPicture(Map<String, Object> paramsMap) {
		String sql = "update `xiaoka-xydk`.user_basicinfo set backPicture=:imgUrl where id=:id";
		return baseDAO.executeNamedCommand(sql, paramsMap);
	}

	/**
	 * 
	 * 获取单位id
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 *
	 * @param company
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public Map<String, Object> getCompanyId(String company) {

		String sql = "select dd.id as cpid,dd.dic_name from v2_dic_type dt "
				+ "LEFT JOIN v2_dic_data dd ON dt.dic_id=dd.dic_id "
				+ " where dt.dic_code='companyType' and dd.dic_value='"
				+ company + "' and dt.isDelete=0 and dd.isDelete=0 ";

		return baseDAO.queryForMap(sql);
	}

	/**
	 * 
	 * 描述:获取用户的集合 作者:gyp
	 * 
	 * @Date 2017年4月17日
	 */
	public List<Map<String, Object>> getUserBasicInfoList() {
		try {
			String sql = "select t.openId as openId,t.nickname as nickname from `xiaoka-xydk`.user_basicinfo t where t.isDelete = 0 AND openId <> '0' GROUP BY t.openId";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * 项目名称：dlws-xiaoka-xydk 描述：获取标签列表信息 创建人： dxf 
	 * 
	 * @return
	 * @version
	 */
	public List<Map<String, Object>> queryLabeList() {
		try {
			String sql = "SELECT id as value,labelName as text FROM `xiaoka-xydk`.label WHERE isDelete=0";
			return baseDAO.queryForList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
